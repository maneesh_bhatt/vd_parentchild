﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class PrintVendorStatusReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Loc"]) && !string.IsNullOrEmpty(Request.QueryString["Sup"]) && !string.IsNullOrEmpty(Request.QueryString["Type"]))
            {
                divVendorStatusReport.InnerHtml = GenerateReport(Convert.ToString(Request.QueryString["Loc"]), Convert.ToString(Request.QueryString["Sup"]), Convert.ToString(Request.QueryString["Type"])).ToString();
            }
        }
    }

    protected StringBuilder GenerateReport(string location, string supervisor, string vendortype)
    {
        StringBuilder sb = new StringBuilder();
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);

        try
        {
            string condition = "";
            string query = "";
            string type = "";
            if (vendortype == "Int")
            {
                type = "Internal Vendor";
            }
            else if (vendortype == "Ext")
            {
                type = "External Vendor";
            }

            if (type != "")
            {
                condition = " vm.Vendor_Type ='" + type + "'";
            }
            if (supervisor != "")
            {
                if (condition != "")
                {
                    condition = condition + " and vm.Allocated_To='" + supervisor + "'";
                }
                else
                {
                    condition = " vm.Allocated_To='" + supervisor + "'";
                }
            }
            if (location != "")
            {
                if (condition != "")
                {
                    condition = condition + " and vm.Vendor_Name='" + location + "'";
                }
                else
                {
                    condition = " vm.Vendor_Name='" + location + "'";
                }
            }

            if (condition != "")
            {
                query = "select vm.Allocated_To, vm.Vendor_Name, lig.Tool_No, lig.Station, lig.Rwk, lig.P_No, isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as PendingQty, '' as Verified_By "
                                + " from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no "
                                + " left join Vendor_Master_Job_Work vm on lig.Location=vm.Vendor_Name where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) and vm.Vendor_Type!='End Store' and "
                                + condition + "  order by vm.Allocated_To Asc, PendingQty Desc, vm.Vendor_Name Asc";
            }
            else
            {
                query = "select vm.Allocated_To, vm.Vendor_Name, lig.Tool_No, lig.Station, lig.Rwk, lig.P_No, isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as PendingQty, '' as Verified_By "
                               + " from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no "
                               + " left join Vendor_Master_Job_Work vm on lig.Location=vm.Vendor_Name where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) and vm.Vendor_Type!='End Store'"
                               + " order by vm.Allocated_To Asc, PendingQty Desc, vm.Vendor_Name Asc";

            }
            SqlDataAdapter ad = new SqlDataAdapter(query, connec);
            DataTable dt = new DataTable();
            ad.Fill(dt);

            sb.Append("<div style='width:1300px;'><h2 style='text-align:center;'>Visit Record</h2>");

            sb.Append("<table style='table-layout:fixed;width:100%;border:1px solid black;border-collapse:collapse;font-size: 12px;line-height: 22px;font-family:Verdana;text-align:center;'>");
            sb.Append("<tr><td style='border:1px solid black;font-weight:bold;width:50px;'> Sr. No.</td>");

            sb.Append("<td style='border:1px solid black;font-weight:bold;width:120px;'>Supervisor Name</td> <td style='border:1px solid black;font-weight:bold;width:200px;'>Vendor Name</td>");



            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int srNo = i + 1;
                sb.Append("<tr><td style='border:1px solid black;'>" + srNo + "</td>");
                sb.Append("<td style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["Vendor_Type"]) + "</td>");
                sb.Append("<td style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["Allocated_To"]) + "</td> <td style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["Vendor_Name"]) + "</td>");
                sb.Append("<td style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["Frequency_Of_Verification"]) + "</td>");
            }

            sb.Append("</tr>");
            sb.Append("</table></div>");
            return sb;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return sb;
        }
        finally
        {
            connec.Close();
        }
    }
}