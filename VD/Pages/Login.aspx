﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Title="Login"
    Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
    <link rel="shortcut icon" type="image/x-icon" href="favicon/favicon.ico" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Login </title>
    <link rel="stylesheet" href="../css1/style3.css">
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin-top: 50px;" align="center">
        <asp:Image ID="Image1" BackColor="#3366FF" ImageUrl="~/images/FALCON LOGO half.jpg"
            runat="server"></asp:Image></div>
    <div>
        <section class="container">
    <div class="login">
      <h1>Login to Tool</h1> 
        <p><asp:TextBox ID="txtuserid" runat="server" value="" placeholder="Username or Email"></asp:TextBox></p>
        <p><asp:TextBox ID="txtpassword" runat="server" value="" placeholder="Password" 
                TextMode="Password"></asp:TextBox></p>
        <p class="remember_me">
          <label>
          <asp:CheckBox ID="checkbox" runat="server"></asp:CheckBox>
            Remember me on this computer
          </label>
        </p>
        <p class="submit"><asp:Button ID="btnsummit" runat="server" Text="Submit" 
                onclick="btnsummit_Click"></asp:Button></p>
    
    </div>

     
    <div><asp:Label ID="Label1" runat="server" Text=""></asp:Label></div>
  </section>
    </div>
    <div align="center">
        <table align="center" width="230px">
            <tr>
                <td colspan="4" align="left" style="color: Blue; font-size: smaller; background-color: white"
                    class="">
                    Copyright © Falcon AutoTech Pvt. Ltd.
                    <br />
                </td>
                <td>
                </td>
            </tr>
    </div>
    </form>
</body>
</html>