﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using activeDirectoryLdapExamples;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Text;

public partial class MasterPage : System.Web.UI.MasterPage
{
    Program obj = new Program();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null)
        {
            Response.Redirect("~/Pages/Login.aspx");
        }
        else
        {
             Label1.Text = Session["UserID"].ToString();
        }

        if (Convert.ToString(Session["UserID"]).ToLower() == "bharat_kaul")
        {
            listControlsRights.Visible = true;
        }
        else
        {
            listControlsRights.Visible = false;
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session["UserID"] = null;
        Response.Cookies["user_id"].Value = null;
        Response.Cookies["pwd"].Value = null;
        //Response.Redirect("~/Pages/Login.aspx");
        Response.Redirect("http://" + HttpContext.Current.Request.Url.Host + ":81/Login.aspx");
    }
}
