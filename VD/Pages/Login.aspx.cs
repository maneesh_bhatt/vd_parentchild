using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using activeDirectoryLdapExamples;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Cryptography;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Net.Mail;
using System.Net;
using System.Net.Cache;

public partial class Login : System.Web.UI.Page
{
    Program obj = new Program();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Cookies["user_id"] != null && Request.Cookies["pwd"] != null)
            {
                txtuserid.Text = Request.Cookies["user_id"].Value;
                txtpassword.Text = Request.Cookies["pwd"].Value;
                string IP = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Pages/LDapIP.txt"));
                authentication(IP);
            }
            else
            {
                Response.Redirect("http://" + HttpContext.Current.Request.Url.Host + ":81/Login.aspx");
            }
        }
    }

    protected void btnsummit_Click(object sender, EventArgs e)
    {
        string ip = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Pages/LDapIP.txt"));
        authentication(ip);
    }

    /// <summary>
    /// Setting the Values for Cookies
    /// </summary>
    private void setResponseCookies()
    {
        Response.Cookies["user_id"].Value = txtuserid.Text;
        Response.Cookies["pwd"].Value = txtpassword.Text;
        Response.Cookies["pwd"].Expires.AddYears(20);
        Response.Cookies["user_id"].Expires.AddYears(20);
    }

    /// <summary>
    /// Authenticating the given credentials are correct or not
    /// </summary>
    /// <param name="ip">IP of Server</param>
    private void authentication(String ip)
    {
        bool kk = obj.IsAuthenticated("LDAP://" + ip + "/CN=USERS,DC=falconautoonline,DC=com", txtuserid.Text, txtpassword.Text);
        if (kk)
        {
            Session["UserID"] = txtuserid.Text;
            Session["pass"] = txtpassword.Text;
            if (Request.Cookies["user_id"] == null && Request.Cookies["pwd"] == null)
            {
                setResponseCookies();
            }
            Response.Redirect("~/Pages/WelcomePage.aspx");
        }
        else
        {
            Label1.Text = "Invalid User or Password, Please Enter Correct userid and password";
            return;
        }
    }
}