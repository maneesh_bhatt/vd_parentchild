﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WelcomePage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("~/Pages/Login.aspx");
            }
            if (!string.IsNullOrEmpty(Request.QueryString["Error"]))
            {
                lblErrorMessage.Text = "You don't have rights to access this Page, Please contact Administrator";
            }
        }

    }
}