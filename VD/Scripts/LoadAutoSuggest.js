﻿$(function () {
    $('#<%=inputVendorName.ClientID%>').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "VendorMasterJobWorkData.aspx/GetVendorName",
                data: "{ 'text':'" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return { value: item }
                    }))
                }
            });
        },
        minLength: 0,
        scroll: true
    }).focus(function () {
        // The following works only once.
        // $(this).trigger('keydown.autocomplete');
        // As suggested by digitalPBK, works multiple times
        $(this).autocomplete("search", "");
    }).on('change', function (e, ui) {

        $('#<%=inputVendorName.ClientID%>').blur();

    }).on('autocompleteselect', function (e, ui) {

        $('#<%=inputVendorName.ClientID%>').blur();

    });
});
