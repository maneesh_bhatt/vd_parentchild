﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class GetPrintChallanTransferQC : System.Web.UI.Page
{
    String MIN = "";
    string location = "";
    SqlCommand cmd;
    SqlDataAdapter adp;

    protected void Page_Load(object sender, EventArgs e)
    {
        SqlTransaction transaction=null ;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            conn.Open();
            LblDt.Text = System.DateTime.Now.ToString("dd-MMM-yyyy");
            MIN = Session["QCchallan"].ToString();
            //location = Session["vendor"].ToString();

            int k = showData1();
            Label1.Text = k.ToString();

            transaction = conn.BeginTransaction();

            string sheet = "QCSheet" + Label1.Text;
            string s = "insert into part_history1(Challan_No) values('" + MIN + "')";
            SqlCommand cmdph = new SqlCommand();
            cmdph.Transaction = transaction;
            Label1.Text = sheet;
            cmdph.CommandText = s;
            cmdph.Connection = conn;
            cmdph.ExecuteNonQuery();

            SqlCommand cmdtph = new SqlCommand();
            cmdtph.Transaction = transaction;
            string s1 = "update   temp_part_history_QC set Qcsheet_no='" + sheet + "' where challan_no='" + MIN + "'";
            cmdtph.CommandText = s1;
            cmdtph.Connection = conn;
            cmdtph.ExecuteNonQuery();

            transaction.Commit();
            string query = "SELECT distinct [tool_no] ,[station]    ,[p_no]    ,[part_name]  ,[Received_Date] ,TransSentQty as QtySent ,location,[Challan_No] FROM  [dbo].[part_history_QC] where Challan_No='" + MIN + "'";
            UtilityFunctions.GetData(query);
            ExportGridView();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }
  
    public int showData1()
    {
        int id = 0;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string conString = "SELECT isnull(MAX(id),0) as id  FROM part_history1 with(nolock)";
        cmd = new SqlCommand(conString, conn);
        adp = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        adp.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            id = Convert.ToInt16(dt.Rows[0][0].ToString());
        }

        return id;
    }

    private void ExportGridView()
    {
        try
        {
            int count = 0;
            string dirpath = AppDomain.CurrentDomain.BaseDirectory;
            DataTable dt1 = new System.Data.DataTable();
            DataRow dr2 = null;
            dt1.Columns.Add(new DataColumn("Sno", typeof(string)));
            //dt1.Columns.Add(new DataColumn("Date", typeof(string)));
            //dt1.Columns.Add(new DataColumn("Vendor Name", typeof(string)));
            dt1.Columns.Add(new DataColumn("Tool No", typeof(string)));
            dt1.Columns.Add(new DataColumn("Station No", typeof(string)));

            dt1.Columns.Add(new DataColumn("Position No", typeof(string)));

            dt1.Columns.Add(new DataColumn("Total Incoming Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("Ok Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("Rejected Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("Parts Sent to party for Rework Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("Inhouse Rework Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("inhouse rework hours planned", typeof(string)));
            dt1.Columns.Add(new DataColumn("Est Inspection Time Hr", typeof(string)));


            dr2 = dt1.NewRow();
            dr2["Sno"] = 0;
            //dr2["Date"] = string.Empty;
            dr2["Tool No"] = string.Empty;
            dr2["Station No"] = string.Empty;

            dr2["Position No"] = string.Empty;
            dr2["Total Incoming Qty"] = string.Empty;
            dr2["Ok Qty"] = string.Empty;
            dr2["Rejected Qty"] = string.Empty;
            dr2["Parts Sent to party for Rework Qty"] = string.Empty;
            dr2["Inhouse Rework Qty"] = string.Empty;
            dr2["inhouse rework hours planned"] = string.Empty;

            dt1.Rows.Add(dr2);

            DataTable dt = (DataTable)Session["DataTableAdvanceReport"];

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                dr2 = dt1.NewRow();
                dt1.Rows.Add(dr2);
                dt1.Rows[i]["Sno"] = i + 1;
                DateTime currentdate = DateTime.Now;
                string currentdate1 = currentdate.ToString("dd/MM/yyyy");
                lblpartyname.Text = dt.Rows[i]["location"].ToString();
                lblchallanno2.Text = dt.Rows[i]["Challan_No"].ToString();
                //lblbillno.Text = dt.Rows[i]["BillNo"].ToString();
                //dt1.Rows[i]["Date"] = currentdate1;
                //dt1.Rows[i]["Vendor Name"] = dt.Rows[i]["location"].ToString();
                dt1.Rows[i]["Tool No"] = dt.Rows[i]["tool_no"];
                dt1.Rows[i]["Station No"] = dt.Rows[i]["station"].ToString();
                dt1.Rows[i]["Position No"] = dt.Rows[i]["p_no"].ToString();
                dt1.Rows[i]["Total Incoming Qty"] = dt.Rows[i]["QtySent"].ToString();

                count = i;
            }

            count = count + 1;
            dr2 = dt1.NewRow();
            dt1.Rows.Add(dr2);
            dt1.Rows[count][0] = "Details of Inhouse Rework Planned";
            count = count + 1;
            dr2 = dt1.NewRow();
            dt1.Rows.Add(dr2);
            dt1.Rows[count][0] = "Estimated Hours";

            count = count + 1;
            dr2 = dt1.NewRow();
            dt1.Rows.Add(dr2);

            dt1.Rows[count][0] = "Tool No";
            dt1.Rows[count][1] = "station no";
            dt1.Rows[count][2] = "Part No";
            dt1.Rows[count][3] = "Description";
            dt1.Rows[count][4] = "Qty";
            dt1.Rows[count][5] = "ML";
            dt1.Rows[count][6] = "SG";
            dt1.Rows[count][7] = "TUR";
            dt1.Rows[count][8] = "BW";
            dt1.Rows[count][9] = "fab";
            dt1.Rows[count][10] = "est.hr";

            for (int i = 1; i <= 35 - 1; i++)
            {
                dr2 = dt1.NewRow();
                dt1.Rows.Add(dr2);
            }
            //dt1.Rows.RemoveAt(0);
            //dt1.AcceptChanges();
            if (dt1.Rows.Count > 0)
            {
                Grdview1.DataSource = dt1;
                Grdview1.DataBind();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
       
    }

    protected void Grdview1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[0].Text == "Details of Inhouse Rework Planned")
            {
                e.Row.Cells[1].ColumnSpan = 3;
                e.Row.Cells[0].ColumnSpan = 12;
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                //now make up for the colspan from cell2
                e.Row.Cells.RemoveAt(1);
                e.Row.Cells.RemoveAt(2);
                e.Row.Cells.RemoveAt(3);
                e.Row.Cells.RemoveAt(4);
                e.Row.Cells.RemoveAt(5);
                //e.Row.Cells.RemoveAt(6);

                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                //e.Row.Cells[6].Visible = false;

            }

            if (e.Row.Cells[0].Text == "Estimated Hours")
            {
                e.Row.Cells[0].ColumnSpan = 13;
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells.RemoveAt(1);
                e.Row.Cells.RemoveAt(2);
                e.Row.Cells.RemoveAt(3);
                e.Row.Cells.RemoveAt(4);
                e.Row.Cells.RemoveAt(5);
                //e.Row.Cells.RemoveAt(6);
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
            }
        }
    }

    protected void Grdview1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string kk = e.Row.Cells[0].Text;
            if (e.Row.Cells[0].Text == "Estimated Hours")
            {

                e.Row.Cells[0].ColumnSpan = 12;
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells.RemoveAt(1);
                e.Row.Cells.RemoveAt(2);
                e.Row.Cells.RemoveAt(3);
                e.Row.Cells.RemoveAt(4);
                e.Row.Cells.RemoveAt(5);
                e.Row.Cells.RemoveAt(6);
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;

            }
        }

    }
}
