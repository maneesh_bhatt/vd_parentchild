﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="POSummaryReport.aspx.cs" Inherits="Tool_POSummaryReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .align_right
        {
            text-align: right;
        }
        .tableheader
        {
            line-height: 15px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#<%=inputToolNo.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ToolOutstandingPartSummary.aspx/GetTooNumber",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputToolNo.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputToolNo.ClientID%>').blur();

            });
        });
    </script>
    <script language="javascript" type="text/javascript">

        $(function () {
            $('#<%=inputVendorName.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ToolOutstandingPartSummary.aspx/GetVendorName",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputVendorName.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputVendorName.ClientID%>').blur();

            });

            $('#<%=inputToolNo.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ToolOutstandingPartSummary.aspx/GetTooNumber",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputToolNo.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputToolNo.ClientID%>').blur();

            });

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="border: 1px solid Blue; min-height: 100px; padding: 9px;">
        <div>
            <div align="center">
                <span class="style13">PO Summary Report</span>
            </div>
        </div>
        <div style="width: 100%; margin-bottom: 10px; margin-top: 20px; height: 36px;">
            <div style="font-size: 13px; width: 150px; float: left;">
                Location
                <br />
                <asp:TextBox runat="server" ID="inputVendorName" ClientIDMode="Static" Style="width: 140px;
                    float: left;" Placeholder=" Vendor Name"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 90px; float: left;">
                Tool No
                <br />
                <asp:TextBox ID="inputToolNo" runat="server" Width="80px"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 85px; float: left;">
                Rework No
                <br />
                <asp:TextBox ID="inputReworkNo" runat="server" Width="75px"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 85px; float: left;">
                Station
                <br />
                <asp:TextBox ID="inputStation" runat="server" Width="75px"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 90px; float: left;">
                Position No
                <br />
                <asp:TextBox ID="inputPNo" CssClass="glowing-border" runat="server" Width="80px"></asp:TextBox>
            </div>
            <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" ImageUrl="~/images/searchbtn.jpg"
                OnClick="btnSearch_Click" Style="margin-top: 9px; float: left;" />
            <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="btnDownload_Click"
                Visible="false" Style="margin-top: 9px; margin-right: 2px; float: right; background-color: green;
                color: white; padding: 6px; border: 1px solid green; box-shadow: 2px 3px 2px green;" />
            <span style="margin-top: 16px; float: left; margin-left: 10px;"><b>Sorted By</b>- PO
                Date Asc</span>
        </div>
        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateGrid">
            <ContentTemplate>
                <asp:GridView runat="server" ID="gridPOSummary" BackColor="White" AutoGenerateColumns="false"
                    EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True" AllowPaging="true"
                    AllowSorting="false" Width="100%" BorderColor="#B8BABD" BorderStyle="None" OnPageIndexChanging="gridPOSummary_PageIndexChanging"
                    OnRowCommand="gridPOSummary_OnRowCommand" OnRowDataBound="gridPOSummary_OnRowDataBound"
                    PageSize="100" BorderWidth="1px" Style="font-size: 10.5px; font-family: Verdana;
                    line-height: 26px;">
                    <HeaderStyle CssClass="tableheader" />
                    <RowStyle BackColor="White" ForeColor="Black" Font-Size="12px" />
                    <Columns>
                        <asp:BoundField DataField="PO_NO" HeaderText="PO NO" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="140px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="PO_Date" DataFormatString="{0:dd-MM-yyyy}" HeaderText="PO Date"
                            HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Tool_No" HeaderText="Tool" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="100px" ItemStyle-Width="100px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Station" HeaderText="Station" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Rwk_No" HeaderText="Rwk No" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="P_No" HeaderText="P No" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="PO_Amount" HeaderText="PO Amount" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="QtySent" HeaderText="QtySent" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="TotalAmount" HeaderText="Total Amount" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Delivery_Commit_Date" HeaderText="Expected Delivery Date(First)"
                            DataFormatString="{0:dd-MM-yyyy}" HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px"
                            HeaderStyle-ForeColor="White" />
                        <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px" HeaderText="Total Qty Received"
                            ItemStyle-Width="120px" HeaderStyle-ForeColor="White">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblTotalReceivedQty"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px" HeaderText="Actual Expected Delivery Date(Last)"
                            ItemStyle-Width="120px" HeaderStyle-ForeColor="White">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblActualDeliveryDate"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Challan_No" HeaderText="Challan No" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="User_Name" HeaderText="User Name" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                    </Columns>
                    <FooterStyle Font-Bold="false" />
                    <PagerStyle BackColor="#335599" ForeColor="White" Font-Size="Large" HorizontalAlign="Center"
                        CssClass="gridview" />
                    <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#000" />
                    <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                </asp:GridView>
                <asp:Label runat="server" ID="lblRecorrdsCount" CssClass="recordsstatus" Width="850px"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
