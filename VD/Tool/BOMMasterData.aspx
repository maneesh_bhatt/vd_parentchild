﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="BOMMasterData.aspx.cs" Inherits="Tool_BOMMasterData" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .align_right
        {
            text-align: right;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#<%=inputToolNo.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ToolOutstandingPartSummary.aspx/GetTooNumber",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputToolNo.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputToolNo.ClientID%>').blur();

            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="border: 1px solid Blue; min-height: 100px; padding: 9px;">
        <div>
            <div align="center">
                <span class="style13">BOM Master Data</span>
            </div>
        </div>
        <div style="width: 100%; margin-bottom: 10px; margin-top: 20px; height: 36px;">
            <div style="font-size: 13px; width: 90px; float: left;">
                Tool No
                <br />
                <asp:TextBox ID="inputToolNo" CssClass="glowing-border" runat="server" Width="80px"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 85px; float: left;">
                Rework No
                <br />
                <asp:TextBox ID="inputReworkNo" CssClass="glowing-border" runat="server" Width="75px"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 85px; float: left;">
                Station
                <br />
                <asp:TextBox ID="inputStation" CssClass="glowing-border" runat="server" Width="75px"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 90px; float: left;">
                Position No
                <br />
                <asp:TextBox ID="inputPNo" CssClass="glowing-border" runat="server" Width="80px"></asp:TextBox>
            </div>
            <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" ImageUrl="~/images/searchbtn.jpg"
                OnClick="btnSearch_Click" Style="margin-top: 9px;" />
            <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="btnDownload_Click"
                Visible="false" Style="margin-top: 9px; margin-right: 2px; float: right; background-color: green;
                color: white; padding: 6px; border: 1px solid green; box-shadow: 2px 3px 2px green;" />
        </div>
        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateGrid">
            <ContentTemplate>
                <asp:GridView runat="server" ID="gridBOM" BackColor="White" AutoGenerateColumns="false"
                    EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True" AllowPaging="true"
                    AllowSorting="false" Width="100%" BorderColor="#B8BABD" BorderStyle="None" OnPageIndexChanging="gridBOM_PageIndexChanging"
                    OnRowCommand="gridBOM_OnRowCommand" OnRowDataBound="gridBOM_OnRowDataBound" PageSize="100"
                    BorderWidth="1px" Style="font-size: 12px; font-family: Verdana; line-height: 26px;">
                    <HeaderStyle CssClass="tableheader" />
                    <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                    <Columns>
                        <asp:BoundField DataField="Tool_No" HeaderText="Tool" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="140px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Station" HeaderText="Station" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Rwk_No" HeaderText="Rwk_No" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px" HeaderText="P No"
                            ItemStyle-Width="120px" HeaderStyle-ForeColor="White">
                            <ItemTemplate>
                                <%# Eval("p_no")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="part_name" HeaderText="Part Name" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="250px" ItemStyle-Width="250px" HeaderStyle-ForeColor="White" />
                              <asp:BoundField DataField="Type" HeaderText="Type" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" ItemStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Version_Number" HeaderText="Version" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="150px" ItemStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Upload_Qty" HeaderText="Upload Qty" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Bom_Qty" HeaderText="BOM Qty" ControlStyle-Font-Bold="true" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Created_Date" HeaderText="Date of Upload" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px" HeaderText="BOM Url"
                            ItemStyle-Width="120px" HeaderStyle-ForeColor="White">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkBtnBOMUrl"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle Font-Bold="false" />
                    <PagerStyle BackColor="#335599" ForeColor="White" Font-Size="Large" HorizontalAlign="Center"
                        CssClass="gridview" />
                    <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#000" />
                    <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                </asp:GridView>
                <asp:Label runat="server" ID="lblRecorrdsCount" CssClass="recordsstatus" Width="850px"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
