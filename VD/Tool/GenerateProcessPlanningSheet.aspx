﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="GenerateProcessPlanningSheet.aspx.cs" Inherits="Tool_GenerateProcessPlanningSheet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <style type="text/css">
        .style2, .style1
        {
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <table style="padding-left: 36px; width: 80%; line-height: 40px;" border="1px" align="center">
        <tr>
            <td colspan="2" align="center" style="font-size: x-large; color: black; border: none;">
                <span class="style1">Generate Process Planning Sheet</span>
            </td>
        </tr>
        <tr>
            <td style="margin-top: 50px;" align="left" class="style2">
                Browse File
            </td>
            <td class="style2" colspan="2" align="center">
                <asp:FileUpload ID="FileUploadControl" runat="server" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only Exl file is allowed!"
                    ValidationExpression="^.+(.xlsx|.xls|.csv)$" ControlToValidate="FileUploadControl"> </asp:RegularExpressionValidator>
                <br />
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="2">
                <asp:Label ID="lblErrorMessage" ForeColor="Red" runat="server" Style="line-height: 20px;"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
            <td class="style2" align="left">
                <asp:ImageButton ID="btnsave" Width="99px" ImageUrl="~/Tool/upload.jpg" Height="44px"
                    Style="margin-left: 198px;" Visible="true" Text="Upload" runat="server" OnClick="UploadButton_Click" />
                <asp:Label ID="lbl" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="border: none;">
                <asp:HyperLink runat="server" ID="hypDownloadFile" Visible="false">Download File</asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>
