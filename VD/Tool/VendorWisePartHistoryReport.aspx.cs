﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using MySql.Data.MySqlClient;
using System.Data;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text.RegularExpressions;
using ClosedXML.Excel;


public partial class Tool_VendorWisePartHistoryReport : System.Web.UI.Page
{
    /// <summary>
    /// Binds the Vendor List from Vendor Master Job Work table
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ";
            string dataTextField = "VENDOR_NAME";
            string dataValueField = "ID";
            UtilityFunctions.bind_vendor(ddllocation, query, dataTextField, dataValueField);
        }
    }

    /// <summary>
    /// Binds the Grid for Search Criteria
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            string partsGrCondition = "";

            if (ddllocation.Text != "")
            {
                if (partsGrCondition != "")
                {
                    partsGrCondition = partsGrCondition + " and phg.location='" + ddllocation.SelectedItem.Text + "'";
                }
                else
                {
                    partsGrCondition = " phg.location='" + ddllocation.SelectedItem.Text + "'";
                }
            }
            if (inputToolNo.Text != "")
            {
                if (partsGrCondition != "")
                {
                    partsGrCondition = partsGrCondition + " and phg.tool_no='" + inputToolNo.Text + "'";
                }
                else
                {
                    partsGrCondition = "  phg.tool_no='" + inputToolNo.Text + "'";
                }
            }
            if (inputStation.Text != "")
            {
                if (partsGrCondition != "")
                {
                    partsGrCondition = partsGrCondition + " and phg.station='" + inputStation.Text + "'";
                }
                else
                {
                    partsGrCondition = "  phg.station='" + inputStation.Text + "'";
                }
            }
            if (inputPNo.Text != "")
            {
                if (partsGrCondition != "")
                {
                    partsGrCondition = partsGrCondition + " and phg.p_no='" + inputPNo.Text + "'";
                }
                else
                {
                    partsGrCondition = "  phg.p_no='" + inputPNo.Text + "'";
                }
            }
            if (inputReworkNo.Text != "")
            {
                if (partsGrCondition != "")
                {
                    partsGrCondition = partsGrCondition + " and phg.rwk_no='" + inputReworkNo.Text + "'";
                }
                else
                {
                    partsGrCondition = "  phg.rwk_no='" + inputReworkNo.Text + "'";
                }
            }
            if (ddlOperation.SelectedValue != "")
            {
                if (partsGrCondition != "")
                {
                    partsGrCondition = partsGrCondition + " and phg.Sent_Or_Rec='" + ddlOperation.SelectedValue + "'";
                }
                else
                {
                    partsGrCondition = "  phg.Sent_Or_Rec='" + ddlOperation.SelectedValue + "'";
                }
            }

            if (ddlDateInterval.SelectedValue != "")
            {
                int month = Convert.ToInt32(ddlDateInterval.SelectedValue);
                partsGrCondition = partsGrCondition + " and Convert(Date,phg.Date_Of_Trans)>='" + Convert.ToDateTime(DateTime.Now.AddMonths(-month)).ToString("yyyy/MM/dd") + "' and Convert(Date,phg.Date_of_Trans)<='" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd") + "'";
            }
            else
            {
                if (inputFromDate.Text != "" && inputToDate.Text != "")
                {
                    if (partsGrCondition != "")
                    {
                        partsGrCondition = partsGrCondition + " and Convert(Date,phg.Date_Of_Trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,phg.Date_of_Trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                    else
                    {
                        partsGrCondition = " Convert(Date,phg.Date_Of_Trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,phg.Date_of_Trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                }
                else if (inputFromDate.Text != "" && inputToDate.Text == "")
                {
                    if (partsGrCondition != "")
                    {
                        partsGrCondition = partsGrCondition + " and Convert(Date,phg.Date_Of_Trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                    else
                    {
                        partsGrCondition = " Convert(Date,phg.Date_Of_Trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                }
                else if (inputFromDate.Text == "" && inputToDate.Text != "")
                {
                    if (partsGrCondition != "")
                    {
                        partsGrCondition = partsGrCondition + " and Convert(Date,phg.Date_Of_Trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                    else
                    {
                        partsGrCondition = " Convert(Date,phg.Date_Of_Trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                }
            }

            if (ddlDateInterval.SelectedValue != "" || (inputFromDate.Text != "" || inputToDate.Text != ""))
            {
                conn.Open();
                SqlCommand cmd = new System.Data.SqlClient.SqlCommand("select phg.location,phg.tool_no,phg.p_no,phg.part_name,phg.station,phg.rwk_no,phg.Sent_Or_Rec as Operation, phg.Quantity, phg.Date_Of_Trans,phg.User_Name from part_history_Gr phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no  where  (isnull(isnull(phg.Upload_Qty,0)-isnull(phg.QtySent,0),0)>=0 and isnull(phg.quantity,0)>=0 and isnull(phg.QtySent,0)>=0 ) and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and phg.location!='Company' and " + partsGrCondition + "  order by phg.location , phg.p_no, phg.part_name, phg.station, phg.rwk_no asc", conn);
                cmd.CommandTimeout = 0;
                gridViewNegativeData.DataSource = cmd.ExecuteReader();
                gridViewNegativeData.DataBind();
                if (gridViewNegativeData.Rows.Count > 0)
                {
                    btnDownload.Visible = true;
                }
                else
                {
                    btnDownload.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Time Interval or Select From Date or To Date');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }

    protected void ExportToExcel()
    {
        Response.ClearContent();
        string fileName = "VendorWiseData";
        string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gridViewNegativeData.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    protected void gridViewNegativeData_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);
            try
            {
                conn.Open();
                string pno = "", partname = "", station = "", rwkno = "", toolno = "";
                pno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "p_no"));
                partname = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "part_name"));
                station = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "station"));
                rwkno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "rwk_no"));
                toolno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "tool_no"));
                decimal quant_sent = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Quantity"));
                string operation = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "operation"));
                if (operation == "Sent")
                {
                    System.Web.UI.WebControls.Label lblEstimatedHrs = e.Row.FindControl("lblEstimatedHrs") as System.Web.UI.WebControls.Label;

                    SqlCommand cmd = new SqlCommand();

                    cmd = new SqlCommand("select opr16 from Parts_Gr where tool_no='" + toolno + "'  and  p_no='" + pno + "' and part_name='" + partname + "' and station='" + station + "' and rwk_no='" + rwkno + "' order by Parts_Gr_Id desc", conn);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            lblEstimatedHrs.Text = CalculateTime(Convert.ToString(reader["Opr16"]), quant_sent);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                conn.Close();
            }
        }
    }

    protected string CalculateTime(string opr16, decimal quantSent)
    {
        string resultText = "";
        decimal result = 0;
        decimal totalTime = 0;
        bool isActiveOperation = decimal.TryParse(opr16, out result);
        if (isActiveOperation)
        {
            decimal roundValue = Math.Round(result * 60);
            totalTime = quantSent * roundValue;
        }
        if (totalTime > 60)
        {
            int hours = 0, minutes = 0;
            hours = Convert.ToInt32(totalTime) / 60;
            minutes = Convert.ToInt32(totalTime) - (60 * hours);
            string min = "", hr = "";
            if (minutes < 10)
            {
                min = "0" + minutes;
            }
            else
            {
                min = Convert.ToString(minutes);
            }
            if (hours < 10)
            {
                hr = "0" + hours;
            }
            else
            {
                hr = Convert.ToString(hours);
            }
            resultText = hr + ":" + min;
        }
        else if (totalTime > 0)
        {
            if (totalTime < 10)
            {
                resultText = "00:" + "0" + Convert.ToInt32(totalTime);
            }
            else
            {
                resultText = "00:" + Convert.ToInt32(totalTime);
            }
        }
        return resultText;
    }
}