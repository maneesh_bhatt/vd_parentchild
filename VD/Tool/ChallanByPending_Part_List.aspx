﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.master"
    CodeFile="ChallanByPending_Part_List.aspx.cs" EnableEventValidation="false" Inherits="ChallanByPending_Part_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <style type="text/css">    
        .Grid
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .Grid td
        {
            background-color: Highlight;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Grid th
        {
            background-color: Navy;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .ChildGrid td
        {
            background-color: #eee !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .ChildGrid th
        {
            background-color: #6C6C6C !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid td
        {
            background-color: #fff !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid th
        {
            background-color: #2B579A !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
    </style>
    <style type="text/css">
        .style2
        {
            border: none;
        }
 .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {

            $("[id*=imgProductsShow]").each(function () {
                if ($(this)[0].src.indexOf("minus") != -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
                    $(this).next().remove();
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="margin-left: 0px; margin-right:15px;" align="left" width="950px">
        <tr>
            <td colspan="55" align="center" style="font-size: x-large; color: black">
                <span class="style1">Challan wise Pending Part List </span>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td height="20px">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            <br />
                Challan No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td height="20px" align="right" valign="bottom">
               <asp:TextBox ID="tbchallanTool" CssClass="glowing-border"  runat="server" Width="200px" Height="30px"></asp:TextBox>
            </td>
            <td height="20px" valign="bottom">
                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/images (1).jpg" runat="server"
                    OnClick="ImageButton1_Click" Style="height: 30px" Height="20px" />
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom" colspan="2">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td height="20px" valign="bottom">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
           
        </tr>
        <tr>
            <td class="style2">
            </td>
            <td class="style2" colspan="22">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td class="style2" colspan="24">
                <asp:ImageButton ID="ImageButton2" ImageUrl="~/images/print.png" runat="server" 
                    onclick="ImageButton2_Click" Width="100px" /></td>
            <td class="style2" colspan="2">
                <span style="color: Blue;">
                    <asp:Label ID="lblTotal" runat="server" Text="Total:"></asp:Label><asp:Label ID="lblTotal1"
                        runat="server" Text=""></asp:Label></span>
            </td>
            <td class="style2">
            </td>
            <td class="style2">
            </td>
        </tr>
        <tr>
            <td class="style1">
            </td>
            <td class="style1" colspan="46">
                <asp:Label ID="lbl" runat="server"></asp:Label>
            </td>
            <td class="style1" colspan="2">
            </td>
            <td class="style1">
            </td>
            <td class="style1">
            </td>
        </tr>
        <tr>
            <td colspan="54" align="left">
                <asp:GridView ID="GridView1" runat="server" CssClass="Grid" DataKeyNames="Challan_No"
                    AutoGenerateColumns="false" EmptyDataText="No Record Found" ForeColor="#333333"
                    GridLines="None" OnPageIndexChanging="GridView1_PageIndexChanging" Font-Size="13px">
                    <RowStyle BackColor="#00CC00" ForeColor="#333333" />
                    <Columns>
                        <asp:TemplateField HeaderStyle-BackColor="Green">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgProductsShow" runat="server" OnClick="Show_Hide_OrdersGrid"
                                    ImageUrl="~/images/plus.png" CommandArgument="Show" />
                                <asp:Panel ID="pnlProducts" runat="server" Visible="false" Style="position: relative">
                                    <asp:GridView ID="gvProducts" runat="server" AutoGenerateColumns="false" CssClass="ChildGrid">
                                        <Columns>
                                            <asp:BoundField ItemStyle-HorizontalAlign="center" ItemStyle-Width="300px" DataField="tool_no" HeaderText="Tool no" />
                                            <asp:BoundField ItemStyle-HorizontalAlign="center" ItemStyle-Width="300px" DataField="station" HeaderText="station" />
                                            <asp:BoundField ItemStyle-HorizontalAlign="center" ItemStyle-Width="300px" DataField="p_no" HeaderText="P No" />
                                            <asp:BoundField ItemStyle-Width="300px" DataField="location" HeaderText="location" />
                                            <asp:BoundField ItemStyle-HorizontalAlign="center" ItemStyle-Width="300px" DataField="qtypending" HeaderText="QtyPending" />
                                            <asp:BoundField ItemStyle-HorizontalAlign="center" ItemStyle-Width="300px" DataField="Upload_Qty" HeaderText="Upload Qty" />
                                             <asp:BoundField ItemStyle-HorizontalAlign="center" ItemStyle-Width="300px" DataField="TransSentQty" HeaderText="Receive Qty" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ItemStyle-Width="200px" HeaderStyle-Width="200px" ItemStyle-HorizontalAlign="center" DataField="Challan_No"
                            HeaderText="Challan No" />
                        <asp:BoundField ItemStyle-Width="200px" HeaderStyle-Width="200px" ItemStyle-HorizontalAlign="center" DataField="tool_no"
                            HeaderText="Tool no" />
                        <asp:BoundField ItemStyle-Width="300px" HeaderStyle-Width="200px" ItemStyle-HorizontalAlign="left" DataField="location"
                            HeaderText="Location" />
                        <asp:BoundField ItemStyle-Width="200px" HeaderStyle-Width="200px" ItemStyle-HorizontalAlign="center" DataField="TotUploadqty"
                            HeaderText="Total UploadQty" />
                        <asp:BoundField ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center" DataField="Total_pending"
                            HeaderText="Total pending Qty" />

                            <asp:BoundField ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center" DataField="TotalRecQty"
                            HeaderText="Tot Recieved Qty" />

                  

                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="200px" HeaderStyle-Width="200px"  ItemStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                                Sent Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label50" runat="server" Text='<%# Convert.ToDateTime(Eval("Sent_Received_Date")).ToString("d") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="200px" HeaderStyle-Width="200px"  ItemStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                                Commit Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label60" runat="server" Text='<%# Eval("Delivery_commit_date","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="200px" HeaderStyle-Width="200px"  ItemStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                                Expected Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label602" runat="server" Text='<%# Eval("Explain_Date","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:BoundField ItemStyle-Width="200px" HeaderStyle-Width="200px" ItemStyle-HorizontalAlign="center" DataField="sent_or_rec"
                            HeaderText="Status" />

                        

                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#999999" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
            </td>
           
        </tr>
    </table>
</asp:Content>
