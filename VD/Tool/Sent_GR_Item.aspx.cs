﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.IO;
using MySql.Data.MySqlClient;
using AjaxControlToolkit;
using System.Web.Services;
using System.Web.Script.Services;
using FileReader;
using System.Drawing;
using ClosedXML.Excel;
using excel = Microsoft.Office.Interop.Excel;
using System.Net;
public partial class Tool_Sent_GR_Item : System.Web.UI.Page
{
    String p_no = "";
    int tool_no, quantity, Qtysent;
    int countchk = 0;
    string approvername = "";
    object Approveid = "";
    string str_dt1 = "";
    string part_name, station, location, str_dt;
    bool chkval;
    static bool isRequestedForSend = false;


    /// <summary>
    /// Binds the Vendor List from Vendor Master Job Work table. Enables and disables the Controls
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        txtpodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
        if (!IsPostBack)
        {
            lblQCSheetNo.Text = String.Empty;
            Session["POList"] = null;
            Session["MIN"] = null;
            Session["vendor"] = null;
            txtPONO.Enabled = false;
            string query = "";
            if (Convert.ToString(Session["UserID"]).ToLower() == "hemant_mishra")
            {
                query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name<>'Assembly' order by Vendor_Name asc ";
            }
            else
            {
                query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name<>'Assembly' and Vendor_Name<>'Weld Parts Consumed' order by Vendor_Name asc ";
            }
            string dataTextField = "VENDOR_NAME";
            string dataValueField = "ID";

            string selectLocationQuery = "SELECT distinct   [location]  FROM [dbo].[part_history_QC_GR] ";
            string dataText = "location";
            string dataValue = "location";

            if (tbTool.Text.Length > 0)
            {
                isRequestedForSend = false;
                showgrid();
                Panel2.Visible = false;

                UtilityFunctions.bind_vendor(ddlvendor, query, dataTextField, dataValueField);
                GridView1.Columns[8].Visible = false;
                GridView1.Columns[9].Visible = false;
                ddlvendor.Enabled = false;
                ddlreceived.Visible = false;
                tdVendorForInspection.Visible = false;
                lblbillno.Visible = false;
                Label4.Visible = false;
                txtbillno.Visible = false;

                UtilityFunctions.bind_vendor(ddlreceived, selectLocationQuery, dataText, dataValue);
            }
            GridView1.Columns[8].Visible = true;
            GridView1.Columns[7].Visible = true;
            GridView1.Columns[9].Visible = false;
            ddlvendor.Enabled = false;
            txtbillno.Visible = false;

            UtilityFunctions.bind_vendor(ddlvendor, query, dataTextField, dataValueField);
            UtilityFunctions.bind_vendor(ddlreceived, selectLocationQuery, dataText, dataValue);

            Panel3.Visible = false;
            Panel2.Visible = false;
            ddlreceived.Visible = false;
            tdVendorForInspection.Visible = false;
            lblbillno.Visible = false;
            Label4.Visible = false;
            tbStation.Visible = false;
            tbTool.Visible = false;
            Button1.Visible = false;

            if (!string.IsNullOrEmpty(Request.QueryString["ChallanNo"]))
            {
                inputChallanNumber.Text = Convert.ToString(Request.QueryString["ChallanNo"]);
                RadioButton2.Checked = true;
                RadioButton2_CheckedChanged(sender, e);
                showgrid();
            }
        }
    }

    /// <summary>
    /// Binds the GridView and display the data as per Search Criteria to the User
    /// </summary>
    public void showgrid()
    {
        lblErrorMessage.Text = String.Empty;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();

        clear();

        DataTable dtChallanItems = new DataTable();
        if (inputChallanNumber.Text.Length > 0)
        {
            string challanCondition = " AND challan_no= '" + inputChallanNumber.Text.Trim() + "'";
            SqlDataAdapter ad = new SqlDataAdapter("select * from part_history_Gr where Challan_No=@ChallanNumber", conn);
            ad.SelectCommand.Parameters.AddWithValue("@ChallanNumber", inputChallanNumber.Text);
            ad.Fill(dtChallanItems);
        }

        string query = "";
        try
        {

            query = "select phg.p_no,phg.part_name,phg.location,phg.date_of_trans,phg.tool_no,phg.Sent_Date,phg.Received_Date,phg.Batch_No, phg.quantity as PendingQty,(isnull(phg.Upload_Qty,0)) as Upload_Qty,isnull(phg.QtySent,0) as QtySent,phg.quantity,phg.station,phg.date_of_trans,phg.rwk_no,phg.Uploaded_File_Name,pgr.Opr17, pgr.Parent_Positions,pgr.Matl,pgr.Hrc,pgr.Status,pgr.Type,pgr.Gr_Reference_No,pgr.req_qty,pgr.version_number from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where phg.location='COMPANY' and phg.quantity>0 and (isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0))>0  and (phg.Sent_Date is not null and phg.Received_Date is not null) and  pgr.Is_Active_Record=1  and ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)";
            if (tbTool.Text.Length > 0)
                query += " AND phg.tool_no= '" + tbTool.Text.Trim() + "'";
            if (txtbatchno.Text.Length > 0)
                query += " AND phg.batch_no= '" + txtbatchno.Text.Trim() + "'";
            if (txtreworkno.Text.Length > 0)
                query += " AND phg.rwk_no= '" + txtreworkno.Text.Trim() + "'";
            if (tbStation.Text.Length > 0)
                query += " AND phg.station= '" + tbStation.Text.Trim() + "'";
            if (ddlvendor.SelectedItem.Text == "GR VD FINISH STORE")
            {
                query += " and (pgr.Status='InActive' or pgr.Status='Old')";
            }
            query += " order by phg.tool_no,phg.station,phg.p_no asc";

            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = conn;

            DataTable dt = new DataTable();
            dt.Load(cmd.ExecuteReader());

            DataTable dtExistInCompanyItems = dt.Clone(); //copy structure
            for (int i = 0; i < dtChallanItems.Rows.Count; i++)
            {
                DataRow dr = dt.AsEnumerable().SingleOrDefault(x => x.Field<int>("Tool_No") == Convert.ToInt32(dtChallanItems.Rows[i]["Tool_No"]) && x.Field<string>("P_No") == Convert.ToString(dtChallanItems.Rows[i]["P_No"]) && x.Field<string>("Part_Name") == Convert.ToString(dtChallanItems.Rows[i]["Part_Name"]) && x.Field<string>("Rwk_No") == Convert.ToString(dtChallanItems.Rows[i]["Rwk_No"]) && x.Field<string>("Station") == Convert.ToString(dtChallanItems.Rows[i]["Station"]));
                if (dr != null)
                {
                    var newDataRow = dtExistInCompanyItems.NewRow();
                    newDataRow.ItemArray = dr.ItemArray;
                    dtExistInCompanyItems.Rows.Add(newDataRow);
                }
            }

            if (inputChallanNumber.Text.Length > 0)
            {
                if (dtExistInCompanyItems.Rows.Count != dtChallanItems.Rows.Count)
                {
                    lblErrorMessage.Text = "All Positions Sent for this Challan does not Exist in Company. Position Sent for this Challan " + dtChallanItems.Rows.Count + ", Positions at Company " + dtExistInCompanyItems.Rows.Count + "";
                }
                dt.Rows.Clear();
                dt = dtExistInCompanyItems.Copy();
            }
            if (dt.Rows.Count > 0)
            {
                btnSend.Visible = false;
                ddlvendor.Enabled = true;
                GridView1.DataSource = dt;
                Session["DataTableAdvanceReport"] = dt;
            }
            else
            {
                btnSend.Visible = false;
                dt = null;
                GridView1.DataSource = dt;
            }

            GridView1.DataBind();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    /// <summary>
    /// Event is fired when user clicks on the Check Box shown on the Header. Controls are enabled and disabled on the basis of Selection
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAll1_CheckedChanged1(object sender, EventArgs e)
    {
        txtPONO.Text = String.Empty;
        CheckBox ChkBoxHeader = (CheckBox)GridView1.HeaderRow.FindControl("chkAll1");
        txtchalanno.Text = String.Empty;
        if (ddlvendor.SelectedItem.Text == "" && ChkBoxHeader.Checked || radiobtnListChallanType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please select Vendor Name/Challan Type before selecting any Position.');", true);
            ChkBoxHeader.Checked = false;
            return;
        }
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            TextBox TextBox3 = (TextBox)row.FindControl("TextBox3");
            TextBox txtbalc = (TextBox)row.FindControl("txtbalc");
            Label Label8 = (Label)row.FindControl("Label8");
            Label Labelsent = (Label)row.FindControl("Labelsent");
            Label uploadqty = (Label)row.FindControl("LabelUpload_Qty");
            TextBox txtPOAmount = (TextBox)row.FindControl("txtPOAmount");

            if (ChkBoxHeader.Checked)
            {
                if (radiobtnListChallanType.SelectedValue == "Purchase Order" || radiobtnListChallanType.SelectedValue == "Job Work")
                {
                    txtPOAmount.Enabled = true;
                }
                else
                {
                    txtPOAmount.Enabled = false;
                }
                ChkBoxRows.Checked = true;
                countchk = countchk + 1;
            }
            else
            {
                txtPOAmount.Enabled = false;
                ChkBoxRows.Checked = false;
            }
            EnableTextBox(ChkBoxRows);
        }
        if (countchk > 0)
        {
            txtPONO.Enabled = true;
            GridView1.Columns[9].Visible = true;
            GridView1.Columns[10].Visible = true;
            btnSend.Visible = true;
        }
        else
        {
            txtPONO.Enabled = false;
            btnSend.Visible = false;
            GridView1.Columns[9].Visible = false;
            GridView1.Columns[10].Visible = false;
        }
        if (!Convert.ToBoolean(ViewState["IsActiveVendor"]))
        {
            btnSend.Visible = false;
        }
        updatePO.Update();
        UpdatePanel1.Update();
    }

    /// <summary>
    /// Event is fired when user selects the Check box shown in front of each row. Controls are enabled/disabled on the basis of Selection
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAllchild_CheckedChanged(object sender, EventArgs e)
    {
        txtPONO.Text = String.Empty;
        txtchalanno.Text = String.Empty;
        CheckBox ChkBoxRows = (CheckBox)sender;
        NumericUpDownExtender ne = (NumericUpDownExtender)ChkBoxRows.FindControl("numericupdownextender3");
        TextBox txtPOAmount = (TextBox)ChkBoxRows.FindControl("txtPOAmount");
        if (ChkBoxRows.Checked == true)
        {
            if (ddlvendor.SelectedItem.Text == "" || radiobtnListChallanType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please select Vendor Name/Challan Type before selecting any Position.');", true);
                ChkBoxRows.Checked = false;
                return;
            }

            if (radiobtnListChallanType.SelectedValue == "Purchase Order" || radiobtnListChallanType.SelectedValue == "Job Work")
            {
                txtPOAmount.Enabled = true;
            }
            else
            {
                txtPOAmount.Enabled = false;
            }

            GridView1.Columns[8].Visible = true;
            GridView1.Columns[9].Visible = true;
            btnSend.Visible = true;
        }
        else
        {
            txtPOAmount.Enabled = false;
            ChkBoxRows.Checked = false;
        }
        EnableTextBox(ChkBoxRows);
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox chkCheckBoxChecked = (CheckBox)GridView1.Rows[row.RowIndex].FindControl("chkAllchild");
            if (chkCheckBoxChecked.Checked)
            {
                countchk = countchk + 1;
            }
        }

        if (countchk > 0)
        {
            txtPONO.Enabled = true;
            LoadPONumbersForInspection();
            GridView1.Columns[9].Visible = true;
            GridView1.Columns[10].Visible = true;
            btnSend.Visible = true;
        }
        else
        {
            txtPONO.Enabled = false;
            btnSend.Visible = false;
            GridView1.Columns[9].Visible = false;
            GridView1.Columns[10].Visible = false;
        }
        if (!Convert.ToBoolean(ViewState["IsActiveVendor"]))
        {
            btnSend.Visible = false;
        }
        updatePO.Update();
        UpdatePanel1.Update();
    }

    private void clear()
    {
        txtrequired.Text = "";
        txtSRREGNO.Text = "";
        txtdelcdate.Text = "";//code changed by himaneesh. clearing delivery commitment date on showing grid
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        showgrid();
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        showgrid();
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlloc = (DropDownList)row.FindControl("ddlloc");
                string selectQuery = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name<>'Assembly' order by Vendor_Name asc ";
                string dataTextField = "VENDOR_NAME";
                string dataValueField = "ID";
                UtilityFunctions.bind_vendor(ddlloc, selectQuery, dataTextField, dataValueField);
            }
        }
    }

    /// <summary>
    /// Enter quantity is validated and Valid quantity is shown in the Text Box. Method is called when user selects/unselects check box at any row
    /// </summary>
    /// <param name="cb"></param>
    protected void EnableTextBox(CheckBox cb)
    {
        int count = int.Parse(GridView1.Rows.Count.ToString());
        NumericUpDownExtender ne = (NumericUpDownExtender)cb.FindControl("NumericUpDownExtender3");
        TextBox TextBox3 = (TextBox)cb.FindControl("TextBox3");
        TextBox txtbalc = (TextBox)cb.FindControl("txtbalc");
        Label Label8 = (Label)cb.FindControl("Label8");
        Label Labelsent = (Label)cb.FindControl("Labelsent");
        Label LabelUpload_Qty = (Label)cb.FindControl("LabelUpload_Qty");
        TextBox txtHSNCode = (TextBox)cb.FindControl("txtHSNCode");
        TextBox txtWeight = (TextBox)cb.FindControl("txtWeight");
        DropDownList ddlWeightUnit = (DropDownList)cb.FindControl("ddlWeightUnit");

        Label lblStation = cb.FindControl("Label10") as Label;
        Label lblrew = cb.FindControl("lblReworkNo") as Label;
        Label lblToolNo = cb.FindControl("lblproject") as Label;
        LinkButton lnkbtnPNo = cb.FindControl("Label1") as LinkButton;
        Label lblPartName = cb.FindControl("Label2") as Label;
        HiddenField hdnNotRequired = cb.FindControl("hdnNotRequired") as HiddenField;
        if (cb.Checked == true)
        {
            //BindPOList(lblToolNo.Text, lnkbtnPNo.Text, lblPartName.Text, lblStation.Text, lblrew.Text, ddlvendor.SelectedItem.Text);
            txtHSNCode.Enabled = true;
            txtWeight.Enabled = true;
            ddlWeightUnit.Enabled = true;
            txtbalc.Text = (Convert.ToInt32(Label8.Text) - Convert.ToInt32(TextBox3.Text)).ToString();
            txtbalc.Text = ((Convert.ToInt32(LabelUpload_Qty.Text) - (Convert.ToInt32(TextBox3.Text) + Convert.ToInt32(Labelsent.Text)))).ToString();
            TextBox3.Enabled = true;

            int notReqQty = (hdnNotRequired.Value == "") ? 0 : Convert.ToInt32(hdnNotRequired.Value);
            int possibleQtyForSend = 0;
            int balanceQty = 0;
            int? availableAndValidQty = ValidateQuantities(lblToolNo.Text, lblStation.Text, lblPartName.Text, lnkbtnPNo.Text, lblrew.Text, notReqQty, Convert.ToInt32(Label8.Text), Convert.ToInt32(TextBox3.Text), out possibleQtyForSend, out balanceQty);
            if (availableAndValidQty != null)
            {
                TextBox3.Text = Convert.ToString(possibleQtyForSend);
                txtbalc.Text = Convert.ToString(balanceQty);
            }
        }
        else
        {
            txtHSNCode.Enabled = false;
            txtWeight.Enabled = false;
            ddlWeightUnit.Enabled = false;
            TextBox3.Enabled = false;
            TextBox3.Text = "0";
        }
    }

    /// <summary>
    /// PO List is stored in Session["POList"] variable when user selects any position / changes Vendor for Inspection
    /// </summary>
    /// <param name="toolNo"></param>
    /// <param name="pNo"></param>
    /// <param name="partName"></param>
    /// <param name="station"></param>
    /// <param name="rwkNo"></param>
    private void BindPOList(string toolNo, string pNo, string partName, string station, string rwkNo)
    {
        SqlCommand cmd;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        try
        {
            cmd = new SqlCommand("SELECT distinct  PO_No FROM  part_history_GR where station=@Station and location=@Location and tool_no=@ToolNo and p_no=@PNo and part_name=@PartName and rwk_no=@RwkNo and Sent_Or_Rec='Sent'", conn);
            cmd.Parameters.AddWithValue("@ToolNo", toolNo);
            cmd.Parameters.AddWithValue("@Station", station);
            cmd.Parameters.AddWithValue("@PNo", pNo);
            cmd.Parameters.AddWithValue("@PartName", partName);
            cmd.Parameters.AddWithValue("@RwkNo", rwkNo);
            cmd.Parameters.AddWithValue("@Location", ddlreceived.SelectedItem.Text);
            DataTable dt = new DataTable();
            List<string> strItems = new List<string>();

            if (Session["POList"] != null)
            {
                strItems = (List<string>)Session["POList"];
            }
            dt.Load(cmd.ExecuteReader());
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (!strItems.Contains(Convert.ToString(dt.Rows[i]["PO_No"])))
                    {
                        strItems.Add(Convert.ToString(dt.Rows[i]["PO_No"]));
                    }
                }
                Session["POList"] = strItems.ToList();
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    /// <summary>
    /// Event is fired when user changes the Qty to Be Send Texbox and entered quantiy is validated
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TextBox3_TextChanged(object sender, EventArgs e)
    {
        TextBox txtQtyToBeSent = (TextBox)sender;
        TextBox TextBox3 = (TextBox)txtQtyToBeSent.FindControl("TextBox3");
        TextBox txtbalc = (TextBox)txtQtyToBeSent.FindControl("txtbalc");
        Label Label8 = (Label)txtQtyToBeSent.FindControl("Label8");
        Label Labelsent = (Label)txtQtyToBeSent.FindControl("Labelsent");
        Label uploadqty = (Label)txtQtyToBeSent.FindControl("LabelUpload_Qty");

        Label lblStation = txtQtyToBeSent.FindControl("Label10") as Label;
        Label lblrew = txtQtyToBeSent.FindControl("lblReworkNo") as Label;
        Label lblToolNo = txtQtyToBeSent.FindControl("lblproject") as Label;
        LinkButton lnkbtnPNo = txtQtyToBeSent.FindControl("Label1") as LinkButton;
        Label lblPartName = txtQtyToBeSent.FindControl("Label2") as Label;
        HiddenField hdnNotRequired = txtQtyToBeSent.FindControl("hdnNotRequired") as HiddenField;

        if (TextBox3.Enabled)
        {
            bool isSelected = (txtQtyToBeSent.FindControl("chkAllchild") as CheckBox).Checked;
            if (isSelected == true && TextBox3.Text != "")
            {
                if (Convert.ToInt32(TextBox3.Text) > 0)
                {
                    if (String.IsNullOrEmpty(Label8.Text))
                        Label8.Text = "0";
                    if (String.IsNullOrEmpty(TextBox3.Text))
                        TextBox3.Text = "0";
                    if (String.IsNullOrEmpty(Labelsent.Text))
                        Labelsent.Text = "0";
                    if (String.IsNullOrEmpty(uploadqty.Text))
                        uploadqty.Text = "0";

                    int notReqQty = (hdnNotRequired.Value == "") ? 0 : Convert.ToInt32(hdnNotRequired.Value);
                    int possibleQtyForSend = 0, balanceQty;
                    int? availableAndValidQty = ValidateQuantities(lblToolNo.Text, lblStation.Text, lblPartName.Text, lnkbtnPNo.Text, lblrew.Text, notReqQty,
                        Convert.ToInt32(Label8.Text), Convert.ToInt32(TextBox3.Text), out possibleQtyForSend, out balanceQty);
                    if (availableAndValidQty != null)
                    {
                        TextBox3.Text = Convert.ToString(possibleQtyForSend);
                        txtbalc.Text = Convert.ToString(balanceQty);
                    }
                }
                else
                {
                    txtbalc.Text = Convert.ToString(Label8.Text);
                }
            }
            else
            {
                txtbalc.Text = Convert.ToString(Label8.Text);
                TextBox3.Text = Convert.ToString("0");
            }
        }
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        DataTable table = (DataTable)Session["DataTableAdvanceReport"];
        GridView1.DataSource = table;
        GridView1.DataBind();
    }

    /// <summary>
    /// Checks if Column in Active or Inactive and displays Active / Inactive based on New Version Uploaded or not.
    /// Calculates and display the Pending Not Required Quantity, In case of Vendor Type Not Required/ GR VD Finish Store shows and hides the rows.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int totalNotRequiredQty = 0;
            string referenceGRNo = "", reworkNoOfGrAgainstRND = "";
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            NumericUpDownExtender ne = (NumericUpDownExtender)e.Row.FindControl("numericupdownextender3");
            Label lblPendingQtyAtPPC = (Label)e.Row.FindControl("Label8");
            TextBox txtBoxQtyToBeSend = (TextBox)e.Row.FindControl("TextBox3");
            Label lblStatus = (Label)e.Row.FindControl("lblStatus");
            CheckBox cb = (CheckBox)e.Row.FindControl("chkAllchild");
            Label lblSent_Date = (Label)e.Row.FindControl("lblSent_Date");
            HiddenField hdnNotRequired = (HiddenField)e.Row.FindControl("hdnNotRequired");
            Label lblPendingNotReq = (Label)e.Row.FindControl("lblPendingNotReq");
            ne.Maximum = Convert.ToDouble(lblPendingQtyAtPPC.Text);
            if (inputChallanNumber.Text.Length > 0)
            {
                string challanCondition = " challan_no= '" + inputChallanNumber.Text.Trim() + "'";
                using (SqlConnection connec = new SqlConnection(sqlconnstring))
                {
                    connec.Open();
                    SqlCommand cmd = new SqlCommand("select * from part_history_Gr where Challan_No=@ChallanNumber and tool_no=@ToolNo and p_no=@PNo and part_name=@PartName and station=@Station and rwk_no=@RwkNo", connec);
                    cmd.Parameters.AddWithValue("@ChallanNumber", inputChallanNumber.Text);
                    cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                    cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                    cmd.Parameters.AddWithValue("@PartName", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Part_Name")));
                    cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                    cmd.Parameters.AddWithValue("@RwkNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        rdr.Read();
                        int sentQtyChallan = Convert.ToInt32(rdr["Quantity"]);
                        int ppcBalanceQty = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "quantity"));
                        if (ppcBalanceQty >= sentQtyChallan)
                        {
                            lblPendingQtyAtPPC.Text = Convert.ToString(sentQtyChallan);
                            txtBoxQtyToBeSend.Text = Convert.ToString(sentQtyChallan);
                            ne.Maximum = Convert.ToDouble(sentQtyChallan);
                        }
                        else if (ppcBalanceQty < sentQtyChallan)
                        {
                            lblPendingQtyAtPPC.Text = Convert.ToString(ppcBalanceQty);
                            txtBoxQtyToBeSend.Text = Convert.ToString(ppcBalanceQty);
                            ne.Maximum = Convert.ToDouble(ppcBalanceQty);
                        }
                    }
                }
            }

            using (SqlConnection connec = new SqlConnection(sqlconnstring))
            {
                connec.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connec;
                cmd.Parameters.Clear();
                cmd.CommandText = "select top 1 date_of_trans from part_history_Gr where tool_no=@ToolNo and p_no=@PNo and part_name=@PartName and station=@Station and rwk_no=@RwkNo and Sent_Or_Rec='Sent' order by id Desc";
                cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                cmd.Parameters.AddWithValue("@PartName", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Part_Name")));
                cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                cmd.Parameters.AddWithValue("@RwkNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    rdr.Read();
                    lblSent_Date.Text = Convert.ToDateTime(rdr["Date_Of_Trans"]).ToString("dd/MM/yyyy HH:MM:ss");
                }
                //2 cases R&D and GR Against R&D to be considered. In other the pending not required qunatity will be shown as it is
                string type = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Type"));
                if (type == "New" || type == "Modification")
                {
                    //means gr against R&D
                    //referenceGRNo = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Gr_Reference_No"));
                }
                else if (type == "R&D")
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "select rwk_no from parts_Gr where Gr_Reference_No=@RwkNo and tool_no=@ToolNo and station=@Station and p_no=@PNo and Type in ('New','Modification')";
                    cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                    cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                    cmd.Parameters.AddWithValue("@PartName", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Part_Name")));
                    cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                    cmd.Parameters.AddWithValue("@RwkNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            reworkNoOfGrAgainstRND = Convert.ToString(rdr["rwk_no"]);
                        }
                    }


                    string lastVersion = "";
                    cmd.Parameters.Clear();
                    cmd.CommandText = "select top 1 Version_Number from parts_Gr where tool_no=@ToolNo and p_no=@PNo and station=@Station order by Version_Number Desc;";
                    cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                    cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                    cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            lastVersion = Convert.ToString(rdr["Version_Number"]);
                        }
                    }

                    cmd.Parameters.Clear();
                    cmd.CommandText = "select top 1 Version_Number from parts_Gr where Status='Active' and tool_no=@ToolNo and p_no=@PNo and station=@Station";
                    cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                    cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                    cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            string activeVersion = Convert.ToString(rdr["Version_Number"]);
                            if ((activeVersion == Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Version_Number"))) || (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Version_Number")) == lastVersion))
                            {
                                lblStatus.Text = "Active";
                            }
                        }
                    }
                }
                cmd.Parameters.Clear();
                //calculating total uploaded not required quantity for current row
                cmd.CommandText = "select Sum(req_qty) as NotRequiredQty from parts_Gr where tool_no=@ToolNo and p_no=@PNo and part_name=@PartName and station=@Station and Gr_Reference_No=@GrReferenceNo and Type='NotRequired'";
                cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                cmd.Parameters.AddWithValue("@PartName", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Part_Name")));
                cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                cmd.Parameters.AddWithValue("@GrReferenceNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.HasRows)
                    {
                        rdr.Read();
                        if (rdr["NotRequiredQty"] != DBNull.Value)
                            totalNotRequiredQty = Convert.ToInt32(rdr["NotRequiredQty"]);
                    }
                }

                int qtySentToNotRequiredLocation = 0;
                int reqQty = 0;
                //calculating quantity sum that are sent to not required location - For R&D
                if (type == "R&D")
                {
                    reqQty = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "req_qty"));

                    cmd.Parameters.Clear();
                    cmd.CommandText = "select isnull(isnull(quant_sent,0)- isnull(quant_rec,0),0) as QtyPending from location_info_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and part_name=@PartName and rwk=@RwkNo and location='Not Required'";
                    cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                    cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                    cmd.Parameters.AddWithValue("@PartName", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Part_Name")));
                    cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                    cmd.Parameters.AddWithValue("@RwkNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            if (rdr["QtyPending"] != DBNull.Value)
                                qtySentToNotRequiredLocation = Convert.ToInt32(rdr["QtyPending"]);
                        }
                    }

                    if (totalNotRequiredQty > 0)
                    {
                        int totalNotReq = Convert.ToInt32(totalNotRequiredQty);
                        int validPossibleNotRequired = 0;
                        if (totalNotReq >= reqQty)
                        {
                            validPossibleNotRequired = reqQty;
                        }
                        else
                        {
                            validPossibleNotRequired = totalNotReq;
                        }
                        hdnNotRequired.Value = Convert.ToString(validPossibleNotRequired);
                        int pendingNotReq = validPossibleNotRequired - qtySentToNotRequiredLocation;
                        if (pendingNotReq > 0)
                            lblPendingNotReq.Text = Convert.ToString(pendingNotReq);

                        int remainingActiveQty = 0;
                        int totalQty = 0;
                        int totalNotReqQty = 0;
                        string query = "select Sum(Req_Qty) as UploadedQty from parts_gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and Rwk_No=@RwkNo";
                        using (SqlDataAdapter adActiveQty = new SqlDataAdapter(query, connec))
                        {
                            adActiveQty.SelectCommand.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                            adActiveQty.SelectCommand.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                            adActiveQty.SelectCommand.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                            adActiveQty.SelectCommand.Parameters.AddWithValue("@RwkNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                            DataTable dtActiveQty = new DataTable();
                            adActiveQty.Fill(dtActiveQty);
                            if (dtActiveQty.Rows.Count > 0)
                            {
                                if (dtActiveQty.Rows[0]["UploadedQty"] != DBNull.Value)
                                    totalQty = Convert.ToInt32(dtActiveQty.Rows[0]["UploadedQty"]);

                            }

                            using (SqlDataAdapter adNotRequired = new SqlDataAdapter("select Sum(Req_Qty) as NotReqQty from parts_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and Gr_Reference_No=@GrReferenceNo and Type=@Type", connec))
                            {
                                adNotRequired.SelectCommand.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                                adNotRequired.SelectCommand.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                                adNotRequired.SelectCommand.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                                adNotRequired.SelectCommand.Parameters.AddWithValue("@GrReferenceNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                                adNotRequired.SelectCommand.Parameters.AddWithValue("@Type", "NotRequired");
                                DataTable dtNotReq = new DataTable();
                                adNotRequired.Fill(dtNotReq);
                                if (dtNotReq.Rows.Count > 0)
                                {
                                    if (dtNotReq.Rows[0]["NotReqQty"] != DBNull.Value)
                                        totalNotReqQty = Convert.ToInt32(dtNotReq.Rows[0]["NotReqQty"]);

                                }
                            }
                            remainingActiveQty = totalQty - totalNotReqQty;

                            if (remainingActiveQty == 0)
                            {
                                lblStatus.Text = "Inactive";
                            }
                        }
                    }
                }
                else
                {
                    //Calculating qty sent to Not Required Location for current row
                    cmd.Parameters.Clear();
                    cmd.CommandText = "select isnull(isnull(quant_sent,0)- isnull(quant_rec,0),0) as QtyPending from location_info_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and part_name=@PartName and rwk=@RwkNo and location='Not Required'";
                    cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                    cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                    cmd.Parameters.AddWithValue("@PartName", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Part_Name")));
                    cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                    cmd.Parameters.AddWithValue("@RwkNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            if (rdr["QtyPending"] != DBNull.Value)
                                qtySentToNotRequiredLocation = Convert.ToInt32(rdr["QtyPending"]);
                        }
                    }

                    if (totalNotRequiredQty > 0)
                    {
                        hdnNotRequired.Value = Convert.ToString(totalNotRequiredQty);
                        int pendingNotReq = Convert.ToInt32(hdnNotRequired.Value) - qtySentToNotRequiredLocation;
                        if (pendingNotReq > 0)
                            lblPendingNotReq.Text = Convert.ToString(pendingNotReq);
                    }

                    //Change your logic here for R&D and GR against that R&D
                }
            }

            if (ddlvendor.SelectedItem.Text == "Not Required")
            {
                if (lblPendingNotReq.Text != "")
                {
                    if (Convert.ToInt32(lblPendingNotReq.Text) > 0)
                    {
                        e.Row.Visible = true;
                    }
                    else
                    {
                        e.Row.Visible = false;
                    }
                }
                else
                {
                    e.Row.Visible = false;
                }
            }
            else if (ddlvendor.SelectedItem.Text == "GR VD FINISH STORE")
            {
                //string status = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Status"));
                if (lblStatus.Text.ToLower() == "inactive")
                {
                    if (lblPendingNotReq.Text != "")
                    {
                        if (Convert.ToInt32(lblPendingNotReq.Text) > 0)
                        {
                            e.Row.Visible = false;
                        }
                        else
                        {
                            e.Row.Visible = true;
                        }
                    }
                    else
                    {
                        e.Row.Visible = true;
                    }
                }
                else if (lblStatus.Text.ToLower() == "active")
                {
                    e.Row.Visible = false;
                }
                else if (lblStatus.Text.ToLower() == "old")
                {
                    e.Row.Visible = true;
                }
            }
            HyperLink hyperLnkExcelFile = (HyperLink)e.Row.FindControl("hyperLnkExcelFile");
            if (DataBinder.Eval(e.Row.DataItem, "Uploaded_File_Name") != DBNull.Value)
            {
                hyperLnkExcelFile.NavigateUrl = "~/UploadedExcelFiles/" + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Uploaded_File_Name"));
                hyperLnkExcelFile.Text = "Download Excel";
            }

            if (!cb.Checked)
            {
                ne.Enabled = false;
            }
        }
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        if (RadioButton1.Checked == true)
        {
            Panel2.Visible = true;
            Panel3.Visible = false;
            RadioButton1.Checked = true;
            RadioButton2.Checked = false;
            tbStation.Visible = false;
            tbTool.Visible = false;
            Button1.Visible = false;
            txtbarcode.Focus();

            string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name<>'Assembly' order by Vendor_Name asc ";
            string dataTextField = "VENDOR_NAME";
            string dataValueField = "ID";
            UtilityFunctions.bind_vendor(ddllocauto, query, dataTextField, dataValueField);
        }
        else
        {
            Panel2.Visible = false;
        }
    }

    protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
    {
        if (RadioButton2.Checked == true)
        {
            Panel2.Visible = false;
            RadioButton1.Checked = false;
            RadioButton2.Checked = true;
            tbStation.Visible = true;
            tbTool.Visible = true;
            Button1.Visible = true;
            Panel3.Visible = true;
        }
        else
        {
            Panel2.Visible = true;
        }
    }

    protected void txtbarcode_TextChanged(object sender, EventArgs e)
    {
        DateTime lastKeyPress = DateTime.Now;
        if (txtbarcode.Text.Length > 0)
        {
            if (((TimeSpan)(DateTime.Now - lastKeyPress)).Seconds <= 0.5)
            {
                string str = null;
                string[] strArr = null;
                str = txtbarcode.Text;

                char[] splitchar = { ' ' };
                strArr = str.Split(splitchar);
                txttoolno.Text = strArr[0];
                txtstationno.Text = strArr[1];
                txtposition.Text = strArr[2];

                txtbarcodescaned.Text = txtbarcode.Text;

                fill_qty();
                ViewState["toolno"] = txttoolno.Text;
                ViewState["stationno"] = txtstationno.Text;
                ViewState["position"] = txtposition.Text;
                ViewState["Qty"] = txtQty.Text;
                ViewState["SENT"] = txtsentQty1.Text;
                ViewState["vendor"] = ddllocauto.SelectedItem.Text;
                ViewState["ItemName"] = txtname1.Text;
                ViewState["TotalQty"] = totalQty.Text;
                ViewState["Rework"] = txtreworkno.Text;
                txtbarcode.Text = "";
                txtbarcode.Focus();
            }
            else
            {
                txtbarcode.Enabled = true;
            }
            lastKeyPress = DateTime.Now;
        }
    }

    public void fill_qty()
    {
        SqlCommand cmd;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);

        conn.Open();
        try
        {
            if (txtstationno.Text.Length > 0 && txtposition.Text.Length > 0 && txttoolno.Text.Length > 0 && txtreworkno.Text.Length > 0)
            {
                cmd = new SqlCommand("SELECT distinct  isnull(quantity,0) as quantity ,isnull(QtySent,0) as QtySent ,isnull(part_name,'') as part_name FROM  part_history_GR where station=@station_no and location='COMPANY' and tool_no=tool_no and p_no=@positio_no and rwk_no=@rwkno and (isnull(quantity,0)>=isnull(QtySent,0)) ");
                cmd.Parameters.AddWithValue("@tool_no", txttoolno.Text);
                cmd.Parameters.AddWithValue("@station_no", txtstationno.Text);
                cmd.Parameters.AddWithValue("@positio_no", txtposition.Text);
                cmd.Parameters.AddWithValue("@positio_no", txtposition.Text);
                cmd.Parameters.AddWithValue("@rwkno", txtreworkno.Text);
                cmd.Connection = conn;

                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                if (dt.Rows.Count > 0)
                {
                    txtQty.Text = dt.Rows[0][0].ToString();
                    txtsentQty1.Text = dt.Rows[0][1].ToString();
                    txtname1.Text = dt.Rows[0][2].ToString();
                    totalQty.Text = dt.Rows[0][0].ToString();
                }
                else
                {
                    txtQty.Text = "0";
                    txtsentQty1.Text = "0";
                    txtname1.Text = "";
                    totalQty.Text = "0";
                    MessageBox("Record Not Found");
                    return;
                }
            }
            else
            {
                MessageBox("Tool no or Station No or Position No should not be empty");
                return;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    protected void txtQty_TextChanged(object sender, EventArgs e)
    {
        if (txtQty.Text.Length > 0)
        {
            ViewState["Qty"] = txtQty.Text;
            txtbarcode.Focus();
        }
    }


    /// <summary>
    /// Validates the Entered Fields and then Sends the position to Selected Vendor. Rows are inserted into Part History Gr table, Location Info Gr table and quantity is updated 
    /// for the Row where location is Company in History Gr table. Data is also insterted in Challan Sent Received Detail table.
    /// Challan is generated for each Send, which can be printed by the user if required.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSend_Click(object sender, EventArgs e)
        {
        if (txtrequired.Text != "")
        {
            if (Convert.ToDateTime(txtrequired.Text) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Required Date Selected.');", true);
                return;
            }
            if(txtdelcdate.Text!="")//delivery commitment date if filled should not be greater than required date
            {
                if (Convert.ToDateTime(txtdelcdate.Text) > Convert.ToDateTime(txtrequired.Text))//if delivery commitment date is greater than required date
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Deivery commitment date should not be greater than required date.');", true);
                    return;
                }
            }
        }
        if(txtdelcdate.Text!="")//code changed by himaneesh start
        {
            if(Convert.ToDateTime(txtdelcdate.Text) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Commitment date Selected.');", true);//commitmnet date should be valid also
                return;//returning from this function if invalid commitment date
            }
            if(txtrequired.Text!="")//delivery commitment date should not be greater than required date. 
            {
                if(Convert.ToDateTime(txtdelcdate.Text) > Convert.ToDateTime(txtrequired.Text))//if delivery commitment date is greater than required date
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Deivery commitment date should not be greater than required date.');", true);
                    return;
                }
            }
        }//code changed by himaneesh end
        if ((radiobtnListChallanType.SelectedValue == "Purchase Order" || radiobtnListChallanType.SelectedValue == "Job Work") && ((hdnVendorType.Value == "External Vendor") || ddlvendor.SelectedItem.Text.Trim() == "Inspection") && txtPONO.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('PO Number is Required for Challan Type (Purchase Order / Job Work) and Vendor Type (External Vendor) excluding Inspection.');", true);
            return;
        }
        if (txtPONO.Text != "")
        {
            if (hdnVendorType.Value == "Internal Vendor" && ddlvendor.SelectedItem.Text.Trim() != "Inspection")
            {
                if (txtPONO.Text != "Internal" && txtPONO.Text != "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid PO Number specified. For Vendor Type (Internal Vendor) excluding Inspection, PO Number has to be Blank or it has to be Specified as Internal');", true);
                    return;
                }
            }
            else if (txtPONO.Text != "")
            {
                //int num = 0;
                //bool isValid = int.TryParse(txtPONO.Text, out num);
                //if (isValid && num > 200)
                //{
                string vendorName = "";
                string sqlconn = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
                using (SqlConnection connec = new SqlConnection(sqlconn))
                {
                    connec.Open();
                    using (SqlCommand cmd = new SqlCommand("Select top 1 Location from part_history_Gr where PO_No=@PONo order by id asc", connec))
                    {
                        cmd.Parameters.AddWithValue("@PONo", txtPONO.Text);
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                rdr.Read();
                                vendorName = Convert.ToString(rdr["Location"]);
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(vendorName) && ddlvendor.SelectedItem.Text.Trim() != "Inspection")
                {
                    if (ddlvendor.SelectedItem.Text != vendorName)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Same PO Number is already used for another Vendor.');", true);
                        return;
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Enter PO Number.');", true);
                //ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Minimum Value for PO No is 200. Please enter PO No greater than 200.');", true);
                return;
            }
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Minimum Value for PO No is 200. Please enter PO No greater than 200.');", true);
            //    return;
            //}
        }

        int count = 0;
        if (ddlvendor.SelectedItem.Text.Trim() == "Inspection")
        {
            if (txtbillno.Text.Length == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('please enter Bill No');", true);
                btnSend.Enabled = true;
                return;
            }
            printispectionchallan.Visible = true;
            btnprint.Visible = false;
            tdPrintChallan.Visible = false;
        }
        else
        {
            btnprint.Visible = true;
            tdPrintChallan.Visible = true;
        }

        btnSend.Attributes.Add("onclick", " this.disabled = true; " + ClientScript.GetPostBackEventReference(btnSend, null) + ";");

        int tool_no1, quantity1, Qtysent1, sent1;

        string positionsNotParent = "";
        string positionNotChild = "";
        string weightUnitRequired = "";
        foreach (GridViewRow row in GridView1.Rows)
        {
            LinkButton lblPNo = row.FindControl("Label1") as LinkButton;
            Label lblPartName = row.FindControl("Label2") as Label;
            Label lblproject = row.FindControl("lblproject") as Label;
            Label lblStation = row.FindControl("Label10") as Label;
            TextBox tobeQtysent = row.FindControl("TextBox3") as TextBox;
            Label lblReworkNo = row.FindControl("lblReworkNo") as Label;
            TextBox txtPOAmount = row.FindControl("txtPOAmount") as TextBox;
            tool_no1 = Convert.ToInt32(lblproject.Text);
            Qtysent1 = Convert.ToInt32(tobeQtysent.Text);

            if (row.RowType == DataControlRowType.DataRow)
            {
                bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                if (isSelected == true)
                {
                    if ((radiobtnListChallanType.SelectedValue == "Purchase Order" || radiobtnListChallanType.SelectedValue == "Job Work") && (hdnVendorType.Value == "External Vendor" || (hdnVendorType.Value == "Internal Vendor" && ddlvendor.SelectedItem.Text.Trim() == "Inspection")) && txtPOAmount.Text == "")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('PO Amount is Required for Challan Type Purchase Order/Job Work');", true);
                        return;
                    }
                    if (ddlvendor.SelectedItem.Text.Trim() == "Inspection")
                    {
                        chkval = chkvalidate(lblPNo.Text, lblPartName.Text, lblStation.Text, tool_no1, Qtysent1, lblReworkNo.Text);
                        if (chkval == true)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('For inspection send Qty Should be equal to Recieve Qty');", true);
                            btnSend.Enabled = true;
                            return;
                        }
                    }
                    else if (ddlvendor.SelectedItem.Text == "Weld Assembly Waiting")
                    {
                        int? resultRows = ValidatePositionIsParent(lblproject.Text, lblPNo.Text, lblPartName.Text, lblStation.Text, lblReworkNo.Text);
                        if (resultRows == 0)
                        {
                            int rowIndex = row.RowIndex + 1;
                            if (!string.IsNullOrEmpty(positionsNotParent))
                            {
                                positionsNotParent = positionsNotParent + "," + rowIndex;
                            }
                            else
                            {
                                positionsNotParent = rowIndex.ToString();
                            }
                        }
                        if (resultRows == null)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured while Validating Selected Positions are Parent or Not.');", true);
                            return;
                        }
                    }
                    else if (ddlvendor.SelectedItem.Text == "Weld Parts Waiting" || ddlvendor.SelectedItem.Text == "Weld Parts Consumed")
                    {

                        bool? isChildPosition = ValidatePositionIsChild(lblproject.Text, lblPNo.Text, lblPartName.Text, lblStation.Text, lblReworkNo.Text);
                        if (!isChildPosition.Value)
                        {
                            int rowIndex = row.RowIndex + 1;
                            if (!string.IsNullOrEmpty(positionNotChild))
                            {
                                positionNotChild = positionNotChild + "," + rowIndex;
                            }
                            else
                            {
                                positionNotChild = rowIndex.ToString();
                            }
                        }
                        if (isChildPosition == null)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured while Validating Selected Positions is Child.');", true);
                            return;
                        }
                    }

                    TextBox txtWeight = row.FindControl("txtWeight") as TextBox;
                    DropDownList ddlWeightUnit = row.FindControl("ddlWeightUnit") as DropDownList;
                    if (txtWeight.Text != "" && ddlWeightUnit.SelectedValue == "")
                    {
                        int rowIndex = row.RowIndex + 1;
                        if (!string.IsNullOrEmpty(weightUnitRequired))
                        {
                            weightUnitRequired = weightUnitRequired + "," + rowIndex;
                        }
                        else
                        {
                            weightUnitRequired = rowIndex.ToString();
                        }

                    }
                    if (txtPONO.Text != "Internal" && txtPONO.Text != "" && txtPONO.Text != "NA")
                    {
                        bool isPONOValidForUse = false;
                        string vendorName = "";
                        string sqlconn = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
                        using (SqlConnection connec = new SqlConnection(sqlconn))
                        {
                            DataTable dt = new DataTable();
                            connec.Open();
                            using (SqlDataAdapter ad = new SqlDataAdapter("Select * from part_history_Gr where PO_No=@PONo", connec))
                            {
                                ad.SelectCommand.Parameters.AddWithValue("@PONo", txtPONO.Text);
                                ad.Fill(dt);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                if (ddlvendor.SelectedItem.Text.Trim() == "Inspection")
                                {
                                    string pNo = lblPNo.Text;
                                    string partName = lblPartName.Text;
                                    string station = lblStation.Text;
                                    string rwkNo = lblReworkNo.Text;
                                    string toolNo = lblproject.Text;
                                    DataRow[] drRow = dt.Select("Location='" + ddlreceived.SelectedItem.Text.Trim() + "' and Tool_No='" + toolNo + "' and P_No='" + pNo + "' and Part_Name='" + partName + "' and Station='" + station + "' and Rwk_No='" + rwkNo + "'");
                                    if (drRow != null)
                                    {
                                        if (drRow.Length > 0)
                                        {
                                            isPONOValidForUse = true;
                                        }
                                        else
                                        {
                                            isPONOValidForUse = false;
                                        }
                                    }
                                    else
                                    {
                                        isPONOValidForUse = false;
                                    }
                                }
                                else
                                {
                                    isPONOValidForUse = false;
                                }

                                if (!isPONOValidForUse)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Same PO Number is already used for another Vendor/ Same Vendor with different Tool No,Position No,Part Name,Station, Rework No.');", true);
                                    return;
                                }
                            }
                            else
                            {
                                if (ddlvendor.SelectedItem.Text.Trim() == "Inspection")
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid PO Number Entered. For Inspection User should select the PO Number which is Entered while Sending the Items to Vendor (Vendor For Inspection) for same Tool No,Position No,Part Name,Station, Rework No.');", true);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
        positionsNotParent = positionsNotParent.TrimEnd(',');
        if (!string.IsNullOrEmpty(positionsNotParent))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Position can not be sent to Weld Assembly Waiting as its not Parent Position. Please check row number " + positionsNotParent + ".');", true);
            return;
        }

        positionNotChild = positionNotChild.TrimEnd(',');
        if (!string.IsNullOrEmpty(positionNotChild))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Position can not be sent to Weld Parts Waiting/Weld Parts Consumed as its not Child Position. Please check row number " + positionNotChild + ".');", true);
            return;
        }

        weightUnitRequired = weightUnitRequired.TrimEnd(',');
        if (!string.IsNullOrEmpty(weightUnitRequired))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Unit of Weight is not selected in some Rows. Please check row number " + weightUnitRequired + ".');", true);
            return;
        }

        MySqlConnection mysqlconn = new MySqlConnection(ConfigurationManager.ConnectionStrings["FCMConnectionString"].ConnectionString);
        MySqlTransaction mysqltrans = null;
        MySqlCommand mysqlcmd = new MySqlCommand();

        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        SqlTransaction trans = null;

        ViewState["GeneratedGRChallanNumber"] = "";
        string prefixStr = "";
        if (!isRequestedForSend)
        {
            isRequestedForSend = true;
            try
            {
                mysqlconn.Open();
                conn.Open();
                trans = conn.BeginTransaction();
                mysqltrans = mysqlconn.BeginTransaction();
                mysqlcmd.Connection = mysqlconn;
                int serialNo = 0;

                if (checkIsVirtualTransaction.Checked)
                {
                    prefixStr = "V_";
                }
                else
                {
                    prefixStr = "CH";
                }
                string generatedChallanNumber = "";
                generatedChallanNumber = UtilityFunctions.gencode(prefixStr);
                txtchalanno.Text = generatedChallanNumber;
                if (!string.IsNullOrEmpty(generatedChallanNumber))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Transaction = trans;
                        mysqlcmd.Transaction = mysqltrans;
                        foreach (GridViewRow row in GridView1.Rows)
                        {
                            if (row.RowType == DataControlRowType.DataRow)
                            {
                                LinkButton p_no = row.FindControl("Label1") as LinkButton;
                                Label part_name = row.FindControl("Label2") as Label;
                                Label lblproject = row.FindControl("lblproject") as Label;
                                Label station = row.FindControl("Label10") as Label;
                                Label Labelrw = row.FindControl("lblReworkNo") as Label;
                                HiddenField hdnBatchNo = row.FindControl("hdnBatchNo") as HiddenField;
                                Label quantityPending = row.FindControl("Label8") as Label;
                                str_dt1 = string.Format("{0:yyyy-MM-dd}", txtpodate.Text); // you can specify format 
                                CultureInfo provider = CultureInfo.InvariantCulture;
                                string kk = System.DateTime.Now.ToLongTimeString();
                                str_dt1 = str_dt1 + " " + kk;


                                string reworkNo = Labelrw.Text;

                                Label quantity = row.FindControl("LabelUpload_Qty") as Label;
                                TextBox tobeQtysent = row.FindControl("TextBox3") as TextBox;
                                Label sent = row.FindControl("Labelsent") as Label;
                                TextBox txtHSNCode = row.FindControl("txtHSNCode") as TextBox;
                                TextBox txtWeight = row.FindControl("txtWeight") as TextBox;
                                DropDownList ddlWeightUnit = row.FindControl("ddlWeightUnit") as DropDownList;
                                TextBox txtPOAmount = row.FindControl("txtPOAmount") as TextBox;
                                quantity1 = Convert.ToInt32(quantity.Text);
                                Qtysent1 = Convert.ToInt32(tobeQtysent.Text);
                                sent1 = Convert.ToInt32(sent.Text);
                                tool_no1 = Convert.ToInt32(lblproject.Text);
                                string location1 = ddlvendor.SelectedItem.Text;
                                bool isValidData = false;
                                bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;

                                if (isSelected)
                                {
                                    SqlCommand cmdCheckQuantity = new SqlCommand("select (isnull(((Upload_Qty)-(isnull(QtySent,0))),0)) as PendingQty "
                                    + " from part_history_Gr where location='Company'  and Sent_Date is not null and Received_Date is not null and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and tool_no='" + lblproject.Text + "' "
                                    + " and station='" + station.Text + "' and rwk_no='" + Labelrw.Text + "'", conn, trans);
                                    using (SqlDataReader reader = cmdCheckQuantity.ExecuteReader())
                                    {
                                        if (reader.HasRows)
                                        {
                                            reader.Read();
                                            int enteredQty = Convert.ToInt32(tobeQtysent.Text);
                                            int pendingQty = Convert.ToInt32(reader["PendingQty"]);

                                            if (enteredQty <= pendingQty)
                                            {
                                                isValidData = true;
                                            }
                                        }
                                    }
                                }
                                if (isSelected)
                                {
                                    if (isValidData)
                                    {
                                        if (location1 == "VD Finish Store" || location1 == "Assembly")
                                        {
                                            if (Convert.ToInt32(tobeQtysent.Text) <= Convert.ToInt32(quantityPending.Text) && Convert.ToInt32(tobeQtysent.Text) > 0)
                                            {
                                                int quant_rec = 0;
                                                int req_qty = 0;
                                                string cmd1 = "insert";
                                                Int32 qtysent1 = Convert.ToInt32(Qtysent1);

                                                string strQuery2 = "update part_history_GR  set  QtySent=" + qtysent1 + ", Sent_Date='" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "' , quantity=(isnull(quantity,0) - " + qtysent1 + ") where  p_no='" + p_no.Text + "' and location='COMPANY' and part_name='" + part_name.Text.Trim() + "' and station='" + station.Text + "' and tool_no=" + tool_no1 + " and rwk_no='" + Labelrw.Text + "' ";
                                                SqlCommand cmd2 = new SqlCommand(strQuery2, conn, trans);
                                                cmd2.ExecuteNonQuery();

                                                string poAmount = (txtPOAmount.Text == "") ? "0" : txtPOAmount.Text;
                                                string s = "insert into part_history_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,Challan_No,Rework,Fresh,TransSentQty,Upload_Qty,Sent_Date,rwk_no,Is_Virtual_Transaction,User_name,Challan_Type,HSN_Code,Weight_Value,Weight_Unit,PO_Amount) values ('" + p_no.Text + "','" + part_name.Text + "', '" + tool_no1 + "','" + station.Text + "','" + location1 + "','" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','Sent'," + qtysent1 + "," + Qtysent1 + ",'" + txtchalanno.Text + "','','','" + Qtysent1 + "'," + quantity1 + ",'" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','" + Labelrw.Text + "','" + checkIsVirtualTransaction.Checked + "','" + Convert.ToString(Session["UserId"]) + "','" + radiobtnListChallanType.SelectedValue + "','" + txtHSNCode.Text + "','" + txtWeight.Text + "','" + ddlWeightUnit.SelectedValue + "'," + poAmount + ")";
                                                cmd.CommandText = s;
                                                cmd.Connection = conn;
                                                cmd.ExecuteNonQuery();

                                                s = "select quant_sent,quant_rec,req_qty from location_info_GR where location='" + location1 + "' and tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "'  and Rwk='" + Labelrw.Text + "'";
                                                cmd.CommandText = s;

                                                DataTable dt = new DataTable();
                                                dt.Load(cmd.ExecuteReader());

                                                if (dt.Rows.Count != 0)
                                                {
                                                    cmd1 = "update";
                                                    quantity1 += Convert.ToInt32(dt.Rows[0][0].ToString());
                                                    quant_rec = Convert.ToInt32(dt.Rows[0][1].ToString());
                                                    req_qty = Convert.ToInt32(dt.Rows[0][2].ToString());
                                                }


                                                if (cmd1.Equals("insert"))
                                                {
                                                    s = "insert into location_info_GR(tool_no,station,p_no,part_name,quant_sent,quant_pend,Sent_Received_Date,Challan_No,Rework,Fresh,TransSentQty,location,req_qty,Upload_Qty,Rwk,Extra_Received_Quantity) values(" + tool_no1 + ",'" + station.Text + "','" + p_no.Text + "','" + part_name.Text + "'," + Qtysent1 + "," + Qtysent1 + ",'" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','" + txtchalanno.Text + "','',''," + qtysent1 + ",'" + location1 + "','" + req_qty + "'," + quantity1 + ",'" + Labelrw.Text + "'," + 0 + ")";
                                                }
                                                else
                                                {
                                                    s = "update location_info_GR set  TransSentQty=" + qtysent1 + ", quant_sent=" + qtysent1 + " + isnull(quant_sent,0), quant_pend=isnull(quant_pend,0) +" + qtysent1 + "  where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and location='" + location1 + "' and Rwk='" + Labelrw.Text + "'";
                                                }
                                                cmd.CommandText = s;
                                                cmd.ExecuteNonQuery();

                                                //Insert into Challan Sent Received Detail table
                                                cmd.Parameters.Clear();
                                                cmd.CommandText = "insert into Challan_Sent_Received_Detail (Challan_No,Tool_No,Part_Name,P_No,Station,Rwk_No,Location,Sent_Or_Rec,Quantity,Outstanding_Qty,Created_Date,Created_By,Send_Receive_Date) Values(@ChallanNo,@ToolNo,@PartName,@PNo,@Station,@RwkNo,@Location,@SentOrRec,@Quantity,@OutstandingQty,@CreatedDate,@CreatedBy,@SendReceiveDate)";
                                                cmd.Parameters.AddWithValue("@ChallanNo", txtchalanno.Text);
                                                cmd.Parameters.AddWithValue("@ToolNo", tool_no1);
                                                cmd.Parameters.AddWithValue("@PartName", part_name.Text);
                                                cmd.Parameters.AddWithValue("@PNo", p_no.Text);
                                                cmd.Parameters.AddWithValue("@Station", station.Text);
                                                cmd.Parameters.AddWithValue("@RwkNo", Labelrw.Text);
                                                cmd.Parameters.AddWithValue("@Location", location1);
                                                cmd.Parameters.AddWithValue("@SentOrRec", "Sent");
                                                cmd.Parameters.AddWithValue("@Quantity", qtysent1);
                                                cmd.Parameters.AddWithValue("@OutstandingQty", qtysent1);
                                                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                                                cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
                                                cmd.Parameters.AddWithValue("@SendReceiveDate", Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss"));
                                                cmd.ExecuteNonQuery();

                                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Sent Data Successfully');", true);

                                            }
                                        }
                                        else
                                        {
                                            if (txtesthrs.Text.Length > 0)
                                            {
                                                if ((location1.Length > 0) && (isSelected == true) && (txtpodate.Text.Length > 0))
                                                {
                                                    if (Convert.ToInt32(tobeQtysent.Text) <= Convert.ToInt32(quantityPending.Text) && Convert.ToInt32(tobeQtysent.Text) > 0)
                                                    {
                                                        int req_qty = 0;
                                                        string s = "";
                                                        int quant_rec = 0;
                                                        string command = "insert";
                                                        int first_location_sent = 0;
                                                        int quant_others = 0;

                                                        DataTable dt = new DataTable();

                                                        if (Qtysent1 <= quantity1)
                                                        {
                                                        }
                                                        else
                                                        {
                                                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Send Qty Should be less than Total Qty');", true);
                                                            return;
                                                        }

                                                        Int32 qtysent1 = Convert.ToInt32(Qtysent1);
                                                        string strQuery4 = "update part_history_GR  set quantity=(quantity - " + qtysent1 + ") , Sent_Date='" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "' where location='COMPANY'  and tool_no=" + tool_no1 + " and  p_no='" + p_no.Text + "' and station='" + station.Text + "' and part_name='" + part_name.Text.Trim() + "' and  rwk_no='" + Labelrw.Text + "' ";
                                                        SqlCommand cmd4 = new SqlCommand(strQuery4, conn, trans);
                                                        Int32 k1 = cmd4.ExecuteNonQuery();

                                                        string strQuery2 = "update part_history_GR  set  QtySent=" + qtysent1 + " + isnull(QtySent,0) where tool_no=" + tool_no1 + " and p_no='" + p_no.Text + "' and station='" + station.Text + "' and location='COMPANY' and  part_name='" + part_name.Text.Trim() + "' and rwk_no='" + Labelrw.Text + "'";
                                                        SqlCommand cmd2 = new SqlCommand(strQuery2, conn, trans);
                                                        cmd2.ExecuteNonQuery();

                                                        cmd.CommandText = s;
                                                        cmd.Connection = conn;

                                                        //TO RETRIEVE REQUIRED QUANTITY OF THE PARTICULAR TOOL FROM PARTS TABLE
                                                        s = "select req_qty from parts_GR where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "'  and rwk_no='" + Labelrw.Text + "'";
                                                        cmd.CommandText = s;
                                                        dt.Load(cmd.ExecuteReader());
                                                        if (dt.Rows.Count == 0)
                                                        {
                                                            lbl.Text = "No such part exists..!!";
                                                            return;
                                                        }
                                                        else
                                                        {
                                                            req_qty = Convert.ToInt32(dt.Rows[0][0].ToString());
                                                        }
                                                        //TO UPDATE part_history_GR TABLE
                                                        //code changed by himaneesh in the part_history_gr adding commitment date when insertng in part_history_Gr
                                                        string poAmount = (txtPOAmount.Text == "") ? "0" : txtPOAmount.Text;
                                                        s = "insert into part_history_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,Explain_Date,Delivery_commit_date,Challan_No,Sr_Reg_No,Batch_No,Required_date,PO_NO,EST_Hrs,Rework,Fresh,TransSentQty,Upload_Qty,Sent_Date,rwk_no,Is_Virtual_Transaction,User_name,Challan_Type,HSN_Code,Weight_Value,Weight_Unit,PO_Amount) values ('" + p_no.Text + "','" + part_name.Text + "', '" + tool_no1 + "','" + station.Text + "','" + location1 + "','" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','Sent'," + qtysent1 + "," + Qtysent1 + ",'" + DateTime.Now.AddDays(3).ToString("yyyy/MM/dd HH:mm:ss") + "','" +txtdelcdate.Text+"','"+txtchalanno.Text + "','" + txtSRREGNO.Text + "','" + hdnBatchNo.Value + "','" + txtrequired.Text + "','" + txtPONO.Text + "','" + txtesthrs.Text + "','',''," + qtysent1 + "," + quantity1 + ",'" + str_dt1 + "','" + Labelrw.Text + "','" + checkIsVirtualTransaction.Checked + "','" + Convert.ToString(Session["UserId"]) + "','" + radiobtnListChallanType.SelectedValue + "','" + txtHSNCode.Text + "','" + txtWeight.Text + "','" + ddlWeightUnit.SelectedValue + "'," + poAmount + ")";
                                                        cmd.CommandText = s;
                                                        cmd.ExecuteNonQuery();
                                                        if (txtdelcdate != null || !txtdelcdate.Equals(DBNull.Value) || txtdelcdate.Text != "")//if user has filled something in delivery commitment date
                                                        {
                                                            string query = "select top 1 id from part_history_gr where tool_no=" + tool_no1 + " and part_name='" + part_name.Text + "' and rwk_no='" + reworkNo + "' and station='" + station.Text + "' and p_no='"+p_no.Text+"' and location='" +location1+ "' and sent_or_rec='Sent' order by date_of_trans desc";
                                                            SqlCommand sqlcommand1 = new SqlCommand(query, conn, trans);
                                                            SqlDataReader dataReader = sqlcommand1.ExecuteReader();
                                                            int partHistoryId=-1;
                                                            while(dataReader.Read())
                                                            {
                                                                if(dataReader["id"]!=null )//checking whether the unique part_history_id exists already in the databsae part_history_gr
                                                                {
                                                                    partHistoryId = Convert.ToInt32(dataReader["id"]);
                                                                }
                                                            }
                                                            dataReader.Close();
                                                            string date = txtdelcdate.Text;
                                                            if (partHistoryId != -1 && !string.IsNullOrEmpty(txtdelcdate.Text))//checking the condition if parthistory id should be there i.e., succesfully inserted intp part_history_gr table and also delivery_commit_date should be there
                                                            {
                                                                string mystring = "insert into CommitmentHistory(part_history_id,p_no,part_name,tool_no,station,location,commitment_date,created_date,created_by,rework_no) values(@part_history_id,@p_no,@part_name,@tool_no,@station,@location,@commitment_date,@created_date,@created_by,@Rwk);";//inserting into commitment_history also 
                                                                SqlCommand sqlcommand2 = new SqlCommand(mystring, conn, trans);
                                                                sqlcommand2.Parameters.AddWithValue("@part_history_id", partHistoryId);
                                                                sqlcommand2.Parameters.AddWithValue("@p_no", p_no.Text);
                                                                sqlcommand2.Parameters.AddWithValue("@part_name", part_name.Text);
                                                                sqlcommand2.Parameters.AddWithValue("@tool_no", tool_no1);
                                                                sqlcommand2.Parameters.AddWithValue("@station", station.Text);
                                                                sqlcommand2.Parameters.AddWithValue("@location", location1);
                                                                sqlcommand2.Parameters.AddWithValue("@commitment_date", txtdelcdate.Text);
                                                                sqlcommand2.Parameters.AddWithValue("@created_by", Session["UserID"]);
                                                                sqlcommand2.Parameters.AddWithValue("@created_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                                                sqlcommand2.Parameters.AddWithValue("@Rwk", reworkNo);
                                                                sqlcommand2.ExecuteNonQuery();
                                                            }
                                                        }
                                                        //code changed by himaneesh adding commitment date while inserting into temp_part_history_qc_gr inserting commitment date
                                                        if (ddlvendor.SelectedItem.Text == " Inspection")
                                                        {
                                                            s = "insert into Temp_part_history_QC_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,Explain_Date,Delivery_commit_date,Challan_No,Sr_Reg_No,Batch_No,Required_date,PO_NO,EST_Hrs,Rework,Fresh,TransSentQty,Upload_Qty,Sent_Date,User_name,Used_Date,BillNo,rwk_no) values ('" + p_no.Text + "','" + part_name.Text + "', '" + tool_no1 + "','" + station.Text + "','" + location1 + "','" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','Sent'," + qtysent1 + "," + Qtysent1 + ",'" + DateTime.Now.AddDays(3).ToString("yyyy/MM/dd HH:mm:ss") + "','" + txtdelcdate.Text + "','" + txtchalanno.Text + "','" + txtSRREGNO.Text + "','" + hdnBatchNo.Value + "','" + txtrequired.Text + "','" + txtPONO.Text + "','" + txtesthrs.Text + "','',''," + qtysent1 + "," + quantity1 + ",'" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','" + Convert.ToString(Session["UserId"]) + "','" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "','" + txtbillno.Text + "','" + Labelrw.Text + "')";
                                                            cmd.CommandText = s;
                                                            cmd.ExecuteNonQuery();
                                                        }

                                                        //UPDATING TABLE LOCATION_INFO
                                                        count = count + 1;

                                                        //RETRIEVING TOTAL QUANTITTY SENT OF A PARTICULAR PART TO A PARTICULAR VENDOR
                                                        s = "select quant_sent,quant_rec from location_info_GR where location='" + location1 + "' and tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and Rwk='" + Labelrw.Text + "'";
                                                        cmd.CommandText = s;
                                                        dt = new DataTable();
                                                        dt.Load(cmd.ExecuteReader());

                                                        if (dt.Rows.Count != 0)
                                                        {
                                                            command = "update";
                                                            quantity1 += Convert.ToInt32(dt.Rows[0][0].ToString());//quant1 is the quantity sent 
                                                            quant_rec = Convert.ToInt32(dt.Rows[0][1].ToString());
                                                        }

                                                        //CALCULATING THE PENDING QUANTITY
                                                        int quant_pend = quantity1 - quant_rec;

                                                        //FINDING QUANTITY PENDING WITH PREVIOUS VENDOR

                                                        //FINDING FIRST VENDOR  and QUANTITY SENT TO HIM
                                                        s = "select top 1 location from part_history_GR where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and rwk_no='" + Labelrw.Text + "' and sent_or_rec='Sent' order by date_of_trans asc";
                                                        cmd.CommandText = s;

                                                        dt = new DataTable();
                                                        dt.Load(cmd.ExecuteReader());

                                                        string first_location = dt.Rows[0][0].ToString();
                                                        int pendingQty = 0;
                                                        if (!first_location.Equals(location))
                                                        {
                                                            s = "select quant_sent, isnull(isnull(quant_sent,0)- isnull(quant_rec,0),0) as PendingQty from location_info_GR where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and location='" + first_location + "' and Rwk='" + Labelrw.Text + "'";
                                                            cmd.CommandText = s;

                                                            dt = new DataTable();
                                                            dt.Load(cmd.ExecuteReader());
                                                            if (dt.Rows.Count > 0)
                                                            {
                                                                first_location_sent = Convert.ToInt32(dt.Rows[0][0].ToString());
                                                                //finding quantity pending with first vendor
                                                                quant_others = first_location_sent - quantity1;//quant others in the location_info_gr table signify the difference between parts sent to first vendor and parts sent to this vendor
                                                                pendingQty = Convert.ToInt32(dt.Rows[0]["PendingQty"].ToString());
                                                            }
                                                        }

                                                        string modsentdate = Convert.ToString(DateTime.Now);
                                                        //NOW INSERTING INTO LOCATION_INFO TABLE
                                                        if (command.Equals("insert"))
                                                        {
                                                            s = "insert into location_info_GR values(" + tool_no1 + ",'" + station.Text + "','" + p_no.Text + "','" + part_name.Text + "'," + req_qty + ",'" + location1 + "'," + Qtysent1 + "," + quant_rec + "," + Qtysent1 + "," + quant_others + ",'" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','','" + DateTime.Now.AddDays(3).ToString("yyyy/MM/dd HH:mm:ss") + "','" + txtdelcdate.Text + "','" + txtchalanno.Text + "','" + txtSRREGNO.Text + "','" + hdnBatchNo.Value + "','" + txtrequired.Text + "','" + txtPONO.Text + "','','','" + txtesthrs.Text + "'," + qtysent1 + "," + quantity1 + ",'" + Convert.ToDateTime(modsentdate).ToString("yyyy/MM/dd HH:mm:ss") + "','" + Labelrw.Text + "'," + 0 + ")";
                                                            cmd.CommandText = s;
                                                            cmd.ExecuteNonQuery();
                                                        }
                                                        else
                                                        {
                                                            if (pendingQty == 0)
                                                            {
                                                                s = "update location_info_GR set  TransSentQty=" + qtysent1 + ", quant_sent=" + qtysent1 + " + isnull(quant_sent,0),quant_pend=isnull(quant_pend,0) +" + qtysent1 + ",quant_others=" + quant_others + ",Delivery_commit_date='" +txtdelcdate.Text + "' where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and location='" + location1 + "' and Rwk='" + Labelrw.Text + "'";
                                                            }
                                                            else
                                                            {
                                                                s = "update location_info_GR set  TransSentQty=" + qtysent1 + ", quant_sent=" + qtysent1 + " + isnull(quant_sent,0),quant_pend=isnull(quant_pend,0) +" + qtysent1 + ",quant_others=" + quant_others + " where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and location='" + location1 + "' and Rwk='" + Labelrw.Text + "'";
                                                            }
                                                            cmd.CommandText = s;
                                                            cmd.ExecuteNonQuery();
                                                        }

                                                        //Insert into Challan Sent Received Detail table
                                                        cmd.Parameters.Clear();
                                                        cmd.CommandText = "insert into Challan_Sent_Received_Detail (Challan_No,Tool_No,Part_Name,P_No,Station,Rwk_No,Location,Sent_Or_Rec,Quantity,Outstanding_Qty,Created_Date,Created_By,Send_Receive_Date) Values(@ChallanNo,@ToolNo,@PartName,@PNo,@Station,@RwkNo,@Location,@SentOrRec,@Quantity,@OutstandingQty,@CreatedDate,@CreatedBy,@SendReceiveDate)";
                                                        cmd.Parameters.AddWithValue("@ChallanNo", txtchalanno.Text);
                                                        cmd.Parameters.AddWithValue("@ToolNo", tool_no1);
                                                        cmd.Parameters.AddWithValue("@PartName", part_name.Text);
                                                        cmd.Parameters.AddWithValue("@PNo", p_no.Text);
                                                        cmd.Parameters.AddWithValue("@Station", station.Text);
                                                        cmd.Parameters.AddWithValue("@RwkNo", Labelrw.Text);
                                                        cmd.Parameters.AddWithValue("@Location", location1);
                                                        cmd.Parameters.AddWithValue("@SentOrRec", "Sent");
                                                        cmd.Parameters.AddWithValue("@Quantity", qtysent1);
                                                        cmd.Parameters.AddWithValue("@OutstandingQty", qtysent1);
                                                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                                                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
                                                        cmd.Parameters.AddWithValue("@SendReceiveDate", Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss"));
                                                        cmd.ExecuteNonQuery();

                                                        if (ddlvendor.SelectedItem.Text != "VD Finish Store" && ddlvendor.SelectedItem.Text != "INH" && ddlvendor.SelectedItem.Text != "PPC" && ddlvendor.SelectedItem.Text != "Store" && ddlvendor.SelectedItem.Text != " Inspection" && ddlvendor.SelectedItem.Text != "Inhouse" && ddlvendor.SelectedItem.Text != "VD Process Store" && ddlvendor.SelectedItem.Text != "Design" && ddlvendor.SelectedItem.Text != "ppc store" && ddlvendor.SelectedItem.Text != "Weld Parts Waiting" && ddlvendor.SelectedItem.Text != "Paint Shop" && ddlvendor.SelectedItem.Text != "Weld Parts Consumed" && ddlvendor.SelectedItem.Text != "GR VD FINISH STORE")
                                                        {
                                                            serialNo = serialNo + 1;
                                                            if (serialNo == 1)
                                                            {
                                                                int? challanNumber = SetChallanNum();
                                                                if (challanNumber != null && challanNumber.Value > 0)
                                                                {
                                                                    ViewState["GeneratedGRChallanNumber"] = Convert.ToString(challanNumber);
                                                                    string vendorNameAndAddress = "";
                                                                    using (SqlConnection conVendor = new SqlConnection(ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString))
                                                                    {
                                                                        conVendor.Open();
                                                                        using (SqlCommand cmdVendor = new SqlCommand("select * from Vendor_Master_Job_work where ID=@ID", conVendor))
                                                                        {
                                                                            cmdVendor.Parameters.AddWithValue("@ID", ddlvendor.SelectedValue);
                                                                            using (SqlDataReader rdr = cmdVendor.ExecuteReader())
                                                                            {
                                                                                if (rdr.HasRows)
                                                                                {
                                                                                    rdr.Read();
                                                                                    vendorNameAndAddress = Convert.ToString(rdr["Vendor_Name"]) + "," + Convert.ToString(rdr["Vendor_Address"]);
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                    mysqlcmd.Parameters.Clear();
                                                                    mysqlcmd.CommandText = "insert into challandetails (ChallanNo,ManualChallanNo,SentDate, IssueDate, MaterialType, PartyNameAndAddress,Dispatched,Location) Values(@ChallanNo,@ManualChallanNo,@SentDate , @IssueDate, @MaterialType,@PartyNameAndAddress,@Dispatched,@Location)";
                                                                    mysqlcmd.Parameters.AddWithValue("@ChallanNo", challanNumber);
                                                                    mysqlcmd.Parameters.AddWithValue("@ManualChallanNo", txtchalanno.Text);
                                                                    mysqlcmd.Parameters.AddWithValue("@SentDate", Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:MM:ss"));
                                                                    mysqlcmd.Parameters.AddWithValue("@IssueDate", Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:MM:ss"));
                                                                    mysqlcmd.Parameters.AddWithValue("@MaterialType", "A4");
                                                                    mysqlcmd.Parameters.AddWithValue("@PartyNameAndAddress", vendorNameAndAddress);
                                                                    mysqlcmd.Parameters.AddWithValue("@Dispatched", "Pending");
                                                                    mysqlcmd.Parameters.AddWithValue("@Location", "Greater Noida, Kasna");
                                                                    mysqlcmd.ExecuteNonQuery();


                                                                }
                                                                else
                                                                {
                                                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Error Occured while sending Data to Falcon Challan Manager.');", true);
                                                                    btnSend.Enabled = true;
                                                                    trans.Rollback();
                                                                    return;
                                                                }
                                                            }

                                                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["GeneratedGRChallanNumber"])))
                                                            {
                                                                mysqlcmd.Parameters.Clear();
                                                                mysqlcmd.CommandText = "INSERT INTO ItemDetails (ItemNo, ItemDescription, QuantitySent, MaterialType, ProjectNo, StationNo, ReworkNo, PositionNo, OpeningBalance, ReceivedQuantity, BalanceQuantity,ChallanNumber, Issuedate, ReceivedInPlantStatus) VALUES (@ItemNo, @ItemDescription, @QuantitySent, @MaterialType, @ProjectNo, @StationNo, @ReworkNo, @PositionNo, @OpeningBalance, @ReceivedQuantity, @BalanceQuantity, @ChallanNumber,@Issuedate, @ReceivedInPlantStatus)";
                                                                mysqlcmd.Parameters.AddWithValue("@ItemNo", serialNo);
                                                                mysqlcmd.Parameters.AddWithValue("@ItemDescription", part_name.Text);
                                                                mysqlcmd.Parameters.AddWithValue("@QuantitySent", Qtysent1);
                                                                mysqlcmd.Parameters.AddWithValue("@MaterialType", "A4");
                                                                mysqlcmd.Parameters.AddWithValue("@ProjectNo", tool_no1);
                                                                mysqlcmd.Parameters.AddWithValue("@StationNo", station.Text);
                                                                mysqlcmd.Parameters.AddWithValue("@ReworkNo", Labelrw.Text);
                                                                mysqlcmd.Parameters.AddWithValue("@PositionNo", p_no.Text);
                                                                mysqlcmd.Parameters.AddWithValue("@OpeningBalance", Qtysent1);
                                                                mysqlcmd.Parameters.AddWithValue("@ReceivedQuantity", 0);
                                                                mysqlcmd.Parameters.AddWithValue("@BalanceQuantity", Qtysent1);
                                                                mysqlcmd.Parameters.AddWithValue("@ChallanNumber", Convert.ToString(ViewState["GeneratedGRChallanNumber"]));
                                                                mysqlcmd.Parameters.AddWithValue("@Issuedate", Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:MM:ss"));
                                                                mysqlcmd.Parameters.AddWithValue("@ReceivedInPlantStatus", false);
                                                                mysqlcmd.ExecuteNonQuery();
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select vendor');", true);
                                                    btnSend.Enabled = true;
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('please enter estimated hrs');", true);
                                                btnSend.Enabled = true;
                                                return;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Quantity Entered. Quantiy has been modified Please refresh the Page.');", true);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Challan Number generation Failed.');", true);
                    return;
                }
                trans.Commit();
                mysqltrans.Commit();
                showgrid();
                //Exporting Data To Excel
                if (ddlvendor.SelectedItem.Text.Trim() == "Inspection")
                {
                    try
                    {
                        LoadQCData(txtchalanno.Text, ddlreceived.SelectedItem.Text);
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                isRequestedForSend = false;
                trans.Rollback();
                mysqltrans.Rollback();
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
                ex.ToString();
            }
            finally
            {
                conn.Close();
                mysqlconn.Close();
            }

            if (count > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Items Sent Successfully');", true);
                btnSend.Visible = false;
                btnSend.Enabled = true;
            }
        }

        string selectQuery = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name<>'Assembly' order by Vendor_Name asc ";
        string dataTextField = "VENDOR_NAME";
        string dataValueField = "ID";
        UtilityFunctions.bind_vendor(ddlvendor, selectQuery, dataTextField, dataValueField);
        conn.Close();
        mysqlconn.Close();
        isRequestedForSend = false;
    }

    /// <summary>
    /// Validates if Position is Parent, returns the number of childs if position is parent
    /// </summary>
    /// <param name="toolNo"></param>
    /// <param name="pNo"></param>
    /// <param name="partName"></param>
    /// <param name="station"></param>
    /// <param name="rwkNo"></param>
    /// <returns></returns>
    private int? ValidatePositionIsParent(string toolNo, string pNo, string partName, string station, string rwkNo)
    {
        int? childRows = null;
        DataTable dt = new DataTable();
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            connec.Open();
            SqlCommand cmd = new SqlCommand();
            string parentName = "P_" + pNo;
            SqlDataAdapter ad = new SqlDataAdapter("select * from parts_Gr where ((opr17 like '%" + parentName + ",%' or opr17 like '%," + parentName + "' or opr17 like '%," + parentName + ",%' or opr17 like '" + parentName + "') or (Parent_Positions like '%" + parentName + ",%' or Parent_Positions like '%," + parentName + "' or Parent_Positions like '%," + parentName + ",%' or Parent_Positions like '" + parentName + "')) and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + rwkNo + "' and Is_Active_Record=1", connec);
            ad.Fill(dt);
            childRows = dt.Rows.Count;
            return childRows;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return childRows;
        }
        finally
        {
            connec.Close();
        }
    }

    /// <summary>
    /// Validates if position is Child position, returns true if position is child and false if position is not child, returns null if there is any exception
    /// </summary>
    /// <param name="toolNo"></param>
    /// <param name="pNo"></param>
    /// <param name="partName"></param>
    /// <param name="station"></param>
    /// <param name="rwkNo"></param>
    /// <returns></returns>
    private bool? ValidatePositionIsChild(string toolNo, string pNo, string partName, string station, string rwkNo)
    {
        bool? isChildPosition = null;
        DataTable dt = new DataTable();
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {

            connec.Open();
            SqlCommand cmd = new SqlCommand();
            //string parentName = "P_";
            SqlDataAdapter ad = new SqlDataAdapter("select * from parts_Gr where ((opr17 is not null and opr17!='') or (Parent_Positions is not null and Parent_Positions!='')) and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + rwkNo + "' and p_no='" + pNo + "' and part_name='" + partName + "' and Is_Active_Record=1", connec);
            ad.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string parentPositionsOld = Convert.ToString(dt.Rows[i]["opr17"]).ToLower();
                    string parentPositionsForNew = Convert.ToString(dt.Rows[i]["Parent_Positions"]).ToLower();
                    if (parentPositionsOld.StartsWith("p_") || parentPositionsForNew.StartsWith("p_"))
                    {
                        string[] opr17 = parentPositionsOld.Split(',');
                        string[] parentPosition = parentPositionsForNew.Split(',');
                        SqlDataAdapter adCheck = new SqlDataAdapter();
                        DataTable dtExist = new DataTable();
                        for (int j = 0; j < opr17.Length; j++)
                        {
                            adCheck = new SqlDataAdapter("select * from parts_gr where (p_no='" + Convert.ToString(opr17[j]).Replace("p_", "") + "' or p_no='" + Convert.ToString(opr17[j]).Replace("p_", "") + "') and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + rwkNo + "' and Is_Active_Record=1", connec);
                            adCheck.Fill(dtExist);
                            if (dtExist.Rows.Count > 0)
                            {
                                isChildPosition = true;
                                break;
                            }
                        }
                        for (int j = 0; j < parentPosition.Length; j++)
                        {
                            adCheck = new SqlDataAdapter("select * from parts_gr where (p_no='" + Convert.ToString(parentPosition[j]).Replace("p_", "") + "' or p_no='" + Convert.ToString(parentPosition[j]).Replace("p_", "") + "') and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + rwkNo + "' and Is_Active_Record=1", connec);
                            adCheck.Fill(dtExist);
                            if (dtExist.Rows.Count > 0)
                            {
                                isChildPosition = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        isChildPosition = false;
                    }
                }
            }
            else
            {
                isChildPosition = false;
            }
            return isChildPosition;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return isChildPosition;
        }
        finally
        {
            connec.Close();
        }
    }

    /// <summary>
    /// A New Chllan Number is generated and data is Sent(inserted) into FCM Data base so that it will shown at the Gate for making it Out and receiving it Back from Vendor
    /// </summary>
    /// <returns></returns>
    protected int? SetChallanNum()
    {
        MySqlConnection connec = new MySqlConnection(ConfigurationManager.ConnectionStrings["FCMConnectionString"].ConnectionString);
        int? challan = null;
        try
        {
            connec.Open();
            string challanquery = "Select ChallanNo, IssueDate from challandetails order by ChallanKey desc limit 1";
            MySqlCommand cmd = new MySqlCommand(challanquery, connec);
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    string date = Convert.ToString(reader[1]);
                    if (date.Contains("-"))
                    {
                        date = date.Replace("-", "/");
                    }

                    date = Convert.ToDateTime(date).ToString("dd/MM/yyyy").Substring(0, 10);
                    int year, month;

                    year = Convert.ToDateTime(date).Year;
                    month = Convert.ToDateTime(date).Month;

                    if ((year == DateTime.Now.Year) && (((month < 4) && (DateTime.Now.Month < 4)) || ((month >= 4) && (DateTime.Now.Month >= 4))))
                        challan = Int32.Parse(reader[0].ToString()) + 1;
                    else if ((DateTime.Now.Year - year == 1) && (month >= 4) && (DateTime.Now.Month < 4))
                        challan = Int32.Parse(reader[0].ToString()) + 1;
                }
            }
            else
            {
                challan = 1;
            }
            reader.Close();
            return challan;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return challan;
            ErrorLogger.LogError("Add Item", "Set Challan Number", ex.Message, ex.StackTrace);
        }
        finally
        {
            connec.Close();
        }

    }

    /// <summary>
    /// Incase of Inspection the Received Quantiy has to be equal to Sent Quantity. So while sending Items to Inspection it is Validated
    /// </summary>
    /// <param name="p_no"></param>
    /// <param name="part_name"></param>
    /// <param name="station"></param>
    /// <param name="tool_no1"></param>
    /// <param name="Qtysent1"></param>
    /// <param name="reworkNo"></param>
    /// <returns></returns>
    public Boolean chkvalidate(string p_no, string part_name, string station, int tool_no1, int Qtysent1, string reworkNo)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();

        DataTable dt = new DataTable();

        try
        {
            string query = "SELECT   [p_no]  ,TransSentQty   ,[part_name]     ,[station]    ,[tool_no]   ,[QtySent]   ,[quant_rec]  FROM [dbo].[part_history_GR] where p_no='" + p_no + "' and part_name='" + part_name + "' and station='" + station + "' and tool_no=" + tool_no1 + " and TransSentQty=" + Qtysent1 + " and sent_or_rec='Received' and rwk_no='" + reworkNo + "'";
            //string query = "SELECT   [p_no]     ,[part_name]     ,[station]    ,[tool_no]   ,[QtySent]   ,[quant_rec]  FROM  [dbo].[part_history_QC] where p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and station='" + station.Text + "' and tool_no=" + tool_no1 + " and TransSentQty>=" + Qtysent1 + " and sent_or_rec='Received'";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                chkval = false;
            }
            else
            {
                chkval = true;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }

        return chkval;
    }
    /// <summary>
    /// Challan Is printed on the basis of Selected Challan Type
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnprint_Click(object sender, EventArgs e)
    {
        Session["MIN"] = txtchalanno.Text;
        if (txtchalanno.Text.Length > 0)
        {
            string challanType = "";
            if (radiobtnListChallanType.SelectedValue == "Purchase Order")
            {
                challanType = "PO";
            }
            else if (radiobtnListChallanType.SelectedValue == "Job Work")
            {
                challanType = "JW";
            }
            else if (radiobtnListChallanType.SelectedValue == "Rework Challan")
            {
                challanType = "RC";
            }
            else if (radiobtnListChallanType.SelectedValue == "Non Returnable Reject Challan")
            {
                challanType = "NR";
            }
            Response.Write("<script language=javascript>child=window.open('SentChallan.aspx?ChallanType=" + challanType + "&AutoGeneratedChallan=" + ViewState["GeneratedGRChallanNumber"] + "');</script>");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Send part before Print');", true);
            return;
        }
    }

    /// <summary>
    /// Validates if Vendor is Active or Inactive and displays the same Message. User is not allowed to send parts to active Vendors only.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlvendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlvendor.SelectedItem.Text == "VD Finish Store" || ddlvendor.SelectedItem.Text == "Assembly")
            {
                txtesthrs.Enabled = false;
                txtrequired.Enabled = false;
                txtdelcdate.Enabled = false;
            }
			else
			{
				txtesthrs.Enabled = true;
                txtrequired.Enabled = true;
                txtdelcdate.Enabled = true;
			}
			
            if (ddlvendor.SelectedItem.Text.Trim() == "Inspection")
            {
                printispectionchallan.Visible = true;
                btnprint.Visible = false;
                tdPrintChallan.Visible = false;
                ddlreceived.Visible = true;
                tdVendorForInspection.Visible = true;
                Label4.Visible = true;
                lblbillno.Visible = true;
                txtbillno.Visible = true;
            }
            else
            {
                printispectionchallan.Visible = false;
                btnprint.Visible = true;
                tdPrintChallan.Visible = true;
                ddlreceived.Visible = false;
                tdVendorForInspection.Visible = false;
                Label4.Visible = false;
                lblbillno.Visible = false;
                txtbillno.Visible = false;
            }
            LoadPONumbersForInspection();
            lblVendorInactiveMessage.Text = String.Empty;
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            using (SqlConnection conn = new SqlConnection(sqlconnstring))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("select * from Vendor_Master_Job_Work where Vendor_Name=@VendorName", conn))
                {
                    cmd.Parameters.AddWithValue("@VendorName", ddlvendor.SelectedItem.Text);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            hdnVendorType.Value = Convert.ToString(rdr["vendor_type"]);
                            ViewState["IsActiveVendor"] = Convert.ToBoolean(rdr["Is_Active"]);
                            if (string.IsNullOrEmpty(Convert.ToString(rdr["Allocated_To"])))
                            {
                                ViewState["IsActiveVendor"] = false;
                            }
                            if (!Convert.ToBoolean(ViewState["IsActiveVendor"]))
                            {
                                lblVendorInactiveMessage.Text = "Inactive Vendor Selected, You can't Send Positions to Inactive Vendor.  <b>Note:-</b> If Supervisor is not assigned then Vendor will be Considered as Inactive.";
                                btnSend.Visible = false;
                            }
                            else
                            {
                                btnSend.Visible = true;
                            }
                        }
                    }
                }
            }
            foreach (GridViewRow row in GridView1.Rows)
            {
                CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
                TextBox TextBox3 = (TextBox)row.FindControl("TextBox3");
                TextBox txtbalc = (TextBox)row.FindControl("txtbalc");
                Label Label8 = (Label)row.FindControl("Label8");
                Label Labelsent = (Label)row.FindControl("Labelsent");
                Label uploadqty = (Label)row.FindControl("LabelUpload_Qty");
                TextBox3.Text = "0";
                if (ChkBoxRows.Checked == true)
                {
                    EnableTextBox(ChkBoxRows);
                }
            }
            if (ddlvendor.SelectedItem.Text == "GR VD FINISH STORE" || ddlvendor.SelectedItem.Text == "Not Required")
            {
                showgrid();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void ddllocauto_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddllocauto.SelectedItem.Text == "Inspection")
        {
            printispectionchallan.Visible = true;
            btnprint.Visible = false;
            tdPrintChallan.Visible = false;
        }
        else
        {
            printispectionchallan.Visible = false;
            btnprint.Visible = true;
            tdPrintChallan.Visible = true;
        }
    }

    protected void printispectionchallan_Click(object sender, EventArgs e)
    {
        if (txtchalanno.Text.Length > 0)
        {
            if (ddlreceived.SelectedItem.Text.Length > 0)
            {
                Session["MIN"] = Convert.ToString(Session["QCSheetNo"]); //txtchalanno.Text;
                Session["vendor"] = ddlreceived.SelectedItem.Text;
                //Session["QCSheetNo"]
                Response.Write("<script language=javascript>child=window.open('GetPrintGRChallan.aspx?Type=QC');</script>");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Vendor For Inspection');", true);
                return;
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Send part');", true);
            return;
        }
    }

    /// <summary>
    /// This event is fired when user clicks on Search Button
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button1_Click(object sender, ImageClickEventArgs e)
    {
        Session["MIN"] = null;
        Session["vendor"] = null;
        lblQCSheetNo.Text = String.Empty;
        txtPONO.Enabled = false;
        txtPONO.Text = String.Empty;
        showgrid();
        //GridView1.Columns[7].Visible = false;
        if (GridView1.Rows.Count == 1)
            lbl.Text = "";
        else
            GridView1.Columns[10].Visible = false;
        GridView1.Columns[11].Visible = false;
    }

    /// <summary>
    /// Validates if date is past or future date and display error message if it is past or future dates
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtpodate_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDateTime(txtpodate.Text) > DateTime.Now || Convert.ToDateTime(txtpodate.Text) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Can not take future/past date');", true);
            txtpodate.Text = "";
        }
    }
    /// <summary>
    ///  Clicking on the position number it displays the History of that Position
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Label1_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        try
        {
            string query = "select  id,location as Party,date_of_trans as Trans_Date,sent_or_rec as Trans_Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as PendingTotQty_in_PPC,TransSentQty as SendQty_Or_ReceivedQty,User_Name  from part_history_GR  where   p_no='" + Positionno + "' and  tool_no='" + toolno + "' and  station='" + station + "' and  part_name='" + part_name + "' and rwk_no='" + rwk_no + "' order by date_of_trans asc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();

            da.Fill(table);

            StringBuilder b = new StringBuilder();

            b.Append("<table style='");
            b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<th>");
                b.Append(column.ColumnName);
                b.Append("</th>");
            }
            b.Append("</tr>");

            foreach (DataRow row in table.Rows)
            {
                b.Append("<tr>");
                foreach (DataColumn column in table.Columns)
                {
                    b.Append("<td>");
                    b.Append(row[column.ColumnName]);
                    b.Append("</td>");
                }
                b.Append("</tr>");
            }
            b.Append("</table>");

            pnlHistoryPopUp.InnerHtml = b.ToString();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }

    /// <summary>
    /// Retrieves the PO Number as Internal if its Internal Vendor apart from Inspection. 
    /// If its Inspection then displays the list of Sent PO number for same tool, pno, partname,station and rework no
    /// </summary>
    /// <param name="text"></param>
    /// <param name="vendorType"></param>
    /// <param name="vendorName"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetPONumber(string text, string vendorType, string vendorName)
    {
        try
        {
            if (vendorType == "Internal Vendor")
            {
                if (vendorName.Trim() != "Inspection")
                {
                    List<string> poNoList = new List<string>();
                    poNoList.Add("Internal");
                    return poNoList;
                }
                else
                {
                    if (HttpContext.Current.Session["POList"] != null)
                    {
                        List<string> poNoList = (List<string>)HttpContext.Current.Session["POList"];
                        if (poNoList.Count == 0)
                        {
                            poNoList.Add("NA");
                        }
                        else
                        {
                            if (poNoList.Count == 1)
                            {
                                if (poNoList[0] == "")
                                {
                                    poNoList.Add("NA");
                                }
                            }
                            return poNoList;
                        }
                    }
                }
            }
            return null;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {

        }
    }

    /// <summary>
    /// Retrieves the List of Sent Received Challan Numbers when users clicks on Challan Number TextBox
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetChallanNumber(string text)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            connec.Open();
            List<string> allItems = new List<string>();
            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrWhiteSpace(text))
            {
                cmd = new SqlCommand("select distinct Challan_No from part_history_Gr where Challan_No like '" + text + "%' and Sent_Or_Rec in ('Sent','Received') order by Challan_No ASC", connec);
            }
            else
            {
                cmd = new SqlCommand("select distinct Challan_No from part_history_Gr where Sent_Or_Rec in ('Sent','Received') order by Challan_No ASC", connec);
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string challanNo = Convert.ToString(reader["Challan_No"]);
                allItems.Add(challanNo);
            }
            reader.Close();
            return allItems;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            connec.Close();
        }
    }

    /// <summary>
    /// Retrieves the Valid and Possible quantity for Send, also Retrieves the Balance Quantity and displays the same to user.
    /// </summary>
    /// <param name="currentToolNo"></param>
    /// <param name="currentStation"></param>
    /// <param name="currentPartName"></param>
    /// <param name="currentPNo"></param>
    /// <param name="currentRwkNo"></param>
    /// <param name="notRequiredQty"></param>
    /// <param name="availableQty"></param>
    /// <param name="enteredQty"></param>
    /// <param name="possibleQtyForSend"></param>
    /// <param name="balanceQty"></param>
    /// <returns></returns>
    protected int? ValidateQuantities(string currentToolNo, string currentStation, string currentPartName, string currentPNo, string currentRwkNo,
       int notRequiredQty, int availableQty, int enteredQty, out int possibleQtyForSend, out int balanceQty)
    {
        try
        {
            possibleQtyForSend = 0; balanceQty = 0;
            int qtySentToGrVDFinish = 0;
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            using (SqlConnection connec = new SqlConnection(sqlconnstring))
            {
                connec.Open();
                using (SqlCommand cmd = new SqlCommand("select isnull(isnull(quant_sent,0)- isnull(quant_rec,0),0) as QtyPending from location_info_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and part_name=@PartName and rwk=@RwkNo and location='Not Required'", connec))
                {
                    cmd.Parameters.AddWithValue("@ToolNo", currentToolNo);
                    cmd.Parameters.AddWithValue("@Station", currentStation);
                    cmd.Parameters.AddWithValue("@PNo", currentPNo);
                    cmd.Parameters.AddWithValue("@PartName", currentPartName);
                    cmd.Parameters.AddWithValue("@RwkNo", currentRwkNo);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            if (rdr["QtyPending"] != DBNull.Value)
                                qtySentToGrVDFinish = Convert.ToInt32(rdr["QtyPending"]);
                        }
                    }
                }
            }

            int? returnQty = null;
            if (notRequiredQty > 0)
            {
                int qtyYetToBeSentAtGRVDFinishStore = notRequiredQty - qtySentToGrVDFinish;
                if (ddlvendor.SelectedItem.Text != "Not Required")
                {
                    availableQty = availableQty - qtyYetToBeSentAtGRVDFinishStore;
                    returnQty = availableQty;
                }
                else
                {
                    if (qtyYetToBeSentAtGRVDFinishStore < availableQty)
                    {
                        availableQty = qtyYetToBeSentAtGRVDFinishStore;
                    }
                    returnQty = availableQty;
                }
            }
            else
            {
                returnQty = availableQty;
            }
            if (enteredQty <= availableQty && enteredQty > 0)
            {
                returnQty = enteredQty;
            }
            if ((ddlvendor.SelectedItem.Text != "Not Required" && notRequiredQty >= 0) || (ddlvendor.SelectedItem.Text == "Not Required" && notRequiredQty > 0))
            {
                if (returnQty != null)
                {
                    if (returnQty > 0)
                    {
                        possibleQtyForSend = Convert.ToInt32(returnQty.Value);
                        int balance = Convert.ToInt32(availableQty) - Convert.ToInt32(returnQty);
                        balanceQty = Convert.ToInt32(balance);
                    }
                    else
                    {
                        balanceQty = 0;
                    }
                }
            }
            else if (ddlvendor.SelectedItem.Text == "Not Required" && notRequiredQty == 0)
            {
                possibleQtyForSend = 0;
                balanceQty = 0;
            }
            else
            {
                int balance = Convert.ToInt32(availableQty) - Convert.ToInt32(returnQty.Value);
                balanceQty = Convert.ToInt32(balance);
                possibleQtyForSend = returnQty.Value;
            }
            return returnQty;
        }
        catch (Exception ex)
        {
            ex.ToString();
            possibleQtyForSend = 0;
            balanceQty = 0;
            return null;
        }
    }

    /// <summary>
    /// On the Change of vendor for inspection dropdown Loads the Po Numbers
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlreceived_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtPONO.Text = String.Empty;
        LoadPONumbersForInspection();
    }

    /// <summary>
    /// Loads the List of PO numbers for Inspection
    /// </summary>
    protected void LoadPONumbersForInspection()
    {
        Session["POList"] = null;
        if (ddlreceived.SelectedItem.Text != "")
        {
            if (ddlvendor.SelectedItem.Text.Trim() == "Inspection")
            {
                foreach (GridViewRow gr in GridView1.Rows)
                {
                    Label lblStation = GridView1.Rows[gr.RowIndex].FindControl("Label10") as Label;
                    Label lblrew = GridView1.Rows[gr.RowIndex].FindControl("lblReworkNo") as Label;
                    Label lblToolNo = GridView1.Rows[gr.RowIndex].FindControl("lblproject") as Label;
                    LinkButton lnkbtnPNo = GridView1.Rows[gr.RowIndex].FindControl("Label1") as LinkButton;
                    Label lblPartName = GridView1.Rows[gr.RowIndex].FindControl("Label2") as Label;
                    HiddenField hdnNotRequired = GridView1.Rows[gr.RowIndex].FindControl("hdnNotRequired") as HiddenField;
                    CheckBox chkAllchild = GridView1.Rows[gr.RowIndex].FindControl("chkAllchild") as CheckBox;
                    if (chkAllchild.Checked == true)
                    {
                        BindPOList(lblToolNo.Text, lnkbtnPNo.Text, lblPartName.Text, lblStation.Text, lblrew.Text);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Inserts the QC Sheet Number in case users is sending Items to Inspeciton and QCSheet Number is generated for the Send, the same is shown to User at the Top 
    /// Or user can print the QC Sheet by clicking the Print QC Sheet button, in that case also QC Sheet is generated for this generated QC Sheet Number which is generated throgh below code
    /// </summary>
    /// <param name="challanNo"></param>
    /// <param name="vendor"></param>
    protected void LoadQCData(string challanNo, string vendor)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        SqlTransaction trans = null;
        try
        {
            con.Open();
            trans = con.BeginTransaction();
            string query = "";
            if (Convert.ToString(Request.QueryString["Type"]) == "QC")
            {
                query = "select * from(select  ROW_NUMBER()  over(partition by [p_no]  ,BillNo,[part_name],[station], quantity  ,tool_no   ,QtySent,Challan_No,date_of_trans order by location Desc)  as  RowNum , [p_no] , BillNo, [part_name],[station],  rwk_no  , quantity  , tool_no   , QtySent, Challan_No, date_of_trans ,location"
                         + " from (SELECT distinct  Temp_Part_QC.[p_no]  , Temp_Part_QC.BillNo,  Temp_Part_QC.[part_name], Temp_Part_QC.[station],  parts.rwk_no  ,  Temp_Part_QC.quantity  , Temp_Part_QC.tool_no   , Temp_Part_QC.QtySent,  Temp_Part_QC.Challan_No, Temp_Part_QC.date_of_trans , QCPart_His.location"
                         + " FROM    Temp_part_history_QC_GR Temp_Part_QC  inner join parts_gr parts  on parts.tool_no=Temp_Part_QC.tool_no and parts.p_no=Temp_Part_QC.[p_no]  and parts.station=Temp_Part_QC.station  and parts.part_name=Temp_Part_QC.part_name and  parts.rwk_no= Temp_Part_QC.rwk_no "
                         + " inner join    part_history_QC_GR  QCPart_His  on QCPart_His.p_no=Temp_Part_QC.p_no  and QCPart_His.part_name=Temp_Part_QC.part_name   and QCPart_His.station=Temp_Part_QC.station "
                         + " and QCPart_His.quantity= Temp_Part_QC.quantity where  Temp_Part_QC.Qcsheet_no='" + challanNo + "' )myTempTable)a where RowNum=1";
            }
            else
            {
                int k = showData1();
                k = k + 1;
                string qcSheetNo = k.ToString();
                string finalQcSheetNo = "";
                SqlCommand cmdTran = new SqlCommand();
                cmdTran.Connection = con;
                cmdTran.Transaction = trans;
                cmdTran.CommandText = "insert into part_history1(Challan_No) values('" + challanNo + "')";
                cmdTran.ExecuteNonQuery();

                if (challanNo.StartsWith("CH") || challanNo.StartsWith("V_"))
                {
                    finalQcSheetNo = "QCSheet" + qcSheetNo;
                }
                else if (challanNo.StartsWith("GR"))
                {
                    finalQcSheetNo = "GRQCSheet" + qcSheetNo;
                }
                Session["QCSheetNo"] = finalQcSheetNo;
                cmdTran.CommandText = "update  temp_part_history_QC_GR set Qcsheet_no='" + finalQcSheetNo + "'where Challan_No='" + challanNo + "'";
                cmdTran.ExecuteNonQuery();
                lblQCSheetNo.Text = finalQcSheetNo;
                updateButton.Update();
                UpdatePanel1.Update();
                trans.Commit();
                //query = "SELECT distinct tphqc_gr.po_no, tphqc_gr.[p_no],tphqc_gr.BillNo,tphqc_gr.[part_name],tphqc_gr.[station],tphqc_gr.quantity,"
                //+ " tphqc_gr.tool_no ,tphqc_gr.QtySent,phq_gr.location,tphqc_gr.Challan_No,tphqc_gr.date_of_trans, phg.rwk_no FROM part_history_QC_GR phq_gr "
                //+ " inner join Temp_part_history_QC_GR tphqc_gr on tphqc_gr.p_no=phq_gr.p_no and phq_gr.part_name=tphqc_gr.part_name and phq_gr.station=tphqc_gr.station "
                //+ " inner join part_history_GR phg on phg.p_no=phq_gr.p_no and phg.location=phq_gr.location and phg.station=phq_gr.station and phg.part_name = phq_gr.part_name "
                //+ " where tphqc_gr.QtySent=phq_gr.quantity and tphqc_gr.Challan_No='" + challanNo + "' and phq_gr.location='" + vendor + "' and phg.[Rwk_No]=tphqc_gr.[Rwk_No]";

                //DataTable dt = ExportToExcel(query);
                //if (dt.Rows.Count > 0)
                //{
                //    SaveDataToFile(dt, finalQcSheetNo);
                //}
            }

        }
        catch (Exception ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            con.Close();
        }
    }

    /// <summary>
    /// Retrieves the Last Value for QCsheet Number from part_history1 table
    /// </summary>
    /// <returns></returns>
    public int showData1()
    {
        int id = 0;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string conString = "SELECT isnull(MAX(id),0) as id  FROM part_history1 ";
        SqlCommand cmd = new SqlCommand(conString, conn);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        adp.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            id = Convert.ToInt16(dt.Rows[0][0].ToString());
        }
        return id;
    }

    protected DataTable ExportToExcel(string query)
    {
        System.Data.DataTable dt = new System.Data.DataTable();
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            using (SqlConnection con = new SqlConnection(sqlconnstring))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);
                        return dt;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }

    }

    //public void SaveDataToFile(DataTable dt, string qcSheetNo)
    //{
    //    try
    //    {
    //        string name = DateTime.Now.ToString("ddMMyyyyHHmmss") + ".csv";
    //        string directory = Server.MapPath("~/POData/");

    //        if (!Directory.Exists(directory))
    //        {
    //            Directory.CreateDirectory(directory);
    //        }


    //        string fileName = directory + @"\" + name;
    //        bool fileNotExist = false;
    //        if (!File.Exists(fileName))
    //        {
    //            fileNotExist = true;
    //        }
    //        if (fileNotExist)
    //        {
    //            StreamWriter writer = new StreamWriter(fileName);
    //            string headings = "QC Date,QC Sheet Number,PO Number,Tool,Station,GR,Position,Qty,Ok Qty,NC Qty,Rework Amount";
    //            writer.WriteLine(headings);
    //            writer.Close();
    //        }
    //        for (int k = 0; k < dt.Rows.Count; k++)
    //        {
    //            string qcDate = DateTime.Now.ToShortDateString();
    //            string poNo = Convert.ToString(dt.Rows[k]["PO_No"]);
    //            string toolNo = Convert.ToString(dt.Rows[k]["Tool_No"]);
    //            string station = Convert.ToString(dt.Rows[k]["Station"]);
    //            string rwkNo = Convert.ToString(dt.Rows[k]["Rwk_No"]);
    //            string pNo = Convert.ToString(dt.Rows[k]["P_No"]);
    //            string qty = Convert.ToString(dt.Rows[k]["Quantity"]);
    //            string okQty = String.Empty;
    //            string notOkQty = String.Empty;
    //            string rwkAmount = String.Empty;

    //            string currentRow = qcDate + "," + qcSheetNo + "," + poNo + "," + toolNo + "," + station + "," + rwkNo + "," + pNo + "," + qty + "," + okQty + "," + notOkQty + "," + rwkAmount;
    //            TextWriter sw = new StreamWriter(fileName, true);
    //            sw.WriteLine(currentRow);
    //            sw.Close();

    //        }
    //        hyplnkDownload.NavigateUrl = "~/POData/" + name;
    //        hyplnkDownload.Text = "Downlad QC Items in Excel";
    //        updateButton.Update();
    //        UpdatePanel1.Update();
    //    }
    //    catch (Exception ex)
    //    {
    //        ex.ToString();
    //    }

    //}

}