﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Reflection;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

public partial class Tool_BOMMasterData : System.Web.UI.Page
{
    public static string searchCondition;
    protected void Page_Load(object sender, EventArgs e)
    {
        searchCondition = "";
        try
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Displays the data from BOM Master table on the basis of Search Criteria
    /// </summary>
    protected void BindGrid()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            string searchCondition = "";

            if (inputToolNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.tool_no='" + inputToolNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.tool_no='" + inputToolNo.Text + "'";
                }
            }
            if (inputStation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.station='" + inputStation.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.station='" + inputStation.Text + "'";
                }
            }
            if (inputPNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.p_no='" + inputPNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.p_no='" + inputPNo.Text + "'";
                }
            }
            if (inputReworkNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.rwk_no='" + inputReworkNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.rwk_no='" + inputReworkNo.Text + "'";
                }
            }
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = "";
            if (!string.IsNullOrEmpty(searchCondition))
            {
                query = "select lig.tool_no,lig.station,lig.p_no,lig.part_name,lig.Rwk_No,lig.Upload_Qty,lig.BOM_Qty,lig.Type,lig.Version_Number, lig.Created_Date from Bom_Master lig where " + searchCondition + " order by lig.tool_no,lig.station asc";
                adapter = new SqlDataAdapter(query, con);
            }
            else
            {
                query = "select lig.tool_no,lig.station,lig.p_no,lig.part_name,lig.Rwk_No,lig.Upload_Qty,lig.BOM_Qty,lig.Type,lig.Version_Number,lig.Created_Date from Bom_Master lig order by lig.tool_no,lig.station asc";
                adapter = new SqlDataAdapter(query, con);
            }
            ViewState["CurrentQuery"] = query;
            DataTable td = new DataTable();
            adapter.Fill(td);
            gridBOM.DataSource = td;
            gridBOM.DataBind();
            gridBOM.EmptyDataText = "No Record Found";
            lblRecorrdsCount.Text = Convert.ToString(td.Rows.Count) + " " + "Records In Total";
            if (gridBOM.Rows.Count > 0)
            {
                btnDownload.Visible = true;
            }
            else
            {
                btnDownload.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }
    }

    protected void gridBOM_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // Set the index of the new display page.  
        gridBOM.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void gridBOM_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            Response.Redirect("Vendor_Master.aspx?ID=" + e.CommandArgument);
        }
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        UtilityFunctions.ExportToExcel(Convert.ToString(ViewState["CurrentQuery"]), "BOMMaster");
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    /// <summary>
    /// Retrieves the File Name for Saved Design File from tbl_VD_Release_info table for each row by Tool No, PNo, Station and Rework Number and displays download link for each row.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridBOM_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            LinkButton lnkBtnBOMUrl = (LinkButton)e.Row.FindControl("lnkBtnBOMUrl");

            using (SqlConnection connec = new SqlConnection(sqlconnstring))
            {
                connec.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connec;
                cmd.Parameters.Clear();
                cmd.CommandText = "select * from tbl_VD_Release_info where Tool_No=@ToolNo and Position_No=@PNo and Station_No=@Station and Rwk_No=@RwkNo";
                cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                cmd.Parameters.AddWithValue("@RwkNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.HasRows)
                    {
                        rdr.Read();
                        if (rdr["FileName"] != DBNull.Value)
                        {
                            lnkBtnBOMUrl.PostBackUrl = "file://192.168.1.250/For_VD_Released_Design/" + Convert.ToString(rdr["FileName"]);
                        }
                    }
                }
            }
        }
    }
}