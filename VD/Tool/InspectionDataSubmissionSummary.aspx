﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="InspectionDataSubmissionSummary.aspx.cs" Inherits="Tool_InspectionDataSubmissionSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 860px;
            height: 440px;
            top: 20%;
            left: 30%;
            margin-left: -185px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
            line-height: 28px;
            overflow-y: scroll;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .awbimagecontainer
        {
            float: right;
            width: 29%;
            margin-right: 12px;
            margin-top: 43px;
            border: 1px solid #2D89EF;
            height: 180px;
        }
        .web_dialog_info
        {
            display: none;
            position: fixed;
            width: 940px;
            height: 510px;
            top: 22%;
            left: 36%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style=" border: 1px solid Blue; min-height: 100px; padding: 9px;">
        <div>
            <div align="center">
                <span class="style13">Inspection Sheet Submission Summary</span>
            </div>
            <p style="font-size: 13px;">
                Note: Displays the Inspection Summary of position along with Information like Actual
                Hrs, Estimated Hrs, Inspected Quantity etc.</p>
        </div>
        <div style="margin-bottom: 10px; margin-top: 20px;">
            <div style="font-size: 13px; width: 90px; float: left;">
                Tool No
                <br />
                <asp:TextBox ID="inputToolNo" CssClass="glowing-border" runat="server" Width="80px"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 85px; float: left;">
                Rework No
                <br />
                <asp:TextBox ID="inputReworkNo" CssClass="glowing-border" runat="server" Width="75px"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 85px; float: left;">
                Station
                <br />
                <asp:TextBox ID="inputStation" CssClass="glowing-border" runat="server" Width="75px"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 90px; float: left;">
                Position No
                <br />
                <asp:TextBox ID="inputPNo" CssClass="glowing-border" runat="server" Width="80px"></asp:TextBox>
            </div>
              <div style="font-size: 13px; width: 85px; float: left;">
                Challan No
                <br />
                <asp:TextBox ID="inputChallanNumber" CssClass="glowing-border" runat="server" Width="75px"></asp:TextBox>
            </div>
              <div style="font-size: 13px; width: 85px; float: left;">
                QC Sheet No
                <br />
                <asp:TextBox ID="inputQCSheetNo" CssClass="glowing-border" runat="server" Width="75px"></asp:TextBox>
            </div>
            <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/images/searchbtn.jpg"
                OnClick="btnSearch_Click" Style="margin-top: 10px;" />
            <asp:HyperLink ID="btnDownload" runat="server" Text="Inspection Submission" Style="margin-top: 9px;
                margin-right: 2px; float: right; background-color: green; color: white; padding: 6px;
                border: 1px solid green; box-shadow: 2px 3px 2px green;" NavigateUrl="~/Tool/InspectionSheetSubmissionByQcSheetNumber.aspx" />
        </div>
        <asp:GridView ID="GridView1" runat="server" EmptyDataText="No Record Found" AutoGenerateColumns="False"
            AllowPaging="true" AllowSorting="false" PageSize="20" Style="font-size: 13px;
            font-family: Verdana; line-height: 26px;" Width="100%" OnPageIndexChanging="GridView1_PageIndexChanging">
            <RowStyle BackColor="White" ForeColor="Black" />
            <Columns>
                <asp:BoundField DataField="Tool_No" HeaderText="Tool No" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="100" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="P_No" HeaderText="Position" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="100" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Part_Name" HeaderText="Part Name" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="400" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Station" HeaderText="Station" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="150" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Rework_No" HeaderText="Rework No" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="150" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Received_Quantity" HeaderText="Received Qty" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="150" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="QC_Sheet_Number" HeaderText="QC Sheet No" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="150" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Challan_No" HeaderText="Challan No" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="150" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Estimated_Time" HeaderText="Estimated Time" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="150" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Actual_Time" HeaderText="Actual Time" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="150" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Inspected_By" HeaderText="Inspected By" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="150" HeaderStyle-ForeColor="White" />
            </Columns>
            <PagerStyle BackColor="#335599" ForeColor="White" Font-Size="Medium" HorizontalAlign="Center"
                CssClass="gridview" />
            <SelectedRowStyle BackColor="White" Font-Bold="False" ForeColor="#000" />
            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
        </asp:GridView>
    </div>
</asp:Content>
