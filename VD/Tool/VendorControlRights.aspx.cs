﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class Tool_VendorControlRights : System.Web.UI.Page
{
    /// <summary>
    /// If QueryString for User Name is not Null then loads the Data for that User, other wise the form is blank
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadAuthors();
            if(!string.IsNullOrEmpty(Request.QueryString["Name"]))
            {
                ddlUser.SelectedValue = Request.QueryString["Name"];
                ddlUser_SelectedIndexChanged(sender,e);
            }
        }
    }

    /// <summary>
    /// Loads the List of Users from LDAP
    /// </summary>
    protected void LoadAuthors()
    {
        try
        {
            List<Users> lstADUsers = Users.LoadUsersList();
            if (lstADUsers.Count > 0)
            {
                List<Users> ldapUsers = lstADUsers.OrderBy(x => x.DisplayName).ToList();
                ddlUser.DataSource = ldapUsers;
                ddlUser.DataTextField = "UserName";
                ddlUser.DataValueField = "UserName";
                ddlUser.DataBind();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// When user selects the User Name it Loads the Data for Selected User
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMessage.Text = String.Empty;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            connec.Open();
            SqlCommand cmd = new SqlCommand("select * from Vendor_Control_Rights where User_Name=@UserName", connec);
            cmd.Parameters.AddWithValue("@UserName", ddlUser.SelectedValue);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    checkAddRights.Checked = Convert.ToBoolean(reader["Add_Rights"]);
                    checkEditRights.Checked = Convert.ToBoolean(reader["Edit_Rights"]);
                    checkViewRights.Checked = Convert.ToBoolean(reader["View_Rights"]);
                    checkDeleteRights.Checked = Convert.ToBoolean(reader["Delete_Rights"]);
                    checkDownloadRights.Checked = Convert.ToBoolean(reader["Download_Rights"]);
                    checkUploadRights.Checked = Convert.ToBoolean(reader["Upload_Rights"]);
                }
                else
                {
                    checkAddRights.Checked = false;
                    checkEditRights.Checked = false;
                    checkViewRights.Checked= false;
                    checkDeleteRights.Checked= false;
                    checkDownloadRights.Checked= false;
                    checkUploadRights.Checked = false;
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            connec.Close();
        }
    }

    /// <summary>
    /// Udates the Data if data already exists for Selected User other wise add the New Data in Vendor Control Rights Table
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            conn.Open();
            cmd.Connection = conn;

            cmd.Parameters.Clear();
            bool recordExist = false;
            cmd.CommandText = "select * from Vendor_Control_Rights where User_Name=@UserName";
            cmd.Parameters.AddWithValue("@UserName", ddlUser.SelectedValue);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    recordExist = true;
                }
            }
            cmd.Parameters.Clear();
            if (recordExist)
            {
                cmd.CommandText = "update  Vendor_Control_Rights set User_Name=@UserName, Add_Rights=@AddRights,Edit_Rights=@EditRights,Delete_Rights=@DeleteRights,View_Rights=@ViewRights,Download_Rights=@DownloadRights,Upload_Rights=@UploadRights,Updated_By=@UpdatedBy, Updated_Date=@UpdatedDate where User_Name=@UserName";
                cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
                cmd.Parameters.AddWithValue("@UpdatedBy", Convert.ToString(Session["UserID"]));
            }
            else
            {
                cmd.CommandText = "insert into Vendor_Control_Rights(User_Name,Add_Rights,Edit_Rights,Delete_Rights,View_Rights,Download_Rights,Upload_Rights,Created_By,Created_Date) Values(@UserName,@AddRights,@EditRights,@DeleteRights,@ViewRights,@DownloadRights,@UploadRights,@CreatedBy,@CreatedDate)";
                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
            }
            cmd.Parameters.AddWithValue("@UserName", ddlUser.SelectedValue);
            cmd.Parameters.AddWithValue("@AddRights", checkAddRights.Checked);
            cmd.Parameters.AddWithValue("@EditRights", checkEditRights.Checked);
            cmd.Parameters.AddWithValue("@DeleteRights", checkDeleteRights.Checked);
            cmd.Parameters.AddWithValue("@ViewRights", checkViewRights.Checked);
            cmd.Parameters.AddWithValue("@DownloadRights", checkDownloadRights.Checked);
            cmd.Parameters.AddWithValue("@UploadRights", checkUploadRights.Checked);
            cmd.ExecuteNonQuery();
            lblMessage.Text = "Data Saved Successfully";
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Some Error Occured While Saving Record. Kindly Contact Administrator.";
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }

}