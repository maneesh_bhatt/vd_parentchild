﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text;
using AjaxControlToolkit;


public partial class Tool_InspectionDataSubmissionSummary : System.Web.UI.Page
{
    SqlConnection connection;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            showgrid();
        }
    }

    /// <summary>
    /// Retrieves the data from InspectionSheetSubmissionHistory table and displays it based on User Search Criteria
    /// </summary>
    public void showgrid()
    {
        try
        {
            string searchCondition = "";
            
            if (inputToolNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.tool_no='" + inputToolNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.tool_no='" + inputToolNo.Text + "'";
                }
            }
            if (inputStation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.station='" + inputStation.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.station='" + inputStation.Text + "'";
                }
            }
            if (inputPNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.p_no='" + inputPNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.p_no='" + inputPNo.Text + "'";
                }
            }
            if (inputReworkNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.Rework_No='" + inputReworkNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.Rework_No='" + inputReworkNo.Text + "'";
                }
            }
            if (inputChallanNumber.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.Challan_No='" + inputChallanNumber.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.Challan_No='" + inputChallanNumber.Text + "'";
                }
            }
            if (inputQCSheetNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.QC_Sheet_Number='" + inputQCSheetNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.QC_Sheet_Number='" + inputQCSheetNo.Text + "'";
                }
            }
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            connection = new SqlConnection(sqlconnstring);
            connection.Open();
            string query = "";
            if (!string.IsNullOrEmpty(searchCondition))
            {
                query = "select lig.tool_no,lig.p_no,lig.part_name,lig.station,lig.Rework_No,isnull(lig.Received_Quantity,0) as Received_Quantity, lig.QC_Sheet_Number,lig.Challan_No,lig.Estimated_Time,lig.Actual_Time,lig.Inspected_By from InspectionSheetSubmissionHistory lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rework_no= pgr.rwk_no where pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchCondition + " order by lig.InspectionSheetSubmissionHistoryId desc";
            }
            else
            {
                query = "select lig.tool_no,lig.p_no,lig.part_name,lig.station,lig.Rework_No,isnull(lig.Received_Quantity,0) as Received_Quantity, lig.QC_Sheet_Number,lig.Challan_No,lig.Estimated_Time,lig.Actual_Time,lig.Inspected_By from InspectionSheetSubmissionHistory lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rework_no= pgr.rwk_no where pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  order by lig.InspectionSheetSubmissionHistoryId desc";
            }
            SqlDataAdapter ad = new SqlDataAdapter(query, connection);
            DataTable dt = new DataTable();
            ad.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                dt = null;
            }
            GridView1.DataSource = dt;
            Session["DataTableAdvanceReport"] = dt;
            GridView1.DataBind();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        showgrid();
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        showgrid();
    }
     
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // Set the index of the new display page.  
        GridView1.PageIndex = e.NewPageIndex;
        showgrid();
    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }
}
