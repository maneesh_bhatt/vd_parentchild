﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text;
using System.Net.Mail;

public partial class Tool_VendorWisePendingItemReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ";
            string dataTextField = "VENDOR_NAME";
            string dataValueField = "ID";
            UtilityFunctions.bind_vendor(ddllocation, query, dataTextField, dataValueField);
        }
    }

    /// <summary>
    /// Retrives the Pending Items Report from Location Info Gr table
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            if (ddllocation.Text != "")
            {
                DataTable dt = new DataTable();
                SqlDataAdapter ad = new SqlDataAdapter("select Challan_No, Tool_No, Challan_Sent_Date,Challan_Type, Delivery_Commit_Date, Sum(Quant_Sent) as QtySent,Sum(Quant_Rec) as QtyReceived, Sum(PendingQty) as QtyPending "
                + " from(select lig.Challan_No,lig.Tool_No, lig.P_No, lig.Part_Name,lig.Station,lig.Rwk,Convert(Date,ph.Date_Of_Trans) as 'Challan_Sent_Date', ph.Challan_Type, "
                + " Convert(Date,tb.Delivery_Commit_Date) as Delivery_Commit_Date,lig.quant_sent,lig.quant_rec,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as PendingQty "
                + " ,lig.Location from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no "
                + "  and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no "
                + "  left join part_history_Gr ph on lig.tool_no=ph.tool_no and lig.p_no=ph.p_no "
                + "  and lig.part_name=ph.part_name and lig.station=ph.station and lig.rwk= ph.rwk_no and lig.location=ph.location and lig.Challan_No=ph.Challan_No "
                + "  outer apply  (select top 1 pg.Delivery_commit_date from part_history_GR pg where pg.tool_no=lig.tool_no and pg.p_no=lig.p_no and pg.part_name=lig.part_name and "
                + "  pg.station=lig.station and  pg.rwk_no= lig.Rwk and pg.sent_or_rec='Sent' and pg.location=lig.location order by pg.date_of_trans desc)tb "
                + "  where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  "
                + "  and lig.Location ='" + ddllocation.SelectedItem.Text + "'"
                + "  )dt group by Challan_No,Tool_No,Challan_Sent_Date,Challan_Type,Delivery_Commit_Date  order by " + ddlSortBy.SelectedValue.Replace("ASC", ddlASCorDESC.SelectedValue) + "", conn);

                ad.SelectCommand.CommandTimeout = 0;
                ad.Fill(dt);
                ViewState["CurrentGridOverview"] = dt;
                gridViewVendorPendingItems.DataSource = dt;
                gridViewVendorPendingItems.DataBind();
                gridViewVendorPendingItems.EmptyDataText = "No Records Found";
            }
            else
            {
                ViewState["CurrentGridOverview"] = null;
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }

    /// <summary>
    /// Exports the Data to Excel
    /// </summary>
    protected void ExportToExcel()
    {
        Response.ClearContent();
        string fileName = "VendorWiseData";
        string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gridViewVendorPendingItems.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    //protected void btnDownload_Click(object sender, EventArgs e)
    //{
    //    ExportToExcel();
    //}

    protected void gridViewVendorPendingItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }

    /// <summary>
    /// Maximes the Challan Item Grid When User clicks on Plus Icon
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Show_Hide_ChallanItemsGrid(object sender, EventArgs e)
    {
        try
        {
            ImageButton imgShowHide = (sender as ImageButton);
            GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);

            Panel panel = (Panel)imgShowHide.FindControl("pnlChallanItems");
            GridView gvChallanItems = (GridView)imgShowHide.FindControl("gvChallanItems");
            DateTime? deliveryCommitDate = null;
            if (imgShowHide.CommandArgument == "Show")
            {
                panel.Visible = true;
                imgShowHide.CommandArgument = "Hide";
                imgShowHide.ImageUrl = "~/images/minus.png";

                string challanNo = gridViewVendorPendingItems.Rows[row.RowIndex].Cells[1].Text;
                string toolNo = gridViewVendorPendingItems.Rows[row.RowIndex].Cells[2].Text;
                DateTime loadingDate = Convert.ToDateTime(gridViewVendorPendingItems.Rows[row.RowIndex].Cells[3].Text);
                if (gridViewVendorPendingItems.Rows[row.RowIndex].Cells[5].Text != "&nbsp;" && !string.IsNullOrEmpty(gridViewVendorPendingItems.Rows[row.RowIndex].Cells[5].Text))
                {
                    deliveryCommitDate = Convert.ToDateTime(gridViewVendorPendingItems.Rows[row.RowIndex].Cells[5].Text);
                }


                ShowChallanItemsGrid(challanNo, gvChallanItems, toolNo, loadingDate, deliveryCommitDate, ddllocation.SelectedItem.Text);
            }
            else
            {
                panel.Visible = false;
                imgShowHide.CommandArgument = "Show";
                imgShowHide.ImageUrl = "~/images/plus.png";
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Binds the Challan Grid
    /// </summary>
    /// <param name="challanNo"></param>
    /// <param name="gvChallanItems"></param>
    /// <param name="toolNo"></param>
    /// <param name="loadingDate"></param>
    /// <param name="deliveryCommitDate"></param>
    /// <param name="location"></param>
    protected void ShowChallanItemsGrid(string challanNo, GridView gvChallanItems, string toolNo, DateTime loadingDate, DateTime? deliveryCommitDate, string location)
    {
        try
        {

            string query = "select lig.Challan_No,lig.Tool_No, lig.P_No, lig.Part_Name,lig.Station,lig.Rwk,Convert(Date,ph.Date_Of_Trans) as 'Challan_Sent_Date', "
             + " ph.Challan_Type, Convert(Date,tb.Delivery_Commit_Date) as Delivery_Commit_Date,lig.Quant_Sent,lig.Quant_Rec, "
             + " isnull(lig.Quant_Sent,0)-isnull(lig.Quant_Rec,0) as PendingQty ,lig.Location from location_info_GR lig "
             + " left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no  and lig.part_name=pgr.part_name "
             + " and lig.station=pgr.station and lig.rwk= pgr.rwk_no  "
             + " left join part_history_Gr ph on lig.tool_no=ph.tool_no and lig.p_no=ph.p_no  and lig.part_name=ph.part_name and "
             + " lig.station=ph.station and lig.rwk= ph.rwk_no and lig.location=ph.location and lig.Challan_No=ph.Challan_No and ph.Sent_or_rec='Sent'"
             + " outer apply  (select top 1 pg.Delivery_commit_date from part_history_GR pg where pg.tool_no=lig.tool_no and pg.p_no=lig.p_no "
             + " and pg.part_name=lig.part_name and  pg.station=lig.station and  pg.rwk_no= lig.Rwk and pg.sent_or_rec='Sent' and "
             + " pg.location=lig.location order by pg.date_of_trans desc)tb  where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 "
             + " and pgr.Is_Active_Record=1  and ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and lig.Challan_No='" + challanNo + "' and lig.Tool_No='" + toolNo + "' ";
            if (deliveryCommitDate != null)
            {
                query = query + "  and Convert(Date,tb.Delivery_Commit_Date)='" + deliveryCommitDate.Value.ToString("yyyy/MM/dd") + "'";
            }
            query = query + " and Convert(Date,ph.Date_Of_Trans)='" + loadingDate.ToString("yyyy/MM/dd") + "'"
             + " order by lig.tool_no,lig.station,ph.Date_Of_Trans asc";
            DataTable dt = new DataTable();
            dt = UtilityFunctions.GetData(query);
            if (dt.Rows.Count > 0)
            {
                gvChallanItems.DataSource = dt;
                gvChallanItems.DataBind();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

    }

    protected void btnEmailOverviewReport_Click(object sender, EventArgs e)
    {
        SendEmail("OverView");
    }

    protected void btnEmailSummaryReport_Click(object sender, EventArgs e)
    {
        SendEmail("Summary");
    }

    /// <summary>
    /// Sends the Report on Email to the Vendor on the Basis of Clicked Button
    /// </summary>
    /// <param name="reportType"></param>
    protected void SendEmail(string reportType)
    {
        try
        {
            DataTable dt = new DataTable();
            if (reportType == "OverView" && ViewState["CurrentGridOverview"] != null)
            {
                dt = (DataTable)ViewState["CurrentGridOverview"];
            }
            else if (reportType == "Summary" && gridViewVendorPendingItems.Rows.Count > 0)
            {
                string query = "select lig.Challan_No,lig.Tool_No, lig.P_No, lig.Part_Name,lig.Station,lig.Rwk,Convert(Date,ph.Date_Of_Trans) as 'Challan_Sent_Date', ph.Challan_Type, "
                + " Convert(Date,tb.Delivery_Commit_Date) as Delivery_Commit_Date,lig.Quant_Sent,lig.Quant_Rec,isnull(lig.Quant_Sent,0)-isnull(lig.Quant_Rec,0) as PendingQty  "
                + " from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no  "
                + " and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no  "
                + " left join part_history_Gr ph on lig.tool_no=ph.tool_no and lig.p_no=ph.p_no "
                + " and lig.part_name=ph.part_name and lig.station=ph.station and lig.rwk= ph.rwk_no and ph.location=lig.location and ph.Challan_No=lig.Challan_No and ph.sent_or_rec='Sent'"
                + " outer apply  (select top 1 pg.Delivery_commit_date from part_history_GR pg where pg.tool_no=lig.tool_no and pg.p_no=lig.p_no and pg.part_name=lig.part_name and  "
                + " pg.station=lig.station and  pg.rwk_no= lig.Rwk and pg.sent_or_rec='Sent' and pg.location=lig.location order by pg.date_of_trans desc)tb  "
                + " where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) and lig.location='" + ddllocation.SelectedItem.Text + "'"
                + " order by lig.tool_no,lig.station,ph.Date_Of_Trans asc";
                dt = UtilityFunctions.GetData(query);
            }
            if (dt.Rows.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<table style='    border: 1px solid #000;width: 1000px;border-collapse: collapse;font-family: Verdana;font-size: 13.5px;text-align: center;line-height: 26px;'><tr>");
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    sb.Append("<td style='border:1px solid #000;color:black;'><b>" + dt.Columns[i].ColumnName + "</b></td>");
                }
                sb.Append("<td style='border:1px solid #000;color:black;'><b>New Commitment Date</b></td>");
                sb.Append("</tr>");
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    sb.Append("<tr>");
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        sb.Append("<td style='border:1px solid #000;color:black;'>");
                        if (dt.Columns[k].ColumnName == "Loading_Date" || dt.Columns[k].ColumnName == "Delivery_Commit_Date")
                        {
                            if (dt.Rows[j][k] != DBNull.Value)
                                sb.Append(Convert.ToDateTime(dt.Rows[j][k]).ToString("dd-MM-yyyy"));
                        }
                        else
                        {
                            sb.Append(dt.Rows[j][k]);
                        }
                        sb.Append("</td>");
                    }
                    sb.Append("<td style='border:1px solid #000;color:black;'></td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
                string html = sb.ToString();

                string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
                using (SqlConnection connec = new SqlConnection(sqlconnstring))
                {
                    connec.Open();
                    using (SqlCommand cmd = new SqlCommand("select * from Vendor_Master_Job_Work where Vendor_Name='" + ddllocation.SelectedItem.Text + "'", connec))
                    {
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                rdr.Read();
                                string toEmail = Convert.ToString(rdr["Email"]);
                                SendEmail(toEmail, html, reportType);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Email ID for selected Vendor is not Configured.');", true);
                            }
                        }
                    }

                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('No Data Found.');", true);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occcured while sending Email, Please contact Administrator.');", true);
        }
    }



    protected void SendEmail(string toEmailAddress, string html, string reportType)
    {
        try
        {
            if (!string.IsNullOrEmpty(toEmailAddress) && !string.IsNullOrEmpty(html))
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                mail.From = new MailAddress("reminder@falconautoonline.com");
                mail.To.Add(toEmailAddress);
                mail.CC.Add("falcon_ppc@falconautoonline.com");
                mail.CC.Add("falcon_wks@falconautoonline.com");
                mail.CC.Add("Kunal.dhiman@falconautoonline.com");
                mail.CC.Add("lalit.kumar@falconautoonline.com");
                mail.CC.Add("amits@falconautoonline.com");
                mail.CC.Add("maneesh@falconautoonline.com");
                if (reportType == "OverView")
                {
                    mail.Subject = "Pending Items Challan Level Summary";
                    mail.Body = "Dear Sir,    <br /><br />  Following challans are outstanding at your end. Please take note of the dates committed by you. Please share revised expected delivery dates in case you would not be able to meet the current committed dates.<br /><br />";
                }
                else if (reportType == "Summary")
                {
                    mail.Subject = "Pending Items Part Level Summary";
                    mail.Body = "Dear Sir,    <br /><br />  Following items are  outstanding at your end. Please take note of the dates committed by you. Please share revised expected delivery dates in case you would not be able to meet the current committed dates.<br /><br />";
                }
                mail.Body += html;
                mail.IsBodyHtml = true;
                SmtpServer.EnableSsl = true;
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("reminder@falconautoonline.com", "Falcon@FAPL@2016");
                SmtpServer.Send(mail);
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Email Successfully Sent to Vendor.');", true);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occcured while sending Email, Please contact Administrator.');", true);
        }

    }

    /// <summary>
    /// Expands all the Plus Icons on this CheckBox Click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void radioBtnExpandAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //
            //GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);


            //GridView gvChallanItems = (GridView)imgShowHide.FindControl("gvChallanItems");
            DataTable dt = (DataTable)ViewState["CurrentGridOverview"];
            foreach (GridViewRow gr in gridViewVendorPendingItems.Rows)
            {
                Panel panel = (Panel)gridViewVendorPendingItems.Rows[gr.RowIndex].FindControl("pnlChallanItems");
                ImageButton imgShowHide = (ImageButton)gridViewVendorPendingItems.Rows[gr.RowIndex].FindControl("imgChallanItems");
                DateTime? deliveryCommitDate = null;
                if (radioBtnExpandAll.Checked)
                {
                    panel.Visible = true;
                    imgShowHide.CommandArgument = "Hide";
                    imgShowHide.ImageUrl = "~/images/minus.png";

                    string challanNo = gridViewVendorPendingItems.Rows[gr.RowIndex].Cells[1].Text;
                    string toolNo = gridViewVendorPendingItems.Rows[gr.RowIndex].Cells[2].Text;
                    DateTime loadingDate = Convert.ToDateTime(gridViewVendorPendingItems.Rows[gr.RowIndex].Cells[3].Text);
                    if (gridViewVendorPendingItems.Rows[gr.RowIndex].Cells[5].Text != "&nbsp;" && !string.IsNullOrEmpty(gridViewVendorPendingItems.Rows[gr.RowIndex].Cells[5].Text))
                    {
                        deliveryCommitDate = Convert.ToDateTime(gridViewVendorPendingItems.Rows[gr.RowIndex].Cells[5].Text);
                    }
                    GridView gvChallanItems = (GridView)gridViewVendorPendingItems.Rows[gr.RowIndex].FindControl("gvChallanItems");

                    ShowChallanItemsGrid(challanNo, gvChallanItems, toolNo, loadingDate, deliveryCommitDate, ddllocation.SelectedItem.Text);
                }
                else
                {
                    panel.Visible = false;
                    imgShowHide.CommandArgument = "Show";
                    imgShowHide.ImageUrl = "~/images/plus.png";
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }
}