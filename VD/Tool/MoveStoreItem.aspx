﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MoveStoreItem.aspx.cs" MasterPageFile="~/Pages/MasterPage.master"
    Inherits="MoveStoreItem" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../Scripts/ScrollableGridViewPlugin_ASP.NetAJAXmin.js" type="text/javascript"></script>
    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate >= new Date()) {
                alert("You cannot select a day less than today!");
                sender._selectedDate = new Date();

                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>
    <script type="text/javascript">


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


        var TL;
        var TB;
        var TH;
        var TWt;
        var TAWB;
        var TMID;
        var TimeElapsed;


        function myFunctionL(e) {

            debugger;

            TL = e.timeStamp;
            document.getElementById("tbTool").value = TL;
            debugger;
        }

       
    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>
    <%-- <style type="text/css">
        body
        {
            overflow: hidden;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <table style="margin-left: 0px; width: 100%;" align="left">
        <tr>
            <td>
            </td>
            <td colspan="2" align="center" style="font-size: x-large; color: black">
                <span class="style1">Move From Store</span>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="Panel3" runat="server">
                    <table>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <table width="600px">
                        <tr>
                            <td>
                            </td>
                            <td style="font-size: 13px;">
                                Date: <span class="style12">* </span>
                                <br />
                                <asp:TextBox ID="txtpodate" runat="server" CssClass="glowing-border" Width="100px"
                                    AutoPostBack="true" OnTextChanged="txtpodate_TextChanged"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpodate"
                                    Display="None" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <cc1:CalendarExtender ID="txtpodate_CalendarExtender" runat="server" Enabled="true"
                                    Format="dd-MMM-yyyy" OnClientDateSelectionChanged="checkDate" TargetControlID="txtpodate"
                                    OnClientShowing="CurrentDateShowing">
                                </cc1:CalendarExtender>
                            </td>
                            <td colspan="3" style="font-size: 13px;">
                                Project No<span class="style12"></span>
                                <br />
                                <asp:TextBox ID="txtprojectno" CssClass="glowing-border" runat="server" Width="100px"></asp:TextBox>
                                <br />
                            </td>
                            <td colspan="3" style="font-size: 13px;">
                                Station No<span class="style12"></span>
                                <br />
                                <asp:TextBox ID="txtstation" CssClass="glowing-border" runat="server" Width="100px"></asp:TextBox>
                                <br />
                            </td>
                            <td colspan="3" style="font-size: 13px;">
                                Department<span class="style12"></span>
                                <br />
                                <asp:DropDownList ID="ddldepartment" CssClass="glowing-border" BackColor="#F6F1DB"
                                    ForeColor="#7d6754" runat="server">
                                    <asp:ListItem></asp:ListItem>
                                    <asp:ListItem Value="DIRECTASSEMBLY" Text="DirectAssembly"></asp:ListItem>
                                    <asp:ListItem Value="VD Process Store" Text="VD Process Store"></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                            </td>
                            <td style="margin-top: 100px;" valign="middle">
                                &nbsp;
                                <asp:Button ID="Button1" CssClass="glowing-border" CausesValidation="false" runat="server"
                                    Text="Search" OnClick="Button1_Click" />
                            </td>
                            <td width="100px">
                                <asp:ImageButton ID="ImageButton1" Visible="true" CausesValidation="false" ImageUrl="~/images/excel.png"
                                    runat="server" OnClick="ImageButton1_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td height="5px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td height="5px">
                                <asp:Label ID="lbl" Visible="false" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="margin-left: 100px;" align="left">
                <td style="margin-left: 100px;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server">
                                <asp:GridView ID="GridView1" runat="server" EmptyDataText="No Record Found" AutoGenerateColumns="False"
                                    Style="border: 1px solid #aba4a4;" CellPadding="4" ForeColor="#333333" GridLines="None"
                                    OnPageIndexChanging="GridView1_PageIndexChanging" Font-Size="13px" Width="100%"
                                    OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing">
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAll1" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll1_CheckedChanged1" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkAllchild" AutoPostBack="true" runat="server" OnCheckedChanged="chkAllchild_CheckedChanged">
                                                </asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Project No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblproject" runat="server" Text='<%# Eval("tool_no") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="20px" ItemStyle-Width="50px"
                                            ItemStyle-HorizontalAlign="left">
                                            <HeaderTemplate>
                                                Station No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label10" runat="server" Text='<%# Eval("station") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Position No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("p_no") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                            ItemStyle-HorizontalAlign="left">
                                            <HeaderTemplate>
                                                Batch No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblbatchno" runat="server" Text='<%# Eval("Batch_No") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="40px" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                GR No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label111" Visible="false" runat="server" Text='<%# Eval("Rwk") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="left" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="left">
                                            <HeaderTemplate>
                                                Part Name
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("part_name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:TemplateField HeaderStyle-HorizontalAlign="Left" Visible="false" HeaderStyle-Width="100px"
                                            ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Location
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("location") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <%-- <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Total Quantity
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" Text='<%# Eval("quant_sent") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Total Quantity
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" Text='<%# Eval("quant_sent") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" Visible="false" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Quantity sent
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Labelsent" Visible="false" runat="server" Text='<%# Eval("quant_sent") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Qty To Be send
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox ID="TextBox3" CssClass="glowing-border" runat="server" OnTextChanged="TextBox3_TextChanged"
                                                    AutoPostBack="true" Enabled="false" Width="100px" onkeypress="return isNumberKey(event)"
                                                    Text='<%# Eval("quant_sent") %>'> 
                                                </asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Upload Qty
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="LabelUpload_Qty" Visible="true" runat="server" Text='<%# Eval("Upload_Qty") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                </asp:GridView>
                            </asp:Panel>
                            </td> </td> </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="margin-top: 50px;" align="left" colspan="4">
                                    <asp:Button ID="btnSend" runat="server" CausesValidation="true" Text="Send" Visible="false"
                                        Width="100px" OnClick="btnSend_Click" Style="height: 26px" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnprint" runat="server" Text="PrintChallan" Visible="False" />
                                </td>
                            </tr>
                        </ContentTemplate>
                    </asp:UpdatePanel>
    </table>
</asp:Content>
