﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintPendingItemsVendorVisitReport.aspx.cs"
    Inherits="PrintPendingItemsVendorVisitReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Print Vendor Visit Report</title>
    
    <style type="text/css">
        body, html, table
        {
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
        }
        table 
        {
             margin: 0;
            padding: 0;
        }
        td
        {
            overflow:hidden;
            padding:0px;
            margin:0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div runat="server" id="panelVendorVisitReport">
    </div>
    </form>
</body>
</html>
