﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using System.Text;

public partial class Tool_WeldAssemblyConsumedItemSummary : System.Web.UI.Page
{
    public static string searchCondition;
    protected void Page_Load(object sender, EventArgs e)
    {
        searchCondition = "";

        try
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Binds the Data with the Grid View which is there is Weld Parts Consumed Location
    /// </summary>
    protected void BindGrid()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            string searchCondition = "";
          
            if (inputToolNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.tool_no='" + inputToolNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.tool_no='" + inputToolNo.Text + "'";
                }
            }
            if (inputStation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.station='" + inputStation.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.station='" + inputStation.Text + "'";
                }
            }
            if (inputPNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.p_no='" + inputPNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.p_no='" + inputPNo.Text + "'";
                }
            }
            if (inputReworkNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.rwk='" + inputReworkNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.rwk='" + inputReworkNo.Text + "'";
                }
            }
            SqlDataAdapter adapter = new SqlDataAdapter();
            if (!string.IsNullOrEmpty(searchCondition))
            {
                adapter = new SqlDataAdapter("select lig.tool_no,lig.station,lig.Upload_Qty,lig.p_no,lig.part_name,lig.quant_sent,lig.quant_rec,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as SentQty,lig.Rwk,pgr.opr17 from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) and lig.location='Weld Parts Consumed' and " + searchCondition + " order by lig.tool_no,lig.station asc", con);
            }
            else
            {
                adapter = new SqlDataAdapter("select lig.tool_no,lig.station,lig.Upload_Qty,lig.p_no,lig.part_name,lig.quant_sent,lig.quant_rec,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as SentQty,lig.Rwk,pgr.opr17 from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) and lig.location='Weld Parts Consumed' order by lig.tool_no,lig.station asc", con);
            }

            DataTable td = new DataTable();
            adapter.Fill(td);
            gridWeldPartConsumed.DataSource = td;
            gridWeldPartConsumed.DataBind();
            gridWeldPartConsumed.EmptyDataText = "No Record Found";
            lblRecorrdsCount.Text = Convert.ToString(td.Rows.Count) + " " + "Records In Total";
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }
    }

    protected void gridWeldPartConsumed_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // Set the index of the new display page.  
        gridWeldPartConsumed.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void gridWeldPartConsumed_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            Response.Redirect("Vendor_Master.aspx?ID=" + e.CommandArgument);
        }
    }

    /// <summary>
    /// Display the History of positions on Position Number click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lblPositionNumber_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        try
        {
            string query = "select  id,location as Party,date_of_trans as Trans_Date,sent_or_rec as Trans_Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as Pending_Qty_PPC,TransSentQty as Send_Or_Rec_Qty,User_Name  from part_history_GR  where   p_no='" + Positionno + "' and  tool_no='" + toolno + "' and  station='" + station + "' and  part_name='" + part_name + "' and rwk_no='" + rwk_no + "' order by date_of_trans asc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();

            da.Fill(table);

            pnlHistoryPopUp.InnerHtml = GetHtmlContent(table);
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
            updateGrid.Update();
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    protected string GetHtmlContent(DataTable table)
    {
        StringBuilder b = new StringBuilder();
        b.Append("<table style='");
        b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
        b.Append("<tr>");
        foreach (DataColumn column in table.Columns)
        {
            b.Append("<th>");
            b.Append(column.ColumnName);
            b.Append("</th>");
        }
        b.Append("</tr>");


        foreach (DataRow row in table.Rows)
        {
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<td>");
                b.Append(row[column.ColumnName]);
                b.Append("</td>");
            }
            b.Append("</tr>");
        }
        b.Append("</table>");
        return b.ToString();
    }

    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }
}

