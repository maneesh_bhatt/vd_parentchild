﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;

public partial class Tool_EmailConfigurationAdmin : System.Web.UI.Page
{
    string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
    public static string searchCondition;
    protected void Page_Load(object sender, EventArgs e)
    {
        searchCondition = "";
        try
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Retrieves the data from Email Cofiguration table.
    /// </summary>
    protected void BindGrid()
    {
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            if (Convert.ToString(ViewState["SearchCondition"]) != "")
            {
                adapter = new SqlDataAdapter("select emc.Email_Configuration_Id, elm.Email_Level, elm.Email_Send_After, emc.Email_Incharge,emc.Email_Subject,emc.Email_To,emc.Email_CC,elm.Category from Email_Configuration emc left join Email_Level_Master elm on emc.Email_Level_Id= elm.Email_Level_Id where " + ViewState["SearchCondition"], con);
            }
            else
            {
                adapter = new SqlDataAdapter("select emc.Email_Configuration_Id,elm.Email_Level, elm.Email_Send_After, emc.Email_Incharge,emc.Email_Subject,emc.Email_To,emc.Email_CC,elm.Category from Email_Configuration emc left join Email_Level_Master elm on emc.Email_Level_Id= elm.Email_Level_Id", con);
            }

            DataTable td = new DataTable();
            adapter.Fill(td);
            gridEmailConfiguration.DataSource = td;
            gridEmailConfiguration.DataBind();
            gridEmailConfiguration.EmptyDataText = "No Record Found";
            lblRecorrdsCount.Text = Convert.ToString(td.Rows.Count) + " " + "Records In Total";
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }

    }

    protected void gridEmailConfiguration_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // Set the index of the new display page.  
        gridEmailConfiguration.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //gridPackages.PageIndex = 1;
        searchCondition = "";
        if (inputCategory.Text != "")
        {
            searchCondition = "elm.Category ='" + inputCategory.Text + "'";
        }

        ViewState["SearchCondition"] = searchCondition;
        BindGrid();
    }

    /// <summary>
    /// Event is called when user clicks on edit or delete button shown in for each row.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridEmailConfiguration_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            Response.Redirect("EmailConfigurationForm.aspx?ID=" + e.CommandArgument);
        }
        else if (e.CommandName == "Delete")
        {
            SqlConnection connec = new SqlConnection(sqlconnstring);
            try
            {
                connec.Open();
                SqlCommand cmd = new SqlCommand("delete from Email_Configuration where Email_Configuration_Id=@EmailConfigurationId", connec);
                cmd.Parameters.AddWithValue("@EmailConfigurationId", e.CommandArgument);
                cmd.ExecuteNonQuery();
                BindGrid();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }


    protected void gridEmailConfiguration_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    /// <summary>
    /// Retrieves rhe List of Vendors from Vendor Master Job Work Table
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetVendorName(string text)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            List<string> allItems = new List<string>();
            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrWhiteSpace(text))
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work where Vendor_Name like '" + text + "%' and Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ", con);
            }
            else
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work where Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ", con);
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string vendorName = Convert.ToString(reader["Vendor_Name"]);
                allItems.Add(vendorName);
            }
            reader.Close();
            return allItems;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            con.Close();
        }
    }

}

