﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintVendorStatusReport.aspx.cs"
    Inherits="PrintVendorStatusReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Print Vendor Status Report</title>
    <style type="text/css">
        body, html, table
        {
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
        }
        table
        {
            margin: 0;
            padding: 0;
        }
        td
        {
            overflow: hidden;
            padding: 0px;
            margin: 0px;
        }
        
        table.report-container
        {
            page-break-after: always;
        }
        thead.report-header
        {
            display: table-header-group;
        }
        tfoot.report-footer
        {
            display: table-footer-group;
        }

        @media print
        {
            thead
            {
                display: table-header-group;
            }
            table.report-container
            {
                page-break-after: always;
            }
            thead.report-header
            {
                display: table-header-group;
            }
            tfoot.report-footer
            {
                display: table-footer-group;
            }
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div runat="server" id="divVendorStatusReport">
    </div>
    </form>
</body>
</html>
