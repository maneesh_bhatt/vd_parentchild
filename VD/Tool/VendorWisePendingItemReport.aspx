﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="VendorWisePendingItemReport.aspx.cs" Inherits="Tool_VendorWisePendingItemReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../Scripts/ScrollableGridViewPlugin_ASP.NetAJAXmin.js" type="text/javascript"></script>
    <title></title>
    <style type="text/css">
        .Grid
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .Grid td
        {
            background-color: Highlight;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Grid th
        {
            background-color: Navy;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .ChildGrid td
        {
            background-color: #eee !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .ChildGrid th
        {
            background-color: #6C6C6C !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid td
        {
            background-color: #fff !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid th
        {
            background-color: #2B579A !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        
        .style2
        {
            border: none;
            width: 100px;
        }
        
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="width: 100%; border: 1px solid Blue; min-height: 100px;">
        <div style="padding: 9px;">
            <div>
                <div align="center">
                    <span class="style13">Vendor Wise Pending Item Report</span>
                </div>
            </div>
            <div style="margin-bottom: 10px; margin-top: 20px;">
                <div style="font-size: 13px; width: 145px; float: left;">
                    Location <span style="color: Red;">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddllocation"
                        ErrorMessage="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <asp:DropDownList ID="ddllocation" CssClass="glowing-border" AppendDataBoundItems="true"
                        BackColor="#F6F1DB" ForeColor="#7d6754" Width="140px" runat="server">
                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="font-size: 13px; width: 205px; float: left;">
                    Sort By <span style="color: Red;">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlSortBy"
                        ErrorMessage="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <asp:DropDownList ID="ddlSortBy" CssClass="glowing-border" AppendDataBoundItems="true"
                        BackColor="#F6F1DB" ForeColor="#7d6754" Width="140px" runat="server">
                        <asp:ListItem Text="Challan Sent Date" Value="Challan_Sent_Date ASC"></asp:ListItem>
                        <asp:ListItem Text="Expected Delivery Date" Value="Delivery_Commit_Date ASC"></asp:ListItem>
                        <asp:ListItem Text="Tool No & LChallan Sent Date" Value="Tool_No ASC, Challan_Sent_Date ASC"></asp:ListItem>
                        <asp:ListItem Text="Tool No & Expected Delivery Date" Value="Tool_No ASC, Delivery_Commit_Date ASC"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlASCorDESC" CssClass="glowing-border" AppendDataBoundItems="true"
                        BackColor="#F6F1DB" ForeColor="#7d6754" Width="50px" runat="server">
                        <asp:ListItem Text="ASC" Value="ASC"></asp:ListItem>
                        <asp:ListItem Text="DESC" Value="DESC"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/images/searchbtn.jpg"
                    OnClick="btnSearch_Click" Style="margin-top: 10px;" />
                <asp:CheckBox runat="server" ID="radioBtnExpandAll" OnCheckedChanged="radioBtnExpandAll_CheckedChanged"
                    AutoPostBack="true" />Expand All
                <%--  <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="btnDownload_Click"
                    Visible="false" Style="margin-top: 9px; margin-right: -4px; float: right; background-color: green;
                    color: white; padding: 6px; border: 1px solid green; box-shadow: 1px 1px 3px green;" />--%>
                <div style="float: right;">
                    <asp:Button ID="btnEmailOverviewReport" Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px;
                        color: white; height: 32px; margin-top: 5px; background-color: Green; cursor: pointer;"
                        runat="server" EnableViewState="true" CausesValidation="false" Text="Email Overview Report"
                        OnClick="btnEmailOverviewReport_Click" />
                    <asp:Button ID="btnEmailSummaryReport" Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px;
                        color: white; height: 32px; margin-top: 5px; background-color: Green; cursor: pointer;"
                        runat="server" EnableViewState="true" CausesValidation="false" Text="Email Summary Report"
                        OnClick="btnEmailSummaryReport_Click" />
                </div>
            </div>
            <asp:Panel runat="server" Style="overflow: scroll;">
                <asp:GridView runat="server" ID="gridViewVendorPendingItems" AutoGenerateColumns="false"
                    Width="100%" ShowHeaderWhenEmpty="true" OnRowDataBound="gridViewVendorPendingItems_RowDataBound"
                    AllowPaging="false" AllowSorting="false" Style="font-size: 13px; font-family: Verdana;
                    line-height: 26px;">
                    <RowStyle BackColor="White" ForeColor="Black" />
                    <Columns>
                        <asp:TemplateField HeaderStyle-BackColor="#335599">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgChallanItems" runat="server" OnClick="Show_Hide_ChallanItemsGrid"
                                    ImageUrl="~/images/plus.png" CommandArgument="Show" />
                                <asp:Panel ID="pnlChallanItems" runat="server" Visible="false" Style="position: relative">
                                    <asp:GridView ID="gvChallanItems" runat="server" AutoGenerateColumns="false" CssClass="Nested_ChildGrid">
                                        <Columns>
                                            <asp:BoundField DataField="Challan_No" HeaderText="Challan No" HeaderStyle-BackColor="#335599"
                                                HeaderStyle-Width="160px" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Tool_No" HeaderText="Tool No" HeaderStyle-BackColor="#335599"
                                                HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="P_No" HeaderText="P_ No" HeaderStyle-BackColor="#335599"
                                                HeaderStyle-Width="220px" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Part_Name" HeaderText="Part Name" HeaderStyle-BackColor="#335599"
                                                HeaderStyle-Width="220px" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Station" HeaderText="Station" HeaderStyle-BackColor="#335599"
                                                HeaderStyle-Width="220px" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Rwk" HeaderText="Rwk" HeaderStyle-BackColor="#335599"
                                                HeaderStyle-Width="220px" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Challan_Sent_Date" DataFormatString="{0:dd-MM-yyyy}" HeaderText="Challan Sent Date"
                                                HeaderStyle-BackColor="#335599" HeaderStyle-Width="220px" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Challan_Type" HeaderText="Challan Type" HeaderStyle-BackColor="#335599"
                                                HeaderStyle-Width="220px" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Delivery_Commit_Date" DataFormatString="{0:dd-MM-yyyy}"
                                                HeaderText="Last Expected Delivery Date" HeaderStyle-BackColor="#335599" HeaderStyle-Width="220px"
                                                HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Quant_Sent" HeaderText="Qty Sent" HeaderStyle-BackColor="#335599"
                                                HeaderStyle-Width="150px" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Quant_Rec" HeaderText="Qty Received" HeaderStyle-BackColor="#335599"
                                                HeaderStyle-Width="150px" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="PendingQty" HeaderText="Qty Pending" HeaderStyle-BackColor="#335599"
                                                HeaderStyle-Width="150px" HeaderStyle-ForeColor="White" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Challan_No" HeaderText="Challan No" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="160px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Tool_No" HeaderText="Tool No" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Challan_Sent_Date" DataFormatString="{0:dd-MM-yyyy}" HeaderText="Challan Sent Date"
                            HeaderStyle-BackColor="#335599" HeaderStyle-Width="220px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Challan_Type" HeaderText="Challan Type" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="220px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Delivery_Commit_Date" DataFormatString="{0:dd-MM-yyyy}"
                            HeaderText="Last Expected Delivery Date" HeaderStyle-BackColor="#335599" HeaderStyle-Width="220px"
                            HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="QtySent" HeaderText="Qty Sent" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="150px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="QtyReceived" HeaderText="Qty Received" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="150px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="QtyPending" HeaderText="Qty Pending" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="150px" HeaderStyle-ForeColor="White" />
                    </Columns>
                </asp:GridView>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
