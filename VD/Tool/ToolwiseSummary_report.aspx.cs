﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class ToolwiseSummary_report : System.Web.UI.Page
{
    string station = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            showgrid();
        }
    }

    /// <summary>
    /// Clicking on Plus Icon shown at Third Level it displays Position Summary for Clicked Row  (Station and Challan No)
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Show_Hide_OrdersGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlOrders").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            GridView gvOrders = row.FindControl("gvOrders") as GridView;

            string customerId = gvOrders.DataKeys[row.RowIndex].Value.ToString();
            string cell_1_Value = gvOrders.Rows[row.RowIndex].Cells[5].Text;
            string station = gvOrders.Rows[row.RowIndex].Cells[2].Text;
            if (tbStation.Text.Length > 0)
            {
                station = tbStation.Text;
            }
            else
            {
                station = "";
            }
            BindOrders(gvOrders, cell_1_Value, station);
        }
        else
        {
            row.FindControl("pnlOrders").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    private void BindOrders(GridView gvOrders, string challan, string station)
    {
        DataTable dt = new DataTable();
        dt = UtilityFunctions.GetData("SELECT distinct       [p_no]     ,[part_name]    ,[station]    ,[location]    ,[QtySent] as qtypending   ,[Sent_Date]   ,[Challan_No]   ,Delivery_commit_date    ,Required_date  FROM  [dbo].[part_history_GR] where Challan_No='" + challan + "' and station='" + station + "' and QtySent>0");
        gvOrders.DataSource = dt;
        Session["BindOrders"] = dt;
        gvOrders.DataBind();
    }

    private void Bindchallan(string tool_no, GridView gvChallan, string location, string station)
    {
        gvChallan.ToolTip = tool_no;
        DataTable dt = new DataTable();
        string searchQueryLocationInfo = "";
        string searchQueryPartList = "";
        SearchQuery(out searchQueryPartList, "phg.", out searchQueryLocationInfo, "lig.");

        if (string.IsNullOrEmpty(searchQueryLocationInfo) && string.IsNullOrEmpty(searchQueryPartList))
        {
            searchQueryPartList = "1=1";
            searchQueryLocationInfo = "1=1";
        }
        dt = UtilityFunctions.GetData(string.Format("select distinct lig.tool_no, lig.part_name, lig.p_no,lig.station, lig.Rwk,lig.location, isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0) as Totalpendingqty, isnull(lig.Challan_No,'No Challan') as Challan_No, lig.Sent_Received_Date from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0)>0 and lig.tool_no='" + tool_no + "' and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchQueryLocationInfo + ""
             + " union all select phg.tool_no, phg.part_name, phg.p_no, phg.station,  phg.Rwk_No, 'Company- ItemWaiting' as Location, (isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0)) as Totalpendingqty, isnull(phg.Challan_No,'No Challan') as Challan_No,''  as Sent_Received_Date from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where   phg.location='COMPANY' and phg.date_of_trans is not null and (phg.Sent_Date is null  and  phg.Received_Date is null) and phg.QtySent is null and phg.quantity>0 AND phg.location='COMPANY' and phg.tool_no='" + tool_no + "' and  pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchQueryPartList + ""
            + " union all select phg.tool_no, phg.part_name, phg.p_no, phg.station,  phg.Rwk_No, phg.location, (isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0)) as Totalpendingqty, isnull(phg.Challan_No,'No Challan') as Challan_No,''  as Sent_Received_Date from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where   phg.location='COMPANY' and phg.date_of_trans is not null and phg.location='COMPANY' and phg.quantity>0 and (isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0))>0  and (phg.Sent_Date is not null and phg.Received_Date is not null) and phg.tool_no='" + tool_no + "' and  pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchQueryPartList + ""));

        var result = dt.AsEnumerable()
                     .Select(p => new
                     {
                         toolNo = p.Field<int>("Tool_No"),
                         challanNo = p.Field<string>("Challan_No"),
                         //sentReceivedDate = p.Field<DateTime>("Sent_Received_Date"),
                         location = p.Field<string>("Location"),
                         totalPendingQty = p.Field<int>("Totalpendingqty")
                     })
                     .GroupBy(p => new { p.toolNo, p.challanNo, p.location }) //p.toolNo,  p.sentReceivedDate,
                     .Select(p => new { Tool_No = p.Key.toolNo, Challan_No = p.Key.challanNo, location = p.Key.location, qtypending = p.Sum(x => Convert.ToInt32(x.totalPendingQty)) }) // Tool_No = p.Key.toolNo, , Sent_Received_Date = p.Key.sentReceivedDate
                     .ToList();

        gvChallan.DataSource = result;
        Session["BindOrders"] = result;
        gvChallan.DataBind();
        gvChallan.Columns[4].Visible = false;
    }

    protected void OnOrdersGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvOrders = (sender as GridView);
        gvOrders.PageIndex = e.NewPageIndex;
        DataTable dt = (DataTable)Session["BindOrders"];
        gvOrders.DataSource = dt;
        gvOrders.DataBind();
    }

    /// <summary>
    /// Clicking on Plus Icon at fourth level displays the Location, Transaction Date, Status and Total Pending Qty at PPC, Sent/Received Qty
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Show_Hide_ProductsGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        GridViewRow Gv2Row = (GridViewRow)((ImageButton)sender).NamingContainer;
        GridView Childgrid = (GridView)(Gv2Row.Parent.Parent);
        GridViewRow Gv1Row = (GridViewRow)(Childgrid.NamingContainer);
        int b = Gv1Row.RowIndex;


        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlProducts").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            GridView gvProducts = row.FindControl("gvProducts") as GridView;

            string Position_no = Childgrid.Rows[row.RowIndex].Cells[1].Text;
            string partname = Childgrid.Rows[row.RowIndex].Cells[2].Text;
            string vendor = Childgrid.Rows[row.RowIndex].Cells[3].Text;
            string station = Childgrid.Rows[row.RowIndex].Cells[4].Text;
            string Challan = Childgrid.Rows[row.RowIndex].Cells[8].Text;
            BindProducts(Position_no, gvProducts, partname, vendor, station, Challan);
        }
        else
        {
            row.FindControl("pnlProducts").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    private void Bindstation(string tool, GridView gvProducts, string location, string station, string challan)
    {
        string searchQueryLocationInfo = "";
        string searchQueryPartList = "";
        SearchQuery(out searchQueryPartList, "", out searchQueryLocationInfo, "");

        if (challan == "No Challan")
        {
            if (searchQueryLocationInfo != "")
            {
                gvProducts.DataSource = UtilityFunctions.GetData(string.Format("select count(*) as 'TotNoPosition' ,sum(temp2.TotPendingQty) as TotPendingQty,Station,'No Challan' as Challan_No,Location,Tool_no from (SELECT  [station],tool_no  ,[p_no],[part_name],[Rwk], (isnull(quant_sent,0)-isnull(quant_rec,0) )as TotPendingQty, location  FROM  [dbo].[location_info_GR] where location='" + location + "' and tool_no='" + tool + "' and (isnull(quant_sent,0)-isnull(quant_rec,0) )>0  and " + searchQueryLocationInfo + " )temp2 group by temp2.station,temp2.location,temp2.tool_no having temp2.location='" + location + "' and temp2.tool_no='" + tool + "'"));
            }
            else
            {
                gvProducts.DataSource = UtilityFunctions.GetData(string.Format("select count(*) as 'TotNoPosition' ,sum(temp2.TotPendingQty) as TotPendingQty,Station,'No Challan' as Challan_No,Location,Tool_no from (SELECT  [station],tool_no  ,[p_no],[part_name],[Rwk], (isnull(quant_sent,0)-isnull(quant_rec,0) )as TotPendingQty, location  FROM  [dbo].[location_info_GR] where location='" + location + "' and tool_no='" + tool + "' and (isnull(quant_sent,0)-isnull(quant_rec,0) )>0)temp2 group by temp2.station,temp2.location,temp2.tool_no having temp2.location='" + location + "' and temp2.tool_no='" + tool + "'"));
            }
        }
        else
        {
            if (searchQueryLocationInfo != "")
            {
                gvProducts.DataSource = UtilityFunctions.GetData(string.Format("select sum(Pendingqty) as TotPendingQty,Station, Challan_No,Count(temp.count1) as TotNoPosition,Location,Tool_no from (select lio.station,lio.p_no as count1,lio.Challan_No,(lio.quant_sent-lio.quant_rec) as pendingqty,location,lio.tool_no FROM  [dbo].[location_info_GR] as lio where lio.Challan_No='" + challan + "' and lio.tool_no='" + tool + "' and  (lio.quant_sent-lio.quant_rec)>0 and " + searchQueryLocationInfo + ") temp  group by Station, Challan_No ,Location,Tool_no"));
            }
            else
            {
                gvProducts.DataSource = UtilityFunctions.GetData(string.Format("select sum(Pendingqty) as TotPendingQty,Station, Challan_No,Count(temp.count1) as TotNoPosition,Location,Tool_no from (select lio.station,count(lio.p_no) as count1,lio.Challan_No,sum(lio.quant_sent-lio.quant_rec) as pendingqty,location,lio.tool_no FROM  [dbo].[location_info_GR] as lio group by lio.station,lio.Challan_No,lio.quant_sent,lio.p_no,lio.quant_rec,lio.tool_no,lio.location having lio.Challan_No='" + challan + "' and lio.tool_no='" + tool + "' and  (lio.quant_sent-lio.quant_rec)>0) temp group by Station, Challan_No ,Location,Tool_no "));
            }
        }
        gvProducts.DataBind();
        gvProducts.Columns[5].Visible = false;
        gvProducts.Columns[6].Visible = false;
    }


    private void BindProducts(string position, GridView gvOrdersPosition, string part_name, string vendor, string station, string challan)
    {
        gvOrdersPosition.DataSource = UtilityFunctions.GetData(string.Format("select id,location,date_of_trans,sent_or_rec,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as PendingTotQty_in_PPC,TransSentQty as SendQty_Or_ReceivedQty,TransSentQty from part_history_GR where   p_no='" + position + "' and part_name='" + part_name + "'  and station='" + station + "' and Challan_No='" + challan + "'  order by date_of_trans asc "));
        gvOrdersPosition.DataBind();
    }


    private void BindProducts_Position(string tool, string position, GridView gvOrdersPosition, string challanNumber, string LOCATION)
    {
        try
        {
            DataTable dt = new DataTable();
            string searchQueryLocationInfo = "";
            string searchQueryPartList = "";
            SearchQuery(out searchQueryPartList, "", out searchQueryLocationInfo, "locinfo.");
            if (challanNumber == "No Challan")
            {
                if (searchQueryLocationInfo != "" && searchQueryPartList != "")
                {
                    gvOrdersPosition.DataSource = UtilityFunctions.GetData(string.Format("SELECT distinct       locinfo.[station]      ,locinfo.[p_no]      ,locinfo.[part_name]  ,([quant_sent]-[quant_rec]) as qtypending   ,locinfo.[location]   ,locinfo.[Sent_Received_Date]   as Sent_Date    ,locinfo.[Delivery_commit_date]    ,locinfo.[Challan_No],locinfo.[Rwk],locinfo.[Extra_Received_Quantity]    FROM  [dbo].[location_info_GR] locinfo where ([quant_sent]-[quant_rec])>0 and  station='" + position + "'  and tool_no='" + tool + "' and locinfo.[location]='" + LOCATION + "'  and " + searchQueryLocationInfo + "  union all    SELECT distinct    [station]   ,[p_no]  ,[part_name]   ,quantity as qtypending ,[location]     ,[Sent_Date] ,Delivery_commit_date  ,[Challan_No], [Rwk_No],'' as Extra_Received_Quantity  FROM  [dbo].[part_history_GR] where   location='COMPANY' and  station='" + position + "' and quantity>0 and tool_no='" + tool + "' and " + searchQueryPartList + ""));
                }
                else
                {
                    gvOrdersPosition.DataSource = UtilityFunctions.GetData(string.Format("SELECT distinct       locinfo.[station]      ,locinfo.[p_no]      ,locinfo.[part_name]  ,([quant_sent]-[quant_rec]) as qtypending   ,locinfo.[location]   ,locinfo.[Sent_Received_Date]   as Sent_Date    ,locinfo.[Delivery_commit_date]    ,locinfo.[Challan_No],locinfo.[Rwk] ,locinfo.[Extra_Received_Quantity]   FROM  [dbo].[location_info_GR] locinfo where ([quant_sent]-[quant_rec])>0 and  station='" + position + "'  and tool_no='" + tool + "' and locinfo.[location]='" + LOCATION + "'   union all    SELECT distinct    [station]   ,[p_no]  ,[part_name]   ,quantity as qtypending ,[location]     ,[Sent_Date] ,Delivery_commit_date  ,[Challan_No], [Rwk_No],'' as Extra_Received_Quantity  FROM  [dbo].[part_history_GR] where   location='COMPANY' and  station='" + position + "' and quantity>0 and tool_no='" + tool + "'"));
                }
                gvOrdersPosition.DataBind();
                gvOrdersPosition.Columns[8].Visible = false;
            }
            else
            {
                if (searchQueryLocationInfo != "" && searchQueryPartList != "")
                {
                    dt = UtilityFunctions.GetData(string.Format("SELECT distinct       locinfo.[station]      ,locinfo.[p_no]      ,locinfo.[part_name]  ,([quant_sent]-[quant_rec]) as qtypending   ,locinfo.[location]   ,locinfo.[Sent_Received_Date]   as Sent_Date    ,locinfo.[Delivery_commit_date]    ,locinfo.[Challan_No],locinfo.tool_no, locinfo.Rwk,locinfo.Extra_Received_Quantity   FROM  [dbo].[location_info_GR] locinfo  where ([quant_sent]-[quant_rec])>0 and locinfo.[Challan_No]='" + challanNumber + "' and station='" + position + "' and locinfo.tool_no='" + tool + "' and " + searchQueryLocationInfo + "   union all    SELECT distinct    [station]   ,[p_no]  ,[part_name]   ,quantity as qtypending ,[location]     ,[Sent_Date] ,Delivery_commit_date  ,[Challan_No],tool_no, Rwk_No,'' as Extra_Received_Quantity  FROM  [dbo].[part_history_GR] where Challan_No='" + challanNumber + "' and location='COMPANY' and tool_no='" + tool + "' and  station='" + position + "' and quantity>0 and " + searchQueryPartList + ""));
                }
                else
                {
                    dt = UtilityFunctions.GetData(string.Format("SELECT distinct       locinfo.[station]      ,locinfo.[p_no]      ,locinfo.[part_name]  ,([quant_sent]-[quant_rec]) as qtypending   ,locinfo.[location]   ,locinfo.[Sent_Received_Date]   as Sent_Date    ,locinfo.[Delivery_commit_date]    ,locinfo.[Challan_No],locinfo.tool_no, locinfo.Rwk,locinfo.Extra_Received_Quantity   FROM  [dbo].[location_info_GR] locinfo  where ([quant_sent]-[quant_rec])>0 and locinfo.[Challan_No]='" + challanNumber + "' and station='" + position + "' and locinfo.tool_no='" + tool + "'    union all    SELECT distinct    [station]   ,[p_no]  ,[part_name]   ,quantity as qtypending ,[location]     ,[Sent_Date] ,Delivery_commit_date  ,[Challan_No],tool_no, Rwk_No, '' as Extra_Received_Quantity  FROM  [dbo].[part_history_GR] where Challan_No='" + challanNumber + "' and location='COMPANY' and tool_no='" + tool + "' and  station='" + position + "' and quantity>0"));
                }
            }
            var result = dt.AsEnumerable()
                   .Select(p => new
                   {
                       pNo = p.Field<string>("p_no"),
                       partName = p.Field<string>("part_name"),
                       location = p.Field<string>("location"),
                       station = p.Field<string>("station"),
                       sentReceivedDate = p.Field<DateTime?>("Sent_Date"),
                       deliveryCommitDate = p.Field<DateTime?>("Delivery_commit_date"),
                       challanNo = p.Field<string>("Challan_No"),
                       qtypending = p.Field<int>("qtypending"),
                       extraReceivedQuantity = p.Field<int>("Extra_Received_Quantity"),
                       toolNo = p.Field<int>("Tool_No"),
                   })
                       .GroupBy(p => new { p.pNo, p.station, p.location, p.sentReceivedDate, p.deliveryCommitDate, p.challanNo, p.partName, p.toolNo, p.extraReceivedQuantity }) //p.toolNo,  p.sentReceivedDate,
                   .Select(p => new
                   {
                       P_No = p.Key.pNo,
                       Station = p.Key.station,
                       Location = p.Key.location,
                       Sent_Date = p.Key.sentReceivedDate,
                       Delivery_commit_date = p.Key.deliveryCommitDate,
                       Challan_No = p.Key.challanNo,
                       Part_Name = p.Key.partName,
                       Tool_No = p.Key.toolNo,
                       qtypending = p.Sum(x => Convert.ToInt32(x.qtypending)),
                       Extra_Received_Quantity = p.Key.extraReceivedQuantity
                   }) // Tool_No = p.Key.toolNo, , Sent_Received_Date = p.Key.sentReceivedDate
                   .ToList();


            gvOrdersPosition.DataSource = result;
            gvOrdersPosition.DataBind();
            gvOrdersPosition.Columns[8].Visible = false;
        }
        catch (Exception ex)
        {
            ex.ToString();

        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    /// <summary>
    /// Generates the Search Query
    /// </summary>
    /// <param name="forparthistoryGr"></param>
    /// <param name="aliasPH"></param>
    /// <param name="forlocationinfoGR"></param>
    /// <param name="aliasLI"></param>
    protected void SearchQuery(out string forparthistoryGr, string aliasPH, out string forlocationinfoGR, string aliasLI)
    {

        string searchQueryPartList = "", searchQueryLocationInfo = "";
        if (tbTool.Text != "")
        {
            searchQueryLocationInfo = aliasLI + "tool_no='" + tbTool.Text + "'";
            searchQueryPartList = aliasPH + "tool_no='" + tbTool.Text + "'";
        }
        if (tbStation.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "station like '" + tbStation.Text + "%'";
            }
            else
            {
                searchQueryLocationInfo = aliasLI + "station like '" + tbStation.Text + "%'";
            }

            if (searchQueryPartList != "")
            {
                searchQueryPartList = searchQueryPartList + " and " + aliasPH + "station like '" + tbStation.Text + "%'";
            }
            else
            {
                searchQueryPartList = aliasPH + "station like '" + tbStation.Text + "%'";
            }
        }
        if (tbLocation.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "location='" + tbLocation.Text + "'";
            }
            else
            {
                searchQueryLocationInfo = aliasLI + "location='" + tbLocation.Text + "'";
            }

            if (searchQueryPartList != "")
            {
                searchQueryPartList = searchQueryPartList + " and " + aliasPH + "location='" + tbLocation.Text + "'";
            }
            else
            {
                searchQueryPartList = aliasPH + "location='" + tbLocation.Text + "'";
            }
        }
        if (tbPartName.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "part_name like '" + tbPartName.Text + "%'";
            }
            else
            {
                searchQueryLocationInfo = aliasLI + "part_name like '" + tbPartName.Text + "%'";
            }

            if (searchQueryPartList != "")
            {
                searchQueryPartList = searchQueryPartList + " and " + aliasPH + "part_name like '" + tbPartName.Text + "%'";
            }
            else
            {
                searchQueryPartList = aliasPH + "part_name like '" + tbPartName.Text + "%'";
            }
        }
        if (tbPosition.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "p_no='" + tbPosition.Text + "'";
            }
            else
            {
                searchQueryLocationInfo = aliasLI + "p_no='" + tbPosition.Text + "'";
            }

            if (searchQueryPartList != "")
            {
                searchQueryPartList = searchQueryPartList + " and " + aliasPH + "p_no='" + tbPosition.Text + "'";
            }
            else
            {
                searchQueryPartList = aliasPH + "p_no='" + tbPosition.Text + "'";
            }
        }
        if (tbReworkNo.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                if (chkBoxReworkNumberContains.Checked)
                {
                    searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "Rwk like '" + tbReworkNo.Text + "%'";
                }
                else
                {
                    searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "Rwk = '" + tbReworkNo.Text + "'";
                }
            }
            else
            {
                if (chkBoxReworkNumberContains.Checked)
                {
                    searchQueryLocationInfo = aliasLI + "Rwk like '" + tbReworkNo.Text + "%'";
                }
                else
                {
                    searchQueryLocationInfo = aliasLI + "Rwk = '" + tbReworkNo.Text + "'";
                }
            }

            if (searchQueryPartList != "")
            {
                if (chkBoxReworkNumberContains.Checked)
                {
                    searchQueryPartList = searchQueryPartList + " and " + aliasPH + "Rwk_No like '" + tbReworkNo.Text + "%'";
                }
                else
                {
                    searchQueryPartList = searchQueryPartList + " and " + aliasPH + "Rwk_No ='" + tbReworkNo.Text + "'";
                }
            }
            else
            {
                if (chkBoxReworkNumberContains.Checked)
                {
                    searchQueryPartList = aliasPH + "Rwk_No like '" + tbReworkNo.Text + "%'";
                }
                else
                {
                    searchQueryPartList = aliasPH + "Rwk_No = '" + tbReworkNo.Text + "'";
                }
            }
        }

        forlocationinfoGR = searchQueryLocationInfo;
        forparthistoryGr = searchQueryPartList;
    }

    /// <summary>
    /// Binds the Tool Wise Summary of Pending Items. Displays the Tool Number and Pending Quantity For each Tool at First Level
    /// </summary>
    public void showgrid()
    {
        string s = "";
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);

            string searchQueryLocationInfo = "";
            string searchQueryPartList = "";
            SearchQuery(out searchQueryPartList, "phg.", out searchQueryLocationInfo, "lig.");


            if (string.IsNullOrEmpty(searchQueryLocationInfo) && string.IsNullOrEmpty(searchQueryPartList))
            {
                searchQueryLocationInfo = "1=1";
                searchQueryPartList = "1=1";
            }

            s = "select distinct lig.tool_no, lig.part_name, lig.p_no,lig.station, lig.Rwk,lig.location, isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0) as Totalpendingqty, isnull(lig.Challan_No,'No Challan') as Challan_No, lig.Sent_Received_Date from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0)>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchQueryLocationInfo + ""
               + " union all select phg.tool_no, phg.part_name, phg.p_no, phg.station,  phg.Rwk_No, 'Company- ItemWaiting' as Location, (isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0)) as Totalpendingqty, isnull(phg.Challan_No,'No Challan') as Challan_No,''  as Sent_Received_Date from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where   phg.location='COMPANY' and phg.date_of_trans is not null and (phg.Sent_Date is null  and  phg.Received_Date is null) and phg.QtySent is null and phg.quantity>0 AND phg.location='COMPANY' and  pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchQueryPartList + ""
              + " union all select phg.tool_no, phg.part_name, phg.p_no, phg.station,  phg.Rwk_No, phg.location, (isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0)) as Totalpendingqty, isnull(phg.Challan_No,'No Challan') as Challan_No,''  as Sent_Received_Date from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where   phg.location='COMPANY' and phg.date_of_trans is not null and phg.location='COMPANY' and phg.quantity>0 and (isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0))>0  and (phg.Sent_Date is not null and phg.Received_Date is not null) and  pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)   and " + searchQueryPartList + ""
            + " order by tool_no,location ";
            DataTable dtItems = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter(s, conn);
            ad.Fill(dtItems);

            var result =
                     dtItems.AsEnumerable()
                         .Select(p => new
                         {
                             tool = p.Field<int>("Tool_No"),
                             quantity = p.Field<int>("Totalpendingqty")
                         })
                         .GroupBy(p => new { p.tool })
                         .Select(p => new { Tool_No = p.Key.tool, Totalpendingqty = p.Sum(x => Convert.ToInt32(x.quantity)) })
                         .ToList();

            Session["DataTableAdvanceReport"] = result;
            gvCustomers.DataSource = result;
            gvCustomers.DataBind();
            conn.Close();
            conn.Dispose();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }


    /// <summary>
    /// Clicking on Plus Icon at First Level this event is called. Clicking on it displays the Challan No, Vendor and Pending Qty Summary for clicked Tool
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Show_Hide_Showchallan(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlOrderschallan").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            string customerId = gvCustomers.DataKeys[row.RowIndex].Value.ToString();
            string cell_1_Value = gvCustomers.Rows[row.RowIndex].Cells[2].Text;
            if (tbStation.Text.Length > 0)
            {
                station = tbStation.Text;
            }
            else
            {
                station = "";
            }

            GridView gvChallan = row.FindControl("gvdchaalan") as GridView;
            Bindchallan(customerId, gvChallan, cell_1_Value, station);
        }
        else
        {
            row.FindControl("pnlOrderschallan").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    /// <summary>
    /// Clicking on Plus icon at Second Level , it display the Station,Total Pending Quantity,Total Positon and Challan NUmber for clicked Row(Challan and Vendor)
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgOrdersShowstation_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        GridViewRow Gv2Row = (GridViewRow)((ImageButton)sender).NamingContainer;
        GridView Childgrid = (GridView)(Gv2Row.Parent.Parent);
        GridViewRow Gv1Row = (GridViewRow)(Childgrid.NamingContainer);
        int b = Gv1Row.RowIndex;


        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlOrdersStation").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            GridView gvProducts = row.FindControl("gvdStation") as GridView;

            string challan = Childgrid.Rows[row.RowIndex].Cells[1].Text;
            string location = Childgrid.Rows[row.RowIndex].Cells[2].Text;
            string tool = Childgrid.Rows[row.RowIndex].Cells[4].Text;
            if (tool.Length > 0)
            {
                Bindstation(tool, gvProducts, location, station, challan);
            }
        }
        else
        {
            row.FindControl("pnlOrdersStation").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    protected void Show_Hide_OrdersGrid(object sender, ImageClickEventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);

        GridViewRow Gv2Row = (GridViewRow)((ImageButton)sender).NamingContainer;
        GridView Childgrid = (GridView)(Gv2Row.Parent.Parent);
        GridViewRow Gv1Row = (GridViewRow)(Childgrid.NamingContainer);
        int b = Gv1Row.RowIndex;

        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlOrders").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            GridView gvProducts = row.FindControl("gvOrders") as GridView;

            string Station = Childgrid.Rows[row.RowIndex].Cells[1].Text;
            string challan = Childgrid.Rows[row.RowIndex].Cells[4].Text;
            string tool = Childgrid.Rows[row.RowIndex].Cells[5].Text;
            string LOCATION = Childgrid.Rows[row.RowIndex].Cells[6].Text;
            BindProducts_Position(tool, Station, gvProducts, challan, LOCATION);
        }
        else
        {
            row.FindControl("pnlOrders").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    protected void gvdchaalan_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label result = (Label)e.Row.FindControl("Label60");
            string langId = e.Row.Cells[2].Text;

            if (langId == "01/01/1900" || langId == "01/01/1900 00:00:00")
            {
                e.Row.Cells[2].Text = "";
            }
        }
    }

    protected void gvOrders_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label result = (Label)e.Row.FindControl("Label60");
            string langId = e.Row.Cells[6].Text;
            string langId1 = e.Row.Cells[7].Text;

            if (langId == "01/01/1900" || langId == "01/01/1900 00:00:00" || langId1 == "01/01/1900" || langId1 == "01/01/1900 00:00:00")
            {
                e.Row.Cells[6].Text = "";
                e.Row.Cells[7].Text = "";
            }
        }
    }
}