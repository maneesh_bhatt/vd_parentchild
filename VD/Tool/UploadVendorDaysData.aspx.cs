﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using FileReader;

public partial class Tool_UploadVendorDaysData : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void UploadButton_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (!FileUploadControl.HasFile)
            {
                lblErrorMessage.Text = "You have not selected any file !!!";
                return;
            }
            string datetime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
            string fileName = Path.GetFileNameWithoutExtension(FileUploadControl.PostedFile.FileName) + "_" + datetime + "" + Path.GetExtension(FileUploadControl.PostedFile.FileName);
            string fileExtension = Path.GetExtension(FileUploadControl.PostedFile.FileName);
            if (!Directory.Exists(Server.MapPath("~/UploadedExcelFiles")))
            {
                Directory.CreateDirectory(Server.MapPath("~/UploadedExcelFiles"));
            }
            string fileLocation = Server.MapPath("~/UploadedExcelFiles/" + fileName);
            FileUploadControl.SaveAs(fileLocation);

            if (!(fileExtension == ".xls") && !(fileExtension == ".xlsx"))
            {
                lblErrorMessage.Text = "Invalid file !!! Select only an excel file";
                return;
            }
            UpdateVendorData(fileLocation);

        }
        catch (Exception ex)
        {

        }


    }




    public void UpdateVendorData(string path)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        SqlTransaction trans = null;
        try
        {
            lblErrorMessage.Text = String.Empty;


            string pathOnly = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);


            int count = 1;
            conn.Open();
            trans = conn.BeginTransaction();
           
            DataSet ds = null;
            DataTable dtprojectItems = new DataTable();
            using (clsFileReadHelper csv = new clsFileReadHelper(path))
            {
                ds = csv.getFileData();
            }
            if (ds.Tables.Count > 0)
            {
                dtprojectItems = ds.Tables[0].Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
            }
            string vendorName = "", days = "";
            for (int i = 1; i < dtprojectItems.Rows.Count; i++)
            {
                if (dtprojectItems.Columns.Count != 2)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Invalid File Selected.');", true);
                    return;
                }

                vendorName = Convert.ToString(dtprojectItems.Rows[i][0]);
                days = Convert.ToString(dtprojectItems.Rows[i][1]);


                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Transaction = trans;
                if (vendorName != "")
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "select * from Vendor_Master_Job_Work where Vendor_Name=@VendorName";
                    cmd.Parameters.AddWithValue("@VendorName", vendorName);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (!rdr.HasRows)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Invalid Vendor Name at Row- " + Convert.ToInt32(i + 1) + "');", true);
                            return;
                        }
                    }

                    DateTime uploaddate = new DateTime();
                    uploaddate = DateTime.Now;
                    string id = "";
                    cmd = new SqlCommand("select Vendor_Days_Master_Id from Vendor_Days_Master where Vendor_Name=@VendorName", conn, trans);
                    cmd.Parameters.AddWithValue("@VendorName", vendorName);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            id = Convert.ToString(rdr["Vendor_Days_Master_Id"]);
                        }
                    }

                    if (!string.IsNullOrEmpty(id))
                    {
                        cmd = new SqlCommand("update Vendor_Days_Master set  Standard_Days=@StandardDays where Vendor_Days_Master_Id=@Id", conn, trans);
                        cmd.Parameters.AddWithValue("@StandardDays", days);
                        cmd.Parameters.AddWithValue("@Id", id);
                        cmd.ExecuteNonQuery();

                        string rowsAffected = "Record Update for Row=" + count;
                    }
                    else
                    {
                        cmd = new SqlCommand("insert into Vendor_Days_Master  (Vendor_Name,Standard_Days) Values(@VendorName,@StandardDays)", conn, trans);
                        cmd.Parameters.AddWithValue("@StandardDays", days);
                        cmd.Parameters.AddWithValue("@VendorName", vendorName);
                        cmd.ExecuteNonQuery();

                        string rowsAffected = "Record Update for Row=" + count;
                    }
                }
            }
            trans.Commit();
            lblErrorMessage.Text = "Vendor Days Updated Successfully.";
        }
        catch (Exception ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
        }
        finally
        {
            conn.Close();
        }
    }
}