﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Security.Cryptography;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using CustomFunctions;

public partial class Vendor_Master : System.Web.UI.Page
{
    static string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
    SqlConnection conn = new SqlConnection(sqlconnstring);
    DataTable dt = new DataTable();
    DataTable addrow = new DataTable();

    object request_id = "";
    int count = 0;
    int total = 0;
    object vendor_id = "";
    int estimatevalue = 0;
    private static bool[] rightsArray;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null)
        {
            Response.Redirect("~/Pages/Login.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                SetControlsRights();
                bind_List(lstOperation);
                if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    txtvendorname.Enabled = false;
                    LoadData(Convert.ToString(Request.QueryString["ID"]));
                }
                // clear();
            }

        }

    }

    /// <summary>
    /// User Rights are Validated in this Method
    /// </summary>
    protected void SetControlsRights()
    {
        try
        {
            rightsArray = AccessRights.SetUserAccessRights();
            if (!rightsArray[3])
            {
                hypLnkView.Visible = false;
            }

            if (!rightsArray[0])
            {
                Response.Redirect("../Pages/WelcomePage.aspx?Error=NoRights");
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

    }

    /// <summary>
    /// When User clicks on Edit Button in Vendor Master Job Work Screen then he is redirected to this Page and this function is loaded that displays the Existing Values
    /// for that vendor Id
    /// </summary>
    /// <param name="id"></param>
    protected void LoadData(string id)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand();
            string vendorName = "";
            cmd = new SqlCommand("select * from Vendor_Master_Job_Work where Id=@Id", con);
            cmd.Parameters.AddWithValue("@Id", id);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                txtvendorname.Text = Convert.ToString(reader["Vendor_Name"]);
                vendorName = Convert.ToString(reader["Vendor_Name"]);
                inputVendorType.SelectedValue = Convert.ToString(reader["Vendor_Type"]);
                txtaddress.Text = Convert.ToString(reader["Vendor_Address"]);
                txtcity.Text = Convert.ToString(reader["Vendor_Address"]);
                txtstate.Text = Convert.ToString(reader["State"]);
                txtpin.Text = Convert.ToString(reader["Pin"]);
                txtphone.Text = Convert.ToString(reader["Phone"]);
                txtemail.Text = Convert.ToString(reader["Email"]);
                txtcontactperson.Text = Convert.ToString(reader["Contact_Person"]);
                chkIsActive.Checked = Convert.ToBoolean(reader["Is_Active"]);
                txtAllocatedTo.Text = Convert.ToString(reader["Allocated_To"]);
                txtInchagePhoneNo.Text = Convert.ToString(reader["Incharge_Phone_No"]);
                txtStandardDays.Text = Convert.ToString(reader["Standard_Days"]);
                if (reader["Item_Pending_Report_Required"] != DBNull.Value)
                    chkItemPendingReportRequired.Checked = Convert.ToBoolean(reader["Item_Pending_Report_Required"]);
                if (reader["Name_Required_For_Vendor_Verification_List"] != DBNull.Value)
                    chkNameRequiredForVerificationList.Checked = Convert.ToBoolean(reader["Name_Required_For_Vendor_Verification_List"]);
                if (reader["Frequency_Of_Verification"] != DBNull.Value)
                    ddlFrequencyOfVerification.SelectedValue = Convert.ToString(reader["Frequency_Of_Verification"]);
                txtGSTNo.Text = Convert.ToString(reader["GST_Number"]);
            }
            reader.Close();
            lstOperation.ClearSelection();
            SqlDataAdapter ad = new SqlDataAdapter("select * from tbl_Vendor_Opration_Details where Vendor_Name=@VendorName", con);
            ad.SelectCommand.Parameters.AddWithValue("@VendorName", vendorName);
            DataTable dt = new DataTable();
            ad.Fill(dt);
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                for (int i = 0; i < lstOperation.Items.Count; i++)
                {
                    if (dt.Rows[j]["Operation_Name"].ToString() == lstOperation.Items[i].Text)
                    {
                        lstOperation.Items[i].Selected = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }

    }

    /// <summary>
    /// Binds the List of vendors on the basis of Selected Vendor
    /// </summary>
    /// <param name="ddlloc"></param>
    private void bind_List(ListBox ddlloc)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        try
        {
            string query = " SELECT  [Id] ,[Operation_Name]  FROM tbl_Operation_Master";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable request_det_head = new DataTable();
            da.Fill(request_det_head);
            if (request_det_head.Rows.Count > 0)
            {
                ddlloc.DataSource = request_det_head;
                ddlloc.DataTextField = "Operation_Name";
                ddlloc.DataValueField = "Id";
                ddlloc.DataBind();
                ddlloc.Items.Insert(0, new ListItem(String.Empty, String.Empty));
                ddlloc.SelectedIndex = 0;
                ddlloc.Visible = true;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    private void clear()
    {
        txtvendorname.Text = "";
        txtaddress.Text = "";
        txtcity.Text = "";
        txtstate.Text = "";
        txtphone.Text = "";
        txtemail.Text = "";
    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    /// <summary>
    /// Updated the vendor if Id is not null (Edit Mode), other wise adds a New Row in the Database
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        SqlTransaction trans = null;
        try
        {
            if ((string.IsNullOrEmpty(txtAllocatedTo.Text) || string.IsNullOrEmpty(txtInchagePhoneNo.Text) || string.IsNullOrEmpty(txtcontactperson.Text) || string.IsNullOrEmpty(txtphone.Text)) && chkIsActive.Checked)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Vendor can not be made as Active if Allocated To/ Incharge Contact No/ Supplier Name/ Supplier Contact No is not provided');", true);
                return;
            }
            conn.Open();
            trans = conn.BeginTransaction();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.Clear();
                if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
                {
                    cmd.CommandText = "Update Vendor_Master_Job_work set Vendor_Name=@Vendor_Name,Vendor_Address=@Vendor_Address, Vendor_Type=@Vendor_Type, City=@City,State=@State,Pin=@Pin,Phone=@Phone,Created_Date=GETDATE(),email=@email,Contact_Person=@contact_Person,Is_Active=@IsActive,Allocated_To=@AllocatedTo,Incharge_Phone_No=@InchargePhoneNo,Standard_Days=@StandardDays,Item_Pending_Report_Required=@ItemPendingReportRequired,Name_Required_For_Vendor_Verification_List=@NameRequiredForVendorVerificationList,Frequency_Of_Verification=@FrequencyOfVerification,GST_Number=@GSTNumber where Id=@Id";
                    cmd.Parameters.AddWithValue("@Id", Request.QueryString["Id"]);
                }
                else
                {
                    cmd.CommandText = "INSERT INTO Vendor_Master_Job_work(Vendor_Name,Vendor_Address,Vendor_Type, City,State,Pin,Phone,Created_Date,email,Contact_Person,Is_Active,Allocated_To,Incharge_Phone_No,Standard_Days,Item_Pending_Report_Required,Name_Required_For_Vendor_Verification_List,Frequency_Of_Verification,GST_Number) VALUES (@Vendor_Name,@Vendor_Address,@Vendor_Type, @City,@State,@Pin ,@Phone,GETDATE(),@email,@contact_Person,@IsActive,@AllocatedTo,@InchargePhoneNo,@StandardDays,@ItemPendingReportRequired,@NameRequiredForVendorVerificationList,@FrequencyOfVerification,@GSTNumber);SELECT SCOPE_IDENTITY()";

                }
                cmd.Transaction = trans;
                cmd.Connection = conn;

                cmd.Parameters.Add("Vendor_Name", SqlDbType.VarChar, 200).Value = txtvendorname.Text;
                cmd.Parameters.Add("Vendor_Address", SqlDbType.VarChar, 400).Value = txtaddress.Text;
                cmd.Parameters.Add("Vendor_Type", SqlDbType.VarChar, 400).Value = inputVendorType.SelectedValue;
                cmd.Parameters.Add("City", SqlDbType.VarChar, 200).Value = txtcity.Text;
                cmd.Parameters.Add("State", SqlDbType.VarChar, 200).Value = txtstate.Text;
                cmd.Parameters.Add("Pin", SqlDbType.VarChar, 100).Value = txtpin.Text;
                cmd.Parameters.Add("Phone", SqlDbType.VarChar, 100).Value = txtphone.Text;
                cmd.Parameters.Add("email", SqlDbType.VarChar, 200).Value = txtemail.Text;
                cmd.Parameters.AddWithValue("@contact_Person", txtcontactperson.Text);
                cmd.Parameters.AddWithValue("@IsActive", chkIsActive.Checked);
                cmd.Parameters.AddWithValue("@AllocatedTo", txtAllocatedTo.Text);
                cmd.Parameters.AddWithValue("@InchargePhoneNo", txtInchagePhoneNo.Text);
                cmd.Parameters.AddWithValue("@StandardDays", (string.IsNullOrEmpty(txtStandardDays.Text)) ? 0 : Convert.ToInt32(txtStandardDays.Text));
                cmd.Parameters.AddWithValue("@ItemPendingReportRequired", chkItemPendingReportRequired.Checked);
                cmd.Parameters.AddWithValue("@NameRequiredForVendorVerificationList", chkNameRequiredForVerificationList.Checked);
                cmd.Parameters.AddWithValue("@FrequencyOfVerification", ddlFrequencyOfVerification.SelectedValue);
                cmd.Parameters.AddWithValue("@GSTNumber", txtGSTNo.Text);
                if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
                {
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    vendor_id = cmd.ExecuteScalar();
                }
                count = count + 1;
            }

            if (lstOperation.Items.Count > 0)
            {
                for (int i = 0; i < lstOperation.Items.Count; i++)
                {
                    string selectedItem = lstOperation.Items[i].Text;
                    string id = lstOperation.Items[i].Value;
                    SqlCommand cmd1 = new System.Data.SqlClient.SqlCommand();
                    cmd1.Transaction = trans;
                    cmd1.Connection = conn;
                    bool recordExist = false;
                    cmd1.CommandText = "select * from tbl_Vendor_Opration_Details where Vendor_Name=@VendorName and Operation_Name=@OperationName";
                    cmd1.Parameters.AddWithValue("@VendorName", txtvendorname.Text);
                    cmd1.Parameters.AddWithValue("@OperationName", selectedItem);
                    using (SqlDataReader reader = cmd1.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            recordExist = true;
                        }
                    }

                    if (lstOperation.Items[i].Selected)
                    {
                        if (!recordExist && lstOperation.Items[i].Text != "")
                        {
                            string query = "insert into tbl_Vendor_Opration_Details(vendor_name,Operation_id,Operation_Name)values('" + txtvendorname.Text + "'," + id + ",'" + selectedItem + "')";
                            cmd1.CommandText = query;
                            cmd1.Connection = conn;
                            cmd1.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        if (recordExist)
                        {
                            string query = "delete from tbl_Vendor_Opration_Details where Operation_Name='" + lstOperation.Items[i].Text + "' and vendor_name='" + txtvendorname.Text + "'";
                            cmd1.CommandText = query;
                            cmd1.Connection = conn;
                            cmd1.ExecuteNonQuery();
                        }
                    }
                }
            }

            if (count > 0)
            {
                MessageBox("Vendor added Successfuly");

                trans.Commit();
                if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
                {
                    Response.Redirect("VendorMasterJobWorkData.aspx", false);
                }
                clear();
            }
            else
            {
                MessageBox("Please Select atleast one Operation");
                return;
            }
        }
        catch (Exception ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }
}

