﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class Tool_GetPrintChallan : System.Web.UI.Page
{
    String MIN = "";
    string location = "";
    SqlCommand cmd;
    SqlDataAdapter adp;
    int Total = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        string sheet = "";
       
        SqlConnection conn = new SqlConnection(sqlconnstring);
        SqlTransaction trans = null;
        try
        {
            conn.Open();
            trans = conn.BeginTransaction();
            LblDt.Text = System.DateTime.Now.ToString("dd-MMM-yyyy");
            MIN = Session["MIN"].ToString();
            location = Session["vendor"].ToString();

            int k = showData1();
            k = k + 1;
            Label1.Text = k.ToString();
            sheet = "QCSheet" + Label1.Text;
             
            SqlCommand cmd2 = new SqlCommand("insert into part_history1(Challan_No) values('" + MIN + "')", conn, trans);
            cmd2.ExecuteNonQuery();
            SqlCommand cmd3 = new SqlCommand("update   temp_part_history_QC set Qcsheet_no='" + sheet + "' where challan_no='" + MIN + "'", conn, trans);
            cmd3.ExecuteNonQuery();
            trans.Commit();

            string query = "SELECT distinct   Temp_Part_QC.[p_no]  ,Temp_Part_QC.BillNo, Temp_Part_QC.[part_name]   ,Temp_Part_QC.[station],Temp_Part_QC.quantity  ,Temp_Part_QC.tool_no, parts.rwk_no,Temp_Part_QC.QtySent,QCPart_His.location,Temp_Part_QC.Challan_No   ,Temp_Part_QC.date_of_trans  FROM  [dbo].[part_history_QC] QCPart_His inner join Temp_part_history_QC Temp_Part_QC on Temp_Part_QC.p_no=QCPart_His.p_no and QCPart_His.part_name=Temp_Part_QC.part_name and QCPart_His.station=Temp_Part_QC.station inner join parts on parts.p_no= Temp_Part_QC.p_no and parts.part_name=Temp_Part_QC.part_name and parts.station=Temp_Part_QC.station  where Temp_Part_QC.QtySent=QCPart_His.quantity and Temp_Part_QC.Challan_No='" + MIN + "' and QCPart_His.location='" + location + "'  order by Temp_Part_QC.tool_no,Temp_Part_QC.station,Temp_Part_QC.p_no ASC ";
            UtilityFunctions.GetData(query);
            ExportGridView();
           
        }
        catch (SqlException ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }

    public int showData1()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            int id = 0;
            conn.Open();
            string conString = "SELECT isnull(MAX(id),0) as id  FROM part_history1 with(nolock)";
            cmd = new SqlCommand(conString, conn);
            adp = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                id = Convert.ToInt16(dt.Rows[0][0].ToString());
            }
            return id;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return 0;
        }
        finally
        {
            conn.Close();
        }
    }

    private void ExportGridView()
    {
        try
        {
            int count = 0;
            string dirpath = AppDomain.CurrentDomain.BaseDirectory;
            DataTable dt1 = new System.Data.DataTable();
            DataRow dr2 = null;
            dt1.Columns.Add(new DataColumn("Sno", typeof(string)));
            dt1.Columns.Add(new DataColumn("Tool No", typeof(string)));
            dt1.Columns.Add(new DataColumn("Station No", typeof(string)));
            dt1.Columns.Add(new DataColumn("Rework No", typeof(string)));
            dt1.Columns.Add(new DataColumn("Position No", typeof(string)));

            dt1.Columns.Add(new DataColumn("Total Incoming Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("Ok Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("Rejected Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("Parts Sent to party for Rework Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("Inhouse Rework Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("inhouse rework hours planned", typeof(string)));
            dt1.Columns.Add(new DataColumn("Est Inspection Time Hr", typeof(string)));
            dt1.Columns.Add(new DataColumn("Checked By", typeof(string)));


            dr2 = dt1.NewRow();
            dr2["Sno"] = 0;
            //dr2["Date"] = string.Empty;
            dr2["Tool No"] = string.Empty;
            dr2["Station No"] = string.Empty;
            dr2["Rework No"] = string.Empty;
            dr2["Position No"] = string.Empty;
            dr2["Total Incoming Qty"] = string.Empty;
            dr2["Ok Qty"] = string.Empty;
            dr2["Rejected Qty"] = string.Empty;
            dr2["Parts Sent to party for Rework Qty"] = string.Empty;
            dr2["Inhouse Rework Qty"] = string.Empty;
            dr2["inhouse rework hours planned"] = string.Empty;

            dt1.Rows.Add(dr2);

            DataTable dt = (DataTable)Session["DataTableAdvanceReport"];

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                dr2 = dt1.NewRow();
                dt1.Rows.Add(dr2);
                dt1.Rows[i]["Sno"] = i + 1;
                DateTime currentdate = DateTime.Now;
                string currentdate1 = currentdate.ToString("dd/MM/yyyy");
                lblpartyname.Text = dt.Rows[i]["location"].ToString();
                lblchallanno2.Text = dt.Rows[i]["Challan_No"].ToString();
                lblbillno.Text = dt.Rows[i]["BillNo"].ToString();
                //dt1.Rows[i]["Date"] = currentdate1;
                //dt1.Rows[i]["Vendor Name"] = dt.Rows[i]["location"].ToString();
                dt1.Rows[i]["Tool No"] = dt.Rows[i]["tool_no"];
                dt1.Rows[i]["Station No"] = dt.Rows[i]["station"].ToString();
                dt1.Rows[i]["Rework No"] = dt.Rows[i]["rwk_no"].ToString();
                dt1.Rows[i]["Position No"] = dt.Rows[i]["p_no"].ToString();
                dt1.Rows[i]["Total Incoming Qty"] = dt.Rows[i]["QtySent"].ToString();
                Total = Total + Convert.ToInt32(dt.Rows[i]["QtySent"].ToString());

                count = i;
            }

            count = count + 2;
            dr2 = dt1.NewRow();
            dt1.Rows[count - 1][3] = "Total Qty";
            dt1.Rows[count - 1][4] = Total;
            dt1.Rows.Add(dr2);
            dt1.Rows[count][0] = "Details of Inhouse Rework Planned";
            count = count + 1;
            dr2 = dt1.NewRow();
            dt1.Rows.Add(dr2);
            dt1.Rows[count][0] = "Estimated Hours";

            count = count + 1;
            dr2 = dt1.NewRow();
            dt1.Rows.Add(dr2);

            dt1.Rows[count][0] = "Tool No";
            dt1.Rows[count][1] = "station no";
            dt1.Rows[count][2] = "Part No";
            dt1.Rows[count][3] = "Description";
            dt1.Rows[count][4] = "Qty";
            dt1.Rows[count][5] = "ML";
            dt1.Rows[count][6] = "SG";
            dt1.Rows[count][7] = "TUR";
            dt1.Rows[count][8] = "BW";
            dt1.Rows[count][9] = "fab";
            dt1.Rows[count][10] = "est.hr";

            for (int i = 1; i <= 35 - 1; i++)
            {
                dr2 = dt1.NewRow();
                dt1.Rows.Add(dr2);
            }
            //dt1.Rows.RemoveAt(0);
            //dt1.AcceptChanges();
            if (dt1.Rows.Count > 0)
            {
                Grdview1.DataSource = dt1;
                Grdview1.DataBind();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

    }

    protected void Grdview1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[3].Text == "Total Qty")
            {
                e.Row.Cells[3].Font.Bold = true;
                e.Row.Cells[4].Font.Bold = true;
                e.Row.BackColor = System.Drawing.Color.LemonChiffon;
            }

            if (e.Row.Cells[0].Text == "Details of Inhouse Rework Planned")
            {
                e.Row.Cells[1].ColumnSpan = 3;
                e.Row.Cells[0].ColumnSpan = 12;
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                //now make up for the colspan from cell2
                e.Row.Cells.RemoveAt(1);
                e.Row.Cells.RemoveAt(2);
                e.Row.Cells.RemoveAt(3);
                e.Row.Cells.RemoveAt(4);
                e.Row.Cells.RemoveAt(5);
                //e.Row.Cells.RemoveAt(6);

                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                //e.Row.Cells[6].Visible = false;

            }

            if (e.Row.Cells[0].Text == "Estimated Hours")
            {

                e.Row.Cells[0].ColumnSpan = 13;
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells.RemoveAt(1);
                e.Row.Cells.RemoveAt(2);
                e.Row.Cells.RemoveAt(3);
                e.Row.Cells.RemoveAt(4);
                e.Row.Cells.RemoveAt(5);
                //e.Row.Cells.RemoveAt(6);
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;

            }
        }
    }

    protected void Grdview1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string kk = e.Row.Cells[0].Text;
            if (e.Row.Cells[0].Text == "Estimated Hours")
            {
                e.Row.Cells[0].ColumnSpan = 12;
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells.RemoveAt(1);
                e.Row.Cells.RemoveAt(2);
                e.Row.Cells.RemoveAt(3);
                e.Row.Cells.RemoveAt(4);
                e.Row.Cells.RemoveAt(5);
                e.Row.Cells.RemoveAt(6);
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
            }
        }
    }
}
