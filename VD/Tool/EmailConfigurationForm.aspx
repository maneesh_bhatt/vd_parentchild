﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="EmailConfigurationForm.aspx.cs" Inherits="Tool_EmailConfigurationForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .style14
        {
            width: 1034px;
        }
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .control-container
        {
            width: 100%;
            height: 54px;
        }
        .label
        {
            width: 18%;
            float: left;
            padding-left: 20px;
        }
        .input-control-container
        {
            width: 60%;
            float: left;
        }
        .input-control
        {
            width: 58%;
            height: 26px;
        }
        .control-container-multiline
        {
            width: 100%;
            height: 90px;
        }
        .input-control-multiline
        {
            width: 58%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="border: 1px solid Blue; min-height: 100px; padding: 9px;">
        <div>
            <div align="center">
                <span class="style13">Email Configuration Form</span>
            </div>
        </div>
        <div style="width: 100%; margin-bottom: 10px; margin-top: 20px; height: 30px;">
            <div style="font-size: 13px; float: left;">
                <asp:Label runat="server" ID="lblMessage"></asp:Label>
            </div>
            <asp:HyperLink ID="HyperLink1" Style="background-color: #335599; float: right; color: White;
                padding: 5px; margin-top: 9px;" runat="server" NavigateUrl="~/Tool/EmailConfigurationAdmin.aspx">View Configuration</asp:HyperLink>
        </div>
        <div class="control-container">
            <div class="label">
                Category <span style="color: Red;">*</span></div>
            <div class="input-control-container">
                <asp:DropDownList ID="ddlCategory" AppendDataBoundItems="true" BackColor="#F6F1DB"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged"
                    Width="58.5%" Height="30px" ForeColor="#7d6754" CssClass="input-control" runat="server">
                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                    <asp:ListItem Text="commitmentChange" Value="commitmentChange"></asp:ListItem>
                    <asp:ListItem Text="itemWaitingForProcess" Value="itemWaitingForProcess"></asp:ListItem>
                    <asp:ListItem Text="OverDue" Value="OverDue"></asp:ListItem>
                    <asp:ListItem Text="partsInCompany" Value="partsInCompany"></asp:ListItem>
                    <asp:ListItem Text="DeliveryCommitDateOverDue" Value="DeliveryCommitDateOverDue"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlLevel"
                    ErrorMessage="Email Level is required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="control-container">
            <div class="label">
                Level <span style="color: Red;">*</span></div>
            <div class="input-control-container">
                <asp:DropDownList ID="ddlLevel" AppendDataBoundItems="true" BackColor="#F6F1DB" Width="58.5%"
                    Height="30px" ForeColor="#7d6754" CssClass="input-control" runat="server">
                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlLevel"
                    ErrorMessage="Email Level is required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="control-container">
            <div class="label">
                Email Incharge
            </div>
            (Applicable for Level 1)
            <div class="input-control-container">
                <asp:DropDownList ID="ddlEmailIncharge" AppendDataBoundItems="true" BackColor="#F6F1DB"
                    Width="58.5%" Height="30px" ForeColor="#7d6754" CssClass="input-control" runat="server">
                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="control-container-multiline" runat="server" visible="false">
            <div class="label">
                Email Subject <span style="color: Red;">*</span></div>
            <div class="input-control-container">
                <asp:TextBox ID="inputEmailSubject" TextMode="MultiLine" Rows="4" runat="server"
                    CssClass="input-control-multiline"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="inputEmailSubject"
                    ErrorMessage="Email Subject is required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="control-container-multiline">
            <div class="label">
                Email To<span style="color: Red;">*</span></div>
            <div class="input-control-container">
                <asp:TextBox ID="inputEmailTo" TextMode="MultiLine" Rows="4" runat="server" CssClass="input-control-multiline"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="inputEmailTo"
                    ErrorMessage="Email To is required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="control-container-multiline">
            <div class="label">
                Email CC</div>
            <div class="input-control-container">
                <asp:TextBox ID="inputEmailCC" runat="server" Rows="4" Columns="6" TextMode="MultiLine"
                    CssClass="input-control-multiline"></asp:TextBox>
            </div>
        </div>
        <div class="control-container">
            <div class="label">
            </div>
            <div class="input-control-container" style="margin-left: 234px;">
                <asp:ImageButton ImageUrl="~/images/summit.jpg" ID="btnsave" Style="width: 20%;"
                    OnClick="btnSubmit_Click" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
