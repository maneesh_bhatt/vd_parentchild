﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Configuration;

public partial class Tool_VendorVisitReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnGenerateVisitReport.Visible = false;
        }
    }

    /// <summary>
    /// When user clicks on Search retrieves the Sent data from Part History Gr for the Selected Date range and given filters
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            Session["DataTableAdvanceReport"] = null;
            gridViewVendorPendingItems.DataSource = null;
            gridViewVendorPendingItems.DataBind();
            if (inputFromDate.Text != "" && inputToDate.Text != "")
            {
                btnGenerateVisitReport.Visible = true;
                string searchCondition = "";
                if (ddlLocation.SelectedItem.Text == "" && ddlLocation.SelectedItem.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please select Tool or Vendor.');", true);
                    return;
                }
                if (inputFromDate.Text != "" && inputToDate.Text != "")
                {
                    searchCondition = " Convert(Date, phg.Date_Of_Trans)>='" + inputFromDate.Text + "' and Convert(Date,phg.Date_Of_Trans)<='" + inputToDate.Text + "'";
                }
                if (radioVendorType.SelectedValue != "")
                {
                    if (searchCondition != "")
                    {
                        searchCondition = searchCondition + " and vm.Vendor_Type='" + radioVendorType.SelectedValue + "'";
                    }
                    else
                    {
                        searchCondition = " vm.Vendor_Type='" + radioVendorType.SelectedValue + "'";
                    }
                }
                if (ddlLocation.SelectedItem.Text != "" && ddlLocation.SelectedItem.Text != "All")
                {
                    if (searchCondition != "")
                    {
                        searchCondition = searchCondition + " and phg.Location='" + ddlLocation.SelectedItem.Text + "'";
                    }
                    else
                    {
                        searchCondition = " phg.Location='" + ddlLocation.SelectedItem.Text + "'";
                    }
                }
                if (ddlChallanNo.SelectedValue != "")
                {
                    string[] challanNo = ddlChallanNo.SelectedValue.Split('-');
                    if (searchCondition != "")
                    {
                        searchCondition = searchCondition + " and phg.Challan_No='" + challanNo[0] + "'";
                    }
                    else
                    {
                        searchCondition = " phg.Challan_No='" + challanNo[0] + "'";
                    }
                }

                string orderby = "";
                orderby = "order by phg.Challan_No asc";

                DataTable dt = new DataTable();
                SqlDataAdapter ad = new SqlDataAdapter("select distinct pgr.Parts_Gr_Id, phg.Location, phg.Date_Of_Trans,phg.Delivery_commit_date, phg.tool_no,phg.station,phg.Upload_Qty, "
                + " phg.p_no,phg.part_name,phg.quantity,phg.Rwk_No,pgr.opr17, phg.Challan_No,phg.User_Name from part_history_Gr phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no  "
                + " and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no left join Vendor_Master_Job_Work vm on phg.Location=vm.Vendor_Name "
                + "  where pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and phg.Sent_Or_Rec='Sent' and " + searchCondition + " " + orderby, conn);

                ad.SelectCommand.CommandTimeout = 0;
                ad.Fill(dt);
                Session["DataTableAdvanceReport"] = dt;
                gridViewVendorPendingItems.DataSource = dt;
                gridViewVendorPendingItems.DataBind();

                gridViewVendorPendingItems.EmptyDataText = "No Records Found";
                //updatePage.Update();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Start Date, End Date and Vendor selection required for generating Report.');", true);
                return;
            }

        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }

    /// <summary>
    /// User can Export the Data to Excel
    /// </summary>
    protected void ExportToExcel()
    {
        Response.ClearContent();
        string fileName = "Tool_Location_Outstanding_Data_Report";
        string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gridViewVendorPendingItems.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void gridViewVendorPendingItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }


    /// <summary>
    /// User can retrieve the List of Distinct Vendors from Vendor Master Job Work Table
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetVendorName(string text)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            List<string> allItems = new List<string>();
            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrWhiteSpace(text))
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work where Vendor_Name like '%" + text + "%' and Vendor_Name not in ('VD Finish Store','Assembly','FinishPartStore','weld vd finish store','Weld Parts Consumed','Reject Parts') order by Vendor_Name asc ", con);
            }
            else
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work where Vendor_Name not in ('VD Finish Store','Assembly','FinishPartStore','weld vd finish store','Weld Parts Consumed','Reject Parts') order by Vendor_Name asc ", con);
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string vendorName = Convert.ToString(reader["Vendor_Name"]);
                allItems.Add(vendorName);
            }
            reader.Close();
            return allItems;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    /// <summary>
    /// Retrieves the distinct Tool from Part History Gr table
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetTooNumber(string text)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            List<string> allItems = new List<string>();
            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrWhiteSpace(text))
            {
                cmd = new SqlCommand("select distinct tool_no from part_history_GR where tool_no is not null and tool_no!='' and tool_no like '%" + text + "%'  order by tool_no ASC", con);
            }
            else
            {
                cmd = new SqlCommand("select distinct tool_no from part_history_GR where tool_no is not null and tool_no!='' order by tool_no asc ", con);
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string vendorName = Convert.ToString(reader["tool_no"]);
                allItems.Add(vendorName);
            }
            reader.Close();
            return allItems;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    /// <summary>
    /// Loads the List of Distinct Vendors from Vendor Master Job Work Table
    /// </summary>
    /// <param name="vendorType"></param>
    protected void LoadVendorList(string vendorType)
    {

        string query = "";
        if (vendorType == "Internal Vendor")
        {
            query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Type='" + vendorType + "' and Vendor_Name!='' and Vendor_Visit_Report_Required=1 and Vendor_Name is not null order by Vendor_Name asc ";
        }
        else
        {
            query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Type='" + vendorType + "' and Vendor_Name!='' and Vendor_Name is not null order by Vendor_Name asc ";
        }
        string dataTextField = "VENDOR_NAME";
        string dataValueField = "ID";
        UtilityFunctions.bind_vendor(ddlLocation, query, dataTextField, dataValueField);
        //updatePage.Update();
    }

    protected void radioVendorType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLocation.Items.Clear();
        ddlChallanNo.Items.Clear();
        if (radioVendorType.SelectedValue != "")
        {
            LoadVendorList(radioVendorType.SelectedValue);
        }
        ddlLocation.Items.Insert(1, new ListItem("All", "All"));
        ddlChallanNo.Items.Insert(0, new ListItem("Select", ""));
    }

    protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadChallanNumbersByFilters();
    }


    protected void inputFromDate_TextChanged(object sender, EventArgs e)
    {
        LoadChallanNumbersByFilters();
    }

    protected void inputToDate_TextChanged(object sender, EventArgs e)
    {
        LoadChallanNumbersByFilters();
    }

    /// <summary>
    /// Challan Number are loaded for the Selected Filters from Part History Gr Table
    /// </summary>
    protected void LoadChallanNumbersByFilters()
    {
        ddlChallanNo.Items.Clear();
        if (ddlLocation.SelectedItem.Text != "")
        {
            if (ddlLocation.SelectedItem.Text == "All")
            {
                ddlChallanNo.Enabled = false;
            }
            else
            {
                ddlChallanNo.Enabled = true;
            }
            string challnSearchCondition = "";
            if (inputFromDate.Text != "" && inputToDate.Text != "")
            {
                challnSearchCondition = " Convert(Date, Date_Of_Trans)>='" + inputFromDate.Text + "' and Convert(Date,Date_Of_Trans)<='" + inputToDate.Text + "'";
            }
            if (ddlLocation.SelectedItem.Text != "")
            {
                if (challnSearchCondition != "")
                {
                    challnSearchCondition = challnSearchCondition + " and Location='" + ddlLocation.SelectedItem.Text + "'";
                }
                else
                {
                    challnSearchCondition = " Location='" + ddlLocation.SelectedItem.Text + "'";
                }
            }
            string query = "select distinct Challan_No+'-'+Convert(varchar(10),CONVERT(date,date_of_trans,106),103) as ChallanInfo, CONVERT(date,date_of_trans,106) as ChallanDate from Part_History_Gr where  Sent_Or_Rec='Sent' and " + challnSearchCondition + " order by ChallanDate Desc";
            DataTable dtChallans = UtilityFunctions.GetData(query);
            ddlChallanNo.DataSource = dtChallans;
            ddlChallanNo.DataTextField = "ChallanInfo";
            ddlChallanNo.DataValueField = "ChallanInfo";
            ddlChallanNo.DataBind();
        }
        ddlChallanNo.Items.Insert(0, new ListItem("Select", ""));
        //updatePage.Update();
    }


    /// <summary>
    /// Generates a new Visit Number in Vendor Visit Detail Table and Redirects User to GenerateVendorVisitReport Page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGenerateVisitReport_Click(object sender, EventArgs e)
    {
        SqlConnection connec = new SqlConnection();
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            if (Session["DataTableAdvanceReport"] != null)
            {
                btnGenerateVisitReport.Visible = false;
                connec.ConnectionString = sqlconnstring;
                SqlCommand cmd = new SqlCommand();
                connec.Open();
                cmd.Connection = connec;
                cmd.CommandText = "select top 1 Visit_Number from Vendor_Visit_Detail order by Cast(Visit_Number as decimal) desc";
                string visitNumber = "";
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        int currentNum = Convert.ToInt32(reader["Visit_Number"]) + 1;
                        visitNumber = Convert.ToString(currentNum);
                        if (currentNum.ToString().Length < 3)
                        {
                            while (visitNumber.Length < 3)
                                visitNumber = "0" + visitNumber;
                        }
                    }
                    else
                    {
                        visitNumber = "001";
                    }
                }

                cmd.Parameters.Clear();
                cmd.CommandText = "insert into Vendor_Visit_Detail (Vendor_Name,Visit_Number,Created_By,Created_Date) Values(@VendorName,@VisitNumber,@CreatedBy,@CreatedDate);Select Scope_Identity();";
                cmd.Parameters.AddWithValue("@VendorName", ddlLocation.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@VisitNumber", visitNumber);
                cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                int ticketDetailId = Convert.ToInt32(cmd.ExecuteScalar());
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "window.open('GenerateVendorVisitReport.aspx?TicketDetailId=" + ticketDetailId + "');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('No Parts to Show on Vendor Visit Report.');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

}