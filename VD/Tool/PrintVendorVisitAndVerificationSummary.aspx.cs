﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Text;


public partial class PrintVendorVisitAndVerificationSummary : System.Web.UI.Page
{
    protected enum ReportType
    {
        VisitSummary,
        VerificationSummary
    }

    /// <summary>
    /// Generates the Visit/Verification Summary on the basis of Type (Visit/Verification), Date Range (Once Week) and Vendor Type (All/Internal/External)
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Type"]) && !string.IsNullOrEmpty(Request.QueryString["Range"]) && !string.IsNullOrEmpty(Request.QueryString["VendorType"]))
            {
                string[] dateRange = Convert.ToString(Request.QueryString["Range"]).Split('_');
                if (Convert.ToString(Request.QueryString["Type"]) == "VisitSummary")
                {
                    divPrintFirstReport.InnerHtml = GenerateReport(Convert.ToDateTime(dateRange[0]), Convert.ToDateTime(dateRange[1]), ReportType.VisitSummary, Convert.ToString(Request.QueryString["VendorType"]), "isnull(lig.quant_sent,0)- isnull(lig.quant_rec,0)>0").ToString();

                    StringBuilder sb = GenerateReport(Convert.ToDateTime(dateRange[0]), Convert.ToDateTime(dateRange[1]), ReportType.VisitSummary, Convert.ToString(Request.QueryString["VendorType"]), "isnull(lig.quant_sent,0)- isnull(lig.quant_rec,0)=0");
                    divPrintSecondReport.InnerHtml = sb.ToString().Replace("Visit Record", "<br/>");

                    string print = "<script language='javascript' type='text/javascript'> window.print(); </script>";
                    Page.RegisterStartupScript("Print", print);

                }
                else if (Convert.ToString(Request.QueryString["Type"]) == "VerificationSummary")
                {
                    divPrintFirstReport.InnerHtml = GenerateReport(Convert.ToDateTime(dateRange[0]), Convert.ToDateTime(dateRange[1]), ReportType.VerificationSummary, Convert.ToString(Request.QueryString["VendorType"]), "").ToString();

                    string print = "<script language='javascript' type='text/javascript'> window.print(); </script>";
                    Page.RegisterStartupScript("Print", print);
                }
            }
        }
    }

    /// <summary>
    /// Generates the Html as per passed Parameters from Vendor Master Job Work and Location Info Gr table
    /// </summary>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <param name="reportType"></param>
    /// <param name="vendorType"></param>
    /// <param name="conditon"></param>
    /// <returns></returns>
    protected StringBuilder GenerateReport(DateTime fromDate, DateTime toDate, ReportType reportType, string vendorType, string conditon)
    {
        StringBuilder sb = new StringBuilder();
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        List<DateTime> dates = new List<DateTime>();
        dates.Add(fromDate);
        while ((fromDate = fromDate.AddDays(1)) <= toDate)
        {
            dates.Add(fromDate);
        }
        try
        {
            string query = "";
            string type = "";
            if (vendorType == "Int")
            {
                type = "Internal Vendor";
            }
            else if (vendorType == "Ext")
            {
                type = "External Vendor";
            }
            else
            {
                type = "All";
            }

            if (reportType == ReportType.VerificationSummary)
            {
                if (type == "All")
                    query = "select vm.Allocated_To, vm.Vendor_Name, vm.Vendor_Type, Sum(isnull(lig.quant_sent,0)- isnull(lig.quant_rec,0)) as PendingQty, vm.Frequency_Of_Verification from Vendor_Master_Job_work vm left join Location_Info_Gr lig on vm.Vendor_Name=lig.Location where Vendor_Visit_Report_Required=1 and vm.Vendor_Type!='End Store' and  ( isnull(lig.quant_sent,0)- isnull(lig.quant_rec,0)>=0) and vm.Is_Active=1 and vm.Item_Pending_Report_Required=1 group by vm.Allocated_To, vm.Vendor_Name, vm.Vendor_Type,  vm.Frequency_Of_Verification order by vm.Vendor_Type ASC,vm.Allocated_To ASC, vm.Vendor_Name ASC";
                else
                    query = "select vm.Allocated_To, vm.Vendor_Name, vm.Vendor_Type, Sum(isnull(lig.quant_sent,0)- isnull(lig.quant_rec,0)) as PendingQty, vm.Frequency_Of_Verification from Vendor_Master_Job_work vm left join Location_Info_Gr lig on vm.Vendor_Name=lig.Location where Vendor_Visit_Report_Required=1 and vm.Vendor_Type!='End Store' and ( isnull(lig.quant_sent,0)- isnull(lig.quant_rec,0)>=0) and  vm.Is_Active=1 and vm.Item_Pending_Report_Required=1 and vm.Vendor_Type='" + type + "' group by vm.Allocated_To, vm.Vendor_Name, vm.Vendor_Type, vm.Frequency_Of_Verification order by vm.Allocated_To ASC,vm.Vendor_Name ASC";
            }
            else if (reportType == ReportType.VisitSummary)
            {
                query = "select vm.Allocated_To, vm.Vendor_Name, vm.Vendor_Type, Sum(isnull(lig.quant_sent,0)- isnull(lig.quant_rec,0)) as PendingQty, vm.Frequency_Of_Verification from Vendor_Master_Job_work vm left join Location_Info_Gr lig on vm.Vendor_Name=lig.Location where Vendor_Visit_Report_Required=1 and vm.Vendor_Type!='End Store' and  vm.Is_Active=1 and vm.Item_Pending_Report_Required=1 and vm.Vendor_Type='" + type + "' and " + conditon + " group by vm.Allocated_To, vm.Vendor_Name, vm.Vendor_Type, vm.Frequency_Of_Verification order by vm.Allocated_To ASC,PendingQty Desc,vm.Vendor_Name ASC";
            }
            SqlDataAdapter ad = new SqlDataAdapter(query, connec);
            DataTable dt = new DataTable();
            ad.Fill(dt);
            if (reportType == ReportType.VerificationSummary)
            {
                sb.Append("<div style='width:1100px;'><h2 style='text-align:center;'>Verification Record</h2>");
            }
            else if (reportType == ReportType.VisitSummary)
            {
                sb.Append("<div style='width:1100px;'><h2 style='text-align:center;'>Visit Record</h2>");
            }

            sb.Append("<table style='table-layout:fixed;width:100%;border:1px solid black;border-collapse:collapse;font-size: 12px;line-height: 22px;font-family:Verdana;text-align:center;'>");
            sb.Append("<tr><td style='border:1px solid black;font-weight:bold;width:40px;'> Sr. No.</td>");
            if (reportType == ReportType.VerificationSummary)
            {
                sb.Append("<td style='border:1px solid black;font-weight:bold;width:90px;'>Type Of Vendor</td>");
            }
            sb.Append("<td style='border:1px solid black;font-weight:bold;width:90px;'>Supervisor Name</td> <td style='border:1px solid black;font-weight:bold;width:140px;'>Vendor Name</td>");

            if (reportType == ReportType.VerificationSummary)
            {
                sb.Append("<td style='border:1px solid black;font-weight:bold;width:60px;'>Frequency</td>");
            }
            else if (reportType == ReportType.VisitSummary)
            {
                sb.Append("<td style='border:1px solid black;font-weight:bold;width:50px;'>Qty Pending</td>");
            }

            foreach (DateTime date in dates)
            {
                sb.Append("<td style='border:1px solid black;font-weight:bold;width:70px;'>" + date.ToString("dd-MM-yyyy") + "</td>");
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int srNo = i + 1;
                sb.Append("<tr><td style='border:1px solid black;'>" + srNo + "</td>");
                if (reportType == ReportType.VerificationSummary)
                {
                    sb.Append("<td style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["Vendor_Type"]) + "</td>");
                }
                sb.Append("<td style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["Allocated_To"]) + "</td> <td style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["Vendor_Name"]) + "</td>");
                if (reportType == ReportType.VerificationSummary)
                {
                    sb.Append("<td style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["Frequency_Of_Verification"]) + "</td>");
                }
                else if (reportType == ReportType.VisitSummary)
                {
                    sb.Append("<td style='border:1px solid black;'>" + Convert.ToInt32(dt.Rows[i]["PendingQty"]) + "</td>");
                }

                foreach (DateTime date in dates)
                {
                    sb.Append("<td style='border:1px solid black;'></td>");
                }
            }

            sb.Append("</tr>");
            sb.Append("</table></div>");
            return sb;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return sb;
        }
        finally
        {
            connec.Close();
        }



    }
}