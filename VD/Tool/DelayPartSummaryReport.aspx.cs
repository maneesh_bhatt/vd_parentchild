﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text;
using AjaxControlToolkit;

public partial class Tool_DelayPartSummaryReport : System.Web.UI.Page
{
    string station, part_name, str_dt, s;
    int tool_no, quant_sent, quant_rec, new_quantity_rec;
    string p_no = "";

    SqlConnection connection;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindVendor();
        }
    }

    /// <summary>
    /// Retrieves and Binds the List of Vendors from Vendor Master Job Work
    /// </summary>
    protected void BindVendor()
    {
        string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ";
        string dataTextField = "VENDOR_NAME";
        string dataValueField = "ID";
        UtilityFunctions.bind_vendor(ddllocation, query, dataTextField, dataValueField);
    }

    public void showgrid()
    {
        try
        {
            string searchCondition = "";
            if (ddllocation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.location='" + ddllocation.SelectedItem.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.location='" + ddllocation.SelectedItem.Text + "'";
                }
            }
            if (inputToolNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.tool_no='" + inputToolNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.tool_no='" + inputToolNo.Text + "'";
                }
            }
            if (inputStation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.station='" + inputStation.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.station='" + inputStation.Text + "'";
                }
            }
            if (inputPNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.p_no='" + inputPNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.p_no='" + inputPNo.Text + "'";
                }
            }
            if (inputReworkNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.rwk='" + inputReworkNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.rwk='" + inputReworkNo.Text + "'";
                }
            }
            if (ddlDateInterval.SelectedValue != "")
            {
                int month = Convert.ToInt32(ddlDateInterval.SelectedValue);
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,trd.Date_Of_Trans)>='" + Convert.ToDateTime(DateTime.Now.AddMonths(-month)).ToString("yyyy/MM/dd") + "' and Convert(Date,trd.Date_of_Trans)<='" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,trd.Date_Of_Trans)>='" + Convert.ToDateTime(DateTime.Now.AddMonths(-month)).ToString("yyyy/MM/dd") + "' and Convert(Date,trd.Date_of_Trans)<='" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd") + "'";
                }
            }
            else
            {
                if (inputFromDate.Text != "" && inputToDate.Text != "")
                {
                    if (searchCondition != "")
                    {
                        searchCondition = searchCondition + " and Convert(Date,trd.Date_Of_Trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,trd.Date_Of_Trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                    else
                    {
                        searchCondition = " Convert(Date,trd.Date_Of_Trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,trd.Date_Of_Trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                }
                else if (inputFromDate.Text != "" && inputToDate.Text == "")
                {
                    if (searchCondition != "")
                    {
                        searchCondition = searchCondition + " and Convert(Date,trd.Date_Of_Trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                    else
                    {
                        searchCondition = " Convert(Date,trd.Date_Of_Trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                }
                else if (inputFromDate.Text == "" && inputToDate.Text != "")
                {
                    if (searchCondition != "")
                    {
                        searchCondition = searchCondition + " and Convert(Date,trd.Date_Of_Trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                    else
                    {
                        searchCondition = " Convert(Date,trd.Date_Of_Trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                }
            }
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            connection = new SqlConnection(sqlconnstring);
            if (ddlDateInterval.SelectedValue != "" || (inputFromDate.Text != "" || inputToDate.Text != ""))
            {
                connection.Open();
                string query = "";
                if (!string.IsNullOrEmpty(searchCondition))
                {
                    // query = "select distinct lig.tool_no,lig.p_no,lig.part_name,lig.station,lig.Rwk ,lig.location,isnull(lig.quant_sent,0) as SentQuantity, isnull(lig.quant_rec,0) as ReceivedQuantity,lig.Upload_Qty,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as PendingQty from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>=0 and pgr.Is_Active_Record=1 and " + searchCondition + " and lig.location not in ('Assembly','VD Finish Store','Store Assembly','Spare Store') order by lig.tool_no,lig.station asc";
                    query = " select distinct lig.tool_no,lig.p_no,lig.part_name,lig.station,lig.Rwk ,lig.location,isnull(lig.quant_sent,0) as SentQuantity, isnull(lig.quant_rec,0) as ReceivedQuantity,lig.Upload_Qty,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as PendingQty, tb.Delivery_commit_date as DeliveryCommitDate,trd.date_of_trans as LastReceivingDate, DATEDIFF(Day,tb.Delivery_commit_date,trd.date_of_trans) as DelayDays from location_info_GR lig "
                        // + " left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no "
                          + " outer apply (select top 1 pg.Delivery_commit_date from part_history_GR pg where pg.tool_no=lig.tool_no and pg.p_no=lig.p_no and pg.part_name=lig.part_name and pg.station=lig.station and  pg.rwk_no= lig.Rwk and pg.sent_or_rec='Sent' and pg.location=lig.location order by pg.date_of_trans desc)tb "
                          + " cross apply (select top 1 pg.date_of_trans from part_history_GR pg where pg.tool_no=lig.tool_no and pg.p_no=lig.p_no and pg.part_name=lig.part_name and pg.station=lig.station and  pg.rwk_no= lig.Rwk and pg.sent_or_rec='Received' and pg.location=lig.location and Convert(Date,pg.date_of_trans) > Convert(Date,tb.Delivery_commit_date) order by pg.date_of_trans desc)trd "
                          + " where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>=0  and " + searchCondition + ""
                        //+ " and pgr.Is_Active_Record=1 
                          + " and lig.location not in ('Assembly','VD Finish Store','Store Assembly','Spare Store') order by lig.tool_no,lig.station asc ";
                }

                SqlDataAdapter ad = new SqlDataAdapter(query, connection);
                ad.SelectCommand.CommandTimeout = 0;
                DataTable dt = new DataTable();
                ad.Fill(dt);

                SqlCommand cmd = new SqlCommand();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cmd = new SqlCommand("select * from parts_Gr where tool_no=" + dt.Rows[i]["tool_no"] + " and p_no='" + dt.Rows[i]["p_no"] + "' and part_name='" + dt.Rows[i]["part_name"] + "' and station='" + dt.Rows[i]["station"] + "' and rwk_no='" + dt.Rows[i]["rwk"] + "' and Is_Active_Record=1", connection);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (!rdr.HasRows)
                        {
                            dt.Rows.RemoveAt(i);
                        }
                    }
                }

                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    btnDownload.Visible = true;
                }
                else
                {
                    dt = null;
                    btnDownload.Visible = false;
                }
                GridView1.DataBind();
                Session["DataTableAdvanceReport"] = dt;
                GridView1.EmptyDataText = "No Records Found";
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Time Interval or Select From Date or To Date');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        showgrid();
        //if (GridView1.Rows.Count == 1)
        //    lbl.Text = "No previous data exists..!!";
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        showgrid();
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        showgrid();
    }


    void load(GridViewUpdateEventArgs e)
    {
        //location = ddllocation.SelectedItem.Text;
        tool_no = Convert.ToInt32(((Label)GridView1.Rows[e.RowIndex].FindControl("Label1")).Text);
        station = ((Label)GridView1.Rows[e.RowIndex].FindControl("Label2")).Text.ToString();
        p_no = ((LinkButton)GridView1.Rows[e.RowIndex].FindControl("Label3")).Text.ToString();
        part_name = ((Label)GridView1.Rows[e.RowIndex].FindControl("Label4")).Text.ToString();
        quant_sent = Convert.ToInt32(((Label)GridView1.Rows[e.RowIndex].FindControl("Label5")).Text);
        quant_rec = Convert.ToInt32(((Label)GridView1.Rows[e.RowIndex].FindControl("Label6")).Text);
        new_quantity_rec = Convert.ToInt32(((TextBox)GridView1.Rows[e.RowIndex].FindControl("TextBox2")).Text);
        DateTime str_dt1 = System.DateTime.Now;
        str_dt = string.Format("{0:yyyy-MM-dd}", str_dt1); // you can specify format 
        CultureInfo provider = CultureInfo.InvariantCulture;
        DateTime.ParseExact(str_dt, "yyyy-m-d", provider);
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }


    //FIXME :: Rename the widgets and the corresponding signal functions.
    protected void Label3_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        try
        {
            string query = "select  id,location as Party,date_of_trans as Trans_Date,sent_or_rec as Trans_Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as PendingTotQty_in_PPC,TransSentQty as SendQty_Or_ReceivedQty  from part_history_GR  where   p_no='" + Positionno + "' and  tool_no='" + toolno + "' and  station='" + station + "' and  part_name='" + part_name + "' and rwk_no='" + rwk_no + "' order by date_of_trans asc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();

            da.Fill(table);

            pnlHistoryPopUp.InnerHtml = GetHtmlContent(table);
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    protected string GetHtmlContent(DataTable table)
    {

        StringBuilder b = new StringBuilder();
        b.Append("<table style='");
        b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
        b.Append("<tr>");
        foreach (DataColumn column in table.Columns)
        {
            b.Append("<th>");
            b.Append(column.ColumnName);
            b.Append("</th>");
        }
        b.Append("</tr>");


        foreach (DataRow row in table.Rows)
        {
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<td>");
                b.Append(row[column.ColumnName]);
                b.Append("</td>");
            }
            b.Append("</tr>");
        }
        b.Append("</table>");
        return b.ToString();
    }

    protected void lnkBtnLastCommitmentDate_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";
        string location = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        location = List[5];
        try
        {
            string id = "";
            SqlCommand cmd = new SqlCommand("select top 1 id from part_history_Gr where p_no='" + Positionno + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk_no='" + rwk_no + "' and tool_no='" + toolno + "' and location='" + location + "'  and sent_or_rec='Sent' order by date_of_trans desc", conn);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    reader.Read();
                    id = Convert.ToString(reader["id"]);
                }
            }

            string query = "select  Tool_No,P_No,Part_Name,Station,Rework_No,Location,Commitment_Date  from CommitmentHistory  where  Part_History_Id=" + id + " union all select  Tool_No,P_No,Part_Name,Station,Rwk_No,Location,Delivery_commit_date  from part_history_Gr  where  id=" + id + " order by Commitment_Date desc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();
            da.Fill(table);

            pnlCommitmentHistory.InnerHtml = GetHtmlContent(table);
            dialogCommitmentHistory.Attributes.Add("style", "display:block;");
            overlayCommitmentHistory.Attributes.Add("style", "display:block;");
        }
        catch (Exception ex)
        {
            ex.ToString();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured. Please Contact Administrator.');", true);
            return;
        }
        finally
        {
            conn.Close();
        }
    }


    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }

    protected void btnCloseCommitmentHistory_Click(object sender, EventArgs e)
    {
        dialogCommitmentHistory.Attributes.Add("style", "display:none;");
        overlayCommitmentHistory.Attributes.Add("style", "display:none;");
    }

    protected void ExportToExcel()
    {
        Response.ClearContent();
        string fileName = "DelayedPartData";
        string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GridView1.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

}
