﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class Tool_PositionNotRequiredReport : System.Web.UI.Page
{
    public static string searchCondition;
    protected void Page_Load(object sender, EventArgs e)
    {
        searchCondition = "";

        try
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Binds the Data that is Uploaded as Not Required from Parts Gr Table
    /// </summary>
    protected void BindGrid()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            string searchCondition = "";

            if (inputToolNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and pgr.tool_no='" + inputToolNo.Text + "'";
                }
                else
                {
                    searchCondition = "  pgr.tool_no='" + inputToolNo.Text + "'";
                }
            }
            if (inputStation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and pgr.station='" + inputStation.Text + "'";
                }
                else
                {
                    searchCondition = "  pgr.station='" + inputStation.Text + "'";
                }
            }
            if (inputPNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and pgr.p_no='" + inputPNo.Text + "'";
                }
                else
                {
                    searchCondition = "  pgr.p_no='" + inputPNo.Text + "'";
                }
            }
            if (inputReworkNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and pgr.rwk_no='" + inputReworkNo.Text + "'";
                }
                else
                {
                    searchCondition = "  pgr.rwk_no='" + inputReworkNo.Text + "'";
                }
            }
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = "";
            if (!string.IsNullOrEmpty(searchCondition))
            {
                query = "select pgr.tool_no,pgr.station,pgr.p_no,pgr.part_name,pgr.rwk_no,pgr.Gr_Reference_No as RefGrNo,pgr.req_qty as not_required_qty,pgr.Type,pgr.Version_Number from  parts_GR pgr where Type='NotRequired' and Gr_Reference_No is not null and Gr_Reference_No!='' and " + searchCondition + " order by pgr.tool_no,pgr.station asc";
                adapter = new SqlDataAdapter(query, con);
            }
            else
            {
                query = "select pgr.tool_no,pgr.station,pgr.p_no,pgr.part_name,pgr.rwk_no,pgr.Gr_Reference_No as RefGrNo,pgr.req_qty as not_required_qty,pgr.Type,pgr.Version_Number from  parts_GR pgr where Type='NotRequired' and Gr_Reference_No is not null and Gr_Reference_No!='' order by pgr.tool_no,pgr.station asc";
                adapter = new SqlDataAdapter(query, con);
            }
            ViewState["CurrentQuery"] = query;
            DataTable td = new DataTable();
            adapter.Fill(td);
            gridPositionNotRequired.DataSource = td;
            gridPositionNotRequired.DataBind();
            gridPositionNotRequired.EmptyDataText = "No Record Found";
            lblRecorrdsCount.Text = Convert.ToString(td.Rows.Count) + " " + "Records In Total";
            if (gridPositionNotRequired.Rows.Count > 0)
            {
                btnDownload.Visible = true;
            }
            else
            {
                btnDownload.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }
    }

    protected void gridPositionNotRequired_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // Set the index of the new display page.  
        gridPositionNotRequired.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    /// <summary>
    /// Exports the Data on the Grid View as per Search Criteria
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDownload_Click(object sender, EventArgs e)
    {
        try
        {
            UtilityFunctions.ExportToExcel(Convert.ToString(ViewState["CurrentQuery"]), "PositionsNotRequired");
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}