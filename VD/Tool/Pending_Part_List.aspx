﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.master"
    CodeFile="Pending_Part_List.aspx.cs" Inherits="Pending_Part_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <style type="text/css">
        .paging
        {
        }
        
        .paging a
        {
            background-color: #00C157;
            padding: 5px 7px;
            text-decoration: none;
            border: 1px solid #00C157;
        }
        
        .paging a:hover
        {
            background-color: #E1FFEF;
            color: #00C157;
            border: 1px solid #00C157;
        }
        
        .paging span
        {
            background-color: #E1FFEF;
            padding: 5px 7px;
            color: #00C157;
            border: 1px solid #00C157;
        }
        
        tr.paging
        {
            background: none !important;
        }
        
        tr.paging tr
        {
            background: none !important;
        }
        tr.paging td
        {
            border: none;
        }
        
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="margin-left: 25px" align="left" width="850px">
        <tr>
            <td colspan="53" align="center" style="font-size: x-large; color: black">
                <span class="style1">Pending Part List </span>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td height="30px" colspan="51">
                Tool No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="tbTool" CssClass="glowing-border" runat="server" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td height="30px" colspan="51">
                Station No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="tbStation" CssClass="glowing-border" runat="server" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td height="30px" colspan="51">
                Position&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                <asp:TextBox ID="txtposition" CssClass="glowing-border" runat="server" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td valign="top" height="30px">
                Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="tbLocation" CssClass="glowing-border" runat="server" Width="200px"></asp:TextBox>
            </td>
            <td valign="top" align="left" height="30px ">
                <asp:ImageButton ID="Button1" runat="server" CausesValidation="false" ImageUrl="~/images/searchbtn.jpg"
                    OnClick="Button1_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td valign="top" height="30px">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <span style="color: Blue;">
                <asp:Label ID="lblTotal" runat="server" Text="Total:"></asp:Label><asp:Label ID="lblTotal1"
                    runat="server" Text=""></asp:Label></span></td>
        </tr>
        <tr>
            <td>
            </td>
            <td height="10px" colspan="51">
                <asp:Label ID="lbl" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">
                <td colspan="52">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server" Width="900px" ScrollBars="Horizontal">
                                <asp:GridView ID="GridView1" runat="server" PagerStyle-CssClass="paging" CssClass="myGridStyle"
                                    AutoGenerateColumns="false" AllowPaging="True" EmptyDataText="No Record Found"
                                    ForeColor="#333333" GridLines="None" OnPageIndexChanging="GridView1_PageIndexChanging"
                                    Font-Size="13px" Width="100%" OnRowDataBound="GridView1_RowDataBound">
                                    <RowStyle BackColor="#00CC00" ForeColor="#333333" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="60px" ItemStyle-Width="60"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Tool No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("tool_no") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Station
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("station") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="80px" ItemStyle-Font-Size="13px"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Position No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="link" Text='<%# Eval("p_no") %>' runat="server" NavigateUrl='<%# string.Format("../Tool/PartHistory.aspx?p_no={0}&part_name={1}&tool_no={2}&station={3}&req_qty={4}",
                    HttpUtility.UrlEncode(Eval("p_no").ToString()), HttpUtility.UrlEncode(Eval("part_name").ToString()), HttpUtility.UrlEncode(Eval("tool_no").ToString()),HttpUtility.UrlEncode(Eval("station").ToString()),HttpUtility.UrlEncode(Eval("req_qty").ToString())) %>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="170px" ItemStyle-Width="170"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Part Name
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("part_name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Location
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("location") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                Upload_Qty
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Upload_Qty") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                Quantity Pending
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" Text='<%# Eval("qtypending") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="75px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Sent_date
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label50" runat="server" Text='<%# Eval("Sent_Received_Date","{0:dd/MM/yyyy HH:mm:ss tt}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="75px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Received_date
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label60" runat="server" Text='<%# Eval("Received_Date","{0:dd/MM/yyyy HH:mm:ss tt}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                </asp:GridView>
                            </asp:Panel>
                            </td> </td> </tr>
                        </ContentTemplate>
                    </asp:UpdatePanel>
    </table>
</asp:Content>
