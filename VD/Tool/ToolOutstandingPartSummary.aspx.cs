﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web.Services;
using System.Web.Script.Services;
using System.Reflection;

public partial class Tool_ToolOutstandingPartSummary : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindVendors();
        }
    }

    /// <summary>
    /// Binds the List of Vendors from Vendor Master Job Work Table
    /// </summary>
    protected void BindVendors()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select Vendor_Name from Vendor_Master_Job_Work where Vendor_Type='End Store'", conn);
            checkEndStores.DataSource = cmd.ExecuteReader();
            checkEndStores.DataTextField = "Vendor_Name";
            checkEndStores.DataValueField = "Vendor_Name";
            checkEndStores.DataBind();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ShowGrid();
    }

    /// <summary>
    /// Retrieves the Summary of Outstanding Parts from Location Info Gr table. Displays the summary of Pending Parts along with Delay Duration
    /// </summary>
    protected void ShowGrid()
    {

        Session["DataTableAdvanceReport"] = null;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            string searchCondition = "";
            if (inputVendorName.Text != "")
            {
                searchCondition = "lig.Location='" + inputVendorName.Text + "'";
            }
            if (inputToolNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + "and lig.Tool_No='" + inputToolNo.Text + "'";
                }
                else
                {
                    searchCondition = " lig.Tool_No='" + inputToolNo.Text + "'";
                }
            }
            string endStoreCondition = "";
            foreach (ListItem checkList in checkEndStores.Items)
            {
                if (!checkList.Selected)
                {
                    endStoreCondition = endStoreCondition + "\'" + checkList.Text + "\',";
                }
            }
            endStoreCondition = endStoreCondition.TrimEnd(',');
            if (!string.IsNullOrEmpty(endStoreCondition))
            {
                if (!string.IsNullOrEmpty(searchCondition))
                {
                    searchCondition = searchCondition + " and lig.location not in (" + endStoreCondition + ")";
                }
                else
                {
                    searchCondition = " lig.location not in (" + endStoreCondition + ")";
                }
            }
            if (chkViewAllTools.Checked)
            {
                if (inputToDate.Text != "")
                {
                    searchCondition = " tbSent.First_Sent_Date<= " + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "";
                }
            }
            if (string.IsNullOrEmpty(searchCondition))
            {
                searchCondition = "1=1";
            }
            DataTable dt = new DataTable();
            //code changed by himaneesh also adding required by date
            SqlDataAdapter ad = new SqlDataAdapter("select pgr.Parts_Gr_Id , lig.Tool_No,lig.Location,lig.Station, lig.P_No,lig.Rwk, lig.Part_Name,lig.Upload_Qty,lig.Quant_Sent,lig.Quant_Rec,isnull(lig.Quant_Sent,0)-isnull(lig.Quant_Rec,0) as SentQty, "
               + " Convert(Date,lastCommitDate.Delivery_Commit_Date) as Delivery_Commit_Date, Convert(Date,lig.Required_Date) as Required_By_Date,tbSent.First_Sent_Date,  Convert(Date,initialDelCommit.Commitment_Date) as Original_Commit_Date, lig.Sent_Received_Date "
               + " from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no "
               + " and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no "
               + " outer apply  (select top 1 pg.Delivery_commit_date from part_history_GR pg where pg.tool_no=lig.tool_no and pg.p_no=lig.p_no and pg.part_name=lig.part_name and "
               + " pg.station=lig.station and  pg.rwk_no= lig.Rwk and pg.sent_or_rec='Sent' and pg.location=lig.location order by pg.id desc)lastCommitDate "
               + " outer apply  (select top 1 ch.Commitment_Date from CommitmentHistory ch where ch.tool_no=lig.tool_no and ch.p_no=lig.p_no and ch.part_name=lig.part_name and "
               + " ch.station=lig.station and  ch.Rework_No= lig.Rwk and ch.location=lig.location order by ch.Part_History_Id asc)initialDelCommit "
               + " outer apply  (select top 1 pg.date_of_trans as First_Sent_Date from part_history_GR pg where pg.tool_no=lig.tool_no and pg.p_no=lig.p_no and pg.part_name=lig.part_name and "
               + " pg.station=lig.station and  pg.rwk_no= lig.Rwk and pg.sent_or_rec='Sent' and pg.location=lig.location order by pg.id asc)tbSent "
               + " where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) "
               + "  and " + searchCondition, conn);

            ad.SelectCommand.CommandTimeout = 0;
            ad.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                btnGenerateVisitReport.Visible = true;
                btnDownload.Visible = true;
            }
            else
            {
                btnGenerateVisitReport.Visible = false;
                btnDownload.Visible = false;
            }
            Session["DataTableAdvanceReport"] = dt;
            DataTable dtOutStandingParts = new DataTable();
            dtOutStandingParts.Columns.Add("Tool", typeof(int));
            dtOutStandingParts.Columns.Add("Location", typeof(string));
            dtOutStandingParts.Columns.Add("station", typeof(string));
            dtOutStandingParts.Columns.Add("p_no", typeof(string));
            dtOutStandingParts.Columns.Add("rwk", typeof(string));
            dtOutStandingParts.Columns.Add("part_name", typeof(string));
            dtOutStandingParts.Columns.Add("Upload_Qty", typeof(string));
            dtOutStandingParts.Columns.Add("Quant_Sent", typeof(string));
            dtOutStandingParts.Columns.Add("Quant_Rec", typeof(string));
            dtOutStandingParts.Columns.Add("SentQty", typeof(string));
            dtOutStandingParts.Columns.Add("First_Sent_Date", typeof(DateTime));
            dtOutStandingParts.Columns.Add("Required_By_Date", typeof(DateTime));//code changed by himaneesh
            dtOutStandingParts.Columns.Add("Original_Commit_Date", typeof(DateTime));
            dtOutStandingParts.Columns.Add("Delivery_Commit_Date", typeof(DateTime));
            dtOutStandingParts.Columns.Add("Delay_From_Original_Date", typeof(string));
            dtOutStandingParts.Columns.Add("Delay_From_Latest_Date", typeof(string));
            dtOutStandingParts.Columns.Add("Upload_Date", typeof(DateTime));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                try
                {
                    DataRow dr = dtOutStandingParts.NewRow();
                    string Positionno = Convert.ToString(dt.Rows[i]["p_no"]);
                    string part_name = Convert.ToString(dt.Rows[i]["part_name"]);
                    string station = Convert.ToString(dt.Rows[i]["station"]);
                    string rwk_no = Convert.ToString(dt.Rows[i]["rwk"]);
                    string toolno = Convert.ToString(dt.Rows[i]["tool_no"]);
                    string location = Convert.ToString(dt.Rows[i]["location"]);
                    String requiredbydate = (dt.Rows[i]["Required_By_Date"] == DBNull.Value) ? "" : Convert.ToDateTime(dt.Rows[i]["Required_By_Date"]).ToString("dd-MM-yyyy");//code changed by himaneesh
                    string lastDeliveryCommitDate = (dt.Rows[i]["Delivery_Commit_Date"] == DBNull.Value) ? "" : Convert.ToString(dt.Rows[i]["Delivery_commit_date"]);
                    conn.Open();
                    string id = "";
                    SqlCommand cmd = new SqlCommand("select top 1 id from part_history_Gr where p_no='" + Positionno + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk_no='" + rwk_no + "' and tool_no='" + toolno + "' and location='" + location + "'  and sent_or_rec='Sent' order by date_of_trans desc", conn);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            id = Convert.ToString(reader["id"]);
                        }
                    }
                    string originalCommitDate = "", delayFromOriginalCommitDate = "", delayFromLatestCommitDate = "", uploadDate = "";
                    string query = "select  Tool_No,P_No,Part_Name,Station,Rework_No,Location,Commitment_Date  from CommitmentHistory  where  Part_History_Id=" + id + " union all select  Tool_No,P_No,Part_Name,Station,Rwk_No,Location,Delivery_commit_date  from part_history_Gr  where  id=" + id + " order by Commitment_Date asc";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    DataTable table = new DataTable();
                    da.Fill(table);
                    if (table.Rows.Count > 0)
                    {
                        if (table.Rows[0]["Commitment_Date"] != DBNull.Value)
                            originalCommitDate = Convert.ToDateTime(table.Rows[0]["Commitment_Date"]).ToString("dd-MM-yyyy");
                    }


                    if (!string.IsNullOrEmpty(originalCommitDate))
                    {
                        string commitDate = Convert.ToDateTime(originalCommitDate).ToString("dd-MM-yyyy");
                        int days = Convert.ToDateTime(DateTime.Now).Subtract(Convert.ToDateTime(commitDate)).Days;
                        if (days > 0)
                        {
                            delayFromOriginalCommitDate = Convert.ToString(days);
                        }
                    }
                    if (!string.IsNullOrEmpty(lastDeliveryCommitDate))
                    {
                        string commitDate = Convert.ToDateTime(lastDeliveryCommitDate).ToString("dd-MM-yyyy");
                        int days = Convert.ToDateTime(DateTime.Now).Subtract(Convert.ToDateTime(commitDate)).Days;
                        if (days > 0)
                        {
                            delayFromLatestCommitDate = Convert.ToString(days);
                        }
                    }

                    using (SqlCommand cmdUploadDate = new SqlCommand("select date_of_trans from part_history_Gr where tool_no=@ToolNo and Station=@Station and p_no=@PNo and part_name=@PartName and Rwk_No=@RwkNo and Location='Company'", conn))
                    {
                        cmdUploadDate.Parameters.AddWithValue("@ToolNo", Convert.ToString(dt.Rows[i]["Tool_No"]));
                        cmdUploadDate.Parameters.AddWithValue("@PNo", Convert.ToString(dt.Rows[i]["P_No"]));
                        cmdUploadDate.Parameters.AddWithValue("@PartName", Convert.ToString(dt.Rows[i]["Part_Name"]));
                        cmdUploadDate.Parameters.AddWithValue("@Station", Convert.ToString(dt.Rows[i]["Station"]));
                        cmdUploadDate.Parameters.AddWithValue("@RwkNo", Convert.ToString(dt.Rows[i]["Rwk"]));
                        using (SqlDataReader rdr = cmdUploadDate.ExecuteReader())
                        {
                            rdr.Read();
                            uploadDate = Convert.ToDateTime(rdr["Date_Of_Trans"]).ToString("dd-MM-yyyy");
                        }
                    }


                    dr["Tool"] = toolno;
                    dr["Location"] = location;
                    dr["station"] = station;
                    dr["p_no"] = Positionno;
                    dr["rwk"] = rwk_no;
                    dr["part_name"] = part_name;
                    dr["Upload_Qty"] = dt.Rows[i]["Upload_Qty"];
                    dr["Quant_Sent"] = dt.Rows[i]["Quant_Sent"];
                    dr["Quant_Rec"] = dt.Rows[i]["Quant_Rec"];
                    dr["SentQty"] = dt.Rows[i]["SentQty"];
                    dr["First_Sent_Date"] = dt.Rows[i]["First_Sent_Date"];

                    //code changed by himaneesh
                    if(!string.IsNullOrEmpty(requiredbydate))//checking required by date should not be empty or null to avoid 1900 date
                    {
                        dr["Required_By_Date"] = requiredbydate;
                    }
                    if(requiredbydate == "01-01-1900")//by default date is this but it reduntantly creates confusion
                    {
                        dr["Required_By_Date"] = DBNull.Value;
                    }
                    if (!string.IsNullOrEmpty(originalCommitDate))
                        dr["Original_Commit_Date"] = originalCommitDate;
                    if (!string.IsNullOrEmpty(lastDeliveryCommitDate))
                        dr["Delivery_Commit_Date"] = lastDeliveryCommitDate;
                    dr["Delay_From_Original_Date"] = delayFromOriginalCommitDate;
                    dr["Delay_From_Latest_Date"] = delayFromLatestCommitDate;
                    if (!string.IsNullOrEmpty(uploadDate))
                        dr["Upload_Date"] = uploadDate;
                    dtOutStandingParts.Rows.Add(dr);

                }
                catch (Exception ex)
                {
                    ex.ToString();
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured. Please Contact Administrator.');", true);
                    return;
                }
                finally
                {
                    conn.Close();
                }
            }

            DataView dv = new DataView(dtOutStandingParts);
            if (chkViewAllTools.Checked)
            {
                dv.Sort = " Upload_Date ASC, Rwk ASC, Tool ASC, Station ASC";
            }
            else if (chkSortGrid.Checked)
            {
                dv.Sort = " Tool ASC, Station ASC, Upload_Date ASC, Rwk ASC";
            }
            else
            {
                dv.Sort = " Location ASC, Delay_From_Original_Date DESC";
            }
            gridViewVendorPendingItems.DataSource = dv;
            gridViewVendorPendingItems.DataBind();
            gridViewVendorPendingItems.EmptyDataText = "No Records Found";
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }

    protected void ExportToExcel()
    {
        Response.ClearContent();
        string fileName = "Tool_Location_Outstanding_Data_Report";
        string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gridViewVendorPendingItems.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    protected void gridViewVendorPendingItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }


    /// <summary>
    /// Retrives the List of vendors from Vendor Master Job Work table
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetVendorName(string text)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            List<string> allItems = new List<string>();
            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrWhiteSpace(text))
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work where Vendor_Name like '%" + text + "%' and Vendor_Name not in ('VD Finish Store','Assembly','FinishPartStore','weld vd finish store','Weld Parts Consumed','Reject Parts') order by Vendor_Name asc ", con);
            }
            else
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work where Vendor_Name not in ('VD Finish Store','Assembly','FinishPartStore','weld vd finish store','Weld Parts Consumed','Reject Parts') order by Vendor_Name asc ", con);
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string vendorName = Convert.ToString(reader["Vendor_Name"]);
                allItems.Add(vendorName);
            }
            reader.Close();
            return allItems;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    /// <summary>
    /// Retrives the List of Distinct Tools from Part History Gr table
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetTooNumber(string text)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            List<string> allItems = new List<string>();
            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrWhiteSpace(text))
            {
                cmd = new SqlCommand("select distinct tool_no from part_history_GR where tool_no is not null and tool_no!='' and tool_no like '%" + text + "%'  order by tool_no ASC", con);
            }
            else
            {
                cmd = new SqlCommand("select distinct tool_no from part_history_GR where tool_no is not null and tool_no!='' order by tool_no asc ", con);
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string vendorName = Convert.ToString(reader["tool_no"]);
                allItems.Add(vendorName);
            }
            reader.Close();
            return allItems;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            con.Close();
        }
    }


    protected void chkAllEndStores_CheckedChanged(object sender, EventArgs e)
    {
        foreach (ListItem checkList in checkEndStores.Items)
        {
            if (chkAllEndStores.Checked)
            {
                checkList.Selected = true;
            }
            else
            {
                checkList.Selected = false;
            }
        }
    }

    /// <summary>
    /// Generates the Visit Number and Redirects user to PrintPendingItemsVendorVisitReport with that VendorVisitId
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGenerateVisitReport_Click(object sender, EventArgs e)
    {
        SqlConnection connec = new SqlConnection();
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            if (Session["DataTableAdvanceReport"] != null)
            {
                btnGenerateVisitReport.Visible = false;
                connec.ConnectionString = sqlconnstring;
                SqlCommand cmd = new SqlCommand();
                connec.Open();
                cmd.Connection = connec;
                cmd.CommandText = "select top 1 Visit_Number from Vendor_Visit_Detail order by Cast(Visit_Number as decimal) desc";
                string visitNumber = "";
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        int currentNum = Convert.ToInt32(reader["Visit_Number"]) + 1;
                        visitNumber = Convert.ToString(currentNum);
                        if (currentNum.ToString().Length < 3)
                        {
                            while (visitNumber.Length < 3)
                                visitNumber = "0" + visitNumber;
                        }
                    }
                    else
                    {
                        visitNumber = "001";
                    }
                }

                cmd.Parameters.Clear();
                cmd.CommandText = "insert into Vendor_Visit_Detail (Vendor_Name,Visit_Number,Created_By,Created_Date) Values(@VendorName,@VisitNumber,@CreatedBy,@CreatedDate);Select Scope_Identity();";
                cmd.Parameters.AddWithValue("@VendorName", inputVendorName.Text);
                cmd.Parameters.AddWithValue("@VisitNumber", visitNumber);
                cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                int ticketDetailId = Convert.ToInt32(cmd.ExecuteScalar());
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "window.open('PrintPendingItemsVendorVisitReport.aspx?TicketDetailId=" + ticketDetailId + "');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Send part before Print');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void chkSortGrid_CheckedChanged(object sender, EventArgs e)
    {
        ShowGrid();
    }

    /// <summary>
    /// Displays the Sent/Received Summary of Position on clicking of any Postion
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkBtnPNumber_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        try
        {
            string query = "select  id,location as Party,date_of_trans as Trans_Date,sent_or_rec as Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as Pending_Qty_PPC,TransSentQty as Send_Or_Rec_Qty,User_Name  from part_history_GR  where   p_no='" + Positionno + "' and  tool_no='" + toolno + "' and  station='" + station + "' and  part_name='" + part_name + "' and rwk_no='" + rwk_no + "' order by date_of_trans asc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();

            da.Fill(table);

            StringBuilder b = new StringBuilder();

            b.Append("<table style='");
            b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<th>");
                b.Append(column.ColumnName);
                b.Append("</th>");
            }
            b.Append("</tr>");

            foreach (DataRow row in table.Rows)
            {
                b.Append("<tr>");
                foreach (DataColumn column in table.Columns)
                {
                    b.Append("<td>");
                    b.Append(row[column.ColumnName]);
                    b.Append("</td>");
                }
                b.Append("</tr>");
            }
            b.Append("</table>");

            pnlHistoryPopUp.InnerHtml = b.ToString();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }

    protected void chkViewAllTools_CheckedChanged(object sender, EventArgs e)
    {
        inputToolNo.Text = String.Empty;
        inputVendorName.Text = String.Empty;
        chkSortGrid.Checked = false;
        if (chkViewAllTools.Checked)
        {
            divRecordsUptoDate.Visible = true;
        }
        else
        {
            divRecordsUptoDate.Visible = false;
        }
    }
}