﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text;
using AjaxControlToolkit;
using System.IO;

public partial class Tool_InspectionSheetSubmissionByQcSheetNumber : System.Web.UI.Page
{
    string location, station, part_name, str_dt, s;
    string approvername = "";
    int tool_no, quant_sent, quant_rec, new_quantity_rec, quant_pend;
    string p_no = "";

    SqlConnection connection;
    SqlCommand cmd;

    int countchk = 0;
    static bool isRequestedForReceive = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        txtReceivecdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
        if (!IsPostBack)
        {
            isRequestedForReceive = false;
            Panel4.Visible = true;
            Panel1.Visible = false;
            txtchallanno.Visible = false;
            lblChallanno.Visible = false;
            btnprint.Visible = false;
            btnRecieved.ID = "btnRecieved";
        }
    }

    /// <summary>
    /// Retrieves the Data which is sent to Inspection by given QC Sheet Number and displays it to the User.
    /// User can fill this data, once user saves this data it is inserted into InspectionSheetSubmissionHistory table
    /// </summary>
    public void showgrid()
    {
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            connection = new SqlConnection(sqlconnstring);
            connection.Open();
            if (txtQCSheetNo.Text != "")
            {
                cmd = new SqlCommand(" select lig.tool_no,lig.station,lig.Upload_Qty,lig.p_no,lig.part_name,lig.quant_sent,lig.quant_rec,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as SentQty,lig.Rwk ,lig.Extra_Received_Quantity from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no left join temp_part_history_QC_GR tphq on lig.tool_no=tphq.tool_no and lig.p_no=tphq.p_no and lig.part_name=tphq.part_name and lig.station=tphq.station and lig.rwk= tphq.rwk_no where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and lig.location=' Inspection' and tphq.Qcsheet_no='" + txtQCSheetNo.Text.TrimEnd() + "'  order by lig.tool_no,lig.station asc");
                cmd.Connection = connection;
                DataTable dt = new DataTable();

                dt.Load(cmd.ExecuteReader());
                Panel1.Visible = true;
                if (dt.Rows.Count <= 0)
                {
                    dt = null;
                }
                btnRecieved.Visible = false;
                GridView1.DataSource = dt;
                Session["DataTableAdvanceReport"] = dt;
                GridView1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Enter QC Sheet Number');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        showgrid();
        if (GridView1.Rows.Count == 1)
            lblChallanno.Text = "No previous data exists..!!";
    }

    protected string CalculateTime(string opr16, decimal quantSent)
    {
        string resultText = "";
        decimal result = 0;
        decimal totalTime = 0;
        bool isActiveOperation = decimal.TryParse(opr16, out result);
        if (isActiveOperation)
        {
            decimal roundValue = Math.Round(result * 60);
            totalTime = quantSent * roundValue;
        }
        if (totalTime > 60)
        {
            int hours = 0, minutes = 0;
            hours = Convert.ToInt32(totalTime) / 60;
            minutes = Convert.ToInt32(totalTime) - (60 * hours);
            resultText = hours + ":" + minutes;
        }
        else if (totalTime > 0)
        {
            if (totalTime < 10)
            {
                resultText = "00:" + "0" + Convert.ToInt32(totalTime);
            }
            else
            {
                resultText = "00:" + Convert.ToInt32(totalTime);
            }
        }
        return resultText;
    }

    /// <summary>
    /// Retrieves the Operation Hours (Opr16 Column) for Inspeciton from Parts Gr table which is uploaded initially by User
    /// Retrieves the Last Expected delivery Date from Part_History_Gr table
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label l = (Label)e.Row.FindControl("Label5");
            Label lblLastCommitmentDate = (Label)e.Row.FindControl("lblLastCommitmentDate");
            DropDownList ddlInspectedBy = (DropDownList)e.Row.FindControl("ddlInspectedBy");
            string inspectors = File.ReadAllText(Server.MapPath("~/Pages/InspectingTeamList.txt"));
            string[] inspectorsList = inspectors.Split(',');
            foreach (var list in inspectorsList)
            {
                ListItem listItem = new ListItem();
                listItem.Value = list;
                listItem.Text = list;
                ddlInspectedBy.Items.Add(listItem);
            }

            CheckBox cb = (CheckBox)e.Row.FindControl("chkAllchild");
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection connec = new SqlConnection(sqlconnstring);
            connec.Open();
            string pno = "", partname = "", station = "", rwkno = "", toolno = "";
            pno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "p_no"));
            partname = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "part_name"));
            station = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "station"));
            rwkno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "rwk"));
            toolno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "tool_no"));
            decimal quant_sent = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "quant_sent"));
            decimal quant_rec = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "quant_rec"));
            decimal quant_pend = quant_sent - quant_rec;
            Label lblEstimatedHrs = (Label)e.Row.FindControl("lblEstimatedHrs");
            try
            {
                SqlCommand cmd = new SqlCommand();

                cmd = new SqlCommand("select opr16 from Parts_Gr where tool_no='" + toolno + "'  and  p_no='" + pno + "' and part_name='" + partname + "' and station='" + station + "' and rwk_no='" + rwkno + "' order by Parts_Gr_Id desc", connec);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        lblEstimatedHrs.Text = CalculateTime(Convert.ToString(reader["Opr16"]), quant_pend);
                    }
                }

                cmd = new SqlCommand("select top 1 Delivery_commit_date from Part_History_Gr where tool_no='" + toolno + "'  and  p_no='" + pno + "' and part_name='" + partname + "' and location=' Inspection' and station='" + station + "' and rwk_no='" + rwkno + "' and sent_or_rec='Sent' order by date_of_trans desc", connec);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        lblLastCommitmentDate.Text = Convert.ToString(reader["Delivery_commit_date"]);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
            }
            finally
            {
                connec.Close();
            }
        }
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        showgrid();
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        showgrid();
    }


    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    protected void chkAllchild_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)GridView1.HeaderRow.FindControl("chkAll1");
        txtchallanno.Text = String.Empty;
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            if (ChkBoxRows.Checked == true)
            {
                ChkBoxRows.Checked = true;
                countchk = countchk + 1;
                GridView1.Columns[7].Visible = true;
                btnRecieved.Visible = true;
                EnableTextBox();
            }
            else
            {
                ChkBoxRows.Checked = false;
                int count = int.Parse(GridView1.Rows.Count.ToString());
                for (int i = 0; i < count; i++)
                {
                    CheckBox cb = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkAllchild");
                    if (cb.Checked == true)
                    {
                        GridView1.Columns[7].Visible = true;
                        GridView1.Columns[9].Visible = true;

                    }
                    else
                    {
                        GridView1.Columns[7].Visible = true;
                        GridView1.Columns[9].Visible = true;
                        btnRecieved.Visible = false;
                    }
                }
            }
        }

        if (countchk > 0)
        {
            GridView1.Columns[7].Visible = true;
            GridView1.Columns[9].Visible = true;
            btnRecieved.Visible = true;

        }
        else
        {
            btnRecieved.Visible = true;
            GridView1.Columns[7].Visible = false;
            GridView1.Columns[9].Visible = false;
            btnRecieved.Visible = false;

        }
        UpdatePanel1.Update();
    }

    protected void chkAll1_CheckedChanged1(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)GridView1.HeaderRow.FindControl("chkAll1");
        txtchallanno.Text = String.Empty;
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
                countchk = countchk + 1;
                EnableTextBox();
            }
            else
            {
                ChkBoxRows.Checked = false;
                int count = int.Parse(GridView1.Rows.Count.ToString());
            }
        }
        UpdatePanel1.Update();
    }

    protected void EnableTextBox()
    {
        int count = int.Parse(GridView1.Rows.Count.ToString());

        for (int i = 0; i < count; i++)
        {
            CheckBox cb = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkAllchild");
            if (cb.Checked == true)
            {
                btnRecieved.Visible = true;
                TextBox TextBox2 = (TextBox)GridView1.Rows[i].Cells[7].FindControl("TextBox2");
                Label Label6sentqty = (Label)GridView1.Rows[i].Cells[9].FindControl("Label6sentqty");
                if (Label6sentqty.Text.Length > 0 && TextBox2.Text.Length == 0)
                {
                    TextBox2.Text = Label6sentqty.Text;
                    TextBox2.Enabled = true;
                }
            }
            else
            {
                TextBox TextBox2 = (TextBox)GridView1.Rows[i].Cells[7].FindControl("TextBox2");
                TextBox2.Text = "";
                TextBox2.Enabled = false;
                btnRecieved.Visible = false;
            }
        }
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// When user clicks on Receive button data is sent to Company and Data is also inserted in InspectionSheetSubmissionHistory
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRecieved_Click(object sender, EventArgs e)
    {

        btnRecieved.Attributes.Add("onclick", " this.disabled = true; " + ClientScript.GetPostBackEventReference(btnRecieved, null) + ";");
        approvername = Session["UserID"].ToString();
        approvername = approvername.Replace("_", " ");

        int count = 0;
        int uploadqty1 = 0;
        object request_id = "";

        string sqlconnstring1 = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring1);
        SqlTransaction trans = null;
        if (!isRequestedForReceive)
        {
            if (txtReceivecdate.Text.Length < 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Enter Received Date');", true);
                return;
            }
            try
            {
                string prefixStr = "CH";
                txtchallanno.Text = UtilityFunctions.gencode(prefixStr);

                txtchallanno.Visible = true;
                lblChallanno.Visible = true;
                btnprint.Visible = true;

                conn.Open();
                trans = conn.BeginTransaction();
                foreach (GridViewRow row in GridView1.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        DateTime str_dt1 = Convert.ToDateTime(txtReceivecdate.Text);
                        str_dt = string.Format("{0:yyyy-MM-dd}", str_dt1); // you can specify format 
                        Label quant_sent1 = row.FindControl("Label5") as Label;
                        Label quant_rec1 = row.FindControl("Label6") as Label;
                        TextBox new_quantity_rec1 = row.FindControl("TextBox2") as TextBox;
                        Label station1 = row.FindControl("Label2") as Label;
                        Label rework_no = row.FindControl("lblrew") as Label;
                        Label lblrew = row.FindControl("lblrew") as Label;

                        Label tool_no1 = row.FindControl("Label1") as Label;
                        LinkButton POSITION = row.FindControl("Label3") as LinkButton;
                        Label part_name1 = row.FindControl("Label4") as Label;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        Label quantity = row.FindControl("Label8") as Label;
                        Label uploadqty = row.FindControl("Label6Upload_Qty") as Label;
                        Label sent = row.FindControl("Labelsent") as Label;
                        Label lblEstimatedHrs = row.FindControl("lblEstimatedHrs") as Label;
                        TextBox inputActualHrs = row.FindControl("inputActualHrs") as TextBox;
                        TextBox inputActualMins = row.FindControl("inputActualMins") as TextBox;
                        DropDownList ddlInspectedBy = row.FindControl("ddlInspectedBy") as DropDownList;

                        quant_sent = Convert.ToInt32(quant_sent1.Text);
                        quant_rec = Convert.ToInt32(quant_rec1.Text);
                        uploadqty1 = Convert.ToInt32(uploadqty.Text);
                        tool_no = Convert.ToInt32(tool_no1.Text);
                        p_no = POSITION.Text;
                        station = station1.Text;
                        part_name = Convert.ToString(part_name1.Text);
                        location = " Inspection";
                        string kk = System.DateTime.Now.ToLongTimeString();
                        str_dt = str_dt + " " + kk;

                        bool isValidData = false;
                        bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                        if (isSelected)
                        {
                            if (ddlInspectedBy.SelectedValue == "" || inputActualHrs.Text.Length == 0)
                            {
                                txtchallanno.Text = String.Empty;
                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Enter Actual Hours and Inspected By Values.');", true);
                                return;
                            }
                            else
                            {
                                SqlCommand cmdCheckQuantity = new SqlCommand("select tool_no,station,Upload_Qty,p_no,part_name,quant_sent,quant_rec,isnull(quant_sent,0)-isnull(quant_rec,0) as SentQty,Rwk from location_info_GR where location=' Inspection' and (isnull(quant_sent,0)-isnull(quant_rec,0))>0 and location=' Inspection' and p_no='" + p_no + "' and part_name='" + part_name + "' and tool_no='" + tool_no + "' "
                                + " and station='" + station + "' and rwk='" + rework_no.Text + "'  order by tool_no,station asc", conn, trans);
                                using (SqlDataReader reader = cmdCheckQuantity.ExecuteReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        int quantSent = Convert.ToInt32(reader["quant_sent"]);
                                        int quantReceive = Convert.ToInt32(reader["quant_rec"]);
                                        new_quantity_rec = Convert.ToInt32(new_quantity_rec1.Text);
                                        if ((quantSent) >= new_quantity_rec + quantReceive)
                                        {
                                            isValidData = true;
                                        }
                                    }
                                }
                            }

                            if (!isValidData)
                            {
                                txtchallanno.Text = String.Empty;
                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Quantity Entered. Quantiy has been modified Please refresh the Page.');", true);
                                return;
                            }
                            if (new_quantity_rec1.Text.Length < 0)
                            {
                                txtchallanno.Text = String.Empty;
                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Enter Valid Receive Qty ');", true);
                                return;
                            }
                            new_quantity_rec = Convert.ToInt32(new_quantity_rec1.Text);
                            if ((quant_sent) < new_quantity_rec + quant_rec)
                            {
                                txtchallanno.Text = String.Empty;
                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Receive Qty should not be greater than sent Qty');", true);
                                return;
                            }
                            if (Convert.ToInt32(new_quantity_rec1.Text) > 0)
                            {
                                s = "insert into part_history_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,TransSentQty,Upload_Qty,Challan_No,rwk_no) values     (@p_no,@part_name,@tool_no,@station,@location,@date_of_trans,@sent_or_rec,@quantity,@TransSentQty,@Upload_Qty,@Challan_No,@Rwk);SELECT SCOPE_IDENTITY()";
                                cmd = new SqlCommand(s, conn, trans);
                                cmd.Parameters.AddWithValue("@p_no", p_no);
                                cmd.Parameters.AddWithValue("@part_name", part_name);
                                cmd.Parameters.AddWithValue("@tool_no", tool_no);
                                cmd.Parameters.AddWithValue("@station", station);
                                cmd.Parameters.AddWithValue("@location", location);
                                cmd.Parameters.AddWithValue("@date_of_trans", str_dt);
                                cmd.Parameters.AddWithValue("@sent_or_rec", "Received");
                                cmd.Parameters.AddWithValue("@quantity", new_quantity_rec);
                                cmd.Parameters.AddWithValue("@TransSentQty", new_quantity_rec);
                                cmd.Parameters.AddWithValue("@Upload_Qty", uploadqty1);
                                cmd.Parameters.AddWithValue("@Challan_No", txtchallanno.Text);
                                cmd.Parameters.AddWithValue("@Rwk", lblrew.Text);
                                request_id = cmd.ExecuteScalar();
                                Session["Requestid"] = request_id;
                                Session["Challano"] = txtchallanno.Text;
                                Session["Location"] = location;

                                string s1 = "insert into part_history_QC_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,TransSentQty,Upload_Qty,Challan_No,User_name,Used_Date,rwk_no) values     (@p_no,@part_name,@tool_no,@station,@location,@date_of_trans,@sent_or_rec,@quantity,@TransSentQty,@Upload_Qty,@Challan_No,@user_name,@useddate,@rwkno);SELECT SCOPE_IDENTITY()";
                                cmd = new SqlCommand(s1, conn, trans);
                                cmd.Parameters.AddWithValue("@p_no", p_no);
                                cmd.Parameters.AddWithValue("@part_name", part_name);
                                cmd.Parameters.AddWithValue("@tool_no", tool_no);
                                cmd.Parameters.AddWithValue("@station", station);
                                cmd.Parameters.AddWithValue("@location", location);
                                cmd.Parameters.AddWithValue("@date_of_trans", str_dt);
                                cmd.Parameters.AddWithValue("@sent_or_rec", "Received");
                                cmd.Parameters.AddWithValue("@quantity", new_quantity_rec);
                                cmd.Parameters.AddWithValue("@TransSentQty", new_quantity_rec);
                                cmd.Parameters.AddWithValue("@Upload_Qty", uploadqty1);
                                cmd.Parameters.AddWithValue("@Challan_No", txtchallanno.Text);
                                cmd.Parameters.AddWithValue("@user_name", approvername);
                                cmd.Parameters.AddWithValue("@useddate", System.DateTime.Now);
                                cmd.Parameters.AddWithValue("@rwkno", lblrew.Text);
                                cmd.ExecuteNonQuery();

                                //UPDATING LOCATION INFO TABLE
                                string r_no = rework_no.Text;

                                string strQuery3 = "update part_history_GR set Received_Date='" + str_dt + "' , quantity=isnull(quantity,0) + " + new_quantity_rec + ", QtySent=isnull(QtySent,0) - " + new_quantity_rec + "  where p_no='" + p_no + "' and part_name='" + part_name + "' and location='COMPANY' and station='" + station + "' and rwk_no='" + r_no + "' ";
                                SqlCommand cmd3 = new SqlCommand(strQuery3, conn, trans);
                                cmd3.ExecuteNonQuery();

                                //1)UPDATING TOTAL QUANTITY RECEIVED,PENDING QUANTITY
                                s = "select isnull(quant_rec,0) from part_history_GR where tool_no=@tool_no and station=@station and p_no=@p_no and  part_name=@part_name and location=@location and rwk_no=@rwkno";
                                cmd = new SqlCommand(s, conn, trans);
                                cmd.Parameters.AddWithValue("@p_no", p_no);
                                cmd.Parameters.AddWithValue("@part_name", part_name);
                                cmd.Parameters.AddWithValue("@tool_no", tool_no);
                                cmd.Parameters.AddWithValue("@station", station);
                                cmd.Parameters.AddWithValue("@location", location);
                                cmd.Parameters.AddWithValue("@rwkno", lblrew.Text);

                                quant_rec += Convert.ToInt32(cmd.ExecuteScalar());
                                quant_pend = quant_sent - quant_rec;

                                //2) NOW UPDATING LOCATION_INFO TABLE

                                string strQuery2 = "update location_info_GR set  quant_rec=" + new_quantity_rec + " + isnull(quant_rec,0),  quant_pend=isnull(quant_pend,0)-" + new_quantity_rec + ", Received_Date='" + str_dt1.ToString("yyyy/MM/dd HH:mm:ss") + "' where p_no='" + p_no + "' and station='" + station + "'  and location='" + location + "' and part_name='" + part_name + "' and tool_no=" + tool_no + " and rwk='" + lblrew.Text + "' ";
                                SqlCommand cmd2 = new SqlCommand(strQuery2, conn, trans);
                                cmd2.ExecuteNonQuery();
                                count = count + 1;


                                string actualTime = inputActualHrs.Text + ":" + inputActualMins.Text;
                                //Inserting into InspectionSheet Submission Summary Table
                                string inspectionQuery = "insert into InspectionSheetSubmissionHistory(p_no,part_name,tool_no,station,rework_no,qc_sheet_number,received_quantity,estimated_time,actual_time,inspected_by,challan_no,created_by,created_date) values(@p_no,@part_name,@tool_no,@station,@rework_no,@qc_sheet_number,@received_quantity,@estimated_time,@actual_time,@inspected_by,@challan_no,@created_by,@created_date);";
                                cmd = new SqlCommand(inspectionQuery, conn, trans);
                                cmd.Parameters.AddWithValue("@p_no", p_no);
                                cmd.Parameters.AddWithValue("@part_name", part_name);
                                cmd.Parameters.AddWithValue("@tool_no", tool_no);
                                cmd.Parameters.AddWithValue("@station", station);
                                cmd.Parameters.AddWithValue("@rework_no", lblrew.Text);
                                cmd.Parameters.AddWithValue("@qc_sheet_number", txtQCSheetNo.Text);
                                cmd.Parameters.AddWithValue("@received_quantity", new_quantity_rec);
                                cmd.Parameters.AddWithValue("@estimated_time", lblEstimatedHrs.Text);
                                cmd.Parameters.AddWithValue("@actual_time", actualTime);
                                cmd.Parameters.AddWithValue("@inspected_by", ddlInspectedBy.SelectedValue);
                                cmd.Parameters.AddWithValue("@challan_no", txtchallanno.Text);
                                cmd.Parameters.AddWithValue("@user_name", approvername);
                                cmd.Parameters.AddWithValue("@created_by", Session["UserID"]);
                                cmd.Parameters.AddWithValue("@created_date", DateTime.Now);
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                txtchallanno.Text = String.Empty;
                txtQCSheetNo.Text = String.Empty;
                count = 0;
                isRequestedForReceive = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
                ex.ToString();
            }
            finally
            {
                conn.Close();
            }

            if (count > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Receive Item Successfully ');", true);
                btnRecieved.Visible = false;
            }
        }
        conn.Close();
        showgrid();
        isRequestedForReceive = false;
    }

    protected void btnprint_Click(object sender, EventArgs e)
    {
        Session["MIN"] = txtchallanno.Text;
        if (txtchallanno.Text.Length > 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "window.open('Item_Challan.aspx?ProcessType=Receive&Vendor=Inspection');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Receive some part before Print');", true);
            return;
        }
    }

    /// <summary>
    /// Validates the Entered Quantity, also retrieves the Estimated Hrs from Parts Gr table.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TextBox2_TextChanged(object sender, EventArgs e)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            conn.Open();
            TextBox row = (TextBox)sender;
            bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;

            TextBox TextBox2 = row.FindControl("TextBox2") as TextBox;
            Label Label6sentqty = row.FindControl("Label6sentqty") as Label;
            Label uploadqty = row.FindControl("Label6Upload_Qty") as Label;
            Label lblEstimatedHrs = row.FindControl("lblEstimatedHrs") as Label;
            Label lblToolNo = row.FindControl("Label1") as Label;
            Label lblStation = row.FindControl("Label2") as Label;
            LinkButton lblPNo = row.FindControl("Label3") as LinkButton;
            Label lblPartName = row.FindControl("Label4") as Label;
            Label lblrew = row.FindControl("lblrew") as Label;
            Label sent = row.FindControl("Labelsent") as Label;
            if (isSelected)
            {
                if (!(Label6sentqty.Text.Length > 0))
                    Label6sentqty.Text = "0";
                if (!(TextBox2.Text.Length > 0))
                    TextBox2.Text = "0";
                if (!(uploadqty.Text.Length > 0))
                    uploadqty.Text = "0";

                if (Convert.ToInt32(TextBox2.Text.ToString()) > Convert.ToInt32(Label6sentqty.Text.ToString()))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('New Recive Qty Should be less than  or equal Total Qty');", true);
                    TextBox2.Text = "0";
                    TextBox2.Focus();
                    return;
                }
                SqlCommand cmd = new SqlCommand("select opr16 from Parts_Gr where tool_no='" + lblToolNo.Text + "'  and  p_no='" + lblPNo.Text + "' and part_name='" + lblPartName.Text + "' and station='" + lblStation.Text + "' and rwk_no='" + lblrew.Text + "' order by Parts_Gr_Id desc", conn);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        lblEstimatedHrs.Text = CalculateTime(Convert.ToString(reader["Opr16"]), Convert.ToDecimal(TextBox2.Text));
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }

    }

    /// <summary>
    /// Validates if date entered is future or past date, returns error if its past or future date
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtReceivecdate_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDateTime(txtReceivecdate.Text) > DateTime.Now || Convert.ToDateTime(txtReceivecdate.Text) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Can not take future/past date');", true);
            txtReceivecdate.Text = "";
        }
    }

    /// <summary>
    /// Displays the History of Position on Postion number click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Label3_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        try
        {
            string query = "select  id,location as Party,date_of_trans as Trans_Date,sent_or_rec as Trans_Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as PendingTotQty_in_PPC,TransSentQty as SendQty_Or_ReceivedQty  from part_history_GR  where   p_no='" + Positionno + "' and  tool_no='" + toolno + "' and  station='" + station + "' and  part_name='" + part_name + "' and rwk_no='" + rwk_no + "' order by date_of_trans asc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();

            da.Fill(table);

            StringBuilder b = new StringBuilder();

            b.Append("<table style='");
            b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<th>");
                b.Append(column.ColumnName);
                b.Append("</th>");
            }
            b.Append("</tr>");


            foreach (DataRow row in table.Rows)
            {
                b.Append("<tr>");
                foreach (DataColumn column in table.Columns)
                {
                    b.Append("<td>");
                    b.Append(row[column.ColumnName]);
                    b.Append("</td>");
                }
                b.Append("</tr>");
            }
            b.Append("</table>");

            pnlHistoryPopUp.InnerHtml = b.ToString();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }
}
