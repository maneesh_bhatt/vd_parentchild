﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class Pending_Part_List_GR : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            showgrid();
        }
    }

    /// <summary>
    /// Binds the Data with the GridView on click of Search Button, User can View the Position History through this Page
    /// </summary>
    public void showgrid()
    {
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);

            string s = " select lig.tool_no,lig.Rework,lig.station,lig.Rwk,lig.p_no,lig.part_name,lig.req_qty,lig.Upload_Qty,lig.location,(isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0)) as qtypending,lig.quant_sent,lig.quant_rec,lig.quant_pend,lig.quant_others, lig.Sent_Received_Date,lig.Received_Date from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where (isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0))>0 and  (lig.location<>'Assembly' and lig.location<>'VD Finish Store') and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) ";
            string s1 = "lig.tool_no=@tool_no";
            string s2 = "lig.station=@station";
            string s4 = "lig.location=@location";
            string s3 = "lig.p_no=@p_no";
            string GR = "lig.Rwk=@Gr";

            if (tbTool.Text != "")
            {
                s = s + " and " + s1;
            }
            if (tbStation.Text != "")
            {
                s = s + " and " + s2;
            }
            if (tbLocation.Text != "")
            {
                s = s + " and " + s3;
            }
            if (txtposition.Text != "")
            {
                s = s + " and " + s4;
            }
            if (txtGr.Text != "")
            {
                s = s + " and " + GR;
            }

            SqlCommand cmd = new SqlCommand(s);
            cmd.Parameters.Add(new SqlParameter("@tool_no", tbTool.Text));
            cmd.Parameters.Add(new SqlParameter("@station", tbStation.Text));
            cmd.Parameters.Add(new SqlParameter("@location", tbLocation.Text));
            cmd.Parameters.Add(new SqlParameter("@p_no", txtposition.Text));
            cmd.Parameters.Add(new SqlParameter("@Gr", txtGr.Text));
            cmd.CommandType = CommandType.Text;
            conn.Open();
            cmd.Connection = conn;
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            Session["DataTableAdvanceReport"] = dt;
            GridView1.DataSource = dt;
            lblTotal1.Text = dt.Rows.Count.ToString();
            GridView1.DataBind();
            conn.Close();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

   
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        DataTable table = (DataTable)Session["DataTableAdvanceReport"];
        GridView1.DataSource = table;
        GridView1.DataBind();
    }
    
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label result = (Label)e.Row.FindControl("Label60");
            if (result.Text == "01/01/1900 00:00:00 AM")
            {
                result.Text = "";
            }
            else
            {

            }
        }
    }
    
    protected void Button1_Click(object sender, ImageClickEventArgs e)
    {
        showgrid();
    }
}