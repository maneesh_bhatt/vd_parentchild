﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using FileReader;
using CustomFunctions;

public partial class Tool_UpdateVendorMasterData : System.Web.UI.Page
{
    private static bool[] rightsArray;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            SetControlsRights();
        }
    }

    /// <summary>
    /// User access right is Validated. Is user does not have access rights then user is rediredcted to Welcome Page with No Rights Message
    /// </summary>
    protected void SetControlsRights()
    {
        try
        {
            rightsArray = AccessRights.SetUserAccessRights();
            if (!rightsArray[5])
            {
                Response.Redirect("../Pages/WelcomePage.aspx?Error=NoRights");
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

    }

    /// <summary>
    /// Event is fired when user presses the Upload Button after browsing the file. File Format is Validated
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UploadButton_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (!FileUploadControl.HasFile)
            {
                lblErrorMessage.Text = "You have not selected any file !!!";
                return;
            }
            string datetime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
            string fileName = Path.GetFileNameWithoutExtension(FileUploadControl.PostedFile.FileName) + "_" + datetime + "" + Path.GetExtension(FileUploadControl.PostedFile.FileName);
            string fileExtension = Path.GetExtension(FileUploadControl.PostedFile.FileName);
            if (!Directory.Exists(Server.MapPath("~/UploadedExcelFiles")))
            {
                Directory.CreateDirectory(Server.MapPath("~/UploadedExcelFiles"));
            }
            string fileLocation = Server.MapPath("~/UploadedExcelFiles/" + fileName);
            FileUploadControl.SaveAs(fileLocation);

            if (!(fileExtension == ".xls") && !(fileExtension == ".xlsx"))
            {
                lblErrorMessage.Text = "Invalid file !!! Select only an excel file";
                return;
            }
            UpdateVendorData(fileLocation);
        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// Vendor Data is Validated and Vendor Data is Validated in Vendor Master Job Work table
    /// </summary>
    /// <param name="path"></param>
    public void UpdateVendorData(string path)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        SqlTransaction trans = null;
        try
        {
            lblErrorMessage.Text = String.Empty;

            string pathOnly = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);


            int count = 1;
            conn.Open();
            trans = conn.BeginTransaction();
            DataSet ds = null;
            DataTable dtprojectItems = new DataTable();
            using (clsFileReadHelper csv = new clsFileReadHelper(path))
            {
                ds = csv.getFileData();
            }
            if (ds.Tables.Count > 0)
            {
                dtprojectItems = ds.Tables[0].Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
            }

            string errorAtRows = "";
            for (int i = 1; i < dtprojectItems.Rows.Count; i++)
            {
                string allocatedTo = "", inchargePhoneNo = "", supplierName = "", supplierContactNo = "";
                bool isActive = false;


                if (dtprojectItems.Rows[i][7] != DBNull.Value)
                    allocatedTo = Convert.ToString(dtprojectItems.Rows[i][7]);

                if (dtprojectItems.Rows[i][8] != DBNull.Value)
                    inchargePhoneNo = Convert.ToString(dtprojectItems.Rows[i][8]);

                if (dtprojectItems.Rows[i][10] != DBNull.Value)
                    isActive = (Convert.ToString(dtprojectItems.Rows[i][10]).ToLower() == "yes") ? true : false;
                 
                if (dtprojectItems.Rows[i][4] != DBNull.Value)
                    supplierName = Convert.ToString(dtprojectItems.Rows[i][4]);

                if (dtprojectItems.Rows[i][3] != DBNull.Value)
                    supplierContactNo = Convert.ToString(dtprojectItems.Rows[i][3]);


                if (isActive && (string.IsNullOrEmpty(allocatedTo) || string.IsNullOrEmpty(inchargePhoneNo) || string.IsNullOrEmpty(supplierName) || string.IsNullOrEmpty(supplierContactNo)))
                {
                    int currentRow = i;
                    errorAtRows = errorAtRows + "," + currentRow;
                }
            }
            errorAtRows = errorAtRows.TrimStart(',');
            if (!string.IsNullOrEmpty(errorAtRows))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Vendor can not be made as Active if Allocated To/ Incharge Contact No/ Supplier Name/ Supplier Contact No is not provided in Excel. Please check Row No- " + errorAtRows + "');", true);
                return;
            }
            for (int i = 1; i < dtprojectItems.Rows.Count; i++)
            {
                if (dtprojectItems.Columns.Count != 16)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Invalid File Selected. No Of Columns does not Match with Actual Format.');", true);
                    return;
                }

                string vendorName = "", vendorType = "", tinNo = "", phoneNumber = "", contactPerson = "", address = "", emailAddress = "", allocatedTo = "", inchargePhoneNo = "", frequencyOfVerification = "", gstNo = "";
                int standardDays = 0;
                bool isActive = false, isItemPendingReportRequired = false, isNameRequiredForVendorVerificationList = false, isVendorVisitReportRequired = false;
                isActive = Convert.ToString(dtprojectItems.Rows[i][9]) == "Yes" ? true : false;

                vendorName = Convert.ToString(dtprojectItems.Rows[i][0]);
                vendorType = Convert.ToString(dtprojectItems.Rows[i][1]);
                address = Convert.ToString(dtprojectItems.Rows[i][2]);
                phoneNumber = Convert.ToString(dtprojectItems.Rows[i][3]);
                contactPerson = Convert.ToString(dtprojectItems.Rows[i][4]);
                emailAddress = Convert.ToString(dtprojectItems.Rows[i][5]);
                tinNo = Convert.ToString(dtprojectItems.Rows[i][6]);
                if (dtprojectItems.Rows[i][7] != DBNull.Value)
                    allocatedTo = Convert.ToString(dtprojectItems.Rows[i][7]);
                if (dtprojectItems.Rows[i][8] != DBNull.Value)
                    inchargePhoneNo = Convert.ToString(dtprojectItems.Rows[i][8]);
                if (dtprojectItems.Rows[i][9] != DBNull.Value)
                    standardDays = Convert.ToInt32(dtprojectItems.Rows[i][9]);
                isActive = (Convert.ToString(dtprojectItems.Rows[i][10]).ToLower() == "yes") ? true : false;
                isItemPendingReportRequired = (Convert.ToString(dtprojectItems.Rows[i][11]).ToLower() == "yes") ? true : false;
                isNameRequiredForVendorVerificationList = (Convert.ToString(dtprojectItems.Rows[i][12]).ToLower() == "yes") ? true : false;
                isVendorVisitReportRequired = (Convert.ToString(dtprojectItems.Rows[i][13]).ToLower() == "yes") ? true : false;
                if (dtprojectItems.Rows[i][14] != DBNull.Value)
                    frequencyOfVerification = Convert.ToString(dtprojectItems.Rows[i][14]);
                if (dtprojectItems.Rows[i][15] != DBNull.Value)
                    gstNo = Convert.ToString(dtprojectItems.Rows[i][15]);


                SqlCommand cmd = new SqlCommand();
                if (vendorName != "")
                {
                    DateTime uploaddate = new DateTime();
                    uploaddate = DateTime.Now;
                    string id = "";
                    cmd = new SqlCommand("select id from Vendor_Master_Job_Work where Vendor_Name=@VendorName", conn, trans);
                    cmd.Parameters.AddWithValue("@VendorName", vendorName);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            id = Convert.ToString(rdr["Id"]);
                        }
                    }

                    if (!string.IsNullOrEmpty(id))
                    {
                        cmd = new SqlCommand("update Vendor_Master_Job_work set  Vendor_Address=@VendorAddress,Phone=@Phone, Contact_Person=@ContactPerson, Email=@Email, Tin_Number=@TinNumber,Allocated_To=@AllocatedTo,Incharge_Phone_No=@InchargePhoneNo,Standard_Days=@StandardDays,Is_Active=@IsActive,Item_Pending_Report_Required=@ItemPendingReportRequired,Name_Required_For_Vendor_Verification_List=@NameRequiredForVendorVerificationList,Frequency_Of_Verification=@FrequencyOfVerification,Vendor_Visit_Report_Required=@VendorVisitReportRequired,GST_Number=@GSTNumber where Id=@Id", conn, trans);
                        cmd.Parameters.AddWithValue("@VendorAddress", address);
                        cmd.Parameters.AddWithValue("@Phone", phoneNumber);
                        cmd.Parameters.AddWithValue("@ContactPerson", contactPerson);
                        cmd.Parameters.AddWithValue("@Email", emailAddress);
                        cmd.Parameters.AddWithValue("@TinNumber", tinNo);
                        cmd.Parameters.AddWithValue("@Id", id);
                        cmd.Parameters.AddWithValue("@AllocatedTo", allocatedTo);
                        cmd.Parameters.AddWithValue("@InchargePhoneNo", inchargePhoneNo);
                        cmd.Parameters.AddWithValue("@StandardDays", standardDays);
                        cmd.Parameters.AddWithValue("@IsActive", isActive);
                        cmd.Parameters.AddWithValue("@ItemPendingReportRequired", isItemPendingReportRequired);
                        cmd.Parameters.AddWithValue("@NameRequiredForVendorVerificationList", isNameRequiredForVendorVerificationList);
                        cmd.Parameters.AddWithValue("@VendorVisitReportRequired", isVendorVisitReportRequired);
                        cmd.Parameters.AddWithValue("@FrequencyOfVerification", frequencyOfVerification);
                        cmd.Parameters.AddWithValue("@GSTNumber", gstNo);
                        cmd.ExecuteNonQuery();
                        count = count + 1;
                    }
                    //else
                    //{

                    //    cmd = new SqlCommand("insert into Vendor_Master_Job_work (Vendor_Name,Vendor_Address,Phone, Contact_Person, Email, Tin_Number,Allocated_To,Incharge_Phone_No,Standard_Days) Values(@VendorName,@VendorAddress,@Phone,@ContactPerson,@Email,@TinNumber,@AllocatedTo,@InchargePhoneNo,@StandardDays)", conn, trans);
                    //    cmd.Parameters.AddWithValue("@VendorName", vendorName);
                    //    cmd.Parameters.AddWithValue("@VendorAddress", address);
                    //    cmd.Parameters.AddWithValue("@Phone", phoneNumber);
                    //    cmd.Parameters.AddWithValue("@ContactPerson", contactPerson);
                    //    cmd.Parameters.AddWithValue("@Email", emailAddress);
                    //    cmd.Parameters.AddWithValue("@TinNumber", tinNo);
                    //    cmd.Parameters.AddWithValue("@AllocatedTo", allocatedTo);
                    //    cmd.Parameters.AddWithValue("@InchargePhoneNo", inchargePhoneNo);
                    //    cmd.Parameters.AddWithValue("@StandardDays", standardDays);
                    //    cmd.ExecuteNonQuery();
                    //}
                }
            }
            trans.Commit();
            lblErrorMessage.Text = "Vendor Data Updated Successfully. Rows Updated-" + count;
        }
        catch (Exception ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }
}