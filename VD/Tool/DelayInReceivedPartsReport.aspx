﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="DelayInReceivedPartsReport.aspx.cs" Inherits="Tool_DelayInReceivedPartsReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style2
        {
            border: none;
            width: 100px;
        }
        
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>
    <style type="text/css">
        .Grid
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .Grid td
        {
            background-color: Highlight;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Grid th
        {
            background-color: Navy;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .ChildGrid td
        {
            background-color: #eee !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .ChildGrid th
        {
            background-color: #6C6C6C !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid td
        {
            background-color: #fff !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid th
        {
            background-color: #2B579A !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=imgChallanItems]").each(function () {
                if ($(this)[0].src.indexOf("minus") != -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
                    $(this).next().remove();
                }
            });
        });


        $(function () {

            $('#<%=inputToolNo.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ToolOutstandingPartSummary.aspx/GetTooNumber",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputToolNo.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputToolNo.ClientID%>').blur();

            });

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="width: 100%; border: 1px solid Blue; min-height: 100px;">
        <div style="padding: 9px;">
            <div>
                <div align="center">
                    <span class="style13">Delay in Received Parts Summary Report</span>
                </div>
            </div>
            <div style="margin-bottom: 10px; margin-top: 20px;">
                <div style="font-size: 13px; width: 95px; float: left;">
                    Start Date
                    <br />
                    <asp:TextBox ID="inputFromDate" CssClass="glowing-border" runat="server" Width="85px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                        Enabled="true" TargetControlID="inputFromDate">
                    </cc1:CalendarExtender>
                </div>
                <div style="font-size: 13px; width: 95px; float: left;">
                    End Date
                    <br />
                    <asp:TextBox ID="inputToDate" CssClass="glowing-border" runat="server" Width="85px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="true" Format="dd-MMM-yyyy"
                        TargetControlID="inputToDate">
                    </cc1:CalendarExtender>
                </div>
                <div style="font-size: 13px; width: 190px; float: left;">
                    Tool No
                    <asp:TextBox runat="server" ID="inputToolNo" ClientIDMode="Static" CssClass="autocomplete glowing-border"
                        Style="width: 170px;" Placeholder=" Tool Number"></asp:TextBox>
                </div>
                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/images/searchbtn.jpg"
                    OnClick="btnSearch_Click" Style="margin-top: 10px;" />
                &nbsp;&nbsp; <span style="color: Green;">Note: Report is shown on the basis of Last
                    Sent Date</span>
                <%--    <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="btnDownload_Click"
                    Style="margin-top: 9px; margin-right: -4px; float: right; background-color: green;
                    color: white; padding: 6px; border: 1px solid green; box-shadow: 1px 1px 3px green;" />--%>
            </div>
            <asp:GridView runat="server" ID="gridViewVendorPendingItems" AutoGenerateColumns="false"
                Width="100%" ShowHeaderWhenEmpty="true" OnRowDataBound="gridViewVendorPendingItems_RowDataBound"
                AllowPaging="false" AllowSorting="false" Style="font-size: 11.5px; font-family: Verdana;
                line-height: 26px;">
                <RowStyle BackColor="White" ForeColor="Black" Font-Size="12px" />
                <Columns>
                    <asp:TemplateField HeaderStyle-BackColor="#335599">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgChallanItems" runat="server" OnClick="Show_Hide_ChallanItemsGrid"
                                ImageUrl="~/images/plus.png" CommandArgument="Show" />
                            <asp:Panel ID="pnlChallanItems" runat="server" Visible="false">
                                <asp:GridView ID="gvChallanItems" runat="server" AutoGenerateColumns="false" CssClass="Nested_ChildGrid"
                                    OnRowDataBound="gvChallanItems_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="Tool_No" HeaderText="Tool" HeaderStyle-BackColor="#335599"
                                            HeaderStyle-Width="70px" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="Station" HeaderText="Station" HeaderStyle-BackColor="#335599"
                                            HeaderStyle-Width="70px" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="P_No" HeaderText="PNo" HeaderStyle-BackColor="#335599"
                                            HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="Part_Name" HeaderText="Part Name" HeaderStyle-BackColor="#335599"
                                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="Rwk_No" HeaderText="Rwk" HeaderStyle-BackColor="#335599"
                                            HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" HeaderStyle-BackColor="#335599"
                                            HeaderStyle-Width="80px" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="Sent_Date" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Last Sent Date"
                                            HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                                        <asp:BoundField DataField="Received_Date" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Received Date"
                                            HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                                        <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="130px" HeaderText="Original Expected Delivery Date"
                                            ItemStyle-Width="150px" HeaderStyle-ForeColor="White">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOriginalCommitDate" runat="Server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="LastCommitDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Last Expected Delivery Date"
                                            HeaderStyle-BackColor="#335599" HeaderStyle-Width="130px" HeaderStyle-ForeColor="White" />
                                        <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="150px" HeaderText="Delay From Last SentDate"
                                            ItemStyle-Width="150px" HeaderStyle-ForeColor="White">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDelayFromOriginalSentDate" runat="Server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="130px" HeaderText="Delay From First Expected Delivery Date"
                                            ItemStyle-Width="150px" HeaderStyle-ForeColor="White">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDelayFromFirstCommitDate" runat="Server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="200px" HeaderStyle-ForeColor="White" />
                    <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px" HeaderText="Standard Days"
                        ItemStyle-Width="70px" HeaderStyle-ForeColor="White">
                        <ItemTemplate>
                            <asp:Label ID="lblStandardDays" runat="Server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Less than 100% Time" HeaderText="Less than 100% Time (Quantity)"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="200px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Between 100% and 200% Time" HeaderText="Between 100% and 200% Time  (Quantity)"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="200px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Between 200% and 300% Time" HeaderText="Between 200% and 300% Time  (Quantity)"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="200px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Greater than 300% Time" HeaderText="Greater than 300% Time  (Quantity)"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="200px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Less than 100% Count" HeaderText="Less than 100% Count (Positions)"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="200px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Between 100% and 200% Count" HeaderText="Between 100% and 200% Count  (Positions)"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="200px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Between 200% and 300% Count" HeaderText="Between 200% and 300% Count  (Positions)"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="200px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Greater than 300% Count" HeaderText="Greater than 300% Count  (Positions)"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="200px" HeaderStyle-ForeColor="White" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
