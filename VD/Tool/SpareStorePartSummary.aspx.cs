﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Reflection;

public partial class Tool_SpareStorePartSummary : System.Web.UI.Page
{
    String p_no = "";
    int tool_no, quantity, Qtysent;
    int countchk = 0;
    object Approveid = "";
    string str_dt1 = "";
    string part_name, station, location, str_dt;
    string location1 = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        txtpodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
        if (!IsPostBack)
        {
            if (txtprojectno.Text.Length > 0)
            {
                showgrid();
                Panel3.Visible = true;
                GridView1.Columns[9].Visible = false;
                GridView1.Columns[10].Visible = false;
                ddldepartment.Enabled = false;
            }
            GridView1.Columns[9].Visible = false;
            GridView1.Columns[8].Visible = true;
            GridView1.Columns[10].Visible = false;
            ddldepartment.Enabled = false;
            Panel3.Visible = true;
            Button1.Visible = true;
        }
    }

    /// <summary>
    /// Retrieves the Data from Location Info Gr table for the Spare Store Location
    /// </summary>
    public void showgrid()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();

        clear();
        string query = "";

        try
        {
            query = "select lig.tool_no,lig.station,lig.p_no,lig.Rework,lig.req_qty,lig.Batch_No,lig.part_name,lig.quant_sent,lig.quant_rec,lig.location,lig.Upload_Qty,lig.Rwk from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where lig.location='Spare Store' and lig.quant_sent>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) ";

            if (txtprojectno.Text.Length > 0)
            {
                query += " AND lig.tool_no= '" + txtprojectno.Text.Trim() + "'";
            }

            if (txtstation.Text.Length > 0)
            {
                query += " AND lig.station= '" + txtstation.Text.Trim() + "'";
            }

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable request_det_head = new DataTable();
            da.Fill(request_det_head);

            if (request_det_head.Rows.Count > 0)
            {
                btnSend.Visible = false;
                ddldepartment.Enabled = true;
                GridView1.DataSource = request_det_head;
                Session["DataTableAdvanceReport"] = request_det_head;
                GridView1.DataBind();
            }
            else
            {
                btnSend.Visible = false;
                request_det_head = null;
                GridView1.DataSource = request_det_head;
                GridView1.DataBind();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    /// <summary>
    /// Event is fired when user clicks on Check All Chekbox shown on the Header.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAll1_CheckedChanged1(object sender, EventArgs e)
    {
        lblChalanno.Text = String.Empty;
        CheckBox ChkBoxHeader = (CheckBox)GridView1.HeaderRow.FindControl("chkAll1");
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
                countchk = countchk + 1;
                EnableTextBox();
            }
            else
            {
                ChkBoxRows.Checked = false;

                int count = int.Parse(GridView1.Rows.Count.ToString());
                EnableTextBox();
            }
        }
        if (countchk > 0)
        {
            GridView1.Columns[10].Visible = true;
            GridView1.Columns[11].Visible = true;
            btnSend.Visible = true;
            btnprint.Visible = false;
        }
        else
        {
            btnprint.Visible = false;
            btnSend.Visible = false;
            GridView1.Columns[10].Visible = false;
            GridView1.Columns[11].Visible = false;
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        showgrid();
        if (GridView1.Rows.Count == 1)
            lbl.Text = "No previous data exists..!!";
        else
            GridView1.Columns[10].Visible = false;
    }

    private void clear()
    {
        txtpodate.Text = "";
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        showgrid();
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        showgrid();
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlloc = (DropDownList)row.FindControl("ddlloc");
                string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work order by Vendor_Name asc ";
                string dataTextField = "VENDOR_NAME";
                string dataValueField = "ID";
                UtilityFunctions.bind_vendor(ddlloc, query, dataTextField, dataValueField);
            }
        }
    }

    /// <summary>
    /// Makes the Controls Enabled/Disabled based on Checked or Unchecked
    /// </summary>
    protected void EnableTextBox()
    {
        int count = int.Parse(GridView1.Rows.Count.ToString());

        for (int i = 0; i < count; i++)
        {
            CheckBox cb = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkAllchild");
            if (cb.Checked == true)
            {
                btnSend.Visible = true;
                btnprint.Visible = false;
                TextBox TextBox3 = (TextBox)GridView1.Rows[i].Cells[10].FindControl("TextBox3");
                Label Label8 = (Label)GridView1.Rows[i].Cells[8].FindControl("Label8");
                Label Labelsent = (Label)GridView1.Rows[i].Cells[9].FindControl("Labelsent");
                TextBox3.Enabled = true;
            }
            else
            {
                TextBox TextBox3 = (TextBox)GridView1.Rows[i].Cells[10].FindControl("TextBox3");
                TextBox3.Enabled = false;
                btnprint.Visible = false;
                btnSend.Visible = false;

            }
        }
    }

    void load(GridViewUpdateEventArgs e)
    {
        p_no = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("Textbox1")).Text.ToString();
        part_name = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("Textbox2")).Text.ToString();
        tool_no = Convert.ToInt32(txtprojectno.Text.ToString());
        station = ((Label)GridView1.Rows[e.RowIndex].FindControl("Label10")).Text.ToString();

        DateTime str_dt1 = System.DateTime.Now;
        str_dt = string.Format("{0:yyyy-MM-dd}", str_dt1); // you can specify format 
        CultureInfo provider = CultureInfo.InvariantCulture;
        DateTime.ParseExact(str_dt, "yyyy-m-d", provider);

        quantity = Convert.ToInt32(((TextBox)GridView1.Rows[e.RowIndex].FindControl("Textbox4")).Text.ToString());
        Qtysent = Convert.ToInt32(((TextBox)GridView1.Rows[e.RowIndex].FindControl("txttobesent")).Text.ToString());
    }

    protected void TextBox3_TextChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            int count = int.Parse(GridView1.Rows.Count.ToString());
            for (int i = 0; i < count; i++)
            {
                TextBox TextBox3 = (TextBox)GridView1.Rows[i].Cells[10].FindControl("TextBox3");
                Label Label8 = (Label)GridView1.Rows[i].Cells[8].FindControl("Label8");
                Label Labelsent = (Label)GridView1.Rows[i].Cells[9].FindControl("Labelsent");
            }
        }
    }

    /// <summary>
    /// Event is fired when user checks the Check all button shown on the header
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAllchild_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)GridView1.HeaderRow.FindControl("chkAll1");
        lblChalanno.Text = String.Empty;
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            if (ChkBoxRows.Checked == true)
            {
                countchk = countchk + 1;
                GridView1.Columns[9].Visible = false;
                GridView1.Columns[10].Visible = true;
                btnSend.Visible = true;
                btnprint.Visible = false;
                EnableTextBox();
            }
            else
            {
                ChkBoxRows.Checked = false;
                int count = int.Parse(GridView1.Rows.Count.ToString());
                for (int i = 0; i < count; i++)
                {
                    CheckBox cb = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkAllchild");
                    if (cb.Checked == true)
                    {
                        GridView1.Columns[8].Visible = true;
                    }
                    else
                    {
                        GridView1.Columns[8].Visible = true;

                        TextBox TextBox3 = (TextBox)GridView1.Rows[i].Cells[10].FindControl("TextBox3");
                        btnprint.Visible = false;
                        btnSend.Visible = true;
                        TextBox3.Enabled = false;
                    }
                }
            }
        }

        if (countchk > 0)
        {
            GridView1.Columns[10].Visible = true;
            btnSend.Visible = true;
            UpdatePanel1.Update();
        }
        else
        {
            btnSend.Visible = false;
            GridView1.Columns[10].Visible = false;
            btnprint.Visible = false;
            UpdatePanel1.Update();
        }
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }


    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        DataTable table = (DataTable)Session["DataTableAdvanceReport"];
        GridView1.DataSource = table;
        GridView1.DataBind();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// Validates the Entered Fields and then Sends the position to Selected Vendor. Rows are inserted into Part History Gr table, Location Info Gr table and quantity is updated 
    /// for the Row where location is Company in History Gr table. Challan is generated on each Send, which can be printed by the user if required.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSend_Click(object sender, EventArgs e)
    {
        int count = 0;
        int tool_no1, quantity1, Qtysent1, sent1, uplqty;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        SqlTransaction trans = null;

        try
        {
            if (ddldepartment.SelectedValue == "Assembly")
            {
                string prefixStr = "CH";
                string generatedChallanNumber = "";
                generatedChallanNumber = UtilityFunctions.gencode(prefixStr);
                lblChalanno.Text = generatedChallanNumber;
                if (string.IsNullOrEmpty(generatedChallanNumber))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Challan Number generation Failed.');", true);
                    return;
                }
            }
            else
            {
                lblChalanno.Text = "";
            }
            conn.Open();
            trans = conn.BeginTransaction();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Transaction = trans;
                foreach (GridViewRow row in GridView1.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        string cmd2 = "";
                        Label p_no = row.FindControl("Label1") as Label;
                        Label part_name = row.FindControl("Label2") as Label;
                        Label lblproject = row.FindControl("lblproject") as Label;
                        Label station = row.FindControl("Label10") as Label;
                        Label lblbatchno = row.FindControl("lblbatchno") as Label;
                        Label LabelUpload_Qty = row.FindControl("LabelUpload_Qty") as Label;
                        Label Reworkno = row.FindControl("Label111") as Label;

                        str_dt1 = string.Format("{0:yyyy-MM-dd}", txtpodate.Text); // you can specify format 
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        int quant_rec = 0;
                        string kk = System.DateTime.Now.ToLongTimeString();
                        str_dt1 = str_dt1 + " " + kk;

                        Label quantity = row.FindControl("Label8") as Label;
                        TextBox tobeQtysent = row.FindControl("TextBox3") as TextBox;
                        Label sent = row.FindControl("Labelsent") as Label;

                        quantity1 = Convert.ToInt32(quantity.Text);
                        Qtysent1 = Convert.ToInt32(tobeQtysent.Text);
                        sent1 = Convert.ToInt32(sent.Text);
                        tool_no1 = Convert.ToInt32(lblproject.Text);
                        uplqty = Convert.ToInt32(LabelUpload_Qty.Text);


                        if (ddldepartment.SelectedItem.Text.Length > 0)
                        {

                            bool isValidData = false;
                            bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                            if (isSelected)
                            {
                                SqlCommand cmdCheckQuantity = new SqlCommand();
                                cmdCheckQuantity = new SqlCommand("select tool_no,station,Upload_Qty,p_no,part_name,quant_sent,quant_rec,isnull(quant_sent,0)-isnull(quant_rec,0) as SentQty,Rwk from location_info_GR where location='Spare Store' and isnull(quant_sent,0)>0 and location='Spare Store' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and tool_no='" + tool_no1 + "' "
                                + " and station='" + station.Text + "' and rwk='" + Reworkno.Text + "'  order by tool_no,station asc", conn, trans);
                                using (SqlDataReader reader = cmdCheckQuantity.ExecuteReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        int quantSent = Convert.ToInt32(reader["quant_sent"]);
                                        int quantReceive = Convert.ToInt32(reader["quant_rec"]);
                                        int uploadQty = Convert.ToInt32(reader["upload_qty"]);
                                        int new_quantity_sent = Convert.ToInt32(tobeQtysent.Text);
                                        if (new_quantity_sent <= quantSent && new_quantity_sent <= uploadQty)
                                        {
                                            isValidData = true;
                                        }
                                        else
                                        {
                                            isValidData = false;
                                        }
                                    }
                                }

                                if (isValidData)
                                {
                                    cmdCheckQuantity = new SqlCommand("select tool_no,station,Upload_Qty,p_no,part_name,quant_sent,quant_rec,isnull(quant_sent,0)-isnull(quant_rec,0) as SentQty,Rwk from location_info_GR where location='" + ddldepartment.SelectedItem.Text + "' and  location='" + ddldepartment.SelectedItem.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and tool_no='" + tool_no1 + "' " //(isnull(quant_sent,0)-isnull(quant_rec,0))>0 and
                                   + " and station='" + station.Text + "' and rwk='" + Reworkno.Text + "'  order by tool_no,station asc", conn, trans);
                                    using (SqlDataReader reader = cmdCheckQuantity.ExecuteReader())
                                    {
                                        if (reader.HasRows)
                                        {
                                            reader.Read();
                                            int quantSent = Convert.ToInt32(reader["quant_sent"]);
                                            int quantReceive = Convert.ToInt32(reader["quant_rec"]);
                                            int uploadQty = Convert.ToInt32(reader["upload_qty"]);
                                            int new_quantity_sent = Convert.ToInt32(tobeQtysent.Text);
                                            if (new_quantity_sent <= uploadQty)
                                            {
                                                isValidData = true;
                                            }
                                            else
                                            {
                                                isValidData = false;
                                            }
                                        }
                                        else
                                        {
                                            isValidData = true;
                                        }
                                    }
                                }
                            }

                            if (isSelected)
                            {
                                if (isValidData)
                                {
                                    Int32 qtysent1 = Convert.ToInt32(Qtysent1);
                                    if (ddldepartment.SelectedItem.Text == "VD Process Store")
                                    {
                                        string strQuery4 = "update part_history_GR  set quantity=isnull(quantity,0) + " + qtysent1 + ", QtySent=isnull(QtySent,0) - " + qtysent1 + " where location='COMPANY' and  p_no='" + p_no.Text + "'  and part_name='" + part_name.Text.Trim() + "' and station='" + station.Text + "' and tool_no=" + tool_no1 + " and rwk_no='" + Reworkno.Text + "'";
                                        SqlCommand cmd4 = new SqlCommand(strQuery4, conn, trans);
                                        Int32 k1 = cmd4.ExecuteNonQuery();

                                        string s = "insert into part_history_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,TransSentQty,Upload_Qty,rwk_no,Challan_No) values     (@p_no,@part_name,@tool_no,@station,@location,@date_of_trans,@sent_or_rec,@quantity,@QtySent,@TransSentQty,@Upload_Qty,@rwk_no,@Challan_No)";
                                        SqlCommand cmd1 = new SqlCommand();
                                        cmd1 = new SqlCommand(s, conn, trans);
                                        cmd1.Parameters.AddWithValue("@p_no", p_no.Text);
                                        cmd1.Parameters.AddWithValue("@part_name", part_name.Text);
                                        cmd1.Parameters.AddWithValue("@tool_no", tool_no1);
                                        cmd1.Parameters.AddWithValue("@station", station.Text);
                                        cmd1.Parameters.AddWithValue("@location", ddldepartment.SelectedItem.Text);
                                        cmd1.Parameters.AddWithValue("@date_of_trans", str_dt1);
                                        cmd1.Parameters.AddWithValue("@sent_or_rec", "Received");
                                        cmd1.Parameters.AddWithValue("@quantity", qtysent1);
                                        cmd1.Parameters.AddWithValue("@QtySent", qtysent1);
                                        cmd1.Parameters.AddWithValue("@TransSentQty", qtysent1);
                                        cmd1.Parameters.AddWithValue("@Upload_Qty", uplqty);
                                        cmd1.Parameters.AddWithValue("@rwk_no", Reworkno.Text);
                                        cmd1.Parameters.AddWithValue("@Challan_No", lblChalanno.Text);
                                        cmd1.ExecuteNonQuery();

                                        s = "select quant_sent,quant_rec from location_info_GR where location='" + location1 + "' and tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and rwk='" + Reworkno.Text + "'";
                                        cmd1.CommandText = s;

                                        DataTable dt = new DataTable();
                                        dt.Load(cmd1.ExecuteReader());

                                        if (dt.Rows.Count != 0)
                                        {
                                            cmd2 = "update";
                                            quantity1 += Convert.ToInt32(dt.Rows[0][0].ToString());
                                            quant_rec = Convert.ToInt32(dt.Rows[0][1].ToString());
                                        }

                                        if (cmd2.Equals("insert"))
                                        {
                                            s = "insert into location_info_GR(tool_no,station,p_no,part_name,req_qty,location,quant_sent,Sent_Received_Date,TransSentQty,Upload_Qty,Rwk,Challan_No) values(" + Convert.ToInt32(tool_no1) + ",'" + station.Text.ToString() + "'," + Convert.ToInt32(p_no.Text) + ",'" + part_name.Text.ToString() + "'," + Convert.ToInt32(qtysent1) + ",'" + ddldepartment.SelectedItem.Text.ToString() + "'," + Convert.ToInt32(qtysent1) + ",'" + str_dt1 + "'," + qtysent1 + "," + uplqty + ",'" + Reworkno.Text + "','" + lblChalanno.Text + "')";
                                            cmd1 = new SqlCommand(s, conn, trans);
                                            cmd1.ExecuteNonQuery();
                                        }
                                        else
                                        {
                                            s = "update location_info_GR set  TransSentQty=" + qtysent1 + ", quant_sent=isnull(quant_sent,0) - " + qtysent1 + "    where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and Rwk='" + Reworkno.Text + "' and location='VD Finish Store'";
                                            cmd1.CommandText = s;
                                            cmd1.ExecuteNonQuery();
                                        }
                                    }
                                    else
                                    {
                                        cmd2 = "insert";
                                        string s = "insert into part_history_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,TransSentQty,Upload_Qty,rwk_no,Challan_No) values     (@p_no,@part_name,@tool_no,@station,@location,@date_of_trans,@sent_or_rec,@quantity,@QtySent,@TransSentQty,@Upload_Qty,@rwk_no,@Challan_No)";
                                        SqlCommand cmd1 = new SqlCommand();
                                        cmd1 = new SqlCommand(s, conn, trans);
                                        cmd1.Parameters.AddWithValue("@p_no", p_no.Text);
                                        cmd1.Parameters.AddWithValue("@part_name", part_name.Text);
                                        cmd1.Parameters.AddWithValue("@tool_no", tool_no1);
                                        cmd1.Parameters.AddWithValue("@station", station.Text);
                                        cmd1.Parameters.AddWithValue("@location", ddldepartment.SelectedItem.Text);
                                        cmd1.Parameters.AddWithValue("@date_of_trans", str_dt1);
                                        cmd1.Parameters.AddWithValue("@sent_or_rec", "Sent");
                                        cmd1.Parameters.AddWithValue("@quantity", qtysent1);
                                        cmd1.Parameters.AddWithValue("@QtySent", qtysent1);
                                        cmd1.Parameters.AddWithValue("@TransSentQty", qtysent1);
                                        cmd1.Parameters.AddWithValue("@Upload_Qty", uplqty);
                                        cmd1.Parameters.AddWithValue("@rwk_no", Reworkno.Text);
                                        cmd1.Parameters.AddWithValue("@Challan_No", lblChalanno.Text);
                                        cmd1.ExecuteNonQuery();

                                        s = "update location_info_GR set  TransSentQty=" + qtysent1 + ", quant_sent=isnull(quant_sent,0)-" + qtysent1 + ", quant_pend=isnull(quant_sent,0)-" + qtysent1 + "   where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and  Rwk='" + Reworkno.Text + "' and location='Spare Store'";
                                        cmd1.CommandText = s;
                                        cmd1.ExecuteNonQuery();

                                        s = "select quant_sent,quant_rec from location_info_GR where location='" + ddldepartment.SelectedItem.Text.ToString() + "' and tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and rwk='" + Reworkno.Text + "'";
                                        cmd1.CommandText = s;
                                        DataTable dt = new DataTable();
                                        dt.Load(cmd1.ExecuteReader());
                                        if (dt.Rows.Count != 0)
                                        {
                                            cmd2 = "update";
                                            quantity1 += Convert.ToInt32(dt.Rows[0][0].ToString());
                                        }

                                        if (cmd2.Equals("insert"))
                                        {
                                            s = "insert into location_info_GR(tool_no,station,p_no,part_name,req_qty,location,quant_sent,quant_rec,quant_pend,quant_others,Sent_Received_Date,TransSentQty,Upload_Qty,Rwk) values(" + Convert.ToInt32(tool_no1) + ",'" + station.Text.ToString() + "','" + p_no.Text + "','" + part_name.Text.ToString() + "'," + Convert.ToInt32(qtysent1) + ",'" + ddldepartment.SelectedItem.Text.ToString() + "'," + Convert.ToInt32(qtysent1) + "," + Convert.ToInt32(0) + "," + Convert.ToInt32(qtysent1) + "," + Convert.ToInt32(0) + ",'" + str_dt1 + "'," + qtysent1 + "," + uplqty + ",'" + Reworkno.Text + "')";
                                            cmd1 = new SqlCommand(s, conn, trans);
                                            cmd1.ExecuteNonQuery();
                                        }
                                        else
                                        {
                                            s = "update location_info_GR set   quant_sent=isnull(quant_sent,0) + " + qtysent1 + ",quant_pend=isnull(quant_sent,0) + " + qtysent1 + "    where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and Rwk='" + Reworkno.Text + "' and part_name='" + part_name.Text + "' and location='" + ddldepartment.SelectedItem.Text.ToString() + "'";
                                            cmd1.CommandText = s;
                                            cmd1.ExecuteNonQuery();
                                        }
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Quantity Entered. Quantiy has been modified Please refresh the Page.');", true);
                                    lblChalanno.Text = String.Empty;
                                    return;
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Department');", true);
                            return;
                        }
                    }
                }
            }

            trans.Commit();
            if (ddldepartment.SelectedValue == "Assembly")
            {
                btnprint.Visible = true;
                btnprint.Enabled = true;
                UpdatePanel1.Update();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Items Sent Successfully');", true);
        }
        catch (Exception ex)
        {
            trans.Rollback();
            lblChalanno.Text = String.Empty;
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
        if (count > 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Send Item Successfully');", true);
            btnSend.Visible = false;
            btnprint.Visible = true;
        }
        showgrid();
    }

    /// <summary>
    /// Challan Can Be Printed through here
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnprintChallan_Click(object sender, EventArgs e)
    {
        Session["MIN"] = lblChalanno.Text;
        if (lblChalanno.Text.Length > 0)
        {
            string autochallan = null;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "window.open('Item_Challan.aspx?ProcessType=Send&AutoGeneratedChallan=" + autochallan + "');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Send part before Print');", true);
            return;
        }
    }

    /// <summary>
    /// Validates if date is past or future date and display error message if it is past or future dates
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtpodate_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDateTime(txtpodate.Text) > DateTime.Now || Convert.ToDateTime(txtpodate.Text) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Can not take future/past date');", true);
            txtpodate.Text = "";
        }
    }

}