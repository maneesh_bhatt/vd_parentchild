﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="GenerateVendorStatusReport.aspx.cs" Inherits="Tool_GenerateVendorStatusReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style2
        {
            border: none;
            width: 100px;
        }
        
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>
    <script language="javascript" type="text/javascript">

        $(function () {
            $('#<%=inputVendorName.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "GenerateVendorStatusReport.aspx/GetVendorName",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputVendorName.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputVendorName.ClientID%>').blur();

            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="width: 100%; border: 1px solid Blue; min-height: 100px;">
        <div style="padding: 9px;">
            <div>
                <div align="center">
                    <span class="style13">Generate Status Report</span>
                </div>
            </div>
            <div style="margin-bottom: 10px; margin-top: 20px;">
                <div style="font-size: 13px; width: 180px; float: left;">
                    Location<br />
                    <asp:TextBox runat="server" ID="inputVendorName" ClientIDMode="Static" CssClass="autocomplete glowing-border"
                        Style="width: 170px;" Placeholder=" Vendor Name"></asp:TextBox>
                </div>
                <div style="font-size: 13px; width: 150px; float: left;">
                    Supervisor
                    <asp:DropDownList ID="ddlSupervisor" AppendDataBoundItems="true" ForeColor="#7d6754"
                        ClientIDMode="Static" Width="140px" runat="server">
                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="font-size: 13px; width: 150px; float: left;">
                    Vendor Type
                    <asp:DropDownList ID="ddlVendorType" AppendDataBoundItems="true" ForeColor="#7d6754"
                        ClientIDMode="Static" Width="140px" runat="server">
                        <asp:ListItem Text="All" Value="All"></asp:ListItem>
                        <asp:ListItem Text="Internal Vendor" Value="Int"></asp:ListItem>
                        <asp:ListItem Text="External Vendor" Value="Ext"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/images/searchbtn.jpg"
                    OnClick="btnSearch_Click" Style="margin-top: 10px;" />
                <asp:Button ID="btnGenerateVisitReport" runat="server" Visible="true" EnableViewState="true"
                    CausesValidation="false" Text="Generate Vendor Status Report" Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px;
                    color: white; height: 34px; width: 200px; float: right; margin-top: 7px;" OnClick="btnGenerateVisitReport_Click" />
            </div>
            <asp:GridView runat="server" ID="gridViewVendorPendingItems" AutoGenerateColumns="false"
                Width="100%" ShowHeaderWhenEmpty="true" OnRowDataBound="gridViewVendorPendingItems_RowDataBound"
                AllowPaging="false" AllowSorting="false" Style="font-size: 13px; font-family: Verdana;
                line-height: 26px;">
                <RowStyle BackColor="White" ForeColor="Black" />
                <Columns>
                    <asp:BoundField DataField="Allocated_To" HeaderText="Supervisor Name" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Vendor_Name" HeaderText="Vendor Name" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="160px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Challan_No" HeaderText="Challan No" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="90px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Date_Of_Trans" HeaderText="Date Of Trans" DataFormatString="{0:dd-MM-yyyy}"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="90px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Tool_No" HeaderText="Tool No" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Station" HeaderText="Station" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="50px" HeaderStyle-ForeColor="White" />
                    <asp:TemplateField HeaderText="Rework No, P No, Pending Qty" ItemStyle-Wrap="true"
                        ItemStyle-Width="300px" HeaderStyle-BackColor="#335599" HeaderStyle-Width="300px"
                        HeaderStyle-ForeColor="White">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%#Eval("Part_Info") %>' Style="width: 300px; word-wrap: break-word;
                                float: left;"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Verified_By" HeaderText="Verified By" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="80px" HeaderStyle-ForeColor="White" />
                </Columns>
            </asp:GridView>
        </div>
        <p>
            <asp:Label runat="server" ID="lblTotalRows" ForeColor="Green"></asp:Label></p>
    </div>
</asp:Content>
