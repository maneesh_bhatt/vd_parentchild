﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="EmailConfigurationAdmin.aspx.cs" Inherits="Tool_EmailConfigurationAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .style14
        {
            width: 1034px;
        }
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="border: 1px solid Blue; min-height: 100px; padding: 9px;">
        <div>
            <div align="center">
                <span class="style13">Email Configuration Data</span>
            </div>
        </div>
        <div style="width: 100%; margin-bottom: 10px; margin-top: 20px;">
            <div style="font-size: 13px; float: left;">
                Category
                <br />
                <asp:TextBox runat="server" ID="inputCategory" ClientIDMode="Static" CssClass="autocomplete glowing-border"
                    Style="width: 170px;" Placeholder="Category"></asp:TextBox>
            </div>
            <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" ImageUrl="~/images/searchbtn.jpg"
                OnClick="btnSearch_Click" Style="margin-top: 14px; margin-left: 15px;" />
                <asp:HyperLink ID="HyperLink1" Style="background-color: #335599; float: right; color: White;
                padding: 5px; margin-top: 9px;" runat="server" NavigateUrl="~/Tool/EmailConfigurationForm.aspx">Add Configuration</asp:HyperLink>
        </div>
        <asp:GridView runat="server" ID="gridEmailConfiguration" BackColor="White" AutoGenerateColumns="false"
            EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True" AllowPaging="true"
            AllowSorting="false" Width="100%" BorderColor="#B8BABD" BorderStyle="None" OnPageIndexChanging="gridEmailConfiguration_PageIndexChanging"
            OnRowCommand="gridEmailConfiguration_RowCommand" OnRowDeleting="gridEmailConfiguration_RowDeleting"
            PageSize="15" BorderWidth="1px" Style="font-size: 12px; font-family: Verdana;
            line-height: 26px;">
            <HeaderStyle CssClass="tableheader" />
            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
            <Columns>
                <asp:BoundField DataField="Category" HeaderText="Category" HeaderStyle-Width="120px"
                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Email_Level" HeaderText="Email Level" HeaderStyle-Width="120px"
                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Email_Send_After" HeaderText="Email Send After" HeaderStyle-Width="110px"
                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Email_Incharge" HeaderText="Email Incharge" HeaderStyle-Width="180px"
                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Email_To" HeaderText="Email To" HeaderStyle-Width="200px"
                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Email_CC" HeaderText="Email CC" HeaderStyle-Width="100px"
                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White" />
                <%-- <asp:BoundField DataField="Email_Subject" HeaderText="Email Subject" HeaderStyle-Width="100px"
                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White" />--%>
                <asp:TemplateField HeaderText="" HeaderStyle-Width="80px" HeaderStyle-Font-Bold="false"
                    HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" CommandName="Edit" CssClass="btnCustom" CommandArgument='<%#Eval("Email_Configuration_Id") %>'
                            ID="lnkBtnEdit">Edit</asp:LinkButton>
                        &nbsp;
                        <asp:LinkButton runat="server" CommandName="Delete" CssClass="btnCustom" CommandArgument='<%#Eval("Email_Configuration_Id") %>'
                            ID="LinkButton1">Delete</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle Font-Bold="false" />
            <PagerStyle BackColor="#335599" ForeColor="White" Font-Size="Large" HorizontalAlign="Center"
                CssClass="gridview" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#000" />
            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
        </asp:GridView>
        <asp:Label runat="server" ID="lblRecorrdsCount" CssClass="recordsstatus" Width="850px"></asp:Label>
    </div>
</asp:Content>
