﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text;
using AjaxControlToolkit;
using System.Web.Services;
using System.Web.Script.Services;
using System.Drawing;
using OfficeExcel = Microsoft.Office.Interop.Excel;
using System.IO;
using ClosedXML.Excel;
using System.Text.RegularExpressions;

public partial class Tool_Recieve_GR_Item : System.Web.UI.Page
{

    string location, station, part_name, str_dt, s;
    string approvername = "";
    int tool_no, quant_sent, quant_rec, new_quantity_rec, quant_pend;
    string p_no = "";
    private static decimal operationSumOfAllPositions = 0;
    SqlConnection connection;
    SqlCommand cmd;

    int countchk = 0;
    static bool isRequestedForReceive = false;
    private DataTable dt = new DataTable();
    private DataRow dr;

    /// <summary>
    /// Makes the Controls Visible/Invisible as the Page Load, Binds the Vendor from Vendor Master Job Work Dropdown
    /// Rebinds the Child Positions shown on Child Position Columns
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        txtReceivecdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
        if (!IsPostBack)
        {
            isRequestedForReceive = false;
            Panel4.Visible = false;
            Panel2.Visible = false;
            txtchallanno.Visible = false;
            lblChallanno.Visible = false;
            btnprint.Visible = false;
            btnSendThisChallan.Visible = false;
            btnRecieved.ID = "btnRecieved";

            string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' and Vendor_Name<>'Weld Parts Consumed'  and Vendor_Name<>'GR VD FINISH STORE' and Vendor_Name<>'Not Required' order by Vendor_Name asc ";
            string dataTextField = "VENDOR_NAME";
            string dataValueField = "ID";
            UtilityFunctions.bind_vendor(ddllocation, query, dataTextField, dataValueField);

        }
        if (ViewState["CurrentTable"] == null)
        {
            LoadDataTable();
        }
        else
        {
            dt = (DataTable)ViewState["CurrentTable"];
        }

        RebindChildPositions();
    }


    protected void LoadDataTable()
    {
        dt.Columns.Add("Is_Selected", typeof(bool));
        dt.Columns.Add("Parts_Gr_Id", typeof(int));
        dt.Columns.Add("Location", typeof(string));
        dt.Columns.Add("Sent_Received_Date", typeof(string));
        dt.Columns.Add("Received_Date", typeof(string));
        dt.Columns.Add("Delivery_Commit_Date", typeof(string));
        dt.Columns.Add("tool_no", typeof(string));
        dt.Columns.Add("station", typeof(string));
        dt.Columns.Add("p_no", typeof(string));
        dt.Columns.Add("part_name", typeof(string));
        dt.Columns.Add("quant_sent", typeof(string));
        dt.Columns.Add("quant_rec", typeof(string));
        dt.Columns.Add("Challan_No", typeof(string));
        dt.Columns.Add("New_Rec_Quantity", typeof(string));
        dt.Columns.Add("Extra_Qty_Received", typeof(string));
        dt.Columns.Add("Extra_Receiving_Qty", typeof(string));
        dt.Columns.Add("Upload_Qty", typeof(string));
        dt.Columns.Add("SentQty", typeof(string));
        dt.Columns.Add("TotalPendingQtyAtVendor", typeof(string));
        dt.Columns.Add("Rwk", typeof(string));

    }

    /// <summary>
    /// Retrieves the Data from Location Info Gr table for the Selected Vendor which can be received back to Company through this Screen by User
    /// </summary>
    public void showgrid()
    {
        lbl.Text = "";
        try
        {
            dt.Rows.Clear();
            Session["DataTableAdvanceReport"] = null;
            ViewState["CurrentSqlCommand"] = "";
            ViewState["CurrentTable"] = null;
            operationSumOfAllPositions = 0;
            lblTotalOperationDuration.Text = String.Empty;
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            connection = new SqlConnection(sqlconnstring);
            connection.Open();
            string searchCondition = "";

            if (txtprojectno.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.tool_no='" + txtprojectno.Text + "'";
                }
                else
                {
                    searchCondition = "lig.tool_no='" + txtprojectno.Text + "'";
                }
            }
            if (tbStation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.station='" + tbStation.Text + "'";
                }
                else
                {
                    searchCondition = " lig.station='" + tbStation.Text + "'";
                }
            }

            if (ddllocation.SelectedItem.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.location='" + ddllocation.SelectedItem.Text + "'";
                }
                else
                {
                    searchCondition = " lig.location='" + ddllocation.SelectedItem.Text + "'";
                }
            }
            if (txtreworkno.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.Rwk='" + txtreworkno.Text + "'";
                }
                else
                {
                    searchCondition = " lig.Rwk='" + txtreworkno.Text + "'";
                }
            }

            if (ddllocation.SelectedItem.Text != "")
            {
                if (ddllocation.SelectedItem.Text.ToLower() == "quotation department")
                {
                    divCommitDate.Visible = false;
                }
                else
                {
                    divCommitDate.Visible = true;
                }
                cmd = new SqlCommand("select 'false' as Is_Selected, pgr.Parts_Gr_Id, lig.Location, lig.Sent_Received_Date,lig.Received_Date,lig.Delivery_commit_date, lig.tool_no,lig.station,lig.Upload_Qty,lig.p_no,lig.part_name,lig.quant_sent,lig.quant_rec,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as SentQty, isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as TotalPendingQtyAtVendor, lig.Rwk ,lig.Extra_Received_Quantity,pgr.opr17,pgr.Status from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1   and ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) and " + searchCondition + " order by lig.tool_no,lig.station asc");
                ViewState["CurrentSqlCommand"] = cmd.CommandText;
                cmd.Connection = connection;
                //DataTable dt = new DataTable();

                dt.Load(cmd.ExecuteReader());
                Panel1.Visible = true;
                if (dt.Rows.Count == 0)
                {
                    dt = null;
                    divTotal.Visible = false;
                }
                if (dt != null)
                {
                    btnGenerateVisitReport.Visible = true;
                }
                else
                {
                    btnGenerateVisitReport.Visible = false;
                }
                btnRecieved.Visible = false;
                ViewState["CurrentTable"] = dt;
                Session["DataTableAdvanceReport"] = dt;

                GridView1.DataSource = ViewState["CurrentTable"];
                GridView1.DataBind();
                if (operationSumOfAllPositions > 60)
                {
                    int hours = 0, minutes = 0;
                    hours = Convert.ToInt32(operationSumOfAllPositions) / 60;
                    minutes = Convert.ToInt32(operationSumOfAllPositions) - (60 * hours);
                    lblTotalOperationDuration.Text = hours + " Hrs " + minutes + " Mins";
                    divTotal.Visible = true;
                }
                else if (operationSumOfAllPositions > 0)
                {
                    string resultText = "";
                    if (operationSumOfAllPositions < 10)
                    {
                        resultText = "00:" + "0" + Convert.ToInt32(operationSumOfAllPositions);
                    }
                    else
                    {
                        resultText = "00:" + Convert.ToInt32(operationSumOfAllPositions);
                    }
                    lblTotalOperationDuration.Text = resultText;
                    divTotal.Visible = true;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Vendor');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        showgrid();
        if (GridView1.Rows.Count == 1)
            lbl.Text = "No previous data exists..!!";
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ShowChildHistory")
        {

        }
    }

    /// <summary>
    /// The Linkbutton to show child history are created dynamically so they are lost on post back so the code is added to rebind those controls
    /// </summary>
    /// <param name="gr"></param>
    protected void RebindChildPositions(GridViewRow gr)
    {
        CheckBox cb = (CheckBox)gr.FindControl("chkAllchild");
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        connec.Open();
        string pno = "", partname = "", station = "", rwkno = "", toolno = "";
        pno = Convert.ToString(DataBinder.Eval(gr.DataItem, "p_no"));
        partname = Convert.ToString(DataBinder.Eval(gr.DataItem, "part_name"));
        station = Convert.ToString(DataBinder.Eval(gr.DataItem, "station"));
        rwkno = Convert.ToString(DataBinder.Eval(gr.DataItem, "rwk"));
        toolno = Convert.ToString(DataBinder.Eval(gr.DataItem, "tool_no"));


        try
        {
            if (pno != "" && partname != "" && partname != "" && station != "" && rwkno != "" && toolno != "")
            {
                PlaceHolder lblChildPositions = (PlaceHolder)gr.FindControl("placeHolderChildPositions");
                if (ddllocation.SelectedItem.Text == "Weld Assembly Waiting")
                {
                    int totalPendingRows = 0;
                    SqlDataAdapter ad = new SqlDataAdapter();
                    DataTable dt = new DataTable();
                    string childPositions = "";
                    string parentName = "P_" + pno;
                    string currentPNo = "";
                    ad = new SqlDataAdapter("select * from parts_Gr where ((opr17 like '%" + parentName + ",%' or opr17 like '%," + parentName + "' or opr17 like '%," + parentName + ",%' or opr17 like '" + parentName + "') or (Parent_Positions like '%" + parentName + ",%' or Parent_Positions like '%," + parentName + "' or Parent_Positions like '%," + parentName + ",%' or Parent_Positions like '" + parentName + "')) and tool_no='" + toolno + "'  and  station='" + station + "' and rwk_no='" + rwkno + "' and Is_Active_Record=1", connec);
                    ad.Fill(dt);
                    int itemsMovedAtWeldPartsWaitingLocation = 0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        currentPNo = Convert.ToString(dt.Rows[i]["p_no"]);
                        cmd = new SqlCommand("select location,isnull(quant_sent,0) as quant_sent, isnull(Upload_Qty,0) as Upload_Qty, isnull((isnull(quant_sent,0)- isnull(quant_rec,0)),0) as quant_pend from location_info_Gr where tool_no='" + dt.Rows[i]["tool_no"] + "'  and  p_no='" + dt.Rows[i]["p_no"] + "' and part_name='" + dt.Rows[i]["part_name"] + "' and  station='" + dt.Rows[i]["station"] + "' and rwk='" + dt.Rows[i]["rwk_no"] + "' ", connec);
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                int k = 0;
                                bool enteredCountCondition = false;
                                int pendingQuantity = 0, uploadQuantity = 0;
                                while (rdr.Read())
                                {
                                    if (k == 0)
                                    {
                                        if (childPositions != "")
                                            childPositions = childPositions + "," + currentPNo;
                                        else
                                            childPositions = currentPNo;
                                    }
                                    k = k + 1;
                                    if (Convert.ToInt32(rdr["quant_pend"]) > 0)
                                    {
                                        totalPendingRows = totalPendingRows + 1;
                                        if (!enteredCountCondition)
                                        {
                                            childPositions = childPositions + "-" + Convert.ToString(rdr["location"]) + " (" + Convert.ToInt32(rdr["quant_pend"]) + ")";
                                            enteredCountCondition = true;
                                        }
                                        else
                                        {
                                            childPositions = childPositions + "," + Convert.ToString(rdr["location"]) + " (" + Convert.ToInt32(rdr["quant_pend"]) + ")";
                                        }
                                    }

                                    if ((Convert.ToString(rdr["Location"]) == "Weld Parts Waiting" || Convert.ToString(rdr["Location"]) == "Weld Parts Consumed"))
                                    {
                                        if (Convert.ToInt32(rdr["Upload_Qty"]) == Convert.ToInt32(rdr["quant_pend"]))
                                        {
                                            itemsMovedAtWeldPartsWaitingLocation = itemsMovedAtWeldPartsWaitingLocation + 1;
                                        }
                                        else
                                        {
                                            uploadQuantity = Convert.ToInt32(rdr["Upload_Qty"]);
                                            pendingQuantity = Convert.ToInt32(rdr["quant_pend"]) + pendingQuantity;
                                        }

                                    }
                                }
                                if (pendingQuantity > 0 && uploadQuantity == pendingQuantity)
                                    itemsMovedAtWeldPartsWaitingLocation = itemsMovedAtWeldPartsWaitingLocation + 2;

                            }
                            else
                            {
                                childPositions = currentPNo + "," + childPositions;
                                totalPendingRows = totalPendingRows + 1;
                            }
                        }
                    }

                    string[] splitChildPositions = childPositions.TrimEnd(',').Split(',');
                    for (int i = 0; i < splitChildPositions.Length; i++)
                    {
                        LinkButton lnkBtnChild = new LinkButton();
                        lnkBtnChild.ID = "lnkBtnChild" + gr.RowIndex + "_" + i + 1;
                        lblChildPositions.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                        lnkBtnChild.Text = splitChildPositions[i];
                        lnkBtnChild.Visible = true;
                        lnkBtnChild.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                        lnkBtnChild.CommandName = "ShowChildHistory";
                        lblChildPositions.Controls.Add(lnkBtnChild);
                        lnkBtnChild.Click += new EventHandler(lnkBtnChild_Click);
                        Literal literalControl = new Literal();
                        if (i != splitChildPositions.Length - 1)
                        {
                            literalControl.Text = ",";
                        }
                        lblChildPositions.Controls.Add(literalControl);
                        UpdatePanel1.Update();
                    }
                }
            }
        }
        catch (Exception ex)
        {
           
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occcured while loading Data. Please Contact Administrator.');", true);
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            connec.Close();
        }
    }

    /// <summary>
    /// Retrieves the Total Operational Time for the selected Vendor, In case of Weld Assembly Waiting Location calculates the Parent Child Ratio and on the basis enables/disables
    /// the position. Retrieves the Last Expected Delivery Date for each row, also retrieves the Uploaded Excel file name for each row.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (ddllocation.SelectedItem.Text == "Weld Assembly Waiting")
            {
                GridView1.Columns[13].Visible = true;
            }
            else
            {
                GridView1.Columns[13].Visible = false;
            }
            NumericUpDownExtender ne = (NumericUpDownExtender)e.Row.FindControl("numericupdownextender3");
            Label l = (Label)e.Row.FindControl("Label5");
            LinkButton lnkBtnLastCommitmentDate = (LinkButton)e.Row.FindControl("lnkBtnLastCommitmentDate");

            CheckBox cb = (CheckBox)e.Row.FindControl("chkAllchild");
            ne.Maximum = Convert.ToDouble(l.Text);
            HyperLink hyperLnkExcelFile = (HyperLink)e.Row.FindControl("hyperLnkExcelFile");
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection connec = new SqlConnection(sqlconnstring);
            connec.Open();
            string pno = "", partname = "", station = "", rwkno = "", toolno = "";
            pno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "p_no"));
            partname = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "part_name"));
            station = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "station"));
            rwkno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "rwk"));
            toolno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "tool_no"));
            decimal sentQty = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "SentQty"));
            string uploadedFileName = "";
            PlaceHolder lblChildPositions = (PlaceHolder)e.Row.FindControl("placeHolderChildPositions");
            HiddenField hdnChildPositions = (HiddenField)e.Row.FindControl("hdnChildPositions");
            Label lblAvailableQtyForReceive = (Label)e.Row.FindControl("lblAvailableQtyForReceive");
            HiddenField hdnPartsGrId = (HiddenField)e.Row.FindControl("hdnPartsGrId");
            try
            {
                SqlCommand cmd = new SqlCommand();
                decimal sumOfOpertions = 0;
                string surfaceTreatment = "";
                decimal opr1Result = 0, opr2Result = 0, opr3Result = 0, opr4Result = 0, opr5Result = 0, opr6Result = 0, opr7Result = 0, opr8Result = 0, opr9Result = 0, opr10Result = 0, opr11Result = 0, opr12Result = 0, opr13Result = 0, opr14Result = 0, opr15Result = 0, opr16Result = 0, opr17Result = 0;
                cmd = new SqlCommand("select * from Parts_Gr where tool_no='" + toolno + "'  and  p_no='" + pno + "' and part_name='" + partname + "' and station='" + station + "' and rwk_no='" + rwkno + "' order by Parts_Gr_Id Desc", connec);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        surfaceTreatment = Convert.ToString(reader["Opr17"]);
                        if (ddllocation.SelectedItem.Text.TrimStart() != "Inspection")
                        {
                            bool isOpr1Valid = decimal.TryParse(Convert.ToString(reader["opr1"]), out opr1Result);
                            if (isOpr1Valid)
                            {
                                sumOfOpertions = Math.Round(opr1Result * 60);
                            }
                            bool isOpr2Valid = decimal.TryParse(Convert.ToString(reader["opr2"]), out opr2Result);
                            if (isOpr2Valid)
                            {
                                sumOfOpertions += Math.Round(opr2Result * 60);
                            }
                            bool isOpr3Valid = decimal.TryParse(Convert.ToString(reader["opr3"]), out opr3Result);
                            if (isOpr3Valid)
                            {
                                sumOfOpertions += Math.Round(opr3Result * 60);
                            }
                            bool isOpr4Valid = decimal.TryParse(Convert.ToString(reader["opr4"]), out opr4Result);
                            if (isOpr4Valid)
                            {
                                sumOfOpertions += Math.Round(opr4Result * 60);
                            }
                            bool isOpr5Valid = decimal.TryParse(Convert.ToString(reader["opr5"]), out opr5Result);
                            if (isOpr5Valid)
                            {
                                sumOfOpertions += Math.Round(opr5Result * 60);
                            }
                            bool isOpr6Valid = decimal.TryParse(Convert.ToString(reader["opr6"]), out opr6Result);
                            if (isOpr6Valid)
                            {
                                sumOfOpertions += Math.Round(opr6Result * 60);
                            }
                            bool isOpr7Valid = decimal.TryParse(Convert.ToString(reader["opr7"]), out opr7Result);
                            if (isOpr7Valid)
                            {
                                sumOfOpertions += Math.Round(opr7Result * 60);
                            }
                            bool isOpr8Valid = decimal.TryParse(Convert.ToString(reader["opr8"]), out opr8Result);
                            if (isOpr8Valid)
                            {
                                sumOfOpertions += Math.Round(opr8Result * 60);
                            }
                            bool isOpr9Valid = decimal.TryParse(Convert.ToString(reader["opr9"]), out opr9Result);
                            if (isOpr9Valid)
                            {
                                sumOfOpertions += Math.Round(opr9Result * 60);
                            }
                            bool isOpr10Valid = decimal.TryParse(Convert.ToString(reader["opr10"]), out opr10Result);
                            if (isOpr10Valid)
                            {
                                sumOfOpertions += Math.Round(opr10Result * 60);
                            }
                            bool isOpr11Valid = decimal.TryParse(Convert.ToString(reader["opr11"]), out opr11Result);
                            if (isOpr11Valid)
                            {
                                sumOfOpertions += Math.Round(opr11Result * 60);
                            }
                            bool isOpr12Valid = decimal.TryParse(Convert.ToString(reader["opr12"]), out opr12Result);
                            if (isOpr12Valid)
                            {
                                sumOfOpertions += Math.Round(opr12Result * 60);
                            }
                            bool isOpr13Valid = decimal.TryParse(Convert.ToString(reader["opr13"]), out opr13Result);
                            if (isOpr13Valid)
                            {
                                sumOfOpertions += Math.Round(opr13Result * 60);
                            }
                            bool isOpr14Valid = decimal.TryParse(Convert.ToString(reader["opr14"]), out opr14Result);
                            if (isOpr14Valid)
                            {
                                sumOfOpertions += Math.Round(opr14Result * 60);
                            }
                            bool isOpr15Valid = decimal.TryParse(Convert.ToString(reader["opr15"]), out opr15Result);
                            if (isOpr15Valid)
                            {
                                sumOfOpertions += Math.Round(opr15Result * 60);
                            }
                            bool isOpr16Valid = decimal.TryParse(Convert.ToString(reader["opr16"]), out opr16Result);
                            if (isOpr16Valid)
                            {
                                sumOfOpertions += Math.Round(opr16Result * 60);
                            }
                        }
                        else
                        {
                            bool isOpr16Valid = decimal.TryParse(Convert.ToString(reader["opr16"]), out opr16Result);
                            if (isOpr16Valid)
                            {
                                sumOfOpertions += opr16Result * 60;
                            }
                        }
                        operationSumOfAllPositions = operationSumOfAllPositions + (sentQty * sumOfOpertions);
                    }
                }

                if (ddllocation.SelectedItem.Text == "Weld Assembly Waiting")
                {
                    int totalPendingRows = 0;
                    SqlDataAdapter ad = new SqlDataAdapter();
                    DataTable dt = new DataTable();
                    DataTable dtChildItems = new DataTable();
                    string childPositions = "";
                    string parentName = "P_" + pno;
                    string currentPNo = "";

                    ad = new SqlDataAdapter("select pcr.Parts_Gr_Id as ParentPartsGrId, chi.Parts_Gr_Id as ChildPartsGrId, chi.Part_Name, pcr.P_No, pcr.Ratio, pg.Tool_No, pg.Station, pg.Rwk_No from Parent_Child_Ratio_Detail pcr "
                     + " inner join Parts_Gr pg on pcr.Parts_Gr_Id= pg.Parts_Gr_Id left join parts_Gr chi on pcr.P_No= chi.P_No and pg.Tool_No= chi.Tool_No and "
                     + " pg.Station= chi.Station and pg.Rwk_No=chi.Rwk_No  where pcr.Parts_Gr_Id=@PartsGrId", connec);
                    ad.SelectCommand.Parameters.AddWithValue("@PartsGrId", Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Parts_Gr_Id")));
                    //int childCount = 0;
                    ad.Fill(dtChildItems);
                    if (dtChildItems.Rows.Count > 0)
                    {
                        int parentAvailableQty = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SentQty"));
                        List<int> parentQtyMadeWithChild = new List<int>();
                        for (int j = 0; j < dtChildItems.Rows.Count; j++)
                        {
                            int ratio = Convert.ToInt32(dtChildItems.Rows[j]["Ratio"]);
                            ad = new SqlDataAdapter("select location,isnull(quant_sent,0) as quant_sent, isnull(Upload_Qty,0) as Upload_Qty, isnull((isnull(quant_sent,0)- isnull(quant_rec,0)),0) as quant_pend from location_info_Gr where tool_no='" + dtChildItems.Rows[j]["tool_no"] + "'  and  p_no='" + dtChildItems.Rows[j]["p_no"] + "' and part_name='" + dtChildItems.Rows[j]["part_name"] + "' and  station='" + dtChildItems.Rows[j]["station"] + "' and rwk='" + dtChildItems.Rows[j]["rwk_no"] + "' and (location='Weld Parts Waiting')", connec);// or location='Weld Parts Consumed'
                            DataTable dtValidate = new DataTable();
                            ad.Fill(dtValidate);
                            int pendingQty = 0;
                            for (int k = 0; k < dtValidate.Rows.Count; k++)
                            {
                                pendingQty = Convert.ToInt32(dtValidate.Rows[k]["quant_pend"]) + pendingQty;
                            }
                            int parentQtyCanBeMadeWithChild = pendingQty / ratio;
                            parentQtyMadeWithChild.Add(parentQtyCanBeMadeWithChild);
                        }
                        int minValue = parentQtyMadeWithChild.Min();
                        if (minValue > 0 && parentAvailableQty > 0)
                        {
                            cb.Enabled = true;
                        }
                        else
                        {
                            cb.Enabled = false;
                        }
                    }
                    ad = new SqlDataAdapter("select * from parts_Gr where ((opr17 like '%" + parentName + ",%' or opr17 like '%," + parentName + "' or opr17 like '%," + parentName + ",%' or opr17 like '" + parentName + "') or (Parent_Positions like '%" + parentName + ",%' or Parent_Positions like '%," + parentName + "' or Parent_Positions like '%," + parentName + ",%' or Parent_Positions like '" + parentName + "')) and tool_no='" + toolno + "'  and  station='" + station + "' and rwk_no='" + rwkno + "' and Is_Active_Record=1", connec);
                    ad.Fill(dt);
                    int itemsMovedAtWeldPartsWaitingLocation = 0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        currentPNo = Convert.ToString(dt.Rows[i]["p_no"]);
                        cmd = new SqlCommand("select location,isnull(quant_sent,0) as quant_sent, isnull(Upload_Qty,0) as Upload_Qty, isnull((isnull(quant_sent,0)- isnull(quant_rec,0)),0) as quant_pend from location_info_Gr where tool_no='" + dt.Rows[i]["tool_no"] + "'  and  p_no='" + dt.Rows[i]["p_no"] + "' and part_name='" + dt.Rows[i]["part_name"] + "' and  station='" + dt.Rows[i]["station"] + "' and rwk='" + dt.Rows[i]["rwk_no"] + "' ", connec);
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                int k = 0;
                                bool enteredCountCondition = false;
                                int pendingQuantity = 0, uploadQuantity = 0;
                                while (rdr.Read())
                                {
                                    if (k == 0)
                                    {
                                        if (childPositions != "")
                                            childPositions = childPositions + "," + currentPNo;
                                        else
                                            childPositions = currentPNo;
                                    }
                                    k = k + 1;
                                    if (Convert.ToInt32(rdr["quant_pend"]) > 0)
                                    {
                                        totalPendingRows = totalPendingRows + 1;
                                        if (!enteredCountCondition)
                                        {
                                            childPositions = childPositions + "-" + Convert.ToString(rdr["location"]) + " (" + Convert.ToInt32(rdr["quant_pend"]) + ")";
                                            enteredCountCondition = true;
                                        }
                                        else
                                        {
                                            childPositions = childPositions + "," + Convert.ToString(rdr["location"]) + " (" + Convert.ToInt32(rdr["quant_pend"]) + ")";
                                        }
                                    }

                                    if ((Convert.ToString(rdr["Location"]) == "Weld Parts Waiting" || Convert.ToString(rdr["Location"]) == "Weld Parts Consumed"))
                                    {
                                        if (Convert.ToInt32(rdr["Upload_Qty"]) == Convert.ToInt32(rdr["quant_pend"]))
                                        {
                                            itemsMovedAtWeldPartsWaitingLocation = itemsMovedAtWeldPartsWaitingLocation + 1;
                                        }
                                        else
                                        {
                                            uploadQuantity = Convert.ToInt32(rdr["Upload_Qty"]);
                                            pendingQuantity = Convert.ToInt32(rdr["quant_pend"]) + pendingQuantity;
                                        }

                                    }
                                }
                                if (pendingQuantity > 0 && uploadQuantity == pendingQuantity)
                                    itemsMovedAtWeldPartsWaitingLocation = itemsMovedAtWeldPartsWaitingLocation + 2;

                            }
                            else
                            {
                                childPositions = currentPNo + "," + childPositions;
                                totalPendingRows = totalPendingRows + 1;
                            }
                        }
                    }
                    hdnChildPositions.Value = childPositions.TrimEnd(',');
                    if (ddllocation.SelectedItem.Text == "Weld Assembly Waiting" && dtChildItems.Rows.Count == 0)
                    {
                        if (totalPendingRows > 0 || itemsMovedAtWeldPartsWaitingLocation > 0)
                        {
                            if (totalPendingRows == itemsMovedAtWeldPartsWaitingLocation && itemsMovedAtWeldPartsWaitingLocation > 0)
                            {
                                cb.Enabled = true;
                            }
                            else
                            {
                                cb.Enabled = false;
                            }
                        }
                        else
                        {
                            cb.Enabled = true;
                        }
                        if (dt.Rows.Count > 0 && totalPendingRows == 0 && itemsMovedAtWeldPartsWaitingLocation == 0)
                        {
                            cb.Enabled = false;
                        }
                    }
                }

                cmd = new SqlCommand("select Uploaded_File_Name from Part_History_Gr where tool_no='" + toolno + "'  and  p_no='" + pno + "' and part_name='" + partname + "' and location='COMPANY' and station='" + station + "' and rwk_no='" + rwkno + "' and sent_or_rec='Uploaded'", connec);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        uploadedFileName = Convert.ToString(reader["Uploaded_File_Name"]);
                    }
                }

                if (uploadedFileName != "")
                {
                    hyperLnkExcelFile.NavigateUrl = "~/UploadedExcelFiles/" + uploadedFileName;
                    hyperLnkExcelFile.Text = "Excel";
                }

                cmd = new SqlCommand("select top 1 Delivery_commit_date from Part_History_Gr where tool_no='" + toolno + "'  and  p_no='" + pno + "' and part_name='" + partname + "' and location='" + ddllocation.SelectedItem.Text + "' and station='" + station + "' and rwk_no='" + rwkno + "' and sent_or_rec='Sent' order by date_of_trans desc", connec);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        if (reader["Delivery_commit_date"] != DBNull.Value)
                            lnkBtnLastCommitmentDate.Text = Convert.ToDateTime(reader["Delivery_commit_date"]).ToString("dd-MM-yyyy");
                    }
                }

                if (!cb.Checked)
                {
                    ne.Enabled = false;
                }

                cmd.Parameters.Clear();
                DropDownList ddlChallanNumbers = (DropDownList)e.Row.FindControl("ddlChallanNumbers");
                cmd = new SqlCommand("select Challan_No +' - '+ Cast(OutStanding_Qty as varchar) +' - '+ Cast(Convert(Date, Send_Receive_Date) as varchar) as ChallanInfo from Challan_Sent_Received_Detail where  tool_no=@ToolNo  and  p_no=@PNo and part_name=@PartName and location=@Location and station=@Station and rwk_no=@RwkNo and Sent_Or_Rec='Sent' and Outstanding_Qty>0", connec);
                cmd.Parameters.AddWithValue("@ToolNo", toolno);
                cmd.Parameters.AddWithValue("@PNo", pno);
                cmd.Parameters.AddWithValue("@PartName", partname);
                cmd.Parameters.AddWithValue("@Location", ddllocation.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@Station", station);
                cmd.Parameters.AddWithValue("@RwkNo", rwkno);
                ddlChallanNumbers.DataSource = cmd.ExecuteReader();
                ddlChallanNumbers.DataTextField = "ChallanInfo";
                ddlChallanNumbers.DataValueField = "ChallanInfo";
                ddlChallanNumbers.DataBind();
                string[] splitValues = Regex.Split(ddlChallanNumbers.SelectedValue, " - ");

                //Display Valid Possible Quantity for Send For Rows having Enabled CheckBoxes
                if (cb.Enabled)
                {
                    if (ddllocation.SelectedItem.Text == "Weld Assembly Waiting")
                    {
                        int? validParentQtyAfterRatio = ValidateRationWiseQuantity(hdnPartsGrId.Value, Convert.ToInt32(sentQty));
                        if (validParentQtyAfterRatio == null)
                        {
                            lblAvailableQtyForReceive.Text = Convert.ToString(null);
                        }
                        else if (validParentQtyAfterRatio != null)
                        {
                            lblAvailableQtyForReceive.Text = Convert.ToString(validParentQtyAfterRatio.Value);
                        }
                    }
                    else
                    {
                        lblAvailableQtyForReceive.Text = Convert.ToString(sentQty);
                    }
                }
                UpdatePanel1.Update();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occcured while loading Data. Please Contact Administrator.');", true);
                ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
            }
            finally
            {
                connec.Close();
            }
        }
    }


    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            RebindChildPositions(e.Row);
        }
    }

    void lb_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        GridViewRow row = (GridViewRow)lb.NamingContainer;
        if (row != null)
        {
            Response.Write("Found it!");
        }
    }

    /// <summary>
    /// On click on Child Positions link, it displays the Sent/Received History of Child Positions
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void lnkBtnChild_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string[] pNoSplit = lnkBtn.Text.Split('-');

        Label lblStation = lnkBtn.FindControl("Label2") as Label;
        Label lblrew = lnkBtn.FindControl("lblrew") as Label;
        Label lblToolNo = lnkBtn.FindControl("Label1") as Label;
        LinkButton lnkbtnPNo = lnkBtn.FindControl("Label3") as LinkButton;
        Label lblPartName = lnkBtn.FindControl("Label4") as Label;

        LoadPositionHistoryWindow(pNoSplit[0], lblToolNo.Text, lblStation.Text, lblPartName.Text, lblrew.Text, "child");
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        showgrid();
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        showgrid();
    }


    private void ResetRowID(DataTable dt)
    {
        int rowNumber = 1;
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow row in dt.Rows)
            {
                row[0] = rowNumber;
                rowNumber++;
            }
        }
    }

    /// <summary>
    /// User can receive the Positions against Multiple challan for Same row by clicking on Add new Button, So this event is fired when user clicks on Add New if there are 
    /// multiple challans shown on Challan Dropdown and a new Row is added for same criteria where user can specify other challan number to receieve position
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="rowIndex"></param>
    /// <param name="pendingQty"></param>
    private void AddNewRowToGrid(object sender, int rowIndex, int pendingQty)
    {

        if (ViewState["CurrentTable"] != null)
        {
            LinkButton lnkBtn = (LinkButton)sender;

            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            DataRow drCurrentRow = null;
            if (dtCurrentTable.Rows.Count > 0)
            {
                drCurrentRow = dtCurrentTable.NewRow();

                CheckBox chkAllchild = lnkBtn.FindControl("chkAllchild") as CheckBox;
                Label lblTotalQtySent = lnkBtn.FindControl("Label5") as Label;
                Label lblReceivedQty = lnkBtn.FindControl("Label6") as Label;
                TextBox txtNewReceivingQty = lnkBtn.FindControl("TextBox2") as TextBox;
                TextBox txtExtraReceivingQty = lnkBtn.FindControl("txtExtraQuantity") as TextBox;
                Label lblStation = lnkBtn.FindControl("Label2") as Label;
                Label lblrew = lnkBtn.FindControl("lblrew") as Label;

                Label lblToolNo = lnkBtn.FindControl("Label1") as Label;
                LinkButton lnkbtnPNo = lnkBtn.FindControl("Label3") as LinkButton;
                Label lblPartName = lnkBtn.FindControl("Label4") as Label;
                CultureInfo provider = CultureInfo.InvariantCulture;
                Label quantity = lnkBtn.FindControl("Label8") as Label;
                Label lblUploadQty = lnkBtn.FindControl("Label6Upload_Qty") as Label;
                DropDownList ddlChallanNumbers = lnkBtn.FindControl("ddlChallanNumbers") as DropDownList;
                Label lblExtraReceivedQty = lnkBtn.FindControl("lblExtraReceivedQty") as Label;
                Label lblSentQtyAtVendor = lnkBtn.FindControl("Label6sentqty") as Label;
                HiddenField hdnPendingRowsAtVendor = lnkBtn.FindControl("hdnPendingRowsAtVendor") as HiddenField;
                HiddenField hdnPartsGrId = lnkBtn.FindControl("hdnPartsGrId") as HiddenField;
                HiddenField hdnLocation = lnkBtn.FindControl("hdnLocation") as HiddenField;
                HiddenField hdnSentReceivedDate = lnkBtn.FindControl("hdnSentReceivedDate") as HiddenField;
                HiddenField hdnReceivedDate = lnkBtn.FindControl("hdnReceivedDate") as HiddenField;
                HiddenField hdnDeliveryCommitDate = lnkBtn.FindControl("hdnDeliveryCommitDate") as HiddenField;

                drCurrentRow["Is_Selected"] = false;
                drCurrentRow["Parts_Gr_Id"] = Convert.ToString(hdnPartsGrId.Value);
                drCurrentRow["Location"] = Convert.ToString(hdnLocation.Value);
                drCurrentRow["Sent_Received_Date"] = Convert.ToString(hdnSentReceivedDate.Value);
                drCurrentRow["Received_Date"] = Convert.ToString(hdnReceivedDate.Value);
                drCurrentRow["Delivery_Commit_Date"] = Convert.ToString(hdnDeliveryCommitDate.Value);

                drCurrentRow["tool_no"] = Convert.ToString(lblToolNo.Text);
                drCurrentRow["station"] = Convert.ToString(lblStation.Text);
                drCurrentRow["p_no"] = Convert.ToString(lnkbtnPNo.Text);
                drCurrentRow["part_name"] = Convert.ToString(lblPartName.Text);
                drCurrentRow["quant_sent"] = Convert.ToString(lblTotalQtySent.Text);
                drCurrentRow["quant_rec"] = Convert.ToString(lblReceivedQty.Text);
                drCurrentRow["Challan_No"] = String.Empty;
                drCurrentRow["New_Rec_Quantity"] = Convert.ToString(pendingQty);
                drCurrentRow["Extra_Qty_Received"] = Convert.ToString(lblExtraReceivedQty.Text);
                drCurrentRow["Extra_Receiving_Qty"] = Convert.ToString(txtExtraReceivingQty.Text);
                drCurrentRow["Upload_Qty"] = Convert.ToString(lblUploadQty.Text);
                drCurrentRow["SentQty"] = Convert.ToString(lblSentQtyAtVendor.Text);
                drCurrentRow["TotalPendingQtyAtVendor"] = Convert.ToString(pendingQty);
                hdnPendingRowsAtVendor.Value = Convert.ToString(pendingQty);
                drCurrentRow["Rwk"] = Convert.ToString(lblrew.Text);
                //add new row to DataTable   
                dtCurrentTable.Rows.InsertAt(drCurrentRow, rowIndex + 1);
                //Store the current data to ViewState for future reference   
                ViewState["CurrentTable"] = dtCurrentTable;
                //Rebind the Grid with the current data to reflect changes   
                GridView1.DataSource = dtCurrentTable;
                GridView1.DataBind();
            }
        }
        else
        {
            Response.Write("ViewState is null");

        }
        //Set Previous Data on Postbacks   
        SetPreviousData();
    }

    /// <summary>
    /// Whenver user clicks on Add New button shown in front of each row, it sets the previous selected data for the row where user has clicked add new
    /// </summary>
    private void SetPreviousData()
    {
        int rowIndex = 0;
        if (ViewState["CurrentTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //extract the TextBox values   

                    CheckBox chkGridAllchild = GridView1.Rows[i].FindControl("chkAllchild") as CheckBox;
                    Label lblTotalQtySent = GridView1.Rows[i].FindControl("Label5") as Label;
                    Label lblReceivedQty = GridView1.Rows[i].FindControl("Label6") as Label;
                    TextBox txtNewReceivingQty = GridView1.Rows[i].FindControl("TextBox2") as TextBox;
                    TextBox txtExtraReceivingQty = GridView1.Rows[i].FindControl("txtExtraQuantity") as TextBox;
                    Label lblStation = GridView1.Rows[i].FindControl("Label2") as Label;
                    Label lblRwkNo = GridView1.Rows[i].FindControl("lblrew") as Label;

                    Label lblToolNo = GridView1.Rows[i].FindControl("Label1") as Label;
                    LinkButton lnkbtnPNo = GridView1.Rows[i].FindControl("Label3") as LinkButton;
                    Label lblPartName = GridView1.Rows[i].FindControl("Label4") as Label;
                    Label lblQuantity = GridView1.Rows[i].FindControl("Label8") as Label;
                    Label lblUploadQty = GridView1.Rows[i].FindControl("Label6Upload_Qty") as Label;
                    DropDownList ddlChallanNumbers = GridView1.Rows[i].FindControl("ddlChallanNumbers") as DropDownList;
                    Label lblExtraReceivedQty = GridView1.Rows[i].FindControl("lblExtraReceivedQty") as Label;
                    Label lblSentQty = GridView1.Rows[i].FindControl("Label6sentqty") as Label;

                    HiddenField hdnPartsGrId = GridView1.Rows[i].FindControl("hdnPartsGrId") as HiddenField;
                    HiddenField hdnLocation = GridView1.Rows[i].FindControl("hdnLocation") as HiddenField;
                    HiddenField hdnSentReceivedDate = GridView1.Rows[i].FindControl("hdnSentReceivedDate") as HiddenField;
                    HiddenField hdnReceivedDate = GridView1.Rows[i].FindControl("hdnReceivedDate") as HiddenField;
                    HiddenField hdnDeliveryCommitDate = GridView1.Rows[i].FindControl("hdnDeliveryCommitDate") as HiddenField;

                    if (i < dt.Rows.Count)
                    {
                        //Assign the value from DataTable to the TextBox 
                        chkGridAllchild.Checked = Convert.ToBoolean(dt.Rows[i]["Is_Selected"]);
                        if (chkGridAllchild.Checked)
                        {
                            txtNewReceivingQty.Enabled = true;
                            txtExtraReceivingQty.Enabled = true;
                        }
                        hdnPartsGrId.Value = Convert.ToString(dt.Rows[i]["Parts_Gr_Id"]);
                        hdnLocation.Value = Convert.ToString(dt.Rows[i]["Location"]);
                        hdnSentReceivedDate.Value = Convert.ToString(dt.Rows[i]["Sent_Received_Date"]);
                        hdnReceivedDate.Value = Convert.ToString(dt.Rows[i]["Received_Date"]);
                        hdnDeliveryCommitDate.Value = Convert.ToString(dt.Rows[i]["Delivery_Commit_Date"]);

                        lblToolNo.Text = Convert.ToString(dt.Rows[i]["tool_no"]);
                        lblStation.Text = Convert.ToString(dt.Rows[i]["station"]);
                        lnkbtnPNo.Text = Convert.ToString(dt.Rows[i]["p_no"]);
                        lblPartName.Text = Convert.ToString(dt.Rows[i]["part_name"]);
                        lblTotalQtySent.Text = Convert.ToString(dt.Rows[i]["quant_sent"]);
                        lblReceivedQty.Text = Convert.ToString(dt.Rows[i]["quant_rec"]);
                        ddlChallanNumbers.SelectedValue = Convert.ToString(dt.Rows[i]["Challan_No"]);
                        txtNewReceivingQty.Text = Convert.ToString(dt.Rows[i]["New_Rec_Quantity"]);
                        lblExtraReceivedQty.Text = Convert.ToString(dt.Rows[i]["Extra_Qty_Received"]);
                        txtExtraReceivingQty.Text = Convert.ToString(dt.Rows[i]["Extra_Receiving_Qty"]);
                        lblUploadQty.Text = Convert.ToString(dt.Rows[i]["Upload_Qty"]);
                        lblSentQty.Text = Convert.ToString(dt.Rows[i]["SentQty"]);
                        lblRwkNo.Text = Convert.ToString(dt.Rows[i]["Rwk"]);
                    }
                    rowIndex++;
                }
            }
        }
    }

    void load(GridViewUpdateEventArgs e)
    {

        location = ddllocation.SelectedItem.Text;
        tool_no = Convert.ToInt32(((Label)GridView1.Rows[e.RowIndex].FindControl("Label1")).Text);
        station = ((Label)GridView1.Rows[e.RowIndex].FindControl("Label2")).Text.ToString();
        p_no = ((LinkButton)GridView1.Rows[e.RowIndex].FindControl("Label3")).Text.ToString();
        part_name = ((Label)GridView1.Rows[e.RowIndex].FindControl("Label4")).Text.ToString();
        quant_sent = Convert.ToInt32(((Label)GridView1.Rows[e.RowIndex].FindControl("Label5")).Text);
        quant_rec = Convert.ToInt32(((Label)GridView1.Rows[e.RowIndex].FindControl("Label6")).Text);
        new_quantity_rec = Convert.ToInt32(((TextBox)GridView1.Rows[e.RowIndex].FindControl("TextBox2")).Text);
        DateTime str_dt1 = System.DateTime.Now;
        str_dt = string.Format("{0:yyyy-MM-dd}", str_dt1); // you can specify format 
        CultureInfo provider = CultureInfo.InvariantCulture;
        DateTime.ParseExact(str_dt, "yyyy-m-d", provider);
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }
    
    /// <summary>
    /// Envent is fired when user checks the Checkbox shown in front of each row
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAllchild_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox childCheck = (CheckBox)sender;
        txtchallanno.Text = String.Empty;

        CheckBox ChkBoxRows = (CheckBox)childCheck.FindControl("chkAllchild");
        TextBox TextBox2 = (TextBox)childCheck.FindControl("TextBox2");
        TextBox txtExtraQuantity = (TextBox)childCheck.FindControl("txtExtraQuantity");

        if (ChkBoxRows.Checked == true)
        {
            ChkBoxRows.Checked = true;
            TextBox2.Visible = true;
            txtExtraQuantity.Visible = true;
            btnRecieved.Visible = true;
            btnRecieved.Enabled = true;
        }
        else
        {
            TextBox2.Enabled = false;
            txtExtraQuantity.Enabled = false;
            btnRecieved.Enabled = false;
        }
        EnableTextBox(ChkBoxRows);
        int checkCount = 0;
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox chkCheckBoxChecked = (CheckBox)GridView1.Rows[row.RowIndex].FindControl("chkAllchild");
            if (chkCheckBoxChecked.Checked)
            {
                checkCount = checkCount + 1;
            }
        }

        if (checkCount > 0)
        {
            btnRecieved.Visible = true;
            btnRecieved.Enabled = true;
        }
        UpdatePanel1.Update();
    }

    /// <summary>
    /// Rebinds the Child Positions for each GridView Row shown on Child Postions Column
    /// </summary>
    protected void RebindChildPositions()
    {

        foreach (GridViewRow gr in GridView1.Rows)
        {
            HiddenField hdnChildPositions = (HiddenField)gr.FindControl("hdnChildPositions");
            PlaceHolder lblChildPositions = (PlaceHolder)gr.FindControl("placeHolderChildPositions");
            string childPositions = hdnChildPositions.Value;
            string[] splitChildPositions = childPositions.TrimEnd(',').Split(',');
            for (int i = 0; i < splitChildPositions.Length; i++)
            {
                LinkButton lnkBtnChild = new LinkButton();
                lnkBtnChild.ID = "lnkBtnChild" + gr.RowIndex + "_" + i + 1;
                lblChildPositions.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                lnkBtnChild.Text = splitChildPositions[i];
                lnkBtnChild.Visible = true;
                lnkBtnChild.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                lnkBtnChild.CommandName = "ShowChildHistory";
                lblChildPositions.Controls.Add(lnkBtnChild);
                lnkBtnChild.Click += new EventHandler(lnkBtnChild_Click);
                Literal literalControl = new Literal();
                if (i != splitChildPositions.Length - 1)
                {
                    literalControl.Text = ",";
                }
                lblChildPositions.Controls.Add(literalControl);
                UpdatePanel1.Update();
            }
        }
    }

    /// <summary>
    /// Event is fired when user clicks on the Check Box shown on Header to Check all Positions
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAll1_CheckedChanged1(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)GridView1.HeaderRow.FindControl("chkAll1");
        txtchallanno.Text = String.Empty;
        btnRecieved.Enabled = false;
        int countchk = 0;
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            if (ChkBoxHeader.Checked == true)
            {
                if (ChkBoxRows.Enabled)
                {
                    btnRecieved.Enabled = true;
                    ChkBoxRows.Checked = true;
                    countchk = countchk + 1;
                }
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
            EnableTextBox(ChkBoxRows);
        }
        if (countchk > 0)
        {
            btnRecieved.Visible = true;
            btnRecieved.Enabled = true;
        }
        UpdatePanel1.Update();
    }

    /// <summary>
    /// If checkbox is check it validates and displays the Valid Quantity that user can receive in the TextBox
    /// </summary>
    /// <param name="cb"></param>
    protected void EnableTextBox(CheckBox cb)
    {
        DropDownList ddlChallanNumbers = (DropDownList)cb.FindControl("ddlChallanNumbers");
        HiddenField hdnPendingRowsAtVendor = cb.FindControl("hdnPendingRowsAtVendor") as HiddenField;
        TextBox txtExtraReceivingQty = cb.FindControl("txtExtraQuantity") as TextBox;
        TextBox txtNewReceivingQty = cb.FindControl("TextBox2") as TextBox;
        HiddenField hdnPartsGrId = cb.FindControl("hdnPartsGrId") as HiddenField;
        GridViewRow gridViewRow = (GridViewRow)cb.NamingContainer;
        if (cb.Checked == true)
        {
            btnRecieved.Visible = true;
            string[] splitValues = Regex.Split(ddlChallanNumbers.SelectedValue, " - ");

            Label lblTotalQtySent = cb.FindControl("Label5") as Label;
            Label lblReceivedQty = cb.FindControl("Label6") as Label;

            Label lblStation = cb.FindControl("Label2") as Label;
            Label lblrew = cb.FindControl("lblrew") as Label;
            Label lblToolNo = cb.FindControl("Label1") as Label;
            LinkButton lnkbtnPNo = cb.FindControl("Label3") as LinkButton;
            Label lblPartName = cb.FindControl("Label4") as Label;
            Label lblTotalQtySentAtVendor = cb.FindControl("Label6sentqty") as Label;
            if (ddlChallanNumbers.Items.Count == 2)
                ddlChallanNumbers.SelectedIndex = 1;
            string challanRecQty = "";
            if (splitValues.Length > 1)
            {
                challanRecQty = splitValues[1];
            }
            else
            {
                challanRecQty = "";
            }
            int? availableAndValidQty = ValidateQuantities(lblToolNo.Text, lblStation.Text, lblPartName.Text, lnkbtnPNo.Text, lblrew.Text, ddllocation.SelectedItem.Text, challanRecQty, gridViewRow);
            int totalChallansCount = ddlChallanNumbers.Items.Count - 1;
            if (availableAndValidQty != null && availableAndValidQty > 0)
            {
                if (ddllocation.SelectedItem.Text == "Weld Assembly Waiting")
                {
                    int? validParentQtyAfterRatio = ValidateRationWiseQuantity(hdnPartsGrId.Value, Convert.ToInt32(availableAndValidQty.Value));
                    txtNewReceivingQty.Text = Convert.ToString(validParentQtyAfterRatio);
                }
                else
                {
                    txtNewReceivingQty.Text = Convert.ToString(availableAndValidQty.Value);
                }
            }
            else
            {
                txtNewReceivingQty.Text = String.Empty;
            }

            if (lblTotalQtySentAtVendor.Text.Length > 0)
            {
                txtNewReceivingQty.Enabled = true;
                txtExtraReceivingQty.Enabled = true;
                ddlChallanNumbers.Enabled = true;
            }
            else
            {
                txtNewReceivingQty.Text = String.Empty;
            }
        }
        else
        {
            txtNewReceivingQty.Text = "";
            txtNewReceivingQty.Enabled = false;
            txtExtraReceivingQty.Enabled = false;
            btnRecieved.Visible = false;
            ddlChallanNumbers.Enabled = false;
            ddlChallanNumbers.ClearSelection();
        }
    }

    /// <summary>
    /// Calculates the Valid Parent Quantity user can receive based on Child Position received Ratio
    /// </summary>
    /// <param name="partsGRId"></param>
    /// <param name="parentAvailableQty"></param>
    /// <returns></returns>
    protected int? ValidateRationWiseQuantity(string partsGRId, int parentAvailableQty)
    {
        int? validParentQtyToBeSent = null;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            //Validate Ration Wise Quantity
            DataTable dtChildItems = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter("select pcr.Parts_Gr_Id as ParentPartsGrId, chi.Parts_Gr_Id as ChildPartsGrId, chi.Part_Name, pcr.P_No, pcr.Ratio, pg.Tool_No, pg.Station, pg.Rwk_No from Parent_Child_Ratio_Detail pcr "
                     + " inner join Parts_Gr pg on pcr.Parts_Gr_Id= pg.Parts_Gr_Id left join parts_Gr chi on pcr.P_No= chi.P_No and pg.Tool_No= chi.Tool_No and "
                     + " pg.Station= chi.Station and pg.Rwk_No=chi.Rwk_No  where pcr.Parts_Gr_Id=@PartsGrId", connec);
            ad.SelectCommand.Parameters.AddWithValue("@PartsGrId", Convert.ToInt32(partsGRId));
            ad.Fill(dtChildItems);
            if (dtChildItems.Rows.Count > 0)
            {
                List<int> arrChildCanBeMade = new List<int>();
                for (int j = 0; j < dtChildItems.Rows.Count; j++)
                {
                    int ratio = Convert.ToInt32(dtChildItems.Rows[j]["Ratio"]);
                    ad = new SqlDataAdapter("select location,isnull(quant_sent,0) as quant_sent, isnull(Upload_Qty,0) as Upload_Qty, isnull((isnull(quant_sent,0)- isnull(quant_rec,0)),0) as quant_pend from location_info_Gr where tool_no='" + dtChildItems.Rows[j]["tool_no"] + "'  and  p_no='" + dtChildItems.Rows[j]["p_no"] + "' and part_name='" + dtChildItems.Rows[j]["part_name"] + "' and  station='" + dtChildItems.Rows[j]["station"] + "' and rwk='" + dtChildItems.Rows[j]["rwk_no"] + "' and (location='Weld Parts Waiting' or location='Weld Parts Consumed')", connec);
                    DataTable dtValidate = new DataTable();
                    ad.Fill(dtValidate);
                    int pendingQty = 0;
                    for (int k = 0; k < dtValidate.Rows.Count; k++)
                    {
                        pendingQty = Convert.ToInt32(dtValidate.Rows[k]["quant_pend"]) + pendingQty;
                    }
                    int parentQtyCanBeMadeWithChild = pendingQty / ratio;
                    arrChildCanBeMade.Add(parentQtyCanBeMadeWithChild);
                }
                int minParentQty = arrChildCanBeMade.Min();
                if (minParentQty > parentAvailableQty)
                {
                    validParentQtyToBeSent = (int)parentAvailableQty;
                }
                else if (minParentQty <= parentAvailableQty)
                {
                    validParentQtyToBeSent = (int)minParentQty;
                }
            }
            else
            {
                validParentQtyToBeSent = parentAvailableQty;
            }
            return validParentQtyToBeSent;

        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void txtbarcode_TextChanged(object sender, EventArgs e)
    {
        DateTime lastKeyPress = DateTime.Now;
        if (((TimeSpan)(DateTime.Now - lastKeyPress)).Seconds <= 0.5)
        {
            string str = null;
            string[] strArr = null;
            str = txtbarcode.Text;
            char[] splitchar = { ' ' };
            strArr = str.Split(splitchar);
            txttoolno.Text = strArr[0];
            txtstationno.Text = strArr[1];
            txtposition.Text = strArr[2];
            fill_qty();
            ViewState["toolno"] = txttoolno.Text;
            ViewState["stationno"] = txtstationno.Text;
            ViewState["position"] = txtposition.Text;
            ViewState["Qty"] = txtQty.Text;
            ViewState["SENT"] = txtsentQty1.Text;
            ViewState["vendor"] = ddllocauto.SelectedItem.Text;
            ViewState["ItemName"] = txtname1.Text;
            ViewState["TotalsentQty"] = txttotalsent.Text;
            txtbarcode.Text = "";
            txtbarcode.Focus();
        }
        else
        {
            txtbarcode.Enabled = false;
        }
        lastKeyPress = DateTime.Now;
    }

    public void fill_qty()
    {
        SqlCommand cmd;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);

        conn.Open();
        try
        {
            if (txtstationno.Text.Length > 0 && txtposition.Text.Length > 0 && txttoolno.Text.Length > 0)
            {
                cmd = new SqlCommand("select tool_no,station,p_no,part_name,quant_sent,quant_rec from location_info_GR where location=@location and tool_no=@tool_no and station=@station_no and p_no=@positio_no and isnull(quant_sent,0)>=ISNULL(quant_rec,0)");

                cmd.Parameters.AddWithValue("@tool_no", txttoolno.Text);
                cmd.Parameters.AddWithValue("@station_no", txtstationno.Text);
                cmd.Parameters.AddWithValue("@positio_no", txtposition.Text);
                cmd.Parameters.AddWithValue("@location", ddllocauto.SelectedItem.Text);
                cmd.Connection = conn;
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                if (dt.Rows.Count > 0)
                {
                    txtQty.Text = dt.Rows[0]["quant_sent"].ToString();
                    txtsentQty1.Text = dt.Rows[0]["quant_rec"].ToString();
                    txtname1.Text = dt.Rows[0]["part_name"].ToString();
                    txttotalsent.Text = dt.Rows[0]["quant_sent"].ToString();
                }
                else
                {
                    txtQty.Text = "0";
                    txtsentQty1.Text = "0";
                    txtname1.Text = "";
                    txttotalsent.Text = "0";
                    MessageBox("Record Not Found");
                    return;
                }
            }
            else
            {
                MessageBox(" Tool no or Station No or Position No should not be empty");
                return;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    protected void txtQty_TextChanged(object sender, EventArgs e)
    {
        if (txtQty.Text.Length > 0)
        {
            ViewState["Qty"] = txtQty.Text;
            txtbarcode.Focus();
        }
    }

    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        if (RadioButton1.Checked == true)
        {
            Panel2.Visible = true;
            Panel4.Visible = false;
            Panel1.Visible = false;
            RadioButton1.Checked = true;
            RadioButton2.Checked = false;
            txtbarcode.Focus();

            string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work order by id asc ";
            string dataTextField = "VENDOR_NAME";
            string dataValueField = "ID";
            UtilityFunctions.bind_vendor(ddllocauto, query, dataTextField, dataValueField);
        }
        else
        {
            Panel4.Visible = false;
        }
    }

    protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
    {
        if (RadioButton2.Checked == true)
        {
            Panel2.Visible = false;
            RadioButton1.Checked = false;
            RadioButton2.Checked = true;
            Panel4.Visible = true;
            Panel2.Visible = false;
            Panel1.Visible = false;
        }
        else
        {
            Panel2.Visible = true;
        }
    }

    /// <summary>
    /// Event is fired when user clicks on Received Button. Checked rows are Validated and if Data is valid then Data is Inserted in Part History Gr table as Received,
    /// Quantity is updated for Company Location in Part History Gr table, Quantiy is Updated at Location Info Gr table for received Position.
    /// Data is Updated in Challan Sent Received Detail table also.
    /// Challan is generated which can be printed by the User
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRecieved_Click(object sender, EventArgs e)
    {
        string prefixStr = "";
        btnRecieved.Attributes.Add("onclick", " this.disabled = true; " + ClientScript.GetPostBackEventReference(btnRecieved, null) + ";");
        approvername = Session["UserID"].ToString();
        approvername = approvername.Replace("_", " ");

        int count = 0;
        int uploadqty1 = 0;
        string sqlconnstring1 = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring1);
        SqlTransaction trans = null;
        if (!isRequestedForReceive)
        {
            if (txtReceivecdate.Text.Length > 0)
            {
                try
                {
                    foreach (GridViewRow row in GridView1.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                            DropDownList ddlChallanNumbers = (DropDownList)row.FindControl("ddlChallanNumbers");
                            if (isSelected)
                            {
                                if (ddlChallanNumbers.SelectedValue == "")
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please select Sent Challan Number and then click on Receive Button.');", true);
                                    return;
                                }
                            }
                        }
                    }

                    conn.Open();
                    trans = conn.BeginTransaction();
                    if (checkIsVirtualTransaction.Checked)
                    {
                        prefixStr = "V_";
                    }
                    else
                    {
                        prefixStr = "CH";
                    }
                    string generatedChallanNumber = "";
                    generatedChallanNumber = UtilityFunctions.gencode(prefixStr);
                    txtchallanno.Text = generatedChallanNumber;
                    if (!string.IsNullOrEmpty(generatedChallanNumber))
                    {
                        foreach (GridViewRow row in GridView1.Rows)
                        {
                            if (row.RowType == DataControlRowType.DataRow)
                            {
                                DateTime str_dt1 = Convert.ToDateTime(txtReceivecdate.Text);
                                str_dt = string.Format("{0:yyyy-MM-dd}", str_dt1); // you can specify format 
                                Label quant_sent1 = row.FindControl("Label5") as Label;
                                Label quant_rec1 = row.FindControl("Label6") as Label;
                                TextBox new_quantity_rec1 = row.FindControl("TextBox2") as TextBox;
                                TextBox txtExtraQuantity = row.FindControl("txtExtraQuantity") as TextBox;
                                Label station1 = row.FindControl("Label2") as Label;
                                Label rework_no = row.FindControl("lblrew") as Label;
                                Label lblrew = row.FindControl("lblrew") as Label;

                                Label tool_no1 = row.FindControl("Label1") as Label;
                                LinkButton POSITION = row.FindControl("Label3") as LinkButton;
                                Label part_name1 = row.FindControl("Label4") as Label;
                                CultureInfo provider = CultureInfo.InvariantCulture;
                                Label quantity = row.FindControl("Label8") as Label;
                                Label uploadqty = row.FindControl("Label6Upload_Qty") as Label;
                                Label sent = row.FindControl("Labelsent") as Label;
                                DropDownList ddlChallanNumbers = (DropDownList)row.FindControl("ddlChallanNumbers");
                                HiddenField hdnPartsGrId = (HiddenField)row.FindControl("hdnPartsGrId");
                                Label lblTotalSentQty = (Label)row.FindControl("Label6sentqty");
                                quant_sent = Convert.ToInt32(quant_sent1.Text);
                                quant_rec = Convert.ToInt32(quant_rec1.Text);
                                uploadqty1 = Convert.ToInt32(uploadqty.Text);
                                tool_no = Convert.ToInt32(tool_no1.Text);
                                p_no = POSITION.Text;
                                station = station1.Text;
                                part_name = Convert.ToString(part_name1.Text);
                                location = ddllocation.SelectedItem.Text;
                                string kk = System.DateTime.Now.ToLongTimeString();
                                str_dt = str_dt + " " + kk;

                                bool isValidData = false;
                                bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;

                                if (isSelected)
                                {
                                    if (ddlChallanNumbers.SelectedValue == "")
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please select Sent Challan Number and then click on Receive Button.');", true);
                                        return;
                                    }

                                    SqlCommand cmdCheckQuantity = new SqlCommand("select tool_no,station,Upload_Qty,p_no,part_name,quant_sent,quant_rec,isnull(quant_sent,0)-isnull(quant_rec,0) as SentQty,Rwk from location_info_GR where location='" + ddllocation.SelectedItem.Text + "' and (isnull(quant_sent,0)-isnull(quant_rec,0))>0 and location='" + ddllocation.SelectedItem.Text + "' and p_no='" + p_no + "' and part_name='" + part_name + "' and tool_no='" + tool_no + "' "
                                    + " and station='" + station + "' and rwk='" + rework_no.Text + "'  order by tool_no,station asc", conn, trans);
                                    using (SqlDataReader reader = cmdCheckQuantity.ExecuteReader())
                                    {
                                        if (reader.HasRows)
                                        {
                                            reader.Read();
                                            int quantSent = Convert.ToInt32(reader["quant_sent"]);
                                            int quantReceive = Convert.ToInt32(reader["quant_rec"]);
                                            new_quantity_rec = Convert.ToInt32(new_quantity_rec1.Text);
                                            if ((quantSent) >= new_quantity_rec + quantReceive)
                                            {
                                                isValidData = true;
                                            }
                                        }
                                    }
                                }

                                if (isSelected)
                                {
                                    if (isValidData)
                                    {
                                        if (ddllocation.SelectedItem.Text.Length > 0)
                                        {
                                            if (new_quantity_rec1.Text.Length > 0)
                                            {
                                                new_quantity_rec = Convert.ToInt32(new_quantity_rec1.Text);
                                                if ((quant_sent) >= new_quantity_rec + quant_rec)
                                                {
                                                    if (Convert.ToInt32(new_quantity_rec1.Text) > 0)
                                                    {
                                                        UpdateLocationInfoGRData(conn, trans, p_no, part_name, lblrew.Text, tool_no, station, location, uploadqty1, txtExtraQuantity.Text, str_dt, new_quantity_rec, txtchallanno.Text, ddlChallanNumbers.SelectedValue);

                                                        if (ddllocation.SelectedItem.Text == "Weld Assembly Waiting")
                                                        {
                                                            //Calculate Ratio

                                                            int totalPendingRows = 0;
                                                            SqlDataAdapter adRatio = new SqlDataAdapter();
                                                            DataTable dt = new DataTable();
                                                            DataTable dtChildItemsRatio = new DataTable();
                                                            string childPositions = "";
                                                            string parentName = "P_" + p_no;
                                                            string currentPNo = "";
                                                            int minValue = 0;
                                                            adRatio = new SqlDataAdapter("select pcr.Parts_Gr_Id as ParentPartsGrId, chi.Parts_Gr_Id as ChildPartsGrId, chi.Part_Name, pcr.P_No, pcr.Ratio, pg.Tool_No, pg.Station, pg.Rwk_No from Parent_Child_Ratio_Detail pcr "
                                                             + " inner join Parts_Gr pg on pcr.Parts_Gr_Id= pg.Parts_Gr_Id left join parts_Gr chi on pcr.P_No= chi.P_No and pg.Tool_No= chi.Tool_No and "
                                                             + " pg.Station= chi.Station and pg.Rwk_No=chi.Rwk_No  where pcr.Parts_Gr_Id=@PartsGrId", conn);
                                                            adRatio.SelectCommand.Parameters.AddWithValue("@PartsGrId", Convert.ToInt32(hdnPartsGrId.Value));
                                                            adRatio.SelectCommand.Transaction = trans;
                                                            adRatio.Fill(dtChildItemsRatio);
                                                            if (dtChildItemsRatio.Rows.Count > 0)
                                                            {
                                                                int parentAvailableQty = Convert.ToInt32(lblTotalSentQty.Text);
                                                                List<int> parentQtyMadeWithChild = new List<int>();
                                                                for (int j = 0; j < dtChildItemsRatio.Rows.Count; j++)
                                                                {
                                                                    int ratio = Convert.ToInt32(dtChildItemsRatio.Rows[j]["Ratio"]);
                                                                    SqlDataAdapter adLocationInfo = new SqlDataAdapter("select location,isnull(quant_sent,0) as quant_sent, isnull(Upload_Qty,0) as Upload_Qty, isnull((isnull(quant_sent,0)- isnull(quant_rec,0)),0) as quant_pend from location_info_Gr where tool_no='" + dtChildItemsRatio.Rows[j]["tool_no"] + "'  and  p_no='" + dtChildItemsRatio.Rows[j]["p_no"] + "' and part_name='" + dtChildItemsRatio.Rows[j]["part_name"] + "' and  station='" + dtChildItemsRatio.Rows[j]["station"] + "' and rwk='" + dtChildItemsRatio.Rows[j]["rwk_no"] + "' and (location='Weld Parts Waiting')", conn);// or location='Weld Parts Consumed'
                                                                    adLocationInfo.SelectCommand.Transaction = trans;
                                                                    DataTable dtValidate = new DataTable();
                                                                    adLocationInfo.Fill(dtValidate);
                                                                    int pendingQty = 0;
                                                                    for (int k = 0; k < dtValidate.Rows.Count; k++)
                                                                    {
                                                                        pendingQty = Convert.ToInt32(dtValidate.Rows[k]["quant_pend"]) + pendingQty;
                                                                    }
                                                                    int parentQtyCanBeMadeWithChild = pendingQty / ratio;
                                                                    parentQtyMadeWithChild.Add(parentQtyCanBeMadeWithChild);
                                                                }
                                                                minValue = parentQtyMadeWithChild.Min();
                                                            }
                                                            //Ration Calculated

                                                            DataTable dtable = new DataTable();
                                                            //string parentName = "P_" + p_no;
                                                            //string currentPNo = "";
                                                            SqlDataAdapter ad = new SqlDataAdapter();

                                                            ad = new SqlDataAdapter("select * from parts_Gr where ((opr17 like '%" + parentName + ",%' or opr17 like '%," + parentName + "' or opr17 like '%," + parentName + ",%' or opr17 like '" + parentName + "') or (Parent_Positions like '%" + parentName + ",%' or Parent_Positions like '%," + parentName + "' or Parent_Positions like '%," + parentName + ",%' or Parent_Positions like '" + parentName + "'))  and tool_no='" + tool_no + "'  and  station='" + station + "' and rwk_no='" + lblrew.Text + "' and Is_Active_Record=1", conn);
                                                            ad.SelectCommand.Transaction = trans;
                                                            ad.Fill(dtable);
                                                            //int totalPendingRows = 0;
                                                            int itemsMovedAtWeldPartsWaitingLocation = 0;
                                                            int weldPartsWaitingQuantity = 0;

                                                            DataTable dtChildParts = new DataTable();
                                                            dtChildParts.Columns.Add("p_no", typeof(string));
                                                            dtChildParts.Columns.Add("part_name", typeof(string));
                                                            dtChildParts.Columns.Add("rwk", typeof(string));
                                                            dtChildParts.Columns.Add("tool_no", typeof(string));
                                                            dtChildParts.Columns.Add("station", typeof(string));
                                                            dtChildParts.Columns.Add("location", typeof(string));
                                                            dtChildParts.Columns.Add("upload_qty", typeof(int));
                                                            dtChildParts.Columns.Add("quant_pend", typeof(int));

                                                            for (int i = 0; i < dtable.Rows.Count; i++)
                                                            {
                                                                currentPNo = Convert.ToString(dtable.Rows[i]["p_no"]);
                                                                int pendingQuantity = 0, uploadQuantity = 0;
                                                                ad = new SqlDataAdapter("select p_no,part_name,rwk,tool_no,station,location,isnull(quant_sent,0) as quant_sent, isnull(Upload_Qty,0) as Upload_Qty, isnull((isnull(quant_sent,0)- isnull(quant_rec,0)),0) as quant_pend from location_info_Gr where tool_no='" + dtable.Rows[i]["tool_no"] + "'  and  p_no='" + dtable.Rows[i]["p_no"] + "' and part_name='" + dtable.Rows[i]["part_name"] + "' and  station='" + dtable.Rows[i]["station"] + "' and rwk='" + dtable.Rows[i]["rwk_no"] + "' ", conn);
                                                                ad.SelectCommand.Transaction = trans;
                                                                DataTable dtPendingChilds = new DataTable();
                                                                ad.Fill(dtPendingChilds);
                                                                for (int l = 0; l < dtPendingChilds.Rows.Count; l++)
                                                                {
                                                                    if ((Convert.ToString(dtPendingChilds.Rows[l]["Location"]) == "Weld Parts Waiting" || Convert.ToString(dtPendingChilds.Rows[l]["Location"]) == "Weld Parts Consumed"))
                                                                    {
                                                                        if ((Convert.ToInt32(dtPendingChilds.Rows[l]["Upload_Qty"]) == Convert.ToInt32(dtPendingChilds.Rows[l]["quant_pend"]) && dtChildItemsRatio.Rows.Count == 0) || (dtChildItemsRatio.Rows.Count > 0))
                                                                        {
                                                                            itemsMovedAtWeldPartsWaitingLocation = itemsMovedAtWeldPartsWaitingLocation + 1;
                                                                            if (Convert.ToString(dtPendingChilds.Rows[l]["Location"]) == "Weld Parts Waiting")
                                                                            {
                                                                                DataRow dr = dtChildParts.NewRow();
                                                                                dr["p_no"] = Convert.ToString(dtPendingChilds.Rows[l]["p_no"]);
                                                                                dr["part_name"] = Convert.ToString(dtPendingChilds.Rows[l]["part_name"]);
                                                                                dr["rwk"] = Convert.ToString(dtPendingChilds.Rows[l]["rwk"]);
                                                                                dr["tool_no"] = Convert.ToString(dtPendingChilds.Rows[l]["tool_no"]);
                                                                                dr["station"] = Convert.ToString(dtPendingChilds.Rows[l]["station"]);
                                                                                dr["location"] = Convert.ToString(dtPendingChilds.Rows[l]["location"]);
                                                                                dr["upload_qty"] = Convert.ToInt32(dtPendingChilds.Rows[l]["upload_qty"]);
                                                                                List<DataRow> drChildRatio = dtChildItemsRatio.Select("tool_no='" + dtPendingChilds.Rows[l]["tool_no"] + "'  and  p_no='" + dtPendingChilds.Rows[l]["p_no"] + "' and part_name='" + dtPendingChilds.Rows[l]["part_name"] + "' and  station='" + dtPendingChilds.Rows[l]["station"] + "' and rwk_no='" + dtPendingChilds.Rows[l]["rwk"] + "'").ToList();
                                                                                if (drChildRatio.Count > 0)
                                                                                {
                                                                                    int ratio = Convert.ToInt32(drChildRatio[0]["Ratio"]);
                                                                                    int pending = Convert.ToInt32(dtPendingChilds.Rows[l]["quant_pend"]);
                                                                                    int enteredParentQty = Convert.ToInt32(new_quantity_rec1.Text);
                                                                                    int qtyAsPerRatioWrtParentQty = ratio * enteredParentQty;
                                                                                    if (qtyAsPerRatioWrtParentQty > 0 && pending >= qtyAsPerRatioWrtParentQty)
                                                                                    {
                                                                                        dr["quant_pend"] = qtyAsPerRatioWrtParentQty;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        dr["quant_pend"] = pending;
                                                                                    }
                                                                                    uploadQuantity = Convert.ToInt32(dtPendingChilds.Rows[l]["Upload_Qty"]);
                                                                                    pendingQuantity = Convert.ToInt32(dr["quant_pend"]);
                                                                                }
                                                                                else
                                                                                {
                                                                                    dr["quant_pend"] = Convert.ToInt32(dtPendingChilds.Rows[l]["quant_pend"]);
                                                                                }
                                                                                if (Convert.ToInt32(dr["quant_pend"]) > 0)
                                                                                {
                                                                                    dtChildParts.Rows.Add(dr);
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            if (dtChildItemsRatio.Rows.Count == 0)
                                                                            {
                                                                                if (Convert.ToString(dtPendingChilds.Rows[l]["Location"]) == "Weld Parts Waiting")
                                                                                {
                                                                                    weldPartsWaitingQuantity = Convert.ToInt32(dtPendingChilds.Rows[l]["quant_pend"]);
                                                                                }
                                                                                uploadQuantity = Convert.ToInt32(dtPendingChilds.Rows[l]["Upload_Qty"]);
                                                                                pendingQuantity = Convert.ToInt32(dtPendingChilds.Rows[l]["quant_pend"]) + pendingQuantity;
                                                                            }
                                                                        }
                                                                    }

                                                                    if (Convert.ToInt32(dtPendingChilds.Rows[l]["quant_pend"]) > 0)
                                                                    {
                                                                        totalPendingRows = totalPendingRows + 1;
                                                                    }

                                                                    //if (l == (dtPendingChilds.Rows.Count - 1))
                                                                    //{
                                                                    //    if (pendingQuantity > 0 && uploadQuantity == pendingQuantity)
                                                                    //    {
                                                                    //        itemsMovedAtWeldPartsWaitingLocation = itemsMovedAtWeldPartsWaitingLocation + 2;

                                                                    //        DataRow dr = dtChildParts.NewRow();
                                                                    //        dr["p_no"] = Convert.ToString(dtPendingChilds.Rows[l]["p_no"]);
                                                                    //        dr["part_name"] = Convert.ToString(dtPendingChilds.Rows[l]["part_name"]);
                                                                    //        dr["rwk"] = Convert.ToString(dtPendingChilds.Rows[l]["rwk"]);
                                                                    //        dr["tool_no"] = Convert.ToString(dtPendingChilds.Rows[l]["tool_no"]);
                                                                    //        dr["station"] = Convert.ToString(dtPendingChilds.Rows[l]["station"]);
                                                                    //        dr["location"] = Convert.ToString(dtPendingChilds.Rows[l]["location"]);
                                                                    //        dr["upload_qty"] = Convert.ToInt32(dtPendingChilds.Rows[l]["upload_qty"]);
                                                                    //        dr["quant_pend"] = weldPartsWaitingQuantity;
                                                                    //        dtChildParts.Rows.Add(dr);
                                                                    //    }
                                                                    //}
                                                                }
                                                            }
                                                            string childPartsChallanNumber = "";
                                                            if (totalPendingRows == itemsMovedAtWeldPartsWaitingLocation)
                                                            {
                                                                if (dtChildParts.Rows.Count > 0)
                                                                {
                                                                    for (int m = 0; m < dtChildParts.Rows.Count; m++)
                                                                    {
                                                                        if (m == 0)
                                                                        {
                                                                            childPartsChallanNumber = UtilityFunctions.gencode(prefixStr);
                                                                        }
                                                                        UpdateLocationInfoGRData(conn, trans, Convert.ToString(dtChildParts.Rows[m]["p_no"]), Convert.ToString(dtChildParts.Rows[m]["part_name"]), Convert.ToString(dtChildParts.Rows[m]["rwk"]), Convert.ToInt32(dtChildParts.Rows[m]["tool_no"]),
                                                                        Convert.ToString(dtChildParts.Rows[m]["station"]), Convert.ToString(dtChildParts.Rows[m]["location"]), Convert.ToInt32(dtChildParts.Rows[m]["upload_qty"]), "", str_dt, Convert.ToInt32(dtChildParts.Rows[m]["quant_pend"]), childPartsChallanNumber, ddlChallanNumbers.SelectedValue);
                                                                    }
                                                                    SendChildPartsToWeldPartsConsumed(conn, trans, childPartsChallanNumber);
                                                                }
                                                            }
                                                        }
                                                        count = count + 1;
                                                    }
                                                    else
                                                    {
                                                        //Updating Extra Received Quantity
                                                        int extraQuantity = 0;
                                                        if (!string.IsNullOrEmpty(txtExtraQuantity.Text))
                                                        {
                                                            extraQuantity = Convert.ToInt32(txtExtraQuantity.Text);
                                                        }
                                                        if (extraQuantity > 0)
                                                        {
                                                            string strQuery2 = "update location_info_GR set Extra_Received_Quantity=isnull(Extra_Received_Quantity,0)+" + extraQuantity + ", Received_Date='" + str_dt1.ToString("yyyy/MM/dd HH:mm:ss") + "' where p_no='" + p_no + "' and station='" + station + "'  and location='" + location + "' and part_name='" + part_name + "' and tool_no=" + tool_no + " and rwk='" + lblrew.Text + "' ";
                                                            SqlCommand cmd2 = new SqlCommand(strQuery2, conn, trans);
                                                            cmd2.ExecuteNonQuery();
                                                            count = count + 1;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Receive Qty should not be greater than sent Qty');", true);
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Enter Valid Receive Qty ');", true);
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Vendor');", true);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Quantity Entered. Quantiy has been modified Please refresh the Page.');", true);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Challan Number generation Failed.');", true);
                        return;
                    }
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    txtchallanno.Text = String.Empty;
                    isRequestedForReceive = false;
                    ErrorLogger.LogError("Receive_GR_Item", "btn_Received_Click", ex.Message, ex.StackTrace);
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
                    ex.ToString();
                }
                finally
                {
                    conn.Close();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Enter Received Date');", true);
                return;
            }

            if (count > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Items Received Successfully ');", true);
                btnRecieved.Visible = false;
            }
        }
        conn.Close();
        showgrid();
        isRequestedForReceive = false;
    }

    /// <summary>
    /// Child Positions are automatically sent to Weld Parts Consumed when user receives the Parent Postion from Weld Assembly Waiting Location
    /// </summary>
    /// <param name="conn"></param>
    /// <param name="trans"></param>
    /// <param name="challanNumber"></param>
    protected void SendChildPartsToWeldPartsConsumed(SqlConnection conn, SqlTransaction trans, string challanNumber)
    {
        try
        {
            string command = "insert";
            SqlCommand cmd = new SqlCommand();
            cmd.Transaction = trans;
            cmd.Connection = conn;
            SqlDataAdapter ad = new SqlDataAdapter("select * from part_history_Gr where Challan_No=@ChallanNumber", conn);
            ad.SelectCommand.Parameters.AddWithValue("@ChallanNumber", challanNumber);
            ad.SelectCommand.Transaction = trans;
            DataTable dt = new DataTable();
            ad.Fill(dt);
            int count = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int req_qty = 0;
                int quantity = Convert.ToInt32(dt.Rows[i]["quantity"]);
                int uploadQty = Convert.ToInt32(dt.Rows[i]["Upload_Qty"]);
                string strQuery4 = "update part_history_GR  set quantity=(quantity - " + quantity + ") ,QtySent=" + quantity + " + isnull(QtySent,0), Sent_Date='" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss") + "' where location='COMPANY'  and tool_no=" + Convert.ToString(dt.Rows[i]["Tool_No"]) + " and  p_no='" + Convert.ToString(dt.Rows[i]["p_no"]) + "' and station='" + Convert.ToString(dt.Rows[i]["station"]) + "' and part_name='" + Convert.ToString(dt.Rows[i]["part_name"]) + "' and  rwk_no='" + Convert.ToString(dt.Rows[i]["rwk_no"]) + "' ";
                SqlCommand cmd4 = new SqlCommand(strQuery4, conn, trans);
                Int32 k1 = cmd4.ExecuteNonQuery();

                //TO RETRIEVE REQUIRED QUANTITY OF THE PARTICULAR TOOL FROM PARTS TABLE
                s = "select req_qty from parts_GR where tool_no=" + Convert.ToString(dt.Rows[i]["Tool_No"]) + " and station='" + Convert.ToString(dt.Rows[i]["station"]) + "' and p_no='" + Convert.ToString(dt.Rows[i]["p_no"]) + "' and part_name='" + Convert.ToString(dt.Rows[i]["part_name"]) + "'  and rwk_no='" + Convert.ToString(dt.Rows[i]["rwk_no"]) + "'";
                cmd.CommandText = s;
                DataTable dtPartsGr = new DataTable();
                dtPartsGr.Load(cmd.ExecuteReader());
                if (dtPartsGr.Rows.Count == 0)
                {
                    lbl.Text = "No such part exists..!!";
                    return;
                }
                else
                {
                    req_qty = Convert.ToInt32(dtPartsGr.Rows[0][0].ToString());
                }
                //TO UPDATE part_history_GR TABLE
                string location = "Weld Parts Consumed";
                string childPartsSentChallanNo = UtilityFunctions.gencode("CH");

                s = "insert into part_history_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,Explain_Date,Delivery_commit_date,Challan_No,Sr_Reg_No,Batch_No,Required_date,PO_NO,EST_Hrs,Rework,Fresh,TransSentQty,Upload_Qty,Sent_Date,rwk_no,Is_Virtual_Transaction,User_name) values ('" + Convert.ToString(dt.Rows[i]["p_no"]) + "','" + Convert.ToString(dt.Rows[i]["part_name"]) + "', '" + Convert.ToString(dt.Rows[i]["tool_no"]) + "','" + Convert.ToString(dt.Rows[i]["station"]) + "','" + location + "','" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss") + "','Sent'," + quantity + "," + quantity + ",'" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss") + "','" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss") + "','" + childPartsSentChallanNo + "','','','','','','',''," + quantity + "," + uploadQty + ",'" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss") + "','" + Convert.ToString(dt.Rows[i]["Rwk_No"]) + "','" + checkIsVirtualTransaction.Checked + "','" + Convert.ToString(Session["UserID"]) + "')";
                cmd.CommandText = s;
                cmd.ExecuteNonQuery();

                //UPDATING TABLE LOCATION_INFO
                count = count + 1;
                int quantity1 = 0;
                //RETRIEVING TOTAL QUANTITTY SENT OF A PARTICULAR PART TO A PARTICULAR VENDOR
                s = "select quant_sent,quant_rec from location_info_GR where location='" + location + "' and tool_no=" + Convert.ToString(dt.Rows[i]["tool_no"]) + " and station='" + Convert.ToString(dt.Rows[i]["station"]) + "' and p_no='" + Convert.ToString(dt.Rows[i]["p_no"]) + "' and part_name='" + Convert.ToString(dt.Rows[i]["part_name"]) + "' and Rwk='" + Convert.ToString(dt.Rows[i]["Rwk_No"]) + "'";
                cmd.CommandText = s;
                DataTable dtLocationInfo = new DataTable();
                dtLocationInfo.Load(cmd.ExecuteReader());
                if (dtLocationInfo.Rows.Count != 0)
                {
                    command = "update";
                    quantity1 += Convert.ToInt32(dtLocationInfo.Rows[0][0].ToString());
                    quant_rec = Convert.ToInt32(dtLocationInfo.Rows[0][1].ToString());
                }

                //CALCULATING THE PENDING QUANTITY
                int quant_pend = quantity1 - quant_rec;

                //FINDING QUANTITY PENDING WITH PREVIOUS VENDOR

                //FINDING FIRST VENDOR  and QUANTITY SENT TO HIM
                s = "select top 1 location from part_history_GR where tool_no=" + Convert.ToString(dt.Rows[i]["tool_no"]) + " and station='" + Convert.ToString(dt.Rows[i]["station"]) + "' and p_no='" + Convert.ToString(dt.Rows[i]["p_no"]) + "' and part_name='" + Convert.ToString(dt.Rows[i]["part_name"]) + "' and rwk_no='" + Convert.ToString(dt.Rows[i]["rwk_no"]) + "' and sent_or_rec='Sent' order by date_of_trans asc";
                cmd.CommandText = s;
                DataTable dtPartHis = new DataTable();
                dtPartHis.Load(cmd.ExecuteReader());

                string first_location = dtPartHis.Rows[0][0].ToString();
                int pendingQty = 0, first_location_sent = 0, quant_others = 0;
                if (!first_location.Equals(location))
                {
                    s = "select quant_sent, isnull(isnull(quant_sent,0)- isnull(quant_rec,0),0) as PendingQty from location_info_GR where tool_no=" + Convert.ToString(dt.Rows[i]["tool_no"]) + " and station='" + Convert.ToString(dt.Rows[i]["station"]) + "' and p_no='" + Convert.ToString(dt.Rows[i]["p_no"]) + "' and part_name='" + Convert.ToString(dt.Rows[i]["part_name"]) + "' and location='" + first_location + "' and Rwk='" + Convert.ToString(dt.Rows[i]["rwk_no"]) + "'";
                    cmd.CommandText = s;
                    DataTable dtLocInfo = new DataTable();
                    dtLocInfo.Load(cmd.ExecuteReader());
                    if (dtLocInfo.Rows.Count > 0)
                    {
                        first_location_sent = Convert.ToInt32(dtLocInfo.Rows[0][0].ToString());
                        //finding quantity pending with first vendor
                        quant_others = first_location_sent - quantity1;
                        pendingQty = Convert.ToInt32(dtLocInfo.Rows[0]["PendingQty"].ToString());
                    }
                }

                string modsentdate = Convert.ToString(DateTime.Now);
                //NOW INSERTING INTO LOCATION_INFO TABLE
                if (command.Equals("insert"))
                {
                    s = "insert into location_info_GR values(" + Convert.ToString(dt.Rows[i]["tool_no"]) + ",'" + Convert.ToString(dt.Rows[i]["station"]) + "','" + Convert.ToString(dt.Rows[i]["p_no"]) + "','" + Convert.ToString(dt.Rows[i]["part_name"]) + "'," + req_qty + ",'" + location + "'," + quantity + "," + 0 + "," + quantity + "," + quant_others + ",'" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss") + "','" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss") + "','" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss") + "','" + txtdelcdate.Text + "','" + challanNumber + "','','','','','','',''," + quantity + "," + Convert.ToInt32(dt.Rows[i]["Upload_Qty"]) + ",'" + Convert.ToDateTime(modsentdate).ToString("yyyy/MM/dd HH:mm:ss") + "','" + Convert.ToString(dt.Rows[i]["rwk_no"]) + "'," + 0 + ")";
                    cmd.CommandText = s;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    if (pendingQty == 0)
                    {
                        s = "update location_info_GR set  TransSentQty=" + quantity + ", quant_sent=" + quantity + " + isnull(quant_sent,0),quant_pend=isnull(quant_pend,0) +" + quantity + ",quant_others=" + quant_others + ",Delivery_commit_date='" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss") + "' where tool_no=" + Convert.ToString(dt.Rows[i]["tool_no"]) + " and station='" + Convert.ToString(dt.Rows[i]["station"]) + "' and p_no='" + Convert.ToString(dt.Rows[i]["p_no"]) + "' and part_name='" + Convert.ToString(dt.Rows[i]["part_name"]) + "' and location='" + location + "' and Rwk='" + Convert.ToString(dt.Rows[i]["Rwk_No"]) + "'";
                    }
                    else
                    {
                        s = "update location_info_GR set  TransSentQty=" + quantity + ", quant_sent=" + quantity + " + isnull(quant_sent,0),quant_pend=isnull(quant_pend,0) +" + quantity + ",quant_others=" + quant_others + " where tool_no=" + Convert.ToString(dt.Rows[i]["tool_no"]) + " and station='" + Convert.ToString(dt.Rows[i]["station"]) + "' and p_no='" + Convert.ToString(dt.Rows[i]["p_no"]) + "' and part_name='" + Convert.ToString(dt.Rows[i]["part_name"]) + "' and location='" + location + "' and Rwk='" + Convert.ToString(dt.Rows[i]["Rwk_No"]) + "'";
                    }
                    cmd.CommandText = s;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
            trans.Rollback();
            ErrorLogger.LogError("Receive_GR_Item", "SendChildPartstoWeldPartsConsumed", ex.Message, ex.StackTrace);
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured while processing your request, Please contact administrator.');", true);
        }
    }

    /// <summary>
    /// Method is called when user Clicks on Receive Button, Data is updated for Location Info Gr table, New Row is inserted in Part History Gr table as Received.
    /// Data at Company Location is updated for Part History Gr table, New row is inserted in Challan Sent Recevied Detail and Outsanding quantity is updated for Sent Row in the same table
    /// </summary>
    /// <param name="conn"></param>
    /// <param name="trans"></param>
    /// <param name="p_no"></param>
    /// <param name="part_name"></param>
    /// <param name="rwkNo"></param>
    /// <param name="tool_no"></param>
    /// <param name="station"></param>
    /// <param name="location"></param>
    /// <param name="uploadqty1"></param>
    /// <param name="extraReceivingQty"></param>
    /// <param name="str_dt"></param>
    /// <param name="newQuantityRec"></param>
    /// <param name="challanNumber"></param>
    /// <param name="sendChallanSelectedValue"></param>
    protected void UpdateLocationInfoGRData(SqlConnection conn, SqlTransaction trans, string p_no, string part_name, string rwkNo, int tool_no, string station, string location, int uploadqty1, string extraReceivingQty, string str_dt, int newQuantityRec, string challanNumber, string sendChallanSelectedValue)
    {
        try
        {
            string[] sendChallanSplit = Regex.Split(sendChallanSelectedValue, " - ");

            //cmd= new SqlCommand("select 

            string queryPartHisGr = "insert into part_history_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,TransSentQty,Upload_Qty,Challan_No,rwk_no,Is_Virtual_Transaction,Reference_Challan_No,User_name) values (@p_no,@part_name,@tool_no,@station,@location,@date_of_trans,@sent_or_rec,@quantity,@TransSentQty,@Upload_Qty,@Challan_No,@Rwk,@IsVirtualTransaction,@ReferenceChallanNo,@Username);SELECT SCOPE_IDENTITY()";
            cmd = new SqlCommand(queryPartHisGr, conn, trans);
            cmd.Parameters.AddWithValue("@p_no", p_no);
            cmd.Parameters.AddWithValue("@part_name", part_name);
            cmd.Parameters.AddWithValue("@tool_no", tool_no);
            cmd.Parameters.AddWithValue("@station", station);
            cmd.Parameters.AddWithValue("@location", location);
            cmd.Parameters.AddWithValue("@date_of_trans", str_dt);
            cmd.Parameters.AddWithValue("@sent_or_rec", "Received");
            cmd.Parameters.AddWithValue("@quantity", newQuantityRec);
            cmd.Parameters.AddWithValue("@TransSentQty", newQuantityRec);
            cmd.Parameters.AddWithValue("@Upload_Qty", uploadqty1);
            cmd.Parameters.AddWithValue("@Challan_No", challanNumber);
            cmd.Parameters.AddWithValue("@Rwk", rwkNo);
            cmd.Parameters.AddWithValue("@IsVirtualTransaction", checkIsVirtualTransaction.Checked);
            cmd.Parameters.AddWithValue("@ReferenceChallanNo", sendChallanSplit[0]);
            cmd.Parameters.AddWithValue("@Username", Convert.ToString(Session["UserId"]));
            cmd.ExecuteNonQuery();

            Session["Challano"] = txtchallanno.Text;
            Session["Location"] = location;

            string queryPartHisQCGR = "insert into part_history_QC_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,TransSentQty,Upload_Qty,Challan_No,User_name,Used_Date,rwk_no) values     (@p_no,@part_name,@tool_no,@station,@location,@date_of_trans,@sent_or_rec,@quantity,@TransSentQty,@Upload_Qty,@Challan_No,@user_name,@useddate,@rwkno);SELECT SCOPE_IDENTITY()";
            cmd = new SqlCommand(queryPartHisQCGR, conn, trans);
            cmd.Parameters.AddWithValue("@p_no", p_no);
            cmd.Parameters.AddWithValue("@part_name", part_name);
            cmd.Parameters.AddWithValue("@tool_no", tool_no);
            cmd.Parameters.AddWithValue("@station", station);
            cmd.Parameters.AddWithValue("@location", location);
            cmd.Parameters.AddWithValue("@date_of_trans", str_dt);
            cmd.Parameters.AddWithValue("@sent_or_rec", "Received");
            cmd.Parameters.AddWithValue("@quantity", newQuantityRec);
            cmd.Parameters.AddWithValue("@TransSentQty", newQuantityRec);
            cmd.Parameters.AddWithValue("@Upload_Qty", uploadqty1);
            cmd.Parameters.AddWithValue("@Challan_No", challanNumber);
            cmd.Parameters.AddWithValue("@user_name", approvername);
            cmd.Parameters.AddWithValue("@useddate", System.DateTime.Now);
            cmd.Parameters.AddWithValue("@rwkno", rwkNo);
            cmd.ExecuteNonQuery();

            //UPDATING LOCATION INFO TABLE
            string r_no = rwkNo;

            cmd.Parameters.Clear();
            string queryUpdatePartHis = "update part_history_GR set Received_Date='" + str_dt + "' , quantity=isnull(quantity,0) + " + newQuantityRec + ", QtySent=isnull(QtySent,0) - " + newQuantityRec + "  where p_no='" + p_no + "' and part_name='" + part_name + "' and location='COMPANY' and station='" + station + "' and rwk_no='" + rwkNo + "'  and tool_no=" + tool_no + "";
            cmd = new SqlCommand(queryUpdatePartHis, conn, trans);
            cmd.ExecuteNonQuery();

            //1)UPDATING TOTAL QUANTITY RECEIVED,PENDING QUANTITY
            string queryUpdatePartHisQty = "select isnull(quant_rec,0) from part_history_GR where tool_no=@tool_no and station=@station and p_no=@p_no and  part_name=@part_name and location=@location and rwk_no=@rwkno";
            cmd = new SqlCommand(queryUpdatePartHisQty, conn, trans);
            cmd.Parameters.AddWithValue("@p_no", p_no);
            cmd.Parameters.AddWithValue("@part_name", part_name);
            cmd.Parameters.AddWithValue("@tool_no", tool_no);
            cmd.Parameters.AddWithValue("@station", station);
            cmd.Parameters.AddWithValue("@location", location);
            cmd.Parameters.AddWithValue("@rwkno", rwkNo);

            quant_rec += Convert.ToInt32(cmd.ExecuteScalar());
            quant_pend = quant_sent - quant_rec;

            //Updating Location Info Gr table
            int extraQuantity = 0;
            if (!string.IsNullOrEmpty(extraReceivingQty))
            {
                extraQuantity = Convert.ToInt32(extraReceivingQty);
            }
            string queryUpdateLoc = "update location_info_GR set  quant_rec=" + newQuantityRec + " + isnull(quant_rec,0),  quant_pend=isnull(quant_pend,0)-" + newQuantityRec + ", Extra_Received_Quantity=isnull(Extra_Received_Quantity,0)+" + extraQuantity + ", Received_Date='" + str_dt + "' where p_no='" + p_no + "' and station='" + station + "'  and location='" + location + "' and part_name='" + part_name + "' and tool_no=" + tool_no + " and rwk='" + rwkNo + "' ";
            cmd = new SqlCommand(queryUpdateLoc, conn, trans);
            cmd.ExecuteNonQuery();

            //Insert into Challan Sent Received Detail table
            string queryChallanSentRecInsert = "insert into Challan_Sent_Received_Detail (Challan_No,Tool_No,Part_Name,P_No,Station,Rwk_No,Location,Sent_Or_Rec,Quantity,Outstanding_Qty,Reference_Challan_No,Created_Date,Created_By,Send_Receive_Date) Values(@ChallanNo,@ToolNo,@PartName,@PNo,@Station,@RwkNo,@Location,@SentOrRec,@Quantity,@OutstandingQty,@ReferenceChallanNo,@CreatedDate,@CreatedBy,@SendReceiveDate)";
            cmd = new SqlCommand(queryChallanSentRecInsert, conn, trans);
            cmd.Parameters.AddWithValue("@ChallanNo", challanNumber);
            cmd.Parameters.AddWithValue("@ToolNo", tool_no);
            cmd.Parameters.AddWithValue("@PartName", part_name);
            cmd.Parameters.AddWithValue("@PNo", p_no);
            cmd.Parameters.AddWithValue("@Station", station);
            cmd.Parameters.AddWithValue("@RwkNo", rwkNo);
            cmd.Parameters.AddWithValue("@Location", location);
            cmd.Parameters.AddWithValue("@SentOrRec", "Received");
            cmd.Parameters.AddWithValue("@Quantity", newQuantityRec);
            cmd.Parameters.AddWithValue("@OutstandingQty", 0);
            cmd.Parameters.AddWithValue("@ReferenceChallanNo", sendChallanSplit[0]);
            cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
            cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
            cmd.Parameters.AddWithValue("@SendReceiveDate", Convert.ToDateTime(str_dt).ToString("yyyy/MM/dd HH:mm:ss"));
            cmd.ExecuteNonQuery();

            cmd.Parameters.Clear();
            int totoalOutStandingQty = 0;
            string queryOutStandingQty = "select * from Challan_Sent_Received_Detail where Challan_No=@ChallanNo and Tool_No=@ToolNo and Part_Name=@PartName and P_No=@PNo and Station=@Station and Rwk_No=@RwkNo and Location=@Location and Sent_Or_Rec=@SentOrRec ";
            cmd = new SqlCommand(queryOutStandingQty, conn, trans);
            cmd.Parameters.AddWithValue("@ToolNo", tool_no);
            cmd.Parameters.AddWithValue("@PartName", part_name);
            cmd.Parameters.AddWithValue("@PNo", p_no);
            cmd.Parameters.AddWithValue("@Station", station);
            cmd.Parameters.AddWithValue("@RwkNo", rwkNo);
            cmd.Parameters.AddWithValue("@Location", location);
            cmd.Parameters.AddWithValue("@ChallanNo", sendChallanSplit[0]);
            cmd.Parameters.AddWithValue("@SentOrRec", "Sent");
            string challanSentReceivedDetailId = "";
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                if (rdr.HasRows)
                {
                    rdr.Read();
                    challanSentReceivedDetailId = Convert.ToString(rdr["Challan_Sent_Receive_Detail_Id"]);
                    totoalOutStandingQty = Convert.ToInt32(rdr["Outstanding_Qty"]);
                }
            }
            if (totoalOutStandingQty > 0)
            {
                if (!string.IsNullOrEmpty(challanSentReceivedDetailId))
                {
                    int pendingQty = totoalOutStandingQty - newQuantityRec;
                    string queryChallanSentRecUpdate = "update Challan_Sent_Received_Detail set Outstanding_Qty=@OutstandingQty,Updated_Date=@UpdatedDate,Updated_By=@UpdatedBy where Challan_Sent_Receive_Detail_Id=@ChallanSentReceivedDetailId";
                    cmd = new SqlCommand(queryChallanSentRecUpdate, conn, trans);
                    cmd.Parameters.AddWithValue("@OutstandingQty", pendingQty);
                    cmd.Parameters.AddWithValue("@ChallanSentReceivedDetailId", challanSentReceivedDetailId);
                    cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@UpdatedBy", Convert.ToString(Session["UserID"]));
                    cmd.ExecuteNonQuery();
                }
            }

            cmd.Parameters.Clear();
            string queryDeliveyCommitDateExistCheck = "select Delivery_commit_date,id,Explain_Date from part_history_gr where Challan_No=@ChallanNo and Tool_No=@ToolNo and Part_Name=@PartName and P_No=@PNo and Station=@Station and Rwk_No=@RwkNo and Location=@Location and Sent_Or_Rec=@SentOrRec ";
            cmd = new SqlCommand(queryDeliveyCommitDateExistCheck, conn, trans);
            cmd.Parameters.AddWithValue("@ToolNo", tool_no);
            cmd.Parameters.AddWithValue("@PartName", part_name);
            cmd.Parameters.AddWithValue("@PNo", p_no);
            cmd.Parameters.AddWithValue("@Station", station);
            cmd.Parameters.AddWithValue("@RwkNo", rwkNo);
            cmd.Parameters.AddWithValue("@Location", location);
            cmd.Parameters.AddWithValue("@ChallanNo", sendChallanSplit[0]);
            cmd.Parameters.AddWithValue("@SentOrRec", "Sent");
            string partHistoryGrId = "";
            string expectedDateOfCommitment = "";
            bool isUnderDeliveryCommit3DaysCycle = false;
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                if (rdr.HasRows)
                {
                    rdr.Read();
                    if (rdr["Delivery_commit_date"] == DBNull.Value)
                    {
                        partHistoryGrId = Convert.ToString(rdr["id"]);
                    }
                    else if (Convert.ToDateTime(rdr["Delivery_commit_date"]) == DateTime.MinValue)
                    {
                        partHistoryGrId = Convert.ToString(rdr["id"]);
                    }
                    if (!string.IsNullOrEmpty(partHistoryGrId))
                    {
                        if (rdr["Explain_Date"] != DBNull.Value)
                        {
                            if (Convert.ToDateTime(rdr["Explain_Date"]) != DateTime.MinValue)
                            {
                                expectedDateOfCommitment = Convert.ToString(rdr["Explain_Date"]);
                                string currentDate = DateTime.Now.ToShortDateString();
                                string expectedCommitDate = Convert.ToDateTime(expectedDateOfCommitment).ToShortDateString();
                                int days = Convert.ToDateTime(currentDate).Subtract(Convert.ToDateTime(expectedCommitDate)).Days;
                                if (days <= 0)
                                {
                                    isUnderDeliveryCommit3DaysCycle = true;
                                }
                            }
                        }
                    }
                }
            }

            if (isUnderDeliveryCommit3DaysCycle)
            {
                cmd.Parameters.Clear();
                string queryUpdateCommitDate = "update part_history_GR set Delivery_commit_date=@Date, Delivery_Commit_Updated_Date=@Date where id=@PartsGrId";
                cmd = new SqlCommand(queryUpdateCommitDate, conn, trans);
                cmd.Parameters.AddWithValue("@Date", DateTime.Now);
                cmd.Parameters.AddWithValue("@PartsGrId", partHistoryGrId);
                cmd.ExecuteNonQuery();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
            trans.Rollback();
            ErrorLogger.LogError("Receive_GR_Item", "UpdateLocationinfoGRData", ex.Message, ex.StackTrace);
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured while processing your request, Please contact administrator.');", true);
        }
    }

    protected void ddllocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddllocation.SelectedItem.Text.Length > 0)
        {
            txtchallanno.Visible = true;
            lblChallanno.Visible = true;
            btnprint.Visible = true;
            btnSendThisChallan.Visible = true;
        }
    }

    /// <summary>
    /// Receive Challan is printed when user clicks on Print Button
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnprint_Click(object sender, EventArgs e)
    {
        Session["MIN"] = txtchallanno.Text;
        if (txtchallanno.Text.Length > 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "window.open('Item_Challan.aspx?ProcessType=Receive');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Receive some part before Print');", true);
            return;
        }
    }

    protected void ddllocauto_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Receiving quantity is Validated when user enters the Receiving qunatity
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TextBox2_TextChanged(object sender, EventArgs e)
    {

        TextBox txtBoxNewQty = (TextBox)sender;
        GridViewRow currentRow = (GridViewRow)txtBoxNewQty.NamingContainer;
        DropDownList ddlChallanNumbers = (DropDownList)txtBoxNewQty.FindControl("ddlChallanNumbers");
        HiddenField hdnPartsGrId = (HiddenField)txtBoxNewQty.FindControl("hdnPartsGrId");
        if (txtBoxNewQty.Text != "" && ddlChallanNumbers.SelectedValue != "")
        {
            string[] splitValues = Regex.Split(ddlChallanNumbers.SelectedValue, " - ");
            TextBox txtNewReceivingQty = ddlChallanNumbers.FindControl("TextBox2") as TextBox;
            Label lblTotalQtySent = ddlChallanNumbers.FindControl("Label5") as Label;
            Label lblReceivedQty = ddlChallanNumbers.FindControl("Label6") as Label;

            Label lblStation = ddlChallanNumbers.FindControl("Label2") as Label;
            Label lblrew = ddlChallanNumbers.FindControl("lblrew") as Label;
            Label lblToolNo = ddlChallanNumbers.FindControl("Label1") as Label;
            LinkButton lnkbtnPNo = ddlChallanNumbers.FindControl("Label3") as LinkButton;
            Label lblPartName = ddlChallanNumbers.FindControl("Label4") as Label;
            Label lblOutStandingQtyAtVendor = ddlChallanNumbers.FindControl("Label6sentqty") as Label;

            // On Text Change display the possible and available qty, Ex. User entered 6 and avalable is 3 and Challan Qty is 4
            int? availableAndValidQty = ValidateQuantities(lblToolNo.Text, lblStation.Text, lblPartName.Text, lnkbtnPNo.Text, lblrew.Text, ddllocation.SelectedItem.Text, Convert.ToString(splitValues[1]), currentRow);
            if (availableAndValidQty != null && availableAndValidQty > 0)
            {
                if (ddllocation.SelectedItem.Text == "Weld Assembly Waiting")
                {
                    int? validParentQtyAfterRatio = ValidateRationWiseQuantity(hdnPartsGrId.Value, Convert.ToInt32(availableAndValidQty.Value));
                    txtNewReceivingQty.Text = Convert.ToString(validParentQtyAfterRatio.Value);
                }
                else
                {
                    if (Convert.ToInt32(txtNewReceivingQty.Text) > availableAndValidQty.Value)
                    {
                        txtNewReceivingQty.Text = Convert.ToString(availableAndValidQty.Value);
                    }
                }
            }
            else
            {
                txtNewReceivingQty.Text = String.Empty;
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Challan Number/New Receiving Quantity is required.');", true);
        }
    }

    /// <summary>
    /// If user enters Past/Future Dates as Receiving Date then Error message is thrown
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtReceivecdate_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDateTime(txtReceivecdate.Text) > DateTime.Now || Convert.ToDateTime(txtReceivecdate.Text) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Can not take future/past date');", true);
            txtReceivecdate.Text = "";
        }
    }

    /// <summary>
    /// If user enters Past Dates as Expected Delivery Date then Error message is thrown
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtdelcdate_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtdelcdate.Text))
        {
            if (Convert.ToDateTime(txtdelcdate.Text) < DateTime.Now.AddDays(-1))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Can not take past dates');", true);
                txtdelcdate.Text = "";
            }
        }
    }

   /// <summary>
   /// Position history is shown when user clicks on  any Position
   /// </summary>
   /// <param name="sender"></param>
   /// <param name="e"></param>
    protected void Label3_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";

        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        LoadPositionHistoryWindow(Positionno, toolno, station, part_name, rwk_no, "");
    }


    /// <summary>
    /// This method is called to bind the Postion History and display it to user on postion click
    /// </summary>
    /// <param name="Positionno"></param>
    /// <param name="toolno"></param>
    /// <param name="station"></param>
    /// <param name="part_name"></param>
    /// <param name="rwk_no"></param>
    /// <param name="positionType"></param>
    protected void LoadPositionHistoryWindow(string Positionno, string toolno, string station, string part_name, string rwk_no, string positionType)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            conn.Open();
            string query = "";
            if (positionType.ToLower() == "child")
                query = "select  id,location as Party,date_of_trans as Trans_Date,sent_or_rec as Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as Pending_Qty_PPC,TransSentQty as Send_Or_Rec_Qty,User_Name  from part_history_GR  where   p_no='" + Positionno + "' and  tool_no='" + toolno + "' and  station='" + station + "' and rwk_no='" + rwk_no + "' order by date_of_trans asc ";
            else
                query = "select  id,location as Party,date_of_trans as Trans_Date,sent_or_rec as Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as Pending_Qty_PPC,TransSentQty as Send_Or_Rec_Qty,User_Name  from part_history_GR  where   p_no='" + Positionno + "' and  tool_no='" + toolno + "' and  station='" + station + "' and  part_name='" + part_name + "' and rwk_no='" + rwk_no + "' order by date_of_trans asc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();

            da.Fill(table);

            StringBuilder b = new StringBuilder();

            b.Append("<table style='");
            b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<th>");
                b.Append(column.ColumnName);
                b.Append("</th>");
            }
            b.Append("</tr>");


            foreach (DataRow row in table.Rows)
            {
                b.Append("<tr>");
                foreach (DataColumn column in table.Columns)
                {
                    b.Append("<td>");
                    b.Append(row[column.ColumnName]);
                    b.Append("</td>");
                }
                b.Append("</tr>");
            }
            b.Append("</table>");

            pnlHistoryPopUp.InnerHtml = b.ToString();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    /// <summary>
    /// Expected Delivery Commit Date Change History is shown to User when he clicks on Expected Delivery Date link button
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkBtnLastCommitmentDate_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        try
        {
            string id = "";
            SqlCommand cmd = new SqlCommand("select top 1 id from part_history_Gr where p_no='" + Positionno + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk_no='" + rwk_no + "' and tool_no='" + toolno + "' and location='" + ddllocation.SelectedItem.Text + "'  and sent_or_rec='Sent' order by date_of_trans desc", conn);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    reader.Read();
                    id = Convert.ToString(reader["id"]);
                }
            }

            string query = "select Tool_No,P_No,Part_Name,Station,Rework_No,Location,Commitment_Date as Expected_Delivery_Date  from CommitmentHistory  where  Part_History_Id=" + id + " union select  Tool_No,P_No,Part_Name,Station,Rwk_No,Location,Delivery_commit_date  as Expected_Delivery_Date  from part_history_Gr  where  id=" + id + " order by Commitment_Date desc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();
            da.Fill(table);

            StringBuilder b = new StringBuilder();

            b.Append("<table style='");
            b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<th>");
                b.Append(column.ColumnName);
                b.Append("</th>");
            }
            b.Append("</tr>");

            foreach (DataRow row in table.Rows)
            {
                b.Append("<tr>");
                foreach (DataColumn column in table.Columns)
                {
                    b.Append("<td>");
                    b.Append(row[column.ColumnName]);
                    b.Append("</td>");
                }
                b.Append("</tr>");
            }
            b.Append("</table>");

            pnlCommitmentHistory.InnerHtml = b.ToString();
            dialogCommitmentHistory.Attributes.Add("style", "display:block;");
            overlayCommitmentHistory.Attributes.Add("style", "display:block;");
        }
        catch (Exception ex)
        {
            ex.ToString();
            ErrorLogger.LogError("Receive_GR_Item", "lnkBtnLastCommitmentDate", ex.Message, ex.StackTrace);
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured. Please Contact Administrator.');", true);
            return;
        }
        finally
        {
            conn.Close();
        }
    }

    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }

    protected void btnCloseCommitmentHistory_Click(object sender, EventArgs e)
    {
        dialogCommitmentHistory.Attributes.Add("style", "display:none;");
        overlayCommitmentHistory.Attributes.Add("style", "display:none;");
    }

    /// <summary>
    /// Method is called when user presses Update Expected Delivery Date button. It is validated how many times the date has already changed and what date user is allowed to enter
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnUpdateCommitmentDate_Click(object sender, EventArgs e)
    {
        if (txtdelcdate.Text != "")
        {
            int count = 0;
            bool isUpdateInProgress = false;
            string sqlconnstring1 = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring1);
            SqlTransaction trans = null;
            try
            {
                conn.Open();
                trans = conn.BeginTransaction();

                if (!isUpdateInProgress)
                {
                    isUpdateInProgress = true;
                    foreach (GridViewRow row in GridView1.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            DateTime commitmentDate = Convert.ToDateTime(txtdelcdate.Text);
                            str_dt = string.Format("{0:yyyy-MM-dd}", commitmentDate); // you can specify format 
                            Label station1 = row.FindControl("Label2") as Label;
                            Label rework_no = row.FindControl("lblrew") as Label;
                            Label lblToolNo = row.FindControl("Label1") as Label;
                            LinkButton POSITION = row.FindControl("Label3") as LinkButton;
                            Label part_name1 = row.FindControl("Label4") as Label;
                            CultureInfo provider = CultureInfo.InvariantCulture;
                            Label quantity = row.FindControl("Label8") as Label;
                            Label uploadqty = row.FindControl("Label6Upload_Qty") as Label;
                            Label sent = row.FindControl("Labelsent") as Label;
                            LinkButton lnkBtnLastCommitmentDate = row.FindControl("lnkBtnLastCommitmentDate") as LinkButton;
                            DropDownList ddlChallanNumbers = (DropDownList)row.FindControl("ddlChallanNumbers");

                            tool_no = Convert.ToInt32(lblToolNo.Text);
                            p_no = POSITION.Text;
                            station = station1.Text;
                            part_name = Convert.ToString(part_name1.Text);
                            location = ddllocation.SelectedItem.Text;
                            string kk = System.DateTime.Now.ToLongTimeString();
                            str_dt = str_dt + " " + kk;

                            bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                            if (isSelected)
                            {
                                if (ddlChallanNumbers.SelectedValue != "")
                                {
                                    if (ddllocation.SelectedItem.Text.Length > 0)
                                    {
                                        if (isSelected == true)
                                        {
                                            string query = "";
                                            string id = "";
                                            string r_no = rework_no.Text;
                                            string challanNo = "", firstDeliveryCommitDate = "",required_by_date="";
                                            string[] challanNoSplit = ddlChallanNumbers.SelectedValue.Split('-');
                                            if (challanNoSplit.Length >= 1)
                                                challanNo = challanNoSplit[0].TrimEnd();
                                            cmd = new SqlCommand("select top 1 id, delivery_commit_date,Required_date from part_history_Gr where p_no='" + p_no + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk_no='" + r_no + "' and tool_no='" + tool_no + "' and location='" + ddllocation.SelectedItem.Text + "'  and sent_or_rec='Sent' and Challan_No='" + challanNo + "' order by date_of_trans desc", conn, trans);
                                            using (SqlDataReader reader = cmd.ExecuteReader())
                                            {
                                                if (reader.HasRows)
                                                {
                                                    reader.Read();
                                                    id = Convert.ToString(reader["id"]);
                                                    firstDeliveryCommitDate = Convert.ToString(reader["delivery_commit_date"]);
                                                    required_by_date=Convert.ToString(reader["Required_date"]);
                                                }
                                            }

                                            if (lnkBtnLastCommitmentDate.Text != "" && firstDeliveryCommitDate != ""  && lnkBtnLastCommitmentDate.Text != "01-01-1900")//code changed by himaneesh text on link button should not be 01-01-1900 as this signfies first delivery commitment date is not updated
                                            {
                                                int revisionNo = 0;
                                                cmd.Parameters.Clear();
                                                query = "select Count(CommitmentHistoryId) as RevisionCount from CommitmentHistory where Part_History_Id=@PartHistoryGrId";
                                                cmd = new SqlCommand(query, conn, trans);
                                                cmd.Parameters.AddWithValue("@PartHistoryGrId", id);
                                                using (SqlDataReader rdr = cmd.ExecuteReader())
                                                {
                                                    if (rdr.HasRows)
                                                    {
                                                        rdr.Read();
                                                        if (rdr["RevisionCount"] != DBNull.Value)
                                                            revisionNo = Convert.ToInt32(rdr["RevisionCount"]);//code changed by himaneesh
                                                        else
                                                            revisionNo = 1;
                                                    }
                                                }

                                                if (revisionNo == 1)
                                                {
                                                    int daysDiff = Convert.ToDateTime(str_dt).Subtract(Convert.ToDateTime(firstDeliveryCommitDate)).Days;
                                                    if (daysDiff > 7)
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Date Entered. For First Revision only 1 Week Extension Allowed.');", true);
                                                        break;
                                                    }
                                                }
                                                else if (revisionNo == 2)
                                                {
                                                    cmd.Parameters.Clear();
                                                    query = "select top 1 Commitment_Date from CommitmentHistory where Part_History_Id=@PartHistoryGrId order by CommitmentHistoryId Desc";
                                                    cmd = new SqlCommand(query, conn, trans);
                                                    cmd.Parameters.AddWithValue("@PartHistoryGrId", id);
                                                    using (SqlDataReader rdr = cmd.ExecuteReader())
                                                    {
                                                        if (rdr.HasRows)
                                                        {
                                                            rdr.Read();
                                                            firstDeliveryCommitDate = Convert.ToString(rdr["Commitment_Date"]);
                                                        }
                                                    }


                                                    int daysDiff = Convert.ToDateTime(str_dt).Subtract(Convert.ToDateTime(firstDeliveryCommitDate)).Days;
                                                    if (daysDiff > 3)
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Date Entered. For Second Revision only 3 Days Extension Allowed.');", true);
                                                        break;
                                                    }
                                                }
                                                int val = Convert.ToDateTime(str_dt).Subtract(Convert.ToDateTime(lnkBtnLastCommitmentDate.Text)).Days;
                                                if (val <= 0)
                                                {
                                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Date Entered. Please select a Valid Date.');", true);
                                                    break;
                                                }

                                                if (revisionNo < 3)
                                                {
                                                    if(required_by_date!="" || required_by_date!="01-01-1900")//so if required by date is not empty or is not equal to the default date i.e., 01-01-1900 so checking if delivery commitment date is valid
                                                    {
                                                        if(Convert.ToDateTime(txtdelcdate.Text) > Convert.ToDateTime(required_by_date))
                                                        {
                                                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('delivery commitment date should never be greater than required by date which is "+required_by_date+ "');", true);
                                                            break;
                                                        }
                                                    }
                                                    s = "insert into CommitmentHistory(part_history_id,p_no,part_name,tool_no,station,location,commitment_date,created_date,created_by,rework_no) values(@part_history_id,@p_no,@part_name,@tool_no,@station,@location,@commitment_date,@created_date,@created_by,@Rwk);";
                                                    cmd = new SqlCommand(s, conn, trans);
                                                    cmd.Parameters.AddWithValue("@part_history_id", id);
                                                    cmd.Parameters.AddWithValue("@p_no", p_no);
                                                    cmd.Parameters.AddWithValue("@part_name", part_name);
                                                    cmd.Parameters.AddWithValue("@tool_no", tool_no);
                                                    cmd.Parameters.AddWithValue("@station", station);
                                                    cmd.Parameters.AddWithValue("@location", location);
                                                    cmd.Parameters.AddWithValue("@commitment_date", str_dt);
                                                    cmd.Parameters.AddWithValue("@created_by", Session["UserID"]);
                                                    cmd.Parameters.AddWithValue("@created_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                                    cmd.Parameters.AddWithValue("@Rwk", rework_no.Text);
                                                    cmd.ExecuteNonQuery();
                                                    string qyeryPartHistoryInfo = "update part_history_gr set Delivery_commit_date='" + str_dt + "' where p_no='" + p_no + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk_no='" + r_no + "' and tool_no='" + tool_no + "' and location='" + ddllocation.SelectedItem.Text + "' and sent_or_rec='Sent' and date_of_trans=(select max(date_of_trans) from part_history_gr where p_no='" + p_no + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk_no='" + r_no + "' and tool_no='" + tool_no + "' and location='" + ddllocation.SelectedItem.Text + "' and sent_or_rec='Sent' )";
                                                    string queryLocationInfo = "update location_info_Gr set Delivery_commit_date='" + str_dt + "'  where p_no='" + p_no + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk='" + r_no + "' and tool_no='" + tool_no + "' and location='" + ddllocation.SelectedItem.Text + "'";
                                                    SqlCommand cmdLI = new SqlCommand(queryLocationInfo, conn, trans);
                                                    cmdLI.ExecuteNonQuery();
                                                    count = count + 1;
                                                    SqlCommand cmdPHG = new SqlCommand(qyeryPartHistoryInfo, conn, trans);
                                                    cmdPHG.ExecuteNonQuery();
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('You are allowed to update Expected Delivery Commit Date 2 times only. After second revison you can not update the date.');", true);
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('You can not update First Delivery Commit Date from here. To update first Commit Date , visit Update Delivery Commit Date By Vendor Screen.');", true);
                                                return;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Vendor');", true);
                                        return;
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Challan Number for which you want to Update Delivery Commit Date.');", true);
                                    return;
                                }
                            }

                        }
                    }
                    trans.Commit();
                }
                isUpdateInProgress = false;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                isUpdateInProgress = false;
                ErrorLogger.LogError("Receive_GR_Item", "btnUpdateCommitmentDate_Click", ex.Message, ex.StackTrace);
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
                ex.ToString();
            }
            finally
            {
                conn.Close();
            }

            if (count > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Expected Delivery Date Updated Successfully');", true);
                btnRecieved.Visible = false;
                showgrid();
            }
            conn.Close();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please enter Date of Commitment First.');", true);
        }
    }

    /// <summary>
    /// Vendor Visit Report Data is Exported into Excel
    /// </summary>
    /// <param name="query"></param>
    protected void ExportDataToExcel(string query)
    {
        OfficeExcel.Workbook workBook;
        //Excel.Sheets workSheets;
        OfficeExcel.Worksheet wSheet;
        OfficeExcel.Application excel;
        string fileName = "VisitReport_" + DateTime.Now.Day + "" + DateTime.Now.Month + "" + DateTime.Now.Year + "_" + DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
        string connectionString = "Provider=Microsoft.Jet.OleDb.4.0; Data Source=" + fileName + "; Extended Properties=Excel 8.0;";

        excel = new Microsoft.Office.Interop.Excel.Application();
        workBook = excel.Workbooks.Add(Type.Missing);
        wSheet = (OfficeExcel.Worksheet)workBook.ActiveSheet;
        OfficeExcel.Range cellRange;

        wSheet.Name = "VisitReport";
        wSheet.Cells[1, 1] = "Vendor Name";
        wSheet.Cells[1, 2] = ddllocation.SelectedItem.Text;

        wSheet.Cells[1, 4] = "Date";
        wSheet.Cells[1, 5] = DateTime.Now;

        wSheet.Cells[1, 7] = "Filled By";
        wSheet.Cells[1, 8] = "";

        wSheet.Cells[3, 1] = "Tool No";
        wSheet.Cells[3, 2] = "Station No";
        wSheet.Cells[3, 3] = "Position No";
        wSheet.Cells[3, 4] = "Part No";
        wSheet.Cells[3, 5] = "Rework No";
        wSheet.Cells[3, 6] = "Qty";
        wSheet.Cells[3, 7] = "Date of Transfer";
        wSheet.Cells[3, 8] = "Original Date";
        wSheet.Cells[3, 9] = "Latest Commitment Date";
        wSheet.Cells[3, 10] = "Operation 1";
        wSheet.Cells[3, 11] = "Operation 2";
        wSheet.Cells[3, 12] = "Operation 3";
        wSheet.Cells[3, 13] = "Operation 4";
        wSheet.Cells[3, 14] = "Operation 5";
        wSheet.Cells[3, 15] = "Remarks";
        excel.Visible = false;
        excel.DisplayAlerts = false;
        excel.PromptForSummaryInfo = false;
        excel.UserControl = false;

        wSheet.Rows.Font.Size = 12;
        wSheet.Rows.Font.Bold = true;
        wSheet.Rows.RowHeight = 32;

        //setting the border in the third row
        cellRange = wSheet.Range[wSheet.Cells[3, 1], wSheet.Cells[3, 15]];
        //cellRange.EntireColumn.AutoFit();
        Microsoft.Office.Interop.Excel.Borders border = cellRange.Borders;
        border[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
        border[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
        border[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
        border[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
        border.Color = Color.Black;
        border.Weight = 2d;

        Microsoft.Office.Interop.Excel.Range range = wSheet.UsedRange;
        int colCount = range.Columns.Count;
        int rowCount = range.Rows.Count;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        using (SqlConnection con = new SqlConnection(sqlconnstring))
        {
            SqlDataAdapter ad = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            ad.Fill(dt);

            int currentRow = rowCount + 1;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cellRange = wSheet.Range[wSheet.Cells[currentRow, 1], wSheet.Cells[currentRow, 15]];
                    cellRange.EntireColumn.AutoFit();
                    Microsoft.Office.Interop.Excel.Borders currentRowBorder = cellRange.Borders;
                    currentRowBorder[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    currentRowBorder[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    currentRowBorder[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    currentRowBorder[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    currentRowBorder.Color = Color.Black;
                    currentRowBorder.Weight = 2d;

                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        if (dt.Columns[j].ColumnName.ToLower() == "tool_no")
                            wSheet.Cells[currentRow, 1] = Convert.ToString(dt.Rows[i]["Tool_No"]);
                        if (dt.Columns[j].ColumnName.ToLower() == "station")
                            wSheet.Cells[currentRow, 2] = Convert.ToString(dt.Rows[i]["Station"]);
                        if (dt.Columns[j].ColumnName.ToLower() == "p_no")
                            wSheet.Cells[currentRow, 3] = Convert.ToString(dt.Rows[i]["P_No"]);
                        if (dt.Columns[j].ColumnName.ToLower() == "part_name")
                            wSheet.Cells[currentRow, 4] = Convert.ToString(dt.Rows[i]["Part_Name"]);
                        if (dt.Columns[j].ColumnName.ToLower() == "rwk")
                            wSheet.Cells[currentRow, 5] = Convert.ToString(dt.Rows[i]["Rwk"]);
                        if (dt.Columns[j].ColumnName.ToLower() == "sentqty")
                            wSheet.Cells[currentRow, 6] = Convert.ToString(dt.Rows[i]["SentQty"]);
                        if (dt.Columns[j].ColumnName.ToLower() == "sent_received_date")
                            wSheet.Cells[currentRow, 7] = Convert.ToDateTime(dt.Rows[i]["Sent_Received_Date"]).ToString("dd-MM-yyyy");
                        if (dt.Columns[j].ColumnName.ToLower() == "sent_received_date")
                            wSheet.Cells[currentRow, 8] = Convert.ToDateTime(dt.Rows[i]["Sent_Received_Date"]).ToString("dd-MM-yyyy");
                        if (dt.Columns[j].ColumnName.ToLower() == "delivery_commit_date")
                            wSheet.Cells[currentRow, 9] = Convert.ToDateTime(dt.Rows[i]["Delivery_commit_date"]).ToString("dd-MM-yyyy");

                        wSheet.Cells[currentRow, 10] = "";
                        wSheet.Cells[currentRow, 11] = "";
                        wSheet.Cells[currentRow, 12] = "";
                        wSheet.Cells[currentRow, 13] = "";
                        wSheet.Cells[currentRow, 14] = "";
                        wSheet.Cells[currentRow, 15] = "";
                    }
                    currentRow = currentRow + 1;
                }
                string path = Server.MapPath("~/UploadedExcelFiles/" + fileName);

                workBook.SaveAs(path, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal,
                Missing.Value, Missing.Value, Missing.Value, Missing.Value, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive,
                Missing.Value, Missing.Value, Missing.Value,
                Missing.Value, Missing.Value);
                //workBook.Close(Missing.Value, Missing.Value, Missing.Value);
                //lbl.Text = "Data Downloaded and Saved Successfully.";

                excel.Visible = true;
                excel.DisplayAlerts = true;
                excel.PromptForSummaryInfo = true;
                excel.UserControl = true;

                //wSheet = null;
                //workBook = null;
                //excel.Quit();
                //GC.WaitForPendingFinalizers();
                //GC.Collect();
                //GC.WaitForPendingFinalizers();
                //GC.Collect();
            }
            else
            {
                //lblMessage.Text = "No Records Found";
            }
        }
    }

    /// <summary>
    /// Visit Report is generated when user clicks  on Generate Vendor Visit Report button and user is redirected to the Report Page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGenerateVisitReport_Click(object sender, EventArgs e)
    {
        SqlConnection connec = new SqlConnection();
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            if (Session["DataTableAdvanceReport"] != null)
            {
                btnGenerateVisitReport.Visible = false;
                connec.ConnectionString = sqlconnstring;
                SqlCommand cmd = new SqlCommand();
                connec.Open();
                cmd.Connection = connec;
                cmd.CommandText = "select top 1 Visit_Number from Vendor_Visit_Detail order by Cast(Visit_Number as decimal) desc";
                string visitNumber = "";
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        int currentNum = Convert.ToInt32(reader["Visit_Number"]) + 1;
                        visitNumber = Convert.ToString(currentNum);
                        if (currentNum.ToString().Length < 3)
                        {
                            while (visitNumber.Length < 3)
                                visitNumber = "0" + visitNumber;
                        }
                    }
                    else
                    {
                        visitNumber = "001";
                    }
                }

                cmd.Parameters.Clear();
                cmd.CommandText = "insert into Vendor_Visit_Detail (Vendor_Name,Visit_Number,Created_By,Created_Date) Values(@VendorName,@VisitNumber,@CreatedBy,@CreatedDate);Select Scope_Identity();";
                cmd.Parameters.AddWithValue("@VendorName", ddllocation.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@VisitNumber", visitNumber);
                cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                int ticketDetailId = Convert.ToInt32(cmd.ExecuteScalar());
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "window.open('PrintPendingItemsVendorVisitReport.aspx?TicketDetailId=" + ticketDetailId + "');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Send part before Print');", true);
                return;
            }
        }
        catch (Exception ex)
        {

            throw;
        }
    }

    protected void btnSendThisChallan_Click(object sender, EventArgs e)
    {
        if (txtchallanno.Text != "")
        {
            Response.Redirect("Sent_GR_Item.aspx?ChallanNo=" + txtchallanno.Text);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('No Challan to Send.');", true);
        }
    }

    /// <summary>
    /// Event is fired when user clicks on Add New Button shown in front of each row, to receive Items against Multiple Challans
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnkBtn = (LinkButton)sender;
            GridViewRow gridViewRow = (GridViewRow)lnkBtn.NamingContainer;
            GridView gridView = (GridView)gridViewRow.NamingContainer;
            int rowIndex = gridViewRow.RowIndex;
            TextBox txtNewReceivingQty = lnkBtn.FindControl("TextBox2") as TextBox;
            Label lblTotalQtySent = lnkBtn.FindControl("Label5") as Label;
            Label lblReceivedQty = lnkBtn.FindControl("Label6") as Label;
            DropDownList ddlChallanNumbers = lnkBtn.FindControl("ddlChallanNumbers") as DropDownList;
            Label lblStation = lnkBtn.FindControl("Label2") as Label;
            Label lblrew = lnkBtn.FindControl("lblrew") as Label;
            Label lblToolNo = lnkBtn.FindControl("Label1") as Label;
            LinkButton lnkbtnPNo = lnkBtn.FindControl("Label3") as LinkButton;
            Label lblPartName = lnkBtn.FindControl("Label4") as Label;
            Label lblOutStandingQtyAtVendor = lnkBtn.FindControl("Label6sentqty") as Label;
            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];

            for (int i = 0; i < dtCurrentTable.Rows.Count; i++)// - 1
            {
                //extract the TextBox values  
                CheckBox chkGridAllchild = GridView1.Rows[i].FindControl("chkAllchild") as CheckBox;
                Label lblGridTotalQtySent = GridView1.Rows[i].FindControl("Label5") as Label;
                Label lblGridReceivedQty = GridView1.Rows[i].FindControl("Label6") as Label;
                TextBox txtGridNewReceivingQty = GridView1.Rows[i].FindControl("TextBox2") as TextBox;
                TextBox txtGridExtraReceivingQty = GridView1.Rows[i].FindControl("txtExtraQuantity") as TextBox;
                Label lblGridStation = GridView1.Rows[i].FindControl("Label2") as Label;
                Label lblGridRwkNo = GridView1.Rows[i].FindControl("lblrew") as Label;

                Label lblGridToolNo = GridView1.Rows[i].FindControl("Label1") as Label;
                LinkButton lnkbtnGridPNo = GridView1.Rows[i].FindControl("Label3") as LinkButton;
                Label lblGridPartName = GridView1.Rows[i].FindControl("Label4") as Label;
                Label lblGridQuantity = GridView1.Rows[i].FindControl("Label8") as Label;
                Label lblGridUploadQty = GridView1.Rows[i].FindControl("Label6Upload_Qty") as Label;
                DropDownList ddlGridChallanNumbers = GridView1.Rows[i].FindControl("ddlChallanNumbers") as DropDownList;
                Label lblGridExtraReceivedQty = GridView1.Rows[i].FindControl("lblExtraReceivedQty") as Label;
                Label lblGridSentQty = GridView1.Rows[i].FindControl("Label6sentqty") as Label;

                HiddenField hdnGridPartsGrId = GridView1.Rows[i].FindControl("hdnPartsGrId") as HiddenField;
                HiddenField hdnGridLocation = GridView1.Rows[i].FindControl("hdnLocation") as HiddenField;
                HiddenField hdnGridSentReceivedDate = GridView1.Rows[i].FindControl("hdnSentReceivedDate") as HiddenField;
                HiddenField hdnGridReceivedDate = GridView1.Rows[i].FindControl("hdnReceivedDate") as HiddenField;
                HiddenField hdnGridDeliveryCommitDate = GridView1.Rows[i].FindControl("hdnDeliveryCommitDate") as HiddenField;

                //GRID
                dtCurrentTable.Rows[i]["Is_Selected"] = Convert.ToBoolean(chkGridAllchild.Checked);
                dtCurrentTable.Rows[i]["Parts_Gr_Id"] = Convert.ToString(hdnGridPartsGrId.Value);
                dtCurrentTable.Rows[i]["Location"] = Convert.ToString(hdnGridLocation.Value);
                dtCurrentTable.Rows[i]["Sent_Received_Date"] = Convert.ToString(hdnGridSentReceivedDate.Value);
                dtCurrentTable.Rows[i]["Received_Date"] = Convert.ToString(hdnGridReceivedDate.Value);
                dtCurrentTable.Rows[i]["Delivery_Commit_Date"] = Convert.ToString(hdnGridDeliveryCommitDate.Value);
                HiddenField hdnPendingRowsAtVendor = GridView1.Rows[i].FindControl("hdnPendingRowsAtVendor") as HiddenField;

                dtCurrentTable.Rows[i]["tool_no"] = Convert.ToString(lblGridToolNo.Text);
                dtCurrentTable.Rows[i]["station"] = Convert.ToString(lblGridStation.Text);
                dtCurrentTable.Rows[i]["p_no"] = Convert.ToString(lnkbtnGridPNo.Text);
                dtCurrentTable.Rows[i]["part_name"] = Convert.ToString(lblGridPartName.Text);
                dtCurrentTable.Rows[i]["quant_sent"] = Convert.ToString(lblGridTotalQtySent.Text);
                dtCurrentTable.Rows[i]["quant_rec"] = Convert.ToString(lblGridReceivedQty.Text);
                dtCurrentTable.Rows[i]["Challan_No"] = Convert.ToString(ddlGridChallanNumbers.SelectedValue);
                dtCurrentTable.Rows[i]["New_Rec_Quantity"] = Convert.ToString(txtGridNewReceivingQty.Text);
                dtCurrentTable.Rows[i]["Extra_Qty_Received"] = Convert.ToString(lblGridExtraReceivedQty.Text);
                dtCurrentTable.Rows[i]["Extra_Receiving_Qty"] = Convert.ToString(txtGridExtraReceivingQty.Text);
                dtCurrentTable.Rows[i]["Upload_Qty"] = Convert.ToString(lblGridUploadQty.Text);
                dtCurrentTable.Rows[i]["SentQty"] = Convert.ToString(lblGridSentQty.Text);
                dtCurrentTable.Rows[i]["Rwk"] = Convert.ToString(lblGridRwkNo.Text);
                dtCurrentTable.Rows[i]["TotalPendingQtyAtVendor"] = Convert.ToString(hdnPendingRowsAtVendor.Value);
            }

            if (!string.IsNullOrEmpty(txtNewReceivingQty.Text))
            {
                List<DataRow> dr = dtCurrentTable.Select("Tool_No=" + lblToolNo.Text + " and Station='" + lblStation.Text + "' and Part_Name='" + lblPartName.Text + "' and P_No='" + lnkbtnPNo.Text + "' and Rwk='" + lblrew.Text + "'").ToList();
                string totalRecQty = "0";
                foreach (DataRow dRow in dr)
                {
                    string values = dRow["New_Rec_Quantity"] as string;
                    if (values != null)
                    {
                        int totalQty = int.Parse(totalRecQty) + int.Parse(values);
                        totalRecQty = totalQty.ToString();
                    }
                }
                int pendingQty = int.Parse(lblOutStandingQtyAtVendor.Text) - int.Parse(totalRecQty);
                int totalChallansCount = ddlChallanNumbers.Items.Count - 1;
                if (dr.Count < totalChallansCount)
                {
                    if (int.Parse(totalRecQty) < Convert.ToInt32(lblOutStandingQtyAtVendor.Text))
                    {
                        AddNewRowToGrid(lnkBtn, rowIndex, pendingQty);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('You dont have enough Quantity to add another Row.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('You can not add rows more then Challan Numbers Count.');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// When user changes the Challan Number Dropdown this event is Fired. The Challan Number is validated if it has been already used in previous row and quantiy is shown on the 
    /// basis of selceted Challan
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlChallanNumbers_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddlChallanNumbers = (DropDownList)sender;
            GridViewRow currentRow = (GridViewRow)ddlChallanNumbers.NamingContainer;
            if (ddlChallanNumbers.SelectedValue != "")
            {
                string[] splitValues = Regex.Split(ddlChallanNumbers.SelectedValue, " - ");
                TextBox txtExtraReceivingQty = ddlChallanNumbers.FindControl("txtExtraQuantity") as TextBox;
                TextBox txtNewReceivingQty = ddlChallanNumbers.FindControl("TextBox2") as TextBox;
                Label lblTotalQtySent = ddlChallanNumbers.FindControl("Label5") as Label;
                Label lblReceivedQty = ddlChallanNumbers.FindControl("Label6") as Label;
                HiddenField hdnPendingRowsAtVendor = ddlChallanNumbers.FindControl("hdnPendingRowsAtVendor") as HiddenField;
                HiddenField hdnPartsGrId = ddlChallanNumbers.FindControl("hdnPartsGrId") as HiddenField;
                Label lblStation = ddlChallanNumbers.FindControl("Label2") as Label;
                Label lblrew = ddlChallanNumbers.FindControl("lblrew") as Label;
                Label lblToolNo = ddlChallanNumbers.FindControl("Label1") as Label;
                LinkButton lnkbtnPNo = ddlChallanNumbers.FindControl("Label3") as LinkButton;
                Label lblPartName = ddlChallanNumbers.FindControl("Label4") as Label;
                Label lblOutStandingQtyAtVendor = ddlChallanNumbers.FindControl("Label6sentqty") as Label;
                txtNewReceivingQty.Text = Convert.ToString(splitValues[1].Replace(" ", ""));

                List<string> challanList = new List<string>();
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow[] dr = dtCurrentTable.Select("Tool_No=" + lblToolNo.Text + " and Station='" + lblStation.Text + "' and Part_Name='" + lblPartName.Text + "' and P_No='" + lnkbtnPNo.Text + "' and Rwk='" + lblrew.Text + "'");
                int totalRecQty = 0;
                foreach (DataRow dRow in dr)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dRow["Challan_No"])))
                    {
                        string[] splitChallanNumber = Regex.Split(Convert.ToString(dRow["Challan_No"]), " - ");
                        challanList.Add(splitChallanNumber[0]);
                    }

                    string values = dRow["New_Rec_Quantity"] as string;
                    if (values != null)
                    {
                        totalRecQty = totalRecQty + int.Parse(values);
                    }
                }
                int? availableAndValidQty = ValidateQuantities(lblToolNo.Text, lblStation.Text, lblPartName.Text, lnkbtnPNo.Text, lblrew.Text, ddllocation.SelectedItem.Text, Convert.ToString(splitValues[1]), currentRow);
                int totalChallansCount = ddlChallanNumbers.Items.Count - 1;
                if (availableAndValidQty != null && availableAndValidQty > 0)
                {
                    if (ddllocation.SelectedItem.Text == "Weld Assembly Waiting")
                    {
                        int? validParentQtyAfterRatio = ValidateRationWiseQuantity(hdnPartsGrId.Value, Convert.ToInt32(availableAndValidQty.Value));
                        txtNewReceivingQty.Text = Convert.ToString(validParentQtyAfterRatio.Value);
                    }
                    else
                    {
                        txtNewReceivingQty.Text = Convert.ToString(availableAndValidQty.Value);
                    }
                }
                else
                {
                    txtNewReceivingQty.Text = String.Empty;
                }
                if (challanList.Contains(splitValues[0]))
                {
                    ddlChallanNumbers.ClearSelection();
                    txtNewReceivingQty.Text = String.Empty;
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Challan Number " + splitValues[0] + " has been already selected for same Tool, Station, Part Name, P No and Rework No.');", true);
                }
            }


        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Entered quantity is validated and Available and Valid quantiy is shown to User
    /// </summary>
    /// <param name="currentToolNo"></param>
    /// <param name="currentStation"></param>
    /// <param name="currentPartName"></param>
    /// <param name="currentPNo"></param>
    /// <param name="currentRwkNo"></param>
    /// <param name="currentLocation"></param>
    /// <param name="challanSentQty"></param>
    /// <param name="gr"></param>
    /// <returns></returns>
    protected int? ValidateQuantities(string currentToolNo, string currentStation, string currentPartName, string currentPNo, string currentRwkNo,
        string currentLocation, string challanSentQty, GridViewRow gr)
    {
        try
        {
            int totalReceivingQty = 0;
            int totalPendingSentQty = 0;

            for (int i = 0; i < GridView1.Rows.Count; i++)// - 1
            {
                //extract the TextBox values  
                CheckBox chkGridAllchild = GridView1.Rows[i].FindControl("chkAllchild") as CheckBox;
                Label lblGridTotalQtySent = GridView1.Rows[i].FindControl("Label5") as Label;
                Label lblGridReceivedQty = GridView1.Rows[i].FindControl("Label6") as Label;
                TextBox txtGridNewReceivingQty = GridView1.Rows[i].FindControl("TextBox2") as TextBox;
                TextBox txtGridExtraReceivingQty = GridView1.Rows[i].FindControl("txtExtraQuantity") as TextBox;
                Label lblGridStation = GridView1.Rows[i].FindControl("Label2") as Label;
                Label lblGridRwkNo = GridView1.Rows[i].FindControl("lblrew") as Label;

                Label lblGridToolNo = GridView1.Rows[i].FindControl("Label1") as Label;
                LinkButton lnkbtnGridPNo = GridView1.Rows[i].FindControl("Label3") as LinkButton;
                Label lblGridPartName = GridView1.Rows[i].FindControl("Label4") as Label;
                Label lblGridQuantity = GridView1.Rows[i].FindControl("Label8") as Label;
                Label lblGridUploadQty = GridView1.Rows[i].FindControl("Label6Upload_Qty") as Label;
                DropDownList ddlGridChallanNumbers = GridView1.Rows[i].FindControl("ddlChallanNumbers") as DropDownList;
                Label lblGridExtraReceivedQty = GridView1.Rows[i].FindControl("lblExtraReceivedQty") as Label;
                Label lblGridSentQty = GridView1.Rows[i].FindControl("Label6sentqty") as Label;

                HiddenField hdnGridPartsGrId = GridView1.Rows[i].FindControl("hdnPartsGrId") as HiddenField;
                HiddenField hdnGridLocation = GridView1.Rows[i].FindControl("hdnLocation") as HiddenField;
                HiddenField hdnGridSentReceivedDate = GridView1.Rows[i].FindControl("hdnSentReceivedDate") as HiddenField;
                HiddenField hdnGridReceivedDate = GridView1.Rows[i].FindControl("hdnReceivedDate") as HiddenField;
                HiddenField hdnGridDeliveryCommitDate = GridView1.Rows[i].FindControl("hdnDeliveryCommitDate") as HiddenField;
                HiddenField hdnPendingRowsAtVendor = GridView1.Rows[i].FindControl("hdnPendingRowsAtVendor") as HiddenField;
                //GRID
                bool Is_Selected = Convert.ToBoolean(chkGridAllchild.Checked);
                string Parts_Gr_Id = Convert.ToString(hdnGridPartsGrId.Value);
                string Location = Convert.ToString(hdnGridLocation.Value);
                string Sent_Received_Date = Convert.ToString(hdnGridSentReceivedDate.Value);
                string Received_Date = Convert.ToString(hdnGridReceivedDate.Value);
                string Delivery_Commit_Date = Convert.ToString(hdnGridDeliveryCommitDate.Value);
                string tool_no = Convert.ToString(lblGridToolNo.Text);
                string stn = Convert.ToString(lblGridStation.Text);
                string p_no = Convert.ToString(lnkbtnGridPNo.Text);
                string part_name = Convert.ToString(lblGridPartName.Text);
                string quant_sent = Convert.ToString(lblGridTotalQtySent.Text);
                string quant_rec = Convert.ToString(lblGridReceivedQty.Text);
                string Challan_No = Convert.ToString(ddlGridChallanNumbers.SelectedValue);
                string New_Rec_Quantity = Convert.ToString(txtGridNewReceivingQty.Text);
                string Extra_Qty_Received = Convert.ToString(lblGridExtraReceivedQty.Text);
                string Extra_Receiving_Qty = Convert.ToString(txtGridExtraReceivingQty.Text);
                string Upload_Qty = Convert.ToString(lblGridUploadQty.Text);
                string SentQty = Convert.ToString(lblGridSentQty.Text);
                string Rwk = Convert.ToString(lblGridRwkNo.Text);
                string TotalPendingQtyAtVendor = Convert.ToString(hdnPendingRowsAtVendor.Value);


                if (tool_no == currentToolNo && stn == currentStation && part_name == currentPartName && p_no == currentPNo && Rwk == currentRwkNo && Location == currentLocation)
                {
                    totalPendingSentQty = int.Parse(SentQty);
                    if (gr.RowIndex != GridView1.Rows[i].RowIndex)
                    {
                        if (!string.IsNullOrEmpty(New_Rec_Quantity))
                        {
                            totalReceivingQty = Convert.ToInt32(New_Rec_Quantity) + totalReceivingQty;
                        }
                    }
                }
            }
            int? returnQty = null;
            int pendingQty = totalPendingSentQty - totalReceivingQty;
            if (!string.IsNullOrEmpty(challanSentQty))
            {
                if (pendingQty > Convert.ToInt32(challanSentQty))
                {
                    returnQty = Convert.ToInt32(challanSentQty);
                }
                if (pendingQty <= Convert.ToInt32(challanSentQty))
                {
                    returnQty = pendingQty;
                }
            }
            else
            {
                return pendingQty;
            }
            return returnQty;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
    }
}
