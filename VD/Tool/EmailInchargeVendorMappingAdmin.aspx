﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="EmailInchargeVendorMappingAdmin.aspx.cs" Inherits="Tool_EmailInchargeVendorMappingAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .style14
        {
            width: 1034px;
        }
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="border: 1px solid Blue; min-height: 100px; padding: 9px;">
        <div>
            <div align="center">
                <span class="style13">Email Incharge & Vendor Configuration</span>
            </div>
        </div>
        <div style="width: 100%; margin-bottom: 10px; margin-top: 20px;">
            <div style="font-size: 13px; float: left;">
                Incharge
                <br />
                <asp:TextBox runat="server" ID="inputEmailIncharge" ClientIDMode="Static" CssClass="autocomplete glowing-border"
                    Style="width: 170px;" Placeholder="Incharge User Name"></asp:TextBox>
            </div>
            <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" ImageUrl="~/images/searchbtn.jpg"
                OnClick="btnSearch_Click" Style="margin-top: 14px; margin-left: 15px;" />
            <asp:HyperLink ID="HyperLink1" Style="background-color: #335599; float: right; color: White;
                padding: 5px; margin-top: 9px;" runat="server" NavigateUrl="~/Tool/EmailInchargeVendorMappingForm.aspx">Add Mapping</asp:HyperLink>
        </div>
        <asp:GridView runat="server" ID="gridEmailInchargeVendorMapping" BackColor="White"
            AutoGenerateColumns="false" EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True"
            AllowPaging="true" AllowSorting="false" Width="100%" BorderColor="#B8BABD" BorderStyle="None"
            OnPageIndexChanging="gridEmailInchargeVendorMapping_PageIndexChanging" OnRowCommand="gridEmailInchargeVendorMapping_RowCommand"
            OnRowDataBound="gridEmailInchargeVendorMapping_RowDataBound" OnRowDeleting="gridEmailInchargeVendorMapping_RowDeleting"
            PageSize="15" BorderWidth="1px" Style="font-size: 12px; font-family: Verdana;
            line-height: 26px;">
            <HeaderStyle CssClass="tableheader" />
            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
            <Columns>
                <asp:BoundField DataField="Email_Incharge" HeaderText="Email Incharge" HeaderStyle-Width="120px"
                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White" />
                <asp:TemplateField HeaderText="Vendors" HeaderStyle-Width="80px" HeaderStyle-Font-Bold="false"
                    HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblVendors"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="" HeaderStyle-Width="80px" HeaderStyle-Font-Bold="false"
                    HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" CommandName="Edit" CssClass="btnCustom" CommandArgument='<%#Eval("Email_Incharge") %>'
                            ID="lnkBtnEdit">Edit</asp:LinkButton>
                        &nbsp;
                        <asp:LinkButton runat="server" CommandName="Delete" CssClass="btnCustom" CommandArgument='<%#Eval("Email_Incharge") %>'
                            ID="LinkButton1">Delete</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle Font-Bold="false" />
            <PagerStyle BackColor="#335599" ForeColor="White" Font-Size="Large" HorizontalAlign="Center"
                CssClass="gridview" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#000" />
            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
        </asp:GridView>
        <asp:Label runat="server" ID="lblRecorrdsCount" CssClass="recordsstatus" Width="850px"></asp:Label>
    </div>
</asp:Content>
