﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;

public partial class PartList_summary_report : System.Web.UI.Page
{
    string station = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                BindVendors();
                showgrid();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
                ex.ToString();
            }
        }
    }

    /// <summary>
    /// Binds the Vendor from Vendor Master Job Work table
    /// </summary>
    protected void BindVendors()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select Vendor_Name from Vendor_Master_Job_Work where Vendor_Type='End Store'", conn);
            checkEndStores.DataSource = cmd.ExecuteReader();
            checkEndStores.DataTextField = "Vendor_Name";
            checkEndStores.DataValueField = "Vendor_Name";
            checkEndStores.DataBind();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }

    /// <summary>
    /// On click on plus icon shown in front of Each Row at First Level, it Maximizes the Grid and shows all the Positions that are pending against that Tool No, Vendor Name.
    /// It calls BindPositionsSummary method inside for binding the inner GridView on click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Show_Hide_OrdersGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlOrders").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";

            string customerId = gvCustomers.DataKeys[row.RowIndex].Value.ToString();
            string cell_1_Value = gvCustomers.Rows[row.RowIndex].Cells[2].Text;
            if (tbStation.Text.Length > 0)
            {
                station = tbStation.Text;
            }
            else
            {
                station = "";
            }

            GridView gvOrders = row.FindControl("gvOrders") as GridView;
            BindPositionsSummary(customerId, gvOrders, cell_1_Value, station);
        }
        else
        {
            row.FindControl("pnlOrders").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    /// <summary>
    /// Binds the Data with the GridView on Plus icon shown on first Level. Shows the the Postion for the clicked Tool and Vendor
    /// </summary>
    /// <param name="customerId"></param>
    /// <param name="gvOrders"></param>
    /// <param name="location"></param>
    /// <param name="station"></param>
    private void BindPositionsSummary(string customerId, GridView gvOrders, string location, string station)
    {
        gvOrders.ToolTip = customerId;
        DataTable dt = new DataTable();
        string searchQueryLocationInfo = "";
        string searchQueryPartList = "";
        SearchQuery(out searchQueryPartList, "phg.", out searchQueryLocationInfo, "lig.");
        if (location == "COMPANY")
        {
            if (string.IsNullOrEmpty(searchQueryPartList))
            {
                searchQueryPartList = "1=1";
            }
            dt = UtilityFunctions.GetData(string.Format("SELECT phg.tool_no ,phg.Rework ,phg.station ,phg.part_name ,phg.quantity,phg.Challan_No,phg.date_of_trans ,(case phg.Delivery_commit_date  when '1900-01-01 00:00:00.000' then  ''   else phg.Delivery_commit_date  end) as  Delivery_commit_date,phg.Upload_Qty,phg.p_no, phg.location , phg.quantity AS qtypending, phg.date_of_trans as Sent_Received_Date, phg.Rwk_No as Rwk,'' as Extra_Received_Quantity  FROM part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where phg.location='" + WebUtility.HtmlDecode(location) + "' and phg.tool_no ='" + customerId + "' and  phg.quantity>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)   and " + searchQueryPartList + " order by phg.tool_no asc"));
        }
        else
        {
            if (string.IsNullOrEmpty(searchQueryLocationInfo))
            {
                searchQueryLocationInfo = "1=1";
            }
            dt = UtilityFunctions.GetData(string.Format("select lig.tool_no,lig.Rework,lig.station,lig.Challan_No,lig.p_no,lig.part_name,lig.req_qty,'' as date_of_trans, lig.Upload_Qty,(case lig.Delivery_commit_date  when '1900-01-01 00:00:00.000' then  ''   else lig.Delivery_commit_date  end) as Delivery_commit_date,lig.location,(isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0)) as qtypending,lig.quant_sent, quant_rec,quant_pend,quant_others,Sent_Received_Date,Received_Date,Rwk,Extra_Received_Quantity from location_info_GR lig  left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where location='" + WebUtility.HtmlDecode(location) + "' and  lig.tool_no ='" + customerId + "' and (isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0))>0 and  pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchQueryLocationInfo + " order by tool_no asc"));
        }
        gvOrders.DataSource = dt;
        Session["BindOrders"] = dt;
        gvOrders.DataBind();
        gvOrders.Columns[7].Visible = true;
    }

    protected void OnOrdersGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvOrders = (sender as GridView);
        gvOrders.PageIndex = e.NewPageIndex;
        DataTable dt = (DataTable)Session["BindOrders"];
        gvOrders.DataSource = dt;
        gvOrders.DataBind();
    }

    /// <summary>
    /// This Event is Called when User presses the Plus Icon shown at Second Level in front of Each position and clicking on that button displays the Position Send/Receive History
    /// It calls BindPositionHistory method to Bind the History GridView
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Show_Hide_ProductsGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        GridViewRow Gv2Row = (GridViewRow)((ImageButton)sender).NamingContainer;
        GridView Childgrid = (GridView)(Gv2Row.Parent.Parent);
        GridViewRow Gv1Row = (GridViewRow)(Childgrid.NamingContainer);
        int b = Gv1Row.RowIndex;

        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlProducts").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            GridView gvProducts = row.FindControl("gvProducts") as GridView;

            string Position_no = Childgrid.Rows[row.RowIndex].Cells[1].Text;
            string partname = Childgrid.Rows[row.RowIndex].Cells[2].Text;
            string vendor = Childgrid.Rows[row.RowIndex].Cells[3].Text;
            string station = Childgrid.Rows[row.RowIndex].Cells[4].Text;
            int tool = Convert.ToInt32(Childgrid.Rows[row.RowIndex].Cells[9].Text);
            string reworkNo = Childgrid.Rows[row.RowIndex].Cells[11].Text.Replace("&nbsp;", "");

            BindPositionHistory(Position_no, gvProducts, partname, vendor, station, tool, reworkNo);
        }
        else
        {
            row.FindControl("pnlProducts").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    /// <summary>
    /// Binds the Positon Sent/Received GridView and show the Complete History of that clicked position for same tool,station,pno, partname and rework No
    /// </summary>
    /// <param name="position"></param>
    /// <param name="gvOrdersPosition"></param>
    /// <param name="part_name"></param>
    /// <param name="vendor"></param>
    /// <param name="station"></param>
    /// <param name="toolno"></param>
    /// <param name="reworkNo"></param>
    private void BindPositionHistory(string position, GridView gvOrdersPosition, string part_name, string vendor, string station, int toolno, string reworkNo)
    {
        gvOrdersPosition.DataSource = UtilityFunctions.GetData(string.Format("select id,location,date_of_trans,sent_or_rec,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as Pending_Qty_PPC,TransSentQty as Send_Or_Rec_Qty,TransSentQty,Rwk_No,User_Name from part_history_GR where   p_no='" + position + "' and part_name='" + part_name + "' and tool_no='" + toolno + "' and station='" + station + "' and rwk_no='" + reworkNo + "'   order by date_of_trans asc "));
        gvOrdersPosition.DataBind();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    /// <summary>
    /// Generates the Search Query based on the Values entered by User for Given Filters like Tool No, Station, Pno etc.
    /// </summary>
    /// <param name="forparthistoryGr"></param>
    /// <param name="aliasPH"></param>
    /// <param name="forlocationinfoGR"></param>
    /// <param name="aliasLI"></param>
    protected void SearchQuery(out string forparthistoryGr, string aliasPH, out string forlocationinfoGR, string aliasLI)
    {
        string searchQueryPartList = "", searchQueryLocationInfo = "";
        if (tbTool.Text != "")
        {
            searchQueryLocationInfo = aliasLI + " tool_no='" + tbTool.Text + "'";
            searchQueryPartList = aliasPH + "tool_no='" + tbTool.Text + "'";
        }
        if (tbStation.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "station = '" + tbStation.Text + "'";
            }
            else
            {
                searchQueryLocationInfo = aliasLI + "station = '" + tbStation.Text + "'";
            }

            if (searchQueryPartList != "")
            {
                searchQueryPartList = searchQueryPartList + " and " + aliasPH + "station = '" + tbStation.Text + "'";
            }
            else
            {
                searchQueryPartList = aliasPH + "station = '" + tbStation.Text + "'";
            }
        }
        if (tbLocation.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "location='" + tbLocation.Text + "'";
            }
            else
            {
                searchQueryLocationInfo = aliasLI + "location='" + tbLocation.Text + "'";
            }

            if (searchQueryPartList != "")
            {
                searchQueryPartList = searchQueryPartList + " and " + aliasPH + "location='" + tbLocation.Text + "'";
            }
            else
            {
                searchQueryPartList = aliasPH + "location='" + tbLocation.Text + "'";
            }
        }
        if (tbPartName.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "part_name like '" + tbPartName.Text + "%'";
            }
            else
            {
                searchQueryLocationInfo = aliasLI + "part_name like '" + tbPartName.Text + "%'";
            }

            if (searchQueryPartList != "")
            {
                searchQueryPartList = searchQueryPartList + " and " + aliasPH + "part_name like '" + tbPartName.Text + "%'";
            }
            else
            {
                searchQueryPartList = aliasPH + "part_name like '" + tbPartName.Text + "%'";
            }
        }
        if (tbPosition.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "p_no='" + tbPosition.Text + "'";
            }
            else
            {
                searchQueryLocationInfo = aliasLI + "p_no='" + tbPosition.Text + "'";
            }

            if (searchQueryPartList != "")
            {
                searchQueryPartList = searchQueryPartList + " and " + aliasPH + "p_no='" + tbPosition.Text + "'";
            }
            else
            {
                searchQueryPartList = aliasPH + "p_no='" + tbPosition.Text + "'";
            }
        }
        if (tbReworkNo.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                if (chkBoxReworkNumberContains.Checked)
                {
                    searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "Rwk like '" + tbReworkNo.Text + "%'";
                }
                else
                {
                    searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "Rwk = '" + tbReworkNo.Text + "'";
                }
            }
            else
            {
                if (chkBoxReworkNumberContains.Checked)
                {
                    searchQueryLocationInfo = aliasLI + "Rwk like '" + tbReworkNo.Text + "%'";
                }
                else
                {
                    searchQueryLocationInfo = aliasLI + "Rwk = '" + tbReworkNo.Text + "'";
                }
            }

            if (searchQueryPartList != "")
            {
                if (chkBoxReworkNumberContains.Checked)
                {
                    searchQueryPartList = searchQueryPartList + " and " + aliasPH + "Rwk_No like '" + tbReworkNo.Text + "%'";
                }
                else
                {
                    searchQueryPartList = searchQueryPartList + " and " + aliasPH + "Rwk_No ='" + tbReworkNo.Text + "'";
                }
            }
            else
            {
                if (chkBoxReworkNumberContains.Checked)
                {
                    searchQueryPartList = aliasPH + "Rwk_No like '" + tbReworkNo.Text + "%'";
                }
                else
                {
                    searchQueryPartList = aliasPH + "Rwk_No ='" + tbReworkNo.Text + "'";
                }
            }
        }

        forlocationinfoGR = searchQueryLocationInfo;
        forparthistoryGr = searchQueryPartList;
    }

    /// <summary>
    /// Binds the GridView at First Level. It shows the Vendor Name, Tool No and Pending Quantity. This Method is called on Page Load or when user clicks on Search Button
    /// </summary>
    public void showgrid()
    {
        string s = "";
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);

            string searchQueryLocationInfo = "";
            string searchQueryPartList = "";
            SearchQuery(out searchQueryPartList, "phg.", out searchQueryLocationInfo, "lig.");
            string endStoreCondition = "";
            foreach (ListItem checkList in checkEndStores.Items)
            {
                if (!checkList.Selected)
                {
                    endStoreCondition = endStoreCondition + "\'" + checkList.Text + "\',";
                }
            }
            endStoreCondition = endStoreCondition.TrimEnd(',');
            if (!string.IsNullOrEmpty(endStoreCondition))
            {
                if (!string.IsNullOrEmpty(searchQueryLocationInfo) && !string.IsNullOrEmpty(searchQueryPartList))
                {
                    searchQueryLocationInfo = searchQueryLocationInfo + " and lig.location not in (" + endStoreCondition + ")";
                    searchQueryPartList = searchQueryPartList + " and phg.location not in (" + endStoreCondition + ")";
                }
                else
                {

                    searchQueryLocationInfo = " lig.location not in (" + endStoreCondition + ")";
                    searchQueryPartList = " phg.location not in (" + endStoreCondition + ")";
                }
            }
            if (string.IsNullOrEmpty(searchQueryLocationInfo) && string.IsNullOrEmpty(searchQueryPartList))
            {
                searchQueryLocationInfo = "1=1";
                searchQueryPartList = "1=1";
            }
            s = " SELECT   phg.[tool_no] , phg.[part_name],phg.[p_no],phg.[station], phg.[Rwk_No], phg.[location],phg.[quantity] as qtypending  FROM part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where phg.location='COMPANY'  and phg.quantity>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchQueryPartList + " union all "
               + " select distinct  lig.[tool_no] , lig.[part_name],lig.[p_no],lig.[station],lig.[Rwk],lig.location, isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0) as qtypending from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0)>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchQueryLocationInfo + " order by  tool_no, location asc";
            DataTable dtItems = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter(s, conn);
            ad.Fill(dtItems);

            var result =
                     dtItems.AsEnumerable()
                         .Select(p => new
                         {
                             tool = p.Field<int>("Tool_No"),
                             location = p.Field<string>("Location"),
                             quantity = p.Field<int>("qtypending")
                         })
                         .GroupBy(p => new { p.tool, p.location })
                         .Select(p => new { Tool_No = p.Key.tool, Location = p.Key.location, QtyPending = p.Sum(x => Convert.ToInt32(x.quantity)) })
                         .ToList();

            Session["DataTableAdvanceReport"] = result;
            gvCustomers.DataSource = result;
            gvCustomers.DataBind();
            conn.Close();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void chkAllEndStores_CheckedChanged(object sender, EventArgs e)
    {
        foreach (ListItem checkList in checkEndStores.Items)
        {
            if (chkAllEndStores.Checked)
            {
                checkList.Selected = true;
            }
            else
            {
                checkList.Selected = false;
            }
        }

    }
}