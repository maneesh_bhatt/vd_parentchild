﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ToolOutstandingPartSummary.aspx.cs" Inherits="Tool_ToolOutstandingPartSummary"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style2
        {
            border: none;
            width: 100px;
        }
        
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            /* border: 1px solid rgba(158, 158, 158, 0.58);
            margin-left: 2px;
            height: 28px;*/
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 860px;
            height: 440px;
            top: 20%;
            left: 30%;
            margin-left: -185px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
            line-height: 28px;
            overflow-y: scroll;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .awbimagecontainer
        {
            float: right;
            width: 29%;
            margin-right: 12px;
            margin-top: 43px;
            border: 1px solid #2D89EF;
            height: 180px;
        }
        .web_dialog_info
        {
            display: none;
            position: fixed;
            width: 940px;
            height: 510px;
            top: 22%;
            left: 36%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
    </style>
    <script language="javascript" type="text/javascript">

        $(function () {
            $('#<%=inputVendorName.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ToolOutstandingPartSummary.aspx/GetVendorName",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputVendorName.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputVendorName.ClientID%>').blur();

            });

            $('#<%=inputToolNo.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ToolOutstandingPartSummary.aspx/GetTooNumber",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputToolNo.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputToolNo.ClientID%>').blur();

            });

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="width: 100%; border: 1px solid Blue; min-height: 300px;">
        <div style="padding: 9px;">
            <div>
                <div align="center">
                    <span class="style13">Tool Outstanding Part Summary Report</span>
                </div>
            </div>
            <div style="margin-bottom: 10px; margin-top: 20px; height: 300px;">
                <div style="height: 40px; float: left; width: 100%;">
                    <div>
                        <span style="width: 150px; float: left;">Location</span>
                        <asp:TextBox runat="server" ID="inputVendorName" ClientIDMode="Static" Style="width: 200px;
                            float: left;" Placeholder=" Vendor Name"></asp:TextBox>
                    </div>
                    <asp:Button ID="btnGenerateVisitReport" runat="server" Visible="false" EnableViewState="true"
                        CausesValidation="false" Text="Generate Vendor Visit Report" Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px;
                        color: white; height: 30px; width: 190px; float: right;" OnClick="btnGenerateVisitReport_Click" />
                    <asp:Button ID="btnDownload" Visible="false" runat="server" Text="Download" OnClick="btnDownload_Click"
                        Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px; color: white;
                        height: 30px; width: 118px; margin-right: 3px; float: right;" />
                </div>
                <div style="height: 40px; float: left; width: 100%;">
                    <span style="width: 150px; float: left;">Tool No</span>
                    <asp:TextBox runat="server" ID="inputToolNo" ClientIDMode="Static" Style="width: 200px;
                        float: left;" Placeholder=" Tool Number"></asp:TextBox>
                </div>
                <div style="float: left; width: 100%;">
                    <span style="width: 150px; float: left;">End Store</span>
                    <div style="line-height: 26px; font-size: 12px; float: left; margin-left: 0px;">
                        <asp:CheckBox runat="server" ID="chkAllEndStores" AutoPostBack="true" OnCheckedChanged="chkAllEndStores_CheckedChanged" />
                        <b>Check All</b>
                        <asp:CheckBoxList runat="server" ID="checkEndStores" RepeatColumns="5" RepeatDirection="Horizontal" />
                    </div>
                </div>
                <div style="margin-left: 12.5%; float: left; height: 40px;">
                    <asp:Button ID="btnSearch" runat="server" Text="Submit" OnClick="btnSearch_Click"
                        Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px; color: white;
                        height: 32px; width: 80px; line-height: 28px; text-align: center; border: 2px solid #dadada;" />
                </div>
                <div style="float: left; width: 100%; height: 40px;">
                    <span style="width: 150px; float: left;">Sort By</span>
                    <div style="line-height: 26px; font-size: 13px; float: left; margin-left: 0px; width: 75%;">
                        <asp:CheckBox runat="server" ID="chkSortGrid" AutoPostBack="true" OnCheckedChanged="chkSortGrid_CheckedChanged" />
                        Tool No, Station, Upload Date, Rework Number ASC &nbsp; &nbsp;
                        <div style="float: right;">
                            <span><b>Default Sorted By:</b></span> Location ASC, Delay_From_Original_Date Desc</div>
                    </div>
                </div>
                <div style="float: left; width: 100%; height: 40px;">
                    <span style="width: 150px; float: left;">View All Tools</span>
                    <div style="line-height: 26px; font-size: 13px; float: left; margin-left: 0px; width: 75%;">
                        <div style="width: 558px; float: left;">
                            <asp:CheckBox runat="server" ID="chkViewAllTools" Style="float: left;" AutoPostBack="true"
                                OnCheckedChanged="chkViewAllTools_CheckedChanged" />
                            <div runat="server" id="divRecordsUptoDate" visible="false" style="float: left;">
                                <span style="width: 60px; float: left;">To Date:</span>
                                <asp:TextBox ID="inputToDate" Style="width: 120px; float: left;" runat="server"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="true" Format="dd-MMM-yyyy"
                                    TargetControlID="inputToDate" OnClientShowing="CurrentDateShowing">
                                </cc1:CalendarExtender>
                            </div>
                        </div>
                        <span><b>Sort By-</b> Upload Date, Rework Number, Tool No, Station ASC</span>
                    </div>
                </div>
            </div>
            <asp:GridView runat="server" ID="gridViewVendorPendingItems" AutoGenerateColumns="false"
                Width="100%" ShowHeaderWhenEmpty="true" OnRowDataBound="gridViewVendorPendingItems_RowDataBound"
                AllowPaging="false" AllowSorting="false" Style="font-size: 13px; font-family: Verdana;
                line-height: 26px;" HeaderStyle-Font-Size="10.5px">
                <RowStyle BackColor="White" ForeColor="Black" Font-Size="12px" />
                <Columns>
                    <asp:BoundField DataField="Tool" HeaderText="Tool" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="160px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Station" HeaderText="Station" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="70px" HeaderStyle-ForeColor="White" />
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="70px" ItemStyle-Width="70px"
                        HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White" ItemStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            P No
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkBtnPNumber" CausesValidation="false" runat="server" Text='<%# Eval("p_no") %>'
                                CommandArgument='<%# Eval("p_no") + ";" + Eval("tool") + ";" + Eval("station") + ";" + Eval("part_name") +";"+Eval("rwk") %>'
                                OnClick="lnkBtnPNumber_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Rwk" HeaderText="Rwk" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Upload_Date" DataFormatString="{0:dd-MM-yyyy}" HeaderText="First Upload Date"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Part_Name" HeaderText="Part Name" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="110px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Upload_Qty" HeaderText="Upload Qty" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Quant_Sent" HeaderText="Qty Sent" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Quant_Rec" HeaderText="Qty Rec" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="SentQty" HeaderText="Qty Pending" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="130px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="First_Sent_Date" DataFormatString="{0:dd-MM-yyyy}" HeaderText="First Sent Date"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="130px" HeaderStyle-ForeColor="White" />
                     <asp:BoundField DataField="Required_By_Date" DataFormatString="{0:dd-MM-yyyy}"
                        HeaderText="Required By Date" HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px"
                        HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Original_Commit_Date" DataFormatString="{0:dd-MM-yyyy}"
                        HeaderText="First Delivery Commitment Date" HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px"
                        HeaderStyle-ForeColor="White" /><%-- code changed by himaneesh--%>
                    <asp:BoundField DataField="Delivery_Commit_Date" DataFormatString="{0:dd-MM-yyyy}"
                        HeaderText="Updated Delivery Commitment Date" HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px"
                        HeaderStyle-ForeColor="White" /><%-- code changed by himaneesh--%>
                    <asp:BoundField DataField="Delay_From_Original_Date" HeaderText="Delay from Original Date"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="130px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Delay_From_Latest_Date" HeaderText="Delay from Latest Date"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="130px" HeaderStyle-ForeColor="White" />
                </Columns>
            </asp:GridView>
        </div>
        <div>
            <div id="overlay" runat="server" clientidmode="Static" class="web_dialog_overlay">
            </div>
            <div id="dialog" clientidmode="Static" runat="server" class="web_dialog">
                <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                    <div style="float: left; width: 100%;">
                        <div class="web_dialog_title" style="width: 20%; float: left;">
                            Item History
                        </div>
                        <div class="web_dialog_title align_right">
                            <asp:LinkButton runat="server" ID="btnClosePopUp" CausesValidation="false" ClientIDMode="Static"
                                OnClick="btnClosePopUp_Click">Close</asp:LinkButton>
                        </div>
                    </div>
                    <div runat="server" id="pnlHistoryPopUp">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
