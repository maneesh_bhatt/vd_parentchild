﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Tool_EmailInchargeVendorMappingAdmin : System.Web.UI.Page
{
    string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
    public static string searchCondition;
    protected void Page_Load(object sender, EventArgs e)
    {
        searchCondition = "";
        try
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Retrieves the distinct Email Encharges from Email_Incharge_Vendor_Mapping tables and binds in the GridView
    /// </summary>
    protected void BindGrid()
    {
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            if (Convert.ToString(ViewState["SearchCondition"]) != "")
            {

                adapter = new SqlDataAdapter("select distinct Email_Incharge from Email_Incharge_Vendor_Mapping where " + ViewState["SearchCondition"], con);
            }
            else
            {
                adapter = new SqlDataAdapter("select distinct Email_Incharge from Email_Incharge_Vendor_Mapping", con);
            }

            DataTable td = new DataTable();
            adapter.Fill(td);
            gridEmailInchargeVendorMapping.DataSource = td;
            gridEmailInchargeVendorMapping.DataBind();
            gridEmailInchargeVendorMapping.EmptyDataText = "No Record Found";
            lblRecorrdsCount.Text = Convert.ToString(td.Rows.Count) + " " + "Records In Total";
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }

    }

    /// <summary>
    /// Retrieves the Vendors Maps against Each Inchage through Email_Incharge_Vendor_Mapping table for each GridView Row.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridEmailInchargeVendorMapping_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblVendors = (Label)e.Row.FindControl("lblVendors");

            try
            {
                using (SqlConnection connec = new SqlConnection(sqlconnstring))
                {
                    connec.Open();
                    List<string> vendorList = new List<string>();
                    DataTable dt = new DataTable();
                    SqlDataAdapter ad = new SqlDataAdapter("select * from Email_Incharge_Vendor_Mapping where Email_Incharge=@EmailIncharge", connec);
                    ad.SelectCommand.Parameters.AddWithValue("@EmailIncharge", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Email_Incharge")));
                    ad.Fill(dt);

                    string vendorName = "";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SqlCommand cmd = new SqlCommand("select * from Vendor_Master_Job_Work where Id=@VendorId", connec);
                        cmd.Parameters.AddWithValue("@VendorId", dt.Rows[i]["Vendor_Id"]);
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                rdr.Read();
                                vendorName = Convert.ToString(rdr["Vendor_Name"]) + "," + vendorName;
                            }
                        }
                    }
                    lblVendors.Text = vendorName.TrimEnd(',');
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }


    protected void gridEmailInchargeVendorMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // Set the index of the new display page.  
        gridEmailInchargeVendorMapping.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //gridPackages.PageIndex = 1;
        searchCondition = "";
        if (inputEmailIncharge.Text != "")
        {
            searchCondition = "Email_Incharge ='" + inputEmailIncharge.Text + "'";
        }

        ViewState["SearchCondition"] = searchCondition;
        BindGrid();
    }

    /// <summary>
    /// This Event is called when user clicks on Edit or Delete button given in front of Each Row
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridEmailInchargeVendorMapping_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            Response.Redirect("EmailInchargeVendorMappingForm.aspx?ID=" + e.CommandArgument);
        }
        else if (e.CommandName == "Delete")
        {
            SqlConnection connec = new SqlConnection(sqlconnstring);
            try
            {
                connec.Open();
                SqlCommand cmd = new SqlCommand("delete from Email_Incharge_Vendor_Mapping where Email_Incharge=@EmailIncharge", connec);
                cmd.Parameters.AddWithValue("@EmailIncharge", e.CommandArgument);
                cmd.ExecuteNonQuery();
                BindGrid();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }


    protected void gridEmailInchargeVendorMapping_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    /// <summary>
    /// Retrieves the Vendor List from Vendor Master Job Work table
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetVendorName(string text)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            List<string> allItems = new List<string>();
            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrWhiteSpace(text))
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work where Vendor_Name like '" + text + "%' and Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ", con);
            }
            else
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work where Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ", con);
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string vendorName = Convert.ToString(reader["Vendor_Name"]);
                allItems.Add(vendorName);
            }
            reader.Close();
            return allItems;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            con.Close();
        }
    }

}

