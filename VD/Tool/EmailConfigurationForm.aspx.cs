﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Data;
using activeDirectoryLdapExamples;
using System.Drawing;

public partial class Tool_EmailConfigurationForm : System.Web.UI.Page
{
    static string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadLevels();
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                LoadData(Convert.ToString(Request.QueryString["ID"]));
            }
        }
    }

    /// <summary>
    /// Loads the Existing data for the Row in which user has clicked Edit in Email Configuration Admin Page
    /// </summary>
    /// <param name="Id"></param>
    protected void LoadData(string Id)
    {
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            connec.Open();
            SqlCommand cmd = new SqlCommand("select * from Email_Configuration where Email_Configuration_Id=@EmailConfigurationId", connec);
            cmd.Parameters.AddWithValue("@EmailConfigurationId", Id);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                if (rdr.HasRows)
                {
                    rdr.Read();
                    ddlCategory.SelectedValue = LoadCategoryByLevelId(Convert.ToString(rdr["Email_Level_Id"]));
                    LoadLevels();
                    ddlLevel.SelectedValue = Convert.ToString(rdr["Email_Level_Id"]);
                    ddlEmailIncharge.SelectedValue = Convert.ToString(rdr["Email_Incharge"]);
                    inputEmailSubject.Text = Convert.ToString(rdr["Email_Subject"]);
                    inputEmailTo.Text = Convert.ToString(rdr["Email_To"]);
                    inputEmailCC.Text = Convert.ToString(rdr["Email_CC"]);

                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Loads the Email Category by Email Level Id
    /// </summary>
    /// <param name="emailLevelId"></param>
    /// <returns></returns>
    private string LoadCategoryByLevelId(string emailLevelId)
    {
        string category = "";
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            ddlLevel.Items.Clear();
            if (!string.IsNullOrEmpty(emailLevelId))
            {
                DataTable dt = new DataTable();
                SqlDataAdapter ad = new SqlDataAdapter();

                ad = new SqlDataAdapter("select Category from Email_Level_Master where Email_Level_Id=" + emailLevelId + "", con);
                ad.Fill(dt);
                category = Convert.ToString(dt.Rows[0]["Category"]);

            }
            return category;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            con.Close();
        }

    }

    /// <summary>
    /// Loads the List of Users from LDAP (Active Directory)
    /// </summary>
    protected void LoadUsers()
    {
        try
        {
            List<Users> userList = Users.LoadUsersList();
            ddlEmailIncharge.DataSource = userList;
            ddlEmailIncharge.DataTextField = "UserName";
            ddlEmailIncharge.DataValueField = "UserName";
            ddlEmailIncharge.DataBind();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Inserts the new records in case of Add, Updates the record in User visits this Page with the Email_Configuration_Id as QueryString
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            connec.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connec;

            if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
            {
                cmd.CommandText = "select * from Email_Configuration where Email_Level_Id=@EmailLevelId and Email_Incharge=@EmailIncharge and Email_Configuration_Id!=@EmailConfigurationId";
                cmd.Parameters.AddWithValue("@EmailConfigurationId", Request.QueryString["Id"]);
            }
            else
            {
                cmd.CommandText = "select * from Email_Configuration where Email_Level_Id=@EmailLevelId and Email_Incharge=@EmailIncharge";
            }
            cmd.Parameters.AddWithValue("@EmailLevelId", ddlLevel.SelectedValue);
            cmd.Parameters.AddWithValue("@EmailIncharge", ddlEmailIncharge.SelectedValue);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                if (rdr.HasRows)
                {
                    lblMessage.Text = "Record For Selected Level and Incharge already Exist in Database";
                    lblMessage.ForeColor = Color.Red;
                    return;
                }
            }

            cmd.Parameters.Clear();

            if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
            {
                cmd.CommandText = "update Email_Configuration set Email_Level_Id=@EmailLevelId,Email_Incharge=@EmailIncharge,Email_Subject=@EmailSubject,Email_To=@EmailTo,Email_CC=@EmailCC,Updated_By=@UpdatedBy,Updated_Date=@UpdatedDate where Email_Configuration_Id=@EmailConfigurationId";
                cmd.Parameters.AddWithValue("@EmailConfigurationId", Request.QueryString["Id"]);
                cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserID"]);
                cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
            }
            else
            {
                cmd.CommandText = "insert into Email_Configuration (Email_Level_Id,Email_Incharge,Email_Subject,Email_To,Email_CC,Created_By,Created_Date) Values(@EmailLevelId,@EmailIncharge,@EmailSubject,@EmailTo,@EmailCC,@CreatedBy,@CreatedDate)";
                cmd.Parameters.AddWithValue("@CreatedBy", Session["UserID"]);
                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
            }
            cmd.Parameters.AddWithValue("@EmailLevelId", ddlLevel.SelectedValue);
            cmd.Parameters.AddWithValue("@EmailIncharge", ddlEmailIncharge.SelectedValue);
            cmd.Parameters.AddWithValue("@EmailSubject", inputEmailSubject.Text);
            cmd.Parameters.AddWithValue("@EmailTo", inputEmailTo.Text);
            cmd.Parameters.AddWithValue("@EmailCC", inputEmailCC.Text);

            cmd.ExecuteNonQuery();
            lblMessage.Text = "Data Saved Successfully.";
            ClearControls();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            connec.Close();
        }
    }

    protected void ClearControls()
    {
        ddlLevel.SelectedIndex = 0;
        ddlEmailIncharge.SelectedIndex = 0;
        ddlCategory.SelectedIndex = 0;
        inputEmailSubject.Text = String.Empty;
        inputEmailTo.Text = String.Empty;
        inputEmailCC.Text = String.Empty;
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadLevels();
    }

    /// <summary>
    /// Loads the Email Levels from Email Level Master table By Email Category
    /// </summary>
    protected void LoadLevels()
    {
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            ddlLevel.Items.Clear();
            if (ddlCategory.SelectedValue != "")
            {
                DataTable dt = new DataTable();
                SqlDataAdapter ad = new SqlDataAdapter();

                ad = new SqlDataAdapter("select * from Email_Level_Master where Category='" + ddlCategory.SelectedValue + "'", con);
                ad.Fill(dt);
                ddlLevel.DataSource = dt;
                ddlLevel.DataTextField = "Email_Level";
                ddlLevel.DataValueField = "Email_Level_Id";
                ddlLevel.DataBind();
            }
            ddlLevel.Items.Insert(0, new ListItem("Select", ""));
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }
    }
}