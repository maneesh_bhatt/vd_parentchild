﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Configuration;
using MySql.Data.MySqlClient;

public partial class Tool_VDDepartmentPerformanceReport : System.Web.UI.Page
{
    public static string searchCondition;
    Image sortImage = new Image();
    private static string sortCondition;
    private static string sortColumn;
    public string SortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
                return string.Empty;
            else
                return ViewState["SortDirection"].ToString();
        }
        set
        {
            ViewState["SortDirection"] = value;
        }
    }
    private string _sortDirection;

    SqlConnection connection;
    SqlCommand cmd;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    /// <summary>
    /// Uploaded data is retrieved and based on the Target Date the if data is sent to Vd Finish Store or Assembly then it is considered as Received on time if not received on the 
    /// Target Date then Considered as Received Late
    /// </summary>
    public void showgrid()
    {
        try
        {
            if (inputFromDate.Text != "" || inputToDate.Text != "")
            {
                List<PartHistory> partList = GetDataByCriteria("Uploaded");

                int UploadQuantity = partList.Count;
                int LateReceivedData = partList.Sum(x => x.LateReceivedData);
                int TimelyReceivedData = partList.Sum(x => x.TimelyReceivedData);

                DataTable dt = new DataTable();
                dt.Columns.Add("UploadQuantity", typeof(int));
                dt.Columns.Add("TimelyReceivedData", typeof(int));
                dt.Columns.Add("LateReceivedData", typeof(int));

                DataRow dr;
                dr = dt.NewRow();
                dr["UploadQuantity"] = UploadQuantity;
                dr["TimelyReceivedData"] = TimelyReceivedData;
                dr["LateReceivedData"] = LateReceivedData;
                dt.Rows.Add(dr);
                girdVDOverAllPerformance.DataSource = dt;
                girdVDOverAllPerformance.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Time Interval or Select From Date or To Date');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

        }
    }

    /// <summary>
    /// Uploaded data is retrieved and based on the Target Date the if data is sent to Vd Finish Store or Assembly then it is considered as Received on time if not received on the 
    /// Target Date then Considered as Received Late
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    protected List<PartHistory> GetDataByCriteria(string type)
    {

        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        List<PartHistory> partList = new List<PartHistory>();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        SqlDataAdapter ad = new SqlDataAdapter();
        try
        {
            string searchCondition = "";

            if (inputFromDate.Text != "" && inputToDate.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,pgr.Target_Date)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,pgr.Target_Date)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,pgr.Target_Date)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,pgr.Target_Date)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
            }
            else if (inputFromDate.Text != "" && inputToDate.Text == "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,pgr.Target_Date)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,pgr.Target_Date)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                }
            }
            else if (inputFromDate.Text == "" && inputToDate.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,pgr.Target_Date)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,pgr.Target_Date)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
            }
            if (searchCondition != "")
            {
                ad = new SqlDataAdapter("select pgr.tool_no,pgr.p_no,pgr.part_name,pgr.station,pgr.rwk_no,pgr.req_qty, pgr.Target_Date from parts_GR pgr where pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchCondition + " and pgr.target_date is not null", connec);
            }
            else
            {
                ad = new SqlDataAdapter("select pgr.tool_no,pgr.p_no,pgr.part_name,pgr.station,pgr.rwk_no,pgr.req_qty, pgr.Target_Date from parts_GR pgr where pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and pgr.target_date is not null", connec);
            }
            DataTable dt = new DataTable();
            ad.Fill(dt);
            SqlCommand cmd = new SqlCommand();
            connec.Open();
            cmd.Connection = connec;
            List<PartHistory> ph = new List<PartHistory>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                PartHistory partHistory = new PartHistory();
                partHistory.ToolNo = Convert.ToInt32(dt.Rows[i]["tool_no"]);
                partHistory.PositionNumber = Convert.ToString(dt.Rows[i]["p_no"]);
                partHistory.PartName = Convert.ToString(dt.Rows[i]["part_name"]);
                partHistory.Station = Convert.ToString(dt.Rows[i]["station"]);
                partHistory.ReworkNumber = Convert.ToString(dt.Rows[i]["rwk_no"]);
                partHistory.UploadQuantity = Convert.ToInt32(dt.Rows[i]["req_qty"]);
                partHistory.PartsSentCount = 1;
                cmd = new SqlCommand("select top 1 * from part_history_Gr where sent_or_rec='Sent' and location in ('Assembly','VD Finish Store') and tool_no=@ToolNo and p_no=@PNo and part_name=@PartName and station=@Station and rwk_no=@RwkNo ", connec);
                cmd.Parameters.AddWithValue("@ToolNo", dt.Rows[i]["tool_no"]);
                cmd.Parameters.AddWithValue("@PNo", dt.Rows[i]["p_no"]);
                cmd.Parameters.AddWithValue("@PartName", dt.Rows[i]["part_name"]);
                cmd.Parameters.AddWithValue("@Station", dt.Rows[i]["station"]);
                cmd.Parameters.AddWithValue("@RwkNo", dt.Rows[i]["rwk_no"]);
                string targetDate = Convert.ToDateTime(dt.Rows[i]["target_date"]).ToString("dd-MM-yyyy");   
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        string dateOfTrans = Convert.ToDateTime(reader["date_of_trans"]).ToString("dd-MM-yyyy");
                        int days = Convert.ToDateTime(dateOfTrans).Subtract(Convert.ToDateTime(targetDate)).Days;
                        partHistory.SentDate = dateOfTrans;
                        //Received On Time
                        if (days <= 0)
                        {
                            if (Convert.ToInt32(dt.Rows[i]["req_qty"]) == Convert.ToInt32(reader["Quantity"]))
                            {
                                partHistory.TimelyReceivedData = 1;
                            }
                            else
                            {
                                partHistory.TimelyReceivedData = 0;
                                partHistory.LateReceivedData = 1;
                            }
                        }
                        else
                        {
                            partHistory.LateReceivedData = 1;
                        }
                    }
                    else
                    {
                        partHistory.LateReceivedData = 1;
                    }
                    if (type == "Uploaded" || (type == "OnTime" && partHistory.TimelyReceivedData > 0) || (type == "Late" && partHistory.LateReceivedData > 0))
                    {
                        partHistory.TargetDate = targetDate;
                        partList.Add(partHistory);
                    }
                    else
                    {
                        partHistory = null;
                    }

                }
            }
            return partList;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    /// <summary>
    /// When user clicks on the Count. Based on the Clicked Type the Data is displayed on the Pop Up
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void girdVDOverAllPerformance_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Uploaded")
        {
            int Count = Convert.ToInt32(e.CommandArgument);
            lblRecordType.Text = "Uploaded";
            girdVDItemWisePerformance.DataSource = GetDataByCriteria("Uploaded");
            girdVDItemWisePerformance.DataBind();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
        else if (e.CommandName == "OnTime")
        {
            int Count = Convert.ToInt32(e.CommandArgument);
            lblRecordType.Text = "OnTime";
            girdVDItemWisePerformance.DataSource = GetDataByCriteria("OnTime");
            girdVDItemWisePerformance.DataBind();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
        else if (e.CommandName == "Late")
        {
            int Count = Convert.ToInt32(e.CommandArgument);
            lblRecordType.Text = "Late";
            girdVDItemWisePerformance.DataSource = GetDataByCriteria("Late");
            girdVDItemWisePerformance.DataBind();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
    }

    /// <summary>
    /// Percentage is Calculated for the Timely and Late Received
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void girdVDOverAllPerformance_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            double uploadQuantity = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "UploadQuantity"));
            double totalOnTimeData = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "TimelyReceivedData"));
            double totalDelayedData = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "LateReceivedData"));
            Label lblOnTimeMovedDataPercent = (Label)e.Row.FindControl("lblOnTimeMovedData");
            Label lblDelayedData = (Label)e.Row.FindControl("lblDelayedData");


            if (totalOnTimeData > 0)
            {
                double onTimeMovedPercent = (totalOnTimeData * 100) / uploadQuantity;
                lblOnTimeMovedDataPercent.Text = Convert.ToString(Math.Round(onTimeMovedPercent, 1));
            }
            else {
                lblOnTimeMovedDataPercent.Text = "0";
            }
            if (totalDelayedData > 0)
            {
                double delayedPercent = (totalDelayedData * 100) / uploadQuantity;
                lblDelayedData.Text = Convert.ToString(Math.Round(delayedPercent, 1));
            }
            else {
                lblDelayedData.Text = "0";
            }
        }
    }

    protected void girdVDItemWisePerformance_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }


    protected void girdVDOverAllPerformance_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }


    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    /// <summary>
    /// Html is generated for the Passed Data Table
    /// </summary>
    /// <param name="table"></param>
    /// <returns></returns>
    protected string GetHtmlContent(DataTable table)
    {

        StringBuilder b = new StringBuilder();
        b.Append("<table style='");
        b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
        b.Append("<tr>");
        foreach (DataColumn column in table.Columns)
        {
            b.Append("<th>");
            b.Append(column.ColumnName);
            b.Append("</th>");
        }
        b.Append("</tr>");


        foreach (DataRow row in table.Rows)
        {
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<td>");
                b.Append(row[column.ColumnName]);
                b.Append("</td>");
            }
            b.Append("</tr>");
        }
        b.Append("</table>");
        return b.ToString();
    }

    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }


    protected void ExportToExcel()
    {
        Response.ClearContent();
        string fileName = "CommitmentDateData";
        string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        girdVDOverAllPerformance.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    protected void ApplySortingImage(string sortExpression)
    {
        int columnIndex = 0;
        foreach (DataControlFieldHeaderCell headerCell in girdVDItemWisePerformance.HeaderRow.Cells)
        {
            if (headerCell.ContainingField.SortExpression == sortExpression)
            {
                columnIndex = girdVDItemWisePerformance.HeaderRow.Cells.GetCellIndex(headerCell);
            }
        }
        girdVDItemWisePerformance.HeaderRow.Cells[columnIndex].Controls.Add(sortImage);
    }

    protected void SetSortDirection(string sortDirection)
    {
        if (sortDirection.ToLower() == "asc")
        {
            _sortDirection = "DESC";
            sortImage.ImageUrl = "../Images/view_sort_ascending.png";

        }
        else
        {
            _sortDirection = "ASC";
            sortImage.ImageUrl = "../Images/view_sort_descending.png";
        }
    }

    protected void girdVDItemWisePerformance_Sorting(object sender, GridViewSortEventArgs e)
    {
        // gridBagList.PageIndex = 0;
        SetSortDirection(SortDirection);
        sortCondition = e.SortExpression + " " + _sortDirection;
        sortColumn = e.SortExpression;

        if (string.IsNullOrEmpty(sortCondition))
        {
            sortCondition = "ToolNo ASC";
        }
        var result = GetDataByCriteria(lblRecordType.Text);
        string[] splitCondition = sortCondition.Split(' ');
        if (splitCondition[1].ToLower() == "asc")
        {
            result = result.OrderBy(r => r.GetType().GetProperty(splitCondition[0]).GetValue(r, null)).ToList();
        }
        else if (splitCondition[1].ToLower() == "desc")
        {
            result = result.OrderByDescending(r => r.GetType().GetProperty(splitCondition[0]).GetValue(r, null)).ToList();
        }
        girdVDItemWisePerformance.DataSource = result;
        girdVDItemWisePerformance.DataBind();

        SortDirection = _sortDirection;
        ApplySortingImage(e.SortExpression);

    }
}
