﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="WeldAssemblyConsumedItemSummary.aspx.cs" Inherits="Tool_WeldAssemblyConsumedItemSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 860px;
            height: 440px;
            top: 20%;
            left: 30%;
            margin-left: -185px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
            line-height: 28px;
            overflow-y: scroll;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .awbimagecontainer
        {
            float: right;
            width: 29%;
            margin-right: 12px;
            margin-top: 43px;
            border: 1px solid #2D89EF;
            height: 180px;
        }
        .web_dialog_info
        {
            display: none;
            position: fixed;
            width: 940px;
            height: 510px;
            top: 22%;
            left: 36%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="border: 1px solid Blue; min-height: 100px; padding: 9px;">
        <div>
            <div align="center">
                <span class="style13">Weld Parts Consumed</span>
            </div>
        </div>
        <div style="width: 100%; margin-bottom: 10px; margin-top: 20px; height: 36px;">
            <div style="font-size: 13px; width: 90px; float: left;">
                Tool No
                <br />
                <asp:TextBox ID="inputToolNo" CssClass="glowing-border" runat="server" Width="80px"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 85px; float: left;">
                Rework No
                <br />
                <asp:TextBox ID="inputReworkNo" CssClass="glowing-border" runat="server" Width="75px"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 85px; float: left;">
                Station
                <br />
                <asp:TextBox ID="inputStation" CssClass="glowing-border" runat="server" Width="75px"></asp:TextBox>
            </div>
            <div style="font-size: 13px; width: 90px; float: left;">
                Position No
                <br />
                <asp:TextBox ID="inputPNo" CssClass="glowing-border" runat="server" Width="80px"></asp:TextBox>
            </div>
            <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" ImageUrl="~/images/searchbtn.jpg"
                OnClick="btnSearch_Click" Style="margin-top: 9px;" />
        </div>
        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateGrid">
            <ContentTemplate>
                <asp:GridView runat="server" ID="gridWeldPartConsumed" BackColor="White" AutoGenerateColumns="false"
                    EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True" AllowPaging="true"
                    AllowSorting="false" Width="100%" BorderColor="#B8BABD" BorderStyle="None" OnPageIndexChanging="gridWeldPartConsumed_PageIndexChanging"
                    OnRowCommand="gridWeldPartConsumed_OnRowCommand" PageSize="20" BorderWidth="1px"
                    Style="font-size: 12px; font-family: Verdana; line-height: 26px;">
                    <HeaderStyle CssClass="tableheader" />
                    <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                    <Columns>
                        <asp:BoundField DataField="Tool_No" HeaderText="Tool" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="140px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Station" HeaderText="Station" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="120px" HeaderText="P No"
                            ItemStyle-Width="120px" HeaderStyle-ForeColor="White">
                            <ItemTemplate>
                                <asp:LinkButton ID="lblPositionNumber" CausesValidation="false" runat="server" Text='<%# Eval("p_no") %>'
                                    CommandArgument='<%# Eval("p_no") + ";" + Eval("tool_no") + ";" + Eval("station") + ";" + Eval("part_name") +";"+Eval("Rwk") %>'
                                    OnClick="lblPositionNumber_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="part_name" HeaderText="Part Name" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="250px" ItemStyle-Width="250px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Upload_Qty" HeaderText="Upload Qty" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Quant_Sent" HeaderText="Sent Qty" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Rwk" HeaderText="Rwk" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="Opr17" HeaderText="Surface Treatment" HeaderStyle-BackColor="#335599"
                            HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                    </Columns>
                    <FooterStyle Font-Bold="false" />
                    <PagerStyle BackColor="#335599" ForeColor="White" Font-Size="Large" HorizontalAlign="Center"
                        CssClass="gridview" />
                    <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#000" />
                    <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                </asp:GridView>
                <asp:Label runat="server" ID="lblRecorrdsCount" CssClass="recordsstatus" Width="850px"></asp:Label>
                <div id="overlay" runat="server" clientidmode="Static" class="web_dialog_overlay">
                </div>
                <div id="dialog" clientidmode="Static" runat="server" class="web_dialog">
                    <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                        <div style="float: left; width: 100%;">
                            <div class="web_dialog_title" style="width: 20%; float: left;">
                                Item History
                            </div>
                            <div class="web_dialog_title align_right">
                                <asp:LinkButton runat="server" ID="btnClosePopUp" ClientIDMode="Static" CausesValidation="false"
                                    OnClick="btnClosePopUp_Click">Close</asp:LinkButton>
                            </div>
                        </div>
                        <div runat="server" id="pnlHistoryPopUp">
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
    </div>
</asp:Content>
