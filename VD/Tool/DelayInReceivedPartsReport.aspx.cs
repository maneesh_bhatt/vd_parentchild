﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class Tool_DelayInReceivedPartsReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    /// <summary>
    /// Retrieves and binds the data for seleced from/to date from Stored Procedure. Displays the Summary in Percentage on the basis of Last Sent Date
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();

        try
        {
            if (inputFromDate.Text == "" && inputToDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please select From Date and To Date.');", true);
                return;
            }
            using (SqlConnection conn = new SqlConnection(sqlconnstring))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("usp_GetLocationWisePercentage", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FromDate", Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd"));
                    cmd.Parameters.AddWithValue("@ToDate", Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd"));
                    if (!string.IsNullOrEmpty(inputToolNo.Text))
                        cmd.Parameters.AddWithValue("@ToolNo", Convert.ToInt32(inputToolNo.Text));
                    gridViewVendorPendingItems.DataSource = cmd.ExecuteReader();
                    gridViewVendorPendingItems.DataBind();
                    gridViewVendorPendingItems.EmptyDataText = "No Records Found";
                }
            }

        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void ExportToExcel()
    {
        Response.ClearContent();
        string fileName = "DelayInReceivedParts_Report";
        string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gridViewVendorPendingItems.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    /// <summary>
    /// Retrieves and shows the Vendor Standard Days for each row
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewVendorPendingItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);
            try
            {
                string location = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "location"));
                if (location == "Inspection" || location == "K R Casting Pvt Ltd")
                {
                    location = " " + location;
                }
                Label lblStandardDays = (Label)e.Row.FindControl("lblStandardDays");
                conn.Open();
                SqlCommand cmd = new SqlCommand("select Standard_Days from Vendor_Master_Job_work where Vendor_Name=@Location", conn);
                cmd.Parameters.AddWithValue("@Location", location);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        lblStandardDays.Text = Convert.ToString(reader["Standard_Days"]);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured. Please Contact Administrator.');", true);
                return;
            }
            finally
            {
                conn.Close();
            }
        }
    }

    /// <summary>
    /// Show and hides the inner grid when user clicks on Plus/Minus Button shown in front of each row
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Show_Hide_ChallanItemsGrid(object sender, EventArgs e)
    {
        try
        {
            ImageButton imgShowHide = (sender as ImageButton);
            GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);

            Panel panel = (Panel)imgShowHide.FindControl("pnlChallanItems");
            GridView gvChallanItems = (GridView)imgShowHide.FindControl("gvChallanItems");

            if (imgShowHide.CommandArgument == "Show")
            {
                panel.Visible = true;
                imgShowHide.CommandArgument = "Hide";
                imgShowHide.ImageUrl = "~/images/minus.png";

                string location = gridViewVendorPendingItems.Rows[row.RowIndex].Cells[1].Text;
                ShowChallanItemsGrid(gvChallanItems, location);
            }
            else
            {
                panel.Visible = false;
                imgShowHide.CommandArgument = "Show";
                imgShowHide.ImageUrl = "~/images/plus.png";
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Displays the Position level summary for clicked Row(Vendor).
    /// </summary>
    /// <param name="gvChallanItems"></param>
    /// <param name="location"></param>
    protected void ShowChallanItemsGrid(GridView gvChallanItems, string location)
    {
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            string searchCondition = "";
            if (location == "Inspection" || location == "K R Casting Pvt Ltd")
            {
                location = " " + location + "";
            }
            searchCondition = "phg.location='" + location + "'";
            if (!string.IsNullOrEmpty(inputToolNo.Text))
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and phg.Tool_No='" + inputToolNo.Text + "'";
                }
                else
                {
                    searchCondition = " phg.Tool_No='" + inputToolNo.Text + "'";
                }
            }

            string query = "   select  phg.id,phg.tool_no, phg.part_name,phg.p_no,phg.station, phg.rwk_no, phg.Upload_Qty, phg.quantity, phg.sent_or_rec, "
                           + " phg.location,phg.date_of_trans as Received_Date, tbLastSentDate.date_of_trans as Sent_Date,tb.Delivery_Commit_Date as LastCommitDate,DateDiff(Day,Convert(Date,tbLastSentDate.date_of_trans), "
                           + " Convert(Date,phg.date_of_trans)) as Total_Days , vdm.Standard_Days, (DateDiff(Day,Convert(Date,tbLastSentDate.date_of_trans), "
                           + " Convert(Date,phg.date_of_trans))*100)/vdm.Standard_Days as Percentage, "
                           + " case when (DateDiff(Day,Convert(Date,tbLastSentDate.date_of_trans), Convert(Date,phg.date_of_trans))*100)/vdm.Standard_Days<=100 then 1 "
                           + " when ((DateDiff(Day,Convert(Date,tbLastSentDate.date_of_trans),Convert(Date,phg.date_of_trans))*100)/vdm.Standard_Days>100 and "
                           + " (DateDiff(Day,Convert(Date,tbLastSentDate.date_of_trans),Convert(Date,phg.date_of_trans))*100)/vdm.Standard_Days<=200) then 2 "
                           + "  when ((DateDiff(Day,Convert(Date,tbLastSentDate.date_of_trans),Convert(Date,phg.date_of_trans))*100)/vdm.Standard_Days>200 and "
                           + " (DateDiff(Day,Convert(Date,tbLastSentDate.date_of_trans),Convert(Date,phg.date_of_trans))*100)/vdm.Standard_Days<=300) then 3 "
                           + " when (DateDiff(Day,Convert(Date,tbLastSentDate.date_of_trans),Convert(Date,phg.date_of_trans))*100)/vdm.Standard_Days>300 then 4 end as Category "
                           + " from part_history_GR phg left join Vendor_Days_Master vdm on phg.location= vdm.Vendor_Name "
                           + " outer apply (select top 1 phig.delivery_commit_date from part_history_GR phig where phig.tool_no=phg.tool_no and phig.part_name=phg.part_name "
                           + " and phig.p_no= phg.p_no and phig.station= phg.station and phig.rwk_no=phg.rwk_no and phig.location= phg.location and phig.sent_or_rec='Sent' "
                           + " and phig.id< phg.id order by phig.id asc)tb "
                           + "  outer apply (select top 1 phig.date_of_trans from part_history_GR phig where phig.tool_no=phg.tool_no and phig.part_name=phg.part_name "
                           + " and phig.p_no= phg.p_no and phig.station= phg.station and phig.rwk_no=phg.rwk_no and phig.location= phg.location and phig.sent_or_rec='Sent' "
                           + " and phig.id< phg.id order by phig.id desc)tbLastSentDate "
                           + " where phg.tool_no is not null and sent_or_rec = 'Received' and vdm.Standard_Days>0  "
                           + " and phg.location not in ('VD Finish Store','Assembly','FinishPartStore','weld vd finish store','Weld Parts Consumed','Reject Parts') "
                           + " and phg.date_of_trans >= @FromDate and phg.date_of_trans<=@ToDate and " + searchCondition + "";
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(sqlconnstring))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@FromDate", Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd"));
                    cmd.Parameters.AddWithValue("@ToDate", Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd"));
                    gvChallanItems.DataSource = cmd.ExecuteReader();
                    gvChallanItems.DataBind();
                    gvChallanItems.EmptyDataText = "No Records Found";
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

    }

    /// <summary>
    /// Retrieves the Original Expected Delivery Date,Last Expected Delivery Date, Delay from Last Sent Date, Delay from First Expected Delivery Date for each row.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvChallanItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);
            try
            {
                string Positionno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "p_no"));
                string part_name = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "part_name"));
                string station = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "station"));
                string rwk_no = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "rwk_no"));
                string toolno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "tool_no"));
                string location = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "location"));
                conn.Open();
                string id = "";
                SqlCommand cmd = new SqlCommand("select top 1 id from part_history_Gr where p_no='" + Positionno + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk_no='" + rwk_no + "' and tool_no='" + toolno + "' and location='" + location + "'  and sent_or_rec='Sent' order by date_of_trans desc", conn);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        id = Convert.ToString(reader["id"]);
                    }
                }
                Label lblOriginalCommitDate = (Label)e.Row.FindControl("lblOriginalCommitDate");
                Label lblDelayFromSentDate = (Label)e.Row.FindControl("lblDelayFromOriginalSentDate");
                Label lblDelayFromFirstCommitDate = (Label)e.Row.FindControl("lblDelayFromFirstCommitDate");

                string query = "select  Tool_No,P_No,Part_Name,Station,Rework_No,Location,Commitment_Date  from CommitmentHistory  where  Part_History_Id=" + id + " union all select  Tool_No,P_No,Part_Name,Station,Rwk_No,Location,Delivery_commit_date  from part_history_Gr  where  id=" + id + " order by Commitment_Date asc";
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                DataTable table = new DataTable();
                da.Fill(table);
                if (table.Rows.Count > 0)
                {
                    if (table.Rows[0]["Commitment_Date"] != DBNull.Value)
                        lblOriginalCommitDate.Text = Convert.ToDateTime(table.Rows[0]["Commitment_Date"]).ToString("dd-MM-yyyy");
                }

                if (DataBinder.Eval(e.Row.DataItem, "Sent_Date") != DBNull.Value && DataBinder.Eval(e.Row.DataItem, "Received_Date") != DBNull.Value)
                {
                    string sentDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "Sent_Date")).ToString("dd-MM-yyyy");
                    string receivedDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "Received_Date")).ToString("dd-MM-yyyy");
                    int days = Convert.ToDateTime(receivedDate).Subtract(Convert.ToDateTime(sentDate)).Days;
                    if (days > 0)
                    {
                        lblDelayFromSentDate.Text = Convert.ToString(days);
                    }
                }
                if (!string.IsNullOrEmpty(lblOriginalCommitDate.Text) && DataBinder.Eval(e.Row.DataItem, "Received_Date") != DBNull.Value)
                {
                    string originalCommitDate = Convert.ToDateTime(lblOriginalCommitDate.Text).ToString("dd-MM-yyyy");
                    string receivedDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "Received_Date")).ToString("dd-MM-yyyy");
                    int days = Convert.ToDateTime(receivedDate).Subtract(Convert.ToDateTime(originalCommitDate)).Days;
                    if (days > 0)
                    {
                        lblDelayFromFirstCommitDate.Text = Convert.ToString(days);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured. Please Contact Administrator.');", true);
                return;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}