﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="VendorDaysMasterGrid.aspx.cs" Inherits="Tool_VendorDaysMasterGrid" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .style14
        {
            width: 1034px;
        }
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>
    <script language="javascript" type="text/javascript">

        $(function () {
            $('#<%=inputVendorName.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "VendorMasterJobWorkData.aspx/GetVendorName",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputVendorName.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputVendorName.ClientID%>').blur();

            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="border: 1px solid Blue; min-height: 100px; padding: 9px;">
        <div>
            <div align="center">
                <span class="style13">Vendor Days Master Grid</span>
            </div>
        </div>
        <div style="width: 100%; margin-bottom: 10px; margin-top: 20px;">
            <div style="font-size: 13px; float: left;">
                Vendor Name
                <br />
                <asp:TextBox runat="server" ID="inputVendorName" ClientIDMode="Static" CssClass="autocomplete glowing-border"
                    Style="width: 170px;" Placeholder=" Vendor Name"></asp:TextBox>
            </div>
            <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" ImageUrl="~/images/searchbtn.jpg"
                OnClick="btnSearch_Click" Style="margin-top: 14px; margin-left: 15px;" />
            <asp:HyperLink ID="HyperLink2" Style="background-color: #335599; float: right; color: White;
                padding: 5px; margin-top: 9px;" runat="server" NavigateUrl="~/Tool/UploadVendorDaysData.aspx">Upload Vendor Days</asp:HyperLink>
            &nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HyperLink1" Style="background-color: #335599; float: right; color: White;
                padding: 5px; margin-top: 9px;margin-right:10px;" runat="server" NavigateUrl="~/Tool/VendorDaysMasterForm.aspx">Add Vendor Days</asp:HyperLink>
                &nbsp;&nbsp;&nbsp;
        </div>
        <asp:GridView runat="server" ID="gridVendor" BackColor="White" AutoGenerateColumns="false"
            EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True" AllowPaging="true"
            AllowSorting="false" Width="100%" BorderColor="#B8BABD" BorderStyle="None" OnPageIndexChanging="gridVendor_PageIndexChanging"
            OnRowCommand="gridVendor_PageIndexChanging_OnRowCommand" PageSize="15" BorderWidth="1px"
            Style="font-size: 12px; font-family: Verdana; line-height: 26px;">
            <HeaderStyle CssClass="tableheader" />
            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
            <Columns>
                <asp:BoundField DataField="Vendor_Name" HeaderText="Vendor Name" HeaderStyle-Width="180px"
                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White" />
                <asp:BoundField DataField="Standard_Days" HeaderText="Standard Days" HeaderStyle-Width="200px"
                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White" />
                <asp:TemplateField HeaderText="" HeaderStyle-Width="80px" HeaderStyle-Font-Bold="false"
                    HeaderStyle-BackColor="#335599" HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" CommandName="Edit" CssClass="btnCustom" CommandArgument='<%#Eval("Vendor_Days_Master_Id") %>'
                            ID="lnkBtnEdit">Edit</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle Font-Bold="false" />
            <PagerStyle BackColor="#335599" ForeColor="White" Font-Size="Large" HorizontalAlign="Center"
                CssClass="gridview" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#000" />
            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
        </asp:GridView>
        <asp:Label runat="server" ID="lblRecorrdsCount" CssClass="recordsstatus" Width="850px"></asp:Label>
    </div>
</asp:Content>
