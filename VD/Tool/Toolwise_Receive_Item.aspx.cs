﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using System.Globalization;
using System.Text;

public partial class Toolwise_Receive_Item : System.Web.UI.Page
{
    string station = "";
    string approvername = "";
    string location, part_name, str_dt, s;
    int tool_no, quant_sent, quant_rec, new_quantity_rec, quant_pend;
    string p_no = "";
    SqlConnection conn;

    SqlCommand cmd;
    int countchk = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtchallanno.Visible = false;
            lblChallanno.Visible = false;
            btnprint.Visible = false;

            string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name <>'Assembly'  order by Vendor_Name asc ";
            string dataTextField = "VENDOR_NAME";
            string dataValueField = "ID";
            UtilityFunctions.bind_vendor(ddllocation, query, dataTextField, dataValueField);
        }
    }

    protected void Show_Hide_OrdersGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlOrders").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            string customerId = gvCustomers.DataKeys[row.RowIndex].Value.ToString();


            GridView gvOrders = row.FindControl("gvOrders") as GridView;
            BindOrders(customerId, gvOrders);
        }
        else
        {
            row.FindControl("pnlOrders").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    private void BindOrders(string toolNo, GridView gvOrders)
    {
        gvOrders.ToolTip = toolNo;
        DataTable dt = new DataTable();
        dt = UtilityFunctions.GetData(string.Format("select distinct lig.tool_no,isnull(lig.Challan_No,'No Challan') as Challan_No,lig.[location], sum((isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0))) as qtypending, CONVERT(date, lig.Sent_Received_Date) as Sent_Received_Date,lig.station,lig.Rwk,pgr.Is_Active_Record from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no group by lig.Challan_No,lig.tool_no,lig.[location], CONVERT(date, lig.Sent_Received_Date),lig.station,lig.Rwk,pgr.Is_Active_Record having   lig.tool_no ='" + toolNo + "'  and   sum((isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0)))>0  and lig.[location]<>'Assembly' and lig.[location]='" + ddllocation.SelectedItem.Text + "'  and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) "));
        gvOrders.DataSource = dt;
        Session["BindOrders"] = dt;
        gvOrders.DataBind();
    }


    protected void chkAllchild_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox imgShowHide = (sender as CheckBox);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);


        GridView Childgrid = (GridView)(row.Parent.Parent);
        CheckBox ChkBoxHeader = (CheckBox)Childgrid.HeaderRow.FindControl("chkAll1");
        Button btnRecieved = Childgrid.FooterRow.FindControl("btnRecieved") as Button;

        foreach (GridViewRow row1 in Childgrid.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row1.FindControl("chkAllchild");
            if (ChkBoxRows.Checked == true)
            {
                ChkBoxRows.Checked = true;

                countchk = countchk + 1;
                Childgrid.Columns[7].Visible = true;
                btnRecieved.Visible = true;
                //btnSend.Visible = true;
                //btnprint.Visible = true;
                EnableTextBox(Childgrid);
            }
            else
            {
                ChkBoxRows.Checked = false;
                int count = int.Parse(Childgrid.Rows.Count.ToString());
                for (int i = 0; i < count; i++)
                {
                    CheckBox cb = (CheckBox)Childgrid.Rows[i].Cells[0].FindControl("chkAllchild");
                    if (cb.Checked == true)
                    {
                        Childgrid.Columns[7].Visible = true;
                    }
                    else
                    {
                        TextBox TextBox2 = (TextBox)Childgrid.Rows[i].Cells[7].FindControl("TextBox2");
                        Childgrid.Columns[7].Visible = true;
                        btnRecieved.Visible = false;
                        TextBox2.Enabled = false;
                    }
                }
            }
        }

        if (countchk > 0)
        {
            Childgrid.Columns[7].Visible = true;
            Childgrid.Columns[7].Visible = true;
            btnRecieved.Visible = true;

        }
        else
        {
            btnRecieved.Visible = true;
            Childgrid.Columns[7].Visible = true;
            btnRecieved.Visible = false;

        }
    }

    protected void chkAll1_CheckedChanged1(object sender, EventArgs e)
    {


        CheckBox imgShowHide = (sender as CheckBox);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        GridView Childgrid = (GridView)(row.Parent.Parent);
        Button btnRecieved = Childgrid.FooterRow.FindControl("btnRecieved") as Button;

        foreach (GridViewRow row1 in Childgrid.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row1.FindControl("chkAllchild");
            if (imgShowHide.Checked == true)
            {
                ChkBoxRows.Checked = true;
                countchk = countchk + 1;
                EnableTextBox(Childgrid);
                //EnableTextBox();

            }
            else
            {
                imgShowHide.Checked = false;

                ChkBoxRows.Checked = false;
                int count = int.Parse(Childgrid.Rows.Count.ToString());
                for (int i = 0; i < count; i++)
                {
                    CheckBox cb = (CheckBox)Childgrid.Rows[i].Cells[0].FindControl("chkAllchild");
                    if (cb.Checked == true)
                    {
                        Childgrid.Columns[7].Visible = true;
                    }
                    else
                    {
                        TextBox TextBox2 = (TextBox)Childgrid.Rows[i].Cells[7].FindControl("TextBox2");
                        Childgrid.Columns[7].Visible = true;
                        btnRecieved.Visible = false;
                        TextBox2.Enabled = false;
                    }
                }
                //int count = int.Parse(gvOrders.Rows.Count.ToString());
            }

        }


        if (countchk > 0)
        {
            Childgrid.Columns[7].Visible = true;

            btnRecieved.Visible = true;

        }
        else
        {
            btnRecieved.Visible = true;
            //Childgrid.Columns[7].Visible = false;
            btnRecieved.Visible = false;


        }
    }



    protected void EnableTextBox(GridView GridView1)
    {
        int count = int.Parse(GridView1.Rows.Count.ToString());

        for (int i = 0; i < count; i++)
        {
            CheckBox cb = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkAllchild");

            Button btnRecieved = GridView1.FooterRow.FindControl("btnRecieved") as Button;

            if (cb.Checked == true)
            {
                btnRecieved.Visible = true;

                TextBox TextBox2 = (TextBox)GridView1.Rows[i].Cells[7].FindControl("TextBox2");
                Label Label6sentqty = (Label)GridView1.Rows[i].Cells[9].FindControl("Label6sentqty");
                if (Label6sentqty.Text.Length > 0)
                {


                    TextBox2.Enabled = true;
                }
                else
                {
                    //TextBox2.Text = "";
                }
                TextBox2.Enabled = true;

            }
            else
            {
                TextBox TextBox2 = (TextBox)GridView1.Rows[i].Cells[7].FindControl("TextBox2");
                //TextBox txtpodate = (TextBox)GridView1.Rows[i].Cells[9].FindControl("txtpodate");
                TextBox2.Enabled = false;
                //txtpodate.Enabled = false;
                btnRecieved.Visible = false;

            }
        }
    }

    protected void OnOrdersGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvOrders = (sender as GridView);
        gvOrders.PageIndex = e.NewPageIndex;
        DataTable dt = (DataTable)Session["BindOrders"];
        gvOrders.DataSource = dt;
        gvOrders.DataBind();
        //BindOrders(gvOrders.ToolTip, gvOrders);
    }

    protected void Show_Hide_ProductsGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        //int tool = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Value);


        GridViewRow Gv2Row = (GridViewRow)((ImageButton)sender).NamingContainer;
        GridView Childgrid = (GridView)(Gv2Row.Parent.Parent);
        GridViewRow Gv1Row = (GridViewRow)(Childgrid.NamingContainer);
        int b = Gv1Row.RowIndex;

        //ImageButton imgShowHide = (sender as ImageButton);
        //GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlProducts").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            //int orderId = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Value);
            GridView gvProducts = row.FindControl("gvProducts") as GridView;

            string Position_no = Childgrid.Rows[row.RowIndex].Cells[1].Text;
            //string partname = Childgrid.Rows[row.RowIndex].Cells[2].Text;
            //string vendor = Childgrid.Rows[row.RowIndex].Cells[3].Text;
            string station = Childgrid.Rows[row.RowIndex].Cells[3].Text;
            string tool = Childgrid.Rows[row.RowIndex].Cells[5].Text;

            //BindOrdersposition(Position_no, gvOrdersp, partname, vendor, station, tool);
            BindProducts(Position_no, gvProducts, station, tool);
        }
        else
        {
            row.FindControl("pnlProducts").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    private void BindProducts(string challanNo, GridView gvOrdersPosition, string station, string tool)
    {
        //gvOrdersPosition.ToolTip = orderId.ToString();
        gvOrdersPosition.DataSource = UtilityFunctions.GetData(string.Format(" select lig.tool_no,lig.station,lig.Challan_No,lig.Upload_Qty,lig.p_no,lig.part_name,lig.quant_sent,lig.quant_rec,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as SentQty,lig.Rwk from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where lig.Challan_No='" + challanNo + "' and lig.tool_no='" + tool + "' and lig.station='" + station + "'  and (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  order by lig.tool_no,lig.station asc "));
        gvOrdersPosition.DataBind();
    }



    protected void btnRecieved_Click(object sender, EventArgs e)
    {
        Button btnRecieved = (sender as Button);
        GridViewRow row = (btnRecieved.NamingContainer as GridViewRow);
        GridView Childgrid = (GridView)(row.Parent.Parent);

        GridView gvProducts = row.FindControl("gvProducts") as GridView;
        btnRecieved.Attributes.Add("onclick", " this.disabled = true; " + ClientScript.GetPostBackEventReference(btnRecieved, null) + ";");
        approvername = Session["UserID"].ToString();
        approvername = approvername.Replace("_", " ");

        string prefixStr = "CH";
        //string query = "SELECT MAX(CAST(REPLACE(Challan_No,'" + prefixStr + "','') AS INT))  FROM [dbo].[part_history_GR] where Challan_No like '" + prefixStr + "%'";
        //string query = "select top 1 Replace(Replace(Challan_No,'CH',''),'V_','') from Part_History_Gr where Challan_No is not null order by id Desc";              
        txtchallanno.Text = UtilityFunctions.gencode(prefixStr);

        int count = 0;
        int uploadqty1 = 0;
        object request_id = "";
        //int tool_no1, quantity1, Qtysent1, sent1;
        string sqlconnstring1 = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring1);
        SqlTransaction trans = null;

        if (txtReceivecdate.Text.Length > 0)
        {
            try
            {
                conn.Open();
                trans = conn.BeginTransaction();
                foreach (GridViewRow row1 in Childgrid.Rows)
                {
                    if (row1.RowType == DataControlRowType.DataRow)
                    {

                        DateTime str_dt1 = Convert.ToDateTime(txtReceivecdate.Text);
                        str_dt = string.Format("{0:yyyy-MM-dd}", str_dt1); // you can specify format 
                        Label quant_sent1 = row1.FindControl("Label5") as Label;
                        Label quant_rec1 = row1.FindControl("Label6") as Label;
                        TextBox new_quantity_rec1 = row1.FindControl("TextBox2") as TextBox;
                        Label station1 = row1.FindControl("Label2") as Label;
                        Label tool_no1 = row1.FindControl("Label1") as Label;
                        LinkButton POSITION = row1.FindControl("Label3") as LinkButton;
                        Label part_name1 = row1.FindControl("Label4") as Label;
                        System.Globalization.CultureInfo provider = CultureInfo.InvariantCulture;
                        Label quantity = row1.FindControl("Label8") as Label;
                        Label uploadqty = row1.FindControl("Label6Upload_Qty") as Label;
                        Label sent = row1.FindControl("Labelsent") as Label;
                        Label lblReworkNo = row1.FindControl("lblReworkNo") as Label;

                        quant_sent = Convert.ToInt32(quant_sent1.Text);
                        quant_rec = Convert.ToInt32(quant_rec1.Text);
                        uploadqty1 = Convert.ToInt32(uploadqty.Text);
                        tool_no = Convert.ToInt32(tool_no1.Text);
                        p_no = POSITION.Text;
                        station = station1.Text;
                        part_name = Convert.ToString(part_name1.Text);
                        //string location1 = ddlvendor.SelectedItem.Text;
                        location = ddllocation.SelectedItem.Text;
                        string kk = System.DateTime.Now.ToLongTimeString();
                        //str_dt2 = Convert.ToDateTime(str_dt1);
                        str_dt = str_dt + " " + kk;
                        if (ddllocation.SelectedItem.Text.Length > 0)
                        {
                            bool isSelected = (row1.FindControl("chkAllchild") as CheckBox).Checked;
                            if (isSelected == true)
                            {
                                if (new_quantity_rec1.Text.Length > 0)
                                {
                                    new_quantity_rec = Convert.ToInt32(new_quantity_rec1.Text);
                                    if ((quant_sent) >= new_quantity_rec + quant_rec)
                                    {
                                        s = "insert into part_history_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,TransSentQty,Upload_Qty,Challan_No,User_name,Used_Date,Rwk_No) values     (@p_no,@part_name,@tool_no,@station,@location,@date_of_trans,@sent_or_rec,@quantity,@TransSentQty,@Upload_Qty,@Challan_No,@user_name,@useddate,@RwkNo);SELECT SCOPE_IDENTITY()";
                                        cmd = new SqlCommand(s, conn, trans);
                                        cmd.Parameters.AddWithValue("@p_no", p_no);
                                        cmd.Parameters.AddWithValue("@part_name", part_name);
                                        cmd.Parameters.AddWithValue("@tool_no", tool_no);
                                        cmd.Parameters.AddWithValue("@station", station);
                                        cmd.Parameters.AddWithValue("@location", location);
                                        cmd.Parameters.AddWithValue("@date_of_trans", str_dt);
                                        cmd.Parameters.AddWithValue("@sent_or_rec", "Received");
                                        cmd.Parameters.AddWithValue("@quantity", new_quantity_rec);
                                        cmd.Parameters.AddWithValue("@TransSentQty", new_quantity_rec);
                                        cmd.Parameters.AddWithValue("@Upload_Qty", uploadqty1);
                                        cmd.Parameters.AddWithValue("@Challan_No", txtchallanno.Text);
                                        cmd.Parameters.AddWithValue("@user_name", approvername);
                                        cmd.Parameters.AddWithValue("@useddate", System.DateTime.Now);
                                        cmd.Parameters.AddWithValue("@RwkNo", lblReworkNo.Text);
                                        request_id = cmd.ExecuteScalar();
                                        Session["Requestid"] = request_id;
                                        Session["Challano"] = txtchallanno.Text;
                                        Session["Location"] = location;

                                        //UPDATING LOCATION INFO TABLE

                                        string strQuery3 = "update part_history_GR set Received_Date='" + str_dt + "' , quantity=isnull(quantity,0) + " + new_quantity_rec + ", QtySent=isnull(QtySent,0) - " + new_quantity_rec + "  where p_no='" + p_no + "' and part_name='" + part_name + "' and location='COMPANY' and station='" + station + "' ";
                                        SqlCommand cmd3 = new SqlCommand(strQuery3, conn, trans);
                                        cmd3.ExecuteNonQuery();

                                        //string strQuery4 = "update part_history_GR set TransSentQty= " + new_quantity_rec + "  where p_no='" + p_no + "' and part_name='" + part_name + "' and location='COMPANY' and station='" + station + "' and tool_no=" + tool_no + " ";
                                        //SqlCommand cmd4 = new SqlCommand(strQuery4, conn);
                                        //cmd4.ExecuteNonQuery();

                                        //1)UPDATING TOTAL QUANTITY RECEIVED,PENDING QUANTITY
                                        s = "select isnull(quant_rec,0) from part_history_GR where tool_no=@tool_no and station=@station and p_no=@p_no and  part_name=@part_name and location=@location";
                                        cmd = new SqlCommand(s, conn, trans);
                                        cmd.Parameters.AddWithValue("@p_no", p_no);
                                        cmd.Parameters.AddWithValue("@part_name", part_name);
                                        cmd.Parameters.AddWithValue("@tool_no", tool_no);
                                        cmd.Parameters.AddWithValue("@station", station);
                                        cmd.Parameters.AddWithValue("@location", location);

                                        quant_rec += Convert.ToInt32(cmd.ExecuteScalar());

                                        quant_pend = quant_sent - quant_rec;

                                        //2) NOW UPDATING location_info_GR TABLE

                                        string strQuery2 = "update location_info_GR  set  quant_rec=" + new_quantity_rec + " + isnull(quant_rec,0),Received_Date='" + str_dt1.ToString("yyyy/MM/dd HH:mm:ss") + "' where p_no='" + p_no + "' and station='" + station + "'  and location='" + location + "' and part_name='" + part_name + "' and tool_no=" + tool_no + " ";
                                        SqlCommand cmd2 = new SqlCommand(strQuery2, conn, trans);
                                        cmd2.ExecuteNonQuery();

                                        count = count + 1;
                                        //3)CHECKING IF PREVIOUS ROW WITH LOCATION=COMPANY EXISTS

                                        s = "select quant_rec from part_history_GR where tool_no=@tool_no and station=@station and p_no=@p_no and   part_name=@part_name and location=@location";
                                        cmd = new SqlCommand(s, conn, trans);
                                        cmd.Parameters.AddWithValue("@p_no", p_no);
                                        cmd.Parameters.AddWithValue("@part_name", part_name);
                                        cmd.Parameters.AddWithValue("@tool_no", tool_no);
                                        cmd.Parameters.AddWithValue("@station", station);
                                        cmd.Parameters.AddWithValue("@location", location);
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Receive Qty should not be greater than sent Qty');", true);
                                        btnRecieved.Enabled = true;
                                        return;
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Enter Receive Qty ');", true);
                                    btnRecieved.Enabled = true;
                                    return;
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Vendor');", true);
                            btnRecieved.Enabled = true;
                            return;
                        }
                    }
                }
                trans.Commit();
                showgrid();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
                ex.ToString();
            }
            finally
            {
                conn.Close();
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Enter Received Date');", true);
            btnRecieved.Enabled = true;
            return;
        }

        if (count > 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Receive Item Successfully ');", true);
            btnRecieved.Enabled = true;
            btnRecieved.Visible = false;
        }
        conn.Close();
    }

    [System.Web.Services.WebMethodAttribute(),
  System.Web.Script.Services.ScriptMethodAttribute()]
    public static string GetDynamicContent(string contextKey)
    {
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";

        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];


        string query = "select  id,location as Party, CONVERT(VARCHAR(10), date_of_trans, 103)  as Trans_Date,sent_or_rec as Trans_Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as PendingTotQty_in_PPC,TransSentQty as SendQty_Or_ReceivedQty  from part_history_GR  where   p_no='" + Positionno + "' and  tool_no='" + toolno + "' and  station='" + station + "' and  part_name='" + part_name + "' order by date_of_trans asc ";

        //string query = "SELECT UnitPrice, UnitsInStock, Description FROM Products WHERE ProductID = " + contextKey;

        SqlDataAdapter da = new SqlDataAdapter(query, conn);
        DataTable table = new DataTable();

        da.Fill(table);

        StringBuilder b = new StringBuilder();

        b.Append("<table style='background-color:#f3f3f3; border: #336699 3px solid; ");
        b.Append("width:350px; font-size:10pt; font-family:Verdana;' cellspacing='0' cellpadding='3'>");

        b.Append("<tr><td colspan='6' style='background-color:#336699;text-align:center; color:white;'>");
        b.Append("<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Summary Details</b>"); b.Append("</td></tr>");


        //b.Append("<tr><td style='width:120px;'><b>location</b></td>");
        //b.Append("<td style='width:120px;'><b>date_of_trans</b></td>");
        //b.Append("<td style='width:120px;'><b>Sent/Received</b></td>");
        //b.Append("<td style='width:120px;'><b>quantity</b></td>");
        //b.Append("<td style='width:120px;'><b>TransSentQty</b></td></tr>");

        b.Append("<tr>");
        foreach (DataColumn column in table.Columns)
        {
            b.Append("<th>");
            b.Append(column.ColumnName);
            b.Append("</th>");
        }
        b.Append("</tr>");


        foreach (DataRow row in table.Rows)
        {
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<td>");
                b.Append(row[column.ColumnName]);
                b.Append("</td>");
            }
            b.Append("</tr>");
        }


        //b.Append("<td>" + table.Rows[i]["location"].ToString() + "</td>");
        //b.Append("<td>" + table.Rows[i]["date_of_trans"].ToString() + "</td>");
        //b.Append("<td>" + table.Rows[i]["sent_or_rec"].ToString() + "</td>");
        //b.Append("<td>" + table.Rows[i]["quantity"].ToString() + "</td>");
        //b.Append("<td>" + table.Rows[i]["TransSentQty"].ToString() + "</td>");


        b.Append("</table>");


        conn.Close();

        return b.ToString();
    }

    protected void ddllocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddllocation.SelectedItem.Text.Length > 0)
        {
            txtchallanno.Visible = true;
            lblChallanno.Visible = true;
            btnprint.Visible = true;
        }
        else
        {
            btnprint.Visible = false;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    public void showgrid()
    {
        lbl.Text = "";
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            conn = new SqlConnection(sqlconnstring);
            conn.Open();

            if (txtprojectno.Text.Length > 0)
            {
                cmd = new SqlCommand("select temp.tool_no,sum(temp.SentQty) as qtypending,temp.location from (select lig.tool_no,lig.location,lig.quant_sent,lig.quant_rec ,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as SentQty from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where lig.location=@location and lig.tool_no=@tool_no and (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) ) temp group by temp.tool_no ,temp.location order by temp.tool_no  asc");
                cmd.Parameters.AddWithValue("@location", ddllocation.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@tool_no", txtprojectno.Text);
            }
            else
            {
                cmd = new SqlCommand("select temp.tool_no,sum(temp.SentQty) as qtypending,temp.location from (select lig.tool_no,lig.location,lig.quant_sent,lig.quant_rec ,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as SentQty from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where lig.location=@location and (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) ) temp group by temp.tool_no ,temp.location order by temp.tool_no  asc");
                cmd.Parameters.AddWithValue("@location", ddllocation.SelectedItem.Text);
            }
            
            cmd.Connection = conn;
            DataTable dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            if (dt.Rows.Count > 0)
            {
                gvCustomers.DataSource = dt;
                Session["DataTableAdvanceReport"] = dt;
                gvCustomers.DataBind();
            }
            else
            {
                dt = null;
                gvCustomers.DataSource = dt;
                Session["DataTableAdvanceReport"] = dt;
                gvCustomers.DataBind();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    protected void btnprint_Click(object sender, EventArgs e)
    {
       
        if (txtchallanno.Text.Length > 0)
        {
            Session["MIN"] = txtchallanno.Text;
            Response.Write("<script language=javascript>child=window.open('Item_Challan.aspx?ProcessType=Receive');</script>");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Challan No should not be blank');", true);
            return;
        }
    }
}