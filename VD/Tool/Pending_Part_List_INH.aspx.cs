﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class Pending_Part_List_INH : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            showgrid();
        }
    }

    public void showgrid()
    {
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);
            string searchCondition = "";
            if (tbTool.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.tool_no='" + tbTool.Text + "'";
                }
                else
                {
                    searchCondition = "lig.tool_no='" + tbTool.Text + "'";
                }
            }
            if (tbStation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.station='" + tbStation.Text + "'";
                }
                else
                {
                    searchCondition = " lig.station='" + tbStation.Text + "'";
                }
            }
            if (tbLocation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.location='" + tbLocation.Text + "'";
                }
                else
                {
                    searchCondition = " lig.location='" + tbLocation.Text + "'";
                }
            }
            
            if (string.IsNullOrEmpty(searchCondition))
            {
                searchCondition = "1=1";
            }
            string s = " select lig.tool_no,lig.Rework,lig.station,lig.p_no,lig.part_name,lig.req_qty,lig.Upload_Qty,lig.location,(isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0)) as qtypending,lig.quant_sent,lig.quant_rec,lig.quant_pend,lig.quant_others,lig.Sent_Received_Date,lig.Received_Date from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where (isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0))>0 and lig.location='INH' and  pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchCondition + "";
             
            SqlCommand cmd = new SqlCommand(s);
            cmd.CommandType = CommandType.Text;
            conn.Open();
            cmd.Connection = conn;
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            Session["DataTableAdvanceReport"] = dt;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            conn.Close();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        GridView1.PageIndex = e.NewPageIndex;
        DataTable table = (DataTable)Session["DataTableAdvanceReport"];
        GridView1.DataSource = table;
        GridView1.DataBind();
    }
    
    protected void GridView1_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label result = (Label)e.Row.FindControl("Label60");
            //string result = (e.Row.Cells[2].Text);
            if (result.Text == "01/01/1900 00:00:00 AM")
            {
                result.Text = "";
            }
            else
            {

            }
        }
    }
}