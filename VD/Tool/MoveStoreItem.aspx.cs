﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;
using System.Globalization;
using System.Reflection;

public partial class MoveStoreItem : System.Web.UI.Page
{
    String p_no = "";
    int tool_no, quantity, Qtysent;
    int countchk = 0;
    object Approveid = "";
    string str_dt1 = "";
    string part_name, station, location, str_dt;
    string location1 = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        txtpodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
        if (!IsPostBack)
        {
            if (txtprojectno.Text.Length > 0)
            {
                showgrid();
                Panel3.Visible = true;
                ddldepartment.Enabled = false;
            }
            ddldepartment.Enabled = false;
            Panel3.Visible = true;
            ImageButton1.Visible = false;
            Button1.Visible = true;
        }
    }

    /// <summary>
    /// Binds the Data which is there at Store Location, user can move this data further
    /// </summary>
    public void showgrid()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();

        clear();
        SqlDataAdapter da = new SqlDataAdapter();

        try
        {
            if (txtprojectno.Text.Length > 0 && txtstation.Text.Length > 0)
            {
                da = new SqlDataAdapter("select tool_no,station,Upload_Qty,p_no,part_name,quant_sent,quant_rec,isnull(quant_sent,0)-isnull(quant_rec,0) as SentQty,Rwk from location_info_GR where location=@location and tool_no=@tool_no and station=@station and (isnull(quant_sent,0)-isnull(quant_rec,0))>0 order by tool_no,station,p_no asc", conn);
                da.SelectCommand.Parameters.AddWithValue("@location", "Store");
                da.SelectCommand.Parameters.AddWithValue("@tool_no", txtprojectno.Text);
                da.SelectCommand.Parameters.AddWithValue("@station", txtstation.Text);
            }

            if (txtstation.Text.Length > 0 && txtprojectno.Text == "")
            {
                da = new SqlDataAdapter("select tool_no,station,Upload_Qty,p_no,part_name,quant_sent,quant_rec,isnull(quant_sent,0)-isnull(quant_rec,0) as SentQty,Rwk from location_info_gr where location=@location and station=@station and (isnull(quant_sent,0)-isnull(quant_rec,0))>0 order by tool_no,station,p_no asc", conn);
                da.SelectCommand.Parameters.AddWithValue("@location", "Store");
                da.SelectCommand.Parameters.AddWithValue("@station", txtstation.Text);
            }

            if (txtprojectno.Text.Length > 0 && txtstation.Text == "")
            {
                da = new SqlDataAdapter("select tool_no,station,Upload_Qty,p_no,part_name,quant_sent,quant_rec,isnull(quant_sent,0)-isnull(quant_rec,0) as SentQty,Rwk from location_info_gr where location=@location and tool_no=@tool_no and (isnull(quant_sent,0)-isnull(quant_rec,0))>0 order by tool_no,station,p_no asc", conn);
                da.SelectCommand.Parameters.AddWithValue("@location", "Store");
                da.SelectCommand.Parameters.AddWithValue("@tool_no", txtprojectno.Text);
            }
            if (txtprojectno.Text == "" && txtstation.Text == "")
            {
                da = new SqlDataAdapter("select tool_no,station,Upload_Qty,p_no,part_name,quant_sent,quant_rec,isnull(quant_sent,0)-isnull(quant_rec,0) as SentQty,Rwk from location_info_gr where location=@location and (isnull(quant_sent,0)-isnull(quant_rec,0))>0 order by tool_no,station,p_no asc", conn);
                da.SelectCommand.Parameters.AddWithValue("@location", "Store");
            }

            DataTable request_det_head = new DataTable();
            da.Fill(request_det_head);

            if (request_det_head.Rows.Count > 0)
            {
                ImageButton1.Visible = true;
                btnSend.Visible = false;
                btnprint.Visible = false;
                ddldepartment.Enabled = true;
                GridView1.DataSource = request_det_head;
                Session["DataTableAdvanceReport"] = request_det_head;
                GridView1.DataBind();
            }
            else
            {
                btnSend.Visible = false;
                btnprint.Visible = false;
                request_det_head = null;
                GridView1.DataSource = request_det_head;
                GridView1.DataBind();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    protected void chkAll1_CheckedChanged1(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)GridView1.HeaderRow.FindControl("chkAll1");
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
                countchk = countchk + 1;
                EnableTextBox();
                //EnableTextBox();
            }
            else
            {
                ChkBoxRows.Checked = false;
                int count = int.Parse(GridView1.Rows.Count.ToString());
                EnableTextBox();
            }
        }
        if (countchk > 0)
        {
            GridView1.Columns[10].Visible = true;
            GridView1.Columns[11].Visible = true;
            btnSend.Visible = true;
            btnprint.Visible = false;
        }
        else
        {
            btnprint.Visible = false;
            btnSend.Visible = false;
            GridView1.Columns[10].Visible = false;
            GridView1.Columns[11].Visible = false;
            //btnApproved.Visible = false;
            //btndissapprove.Visible = false;
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        showgrid();
        if (GridView1.Rows.Count == 1)
            lbl.Text = "No previous data exists..!!";
    }

    private void clear()
    {
        txtpodate.Text = "";
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        showgrid();
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        showgrid();
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlloc = (DropDownList)row.FindControl("ddlloc");
        
                string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work order by Vendor_Name asc ";
                string dataTextField = "VENDOR_NAME";
                string dataValueField = "ID";
                UtilityFunctions.bind_vendor(ddlloc, query, dataTextField, dataValueField);
            }
        }
    }

    protected void EnableTextBox()
    {
        int count = int.Parse(GridView1.Rows.Count.ToString());

        for (int i = 0; i < count; i++)
        {
            CheckBox cb = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkAllchild");
            if (cb.Checked == true)
            {
                btnSend.Visible = true;
                btnprint.Visible = false;
                TextBox TextBox3 = (TextBox)GridView1.Rows[i].Cells[9].FindControl("TextBox3");
                Label Label8 = (Label)GridView1.Rows[i].Cells[7].FindControl("Label8");
                Label Labelsent = (Label)GridView1.Rows[i].Cells[8].FindControl("Labelsent");
                TextBox3.Enabled = true;
            }
            else
            {
                TextBox TextBox3 = (TextBox)GridView1.Rows[i].Cells[9].FindControl("TextBox3");
                TextBox3.Enabled = false;
                btnprint.Visible = false;
                btnSend.Visible = false;
            }
        }
    }

    void load(GridViewUpdateEventArgs e)
    {
        p_no = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("Textbox1")).Text.ToString();
        part_name = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("Textbox2")).Text.ToString();
        tool_no = Convert.ToInt32(txtprojectno.Text.ToString());
        station = ((Label)GridView1.Rows[e.RowIndex].FindControl("Label10")).Text.ToString();

        DateTime str_dt1 = System.DateTime.Now;
        str_dt = string.Format("{0:yyyy-MM-dd}", str_dt1); // you can specify format 
        CultureInfo provider = CultureInfo.InvariantCulture;

        DateTime.ParseExact(str_dt, "yyyy-m-d", provider);

        quantity = Convert.ToInt32(((TextBox)GridView1.Rows[e.RowIndex].FindControl("Textbox4")).Text.ToString());
        Qtysent = Convert.ToInt32(((TextBox)GridView1.Rows[e.RowIndex].FindControl("txttobesent")).Text.ToString());
    }

    protected void TextBox3_TextChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            int count = int.Parse(GridView1.Rows.Count.ToString());
            for (int i = 0; i < count; i++)
            {
                TextBox TextBox3 = (TextBox)GridView1.Rows[i].Cells[9].FindControl("TextBox3");
                Label Label8 = (Label)GridView1.Rows[i].Cells[7].FindControl("Label8");
                Label Labelsent = (Label)GridView1.Rows[i].Cells[8].FindControl("Labelsent");
            }
        }
    }

    protected void chkAllchild_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)GridView1.HeaderRow.FindControl("chkAll1");

        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            if (ChkBoxRows.Checked == true)
            {
                countchk = countchk + 1;
                btnSend.Visible = true;
                btnprint.Visible = false;
                EnableTextBox();
            }
            else
            {
                ChkBoxRows.Checked = false;
                int count = int.Parse(GridView1.Rows.Count.ToString());
                for (int i = 0; i < count; i++)
                {
                    CheckBox cb = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkAllchild");
                    btnprint.Visible = false;
                    btnSend.Visible = true;
                }
            }
        }

        if (countchk > 0)
        {
            btnSend.Visible = true;
        }
        else
        {
            btnSend.Visible = false;
            btnprint.Visible = false;
        }
    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        DataTable table = (DataTable)Session["DataTableAdvanceReport"];
        GridView1.DataSource = table;
        GridView1.DataBind();
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        int count = 0;
        int tool_no1, quantity1, Qtysent1, sent1, uplqty;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        SqlTransaction trans = null;

        try
        {
            conn.Open();
            trans = conn.BeginTransaction();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Transaction = trans;
                cmd.Connection = conn;
                foreach (GridViewRow row in GridView1.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        string cmd2 = "";
                        Label p_no = row.FindControl("Label1") as Label;
                        Label part_name = row.FindControl("Label2") as Label;
                        Label lblproject = row.FindControl("lblproject") as Label;
                        Label station = row.FindControl("Label10") as Label;
                        Label lblbatchno = row.FindControl("lblbatchno") as Label;
                        Label LabelUpload_Qty = row.FindControl("LabelUpload_Qty") as Label;

                        //TextBox txtpodate = row.FindControl("txtpodate") as TextBox;1PCR0078-SC10007WR
                        str_dt1 = string.Format("{0:yyyy-MM-dd}", txtpodate.Text); // you can specify format 
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        int quant_rec = 0;
                        string kk = System.DateTime.Now.ToLongTimeString();
                        str_dt1 = str_dt1 + " " + kk;
                        Label Reworkno = row.FindControl("Label111") as Label;
                        //DateTime.ParseExact(str_dt, "yyyy-m-d", provider);

                        Label quantity = row.FindControl("Label8") as Label;
                        TextBox tobeQtysent = row.FindControl("TextBox3") as TextBox;
                        Label sent = row.FindControl("Labelsent") as Label;
                        
                        quantity1 = Convert.ToInt32(quantity.Text);
                        Qtysent1 = Convert.ToInt32(tobeQtysent.Text);
                        sent1 = Convert.ToInt32(sent.Text);
                        tool_no1 = Convert.ToInt32(lblproject.Text);
                        uplqty = Convert.ToInt32(LabelUpload_Qty.Text);


                        if (ddldepartment.SelectedItem.Text.Length > 0)
                        {

                            bool isValidData = false;
                            bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                            if (isSelected)
                            {
                                SqlCommand cmdCheckQuantity = new SqlCommand();
                                cmdCheckQuantity = new SqlCommand("select tool_no,station,Upload_Qty,p_no,part_name,quant_sent,quant_rec,isnull(quant_sent,0)-isnull(quant_rec,0) as SentQty,Rwk from location_info_GR where location='Store' and isnull(quant_sent,0)>0 and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and tool_no='" + tool_no1 + "' "
                                + " and station='" + station.Text + "' and rwk='" + Reworkno.Text + "'  order by tool_no,station asc", conn, trans);
                                using (SqlDataReader reader = cmdCheckQuantity.ExecuteReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        int quantSent = Convert.ToInt32(reader["quant_sent"]);
                                        int quantReceive = Convert.ToInt32(reader["quant_rec"]);
                                        int uploadQty = Convert.ToInt32(reader["upload_qty"]);
                                        int new_quantity_sent = Convert.ToInt32(tobeQtysent.Text);
                                        if (new_quantity_sent <= quantSent && new_quantity_sent <= uploadQty)
                                        {
                                            isValidData = true;
                                        }
                                        else
                                        {
                                            isValidData = false;
                                        }
                                    }
                                }

                                if (isValidData)
                                {
                                    cmdCheckQuantity = new SqlCommand("select tool_no,station,Upload_Qty,p_no,part_name,quant_sent,quant_rec,isnull(quant_sent,0)-isnull(quant_rec,0) as SentQty,Rwk from location_info_GR where location='" + ddldepartment.SelectedValue + "' and  location='" + ddldepartment.SelectedValue + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and tool_no='" + tool_no1 + "' " //(isnull(quant_sent,0)-isnull(quant_rec,0))>0 and
                                   + " and station='" + station.Text + "' and rwk='" + Reworkno.Text + "'  order by tool_no,station asc", conn, trans);
                                    using (SqlDataReader reader = cmdCheckQuantity.ExecuteReader())
                                    {
                                        if (reader.HasRows)
                                        {
                                            reader.Read();
                                            int quantSent = Convert.ToInt32(reader["quant_sent"]);
                                            int quantReceive = Convert.ToInt32(reader["quant_rec"]);
                                            int uploadQty = Convert.ToInt32(reader["upload_qty"]);
                                            int new_quantity_sent = Convert.ToInt32(tobeQtysent.Text);
                                            if (new_quantity_sent <= uploadQty)
                                            {
                                                isValidData = true;
                                            }
                                            else
                                            {
                                                isValidData = false;
                                            }
                                        }
                                        else
                                        {
                                            isValidData = true;
                                        }
                                    }
                                }
                            }

                            if (isSelected)
                            {
                                if (isValidData)
                                {
                                    Int32 qtysent1 = Convert.ToInt32(Qtysent1);
                                    if (ddldepartment.SelectedItem.Text == "VD Process Store")
                                    {

									SqlCommand cmd1 = new SqlCommand();


                                    string strQuery4 = "update part_history_gr  set quantity=isnull(quantity,0) + " + qtysent1 + ", QtySent=isnull(QtySent,0) - " + qtysent1 + " where location='STORE' and  p_no='" + p_no.Text + "'  and part_name='" + part_name.Text.Trim() + "' and station='" + station.Text + "' and tool_no=" + tool_no1 + "  and rwk_no='" + Reworkno.Text + "'";
                                    cmd1 = new SqlCommand(strQuery4, conn, trans);
                                    cmd1.ExecuteNonQuery();

                                    string s = "insert into part_history_gr(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,TransSentQty,Upload_Qty,rwk_no) values     (@p_no,@part_name,@tool_no,@station,@location,@date_of_trans,@sent_or_rec,@quantity,@QtySent,@TransSentQty,@Upload_Qty,@rwk_no)";
                                    cmd1.CommandText = s;
                                    
                                    cmd1.Parameters.AddWithValue("@p_no", p_no.Text);
                                    cmd1.Parameters.AddWithValue("@part_name", part_name.Text);
                                    cmd1.Parameters.AddWithValue("@tool_no", tool_no1);
                                    cmd1.Parameters.AddWithValue("@station", station.Text);
                                    cmd1.Parameters.AddWithValue("@location", ddldepartment.SelectedItem.Text);
                                    cmd1.Parameters.AddWithValue("@date_of_trans", str_dt1);
                                    cmd1.Parameters.AddWithValue("@sent_or_rec", "Received");
                                    cmd1.Parameters.AddWithValue("@quantity", qtysent1);
                                    cmd1.Parameters.AddWithValue("@QtySent", qtysent1);
                                    cmd1.Parameters.AddWithValue("@TransSentQty", qtysent1);
                                    cmd1.Parameters.AddWithValue("@Upload_Qty", uplqty);
                                    cmd1.Parameters.AddWithValue("@rwk_no", Reworkno.Text);
                                    cmd1.ExecuteNonQuery();

                                    s = "select quant_sent,quant_rec from location_info_gr where location='" + location1 + "' and tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "'  and rwk='" + Reworkno.Text + "'";
                                    cmd1.CommandText = s;

                                    DataTable dt = new DataTable();
                                    dt.Load(cmd1.ExecuteReader());

                                    if (dt.Rows.Count != 0)
                                    {
                                        cmd2 = "update";
                                        quantity1 += Convert.ToInt32(dt.Rows[0][0].ToString());
                                        quant_rec = Convert.ToInt32(dt.Rows[0][1].ToString());
                                    }

                                    if (cmd2.Equals("insert"))
                                    {
                                        s = "insert into location_info_gr(tool_no,station,p_no,part_name,req_qty,location,quant_sent,Sent_Received_Date,TransSentQty,Upload_Qty,Rwk) values(" + Convert.ToInt32(tool_no1) + ",'" + station.Text.ToString() + "'," + Convert.ToInt32(p_no.Text) + ",'" + part_name.Text.ToString() + "'," + Convert.ToInt32(qtysent1) + ",'" + ddldepartment.SelectedItem.Text.ToString() + "'," + Convert.ToInt32(qtysent1) + ",'" + str_dt1 + "'," + qtysent1 + "," + uplqty + ",'" + Reworkno.Text + "')";
                                    }
                                    else
                                    {
                                        s = "update location_info_gr set  TransSentQty=" + qtysent1 + ", quant_sent=isnull(quant_sent,0) - " + qtysent1 + " ,req_qty=isnull(req_qty,0) - " + qtysent1 + "   where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "'  and Rwk='" + Reworkno.Text + "'   and location='Store'";
                                    }
                                    cmd1.CommandText = s;
                                    cmd1.ExecuteNonQuery();
								}
                                else
                                {
                                    cmd2 = "insert";
                                    string s = "insert into part_history_gr(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,TransSentQty,Upload_Qty,rwk_no) values     (@p_no,@part_name,@tool_no,@station,@location,@date_of_trans,@sent_or_rec,@quantity,@QtySent,@TransSentQty,@Upload_Qty,@rwk_no)";

                                    SqlCommand cmd1 = new SqlCommand();
                                    cmd1.Connection = conn;
                                    cmd1.Transaction = trans;
                                    cmd1.CommandText = s;
                                    cmd1.Parameters.AddWithValue("@p_no", p_no.Text);
                                    cmd1.Parameters.AddWithValue("@part_name", part_name.Text);
                                    cmd1.Parameters.AddWithValue("@tool_no", tool_no1);
                                    cmd1.Parameters.AddWithValue("@station", station.Text);
                                    cmd1.Parameters.AddWithValue("@location", ddldepartment.SelectedItem.Text);
                                    cmd1.Parameters.AddWithValue("@date_of_trans", str_dt1);
                                    cmd1.Parameters.AddWithValue("@sent_or_rec", "Sent");
                                    cmd1.Parameters.AddWithValue("@quantity", qtysent1);
                                    cmd1.Parameters.AddWithValue("@QtySent", qtysent1);
                                    cmd1.Parameters.AddWithValue("@TransSentQty", qtysent1);
                                    cmd1.Parameters.AddWithValue("@Upload_Qty", uplqty);
                                    cmd1.Parameters.AddWithValue("@rwk_no", Reworkno.Text);
                                    cmd1.ExecuteNonQuery();

                                    s = "update location_info_gr set  TransSentQty=" + qtysent1 + ", quant_sent=isnull(quant_sent,0) - " + qtysent1 + " ,req_qty=isnull(req_qty,0) - " + qtysent1 + "   where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "'  and  Rwk='" + Reworkno.Text + "' and location='Store'";
                                    cmd1.CommandText = s;
                                    cmd1.ExecuteNonQuery();

                                    s = "select quant_sent,quant_rec from location_info_gr where location='" + ddldepartment.SelectedItem.Text.ToString() + "' and tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "'  and rwk='" + Reworkno.Text + "'";
                                    cmd1.CommandText = s;

                                    DataTable dt = new DataTable();
                                    dt.Load(cmd1.ExecuteReader());

                                    if (dt.Rows.Count != 0)
                                    {
                                        cmd2 = "update";
                                        quantity1 += Convert.ToInt32(dt.Rows[0][0].ToString());
                                        quant_rec = Convert.ToInt32(dt.Rows[0][1].ToString());
                                    }

                                    if (cmd2.Equals("insert"))
                                    {
                                        s = "insert into location_info_gr(tool_no,station,p_no,part_name,req_qty,location,quant_sent,Sent_Received_Date,TransSentQty,Upload_Qty,Rwk) values(" + Convert.ToInt32(tool_no1) + ",'" + station.Text.ToString() + "','" + p_no.Text + "','" + part_name.Text.ToString() + "'," + Convert.ToInt32(qtysent1) + ",'" + ddldepartment.SelectedItem.Text.ToString() + "'," + Convert.ToInt32(qtysent1) + ",'" + str_dt1 + "'," + qtysent1 + "," + uplqty + ",'" + Reworkno.Text + "')";
                                    }
                                    else
                                    {
                                        s = "update location_info_gr set   quant_sent=isnull(quant_sent,0) + " + qtysent1 + " ,req_qty=isnull(req_qty,0) - " + qtysent1 + "   where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and Rwk='" + Reworkno.Text + "' and part_name='" + part_name.Text + "' and location='" + ddldepartment.SelectedItem.Text.ToString() + "'";
                                    }
                                    cmd1.CommandText = s;
                                    cmd1.ExecuteNonQuery();
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Quantity Entered. Quantiy has been modified Please refresh the Page.');", true);
                                    return;
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Department');", true);
                            return;
                        }
                    }
                }
            }
            trans.Commit();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Items Sent Successfully');", true);
        }
        catch (Exception ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally 
        {
            conn.Close(); 
        }

        if (count > 0)
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Send Item Successfully');", true);
            btnSend.Visible = false;
            btnprint.Visible = false;
        }
        showgrid();
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        ExportGridView();
        string strRequest = @"~/foo/VDFINISHDATA.csv";
        //-- if something was passed to the file querystring
        //get absolute path of the file
        if (!string.IsNullOrEmpty(strRequest))
        {
            string path = Server.MapPath(strRequest);
            //get file object as FileInfo
            //string path = AppDomain.CurrentDomain.BaseDirectory + "AverageReport.csv";
            //System.IO.FileInfo file = new System.IO.FileInfo(@"C:\Inetpub\wwwroot\DisplayRecords\CSV\AdvanceReport.csv");
            System.IO.FileInfo file = new System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + "VDFINISHDATA.csv");
            //-- if the file exists on the server
            //set appropriate headers

            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(file.FullName);
                Response.End();
                //if file does not exist
            }
            else
            {
                //Response.Write("This file does not exist.");
            }
            //nothing in the URL as HTTP GET
        }
        else
        {
            //Response.Write("Please provide a file to download.");
        }
    }

    private void ExportGridView()
    {
        string path = AppDomain.CurrentDomain.BaseDirectory + "VDFINISHDATA.csv";
        // Create the CSV file to which grid data will be exported.
        //StreamWriter sw = new StreamWriter(@"C:\Inetpub\wwwroot\DisplayRecords\CSV\AdvanceReport.csv");

        //StreamWriter sw = new StreamWriter(@"D:\DisplayRecords\AverageReport.csv");
        StreamWriter sw = new StreamWriter(path);
        // First we will write the headers.

        DataTable dt = (DataTable)Session["DataTableAdvanceReport"];
        DataTable dtexport = new DataTable();

        int iColCount = dt.Columns.Count;
        for (int i = 0; i < iColCount; i++)
        {
            sw.Write(dt.Columns[i]);
            if (i < iColCount - 1)
            {
                sw.Write(",");
            }
        }
        sw.Write(sw.NewLine);
        // Now write all the rows.
        foreach (DataRow dr in dt.Rows)
        {
            for (int i = 0; i < iColCount; i++)
            {
                if (!Convert.IsDBNull(dr[i]))
                {
                    sw.Write(dr[i].ToString());
                }
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
        }
        sw.Close();
    }

    protected void txtpodate_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDateTime(txtpodate.Text) > DateTime.Now || Convert.ToDateTime(txtpodate.Text) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Can not take future/past date');", true);
            txtpodate.Text = "";
        }
    }
}