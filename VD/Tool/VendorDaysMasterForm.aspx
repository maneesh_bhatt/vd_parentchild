﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="VendorDaysMasterForm.aspx.cs" Inherits="Tool_VendorDaysMasterForm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <link href="../css/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <script src='<%=Page.ResolveClientUrl("~/js/jquery-1.7.js") %>' type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.9.2.min.js"></script>
    <link href="../css/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <script src="../css/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".fancybox").fancybox();
        });
    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .style14
        {
            width: 1034px;
        }
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            /* border: 1px solid rgba(158, 158, 158, 0.58);
            margin-left: 2px;
            height: 28px;*/
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        tr
        {
            height: 35px;
        }
    </style>
    <script type="text/javascript">


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <script language="javascript" type="text/javascript">

        $(function () {
            $('#<%=inputVendorName.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "VendorMasterJobWorkData.aspx/GetVendorName",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputVendorName.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputVendorName.ClientID%>').blur();

            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="pnlUserDetail" runat="server">
        <div>
            <div align="center">
                <span class="style13">Vendor Days Master Form</span>
            </div>
        </div>
        <div style="width: 100%; margin-bottom: 10px; margin-top: 20px;">
            <asp:HyperLink ID="HyperLink1" Style="background-color: #335599; float: right; color: White;
                padding: 5px; margin-top: -15px;" runat="server" NavigateUrl="~/Tool/VendorDaysMasterGrid.aspx">View All</asp:HyperLink>
        </div>
        <br />
        <hr />
        <table width="850px" align="center" cellpadding="0" cellspacing="0" style="margin-top: 5px;
            margin-left: 10px; margin-bottom: 5px;">
            <tr>
                <td width="850px">
                    <table align="center" cellpadding="0" cellspacing="0" style="width: 850px; height: 230px;">
                        <tr>
                            <td>
                                <asp:Label ID="LblResult" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Vendor Name <span class="style12">* </span>
                            </td>
                            <td align="left" colspan="2">
                                <asp:TextBox ID="inputVendorName" CssClass="glowing-border " runat="server" MaxLength="100"
                                    Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="inputVendorName"
                                    ErrorMessage="Enter Vendor Name" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Standard Days
                            </td>
                            <td>
                                <asp:TextBox ID="inputStandardDays" CssClass="glowing-border " runat="server" MaxLength="50"
                                    Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="left">
                                <asp:ImageButton ImageUrl="~/images/summit.jpg" ID="btnsave" OnClick="btnSubmit_Click"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <br />
    </asp:Panel>
</asp:Content>
