﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Pages/MasterPage.master"
    AutoEventWireup="true" CodeFile="Vendor_Master.aspx.cs" Inherits="Vendor_Master"
    Title="Add Vendor " %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <link href="../css/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <script src='<%=Page.ResolveClientUrl("~/js/jquery-1.7.js") %>' type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.9.2.min.js"></script>
    <link href="../css/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <script src="../css/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".fancybox").fancybox();
        });
    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .style14
        {
            width: 1034px;
        }
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        tr
        {
            height: 35px;
        }
    </style>
    <script type="text/javascript">


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="pnlUserDetail" runat="server">
        <div>
            <div align="center">
                <span class="style13">Add Job Work Vendor</span>
            </div>
        </div>
        <div style="width: 100%; margin-bottom: 10px; margin-top: 20px;">
            <asp:HyperLink ID="hypLnkView" Style="background-color: #335599; float: right; color: White;
                padding: 5px; margin-top: -15px;" runat="server" NavigateUrl="~/Tool/VendorMasterJobWorkData.aspx">View All</asp:HyperLink>
        </div>
        <br />
        <hr />
        <table width="850px" align="center" cellpadding="0" cellspacing="0" style="margin-top: 5px;
            margin-left: 10px; margin-bottom: 5px;">
            <tr>
                <td width="850px">
                    <table align="center" cellpadding="0" cellspacing="0" style="width: 850px; height: 230px;">
                        <tr>
                            <td>
                                <asp:Label ID="LblResult" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Vendor Name <span class="style12">* </span>
                            </td>
                            <td align="left" colspan="2">
                                <asp:TextBox ID="txtvendorname" CssClass="glowing-border " runat="server" MaxLength="100"
                                    Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtvendorname" ErrorMessage="Enter Vendor Name"
                                    ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Opration
                            </td>
                            <td>
                                <asp:ListBox ID="lstOperation" CssClass="glowing-border" Style="margin-bottom: 12px;"
                                    runat="server" SelectionMode="Multiple" Width="200px"></asp:ListBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Vendor Address <span class="style12">* </span>
                            </td>
                            <td class="style9">
                                <asp:TextBox ID="txtaddress" runat="server" CssClass="glowing-border " MaxLength="300"
                                    Width="400px" TextMode="MultiLine" Height="77px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtaddress"
                                    ErrorMessage="Enter Vendor Address" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td class="style13">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Vendor Type
                            </td>
                            <td>
                                <asp:DropDownList ID="inputVendorType" CssClass="glowing-border" AppendDataBoundItems="true"
                                    runat="server" MaxLength="50" Width="300px">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    <asp:ListItem Text="End Store" Value="End Store"></asp:ListItem>
                                    <asp:ListItem Text="External Vendor" Value="External Vendor"></asp:ListItem>
                                    <asp:ListItem Text="Internal Vendor" Value="Internal Vendor"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                City
                            </td>
                            <td>
                                <asp:TextBox ID="txtcity" CssClass="glowing-border " runat="server" MaxLength="50"
                                    Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                State
                            </td>
                            <td>
                                <asp:TextBox ID="txtstate" CssClass="glowing-border " runat="server" MaxLength="50"
                                    Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Pin Code
                            </td>
                            <td>
                                <asp:TextBox ID="txtpin" CssClass="glowing-border " runat="server" onkeypress="return isNumberKey(event)"
                                    MaxLength="50" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Phone
                            </td>
                            <td>
                                <asp:TextBox ID="txtphone" CssClass="glowing-border " runat="server" MaxLength="12"
                                    Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Email
                            </td>
                            <td>
                                <asp:TextBox ID="txtemail" CssClass="glowing-border " runat="server" MaxLength="50"
                                    Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server"
                                    ControlToValidate="txtemail" ErrorMessage="Enter Vendor Email" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Please Enter Correct Email"
                                    Display="Dynamic" ControlToValidate="txtemail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Contact Person <span class="style12">&nbsp;</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtcontactperson" CssClass="glowing-border " runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Is Active <span class="style12">&nbsp;</span>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkIsActive" runat="server" Width="200px"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Allocated To <span class="style12">&nbsp;</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtAllocatedTo" CssClass="glowing-border " runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Incharge Phone No <span class="style12">&nbsp;</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtInchagePhoneNo" CssClass="glowing-border " runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Standard Days <span class="style12">&nbsp;</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtStandardDays" CssClass="glowing-border " runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Item Pending Report Required <span class="style12">&nbsp;</span>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkItemPendingReportRequired" runat="server" Width="200px"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Name Required For Verification List <span class="style12">&nbsp;</span>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkNameRequiredForVerificationList" runat="server" Width="200px">
                                </asp:CheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                Frequency Of Verification <span class="style12">&nbsp;</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlFrequencyOfVerification" runat="server" Width="200px" AppendDataBoundItems="true">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Daily" Value="Daily"></asp:ListItem>
                                    <asp:ListItem Text="Weekly" Value="Weekly"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                         <tr>
                            <td class="style10">
                                GST No<span class="style12">&nbsp;</span>
                            </td>
                            <td>
                               <asp:TextBox ID="txtGSTNo" CssClass="glowing-border " runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="left">
                                <asp:ImageButton ImageUrl="~/images/summit.jpg" ID="btnsave" OnClick="btnSubmit_Click"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <br />
    </asp:Panel>
</asp:Content>
