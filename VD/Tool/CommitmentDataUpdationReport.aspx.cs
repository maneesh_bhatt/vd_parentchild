﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text;
using AjaxControlToolkit;

public partial class Tool_CommitmentDataUpdationReport : System.Web.UI.Page
{
    SqlConnection connection;
    SqlCommand cmd;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadVendors();
        }
    }

    /// <summary>
    /// Loads the List of Vendors from Vendor Master Job Work Table
    /// </summary>
    protected void LoadVendors() {

        string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ";
        string dataTextField = "VENDOR_NAME";
        string dataValueField = "ID";
        UtilityFunctions.bind_vendor(ddllocation, query, dataTextField, dataValueField);
    }

    /// <summary>
    /// Retrieves the Expected Delivery Date Change Summary for selected criteria. Displays the data for those positions for whom ExpectedDelivery Date has Changed
    /// </summary>
    public void showgrid()
    {
        try
        {
            string searchCondition = "";
            if (ddllocation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.location='" + ddllocation.SelectedItem.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.location='" + ddllocation.SelectedItem.Text + "'";
                }
            }
            if (inputToolNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.tool_no='" + inputToolNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.tool_no='" + inputToolNo.Text + "'";
                }
            }
            if (inputStation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.station='" + inputStation.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.station='" + inputStation.Text + "'";
                }
            }
            if (inputPNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.p_no='" + inputPNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.p_no='" + inputPNo.Text + "'";
                }
            }
            if (inputReworkNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.rwk='" + inputReworkNo.Text + "'";
                }
                else
                {
                    searchCondition = "  lig.rwk='" + inputReworkNo.Text + "'";
                }
            }
            if (ddlDateInterval.SelectedValue != "")
            {
                int month = Convert.ToInt32(ddlDateInterval.SelectedValue);
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,tb.Delivery_commit_date)>='" + Convert.ToDateTime(DateTime.Now.AddMonths(-month)).ToString("yyyy/MM/dd") + "' and Convert(Date,tb.Delivery_commit_date)<='" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,tb.Delivery_commit_date)>='" + Convert.ToDateTime(DateTime.Now.AddMonths(-month)).ToString("yyyy/MM/dd") + "' and Convert(Date,tb.Delivery_commit_date)<='" + Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd") + "'";
                }
            }
            else
            {
                if (inputFromDate.Text != "" && inputToDate.Text != "")
                {
                    if (searchCondition != "")
                    {
                        searchCondition = searchCondition + " and Convert(Date,tb.Delivery_commit_date)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,tb.Delivery_commit_date)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                    else
                    {
                        searchCondition = " Convert(Date,tb.Delivery_commit_date)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,tb.Delivery_commit_date)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                }
                else if (inputFromDate.Text != "" && inputToDate.Text == "")
                {
                    if (searchCondition != "")
                    {
                        searchCondition = searchCondition + " and Convert(Date,tb.Delivery_commit_date)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                    else
                    {
                        searchCondition = " Convert(Date,tb.Delivery_commit_date)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                }
                else if (inputFromDate.Text == "" && inputToDate.Text != "")
                {
                    if (searchCondition != "")
                    {
                        searchCondition = searchCondition + " and Convert(Date,tb.Delivery_commit_date)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                    else
                    {
                        searchCondition = " Convert(Date,tb.Delivery_commit_date)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                }
            }


            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            connection = new SqlConnection(sqlconnstring);
            if (ddlDateInterval.SelectedValue != "" || (inputFromDate.Text != "" || inputToDate.Text != ""))
            {
                connection.Open();
                string query = "";
                if (!string.IsNullOrEmpty(searchCondition))
                {
                    query = "select  lig.tool_no,lig.p_no,lig.part_name,lig.station,lig.Rwk ,lig.location,isnull(lig.quant_sent,0) as SentQuantity, isnull(lig.quant_rec,0) as ReceivedQuantity,lig.Upload_Qty,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as PendingQty,tb.Delivery_commit_date, tb.Delivery_Commit_Updated_Date, tb.Part_History_Id  from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no outer apply (select top 1 pg.Delivery_commit_date, pg.Delivery_Commit_Updated_Date,pg.id as Part_History_Id from part_history_GR pg where pg.tool_no=lig.tool_no and pg.p_no=lig.p_no and pg.part_name=lig.part_name and pg.station=lig.station and pg.rwk_no= lig.Rwk and pg.sent_or_rec='Sent' and pg.location=lig.location order by pg.date_of_trans desc)tb where pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchCondition + " and lig.location not in ('Assembly','VD Finish Store','Store Assembly','Spare Store') order by lig.tool_no, lig.station asc";
                }
                else
                {
                    query = "select  lig.tool_no,lig.p_no,lig.part_name,lig.station,lig.Rwk ,lig.location,isnull(lig.quant_sent,0) as SentQuantity, isnull(lig.quant_rec,0) as ReceivedQuantity,lig.Upload_Qty,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as PendingQty,tb.Delivery_commit_date, tb.Delivery_Commit_Updated_Date, tb.Part_History_Id  from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no outer apply (select top 1 pg.Delivery_commit_date, pg.Delivery_Commit_Updated_Date,pg.id as Part_History_Id from part_history_GR pg where pg.tool_no=lig.tool_no and pg.p_no=lig.p_no and pg.part_name=lig.part_name and pg.station=lig.station and pg.rwk_no= lig.Rwk and pg.sent_or_rec='Sent' and pg.location=lig.location order by pg.date_of_trans desc)tb where pgr.Is_Active_Record=1 and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and lig.location not in ('Assembly','VD Finish Store','Store Assembly','Spare Store') order by lig.tool_no, lig.station asc";
                }
                SqlDataAdapter ad = new SqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                ad.Fill(dt);
                DataTable dtable = new DataTable();

                dtable.Columns.Add("tool_no", typeof(int));
                dtable.Columns.Add("p_no", typeof(string));
                dtable.Columns.Add("part_name", typeof(string));
                dtable.Columns.Add("station", typeof(string));
                dtable.Columns.Add("Rwk", typeof(string));
                dtable.Columns.Add("location", typeof(string));
                dtable.Columns.Add("SentQuantity", typeof(int));
                dtable.Columns.Add("ReceivedQuantity", typeof(int));
                dtable.Columns.Add("Upload_Qty", typeof(int));
                dtable.Columns.Add("PendingQty", typeof(int));
                dtable.Columns.Add("DeliveryCommitDate", typeof(DateTime));
                dtable.Columns.Add("CommitmentChangeCount", typeof(int));

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bool isCommitmentDateChanged = false;
                    int commitmentDateChangeCount = 0;
                    if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Part_History_Id"])))
                    {
                        cmd.CommandText = "select Count(CommitmentHistoryId) as RecordsCount  from CommitmentHistory  where  Part_History_Id=" + Convert.ToString(dt.Rows[i]["Part_History_Id"]) + "";
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                reader.Read();
                                if (Convert.ToInt32(reader["RecordsCount"]) > 0)
                                {
                                    commitmentDateChangeCount = Convert.ToInt32(reader["RecordsCount"]);
                                    isCommitmentDateChanged = true;
                                }
                            }
                        }
                    }


                    if (isCommitmentDateChanged)
                    {
                        DataRow drCommitmentData = dtable.NewRow();
                        drCommitmentData["tool_no"] = dt.Rows[i]["tool_no"];
                        drCommitmentData["station"] = dt.Rows[i]["station"];
                        drCommitmentData["part_name"] = dt.Rows[i]["part_name"];
                        drCommitmentData["p_no"] = dt.Rows[i]["p_no"];
                        drCommitmentData["Rwk"] = dt.Rows[i]["Rwk"];
                        drCommitmentData["location"] = dt.Rows[i]["location"];
                        drCommitmentData["SentQuantity"] = dt.Rows[i]["SentQuantity"];
                        drCommitmentData["ReceivedQuantity"] = dt.Rows[i]["ReceivedQuantity"];
                        drCommitmentData["Upload_Qty"] = dt.Rows[i]["Upload_Qty"];
                        drCommitmentData["PendingQty"] = dt.Rows[i]["PendingQty"];
                        drCommitmentData["DeliveryCommitDate"] = dt.Rows[i]["Delivery_commit_date"];
                        drCommitmentData["CommitmentChangeCount"] = commitmentDateChangeCount;

                        dtable.Rows.Add(drCommitmentData);
                    }
                }

                if (dtable.Rows.Count > 0)
                {
                    GridView1.DataSource = dtable;
                    Session["DataTableAdvanceReport"] = dtable;
                    GridView1.DataBind();
                }
                else
                {
                    dt = null;
                    GridView1.DataSource = dtable;
                    Session["DataTableAdvanceReport"] = dtable;
                    GridView1.DataBind();
                }
                if (GridView1.Rows.Count > 0)
                {
                    btnDownload.Visible = true;
                }
                else
                {
                    btnDownload.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Time Interval or Select From Date or To Date');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            connection.Close();
        }
    } 

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        showgrid();
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        showgrid();
    }
     

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// Generates the HTML for the Passed DataTable as Parameter. This method is used to show the History on Button click,
    /// Like- Part History On Button click, Commitment Date Change History On Change Count Click
    /// </summary>
    /// <param name="table">DataTable is passed as Parameter</param>
    /// <returns></returns>
    protected string GetHtmlContent(DataTable table)
    {

        StringBuilder b = new StringBuilder();
        b.Append("<table style='");
        b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
        b.Append("<tr>");
        foreach (DataColumn column in table.Columns)
        {
            b.Append("<th>");
            b.Append(column.ColumnName);
            b.Append("</th>");
        }
        b.Append("</tr>");


        foreach (DataRow row in table.Rows)
        {
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<td>");
                b.Append(row[column.ColumnName]);
                b.Append("</td>");
            }
            b.Append("</tr>");
        }
        b.Append("</table>");
        return b.ToString();
    }
    
    /// <summary>
    /// Displays the Sent/Received History of Position on Position link click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Label3_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        try
        {
            string query = "select  id,location as Party,date_of_trans as Trans_Date,sent_or_rec as Trans_Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as Pending_Qty_PPC,TransSentQty as Send_Or_Rec_Qty,User_Name  from part_history_GR  where   p_no='" + Positionno + "' and  tool_no='" + toolno + "' and  station='" + station + "' and  part_name='" + part_name + "' and rwk_no='" + rwk_no + "' order by date_of_trans asc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();
            da.Fill(table);
            pnlHistoryPopUp.InnerHtml = GetHtmlContent(table);
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    /// <summary>
    /// Displays the History of Changed Expected Delivery Date on Expected Delivery Changed Count Click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkBtnLastCommitmentDate_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";
        string location = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        location = List[5];
        try
        {
            string id = "";
            SqlCommand cmd = new SqlCommand("select top 1 id from part_history_Gr where p_no='" + Positionno + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk_no='" + rwk_no + "' and tool_no='" + toolno + "' and location='" + location + "'  and sent_or_rec='Sent' order by date_of_trans desc", conn);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    reader.Read();
                    id = Convert.ToString(reader["id"]);
                }
            }

            string query = "select  Tool_No,P_No,Part_Name,Station,Rework_No,Location,Commitment_Date as Expected_Delivery_Date  from CommitmentHistory  where  Part_History_Id=" + id + " union all select  Tool_No,P_No,Part_Name,Station,Rwk_No,Location,Delivery_commit_date  from part_history_Gr  where  id=" + id + " order by Commitment_Date desc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();
            da.Fill(table);
            pnlCommitmentHistory.InnerHtml = GetHtmlContent(table);
            dialogCommitmentHistory.Attributes.Add("style", "display:block;");
            overlayCommitmentHistory.Attributes.Add("style", "display:block;");
        }
        catch (Exception ex)
        {
            ex.ToString();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured. Please Contact Administrator.');", true);
            return;
        }
        finally
        {
            conn.Close();
        }
    }


    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }

    protected void btnCloseCommitmentHistory_Click(object sender, EventArgs e)
    {
        dialogCommitmentHistory.Attributes.Add("style", "display:none;");
        overlayCommitmentHistory.Attributes.Add("style", "display:none;");
    }

    /// <summary>
    /// Exports the Data To Excel
    /// </summary>
    protected void ExportToExcel()
    {
        Response.ClearContent();
        string fileName = "CommitmentDateData";
        string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GridView1.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

}
