﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="VDDepartmentPerformanceReport.aspx.cs" Inherits="Tool_VDDepartmentPerformanceReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 1000px;
            height: 500px;
            top: 16%;
            left: 25%;
            margin-left: -185px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 9pt;
            line-height: 28px;
            overflow-y: scroll;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .awbimagecontainer
        {
            float: right;
            width: 29%;
            margin-right: 12px;
            margin-top: 43px;
            border: 1px solid #2D89EF;
            height: 180px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="width: 100%; border: 1px solid Blue; min-height: 100px;">
    <div style=" padding: 9px;">
        <div>
            <div align="center">
                <span class="style13">VD Department Performance Report</span>
            </div>
        </div>
        <div style="width: 100%; margin-bottom: 10px; margin-top: 20px;">
            <div style="font-size: 13px; width: 95px; float: left;">
                Start Date
                <br />
                <asp:TextBox ID="inputFromDate" CssClass="glowing-border" runat="server" Width="85px"></asp:TextBox>
                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                    Enabled="true" TargetControlID="inputFromDate" OnClientShowing="CurrentDateShowing">
                </cc1:CalendarExtender>
            </div>
            <div style="font-size: 13px; width: 95px; float: left;">
                End Date
                <br />
                <asp:TextBox ID="inputToDate" CssClass="glowing-border" runat="server" Width="85px"></asp:TextBox>
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="true" Format="dd-MMM-yyyy"
                    TargetControlID="inputToDate" OnClientShowing="CurrentDateShowing">
                </cc1:CalendarExtender>
            </div>
            <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/images/searchbtn.jpg"
                OnClick="btnSearch_Click" Style="margin-top: 10px;" />
            <%-- <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="btnDownload_Click"
                Visible="false" Style="margin-top: 9px; margin-right: 2px; float: right; background-color: green;
                color: white; padding: 6px; border: 1px solid green; box-shadow: 2px 3px 2px green;" />--%>
        </div>
        <asp:GridView ID="girdVDOverAllPerformance" runat="server" EmptyDataText="No Record Found"
            AutoGenerateColumns="False" OnRowDataBound="girdVDOverAllPerformance_RowDataBound"
            OnRowCommand="girdVDOverAllPerformance_RowCommand" AllowPaging="false" AllowSorting="false"
            Style="font-size: 13px; font-family: Verdana; line-height: 26px;" Width="100%">
            <RowStyle BackColor="White" ForeColor="Black" />
            <Columns>
                <asp:TemplateField HeaderText="Upload Parts Count" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="40px" HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lnkbtnViewUploaded" Text='<%#Eval("UploadQuantity") %>'
                            CommandName="Uploaded" CommandArgument='<%#Eval("UploadQuantity") %>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Moved On Time Count" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="40px" HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lnkbtnViewMovedOnTime" Text='<%#Eval("TimelyReceivedData") %>'
                            CommandName="OnTime" CommandArgument='<%#Eval("TimelyReceivedData") %>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delayed Parts Count" HeaderStyle-BackColor="#335599"
                    HeaderStyle-Width="40px" HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lnkbtnViewLateReceived" Text='<%#Eval("LateReceivedData") %>'
                            CommandName="Late" CommandArgument='<%#Eval("LateReceivedData") %>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="OnTime %" HeaderStyle-BackColor="#335599" HeaderStyle-Width="70px"
                    HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblOnTimeMovedData"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delayed %" HeaderStyle-BackColor="#335599" HeaderStyle-Width="70px"
                    HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblDelayedData"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div>
        <div id="overlay" runat="server" clientidmode="Static" class="web_dialog_overlay">
        </div>
        <div id="dialog" clientidmode="Static" runat="server" class="web_dialog">
            <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                <div style="float: left; width: 100%;">
                    <div class="web_dialog_title" style="width: 20%; float: left; margin-bottom: 10px;">
                        Part Summary
                    </div>
                    <div class="web_dialog_title align_right">
                        <asp:LinkButton runat="server" ID="btnClosePopUp" ClientIDMode="Static" OnClick="btnClosePopUp_Click">Close</asp:LinkButton>
                    </div>
                </div>
                <div style="margin: 16px; float: left;">
                    <h3 style="margin: 0px;">
                        <asp:Label runat="server" ID="lblRecordType"></asp:Label></h3>
                    <asp:GridView ID="girdVDItemWisePerformance" runat="server" EmptyDataText="No Record Found"
                        AutoGenerateColumns="False" OnRowDataBound="girdVDItemWisePerformance_RowDataBound"
                        AllowSorting="true" OnSorting="girdVDItemWisePerformance_Sorting" AllowPaging="false">
                        <RowStyle BackColor="White" ForeColor="Black" />
                        <Columns>
                            <asp:BoundField HeaderText="Tool" DataField="ToolNo" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="90px" HeaderStyle-ForeColor="White" SortExpression="ToolNo" />
                            <asp:BoundField HeaderText="P No" DataField="PositionNumber" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="90px" HeaderStyle-ForeColor="White" SortExpression="PositionNumber" />
                            <asp:BoundField HeaderText="Part Name" DataField="PartName" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="160px" HeaderStyle-ForeColor="White" SortExpression="PartName" />
                            <asp:BoundField HeaderText="Station" DataField="Station" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" SortExpression="Station" />
                            <asp:BoundField HeaderText="Rwk No" DataField="ReworkNumber" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="90px" HeaderStyle-ForeColor="White" SortExpression="ReworkNumber" />
                            <asp:BoundField HeaderText="Upload Quantity" DataField="UploadQuantity" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" SortExpression="UploadQuantity" />
                            <asp:BoundField HeaderText="Target Date" DataField="TargetDate" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" SortExpression="TargetDate"
                                DataFormatString="{0:dd-MM-yyyy}" />
                            <asp:BoundField HeaderText="Sent Date" DataField="SentDate" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" SortExpression="SentDate"
                                DataFormatString="{0:dd-MM-yyyy}" />
                            <asp:BoundField HeaderText="Moved On Time" DataField="TimelyReceivedData" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" SortExpression="TimelyReceivedData" />
                            <asp:BoundField HeaderText="Delayed Part" DataField="LateReceivedData" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" SortExpression="LateReceivedData" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>

