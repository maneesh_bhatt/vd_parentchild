﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Reflection;
using MySql.Data.MySqlClient;


public partial class Tool_Print_Previou_Challan : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //bind_Sent_challan();
            //bind_Received_challan();
            //bind_QC_challan();


            ddlReceivedchallan.Enabled = false;
            DDLSentchallan.Enabled = false;
            ddlvendor.Enabled = false;
            ddlsheet.Enabled = false;
            bind_vendor();
            bind_sheet();

        }
    }
    protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
    {
        if (RadioButton2.Checked == true)
        {
            lblsentchallan.Visible = true;
            DDLSentchallan.Enabled = false;
            lblQCchallan.Visible = true;
            lblReceivedchallan.Visible = true;
            ddlReceivedchallan.Enabled = true;
            RadioButton1.Checked = false;
            RadioButton3.Checked = false;
            ddlvendor.Enabled = true;
            ddlsheet.SelectedIndex = 0;
        }
    }
    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        if (RadioButton1.Checked == true)
        {
            lblReceivedchallan.Visible = true;
            ddlReceivedchallan.Enabled = false;
            lblsentchallan.Visible = true;
            DDLSentchallan.Enabled = true;
            RadioButton2.Checked = false;
            RadioButton3.Checked = false;
            ddlvendor.Enabled = true;
            ddlsheet.Enabled = false;

            ddlsheet.SelectedIndex = 0;
        }
    }

    private void bind_Sent_challan(DropDownList vendor)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {

            conn.Open();
            string query = " SELECT distinct isnull([Challan_No],'') as Challan_No FROM part_history_gr where sent_or_rec='Sent' and location='" + vendor.SelectedItem.Text + "' order by Challan_No desc";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable dtsentchallan = new DataTable();
            da.Fill(dtsentchallan);
            if (dtsentchallan.Rows.Count > 0)
            {
                DDLSentchallan.DataSource = dtsentchallan;
                DDLSentchallan.DataTextField = "Challan_No";
                DDLSentchallan.DataValueField = "Challan_No";
                DDLSentchallan.DataBind();
                DDLSentchallan.Items.Insert(0, new ListItem(String.Empty, String.Empty));
                DDLSentchallan.SelectedIndex = 0;
                DDLSentchallan.Visible = true;

            }
        }
        catch (Exception ex)
        {
            conn.Close();
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        conn.Close();

    }



    private void bind_Received_challan(DropDownList vendor)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {

            conn.Open();
            string query = " SELECT distinct     isnull([Challan_No],'') as Challan_No FROM part_history_gr where sent_or_rec='Received' and location='" + vendor.SelectedItem.Text + "' order by Challan_No desc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable dtreceivedchallan = new DataTable();
            da.Fill(dtreceivedchallan);
            if (dtreceivedchallan.Rows.Count > 0)
            {
                ddlReceivedchallan.DataSource = dtreceivedchallan;
                ddlReceivedchallan.DataTextField = "Challan_No";
                ddlReceivedchallan.DataValueField = "Challan_No";
                ddlReceivedchallan.DataBind();
                ddlReceivedchallan.Items.Insert(0, new ListItem(String.Empty, String.Empty));
                ddlReceivedchallan.SelectedIndex = 0;
                ddlReceivedchallan.Visible = true;

            }
        }
        catch (Exception ex)
        {
            conn.Close();
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        conn.Close();

    }


    //private void bind_QC_challan(DropDownList  vendor)
    //{
    //    string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
    //    SqlConnection conn = new SqlConnection(sqlconnstring);
    //    try
    //    {

    //        conn.Open();
    //        string query = " SELECT distinct   Challan_No   FROM temp_part_history_QC where location=' Inspection' ";
    //        SqlDataAdapter da = new SqlDataAdapter(query, conn);
    //        DataTable dtQcchallan = new DataTable();
    //        da.Fill(dtQcchallan);
    //        if (dtQcchallan.Rows.Count > 0)
    //        {
    //            ddlQCchallan.DataSource = dtQcchallan;
    //            ddlQCchallan.DataTextField = "Challan_No";
    //            ddlQCchallan.DataValueField = "Challan_No";
    //            ddlQCchallan.DataBind();
    //            ddlQCchallan.Items.Insert(0, new ListItem(String.Empty, String.Empty));
    //            ddlQCchallan.SelectedIndex = 0;
    //            ddlQCchallan.Visible = true;

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        conn.Close();
    //        ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
    //    }
    //    conn.Close();

    //}



    protected void RadioButton3_CheckedChanged(object sender, EventArgs e)
    {
        if (RadioButton3.Checked == true)
        {

            lblsentchallan.Visible = true;
            DDLSentchallan.Enabled = false;
            lblReceivedchallan.Visible = true;
            ddlReceivedchallan.Enabled = false;

            RadioButton1.Checked = false;
            RadioButton2.Checked = false;
            ddlvendor.Enabled = false;
            ddlsheet.Enabled = true;
            ddlvendor.SelectedIndex = 0;
            //ddlReceivedchallan.SelectedIndex = 0;
            //DDLSentchallan.SelectedIndex = 0;


        }
    }

    protected void ImageButton1_Click1(object sender, ImageClickEventArgs e)
    {
        if (ddlsheet.SelectedItem.Text.Length > 0)
        {
            Session["MIN"] = ddlsheet.SelectedItem.Text;
            //Session["vendor"] = ddlvendor.SelectedItem.Text;
            //Response.Redirect("GateInPrint_Item_Challan.aspx");
            Response.Write("<script language=javascript>child=window.open('GetPrintGRChallan.aspx?Type=QC');</script>");
        }
        else
        {
            if (DDLSentchallan.SelectedItem.Text.Length > 0 && RadioButton1.Checked)
            {
                Session["MIN"] = DDLSentchallan.SelectedItem.Text;
                //Response.Redirect("GateInPrint_Item_Challan.aspx");
                string challanNo = "";
                using (MySqlConnection connec = new MySqlConnection(ConfigurationManager.ConnectionStrings["FCMConnectionString"].ConnectionString.ToString()))
                {
                    connec.Open();
                    MySqlCommand cmd = new MySqlCommand("select * from ChallanDetails where ManualChallanNo=@ManualChallanNo", connec);
                    cmd.Parameters.AddWithValue("@ManualChallanNo", DDLSentchallan.SelectedItem.Text);
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            challanNo = Convert.ToString(reader["ChallanNo"]);
                        }
                    }
                }
                string challanType = "";
                string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
                using (SqlConnection conn = new SqlConnection(sqlconnstring))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("select top 1 Challan_Type from part_history_gr where Challan_No='" + DDLSentchallan.SelectedItem.Text + "'and Challan_Type is not null and Challan_Type!='' ", conn))
                    {
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                rdr.Read();
                               string dbChallnType = Convert.ToString(rdr["Challan_Type"]);

                               if (dbChallnType == "Purchase Order")
                                {
                                    challanType = "PO";
                                }
                               else if (dbChallnType == "Job Work")
                                {
                                    challanType = "JW";
                                }
                               else if (dbChallnType == "Rework Challan")
                                {
                                    challanType = "RC";
                                }
                               else if (dbChallnType == "Non Returnable Reject Challan")
                                {
                                    challanType = "NR";
                                }
                            }
                            else
                            {
                                challanType = "NA";
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(challanNo))
                {
                    //Response.Write("<script language=javascript>child=window.open('Item_Challan.aspx?ProcessType=Send&AutoGeneratedChallan=" + challanNo + "');</script>");
                    Response.Write("<script language=javascript>child=window.open('SentChallan.aspx?ChallanType=" + challanType + "&AutoGeneratedChallan=" + challanNo + "');</script>");
                }
                else
                {
                   // Response.Write("<script language=javascript>child=window.open('Item_Challan.aspx?ProcessType=Send');</script>");
                    Response.Write("<script language=javascript>child=window.open('SentChallan.aspx?ChallanType=" + challanType + "');</script>");
                }
            }

            else if (ddlReceivedchallan.SelectedValue != "" && RadioButton2.Checked)
            {
                Session["MIN"] = ddlReceivedchallan.SelectedItem.Text;
                if (ddlvendor.SelectedItem.Text.TrimStart(' ') == "Inspection")
                {
                    Response.Write("<script language=javascript>child=window.open('Item_Challan.aspx?ProcessType=Receive&Vendor=Inspection');</script>");
                }
                else
                {
                    Response.Write("<script language=javascript>child=window.open('Item_Challan.aspx?ProcessType=Receive');</script>");
                }
            }
        }


    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }
    protected void ddlvendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlvendor.SelectedItem.Text.Length > 0)
        {

            bind_Received_challan(ddlvendor);
            bind_Sent_challan(ddlvendor);

        }
    }

    private void bind_vendor()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {

            conn.Open();
            string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work order by id asc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable request_det_head = new DataTable();
            da.Fill(request_det_head);
            if (request_det_head.Rows.Count > 0)
            {
                ddlvendor.DataSource = request_det_head;
                ddlvendor.DataTextField = "VENDOR_NAME";
                ddlvendor.DataValueField = "ID";
                ddlvendor.DataBind();
                ddlvendor.Items.Insert(0, new ListItem(String.Empty, String.Empty));
                ddlvendor.SelectedIndex = 0;
                ddlvendor.Visible = true;

            }
        }
        catch (Exception ex)
        {
            conn.Close();
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        conn.Close();

    }


    private void bind_sheet()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {

            conn.Open();
            string query = " SELECT distinct    Qcsheet_no   FROM temp_part_history_QC_gr with(nolock) order by Qcsheet_no desc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable request_det_head = new DataTable();
            da.Fill(request_det_head);
            if (request_det_head.Rows.Count > 0)
            {

                ddlsheet.DataSource = request_det_head;
                ddlsheet.DataTextField = "Qcsheet_no";
                ddlsheet.DataValueField = "Qcsheet_no";
                ddlsheet.DataBind();
                ddlsheet.Items.Insert(0, new ListItem(String.Empty, String.Empty));
                ddlsheet.SelectedIndex = 0;
                ddlsheet.Visible = true;

            }
        }
        catch (Exception ex)
        {
            conn.Close();
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        conn.Close();

    }

    protected void ddlsheet_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}