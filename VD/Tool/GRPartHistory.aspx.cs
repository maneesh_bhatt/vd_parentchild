﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class Tool_GRPartHistory : System.Web.UI.Page
{
    string p_no, tool_no, station, req_qty, rwk_no;
    string part_name = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        p_no = Request.QueryString["p_no"];

        part_name = Request.QueryString["part_name"];
        tool_no = Request.QueryString["tool_no"];
        station = Request.QueryString["station"];
        req_qty = Request.QueryString["req_qty"];
        rwk_no = Request.QueryString["rwk_no"];
        Label1.Text = part_name;
        Label2.Text = p_no;
        Label3.Text = tool_no;
        Label4.Text = station;
        //Label5.Text = req_qty;

        if (!IsPostBack)
        {
            if (Label1.Text.Length > 0)
            {
                //datediff();
                showgrid();
            }
        }

    }

    public void datediff()
    {
        DateTime uplode = new DateTime();
        DateTime send = new DateTime();
        DateTime received = new DateTime();
        DataTable uploaddate = new DataTable();
        DataTable senddate = new DataTable();
        DataTable receiveddate = new DataTable();
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();

        string query = "select date_of_trans from part_history where   p_no='" + p_no + "' and part_name='" + part_name + "' and tool_no='" + tool_no + "' and station='" + station + "' and sent_or_rec='Uploaded'   ";
        SqlDataAdapter da = new SqlDataAdapter(query, conn);
        da.Fill(uploaddate);

        string query1 = "select date_of_trans from part_history where   p_no='" + p_no + "' and part_name='" + part_name + "' and tool_no='" + tool_no + "' and station='" + station + "' and sent_or_rec='Sent'  ";
        da = new SqlDataAdapter(query1, conn);
        da.Fill(senddate);

        string query2 = "select date_of_trans from part_history where   p_no='" + p_no + "' and part_name='" + part_name + "' and tool_no='" + tool_no + "' and station='" + station + "' and sent_or_rec='Received'  ";
        da = new SqlDataAdapter(query2, conn);
        da.Fill(receiveddate);

        if (uploaddate.Rows.Count > 0)
        {
            uplode = Convert.ToDateTime(uploaddate.Rows[0]["date_of_trans"].ToString());
        }

        if (senddate.Rows.Count > 0)
        {
            send = Convert.ToDateTime(senddate.Rows[0]["date_of_trans"].ToString());
            int uploadday = uplode.Day;
            int senday = send.Day;
            DateTime finalsenddiff1 = send.AddDays(-uploadday);
            int finsendday = finalsenddiff1.Day;
        }

        if (receiveddate.Rows.Count > 0)
        {
            received = Convert.ToDateTime(receiveddate.Rows[0]["date_of_trans"].ToString());

            if (receiveddate.Rows.Count > 0)
            {
                received = Convert.ToDateTime(senddate.Rows[0]["date_of_trans"].ToString());
                int receivedday = received.Day;
                int senday = send.Day;
                DateTime finalReciveddiff1 = received.AddDays(-senday);
                int finreciveddday = finalReciveddiff1.Day;
            }
        }

        conn.Close();
    }

    public void showgrid()
    {
        try
        {

            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);
            conn.Open();

            SqlCommand cmd = new SqlCommand("select id,location,date_of_trans,rwk_no,sent_or_rec, quantity  ,TransSentQty as SendQty_Or_ReceivedQty,User_Name from part_history_GR  where   p_no=@p_no and part_name=@part_name and tool_no=@tool_no and station=@station and rwk_no=@rwk_no order by date_of_trans asc ");

            cmd.Parameters.AddWithValue("@p_no", p_no);
            cmd.Parameters.AddWithValue("@part_name", part_name);
            cmd.Parameters.AddWithValue("@tool_no", tool_no);
            cmd.Parameters.AddWithValue("@station", station);
            cmd.Parameters.AddWithValue("@rwk_no", rwk_no);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;
            DataTable dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            if (dt.Rows.Count > 0)
            {

                GridView1.DataSource = dt;
                Session["DataTableAdvanceReport"] = dt;
                GridView1.DataBind();
                conn.Close();
                conn.Dispose();
            }
            else
            {
                dt = null;
                GridView1.DataSource = dt;
                GridView1.DataBind();
                conn.Close();
                conn.Dispose();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        GridView1.PageIndex = e.NewPageIndex;
        DataTable table = (DataTable)Session["DataTableAdvanceReport"];
        GridView1.DataSource = table;
        GridView1.DataBind();

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label result = (Label)e.Row.FindControl("Label6");
            //string result = (e.Row.Cells[2].Text);
            if (result.Text == "COMPANY")
            {
                e.Row.BackColor = System.Drawing.Color.Cyan;
            }
            else
            {

            }
        }
    }
}