﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class Tool_GenerateNegativeDataReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //showgrid();
        }
    }

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            string partsGrCondition = "", locationGrCondition = "";

            if (inputFromDate.Text != "" && inputToDate.Text != "")
            {
                partsGrCondition = " Convert(Date,Date_Of_Trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,Date_of_Trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                locationGrCondition = " Convert(Date,Sent_Received_Date)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,Sent_Received_Date)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
            }
            else if (inputFromDate.Text != "" && inputToDate.Text == "")
            {
                partsGrCondition = " Convert(Date,Date_Of_Trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                locationGrCondition = " Convert(Date,Sent_Received_Date)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
            }
            else if (inputFromDate.Text == "" && inputToDate.Text != "")
            {
                partsGrCondition = " Convert(Date,Date_Of_Trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                locationGrCondition = " Convert(Date,Sent_Received_Date)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
            }
            else
            {
                partsGrCondition = " 1=1";
                locationGrCondition = " 1=1";
            }

            if (inputToolNo.Text != "")
            {
                if (partsGrCondition != "" && locationGrCondition != "")
                {
                    partsGrCondition = partsGrCondition + " and tool_no='" + inputToolNo.Text + "'";
                    locationGrCondition = locationGrCondition + " and tool_no='" + inputToolNo.Text + "'";
                }
                else
                {
                    partsGrCondition = "  tool_no='" + inputToolNo.Text + "'";
                    locationGrCondition = " tool_no='" + inputToolNo.Text + "'";
                }
            }
            conn.Open();
            SqlCommand cmd = new System.Data.SqlClient.SqlCommand("select tool_no,p_no,part_name,station,rwk_no from part_history_Gr where  (isnull(isnull(Upload_Qty,0)-isnull(QtySent,0),0)<0 or isnull(quantity,0)<0 or isnull(QtySent,0)<0 ) and " + partsGrCondition + " union "
            + " select tool_no,p_no,part_name,station,rwk from location_info_Gr where (isnull(isnull(quant_sent,0)- isnull(quant_rec,0),0)<0 or quant_pend<0) and " + locationGrCondition + "", conn);
            gridViewNegativeData.DataSource = cmd.ExecuteReader();
            gridViewNegativeData.DataBind();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }


    protected void gridViewNegativeData_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            if (e.CommandName == "ViewDetails")
            {

                string[] splitCommandArgument = Convert.ToString(e.CommandArgument).Split('-');
                string toolNo = splitCommandArgument[0];
                string pNo = splitCommandArgument[1];
                string partName = splitCommandArgument[2];
                string station = splitCommandArgument[3];
                string reworkNo = splitCommandArgument[4];

                string query = "";
                SqlDataAdapter da = new SqlDataAdapter();
                DataTable table = new DataTable();

                table.Rows.Clear();
                query = "select  Id,Location,Date_Of_Trans as TransDate,Sent_Or_Rec as Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as PendingQtyInPPC,TransSentQty as SentReceivedQty  from part_history_GR  where   p_no='" + pNo + "' and  tool_no='" + toolNo + "' and  station='" + station + "' and  part_name='" + partName + "' and rwk_no='" + reworkNo + "' order by date_of_trans asc ";
                da = new SqlDataAdapter(query, conn);
                da.Fill(table);
                gridViewPartHistory.EmptyDataText = "No Record Found";
                gridViewPartHistory.DataSource = table;
                gridViewPartHistory.DataBind();

                table.Rows.Clear();
                query = "select  Location,Sent_Received_Date as TransDate, quant_sent,quant_rec ,quant_pend  from Location_Info_GR  where   p_no='" + pNo + "' and  tool_no='" + toolNo + "' and  station='" + station + "' and  part_name='" + partName + "' and rwk='" + reworkNo + "' order by Sent_Received_Date asc ";
                da = new SqlDataAdapter(query, conn);
                da.Fill(table);
                gridViewLocationInfo.EmptyDataText = "No Record Found";
                gridViewLocationInfo.DataSource = table;
                gridViewLocationInfo.DataBind();

                dialog.Attributes.Add("style", "display:block;");
                overlay.Attributes.Add("style", "display:block;");
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }

    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }
}