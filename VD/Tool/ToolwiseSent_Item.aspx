﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Pages/MasterPage.master" CodeFile="ToolwiseSent_Item.aspx.cs"
    Inherits="ToolwiseSent_Item" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../Scripts/ScrollableGridViewPlugin_ASP.NetAJAXmin.js" type="text/javascript"></script>
    <title></title>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .Grid td
        {
            background-color: Highlight;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Grid th
        {
            background-color: Navy;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .ChildGrid td
        {
            background-color: #eee !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .ChildGrid th
        {
            background-color: #6C6C6C !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid1 td
        {
            background-color: #fff !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid1 th
        {
            background-color: #2B579A !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid2 td
        {
            background-color: #2B579A !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid2 th
        {
            background-color: Gray !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
    </style>
    <style type="text/css">
        .style2
        {
            border: none;
        }
    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">

        $.noConflict();
        jQuery(document).ready(function ($) {

            $(function () {

                $("[id*=imgOrdersShow]").each(function () {
                    if ($(this)[0].src.indexOf("minus") != -1) {
                        $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
                        $(this).next().remove();
                    }
                });

                $("[id*=imgProductsShow]").each(function () {
                    if ($(this)[0].src.indexOf("minus") != -1) {
                        $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
                        $(this).next().remove();
                    }
                });

            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="1px" style="margin-left: 0px; border-color: Blue" align="left" width="100%">
        <tr>
            <td class="style2" colspan="4" align="center" style="font-size: x-large; color: black">
                <span class="style1">Toolwise Sent Items </span>
                <asp:ScriptManager ID="ScriptManager2" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="6" align="right">
                <%--<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                            <ProgressTemplate>
                                                <center>
                                                    <img alt="" src="../ajax-loader.gif" style="width: 32px; height: 32px" />
                                                </center>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>--%>
            </td>
        </tr>
        
        <tr>
            <td class="style2">
            </td>
            <td>
                <asp:Panel ID="Panel5" runat="server">
                    <table>
                        <tr>
                            <td>
                            </td>
                            <td height="5px">
                                <asp:Label ID="lbl" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td>
                            </td>
                            <td style="font-size: 13px;">
                                Date: <span class="style12" style="color: Red;">* </span>
                                <br />
                                <asp:TextBox ID="txtpodate" runat="server" Width="100px"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpodate"
                                    Display="None" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <cc1:CalendarExtender ID="txtpodate_CalendarExtender" runat="server" Enabled="true"
                                    Format="dd-MMM-yyyy" TargetControlID="txtpodate">
                                </cc1:CalendarExtender>
                            </td>
                            <td style="font-size: 13px;">
                                &nbsp;Comp.Chall.No <span class="style12" style="color: Red;">* </span>
                                <br />
                                <asp:TextBox ID="txtchalanno" runat="server" Enabled="False" Width="100px"></asp:TextBox>
                                <br />
                            </td>
                            <td colspan="3" style="font-size: 13px;">
                                Vendor Name<span class="style12" style="color: Red;">* </span>
                                <br />
                                <asp:DropDownList ID="ddlvendor1" runat="server" AutoPostBack="true" EnableViewState="true"
                                    AppendDataBoundItems="true" Width="150px" OnSelectedIndexChanged="ddlvendor_SelectedIndexChanged">
                                </asp:DropDownList>
                                <%--  <br />--%>
                                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlvendor"
                                    Display="Dynamic" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                            </td>
                            
                            <td style="font-size: 13px;">
                                Tool No<br />
                                <asp:TextBox ID="tbTool" runat="server" Visible="true" Width="90px"></asp:TextBox>
                            </td>
                            <td height="15px" style="font-size: 13px;">
                                Station No<br />
                                <asp:TextBox ID="tbStation" runat="server" Visible="true" Width="90px"></asp:TextBox>
                            </td>
                            <td style="font-size: 13px;">
                                Rework No
                                <br />
                                <asp:TextBox ID="txtreworkno" runat="server" Width="90px"></asp:TextBox>
                            </td>
                            <td style="font-size: 13px;">
                                Batch No<br />
                                <asp:TextBox ID="txtbatchno" runat="server" Width="90px"></asp:TextBox>
                            </td>
                            <td style="font-size: 13px;">
                                &nbsp;
                            </td>
                            <td colspan="2" style="font-size: 13px;">
                                &nbsp;
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td height="5px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="font-size: 13px;">
                                PO NO<span class="style12"> </span>
                                <br />
                                <br />
                                <asp:TextBox ID="txtPONO" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td style="font-size: 13px;">
                                Sr No of Reg.<br />
                                <br />
                                <asp:TextBox ID="txtSRREGNO" runat="server" Width="100px"></asp:TextBox>
                                <br />
                            </td>
                            <td style="font-size: 13px;" valign="middle">
                                Est.Hrs<span class="style12" style="color: Red;">* </span>
                                <br />
                                <br />
                                <asp:TextBox ID="txtesthrs" runat="server" Width="75px"></asp:TextBox>
                            </td>
                            <td valign="bottom" style="font-size: 13px;">
                                Req By Date:
                                <br />
                                <br />
                                <asp:TextBox ID="txtrequired" runat="server" Width="90px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="true" Format="dd-MMM-yyyy"
                                    TargetControlID="txtrequired">
                                </cc1:CalendarExtender>
                                &nbsp;
                            </td>
                            <td style="font-size: 13px;" colspan="2">
                                Del.Commit.Date<br />
                                By vendor <span class="style12" style="color: Red;">* </span>
                                <br />
                                <asp:TextBox ID="txtdelcdate" runat="server" Width="90px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                    Enabled="true" TargetControlID="txtdelcdate">
                                </cc1:CalendarExtender>
                            </td>
                            <td style="font-size: 13px; margin-right: 100px;">
                                Exp.Date of<br />
                                Commt<span class="style12" style="color: Red;">* </span>
                                <br />
                                <asp:TextBox ID="txtexpdate" runat="server" Width="90px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="true" Format="dd-MMM-yyyy"
                                    TargetControlID="txtexpdate">
                                </cc1:CalendarExtender>
                            </td>
                            
                            <td class="style1" style="font-size: 13px;">
                                 <asp:Button ID="Button1" CausesValidation="false" style="background:#335599 url(../images/bg.png) repeat-x 0 -110px;color:white;height:35px;width:90px;margin-top:18px;"  runat="server" Text="Search" OnClick="Button1_Click" />
           
                                &nbsp;
                                <asp:Button ID="btnprint" runat="server" CausesValidation="false" OnClick="btnprint_Click"
                                    Text="PrintChallan" Visible="false" />
                            </td>
                            <td style="font-size: 13px;" valign="middle">
                                &nbsp;
                                <asp:Button ID="printispectionchallan" runat="server" CausesValidation="false" OnClick="printispectionchallan_Click"
                                    Text="QCChallan" Visible="false" Width="100px" />
                            </td>
                             <td style="font-size: 13px;" valign="middle" runat="server" id="tdReceived" visible="false">
                                &nbsp; &nbsp;
                                <asp:Label ID="Label4" runat="server" Text="Vendor For Inspection" Visible="true"></asp:Label>
                                <br />
                                <asp:DropDownList ID="ddlreceived" BackColor="#F6F1DB" ForeColor="#7d6754" CssClass="glowing-border"
                                    runat="server" Width="150px">
                                </asp:DropDownList>

                                 <br />
                                 <br />
                                <asp:Label ID="lblbillno" runat="server" Text="Bill No"></asp:Label>&nbsp;&nbsp;<asp:TextBox
                                    ID="txtbillno" runat="server" CssClass="glowing-border" Width="75px"></asp:TextBox>
                           
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="5">
                <asp:GridView ID="gvCustomers" runat="server" AutoGenerateColumns="false" Width="100%"
                    CssClass="Grid" DataKeyNames="tool_no">
                    <Columns>
                        <asp:TemplateField HeaderStyle-BackColor="AliceBlue">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgOrdersShow" runat="server" CausesValidation="false" ImageUrl="~/images/plus.png"
                                    CommandArgument="Show" OnClick="Show_Hide_OrdersGrid" />
                                <asp:Panel ID="pnlOrdersStation" runat="server" Visible="false" Style="position: relative">
                                    <asp:GridView ID="gvdStation" AutoGenerateColumns="false" Width="900px" CssClass="ChildGrid"
                                        DataKeyNames="Station" runat="server">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-BackColor="Brown">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgProductsShow" CausesValidation="false" runat="server" OnClick="Show_Hide_ProductsGrid"
                                                        ImageUrl="~/images/plus.png" CommandArgument="Show" />
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="Panel1" runat="server">
                                                                <asp:GridView ID="GridView1" runat="server" ShowFooter="true" AutoGenerateColumns="False"
                                                                    CellPadding="2" ForeColor="#333333" GridLines="None" Font-Size="13px" Width="900px">
                                                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkAll1" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll1_CheckedChanged1" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkAllchild" AutoPostBack="true" runat="server" OnCheckedChanged="chkAllchild_CheckedChanged">
                                                                                </asp:CheckBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller">
                                                                            <HeaderTemplate>
                                                                                Project No
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblproject" runat="server" Text='<%# Eval("tool_no") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            HeaderStyle-Width="20px" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="left">
                                                                            <HeaderTemplate>
                                                                                Station No
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label10" runat="server" Text='<%# Eval("station") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Position No
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <%--<asp:Label ID="Label1" runat="server" Text='<%# Eval("p_no") %>'></asp:Label>--%>
                                                                                <asp:LinkButton ID="Label1" runat="server" Text='<%# Eval("p_no") %>'></asp:LinkButton>
                                                                                <cc1:PopupControlExtender ID="PopupControlExtender1" runat="server" PopupControlID="Panel4"
                                                                                    TargetControlID="Label1" DynamicContextKey='<%# Eval("p_no") + ";" + Eval("tool_no") + ";" + Eval("station") + ";" + Eval("part_name")+ ";" + Eval("rwk_no") %>'
                                                                                    DynamicControlID="Panel4" DynamicServiceMethod="GetDynamicContent" Position="Bottom">
                                                                                </cc1:PopupControlExtender>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            HeaderStyle-Width="50px" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="left">
                                                                            <HeaderTemplate>
                                                                                Batch No
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblbatchno" runat="server" Text='<%# Eval("Batch_No") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            HeaderStyle-Width="40px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                rwk No
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label111" runat="server" Text='<%# Eval("rwk_no") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="left" ItemStyle-Font-Size="Smaller"
                                                                            ItemStyle-Width="200px" ItemStyle-HorizontalAlign="left">
                                                                            <HeaderTemplate>
                                                                                Part Name
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("part_name") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            Visible="false" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Location
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("location") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Total Bal.Qty In PPC
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label8" runat="server" Text='<%# Eval("quantity") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Quantity sent
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Labelsent" Visible="true" runat="server" Text='<%# Eval("QtySent") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Qty To Be send
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="TextBox3" runat="server" OnTextChanged="TextBox3_TextChanged" AutoPostBack="true"
                                                                                    Enabled="false" Width="75px" onkeypress="return isNumberKey(event)" Text='<%# Eval("PendingQty") %>'> 
                                                                            </asp:TextBox>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Bal.Qty
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtbalc" runat="server" Enabled="false" Width="75px" Text=""></asp:TextBox>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Upl Qty
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LabelUpload_Qty" Visible="true" runat="server" Text='<%# Eval("Upload_Qty") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Upl Date
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbluplodedate" Visible="true" runat="server" Text='<%# Eval("date_of_trans","{0:dd/MM/yyyy}") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                LSentDate
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSent_Date" Visible="true" runat="server" Text='<%# Eval("Sent_Date","{0:dd/MM/yyyy}") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Smaller"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                LRecDate
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblReceived_Date" Visible="true" runat="server" Text='<%# Eval("Received_Date","{0:dd/MM/yyyy}") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                            <FooterTemplate>
                                                                                <asp:Button ID="btnSend" runat="server" CausesValidation="true" EnableViewState="true"
                                                                                    Text="Send" Visible="false" Width="90px" OnClick="btnSend_Click" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                                    <EditRowStyle BackColor="#999999" />
                                                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                                                </asp:GridView>
                                                                <asp:Panel ID="Panel4" runat="server">
                                                                </asp:Panel>
                                                            </asp:Panel>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    </ContentTemplate>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField ItemStyle-Width="500px" DataField="Station" HeaderStyle-BackColor="Green"
                                                HeaderText="Station" />
                                            <asp:BoundField ItemStyle-Width="500px" DataField="TotalPendingQty" HeaderStyle-BackColor="Green"
                                                HeaderText="TotPendingQty" />
                                            <asp:BoundField ItemStyle-Width="500px" DataField="TotNoPosition" HeaderStyle-BackColor="Green"
                                                HeaderText="TotPosition" />
                                            <asp:BoundField ItemStyle-Width="500px" DataField="tool_no" HeaderStyle-BackColor="Green"
                                                HeaderText="tool_no" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ItemStyle-Width="500px" DataField="tool_no" HeaderStyle-BackColor="Green"
                            HeaderText="ToolNo" />
                        <%--   <asp:BoundField ItemStyle-Width="400px" DataField="location" HeaderStyle-BackColor="Green"
                            HeaderText="Vendor" />--%>
                        <%-- <asp:BoundField ItemStyle-Width="150px" DataField="part_name" HeaderText="Part Name" />--%>
                        <asp:BoundField ItemStyle-Width="500px" DataField="Totalpendingqty" HeaderStyle-BackColor="Green"
                            HeaderText="PendingQty" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
