﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="GenerateVisitAndVerificationSummary.aspx.cs" Inherits="Tool_GenerateVisitAndVerificationSummary" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style2
        {
            border: none;
            width: 100px;
        }
        
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatePage" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 100%; border: 1px solid Blue; min-height: 100px;">
                <div style="padding: 9px;">
                    <div>
                        <div align="center">
                            <span class="style13">Generate Vendor Visit / Verification Summary</span>
                        </div>
                    </div>
                    <div style="margin-bottom: 10px; margin-top: 20px;">
                        <div style="font-size: 13px; width: 140px; float: left;">
                            Select Range
                            <br />
                            <asp:DropDownList ID="ddlDateRange" ForeColor="#7d6754" AppendDataBoundItems="true"
                                Width="130px" runat="server">
                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div style="font-size: 13px; width: 150px; float: left;">
                            Report Type
                            <asp:DropDownList ID="ddlReportType" AppendDataBoundItems="true" ForeColor="#7d6754"
                                OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged" AutoPostBack="true"
                                ClientIDMode="Static" Width="140px" runat="server">
                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                <asp:ListItem Text="Visit Summary" Value="VisitSummary"></asp:ListItem>
                                <asp:ListItem Text="Verification Summary List" Value="VerificationSummary"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div style="font-size: 13px; width: 150px; float: left;">
                            Vendor Type
                            <asp:DropDownList ID="ddlVendorType" AppendDataBoundItems="true" ForeColor="#7d6754"
                                ClientIDMode="Static" Width="140px" runat="server">
                                <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                <asp:ListItem Text="Internal Vendor" Value="Int"></asp:ListItem>
                                <asp:ListItem Text="External Vendor" Value="Ext"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:Button ID="btnGenerateVisitReport" ClientIDMode="Static" runat="server" Visible="true"
                            EnableViewState="true" CausesValidation="false" Text="Generate Report" Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px;
                            color: white; height: 28px; width: 133px; margin-top: 8px;" OnClick="btnGenerateVisitReport_Click" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
