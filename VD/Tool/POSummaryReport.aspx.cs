﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class Tool_POSummaryReport : System.Web.UI.Page
{
    public static string searchCondition;
    protected void Page_Load(object sender, EventArgs e)
    {
        searchCondition = "";
    }

    /// <summary>
    /// Displays the Summary of Items from Part History Gr Table that are Sent as Purchase Order or Job Work
    /// </summary>
    protected void BindGrid()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            string searchCondition = "";
            if (inputVendorName.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and phg.location='" + inputVendorName.Text + "'";
                }
                else
                {
                    searchCondition = " phg.location='" + inputVendorName.Text + "'";
                }
            }
            if (inputToolNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and phg.tool_no='" + inputToolNo.Text + "'";
                }
                else
                {
                    searchCondition = " phg.tool_no='" + inputToolNo.Text + "'";
                }
            }
            if (inputStation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and phg.station='" + inputStation.Text + "'";
                }
                else
                {
                    searchCondition = "  phg.station='" + inputStation.Text + "'";
                }
            }
            if (inputPNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and phg.p_no='" + inputPNo.Text + "'";
                }
                else
                {
                    searchCondition = " phg.p_no='" + inputPNo.Text + "'";
                }
            }
            if (inputReworkNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and phg.rwk_no='" + inputReworkNo.Text + "'";
                }
                else
                {
                    searchCondition = " phg.rwk_no='" + inputReworkNo.Text + "'";
                }
            }
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = "";
            if (!string.IsNullOrEmpty(searchCondition))
            {
                query = "select phg.PO_NO, phg.Part_Name, phg.Date_Of_Trans as PO_Date, phg.Location, phg.Tool_No, phg.Station, phg.Rwk_No, phg.P_No, "
                + " phg.PO_Amount, phg.QtySent, phg.Po_Amount* phg.QtySent as TotalAmount, phg.Delivery_Commit_Date,phg.Received_Date, "
                + " phg.Challan_No,phg.User_Name from part_history_GR phg "
                + " left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and "
                + " phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where phg.location!='Company' and phg.Sent_Or_Rec='Sent' "
                + " and phg.Challan_Type in ('Job Work','Purchase Order') and pgr.Is_Active_Record=1   and ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) and phg.location not in ('VD Finish Store','Assembly','FinishPartStore','weld vd finish store','Weld Parts Consumed','Reject Parts','DirectAssembly','Weld Parts Waiting','Waiting For Surface Treatment','Paint Shop') and " + searchCondition + " order by phg.Date_Of_Trans Asc";
                adapter = new SqlDataAdapter(query, con);
            }
            else
            {
                query = "select phg.PO_NO, phg.Part_Name, phg.Date_Of_Trans as PO_Date, phg.Location, phg.Tool_No, phg.Station, phg.Rwk_No, phg.P_No, "
                  + " phg.PO_Amount, phg.QtySent, phg.Po_Amount* phg.QtySent as TotalAmount, phg.Delivery_Commit_Date,phg.Received_Date, "
                  + " phg.Challan_No,phg.User_Name from part_history_GR phg "
                  + " left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and "
                  + " phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where phg.location!='Company' and phg.Sent_Or_Rec='Sent' "
                  + " and phg.Challan_Type in ('Job Work','Purchase Order') and pgr.Is_Active_Record=1   and ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and phg.location not in ('VD Finish Store','Assembly','FinishPartStore','weld vd finish store','Weld Parts Consumed','Reject Parts','DirectAssembly','Weld Parts Waiting','Waiting For Surface Treatment','Paint Shop') order by phg.Date_Of_Trans Asc";
                adapter = new SqlDataAdapter(query, con);
            }
            ViewState["CurrentQuery"] = query;
            DataTable td = new DataTable();
            adapter.Fill(td);
            gridPOSummary.DataSource = td;
            gridPOSummary.DataBind();
            gridPOSummary.EmptyDataText = "No Record Found";
            lblRecorrdsCount.Text = Convert.ToString(td.Rows.Count) + " " + "Records In Total";
            if (gridPOSummary.Rows.Count > 0)
            {
                btnDownload.Visible = true;
            }
            else
            {
                btnDownload.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }
    }

    protected void gridPOSummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // Set the index of the new display page.  
        gridPOSummary.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void gridPOSummary_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            Response.Redirect("Vendor_Master.aspx?ID=" + e.CommandArgument);
        }
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        UtilityFunctions.ExportToExcel(Convert.ToString(ViewState["CurrentQuery"]), "BOMMaster");
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    /// <summary>
    /// Calculates the First Sent Date and Total Received Quantity
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPOSummary_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            Label lblActualDeliveryDate = (Label)e.Row.FindControl("lblActualDeliveryDate");
            Label lblTotalReceivedQty = (Label)e.Row.FindControl("lblTotalReceivedQty");
            using (SqlConnection connec = new SqlConnection(sqlconnstring))
            {
                SqlDataAdapter ad = new SqlDataAdapter("select date_of_trans,quantity from part_history_Gr where Reference_Challan_No=@ChallanNumber and tool_no=@ToolNo and p_no=@PNo and part_name=@PartName and station=@Station and rwk_no=@RwkNo and Sent_Or_Rec='Received' order by Id asc", connec);
                ad.SelectCommand.Parameters.AddWithValue("@ChallanNumber", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Challan_No")));
                ad.SelectCommand.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                ad.SelectCommand.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                ad.SelectCommand.Parameters.AddWithValue("@PartName", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Part_Name")));
                ad.SelectCommand.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                ad.SelectCommand.Parameters.AddWithValue("@RwkNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                int totalReceivedQty = 0;
                DataTable dt = new DataTable();
                ad.Fill(dt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    totalReceivedQty = totalReceivedQty + Convert.ToInt32(dt.Rows[i]["quantity"]);
                    if (i == dt.Rows.Count - 1)
                    {
                        lblActualDeliveryDate.Text = Convert.ToDateTime(dt.Rows[i]["date_of_trans"]).ToString("dd-MM-yyyy");
                    }
                }
                lblTotalReceivedQty.Text = Convert.ToString(totalReceivedQty);
            }
        }
    }
}