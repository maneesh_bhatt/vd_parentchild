﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Text;

public partial class Tool_VendorDelayedReport : System.Web.UI.Page
{
    SqlConnection connection;
    SqlCommand cmd;
    public static string searchConditionVendor;
    Image sortImageVendor = new Image();
    private static string sortConditionVendor;
    private static string sortColumnVendor;
    public string SortDirectionVendor
    {
        get
        {
            if (ViewState["SortDirectionVendor"] == null)
                return string.Empty;
            else
                return ViewState["SortDirectionVendor"].ToString();
        }
        set
        {
            ViewState["SortDirectionVendor"] = value;
        }
    }
    private string _sortDirectionVendor;

    public static string searchConditionLocation;
    Image sortImageLocation = new Image();
    private static string sortConditionLocation;
    private static string sortColumnLocation;
    public string SortDirectionLocation
    {
        get
        {
            if (ViewState["SortDirectionLocation"] == null)
                return string.Empty;
            else
                return ViewState["SortDirectionLocation"].ToString();
        }
        set
        {
            ViewState["SortDirectionLocation"] = value;
        }
    }
    private string _sortDirectionLocation;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Binds the Grid when user provides the Date Filters and Presses the Search Button
    /// </summary>
    public void showgrid()
    {
        try
        {
            if (inputFromDate.Text != "" || inputToDate.Text != "")
            {
                DateTime dt = DateTime.MinValue;
                DataTable partList = GetDataByCriteria("");


                var result =
                partList.AsEnumerable()
                    .Select(p => new
                    {
                        location = p.Field<string>("Location"),
                        dateChangedCount = p.Field<int>("DateChangeCount")
                    })
                    .GroupBy(p => new { p.location })
                    .Select(p => new { Location = p.Key.location, PositionsSent = p.Count(), DateChangeCount = p.Sum(x => Convert.ToInt32(x.dateChangedCount)) })
                    .ToList();


                if (string.IsNullOrEmpty(sortConditionVendor))
                {
                    sortConditionVendor = "Location ASC";
                }
                string[] splitCondition = sortConditionVendor.Split(' ');
                if (splitCondition[1].ToLower() == "asc")
                {
                    result = result.OrderBy(r => r.GetType().GetProperty(splitCondition[0]).GetValue(r, null)).ToList();
                }
                else if (splitCondition[1].ToLower() == "desc")
                {
                    result = result.OrderByDescending(r => r.GetType().GetProperty(splitCondition[0]).GetValue(r, null)).ToList();
                }

                girdVendorDelayedReport.DataSource = result;
                girdVendorDelayedReport.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Time Interval or Select From Date or To Date');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

        }
    }

    /// <summary>
    /// Retrieves the Data from Location Info Gr and Commitment History Table, If Expected Delivery Date Count is Greater then Zero it Means Date has been changed,
    /// So that position will be considered as Delayed
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    protected DataTable GetDataByCriteria(string location)
    {

        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        List<PartHistory> partList = new List<PartHistory>();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        SqlDataAdapter ad = new SqlDataAdapter();
        try
        {
            string searchCondition = "";
            if (location == "")
            {
                location = inputVendorName.Text;
            }
            if (location != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.location='" + location + "'";
                }
                else
                {
                    searchCondition = " lig.location='" + location + "'";
                }
            }
            if (location == "")
            {
                if (checkBoxInternalVendors.Checked && checkBoxEnternalVendors.Checked)
                {
                    if (searchCondition != "")
                        searchCondition = searchCondition + " and (vm.vendor_type='End Store' or vm.vendor_type='Internal Vendor' or vm.Vendor_Type='External Vendor')";
                    else
                        searchCondition = " (vm.vendor_type='End Store' or vm.vendor_type='Internal Vendor' or vm.Vendor_Type='External Vendor')";
                }
                else if (checkBoxInternalVendors.Checked)
                {
                    if (searchCondition != "")
                        searchCondition = searchCondition + " and (vm.vendor_type='End Store' or vm.vendor_type='Internal Vendor')";
                    else
                        searchCondition = " (vm.vendor_type='End Store' or vm.Vendor_Type='Internal Vendor')";
                }
                else if (checkBoxEnternalVendors.Checked)
                {
                    if (searchCondition != "")
                        searchCondition = searchCondition + " and vm.vendor_type='External Vendor'";
                    else
                        searchCondition = " vm.vendor_type='External Vendor'";
                }
            }
            if (inputFromDate.Text != "" && inputToDate.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,tb.date_of_trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,tb.date_of_trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,tb.date_of_trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,tb.date_of_trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
            }
            else if (inputFromDate.Text != "" && inputToDate.Text == "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,tb.date_of_trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,tb.date_of_trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                }
            }
            else if (inputFromDate.Text == "" && inputToDate.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,tb.date_of_trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,tb.date_of_trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
            }
            if (searchCondition != "")
            {
                ad = new SqlDataAdapter("select lig.location,lig.tool_no,lig.p_no,lig.part_name,lig.station,lig.rwk,tb.QtySent as SentQuantity , tb.date_of_trans,tb.Delivery_commit_date,tb.Part_History_Id, ch.RecordsCount  as DateChangeCount from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no  left join Vendor_Master_Job_work vm on lig.location=vm.Vendor_Name  outer apply (select top 1 pg.QtySent,pg.date_of_trans,pg.Delivery_commit_date, pg.id as Part_History_Id "
                     + " from part_history_GR pg where pg.tool_no=lig.tool_no and pg.p_no=lig.p_no and pg.part_name=lig.part_name and pg.station=lig.station and pg.rwk_no= lig.Rwk and pg.sent_or_rec='Sent' and pg.location=lig.location order by pg.date_of_trans desc)tb outer apply (select Count(ch.CommitmentHistoryId) as RecordsCount  from CommitmentHistory ch where  ch.Part_History_Id=tb.Part_History_Id)ch where pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and lig.location !='' and ch.RecordsCount>0 and " + searchCondition + "", connec);
            }
            else
            {
                ad = new SqlDataAdapter("select lig.location,lig.tool_no,lig.p_no,lig.part_name,lig.station,lig.rwk,tb.QtySent as SentQuantity , tb.date_of_trans,tb.Delivery_commit_date,tb.Part_History_Id, ch.RecordsCount  as DateChangeCount from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no  left join Vendor_Master_Job_work vm on lig.location=vm.Vendor_Name  outer apply (select top 1 pg.QtySent,pg.date_of_trans,pg.Delivery_commit_date, pg.id as Part_History_Id "
                     + " from part_history_GR pg where pg.tool_no=lig.tool_no and pg.p_no=lig.p_no and pg.part_name=lig.part_name and pg.station=lig.station and pg.rwk_no= lig.Rwk and pg.sent_or_rec='Sent' and pg.location=lig.location order by pg.date_of_trans desc)tb outer apply (select Count(ch.CommitmentHistoryId) as RecordsCount  from CommitmentHistory ch where  ch.Part_History_Id=tb.Part_History_Id)ch where pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and lig.location !='' and ch.RecordsCount>0", connec);
            }
            DataTable dt = new DataTable();
            ad.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    protected void girdVendorDelayedReport_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            string location = Convert.ToString(e.CommandArgument);
            DateTime dt = DateTime.MinValue;

            gridViewLocationWiseData.DataSource = GetDataByCriteria(location);
            gridViewLocationWiseData.DataBind();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
    }

    protected void girdVendorDelayedReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }

    protected void girdVendorDelayedReport_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    protected void girdVendorDelayedReport_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected string GetHtmlContent(DataTable table)
    {

        StringBuilder b = new StringBuilder();
        b.Append("<table style='");
        b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
        b.Append("<tr>");
        foreach (DataColumn column in table.Columns)
        {
            b.Append("<th>");
            b.Append(column.ColumnName);
            b.Append("</th>");
        }
        b.Append("</tr>");


        foreach (DataRow row in table.Rows)
        {
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<td>");
                b.Append(row[column.ColumnName]);
                b.Append("</td>");
            }
            b.Append("</tr>");
        }
        b.Append("</table>");
        return b.ToString();
    }

    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }


    protected void ExportToExcel()
    {
        Response.ClearContent();
        string fileName = "CommitmentDateData";
        string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        girdVendorDelayedReport.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }
    
    /// <summary>
    /// When User clicks on the Date Change Count, it displays the History of Date Changed from Commitment History Table
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewLocationWiseData_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            if (e.CommandName == "ViewDateChangedData")
            {
                int partHistoryId = Convert.ToInt32(e.CommandArgument);
                string query = "select  Tool_No,P_No,Part_Name,Station,Rework_No,Location,Commitment_Date  from CommitmentHistory  where  Part_History_Id=" + partHistoryId + " union all select  Tool_No,P_No,Part_Name,Station,Rwk_No,Location,Delivery_commit_date  from part_history_Gr  where  id=" + partHistoryId + " order by Commitment_Date desc ";
                SqlDataAdapter da = new SqlDataAdapter(query, connec);
                DataTable table = new DataTable();
                da.Fill(table);
                gridViewDateChangeSummary.DataSource = table;
                gridViewDateChangeSummary.DataBind();
                dialogCommitmentHistory.Attributes.Add("style", "display:block;");
                overlay.Attributes.Add("style", "display:block;");
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void btnCloseCommitmentHistory_Click(object sender, EventArgs e)
    {
        dialogCommitmentHistory.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }

    protected void ApplySortingImageVendor(string sortExpression)
    {
        int columnIndex = 0;
        foreach (DataControlFieldHeaderCell headerCell in girdVendorDelayedReport.HeaderRow.Cells)
        {
            if (headerCell.ContainingField.SortExpression == sortExpression)
            {
                columnIndex = girdVendorDelayedReport.HeaderRow.Cells.GetCellIndex(headerCell);
            }
        }

        girdVendorDelayedReport.HeaderRow.Cells[columnIndex].Controls.Add(sortImageVendor);
    }

    protected void SetSortDirectionVendor(string sortDirection)
    {
        if (sortDirection.ToLower() == "asc")
        {
            _sortDirectionVendor = "DESC";
            sortImageVendor.ImageUrl = "../Images/view_sort_ascending.png";

        }
        else
        {
            _sortDirectionVendor = "ASC";
            sortImageVendor.ImageUrl = "../Images/view_sort_descending.png";
        }
    }

    protected void ApplySortingImageLocation(string sortExpression)
    {
        int columnIndex = 0;
        foreach (DataControlFieldHeaderCell headerCell in gridViewLocationWiseData.HeaderRow.Cells)
        {
            if (headerCell.ContainingField.SortExpression == sortExpression)
            {
                columnIndex = gridViewLocationWiseData.HeaderRow.Cells.GetCellIndex(headerCell);
            }
        }

        gridViewLocationWiseData.HeaderRow.Cells[columnIndex].Controls.Add(sortImageLocation);
    }

    protected void SetSortDirectionLocation(string sortDirection)
    {
        if (sortDirection.ToLower() == "asc")
        {
            _sortDirectionLocation = "DESC";
            sortImageLocation.ImageUrl = "../Images/view_sort_ascending.png";

        }
        else
        {
            _sortDirectionLocation = "ASC";
            sortImageLocation.ImageUrl = "../Images/view_sort_descending.png";
        }
    }

    protected void girdVendorDelayedReport_Sorting(object sender, GridViewSortEventArgs e)
    {
        // gridBagList.PageIndex = 0;
        SetSortDirectionVendor(SortDirectionVendor);
        sortConditionVendor = e.SortExpression + " " + _sortDirectionVendor;
        sortColumnVendor = e.SortExpression;
        showgrid();
        SortDirectionVendor = _sortDirectionVendor;
        ApplySortingImageVendor(e.SortExpression);

    }

    protected void gridViewLocationWiseData_Sorting(object sender, GridViewSortEventArgs e)
    {
        SetSortDirectionLocation(SortDirectionLocation);
        sortConditionLocation = e.SortExpression + " " + _sortDirectionLocation;
        sortColumnLocation = e.SortExpression;

        DataView view = null;
        view = new DataView(GetDataByCriteria(lblLocation.Text));
        if (string.IsNullOrEmpty(sortConditionLocation))
        {
            sortConditionLocation = "Tool_No ASC";
        }
        view.Sort = sortConditionLocation;
        gridViewLocationWiseData.DataSource = view;
        gridViewLocationWiseData.DataBind();
        SortDirectionLocation = _sortDirectionLocation;
        ApplySortingImageLocation(e.SortExpression);

    }
}
