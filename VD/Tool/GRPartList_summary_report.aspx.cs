﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Windows.Forms;

public partial class GRPartList_summary_report : System.Web.UI.Page
{
    string station = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);

            try
            {
                conn.Open();
                gvCustomers.DataSource = UtilityFunctions.GetData("SELECT   phg.[tool_no]    , phg.location ,sum(phg.quantity) as QtyPending , pgr.Is_Active_Record FROM part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no group by phg.location,phg.tool_no,phg.sent_or_rec,pgr.Is_Active_Record having phg.location='COMPANY'  and sum(phg.quantity)>0  and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  union all  "
               + " select distinct lig.tool_no,lig.location,sum(isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0)) as qtypending,pgr.Is_Active_Record from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no "
               + " group by lig.location,lig.tool_no,pgr.Is_Active_Record having  sum(isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0))>0  and lig.location<>'assembly' and  pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  order by tool_no asc");
                gvCustomers.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
                ex.ToString();
            }
            finally
            {
                conn.Close();
            }
        }
    }

    protected void Show_Hide_OrdersGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlOrders").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";

            string customerId = gvCustomers.DataKeys[row.RowIndex].Value.ToString();
            string cell_1_Value = gvCustomers.Rows[row.RowIndex].Cells[2].Text;
            cell_1_Value = cell_1_Value.Replace("amp;", "");
            if (tbStation.Text.Length > 0)
            {
                station = tbStation.Text;
            }
            else
            {
                station = "";
            }

            GridView gvOrders = row.FindControl("gvOrders") as GridView;
            BindOrders(customerId, gvOrders, cell_1_Value, station);
        }
        else
        {
            row.FindControl("pnlOrders").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    private void BindOrders(string customerId, GridView gvOrders, string part_name, string station)
    {
        gvOrders.ToolTip = customerId;
        DataTable dt = new DataTable();
        if (part_name == "COMPANY")
        {
            if ((station.Length > 0))
            {
                if (txtGr.Text.Length > 0)
                {
                    dt = UtilityFunctions.GetData(string.Format(" SELECT tool_no ,Rework ,rwk_no, station ,part_name ,quantity,Challan_No,date_of_trans , (case Delivery_commit_date  when '1900-01-01 00:00:00.000' then  ''   else Delivery_commit_date  end) as Delivery_commit_date,Upload_Qty,p_no, location , quantity AS qtypending,Sent_Date as Sent_Received_Date  FROM part_history_GR where location='" + part_name + "' and  tool_no ='" + customerId + "'  and station='" + station + "' and rwk_no='" + txtGr.Text + "'  AND  quantity>0  order by tool_no asc"));
                }
                else
                {
                    dt = UtilityFunctions.GetData(string.Format(" SELECT tool_no ,Rework ,rwk_no, station ,part_name ,quantity,Challan_No,date_of_trans , (case Delivery_commit_date  when '1900-01-01 00:00:00.000' then  ''   else Delivery_commit_date  end) as Delivery_commit_date,Upload_Qty,p_no, location , quantity AS qtypending,Sent_Date as Sent_Received_Date  FROM part_history_GR where location='" + part_name + "' and  tool_no ='" + customerId + "'  and station='" + station + "'  AND  quantity>0  order by tool_no asc"));

                }
            }
            else
            {
                if (txtGr.Text.Length > 0)
                {
                    dt = UtilityFunctions.GetData(string.Format(" SELECT tool_no ,Rework ,rwk_no ,p_no,station ,part_name ,Challan_No,quantity,date_of_trans ,Upload_Qty,(case Delivery_commit_date  when '1900-01-01 00:00:00.000' then  ''   else Delivery_commit_date  end) as Delivery_commit_date, location , quantity AS qtypending,Sent_Date as Sent_Received_Date FROM part_history_GR where location='" + part_name + "' and  tool_no ='" + customerId + "' and rwk_no='" + txtGr.Text + "'  AND  quantity>0  order by tool_no asc"));
                }
                else
                {
                    dt = UtilityFunctions.GetData(string.Format(" SELECT tool_no ,Rework ,rwk_no,p_no,station ,part_name ,Challan_No,quantity,date_of_trans ,Upload_Qty,(case Delivery_commit_date  when '1900-01-01 00:00:00.000' then  ''   else Delivery_commit_date  end) as Delivery_commit_date, location , quantity AS qtypending,Sent_Date as Sent_Received_Date FROM part_history_GR where location='" + part_name + "' and  tool_no ='" + customerId + "'   AND  quantity>0  order by tool_no asc"));
                }
            }
        }
        else
        {
            if (station.Length > 0)
            {
                if (txtGr.Text.Length > 0)
                {

                    dt = UtilityFunctions.GetData(string.Format("select tool_no,Rework,Rwk as rwk_no,station,Challan_No,p_no,part_name,req_qty,'' as date_of_trans ,Upload_Qty,(case Delivery_commit_date  when '1900-01-01 00:00:00.000' then  ''   else Delivery_commit_date  end) as Delivery_commit_date,location,(isnull(quant_sent,0)-ISNULL(quant_rec,0)) as qtypending,quant_sent,quant_rec,quant_pend,quant_others,Sent_Received_Date,Received_Date from location_info_GR where  location='" + part_name + "' and  tool_no ='" + customerId + "'  and station='" + station + "' and Rwk='" + txtGr.Text + "' and (isnull(quant_sent,0)-ISNULL(quant_rec,0))>0  order by tool_no asc"));
                }
                else
                {
                    dt = UtilityFunctions.GetData(string.Format("select tool_no,Rework,Rwk as rwk_no,station,Challan_No,p_no,part_name,req_qty,'' as date_of_trans ,Upload_Qty,(case Delivery_commit_date  when '1900-01-01 00:00:00.000' then  ''   else Delivery_commit_date  end) as Delivery_commit_date,location,(isnull(quant_sent,0)-ISNULL(quant_rec,0)) as qtypending,quant_sent,quant_rec,quant_pend,quant_others,Sent_Received_Date,Received_Date from location_info_GR where  location='" + part_name + "' and  tool_no ='" + customerId + "'  and station='" + station + "' and (isnull(quant_sent,0)-ISNULL(quant_rec,0))>0  order by tool_no asc"));
                }
            }
            else
            {
                if (txtGr.Text.Length > 0)
                {
                    dt = UtilityFunctions.GetData(string.Format("select tool_no,Rework,Rwk as rwk_no,station,Challan_No,p_no,part_name,req_qty,'' as date_of_trans  ,Upload_Qty,(case Delivery_commit_date  when '1900-01-01 00:00:00.000' then  ''   else Delivery_commit_date  end) as Delivery_commit_date,location,(isnull(quant_sent,0)-ISNULL(quant_rec,0)) as qtypending,quant_sent,quant_rec,quant_pend,quant_others,Sent_Received_Date,Received_Date from location_info_GR where  location='" + part_name + "' and  tool_no ='" + customerId + "' and Rwk='" + txtGr.Text + "'  and (isnull(quant_sent,0)-ISNULL(quant_rec,0))>0   order by tool_no asc"));
                }
                else
                {
                    dt = UtilityFunctions.GetData(string.Format("select tool_no,Rework,Rwk as rwk_no,station,Challan_No,p_no,part_name,req_qty,'' as date_of_trans  ,Upload_Qty,(case Delivery_commit_date  when '1900-01-01 00:00:00.000' then  ''   else Delivery_commit_date  end) as Delivery_commit_date,location,(isnull(quant_sent,0)-ISNULL(quant_rec,0)) as qtypending,quant_sent,quant_rec,quant_pend,quant_others,Sent_Received_Date,Received_Date from location_info_GR where  location='" + part_name + "' and  tool_no ='" + customerId + "'  and (isnull(quant_sent,0)-ISNULL(quant_rec,0))>0   order by tool_no asc"));
                }
            }
        }

        gvOrders.DataSource = dt;
        Session["BindOrders"] = dt;
        gvOrders.DataBind();
        gvOrders.Columns[7].Visible = true;
    }

    protected void OnOrdersGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvOrders = (sender as GridView);
        gvOrders.PageIndex = e.NewPageIndex;
        DataTable dt = (DataTable)Session["BindOrders"];
        gvOrders.DataSource = dt;
        gvOrders.DataBind();
        //BindOrders(gvOrders.ToolTip, gvOrders);
    }

    protected void Show_Hide_ProductsGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        //int tool = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Value);

        GridViewRow Gv2Row = (GridViewRow)((ImageButton)sender).NamingContainer;
        GridView Childgrid = (GridView)(Gv2Row.Parent.Parent);
        GridViewRow Gv1Row = (GridViewRow)(Childgrid.NamingContainer);
        int b = Gv1Row.RowIndex;

        //ImageButton imgShowHide = (sender as ImageButton);
        //GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlProducts").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            //int orderId = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Value);
            GridView gvProducts = row.FindControl("gvProducts") as GridView;
            HiddenField hdnGrNo = (HiddenField)Childgrid.Rows[row.RowIndex].FindControl("hdnGrNo");
            string Position_no = Childgrid.Rows[row.RowIndex].Cells[1].Text;
            string partname = Childgrid.Rows[row.RowIndex].Cells[2].Text;
            string vendor = Childgrid.Rows[row.RowIndex].Cells[3].Text;
            string station = Childgrid.Rows[row.RowIndex].Cells[4].Text;
            int tool = Convert.ToInt32(Childgrid.Rows[row.RowIndex].Cells[9].Text);

            string rwk_No = Convert.ToString(hdnGrNo.Value);
            //BindOrdersposition(Position_no, gvOrdersp, partname, vendor, station, tool);
            BindProducts(Position_no, gvProducts, partname, vendor, station, tool, rwk_No);
        }
        else
        {
            row.FindControl("pnlProducts").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    private void BindProducts(string position, GridView gvOrdersPosition, string part_name, string vendor, string station, int toolno, string grNo)
    {
        //gvOrdersPosition.ToolTip = orderId.ToString();
        gvOrdersPosition.DataSource = UtilityFunctions.GetData(string.Format("select id,location,date_of_trans,sent_or_rec,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as PendingTotQty_in_PPC,TransSentQty as SendQty_Or_ReceivedQty,TransSentQty from part_history_GR where   p_no='" + position + "' and part_name='" + part_name + "' and tool_no='" + toolno + "' and station='" + station + "' and rwk_No='" + grNo + "'  order by date_of_trans asc "));
        Session["DataTableHistory"] = UtilityFunctions.GetData(string.Format("select id,location,date_of_trans,sent_or_rec,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as PendingTotQty_in_PPC,TransSentQty as SendQty_Or_ReceivedQty,TransSentQty from part_history_GR where   p_no='" + position + "' and part_name='" + part_name + "' and tool_no='" + toolno + "' and station='" + station + "' and rwk_No='" + grNo + "' order by date_of_trans asc "));
        gvOrdersPosition.DataBind();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    public void showgrid()
    {
        string s = "";
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);
            string searchQueryLocationInfo = "";
            string searchQueryPartList = "";
            SearchQuery(out searchQueryPartList, "partlist.", out searchQueryLocationInfo, "locinfo.");


            if (string.IsNullOrEmpty(searchQueryLocationInfo) && string.IsNullOrEmpty(searchQueryPartList))
            {
                searchQueryLocationInfo = "1=1";
                searchQueryPartList = "1=1";
            }

            s = "SELECT   partlist.[tool_no]    , partlist.location     ,sum(partlist.quantity) as QtyPending  FROM part_history_GR partlist where (isnull(((partlist.Upload_Qty)-(isnull(partlist.QtySent,0))),0))>0 and " + searchQueryPartList + " group by partlist.location,partlist.tool_no,partlist.sent_or_rec having partlist.location='COMPANY'  and sum(partlist.quantity)>0 union all  select distinct locinfo.tool_no,locinfo.location,sum(isnull(locinfo.quant_sent,0)-ISNULL(locinfo.quant_rec,0)) as qtypending from location_info_GR locinfo where " + searchQueryLocationInfo + "  group by locinfo.location,locinfo.tool_no having  sum(isnull(locinfo.quant_sent,0)-ISNULL(locinfo.quant_rec,0))>0 and location<>'assembly'order by tool_no asc ";



            SqlCommand cmd = new SqlCommand(s);
            cmd.CommandType = CommandType.Text;
            conn.Open();
            cmd.Connection = conn;
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            Session["DataTableAdvanceReport"] = dt;
            gvCustomers.DataSource = dt;
            gvCustomers.DataBind();
            conn.Close();
            conn.Dispose();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        ExportGridView();
        string strRequest = @"~/foo/GRSummaryRepo.csv";
        //-- if something was passed to the file querystring
        //get absolute path of the file
        if (!string.IsNullOrEmpty(strRequest))
        {
            string path = Server.MapPath(strRequest);
            //get file object as FileInfo
            //string path = AppDomain.CurrentDomain.BaseDirectory + "AverageReport.csv";
            //System.IO.FileInfo file = new System.IO.FileInfo(@"C:\Inetpub\wwwroot\DisplayRecords\CSV\AdvanceReport.csv");
            System.IO.FileInfo file = new System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + "GRSummaryRepo.csv");
            //-- if the file exists on the server
            //set appropriate headers

            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(file.FullName);
                Response.End();
                //if file does not exist
            }
            else
            {
                //Response.Write("This file does not exist.");
            }
            //nothing in the URL as HTTP GET
        }
        else
        {
            //Response.Write("Please provide a file to download.");
        }
    }

    private void ExportGridView()
    {
        string path = AppDomain.CurrentDomain.BaseDirectory + "GRSummaryRepo.csv";
        StreamWriter sw = new StreamWriter(path);

        DataTable dt = (DataTable)Session["DataTableAdvanceReport"];
        DataTable dtexport = new DataTable();


        int iColCount = dt.Columns.Count;
        for (int i = 0; i < iColCount; i++)
        {

            sw.Write(dt.Columns[i]);
            if (i < iColCount - 1)
            {
                sw.Write(",");
            }

        }
        sw.Write(sw.NewLine);
        // Now write all the rows.
        foreach (DataRow dr in dt.Rows)
        {
            for (int i = 0; i < iColCount; i++)
            {
                if (!Convert.IsDBNull(dr[i]))
                {
                    sw.Write(dr[i].ToString());
                }
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
        }
        sw.Close();
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        string filename = "InternalTableData.csv";
        exportData(filename);
    }

    protected void loadExternalData(object sender, EventArgs e)
    {
        string filename = "ExternalTableData.csv";
        exportData(filename);
    }

    protected void exportData(string filename)
    {
        string path = AppDomain.CurrentDomain.BaseDirectory + filename;
        StreamWriter sw = new StreamWriter(path);

        DataTable dt = (DataTable)Session["BindOrders"];
        DataTable dtexport = new DataTable();
        
        int iColCount = dt.Columns.Count;
        for (int i = 0; i < iColCount; i++)
        {
            sw.Write(dt.Columns[i]);
            if (i < iColCount - 1)
            {
                sw.Write(",");
            }

        }
        sw.Write(sw.NewLine);
        // Now write all the rows.
        foreach (DataRow dr in dt.Rows)
        {
            for (int i = 0; i < iColCount; i++)
            {
                if (!Convert.IsDBNull(dr[i]))
                {
                    sw.Write(dr[i].ToString());
                }
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
        }
        sw.Close();

        string strRequest = @"~/foo/"+filename;
        if (!string.IsNullOrEmpty(strRequest))
        {
            string path1 = Server.MapPath(strRequest);
            System.IO.FileInfo file1 = new System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + filename);

            if (!file1.Exists)
            {
                Response.Write("<script language=javascript>alert('File Does Not Exists');</script>");
                //MessageBox.Show("File Does Not Exists");
            }
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file1.Name);
            Response.AddHeader("Content-Length", file1.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(file1.FullName);
            Response.End();
            //if file does not exist
        }
    }

    protected void SearchQuery(out string forparthistoryGr, string aliasPH, out string forlocationinfoGR, string aliasLI)
    {
        string searchQueryPartList = "", searchQueryLocationInfo = "";
        if (tbTool.Text != "")
        {
            searchQueryLocationInfo = aliasLI + " tool_no='" + tbTool.Text + "'";
            searchQueryPartList = aliasPH + "tool_no='" + tbTool.Text + "'";
        }
        if (tbStation.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "station like '" + tbStation.Text + "%'";
            }
            else
            {
                searchQueryLocationInfo = aliasLI + "station like '" + tbStation.Text + "%'";
            }

            if (searchQueryPartList != "")
            {
                searchQueryPartList = searchQueryPartList + " and " + aliasPH + "station like '" + tbStation.Text + "%'";
            }
            else
            {
                searchQueryPartList = aliasPH + "station like '" + tbStation.Text + "%'";
            }
        }
        if (tbLocation.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "location='" + tbLocation.Text + "'";
            }
            else
            {
                searchQueryLocationInfo = aliasLI + "location='" + tbLocation.Text + "'";
            }

            if (searchQueryPartList != "")
            {
                searchQueryPartList = searchQueryPartList + " and " + aliasPH + "location='" + tbLocation.Text + "'";
            }
            else
            {
                searchQueryPartList = aliasPH + "location='" + tbLocation.Text + "'";
            }
        }

        if (txtGr.Text != "")
        {
            if (searchQueryLocationInfo != "")
            {
                searchQueryLocationInfo = searchQueryLocationInfo + " and " + aliasLI + "Rwk = '" + txtGr.Text + "'";
            }
            else
            {
                searchQueryLocationInfo = aliasLI + "Rwk = '" + txtGr.Text + "'";
            }

            if (searchQueryPartList != "")
            {
                searchQueryPartList = searchQueryPartList + " and " + aliasPH + "Rwk_No ='" + txtGr.Text + "'";
            }
            else
            {
                searchQueryPartList = aliasPH + "Rwk_No ='" + txtGr.Text + "'";
            }
        }

        forlocationinfoGR = searchQueryLocationInfo;
        forparthistoryGr = searchQueryPartList;
    }

}