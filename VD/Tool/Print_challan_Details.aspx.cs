﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class Print_challan_Details : System.Web.UI.Page
{
    String MIN = "";
    SqlConnection mycon;
    SqlCommand cmd;
    SqlDataAdapter adp;
    string Challanno = "";
    string Location = "";
   


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            LblDt.Text = System.DateTime.Now.ToString("dd-MMM-yyyy");

            MIN = Session["Min"].ToString();
            lblMino.Text = MIN;
            //Challanno= Session["Challano"].ToString();
            //Location=Session["Location"].ToString();
            //MIN = "CH1326";
            lblMino.Text = Challanno;
            Label1.Text = Challanno;
            Label2.Text = MIN;

            DataTable dt = showData(MIN);
            if (dt.Rows.Count > 0)
            {
                Grdview1.DataSource = dt;
                Session["BindOrders"] = dt;
                Grdview1.DataBind();
            }
            BindOrders(MIN);
            //BindDetails(Challanno, MIN, Location);
            //vendor_detail(MIN);
            string print = "<script language='javascript' type='text/javascript'> window.print(); </script>";
            Page.RegisterStartupScript("Print", print);
            //Page.RegisterStartupScript("Print", print);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
    }

    public DataTable showData(string Challanno)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);

        conn.Open();
        string conString = "select temp.tool_no,temp.location,SUM(temp.Total_pending) as Total_pending,'' as TotalRecQty ,sum(temp.TotUploadqty) as TotUploadqty,temp.Sent_Received_Date,temp.Delivery_commit_date,temp.Challan_No,temp.Explain_Date,temp.sent_or_rec from (select tool_no,sum(Upload_Qty) as TotUploadqty,location,sum(isnull(quant_sent,0)-ISNULL(quant_rec,0)) as Total_pending,(isnull(quant_sent,0)-ISNULL(quant_rec,0)) as qtypending,quant_sent,quant_rec, convert(varchar(30),CONVERT(date,Sent_Received_Date,103)) as Sent_Received_Date,convert(varchar(30),CONVERT(date,Delivery_commit_date,103)) as Delivery_commit_date,convert(varchar(30),CONVERT(date,Explain_Date,103)) as Explain_Date,Challan_No,'' as sent_or_rec from location_info_GR  group by Challan_No,tool_no,location,Upload_Qty,quant_sent,quant_rec,Sent_Received_Date,Received_Date,Delivery_commit_date,Explain_Date  having (isnull(quant_sent,0)-ISNULL(quant_rec,0))>0 and  (location<>'Assembly' and location<>'VD Finish Store')  and Challan_No='" + MIN + "'   )   temp group by temp.Challan_No,temp.tool_no,temp.Delivery_commit_date,temp.Sent_Received_Date,temp.Explain_Date ,temp.location,temp.sent_or_rec  union all select temp2.tool_no,temp2.location,'' as Total_pending,sum(temp2.TransSentQty)  as TotalRecQty ,SUM(temp2.Upload_Qty) as TotalUpload_Qty,convert(varchar(30),CONVERT(date,temp2.date_of_trans,103)) as Sent_Received_Date,convert(varchar(30),CONVERT(date,temp2.Delivery_commit_date,103)) as Delivery_commit_date,temp2.Challan_No,convert(varchar(30),CONVERT(date,temp2.Explain_Date,103)) as Explain_Date,temp2.sent_or_rec from(SELECT distinct tool_no ,station,date_of_trans,p_no ,location ,quantity ,QtySent ,TransSentQty ,Upload_Qty ,Explain_Date ,Delivery_commit_date, Challan_No,sent_or_rec FROM part_history_GR where TransSentQty>0  and (location<>'Assembly' and location<>'VD Finish Store')   and sent_or_rec='Received' and  Challan_No='" + MIN + "')temp2 group by Challan_No,temp2.tool_no,temp2.location,convert(varchar(30),CONVERT(date,temp2.date_of_trans,103)),convert(varchar(30),CONVERT(date,temp2.Delivery_commit_date,103)) ,convert(varchar(30),CONVERT(date,temp2.Explain_Date,103)),temp2.sent_or_rec ";
        cmd = new SqlCommand(conString, conn);
        adp = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        adp.Fill(dt);
        conn.Close();
        return dt;
    }
      

    private void BindOrders(string challan_no)
    {
       
        DataTable dt = new DataTable();
        dt = GetData(string.Format("select tool_no,station,p_no,location,'' as TransSentQty,Upload_Qty,(isnull(quant_sent,0)-ISNULL(quant_rec,0)) as qtypending,quant_sent,quant_rec from location_info_GR  where (isnull(quant_sent,0)-ISNULL(quant_rec,0))>0 and  (location<>'Assembly' and location<>'VD Finish Store' ) and Challan_No='" + challan_no + "'  union all SELECT tool_no,station, p_no,location,quantity, Upload_Qty ,'' as qtypending,TransSentQty,'' as quant_rec  FROM part_history_GR where Challan_No='" + challan_no + "' and sent_or_rec='Received' and (location<>'Assembly' and location<>'VD Finish Store' ) and TransSentQty>0 "));

        GridView2.DataSource = dt;
        Session["BindOrders"] = dt;
        GridView2.DataBind();
        //gvOrders.Columns[7].Visible = true;
    }

    private static DataTable GetData(string query)
    {
        string constr = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = query;
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataSet ds = new DataSet())
                    {
                        DataTable dt = new DataTable();
                        sda.Fill(dt);

                        return dt;
                    }
                }
            }
        }
    }
    

   
    protected void Grdview1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label result = (Label)e.Row.FindControl("lblSent_Received_Date");
            Label result1 = (Label)e.Row.FindControl("lblExplain_Date");
            Label result2 = (Label)e.Row.FindControl("lblsub_head1");
            if (result1.Text == "")
            {
                result1.Text = "01/01/1900";
            }
            if (result2.Text == "")
            {
                result2.Text = "01/01/1900";
            }

            if (result.Text == "")
            {
                result.Text = "01/01/1900";
            }

            DateTime date1 = new DateTime();
            date1 = Convert.ToDateTime(result.Text);
            DateTime date2 = new DateTime();
            date2 = Convert.ToDateTime(result1.Text);
            DateTime date3 = new DateTime();
            date3 = Convert.ToDateTime(result2.Text);
            result.Text = date1.ToString("dd/MM/yyyy");
            result1.Text = date2.ToString("dd/MM/yyyy");
            result2.Text = date3.ToString("dd/MM/yyyy");

            //string result = (e.Row.Cells[2].Text);
            if (result.Text == "01/01/1900")
            {
                result.Text = "";
            }
            else
            {

            }

            if (result1.Text == "01/01/1900")
            {
                result1.Text = "";
            }
            else
            {

            }
            if (result2.Text == "01/01/1900" || result2.Text == "1900-01-01")
            {
                result2.Text = "";
            }
            else
            {

            }

        }

    }

    protected void Grdview2_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
}