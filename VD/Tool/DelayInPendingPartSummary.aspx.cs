﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class Tool_DelayInPendingPartSummary : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    /// <summary>
    /// Retrieves the data for Delay in Received Parts through Stored Procedure
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        try
        {
            gridViewVendorPendingItems.DataSource = null;
            gridViewVendorPendingItems.DataBind();
            using (SqlConnection conn = new SqlConnection(sqlconnstring))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("usp_DelayInReceivedPartSummaryReport", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (!string.IsNullOrEmpty(inputToolNo.Text))
                        cmd.Parameters.AddWithValue("@ToolNo", Convert.ToInt32(inputToolNo.Text));
                    if (!string.IsNullOrEmpty(inputVendorName.Text))
                        cmd.Parameters.AddWithValue("@Location", Convert.ToString(inputVendorName.Text));
                    gridViewVendorPendingItems.DataSource = cmd.ExecuteReader();
                    gridViewVendorPendingItems.DataBind();
                    gridViewVendorPendingItems.EmptyDataText = "No Records Found";
                }
            }

        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void ExportToExcel()
    {
        Response.ClearContent();
        string fileName = "DelayInPendingParts_Report";
        string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gridViewVendorPendingItems.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    /// <summary>
    /// Retrieves the Vendor Standard Days from Vendor Master Job Work table
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewVendorPendingItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);
            try
            {
                string location = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "location"));
                if (location == "Inspection" || location == "K R Casting Pvt Ltd")
                {
                    location = " " + location;
                }
                Label lblStandardDays = (Label)e.Row.FindControl("lblStandardDays");
                conn.Open();
                SqlCommand cmd = new SqlCommand("select Standard_Days from Vendor_Master_Job_work where Vendor_Name=@Location", conn);
                cmd.Parameters.AddWithValue("@Location", location);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        lblStandardDays.Text = Convert.ToString(reader["Standard_Days"]);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured. Please Contact Administrator.');", true);
                return;
            }
            finally
            {
                conn.Close();
            }
        }
    }

    /// <summary>
    /// When user clicks on Plus/Minus button shown in front of each row then this method is called. 
    /// It calls ShowChallanItemsGrid method to show the data in Inner Grid View on Plus Click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Show_Hide_ChallanItemsGrid(object sender, EventArgs e)
    {
        try
        {
            ImageButton imgShowHide = (sender as ImageButton);
            GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);

            Panel panel = (Panel)imgShowHide.FindControl("pnlChallanItems");
            GridView gvChallanItems = (GridView)imgShowHide.FindControl("gvChallanItems");

            if (imgShowHide.CommandArgument == "Show")
            {
                panel.Visible = true;
                imgShowHide.CommandArgument = "Hide";
                imgShowHide.ImageUrl = "~/images/minus.png";

                string location = gridViewVendorPendingItems.Rows[row.RowIndex].Cells[1].Text;
                ShowChallanItemsGrid(gvChallanItems, location);
            }
            else
            {
                panel.Visible = false;
                imgShowHide.CommandArgument = "Show";
                imgShowHide.ImageUrl = "~/images/plus.png";
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Retrieves and Binds the Data in the Grid View for Clicked Location
    /// </summary>
    /// <param name="gvChallanItems"></param>
    /// <param name="location"></param>
    protected void ShowChallanItemsGrid(GridView gvChallanItems, string location)
    {
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            string searchCondition = "";
            if (location == "Inspection" || location == "K R Casting Pvt Ltd")
            {
                location = " " + location + "";
            }
            searchCondition = "lig.location='" + location + "'";
            if (!string.IsNullOrEmpty(inputToolNo.Text))
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and lig.Tool_No='" + inputToolNo.Text + "'";
                }
                else
                {
                    searchCondition = " lig.Tool_No='" + inputToolNo.Text + "'";
                }
            }
            string query = " select  lig.Challan_No,lig.tool_no, lig.part_name,lig.p_no,lig.station, lig.rwk_no,phgr.Upload_Qty, isnull(lig.outstanding_qty,0) as quantity, "
             + " lig.location, lig.Send_Receive_Date as Sent_Date, DateDiff(Day,Convert(Date,lig.Send_Receive_Date), "
             + " Convert(Date,GetDate())) as Total_Days , vdm.Standard_Days, (DateDiff(Day,Convert(Date,lig.Send_Receive_Date), "
             + " Convert(Date,GetDate()))*100)/vdm.Standard_Days as Percentage, "
             + " case when (DateDiff(Day,Convert(Date,lig.Send_Receive_Date), Convert(Date,GetDate()))*100)/vdm.Standard_Days<=100 then 1 "
             + " when ((DateDiff(Day,Convert(Date,lig.Send_Receive_Date),Convert(Date,GetDate()))*100)/vdm.Standard_Days>100 and "
             + " (DateDiff(Day,Convert(Date,lig.Send_Receive_Date),Convert(Date,GetDate()))*100)/vdm.Standard_Days<=200) then 2 "
             + "  when ((DateDiff(Day,Convert(Date,lig.Send_Receive_Date),Convert(Date,GetDate()))*100)/vdm.Standard_Days>200 and "
             + " (DateDiff(Day,Convert(Date,lig.Send_Receive_Date),Convert(Date,GetDate()))*100)/vdm.Standard_Days<=300) then 3 "
             + " when (DateDiff(Day,Convert(Date,lig.Send_Receive_Date),Convert(Date,GetDate()))*100)/vdm.Standard_Days>300 then 4 end as Category "
             + " from Challan_Sent_Received_Detail lig left join Vendor_Master_Job_work vdm on lig.location= vdm.Vendor_Name "
             + " left join part_history_gr phgr on lig.tool_no=phgr.tool_no and lig.station=phgr.station and lig.p_no=phgr.p_no and "
             + " lig.part_name= phgr.part_name and lig.rwk_no= phgr.rwk_no and phgr.location='Company' "
             + " where lig.tool_no is not null  and lig.outstanding_qty>0 and vdm.Standard_Days>0 and lig.Sent_Or_Rec='Sent' and "
             + " lig.location not in ('VD Finish Store','Assembly','FinishPartStore','weld vd finish store','Weld Parts Consumed','Reject Parts')  and " + searchCondition + "";
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(sqlconnstring))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    gvChallanItems.DataSource = cmd.ExecuteReader();
                    gvChallanItems.DataBind();
                    gvChallanItems.EmptyDataText = "No Records Found";
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

    }

    /// <summary>
    /// Retrieves the Original Commit Date, Last Commit Date, Delay from Sent Date, Delay from Original Commit Date
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvChallanItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);
            try
            {
                string Positionno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "p_no"));
                string part_name = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "part_name"));
                string station = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "station"));
                string rwk_no = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "rwk_no"));
                string toolno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "tool_no"));
                string location = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "location"));
                string challanNo = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Challan_No"));
                conn.Open();
                string id = "";
                SqlCommand cmd = new SqlCommand("select top 1 id from part_history_Gr where p_no='" + Positionno + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk_no='" + rwk_no + "' and tool_no='" + toolno + "' and location='" + location + "' and Challan_No='" + challanNo + "'  and sent_or_rec='Sent' order by date_of_trans desc", conn);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        id = Convert.ToString(reader["id"]);
                    }
                }
                Label lblOriginalCommitDate = (Label)e.Row.FindControl("lblOriginalCommitDate");
                Label lblLastCommitDate = (Label)e.Row.FindControl("lblLastCommitDate");

                string query = "select  Tool_No,P_No,Part_Name,Station,Rework_No,Location,Commitment_Date  from CommitmentHistory  where  Part_History_Id=" + id + " union all select  Tool_No,P_No,Part_Name,Station,Rwk_No,Location,Delivery_commit_date  from part_history_Gr  where  id=" + id + " order by Commitment_Date asc";
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                DataTable table = new DataTable();
                da.Fill(table);
                if (table.Rows.Count > 0)
                {
                    if (table.Rows[0]["Commitment_Date"] != DBNull.Value)
                        lblOriginalCommitDate.Text = Convert.ToDateTime(table.Rows[0]["Commitment_Date"]).ToString("dd/MM/yyyy");
                }

                string query1 = "select  Tool_No,P_No,Part_Name,Station,Rework_No,Location,Commitment_Date  from CommitmentHistory  where  Part_History_Id=" + id + " union all select  Tool_No,P_No,Part_Name,Station,Rwk_No,Location,Delivery_commit_date  from part_history_Gr  where  id=" + id + " order by Commitment_Date desc ";
                SqlDataAdapter da1 = new SqlDataAdapter(query1, conn);
                DataTable table1 = new DataTable();
                da1.Fill(table1);
                if (table1.Rows.Count > 0)
                {
                    if (table.Rows[0]["Commitment_Date"] != DBNull.Value)
                        lblLastCommitDate.Text = Convert.ToDateTime(table.Rows[0]["Commitment_Date"]).ToString("dd/MM/yyyy");
                }
                Label lblDelayFromSentDate = (Label)e.Row.FindControl("lblDelayFromLastSentDate");
                Label lblDelayFromFirstCommitDate = (Label)e.Row.FindControl("lblDelayFromFirstCommitDate");

                if (DataBinder.Eval(e.Row.DataItem, "Sent_Date") != DBNull.Value)
                {
                    string sentDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "Sent_Date")).ToString("dd-MM-yyyy");
                    string currentDate = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy");
                    int days = Convert.ToDateTime(currentDate).Subtract(Convert.ToDateTime(sentDate)).Days;
                    if (days > 0)
                    {
                        lblDelayFromSentDate.Text = Convert.ToString(days);
                    }
                }
                if (!string.IsNullOrEmpty(lblOriginalCommitDate.Text))
                {
                    string originalCommitDate = Convert.ToDateTime(lblOriginalCommitDate.Text).ToString("dd-MM-yyyy");
                    string currentDate = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy");
                    int days = Convert.ToDateTime(currentDate).Subtract(Convert.ToDateTime(originalCommitDate)).Days;
                    if (days > 0)
                    {
                        lblDelayFromFirstCommitDate.Text = Convert.ToString(days);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured. Please Contact Administrator.');", true);
                return;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}