﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using System.Diagnostics;

public partial class PrintVendorStatusReport : System.Web.UI.Page
{
    /// <summary>
    /// Generates the Vendor Status Report for Passed Location, Supervisor and Vendor Type
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Loc"]) && !string.IsNullOrEmpty(Request.QueryString["Sup"]) && !string.IsNullOrEmpty(Request.QueryString["Type"]))
            {
                divVendorStatusReport.InnerHtml = GenerateReport(Convert.ToString(Request.QueryString["Loc"]), Convert.ToString(Request.QueryString["Sup"]), Convert.ToString(Request.QueryString["Type"])).ToString();

                string print = "<script language='javascript' type='text/javascript'> window.print(); </script>";
                Page.RegisterStartupScript("Print", print);
            }
        }
    }

    /// <summary>
    /// Retrieves the Data for the Location , Supervisor and Vendor Type passed in the Method and creates Html for it
    /// </summary>
    /// <param name="location"></param>
    /// <param name="supervisor"></param>
    /// <param name="vendortype"></param>
    /// <returns></returns>
    protected StringBuilder GenerateReport(string location, string supervisor, string vendortype)
    {
        StringBuilder sb = new StringBuilder();
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);

        try
        {
            string vendor = "Vendor", allocatedUser = "Supervisor";
            if (location != "All")
            {
                vendor = location;
                allocatedUser = GetSupervisorName(vendor);
            }
            DataTable dt = UtilityFunctions.GetDataForVendorStatusReport(location, supervisor, vendortype);

            sb.Append("<table style='width:1100px;border-collapse:collapse;font-size: 12px;line-height: 22px;font-family:Verdana;text-align:center;' class='report-container'><thead class='report-header'><tr> <th colspan='7' class='report-header-cell' ><h2 style='text-align:center;'>Status Confirmation report for " + vendor + " / " + allocatedUser + " as on " + DateTime.Now.ToString("dd-MM-yyyy") + "</h2></th></tr></thead>");

            sb.Append("");
            sb.Append("<tr>");
            sb.Append("<td style='border:1px solid black;font-weight:bold;width:100px;'>Supervisor Name</td> <td style='border:1px solid black;font-weight:bold;width:200px;'>Vendor Name</td>");
            sb.Append("<td style='border:1px solid black;font-weight:bold;width:80px;'>Challan No</td> <td style='border:1px solid black;font-weight:bold;width:80px;'>Tool No</td> <td style='border:1px solid black;font-weight:bold;width:70px;'>Station</td>");//<td style='border:1px solid black;font-weight:bold;width:80px;'>Date Of Trans</td> 
            sb.Append("<td style='border:1px solid black;font-weight:bold;width:120px;'>GR/Position/Qty</td> <td style='border:1px solid black;font-weight:bold;width:100px;'>Verified By</td></tr>");

            var result =
                   dt.AsEnumerable()
                       .Select(p => new
                       {
                           allocatedTo = p.Field<string>("Allocated_To"),
                           vendorName = p.Field<string>("Vendor_Name"),
                           vendorType = p.Field<string>("Vendor_Type"),
                           tool = p.Field<int>("Tool_No"),
                           station = p.Field<string>("Station"),
                           verifiedBy = p.Field<string>("Verified_By"),
                           challanNo = p.Field<string>("Challan_No")//,
                           //dateOfTrans = p.Field<DateTime>("Date_Of_Trans")

                       }).Distinct().Select(p => new { Allocated_To = p.allocatedTo, Vendor_Name = p.vendorName, Vendor_Type = p.vendorType, Tool_No = p.tool, Station = p.station, Verified_By = p.verifiedBy, Challan_No = p.challanNo })//, Date_Of_Trans = p.dateOfTrans
                       .ToList();

            DataTable dtOutStandingParts = new DataTable();
            dtOutStandingParts.Columns.Add("Allocated_To", typeof(string));
            dtOutStandingParts.Columns.Add("Vendor_Name", typeof(string));
            dtOutStandingParts.Columns.Add("Vendor_Type", typeof(string));
            dtOutStandingParts.Columns.Add("Tool_No", typeof(string));
            dtOutStandingParts.Columns.Add("Station", typeof(string));
            dtOutStandingParts.Columns.Add("Verified_By", typeof(string));
            dtOutStandingParts.Columns.Add("Challan_No", typeof(string));
            //dtOutStandingParts.Columns.Add("Date_Of_Trans", typeof(DateTime));
            dtOutStandingParts.Columns.Add("Part_Info", typeof(string));
            int total = 0;
            for (int i = 0; i < result.Count; i++)
            {
                string allocatedTo = result[i].Allocated_To;
                string vendorName = result[i].Vendor_Name;
                string vendorType = result[i].Vendor_Type;
                int tool = result[i].Tool_No;
                string station = result[i].Station;
                string verifiedBy = result[i].Verified_By;
                string challanNo = result[i].Challan_No;
                //DateTime dateOfTrans = result[i].Date_Of_Trans;
                string html = "";
                DataRow dr = dtOutStandingParts.NewRow();
                DataRow[] drPartInfo = dt.Select("Allocated_To='" + allocatedTo + "' and Vendor_Name='" + vendorName + "' and Vendor_Type='" + vendorType + "' and Tool_No='" + tool + "' and Station='" + station + "' and Verified_By='" + verifiedBy + "' and Challan_No='" + challanNo + "'");
                if (drPartInfo.Length > 0)
                {
                    foreach (DataRow drRow in drPartInfo)
                    {
                        html = drRow["Rwk"] + "," + drRow["P_No"] + "," + drRow["PendingQty"] + " | " + html;
                        total = total + 1;
                    }
                    html = html.TrimEnd(' ');
                    html = html.TrimEnd('|');
                }

                int srNo = i + 1;
                sb.Append("<tr>");
                sb.Append("<td style='border:1px solid black;'>" + allocatedTo + "</td>");
                sb.Append("<td style='border:1px solid black;'>" + vendorName + "</td>");
                sb.Append("<td style='border:1px solid black;'>" + challanNo + "</td>");
                //sb.Append("<td style='border:1px solid black;'>" + Convert.ToDateTime(dateOfTrans).ToShortDateString() + "</td>");
                sb.Append("<td style='border:1px solid black;'>" + tool + "</td>");
                sb.Append("<td style='border:1px solid black;'>" + station + "</td>");
                sb.Append("<td style='border:1px solid black;'>" + html + "</td>");
                sb.Append("<td style='border:1px solid black;'>" + verifiedBy + "</td>");
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return sb;
        }
        finally
        {
            connec.Close();
        }
    }

    /// <summary>
    /// Retrieves the Supervisor Name for Passed Vendor as Parameter
    /// </summary>
    /// <param name="vendorName"></param>
    /// <returns></returns>
    protected string GetSupervisorName(string vendorName)
    {

        string allocatedTo = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            connec.Open();
            SqlCommand cmd = new SqlCommand("select Allocated_To from Vendor_Master_Job_Work where Vendor_Name=@VendorName", connec);
            cmd.Parameters.AddWithValue("@VendorName", vendorName);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                if (rdr.HasRows)
                {
                    rdr.Read();
                    allocatedTo = Convert.ToString(rdr["Allocated_To"]);
                }
            }
            return allocatedTo;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            connec.Close();
        }
    }
}