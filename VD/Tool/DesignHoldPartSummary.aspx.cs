﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text;
using AjaxControlToolkit;

public partial class Tool_DesignHoldPartSummary : System.Web.UI.Page
{
    string location, station, part_name, str_dt, s;
    string approvername = "";
    int tool_no, quant_sent, quant_rec, new_quantity_rec, quant_pend;
    string p_no = "";
    SqlConnection connection;

    SqlCommand cmd;
    int countchk = 0;
    static bool isRequestedForReceive = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        txtReceivecdate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
        if (!IsPostBack)
        {
            isRequestedForReceive = false;
            Panel4.Visible = true;
            Panel2.Visible = false;
            txtchallanno.Visible = false;
            lblChallanno.Visible = false;
            btnprint.Visible = false;
            Panel1.Visible = false;
            btnRecieved.ID = "btnRecieved";
        }
    }

    /// <summary>
    /// Displays the Overall Pending Data for Design Location from Location Info Gr table. User can receive that data back to the Company.
    /// </summary>
    public void showgrid()
    {
        lbl.Text = "";
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            connection = new SqlConnection(sqlconnstring);
            connection.Open();
            string searchCondtion = "lig.location='Design'";
            if (txtprojectno.Text != "")
            {
                searchCondtion = searchCondtion + " and lig.tool_no ='" + txtprojectno.Text + "'";
            }
            cmd = new SqlCommand("select  lig.tool_no,lig.station,lig.Upload_Qty,lig.p_no,lig.part_name,lig.quant_sent,lig.quant_rec,isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0) as SentQty,lig.Rwk from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where (isnull(lig.quant_sent,0)-isnull(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchCondtion + " order by lig.tool_no,lig.station asc");
            cmd.Connection = connection;
            DataTable dt = new DataTable();

            dt.Load(cmd.ExecuteReader());
            Panel1.Visible = true;
            if (dt.Rows.Count <= 0)
            {
                dt = null;
            }
            btnRecieved.Visible = false;
            GridView1.DataSource = dt;
            Session["DataTableAdvanceReport"] = dt;
            GridView1.DataBind();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        showgrid();
        if (GridView1.Rows.Count == 1)
            lbl.Text = "No previous data exists..!!";
    }

    /// <summary>
    /// Retrieves the Uploaded Excel File download link for each row, and last Expected Delivery Date
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            NumericUpDownExtender ne = (NumericUpDownExtender)e.Row.FindControl("numericupdownextender3");
            Label l = (Label)e.Row.FindControl("Label5");
            LinkButton lnkBtnLastCommitmentDate = (LinkButton)e.Row.FindControl("lnkBtnLastCommitmentDate");

            CheckBox cb = (CheckBox)e.Row.FindControl("chkAllchild");
            ne.Maximum = Convert.ToDouble(l.Text);
            HyperLink hyperLnkExcelFile = (HyperLink)e.Row.FindControl("hyperLnkExcelFile");
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection connec = new SqlConnection(sqlconnstring);
            connec.Open();
            string pno = "", partname = "", station = "", rwkno = "", toolno = "";
            pno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "p_no"));
            partname = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "part_name"));
            station = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "station"));
            rwkno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "rwk"));
            toolno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "tool_no"));
            string uploadedFileName = "";

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand("select Uploaded_File_Name from Part_History_Gr where tool_no='" + toolno + "'  and  p_no='" + pno + "' and part_name='" + partname + "' and location='COMPANY' and station='" + station + "' and rwk_no='" + rwkno + "' and sent_or_rec='Uploaded'", connec);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        uploadedFileName = Convert.ToString(reader["Uploaded_File_Name"]);
                    }
                }

                if (uploadedFileName != "")
                {
                    hyperLnkExcelFile.NavigateUrl = "~/UploadedExcelFiles/" + uploadedFileName;
                    hyperLnkExcelFile.Text = "Download Excel";
                }

                cmd = new SqlCommand("select top 1 Delivery_commit_date from Part_History_Gr where tool_no='" + toolno + "'  and  p_no='" + pno + "' and part_name='" + partname + "' and location='Design' and station='" + station + "' and rwk_no='" + rwkno + "' and sent_or_rec='Sent' order by date_of_trans desc", connec);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        lnkBtnLastCommitmentDate.Text = Convert.ToString(reader["Delivery_commit_date"]);
                    }
                }

                if (!cb.Checked)
                {
                    ne.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
            }
            finally
            {
                connec.Close();
            }
        }
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        showgrid();
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        showgrid();
    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    /// <summary>
    /// Called when user selects any Positon
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAllchild_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)GridView1.HeaderRow.FindControl("chkAll1");
        txtchallanno.Text = String.Empty;
        lblChallanno.Visible = false;
        txtchallanno.Visible = false;
        btnprint.Visible = false;
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            if (ChkBoxRows.Checked == true)
            {
                ChkBoxRows.Checked = true;

                countchk = countchk + 1;
                GridView1.Columns[7].Visible = true;
                btnRecieved.Visible = true;
                btnUpdateAsInactive.Visible = true;
                EnableTextBox();
            }
            else
            {
                ChkBoxRows.Checked = false;
                int count = int.Parse(GridView1.Rows.Count.ToString());
                for (int i = 0; i < count; i++)
                {
                    CheckBox cb = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkAllchild");
                    if (cb.Checked == true)
                    {
                        GridView1.Columns[7].Visible = true;
                    }
                    else
                    {
                        GridView1.Columns[7].Visible = true;
                        btnRecieved.Visible = false;
                        btnUpdateAsInactive.Visible = false;
                    }
                }
            }
        }

        if (countchk > 0)
        {
            GridView1.Columns[7].Visible = true;
            GridView1.Columns[9].Visible = true;
            btnRecieved.Visible = true;
            btnUpdateAsInactive.Visible = true;
        }
        else
        {
            btnRecieved.Visible = true;
            GridView1.Columns[7].Visible = false;
            btnRecieved.Visible = false;
            btnUpdateAsInactive.Visible = false;
        }
        UpdatePanel1.Update();
    }

    /// <summary>
    /// Called when Check all Check Box is selected by the User.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAll1_CheckedChanged1(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)GridView1.HeaderRow.FindControl("chkAll1");
        txtchallanno.Text = String.Empty;
        lblChallanno.Visible = false;
        txtchallanno.Visible = false;
        btnprint.Visible = false;
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
                countchk = countchk + 1;
                EnableTextBox();
            }
            else
            {
                ChkBoxRows.Checked = false;
                int count = int.Parse(GridView1.Rows.Count.ToString());

            }
        }
    }

    /// <summary>
    /// Enables/Disables the checked boxes on check/uncheck of GridView Row
    /// </summary>
    protected void EnableTextBox()
    {
        int count = int.Parse(GridView1.Rows.Count.ToString());

        for (int i = 0; i < count; i++)
        {
            CheckBox cb = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkAllchild");
            if (cb.Checked)
            {
                btnRecieved.Visible = true;

                TextBox TextBox2 = (TextBox)GridView1.Rows[i].Cells[7].FindControl("TextBox2");
                Label Label6sentqty = (Label)GridView1.Rows[i].Cells[9].FindControl("Label6sentqty");

                if (Label6sentqty.Text.Length > 0)
                {
                    TextBox2.Text = Label6sentqty.Text;
                    TextBox2.Enabled = true;
                }
                else
                {
                    TextBox2.Text = "";
                }
            }
            else
            {
                TextBox TextBox2 = (TextBox)GridView1.Rows[i].Cells[7].FindControl("TextBox2");
                TextBox2.Text = "";
                TextBox2.Enabled = false;
                btnRecieved.Visible = false;
            }
        }
    }

    protected void txtbarcode_TextChanged(object sender, EventArgs e)
    {
        DateTime lastKeyPress = DateTime.Now;
        if (((TimeSpan)(DateTime.Now - lastKeyPress)).Seconds <= 0.5)
        {
            string str = null;
            string[] strArr = null;
            str = txtbarcode.Text;
            char[] splitchar = { ' ' };
            strArr = str.Split(splitchar);
            txttoolno.Text = strArr[0];
            txtstationno.Text = strArr[1];
            txtposition.Text = strArr[2];
            fill_qty();
            ViewState["toolno"] = txttoolno.Text;
            ViewState["stationno"] = txtstationno.Text;
            ViewState["position"] = txtposition.Text;
            ViewState["Qty"] = txtQty.Text;
            ViewState["SENT"] = txtsentQty1.Text;
            ViewState["vendor"] = ddllocauto.SelectedItem.Text;
            ViewState["ItemName"] = txtname1.Text;
            ViewState["TotalsentQty"] = txttotalsent.Text;
            txtbarcode.Text = "";
            txtbarcode.Focus();
        }
        else
        {
            txtbarcode.Enabled = false;
        }
        lastKeyPress = DateTime.Now;
    }

    public void fill_qty()
    {
        SqlCommand cmd;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);

        conn.Open();
        try
        {
            if (txtstationno.Text.Length <= 0 && txtposition.Text.Length <= 0 && txttoolno.Text.Length <= 0)
            {
                MessageBox(" Tool no or Station No or Position No should not be empty");
                return;
            }

            cmd = new SqlCommand("select tool_no,station,p_no,part_name,quant_sent,quant_rec from location_info_GR where location=@location and tool_no=@tool_no and station=@station_no and p_no=@positio_no and isnull(quant_sent,0)>=ISNULL(quant_rec,0)");

            cmd.Parameters.AddWithValue("@tool_no", txttoolno.Text);
            cmd.Parameters.AddWithValue("@station_no", txtstationno.Text);
            cmd.Parameters.AddWithValue("@positio_no", txtposition.Text);
            cmd.Parameters.AddWithValue("@location", ddllocauto.SelectedItem.Text);
            cmd.Connection = conn;
            DataTable dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            if (dt.Rows.Count > 0)
            {
                txtQty.Text = dt.Rows[0]["quant_sent"].ToString();
                txtsentQty1.Text = dt.Rows[0]["quant_rec"].ToString();
                txtname1.Text = dt.Rows[0]["part_name"].ToString();
                txttotalsent.Text = dt.Rows[0]["quant_sent"].ToString();
            }
            else
            {
                txtQty.Text = "0";
                txtsentQty1.Text = "0";
                txtname1.Text = "";
                txttotalsent.Text = "0";
                MessageBox("Record Not Found");
                return;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);

        }
        finally
        {
            conn.Close();
        }
    }

    protected void txtQty_TextChanged(object sender, EventArgs e)
    {
        if (txtQty.Text.Length > 0)
        {
            ViewState["Qty"] = txtQty.Text;
            txtbarcode.Focus();
        }
    }

    /// <summary>
    /// This methods receives the Items In the company when user clicks on Receive Button.
    /// Inserts the new Row in Part History Gr table as Received, Part History QC Gr table, Updates the Quantities(Adds the received Quantity) in Part History Gr table for Company Location.
    /// Upates the Quantities at Location Info Gr table
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRecieved_Click(object sender, EventArgs e)
    {

        btnRecieved.Attributes.Add("onclick", " this.disabled = true; " + ClientScript.GetPostBackEventReference(btnRecieved, null) + ";");
        approvername = Session["UserID"].ToString();
        approvername = approvername.Replace("_", " ");


        string prefixStr = "CH";
        string generatedChallanNumber = "";
        generatedChallanNumber = UtilityFunctions.gencode(prefixStr);
        if (!string.IsNullOrEmpty(generatedChallanNumber))
        {
            txtchallanno.Text = generatedChallanNumber;
            txtchallanno.Visible = true;
            lblChallanno.Visible = true;
            btnprint.Visible = true;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Challan Number generation Failed.');", true);
            return;
        }
        int count = 0;
        int uploadqty1 = 0;
        object request_id = "";
        string sqlconnstring1 = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring1);
        SqlTransaction trans = null;
        if (!isRequestedForReceive)
        {
            if (txtReceivecdate.Text.Length <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Enter Sending Date');", true);
                return;
            }
            try
            {
                conn.Open();
                trans = conn.BeginTransaction();
                foreach (GridViewRow row in GridView1.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        DateTime str_dt1 = Convert.ToDateTime(txtReceivecdate.Text);
                        str_dt = string.Format("{0:yyyy-MM-dd}", str_dt1); // you can specify format 
                        Label quant_sent1 = row.FindControl("Label5") as Label;
                        Label quant_rec1 = row.FindControl("Label6") as Label;
                        TextBox new_quantity_rec1 = row.FindControl("TextBox2") as TextBox;
                        Label station1 = row.FindControl("Label2") as Label;
                        Label rework_no = row.FindControl("lblrew") as Label;
                        Label lblrew = row.FindControl("lblrew") as Label;

                        Label tool_no1 = row.FindControl("Label1") as Label;
                        LinkButton POSITION = row.FindControl("Label3") as LinkButton;
                        Label part_name1 = row.FindControl("Label4") as Label;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        Label quantity = row.FindControl("Label8") as Label;
                        Label uploadqty = row.FindControl("Label6Upload_Qty") as Label;
                        Label sent = row.FindControl("Labelsent") as Label;
                        quant_sent = Convert.ToInt32(quant_sent1.Text);
                        quant_rec = Convert.ToInt32(quant_rec1.Text);
                        uploadqty1 = Convert.ToInt32(uploadqty.Text);
                        tool_no = Convert.ToInt32(tool_no1.Text);
                        p_no = POSITION.Text;
                        station = station1.Text;
                        part_name = Convert.ToString(part_name1.Text);
                        location = "Design";
                        string kk = System.DateTime.Now.ToLongTimeString();
                        str_dt = str_dt + " " + kk;

                        bool isValidData = false;
                        bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                        if (isSelected)
                        {
                            SqlCommand cmdCheckQuantity = new SqlCommand("select tool_no,station,Upload_Qty,p_no,part_name,quant_sent,quant_rec,isnull(quant_sent,0)-isnull(quant_rec,0) as SentQty,Rwk from location_info_GR where location='Design' and (isnull(quant_sent,0)-isnull(quant_rec,0))>0 and location='Design' and p_no='" + p_no + "' and part_name='" + part_name + "' and tool_no='" + tool_no + "' "
                            + " and station='" + station + "' and rwk='" + rework_no.Text + "'  order by tool_no,station asc", conn, trans);
                            using (SqlDataReader reader = cmdCheckQuantity.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    reader.Read();
                                    int quantSent = Convert.ToInt32(reader["quant_sent"]);
                                    int quantReceive = Convert.ToInt32(reader["quant_rec"]);
                                    int uploadQty = Convert.ToInt32(reader["Upload_Qty"]);
                                    new_quantity_rec = Convert.ToInt32(new_quantity_rec1.Text);
                                    if ((new_quantity_rec + quantReceive) <= quantSent && new_quantity_rec <= uploadQty)
                                    {
                                        isValidData = true;
                                    }
                                }
                            }
                            if (!isValidData)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Quantity Entered. Quantiy has been modified Please refresh the Page.');", true);
                                return;
                            }

                            if (new_quantity_rec1.Text.Length <= 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Enter Sending Qty ');", true);
                                return;
                            }
                            new_quantity_rec = Convert.ToInt32(new_quantity_rec1.Text);
                            if ((quant_sent) < new_quantity_rec + quant_rec)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Receive Qty should not be greater than sent Qty');", true);
                                return;
                            }
                            s = "insert into part_history_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,TransSentQty,Upload_Qty,Challan_No,rwk_no) values     (@p_no,@part_name,@tool_no,@station,@location,@date_of_trans,@sent_or_rec,@quantity,@TransSentQty,@Upload_Qty,@Challan_No,@Rwk);SELECT SCOPE_IDENTITY()";
                            cmd = new SqlCommand(s, conn, trans);
                            cmd.Parameters.AddWithValue("@p_no", p_no);
                            cmd.Parameters.AddWithValue("@part_name", part_name);
                            cmd.Parameters.AddWithValue("@tool_no", tool_no);
                            cmd.Parameters.AddWithValue("@station", station);
                            cmd.Parameters.AddWithValue("@location", location);
                            cmd.Parameters.AddWithValue("@date_of_trans", str_dt);
                            cmd.Parameters.AddWithValue("@sent_or_rec", "Received");
                            cmd.Parameters.AddWithValue("@quantity", new_quantity_rec);
                            cmd.Parameters.AddWithValue("@TransSentQty", new_quantity_rec);
                            cmd.Parameters.AddWithValue("@Upload_Qty", uploadqty1);
                            cmd.Parameters.AddWithValue("@Challan_No", txtchallanno.Text);
                            cmd.Parameters.AddWithValue("@Rwk", lblrew.Text);
                            request_id = cmd.ExecuteScalar();
                            Session["Requestid"] = request_id;
                            Session["Challano"] = txtchallanno.Text;
                            Session["Location"] = location;


                            string s1 = "insert into part_history_QC_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,TransSentQty,Upload_Qty,Challan_No,User_name,Used_Date,rwk_no) values     (@p_no,@part_name,@tool_no,@station,@location,@date_of_trans,@sent_or_rec,@quantity,@TransSentQty,@Upload_Qty,@Challan_No,@user_name,@useddate,@rwkno);SELECT SCOPE_IDENTITY()";
                            cmd = new SqlCommand(s1, conn, trans);
                            cmd.Parameters.AddWithValue("@p_no", p_no);
                            cmd.Parameters.AddWithValue("@part_name", part_name);
                            cmd.Parameters.AddWithValue("@tool_no", tool_no);
                            cmd.Parameters.AddWithValue("@station", station);
                            cmd.Parameters.AddWithValue("@location", location);
                            cmd.Parameters.AddWithValue("@date_of_trans", str_dt);
                            cmd.Parameters.AddWithValue("@sent_or_rec", "Received");
                            cmd.Parameters.AddWithValue("@quantity", new_quantity_rec);
                            cmd.Parameters.AddWithValue("@TransSentQty", new_quantity_rec);
                            cmd.Parameters.AddWithValue("@Upload_Qty", uploadqty1);
                            cmd.Parameters.AddWithValue("@Challan_No", txtchallanno.Text);
                            cmd.Parameters.AddWithValue("@user_name", approvername);
                            cmd.Parameters.AddWithValue("@useddate", System.DateTime.Now);
                            cmd.Parameters.AddWithValue("@rwkno", lblrew.Text);
                            cmd.ExecuteNonQuery();

                            //UPDATING LOCATION INFO TABLE

                            string r_no = rework_no.Text;

                            string strQuery3 = "update part_history_GR set Received_Date='" + str_dt + "' , quantity=isnull(quantity,0) + " + new_quantity_rec + ", QtySent=isnull(QtySent,0) - " + new_quantity_rec + "  where p_no='" + p_no + "' and part_name='" + part_name + "' and location='COMPANY' and station='" + station + "' and rwk_no='" + r_no + "' ";
                            SqlCommand cmd3 = new SqlCommand(strQuery3, conn, trans);
                            cmd3.ExecuteNonQuery();

                            //1)UPDATING TOTAL QUANTITY RECEIVED,PENDING QUANTITY
                            s = "select isnull(quant_rec,0) from part_history_GR where tool_no=@tool_no and station=@station and p_no=@p_no and  part_name=@part_name and location=@location and rwk_no=@rwkno";
                            cmd = new SqlCommand(s, conn, trans);
                            cmd.Parameters.AddWithValue("@p_no", p_no);
                            cmd.Parameters.AddWithValue("@part_name", part_name);
                            cmd.Parameters.AddWithValue("@tool_no", tool_no);
                            cmd.Parameters.AddWithValue("@station", station);
                            cmd.Parameters.AddWithValue("@location", location);
                            cmd.Parameters.AddWithValue("@rwkno", lblrew.Text);

                            quant_rec += Convert.ToInt32(cmd.ExecuteScalar());

                            quant_pend = quant_sent - quant_rec;

                            //2) NOW UPDATING LOCATION_INFO TABLE

                            string strQuery2 = "update location_info_GR set  quant_rec=" + new_quantity_rec + " + isnull(quant_rec,0),Received_Date='" + str_dt1.ToString("yyyy/MM/dd HH:mm:ss") + "' where p_no='" + p_no + "' and station='" + station + "'  and location='" + location + "' and part_name='" + part_name + "' and tool_no=" + tool_no + " and rwk='" + lblrew.Text + "' ";
                            SqlCommand cmd2 = new SqlCommand(strQuery2, conn, trans);
                            cmd2.ExecuteNonQuery();

                            count = count + 1;
                        }
                    }
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                txtchallanno.Text = String.Empty;
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
                ex.ToString();
            }
            finally
            {
                conn.Close();
            }

            if (count > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Item Sent To Company Successfully ');", true);
                btnRecieved.Visible = false;

            }
        }
        conn.Close();
        showgrid();
        UpdatePanel1.Update();
        isRequestedForReceive = false;
    }

    /// <summary>
    /// Prints the Received Challan
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnprint_Click(object sender, EventArgs e)
    {
        if (txtchallanno.Text.Length > 0)
        {
            Session["MIN"] = txtchallanno.Text;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "window.open('Item_Challan.aspx?ProcessType=Receive');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Receive some part before Print');", true);
            return;
        }
    }

    /// <summary>
    /// Validates the Entered Quantity by User
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TextBox2_TextChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;

            TextBox TextBox2 = row.FindControl("TextBox2") as TextBox;
            Label Label6sentqty = row.FindControl("Label6sentqty") as Label;
            Label uploadqty = row.FindControl("Label6Upload_Qty") as Label;

            if (isSelected)
            {
                if (Label6sentqty.Text.Length <= 0)
                    Label6sentqty.Text = "0";
                if (TextBox2.Text.Length <= 0)
                    TextBox2.Text = "0";
                if (uploadqty.Text.Length <= 0)
                    uploadqty.Text = "0";


                if (Convert.ToInt32(TextBox2.Text.ToString()) > Convert.ToInt32(Label6sentqty.Text.ToString()))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('New Recive Qty Should be less than  or equal Total Qty');", true);
                    TextBox2.Text = "0";
                    TextBox2.Focus();
                    return;
                }
            }
        }
    }

    /// <summary>
    /// Validates the Date is not a Future or Past Date
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtReceivecdate_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDateTime(txtReceivecdate.Text) > DateTime.Now)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Can not take future date');", true);
            txtReceivecdate.Text = "";
        }
    }

    /// <summary>
    /// Retrieves the History of a Position on Position click and displays it in the Pop Up.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Label3_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        try
        {
            string query = "select  id,location as Party,date_of_trans as Trans_Date,sent_or_rec as Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as Pending_Qty_PPC,TransSentQty as Send_Or_Rec_Qty, User_Name  from part_history_GR  where   p_no='" + Positionno + "' and  tool_no='" + toolno + "' and  station='" + station + "' and  part_name='" + part_name + "' and rwk_no='" + rwk_no + "' order by date_of_trans asc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();

            da.Fill(table);

            StringBuilder b = new StringBuilder();

            b.Append("<table style='");
            b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<th>");
                b.Append(column.ColumnName);
                b.Append("</th>");
            }
            b.Append("</tr>");

            foreach (DataRow row in table.Rows)
            {
                b.Append("<tr>");
                foreach (DataColumn column in table.Columns)
                {
                    b.Append("<td>");
                    b.Append(row[column.ColumnName]);
                    b.Append("</td>");
                }
                b.Append("</tr>");
            }
            b.Append("</table>");

            pnlHistoryPopUp.InnerHtml = b.ToString();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    /// <summary>
    /// If user no longer needs those positons then they can be updated as Inactive by User.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnUpdateAsInactive_Click(object sender, EventArgs e)
    {
        int count = 0;
        bool isUpdateInProgress = false;
        string sqlconnstring1 = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring1);
        SqlTransaction trans = null;
        try
        {
            conn.Open();
            trans = conn.BeginTransaction();
            if (!isUpdateInProgress)
            {
                isUpdateInProgress = true;
                foreach (GridViewRow row in GridView1.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {

                        //DateTime commitmentDate = Convert.ToDateTime(txtdelcdate.Text);
                        ///str_dt = string.Format("{0:yyyy-MM-dd}", commitmentDate); // you can specify format 
                        Label station1 = row.FindControl("Label2") as Label;
                        Label rework_no = row.FindControl("lblrew") as Label;
                        Label lblToolNo = row.FindControl("Label1") as Label;
                        LinkButton POSITION = row.FindControl("Label3") as LinkButton;
                        Label part_name1 = row.FindControl("Label4") as Label;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        Label quantity = row.FindControl("Label8") as Label;
                        Label uploadqty = row.FindControl("Label6Upload_Qty") as Label;
                        Label sent = row.FindControl("Labelsent") as Label;
                        LinkButton lnkBtnLastCommitmentDate = row.FindControl("lnkBtnLastCommitmentDate") as LinkButton;

                        tool_no = Convert.ToInt32(lblToolNo.Text);
                        p_no = POSITION.Text;
                        station = station1.Text;
                        part_name = Convert.ToString(part_name1.Text);
                        location = "Design";
                        string kk = System.DateTime.Now.ToLongTimeString();
                        str_dt = str_dt + " " + kk;

                        bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                        if (isSelected)
                        {
                            string id = "";
                            string r_no = rework_no.Text;
                            cmd = new SqlCommand("select top 1 Parts_Gr_Id from parts_Gr where p_no='" + p_no + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk_no='" + r_no + "' and tool_no='" + tool_no + "' order by Parts_Gr_Id Desc", conn, trans);
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    reader.Read();
                                    id = Convert.ToString(reader["Parts_Gr_Id"]);
                                }
                            }

                            if (!string.IsNullOrEmpty(id))
                            {
                                string query = "update parts_gr set Is_Active_Record=0, Updated_By='" + Session["UserId"] + "', Updated_Date='" + DateTime.Now.ToString("yyyy/MM/dd HH:MM:ss") + "'  where Parts_Gr_Id=" + id + "";
                                SqlCommand cmdUpdatePartsGr = new SqlCommand(query, conn, trans);
                                cmdUpdatePartsGr.ExecuteNonQuery();
                                count = count + 1;
                            }
                        }
                    }
                }
                trans.Commit();
            }
            isUpdateInProgress = false;
        }
        catch (Exception ex)
        {
            trans.Rollback();
            isUpdateInProgress = false;
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }

        if (count > 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Record status has been set to In Active.');", true);
            btnRecieved.Visible = false;
            showgrid();
        }
    }

    /// <summary>
    /// Displays the History of Changed Expected Delivery Dates on Last Expeected Delivery Date Click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkBtnLastCommitmentDate_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        try
        {
            string id = "";
            SqlCommand cmd = new SqlCommand("select top 1 id from part_history_Gr where p_no='" + Positionno + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk_no='" + rwk_no + "' and tool_no='" + toolno + "' and location='Design'  and sent_or_rec='Sent' order by date_of_trans desc", conn);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    reader.Read();
                    id = Convert.ToString(reader["id"]);
                }
            }

            string query = "select  Tool_No,P_No,Part_Name,Station,Rework_No,Location,Commitment_Date as Expected_Delivery_Date  from CommitmentHistory  where  Part_History_Id=" + id + " union all select  Tool_No,P_No,Part_Name,Station,Rwk_No,Location,Delivery_commit_date  from part_history_Gr  where  id=" + id + " order by Commitment_Date desc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();
            da.Fill(table);

            StringBuilder b = new StringBuilder();

            b.Append("<table style='");
            b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<th>");
                b.Append(column.ColumnName);
                b.Append("</th>");
            }
            b.Append("</tr>");

            foreach (DataRow row in table.Rows)
            {
                b.Append("<tr>");
                foreach (DataColumn column in table.Columns)
                {
                    b.Append("<td>");
                    b.Append(row[column.ColumnName]);
                    b.Append("</td>");
                }
                b.Append("</tr>");
            }
            b.Append("</table>");

            pnlCommitmentHistory.InnerHtml = b.ToString();
            dialogCommitmentHistory.Attributes.Add("style", "display:block;");
            overlayCommitmentHistory.Attributes.Add("style", "display:block;");
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }

    protected void btnCloseCommitmentHistory_Click(object sender, EventArgs e)
    {
        dialogCommitmentHistory.Attributes.Add("style", "display:none;");
        overlayCommitmentHistory.Attributes.Add("style", "display:none;");
    }

    /// <summary>
    /// Updates the Commitment Date for selected Items
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnUpdateCommitmentDate_Click(object sender, EventArgs e)
    {
        if (txtdelcdate.Text != "")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please enter Date of Commitment First.');", true);
        }

        int count = 0;
        bool isUpdateInProgress = false;
        string sqlconnstring1 = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring1);
        SqlTransaction trans = null;
        try
        {
            conn.Open();
            trans = conn.BeginTransaction();

            if (!isUpdateInProgress)
            {
                isUpdateInProgress = true;
                foreach (GridViewRow row in GridView1.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        DateTime commitmentDate = Convert.ToDateTime(txtdelcdate.Text);
                        str_dt = string.Format("{0:yyyy-MM-dd}", commitmentDate); // you can specify format 
                        Label station1 = row.FindControl("Label2") as Label;
                        Label rework_no = row.FindControl("lblrew") as Label;
                        Label lblToolNo = row.FindControl("Label1") as Label;
                        LinkButton POSITION = row.FindControl("Label3") as LinkButton;
                        Label part_name1 = row.FindControl("Label4") as Label;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        Label quantity = row.FindControl("Label8") as Label;
                        Label uploadqty = row.FindControl("Label6Upload_Qty") as Label;
                        Label sent = row.FindControl("Labelsent") as Label;
                        LinkButton lnkBtnLastCommitmentDate = row.FindControl("lnkBtnLastCommitmentDate") as LinkButton;

                        tool_no = Convert.ToInt32(lblToolNo.Text);
                        p_no = POSITION.Text;
                        station = station1.Text;
                        part_name = Convert.ToString(part_name1.Text);
                        location = "Design";
                        string kk = System.DateTime.Now.ToLongTimeString();
                        str_dt = str_dt + " " + kk;

                        bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                        if (isSelected)
                        {
                            string id = "";
                            string r_no = rework_no.Text;
                            cmd = new SqlCommand("select top 1 id from part_history_Gr where p_no='" + p_no + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk_no='" + r_no + "' and tool_no='" + tool_no + "' and location='Design'  and sent_or_rec='Sent' order by date_of_trans desc", conn, trans);
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    reader.Read();
                                    id = Convert.ToString(reader["id"]);
                                }
                            }

                            if (lnkBtnLastCommitmentDate.Text != "")
                            {
                                int val = Convert.ToDateTime(str_dt).Subtract(Convert.ToDateTime(lnkBtnLastCommitmentDate.Text)).Days;
                                if (val <= 0)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Date Entered. Please select a Valid Date.');", true);
                                    break;
                                }

                                s = "insert into CommitmentHistory(part_history_id,p_no,part_name,tool_no,station,location,commitment_date,created_date,created_by,rework_no) values(@part_history_id,@p_no,@part_name,@tool_no,@station,@location,@commitment_date,@created_date,@created_by,@Rwk);";
                                cmd = new SqlCommand(s, conn, trans);
                                cmd.Parameters.AddWithValue("@part_history_id", id);
                                cmd.Parameters.AddWithValue("@p_no", p_no);
                                cmd.Parameters.AddWithValue("@part_name", part_name);
                                cmd.Parameters.AddWithValue("@tool_no", tool_no);
                                cmd.Parameters.AddWithValue("@station", station);
                                cmd.Parameters.AddWithValue("@location", location);
                                cmd.Parameters.AddWithValue("@commitment_date", Convert.ToDateTime(lnkBtnLastCommitmentDate.Text).ToString("yyyy-MM-dd HH:MM:ss"));
                                cmd.Parameters.AddWithValue("@created_by", Session["UserID"]);
                                cmd.Parameters.AddWithValue("@created_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                cmd.Parameters.AddWithValue("@Rwk", rework_no.Text);
                                cmd.ExecuteNonQuery();
                            }

                            //UPDATING LOCATION INFO TABLE

                            string query = "update part_history_GR set Delivery_commit_date='" + str_dt + "'  where id=" + id + "";
                            cmd.CommandText = query;
                            cmd.ExecuteNonQuery();

                            string queryLocationInfo = "update location_info_Gr set Delivery_commit_date='" + str_dt + "'  where p_no='" + p_no + "' and part_name='" + part_name + "'  and station='" + station + "' and rwk='" + r_no + "' and tool_no='" + tool_no + "' and location='Design'";
                            cmd.CommandText = queryLocationInfo;
                            cmd.ExecuteNonQuery();

                            count = count + 1;
                        }
                    }
                }
                trans.Commit();
            }
            isUpdateInProgress = false;
        }
        catch (Exception ex)
        {
            trans.Rollback();
            isUpdateInProgress = false;
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }

        if (count > 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Commitment Date Updated Successfully');", true);
            btnRecieved.Visible = false;
            showgrid();
        }
    }
}
