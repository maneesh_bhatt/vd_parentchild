﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.master"
    CodeFile="GRPrint_Previou_Challan.aspx.cs" Inherits="GRPrint_Previou_Challan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <style type="text/css">
        .style1
        {
            height: 26px;
        }
        .style2
        {
            border: none;
        }
        .style3
        {
            font-size: smaller;
        }
        
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="margin-left: 40px;"   align="left"; width="850px; border-color: Blue; " border="1px" height="300px">
        <tr> 
            
            <td class="style2">
            </td>
              <td class="style2">
            </td>
            <td class="style2" align="center" style="font-size: x-large; color: black">
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <span class="style1"><span class="style3">GRPrint Challan</span> </span>
                
            </td>
          
            
        </tr>
        <tr><td height="20px" class="style2"></td></tr>
        <tr>
            <td class="style2">
            </td>
            <td class="style2">
                <asp:RadioButton ID="RadioButton1" Text="Sent Challan" AutoPostBack="true" runat="server"
                    OnCheckedChanged="RadioButton1_CheckedChanged" BackColor="Lime" />
            </td>
            <td class="style2" width="220px">
                <asp:RadioButton ID="RadioButton2" Text="Received Challan" runat="server" AutoPostBack="true"
                    OnCheckedChanged="RadioButton2_CheckedChanged" BackColor="Lime" />
            </td>
            <td class="style2" width="220px">
                <asp:RadioButton ID="RadioButton3" Text="QC Challan" runat="server" AutoPostBack="true"
                    OnCheckedChanged="RadioButton3_CheckedChanged" BackColor="Lime" />
            </td>
            <td class="style2" width="220px">
                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/print.png" runat="server" OnClick="ImageButton1_Click1" />
            </td>
        </tr>

        <tr>
            <td class="style2" height="20px">
            </td>
        </tr>
        <tr>
            <td class="style2" height="20px">
            </td>

             <td class="style2" colspan ="2" height="30px">
             Select Vendor<br />
             <asp:DropDownList ID="ddlvendor" Visible="true" BackColor="#F6F1DB" ForeColor="#7d6754" CssClass="glowing-border" AutoPostBack="true" runat="server" Width="200px" 
                     onselectedindexchanged="ddlvendor_SelectedIndexChanged">
                </asp:DropDownList>
            </td>

           

            <td class="style2" height="30px">
              
            </td>
        </tr>
         <tr>
            <td class="style2" height="30px">
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
            <td class="style2" width="220px" valign="top">
                <asp:Label ID="lblsentchallan" runat="server" Text="Select Sent Challan"></asp:Label>
                <br />
               <asp:DropDownList ID="DDLSentchallan" BackColor="#F6F1DB" ForeColor="#7d6754" CssClass="glowing-border" Visible="true" runat="server" 
                    Width="120px">
                </asp:DropDownList>
            </td>
            <td class="style2" width="220px" valign="top">
                <asp:Label ID="lblReceivedchallan" runat="server" Text="Select Recieve Challan"></asp:Label>
               
                <br />
                <asp:DropDownList ID="ddlReceivedchallan" BackColor="#F6F1DB" ForeColor="#7d6754" CssClass="glowing-border" Visible="true" runat="server" 
                    Width="135px">
                </asp:DropDownList>
            </td>
            <td class="style2" width="220px" valign="top">
                <asp:Label ID="lblQCchallan" runat="server" Text="Select QC Sheet"></asp:Label>
              
                <br />
                
             <asp:DropDownList ID="ddlsheet" Visible="true" BackColor="#F6F1DB" ForeColor="#7d6754" CssClass="glowing-border" AutoPostBack="true" runat="server" Width="120px">
                </asp:DropDownList></td>
           
            <td class="style2" width="220px" valign="top">
            </td>
        </tr>
    </table>
</asp:Content>
