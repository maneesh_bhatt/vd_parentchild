﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="InspectionSheetSubmissionByQcSheetNumber.aspx.cs" Inherits="Tool_InspectionSheetSubmissionByQcSheetNumber" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


    </script>
    <style type="text/css">
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 860px;
            height: 440px;
            top: 20%;
            left: 30%;
            margin-left: -185px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
            line-height: 28px;
            overflow-y: scroll;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .awbimagecontainer
        {
            float: right;
            width: 29%;
            margin-right: 12px;
            margin-top: 43px;
            border: 1px solid #2D89EF;
            height: 180px;
        }
        .web_dialog_info
        {
            display: none;
            position: fixed;
            width: 940px;
            height: 510px;
            top: 22%;
            left: 36%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 99%; border: 1px solid Blue; min-height: 100px; padding: 4px;
                float: left;">
                <table style="width: 100%;" align="left">
                    <tr>
                        <td colspan="2" align="center" style="font-size: x-large; color: black">
                            <span class="style1">Inspection Sheet Submission By QC Sheet No </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="right">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <center>
                                        <img alt="" src="../ajax-loader.gif" style="width: 32px; height: 32px" />
                                    </center>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:Panel ID="Panel4" runat="server">
                                QC Sheet No &nbsp;<asp:TextBox ID="txtQCSheetNo" runat="server" CssClass="glowing-border"
                                    Width="100px"></asp:TextBox>
                                <asp:Button ID="btnSearch" runat="server" CssClass="glowing-border" Text="Search"
                                    OnClick="btnSearch_Click" />
                                &nbsp; Receive Date<span class="style12">&nbsp;</span>
                                <asp:TextBox ID="txtReceivecdate" CssClass="glowing-border" runat="server" AutoPostBack="true"
                                    Width="90px" OnTextChanged="txtReceivecdate_TextChanged"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                    Enabled="true" TargetControlID="txtReceivecdate" OnClientShowing="CurrentDateShowing">
                                </cc1:CalendarExtender>
                                &nbsp;
                                <asp:Button ID="btnprint" runat="server" EnableViewState="true" CausesValidation="false"
                                    Text="PrintChallan" OnClick="btnprint_Click" />
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblChallanno" runat="server" Text="Challan No"></asp:Label>
                                <asp:TextBox ID="txtchallanno" CssClass="glowing-border" Visible="False" runat="server"
                                    Width="80px" Enabled="False"></asp:TextBox>
                                <asp:Button ID="btnRecieved" runat="server" CausesValidation="true" EnableViewState="true"
                                    Text="Recieved" Visible="false" Width="100px" OnClick="btnRecieved_Click" />
                            </asp:Panel>
                            <asp:HyperLink ID="btnDownload" runat="server" Text="View Summary" Style="margin-top: 9px;
                                margin-right: 2px; float: right; background-color: green; color: white; padding: 6px;
                                border: 1px solid green; box-shadow: 2px 3px 2px green;" NavigateUrl="~/Tool/InspectionDataSubmissionSummary.aspx" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <td>
                                <asp:Panel ID="Panel1" runat="server" ScrollBars="Horizontal">
                                    <asp:GridView ID="GridView1" runat="server" EmptyDataText="No Record Found" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                        Style="border: 1px solid #aba4a4;" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating"
                                        AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None"
                                        Font-Size="13px" Width="100%" OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="30px" ItemStyle-Width="50px"
                                                ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkAll1" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll1_CheckedChanged1" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkAllchild" AutoPostBack="true" runat="server" OnCheckedChanged="chkAllchild_CheckedChanged">
                                                    </asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    Tool No
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="Server" Text='<%# Eval("tool_no") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    Station
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="Server" Text='<%# Eval("station") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    Position No
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Label3" CausesValidation="false" runat="server" Text='<%# Eval("p_no") %>'
                                                        CommandArgument='<%# Eval("p_no") + ";" + Eval("tool_no") + ";" + Eval("station") + ";" + Eval("part_name") +";"+Eval("Rwk") %>'
                                                        OnClick="Label3_Click"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="110px" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    Part Name
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="Server" Text='<%# Eval("part_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    Total Quantity Sent
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="Server" Text='<%# Eval("quant_sent") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    Total Received Qty
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="Server" Text='<%# Eval("quant_rec") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    New Receiving Qty
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TextBox2" runat="server" CssClass="glowing-border" onkeypress="return isNumberKey(event)"
                                                        Text="" AutoPostBack="true" Enabled="false" Width="60px" OnTextChanged="TextBox2_TextChanged"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    Upl Qty
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6Upload_Qty" runat="Server" Text='<%# Eval("Upload_Qty") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    Sent Qty
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6sentqty" runat="Server" Text='<%# Eval("SentQty") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    Rework No
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblrew" runat="Server" Text='<%# Eval("Rwk") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    Estimated Hrs
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEstimatedHrs" runat="server" Width="60px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                ItemStyle-Width="140px" HeaderStyle-Width="140px">
                                                <HeaderTemplate>
                                                    Actual Time
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="inputActualHrs" runat="server" CssClass="glowing-border" placeholder="Hrs"
                                                        onkeypress="return isNumberKey(event)" Width="35px"></asp:TextBox>
                                                    <asp:TextBox ID="inputActualMins" runat="server" CssClass="glowing-border" placeholder="Mins"
                                                        onkeypress="return isNumberKey(event)" Width="35px"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    Inspected By
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlInspectedBy" runat="server" CssClass="glowing-border" AppendDataBoundItems="true"
                                                        Width="90px">
                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    Last Commitment Date
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLastCommitmentDate" runat="Server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <EditRowStyle BackColor="#999999" />
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="overlay" runat="server" clientidmode="Static" class="web_dialog_overlay">
                            </div>
                            <div id="dialog" clientidmode="Static" runat="server" class="web_dialog">
                                <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                                    <div style="float: left; width: 100%;">
                                        <div class="web_dialog_title" style="width: 20%; float: left;">
                                            Item History
                                        </div>
                                        <div class="web_dialog_title align_right">
                                            <asp:LinkButton runat="server" ID="btnClosePopUp" ClientIDMode="Static" OnClick="btnClosePopUp_Click">Close</asp:LinkButton>
                                        </div>
                                    </div>
                                    <div runat="server" id="pnlHistoryPopUp">
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
