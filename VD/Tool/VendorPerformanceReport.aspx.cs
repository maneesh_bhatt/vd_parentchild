﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Text;

public partial class Tool_VendorPerformanceReport : System.Web.UI.Page
{
    public static string searchConditionVendor;
    Image sortImageVendor = new Image();
    private static string sortConditionVendor;
    private static string sortColumnVendor;
    public string SortDirectionVendor
    {
        get
        {
            if (ViewState["SortDirectionVendor"] == null)
                return string.Empty;
            else
                return ViewState["SortDirectionVendor"].ToString();
        }
        set
        {
            ViewState["SortDirectionVendor"] = value;
        }
    }
    private string _sortDirectionVendor;

    public static string searchConditionLocation;
    Image sortImageLocation = new Image();
    private static string sortConditionLocation;
    private static string sortColumnLocation;
    public string SortDirectionLocation
    {
        get
        {
            if (ViewState["SortDirectionLocation"] == null)
                return string.Empty;
            else
                return ViewState["SortDirectionLocation"].ToString();
        }
        set
        {
            ViewState["SortDirectionLocation"] = value;
        }
    }
    private string _sortDirectionLocation;

    SqlConnection connection;
    SqlCommand cmd;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    /// <summary>
    /// Binds the Grid View and Displays the Late Received Count, Timely Received Count and Outstanding Count along with its Percentage
    /// </summary>
    public void showgrid()
    {
        try
        {
            if (inputFromDate.Text != "" || inputToDate.Text != "")
            {

                DateTime dt = DateTime.MinValue;
                List<PartHistory> partList = GetDataByCriteria("");
                var result =
                         partList.AsEnumerable()
                             .Select(p => new
                             {
                                 location = p.Location,
                                 sentPartsCount = p.PartsSentCount,
                                 lateReceived = p.LateReceivedData,
                                 TimelyReceived = p.TimelyReceivedData,
                                 outStanding = p.PartsSentCount - (p.LateReceivedData + p.TimelyReceivedData)


                             })
                             .GroupBy(p => new { p.location })
                             .Select(p => new
                             {
                                 Location = p.Key.location,
                                 TotalSentQuantity = p.Sum(x => Convert.ToInt32(x.sentPartsCount)),
                                 TotalLateReceivedCount = p.Sum(x => Convert.ToInt32(x.lateReceived)),
                                 TotalTimelyReceivedCount = p.Sum(x => Convert.ToInt32(x.TimelyReceived)),
                                 TotalOutStandingCount = p.Sum(x => Convert.ToInt32(x.outStanding)),
                                 LateReceivedPercent = (p.Sum(x => Convert.ToDouble(x.lateReceived)) != 0) ? Math.Round((p.Sum(x => Convert.ToDouble(x.lateReceived)) * 100) / p.Sum(x => Convert.ToDouble(x.sentPartsCount)), 1) : 0,
                                 TimelyReceivedPercent = (p.Sum(x => Convert.ToDouble(x.TimelyReceived)) != 0) ? Math.Round((p.Sum(x => Convert.ToDouble(x.TimelyReceived)) * 100) / p.Sum(x => Convert.ToDouble(x.sentPartsCount)), 1) : 0,
                                 OutStandingPercent = (p.Sum(x => Convert.ToDouble(x.outStanding)) != 0) ? Math.Round((p.Sum(x => Convert.ToDouble(x.outStanding)) * 100) / p.Sum(x => Convert.ToDouble(x.sentPartsCount)), 1) : 0,

                             });


                if (string.IsNullOrEmpty(sortConditionVendor))
                {
                    sortConditionVendor = "Location ASC";
                }
                string[] splitCondition = sortConditionVendor.Split(' ');
                if (splitCondition[1].ToLower() == "asc")
                {
                    result = result.OrderBy(r => r.GetType().GetProperty(splitCondition[0]).GetValue(r, null)).ToList();
                }
                else if (splitCondition[1].ToLower() == "desc")
                {
                    result = result.OrderByDescending(r => r.GetType().GetProperty(splitCondition[0]).GetValue(r, null)).ToList();
                }

                girdVendorPerformanceReport.DataSource = result;
                girdVendorPerformanceReport.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Time Interval or Select From Date or To Date');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

        }
    }

    /// <summary>
    /// Retrives the Data and Calculates the TimelyReceivedData ,LateReceivedData,OutStandingCount which is late user to calculate over all count and percentage
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    protected List<PartHistory> GetDataByCriteria(string location)
    {

        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        List<PartHistory> partList = new List<PartHistory>();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        SqlDataAdapter ad = new SqlDataAdapter();
        try
        {
            string searchCondition = "";
            if (location == "")
            {
                location = inputVendorName.Text;
            }
            if (location != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and phg.location='" + location + "'";
                }
                else
                {
                    searchCondition = " phg.location='" + location + "'";
                }
            }
            if (location == "")
            {
                if (checkBoxInternalVendors.Checked && checkBoxEnternalVendors.Checked)
                {
                    if (searchCondition != "")
                        searchCondition = searchCondition + " and (vm.vendor_type='End Store' or vm.vendor_type='Internal Vendor' or vm.Vendor_Type='External Vendor')";
                    else
                        searchCondition = " (vm.vendor_type='End Store' or vm.vendor_type='Internal Vendor' or vm.Vendor_Type='External Vendor')";
                }
                else if (checkBoxInternalVendors.Checked)
                {
                    if (searchCondition != "")
                        searchCondition = searchCondition + " and (vm.vendor_type='End Store' or vm.vendor_type='Internal Vendor')";
                    else
                        searchCondition = " (vm.vendor_type='End Store' or vm.Vendor_Type='Internal Vendor')";
                }
                else if (checkBoxEnternalVendors.Checked)
                {
                    if (searchCondition != "")
                        searchCondition = searchCondition + " and vm.vendor_type='External Vendor'";
                    else
                        searchCondition = " vm.vendor_type='External Vendor'";
                }
            }
            if (inputFromDate.Text != "" && inputToDate.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,phg.date_of_trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,phg.date_of_trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,phg.date_of_trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,phg.date_of_trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
            }
            else if (inputFromDate.Text != "" && inputToDate.Text == "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,phg.date_of_trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,phg.date_of_trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                }
            }
            else if (inputFromDate.Text == "" && inputToDate.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,phg.date_of_trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,phg.date_of_trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
            }
            if (searchCondition != "")
            {
                ad = new SqlDataAdapter("select distinct phg.location,phg.tool_no,phg.p_no,phg.part_name,phg.station,phg.rwk_no,phg.Upload_Qty,phg.quantity,phg.id, phg.sent_or_rec, phg.date_of_trans, phg.Delivery_commit_date from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no left join Vendor_Master_Job_work vm on phg.location=vm.Vendor_Name where phg.Location!='Company' and phg.Date_of_Trans is not null and " + searchCondition + " and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  order by phg.location,phg.tool_no, phg.p_no, phg.part_name, phg.station, phg.rwk_no, phg.id ", connec);
            }
            else
            {
                ad = new SqlDataAdapter("select distinct phg.location,phg.tool_no,phg.p_no,phg.part_name,phg.station,phg.rwk_no,phg.Upload_Qty,phg.quantity,phg.id, phg.sent_or_rec, phg.date_of_trans, phg.Delivery_commit_date from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no left join Vendor_Master_Job_work vm on phg.location=vm.Vendor_Name where phg.Location!='Company' and phg.Date_of_Trans is not null and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  order by phg.location,phg.tool_no, phg.p_no, phg.part_name, phg.station, phg.rwk_no, phg.id ", connec);
            }
            DataTable dt = new DataTable();
            ad.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (Convert.ToString(dt.Rows[i]["Sent_Or_Rec"]) == "Received")
                {
                    List<PartHistory> partHisExist = partList.FindAll(x => x.Location == Convert.ToString(dt.Rows[i]["Location"])
                        && x.ToolNo == Convert.ToInt32(dt.Rows[i]["Tool_No"])
                        && x.PositionNumber == Convert.ToString(dt.Rows[i]["P_No"])
                        && x.PartName == Convert.ToString(dt.Rows[i]["Part_Name"])
                        && x.Station == Convert.ToString(dt.Rows[i]["Station"])
                        && x.ReworkNumber == Convert.ToString(dt.Rows[i]["Rwk_No"])
                        );
                    if (partHisExist.Count > 0)
                    {
                        int currentCount = 0;
                        int totalRows = partHisExist.Count;
                        foreach (PartHistory partHistory in partHisExist)
                        {
                            currentCount = currentCount + 1;

                            string delCommitDate = partHistory.DeliveryCommitDate.ToShortDateString();
                            string dateOfTrans = Convert.ToDateTime(dt.Rows[i]["date_of_trans"]).ToString("dd-MM-yyyy");
                            int days = Convert.ToDateTime(dateOfTrans).Subtract(Convert.ToDateTime(delCommitDate)).Days;
                            //Received On Time
                            if (days <= 0)
                            {
                                if (partHistory.OutStandingData > 0)
                                {
                                    int quantity = Convert.ToInt32(dt.Rows[i]["Quantity"]);
                                    if (quantity > 0)
                                    {
                                        if (Convert.ToInt32(dt.Rows[i]["Quantity"]) >= partHistory.OutStandingData)
                                        {
                                            partHistory.TimelyReceivedData = 1;
                                            partHistory.ReceivedDate = dateOfTrans;
                                            dt.Rows[i]["Quantity"] = quantity - partHistory.OutStandingData;
                                            partHistory.OutStandingData = 0;
                                            partHistory.LateReceivedData = 0;
                                            partHistory.OutStandingCount = 0;
                                        }
                                        else
                                        {
                                            partHistory.OutStandingData = partHistory.OutStandingData - quantity;
                                            partHistory.ReceivedDate = dateOfTrans;

                                            dt.Rows[i]["Quantity"] = Convert.ToInt32(dt.Rows[i]["Quantity"]) - quantity;
                                            if (partHistory.OutStandingData > 0)
                                            {
                                                partHistory.TimelyReceivedData = 0;
                                                partHistory.LateReceivedData = 0;
                                                partHistory.OutStandingCount = 1;
                                            }
                                            else
                                            {
                                                partHistory.TimelyReceivedData = 1;
                                                partHistory.LateReceivedData = 0;
                                                partHistory.OutStandingCount = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (partHistory.OutStandingData > 0)
                                {
                                    int quantity = Convert.ToInt32(dt.Rows[i]["Quantity"]);
                                    if (quantity > 0)
                                    {
                                        if (Convert.ToInt32(dt.Rows[i]["Quantity"]) >= partHistory.OutStandingData)
                                        {
                                            partHistory.ReceivedDate = dateOfTrans;
                                            partHistory.TimelyReceivedData = 0;
                                            partHistory.LateReceivedData = 1;
                                            partHistory.OutStandingCount = 0;
                                            dt.Rows[i]["Quantity"] = quantity - partHistory.OutStandingData;
                                            partHistory.OutStandingData = 0;

                                        }
                                        else
                                        {
                                            partHistory.ReceivedDate = dateOfTrans;
                                            partHistory.OutStandingData = partHistory.OutStandingData - quantity;
                                            dt.Rows[i]["Quantity"] = Convert.ToInt32(dt.Rows[i]["Quantity"]) - quantity;
                                            if (partHistory.OutStandingData > 0)
                                            {
                                                partHistory.LateReceivedData = 0;
                                                partHistory.TimelyReceivedData = 0;
                                                partHistory.OutStandingCount = 1;
                                            }
                                            else
                                            {
                                                partHistory.LateReceivedData = 1;
                                                partHistory.TimelyReceivedData = 0;
                                                partHistory.OutStandingCount = 0;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
                else
                {
                    PartHistory ph = new PartHistory();
                    ph.Location = Convert.ToString(dt.Rows[i]["Location"]);
                    ph.ToolNo = Convert.ToInt32(dt.Rows[i]["Tool_No"]);
                    ph.PositionNumber = Convert.ToString(dt.Rows[i]["P_No"]);
                    ph.PartName = Convert.ToString(dt.Rows[i]["Part_Name"]);
                    ph.Station = Convert.ToString(dt.Rows[i]["Station"]);
                    ph.ReworkNumber = Convert.ToString(dt.Rows[i]["Rwk_No"]);
                    ph.UploadQuantity = Convert.ToInt32(dt.Rows[i]["Upload_Qty"]);
                    ph.Quantity = Convert.ToInt32(dt.Rows[i]["Quantity"]);
                    ph.DateOfTrans = Convert.ToDateTime(dt.Rows[i]["Date_Of_Trans"]);
                    ph.OutStandingData = Convert.ToInt32(dt.Rows[i]["Quantity"]);
                    ph.OutStandingCount = 1;
                    ph.PartsSentCount = 1;
                    if (dt.Rows[i]["Delivery_commit_date"] != DBNull.Value)
                        ph.DeliveryCommitDate = Convert.ToDateTime(dt.Rows[i]["Delivery_commit_date"]);
                    ph.SentOrRec = Convert.ToString(dt.Rows[i]["Sent_Or_Rec"]);
                    partList.Add(ph);
                }
            }
            return partList;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    protected void girdVendorPerformanceReport_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            string location = Convert.ToString(e.CommandArgument);
            DateTime dt = DateTime.MinValue;
            lblLocation.Text = location;
            gridViewLocationWiseData.DataSource = GetDataByCriteria(location);
            gridViewLocationWiseData.DataBind();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
    }

    protected void girdVendorPerformanceReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            double totalLateReceivedQuantity = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "LateReceivedPercent"));
            if (totalLateReceivedQuantity > 50)
            {
                e.Row.Attributes.Add("style", "background-color:rgba(255, 0, 24, 0.25);");
            }
            else if (totalLateReceivedQuantity >= 15 && totalLateReceivedQuantity <= 50)
            {
                e.Row.Attributes.Add("style", "background-color:rgba(255, 255, 0, 0.52);");
            }
            else if (totalLateReceivedQuantity >= 0 && totalLateReceivedQuantity < 15)
            {
                e.Row.Attributes.Add("style", "background-color:lightgreen;");
            }
        }
    }

    protected void girdVendorPerformanceReport_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    protected void girdVendorPerformanceReport_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected string GetHtmlContent(DataTable table)
    {

        StringBuilder b = new StringBuilder();
        b.Append("<table style='");
        b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
        b.Append("<tr>");
        foreach (DataColumn column in table.Columns)
        {
            b.Append("<th>");
            b.Append(column.ColumnName);
            b.Append("</th>");
        }
        b.Append("</tr>");


        foreach (DataRow row in table.Rows)
        {
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<td>");
                b.Append(row[column.ColumnName]);
                b.Append("</td>");
            }
            b.Append("</tr>");
        }
        b.Append("</table>");
        return b.ToString();
    }

    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }


    protected void ExportToExcel()
    {
        Response.ClearContent();
        string fileName = "CommitmentDateData";
        string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        girdVendorPerformanceReport.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    protected void ApplySortingImageVendor(string sortExpression)
    {
        int columnIndex = 0;
        foreach (DataControlFieldHeaderCell headerCell in girdVendorPerformanceReport.HeaderRow.Cells)
        {
            if (headerCell.ContainingField.SortExpression == sortExpression)
            {
                columnIndex = girdVendorPerformanceReport.HeaderRow.Cells.GetCellIndex(headerCell);
            }
        }

        girdVendorPerformanceReport.HeaderRow.Cells[columnIndex].Controls.Add(sortImageVendor);
    }

    protected void SetSortDirectionVendor(string sortDirection)
    {
        if (sortDirection.ToLower() == "asc")
        {
            _sortDirectionVendor = "DESC";
            sortImageVendor.ImageUrl = "../Images/view_sort_ascending.png";

        }
        else
        {
            _sortDirectionVendor = "ASC";
            sortImageVendor.ImageUrl = "../Images/view_sort_descending.png";
        }
    }

    protected void ApplySortingImageLocation(string sortExpression)
    {
        int columnIndex = 0;
        foreach (DataControlFieldHeaderCell headerCell in gridViewLocationWiseData.HeaderRow.Cells)
        {
            if (headerCell.ContainingField.SortExpression == sortExpression)
            {
                columnIndex = gridViewLocationWiseData.HeaderRow.Cells.GetCellIndex(headerCell);
            }
        }

        gridViewLocationWiseData.HeaderRow.Cells[columnIndex].Controls.Add(sortImageLocation);
    }

    protected void SetSortDirectionLocation(string sortDirection)
    {
        if (sortDirection.ToLower() == "asc")
        {
            _sortDirectionLocation = "DESC";
            sortImageLocation.ImageUrl = "../Images/view_sort_ascending.png";

        }
        else
        {
            _sortDirectionLocation = "ASC";
            sortImageLocation.ImageUrl = "../Images/view_sort_descending.png";
        }
    }

    protected void girdVendorPerformanceReport_Sorting(object sender, GridViewSortEventArgs e)
    {
        // gridBagList.PageIndex = 0;
        SetSortDirectionVendor(SortDirectionVendor);
        sortConditionVendor = e.SortExpression + " " + _sortDirectionVendor;
        sortColumnVendor = e.SortExpression;
        showgrid();
        SortDirectionVendor = _sortDirectionVendor;
        ApplySortingImageVendor(e.SortExpression);

    }

    protected void gridViewLocationWiseData_Sorting(object sender, GridViewSortEventArgs e)
    {
        SetSortDirectionLocation(SortDirectionLocation);
        sortConditionLocation = e.SortExpression + " " + _sortDirectionLocation;
        sortColumnLocation = e.SortExpression;
        var result = GetDataByCriteria(lblLocation.Text);
        if (string.IsNullOrEmpty(sortConditionLocation))
        {
            sortConditionLocation = "ToolNo ASC";
        }
        string[] splitCondition = sortConditionLocation.Split(' ');
        if (splitCondition[1].ToLower() == "asc")
        {
            result = result.OrderBy(r => r.GetType().GetProperty(splitCondition[0]).GetValue(r, null)).ToList();
        }
        else if (splitCondition[1].ToLower() == "desc")
        {
            result = result.OrderByDescending(r => r.GetType().GetProperty(splitCondition[0]).GetValue(r, null)).ToList();
        }

        gridViewLocationWiseData.DataSource = result;
        gridViewLocationWiseData.DataBind();
        SortDirectionLocation = _sortDirectionLocation;
        ApplySortingImageLocation(e.SortExpression);

    }

}
