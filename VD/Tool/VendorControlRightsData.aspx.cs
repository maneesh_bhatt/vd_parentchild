﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Tool_VendorControlRightsData : System.Web.UI.Page
{
    public static string searchCondition;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

   /// <summary>
   /// Loads the Data from Vendor Control Rights table for all Users
   /// </summary>
    protected void BindGrid()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            if (Convert.ToString(ViewState["SearchCondition"]) != "")
            {
                adapter = new SqlDataAdapter("select * from Vendor_Control_Rights where " + ViewState["SearchCondition"] + " order by User_Name ASC", con);
            }
            else
            {
                adapter = new SqlDataAdapter("select * from Vendor_Control_Rights order by User_Name ASC", con);
            }

            DataTable td = new DataTable();
            adapter.Fill(td);
            gridUserControlRights.DataSource = td;
            gridUserControlRights.DataBind();
            gridUserControlRights.EmptyDataText = "No Record Found";
            lblRecorrdsCount.Text = Convert.ToString(td.Rows.Count) + " " + "Records In Total";
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }

    }

    protected void gridUserControlRights_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // Set the index of the new display page.  
        gridUserControlRights.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //gridPackages.PageIndex = 1;
        searchCondition = "";
        if (inputUserName.Text != "")
        {
            searchCondition = "User_Name ='" + inputUserName.Text + "'";
        }

        ViewState["SearchCondition"] = searchCondition;
        BindGrid();
    }

    protected void gridUserControlRights_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            Response.Redirect("VendorControlRights.aspx?Name=" + e.CommandArgument);
        }
    }

    /// <summary>
    /// Retrives the List of User Names from Vendor Control Rights Table
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetUserName(string text)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            List<string> allItems = new List<string>();
            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrWhiteSpace(text))
            {
                cmd = new SqlCommand("select distinct User_Name from Vendor_Control_Rights where User_Name like '" + text + "%' order by User_Name asc ", con);
            }
            else
            {
                cmd = new SqlCommand("select distinct User_Name from Vendor_Control_Rights order by User_Name asc ", con);
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string userName = Convert.ToString(reader["User_Name"]);
                allItems.Add(userName);
            }
            reader.Close();
            return allItems;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            con.Close();
        }
    }
}

