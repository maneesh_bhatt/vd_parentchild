﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Print_challan_Details.aspx.cs" Inherits="Print_challan_Details" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <script type="text/javascript" language="JavaScript">

    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="css.css" rel="stylesheet" type="text/css" />
</head>
<body onload="timer=setTimeout('move()',50000)">
    <form runat="server" id="frm1">
    <div id="wraper">
        <div id="main ">
            <div class="box_contner1">
                <div class="logo">
                    <asp:Image ID="Image1" ImageUrl="~/images/FALCON LOGO quarter.jpg" runat="server" /></div>
                <div class="textmed">
                    Falcon AutoTech PVT. LTD<br />
                    Works: Plot No. 308-309,Eco tech Ext-1, Greater Noida,Dist Gautam Budh Nagar,U.P
                    PIN-201308<br />
                    E-mail falconautotech@hotmail.com&nbsp;
                                       <br /> &nbsp;<div style="margin-top: 12px;">
                        Print Challan Details </div>
                </div>
                <div class="textmedR">
                    <asp:Label runat="server"  ID="LblDt"></asp:Label><br />
                    
                    <br />
                </div>
               
            </div>
            <div style="visibility: hidden" class="cl">
            </div>
            <div class="box_contner mrgnt">
                <div class="hed">
                    Challan No-<div  style ="font-family:IDAutomationHC39M; height:20PX;">
                    <br />
                      <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="lblMino" Visible="true" runat="server" ></asp:Label>
                    </div>
                    <br />
                  
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -</div>
                     <br />
                <div class="cont_box">
                    <div class="cont_boxinr">
                        <%--<div class="floatL fbold">
                            Valid Up To
                            <asp:Label ID="LblValidUpto" Font-Bold="false" runat="server"></asp:Label><br />
                            Revalid Up To
                            <asp:Label ID="LblReValidUpto" Font-Bold="false" runat="server"></asp:Label>
                            <br />
                            Actual gate In
                            <asp:Label ID="ActualGateIn" Font-Bold="false" runat="server"></asp:Label><br />
                            Vehicle No
                            <asp:Label ID="LblVehicleNo" Font-Bold="false" runat="server"></asp:Label><br />
                            Transporter Name
                            <asp:Label ID="LblTranporterName" Font-Bold="false" runat="server"></asp:Label>
                        </div>--%>
                        <div class="cl">
                        </div>
                    </div>
                    <div class="cl">
                    </div>
                </div>
                <div class="hed">
                    </div>
                <div>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Following items are being&nbsp; Against Challan 
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></div>
            </div>
        </div>
        <div class="box_contner1">
            <asp:GridView Width="800px" runat="server" AutoGenerateColumns="false" ID="Grdview1" OnRowDataBound="Grdview1_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            Sr.No.
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Lblsr" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                          Challan No
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblChallan_No" Text='<%# Eval("Challan_No") %>' Width="100px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                          Tool No
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbltool_no" Text='<%# Eval("tool_no") %>' runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                           Tot Pending Qty
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTotal_pending" Text='<%# Eval("Total_pending") %>' Width="100px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>

                                             


                     <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                          TotalRecQty
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTotalRecQty" Text='<%# Eval("TotalRecQty") %>' runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
            

                 
                  <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                           TotUploadqty
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTotUploadqty" Text='<%# Eval("TotUploadqty") %>' Width="100px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            Sent Date
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSent_Received_Date" Text='<%# Eval("Sent_Received_Date") %>'  Width="100px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                           Explain Date
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblExplain_Date" Text='<%# Eval("Explain_Date") %>'  Width="100px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                      
                
                       <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                           Commit Date
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblsub_head1" Text='<%# Eval("Delivery_commit_date") %>' runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>

                                     
                              

               
                </Columns>
            </asp:GridView>
        </div>

        <div class="box_contner1">
            <asp:GridView Width="800px" runat="server" AutoGenerateColumns="false" ID="GridView2" OnRowDataBound="Grdview2_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            Sr.No.
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Lblsr" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                           Project No
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbltool_no" Text='<%# Eval("tool_no") %>' Width="100px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                         station
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblstation" Text='<%# Eval("station") %>' runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                           p no
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblp_no" Text='<%# Eval("p_no") %>' Width="100px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                                                 


                     <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                           Location
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbllocation" Text='<%# Eval("location") %>' Width="200px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
            

                 
                  <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                           qtypending
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblqtypending" Text='<%# Eval("qtypending") %>' Width="100px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            Upload Qty
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUpload_Qty" Text='<%# Eval("Upload_Qty") %>'  Width="100px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                      
                
                       <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            QtyReceived
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTransSentQty" Text='<%# Eval("TransSentQty") %>' runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                            
                                      
                              

               
                </Columns>
            </asp:GridView>
        </div>
    </div>
   
    <div class="box_contner1">
        
        <div style="vertical-align="Top">
            <br />
            <br />
            <br />
            Approver1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            Approver2 Signature&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            Signature&nbsp;&nbsp;&nbsp; <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
    </div>
    </div> </div>
    </form>
</body>
</html>
