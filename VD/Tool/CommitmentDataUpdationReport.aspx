﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="CommitmentDataUpdationReport.aspx.cs" Inherits="Tool_CommitmentDataUpdationReport"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 860px;
            height: 440px;
            top: 20%;
            left: 30%;
            margin-left: -185px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
            line-height: 28px;
            overflow-y: scroll;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .awbimagecontainer
        {
            float: right;
            width: 29%;
            margin-right: 12px;
            margin-top: 43px;
            border: 1px solid #2D89EF;
            height: 180px;
        }
        .web_dialog_info
        {
            display: none;
            position: fixed;
            width: 940px;
            height: 510px;
            top: 22%;
            left: 36%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="width: 100%; border: 1px solid Blue; min-height: 100px;">
        <div style="padding: 10px;">
            <div>
                <div align="center">
                    <span class="style13">Expected Delivery Date Updation Summary</span>
                </div>
                <p style="font-size: 13px;">
                    Note: Displays the records whose expected delivery date has been changed and are not yet
                    received from Vendor.</p>
            </div>
            <div style="margin-bottom: 10px; margin-top: 20px;">
                <div style="font-size: 13px; width: 150px; float: left;">
                    Location<%-- <span style="color: Red;">*</span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddllocation"
                    ErrorMessage="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>--%><br />
                    <asp:DropDownList ID="ddllocation" CssClass="glowing-border" AppendDataBoundItems="true"
                        BackColor="#F6F1DB" ForeColor="#7d6754" Width="140px" runat="server">
                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="font-size: 13px; width: 90px; float: left;">
                    Tool No
                    <br />
                    <asp:TextBox ID="inputToolNo" CssClass="glowing-border" runat="server" Width="80px"></asp:TextBox>
                </div>
                <div style="font-size: 13px; width: 85px; float: left;">
                    Rework No
                    <br />
                    <asp:TextBox ID="inputReworkNo" CssClass="glowing-border" runat="server" Width="75px"></asp:TextBox>
                </div>
                <div style="font-size: 13px; width: 85px; float: left;">
                    Station
                    <br />
                    <asp:TextBox ID="inputStation" CssClass="glowing-border" runat="server" Width="75px"></asp:TextBox>
                </div>
                <div style="font-size: 13px; width: 90px; float: left;">
                    Position No
                    <br />
                    <asp:TextBox ID="inputPNo" CssClass="glowing-border" runat="server" Width="80px"></asp:TextBox>
                </div>
                <div style="font-size: 13px; width: 80px; float: left;">
                    Date Interval
                    <br />
                    <asp:DropDownList ID="ddlDateInterval" CssClass="glowing-border" AppendDataBoundItems="true"
                        BackColor="#F6F1DB" ForeColor="#7d6754" Width="70px" runat="server">
                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                        <asp:ListItem Text="1 Month" Value="1"></asp:ListItem>
                        <asp:ListItem Text="2 Month" Value="2"></asp:ListItem>
                        <asp:ListItem Text="3 Month" Value="3"></asp:ListItem>
                        <asp:ListItem Text="6 Month" Value="6"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="font-size: 13px; width: 95px; float: left;">
                    Start Date
                    <br />
                    <asp:TextBox ID="inputFromDate" CssClass="glowing-border" runat="server" Width="85px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                        Enabled="true" TargetControlID="inputFromDate" OnClientShowing="CurrentDateShowing">
                    </cc1:CalendarExtender>
                </div>
                <div style="font-size: 13px; width: 95px; float: left;">
                    End Date
                    <br />
                    <asp:TextBox ID="inputToDate" CssClass="glowing-border" runat="server" Width="85px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="true" Format="dd-MMM-yyyy"
                        TargetControlID="inputToDate" OnClientShowing="CurrentDateShowing">
                    </cc1:CalendarExtender>
                </div>
                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/images/searchbtn.jpg"
                    OnClick="btnSearch_Click" Style="margin-top: 10px;" />
                <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="btnDownload_Click"
                    Visible="false" Style="margin-top: 9px; margin-right: 2px; float: right; background-color: green;
                    color: white; padding: 6px; border: 1px solid green; box-shadow: 2px 3px 2px green;" />
            </div>
            <asp:GridView ID="GridView1" runat="server" EmptyDataText="No Record Found" AutoGenerateColumns="False"
                OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                AllowPaging="false" AllowSorting="false" Style="font-size: 13px; font-family: Verdana;
                line-height: 26px;" Width="100%">
                <RowStyle BackColor="White" ForeColor="Black" />
                <Columns>
                    <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                    <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="70px" HeaderStyle-ForeColor="White">
                        <HeaderTemplate>
                            Tool
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="Server" Text='<%# Eval("tool_no") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="70px" HeaderStyle-ForeColor="White">
                        <HeaderTemplate>
                            Station
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="Server" Text='<%# Eval("station") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="60px" HeaderStyle-ForeColor="White">
                        <HeaderTemplate>
                            P No
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="Label3" CausesValidation="false" runat="server" Text='<%# Eval("p_no") %>'
                                CommandArgument='<%# Eval("p_no") + ";" + Eval("tool_no") + ";" + Eval("station") + ";" + Eval("part_name") +";"+Eval("Rwk") %>'
                                OnClick="Label3_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="90px" ItemStyle-Width="90px" HeaderStyle-ForeColor="White">
                        <HeaderTemplate>
                            Part Name
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="Server" Text='<%# Eval("part_name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="80px" HeaderStyle-ForeColor="White">
                        <HeaderTemplate>
                            Upl Qty
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6Upload_Qty" runat="Server" Text='<%# Eval("Upload_Qty") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="60px" HeaderStyle-ForeColor="White">
                        <HeaderTemplate>
                            Sent Qty
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6sentqty" runat="Server" Text='<%# Eval("SentQuantity") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="80px" HeaderStyle-ForeColor="White">
                        <HeaderTemplate>
                            Received Qty
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblReceivedQty" runat="Server" Text='<%# Eval("ReceivedQuantity") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="80px" HeaderStyle-ForeColor="White">
                        <HeaderTemplate>
                            Rework
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblrew" runat="Server" Text='<%# Eval("Rwk") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="160px" HeaderStyle-ForeColor="White">
                        <HeaderTemplate>
                            Last Expected Delivery Date
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkBtnLastCommitmentDate" CausesValidation="false" runat="server"
                                Text='<%#Convert.ToDateTime(Eval("DeliveryCommitDate")).ToString("dd/MM/yyyy") %>'
                                CommandArgument='<%# Eval("p_no") + ";" + Eval("tool_no") + ";" + Eval("station") + ";" + Eval("part_name") +";"+Eval("Rwk") +";"+Eval("Location") %>'
                                OnClick="lnkBtnLastCommitmentDate_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="160px" HeaderStyle-ForeColor="White">
                        <HeaderTemplate>
                            Expected Delivery Date Change Count
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCommitmenChangeCount" runat="Server" Text='<%# Eval("CommitmentChangeCount") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <div>
        <div id="overlay" runat="server" clientidmode="Static" class="web_dialog_overlay">
        </div>
        <div id="dialog" clientidmode="Static" runat="server" class="web_dialog">
            <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                <div style="float: left; width: 100%;">
                    <div class="web_dialog_title" style="width: 20%; float: left;">
                        Item History
                    </div>
                    <div class="web_dialog_title align_right">
                        <asp:LinkButton runat="server" ID="btnClosePopUp" ClientIDMode="Static" OnClick="btnClosePopUp_Click">Close</asp:LinkButton>
                    </div>
                </div>
                <div runat="server" id="pnlHistoryPopUp">
                </div>
            </div>
        </div>
    </div>
    <div>
        <div id="overlayCommitmentHistory" runat="server" clientidmode="Static" class="web_dialog_overlay">
        </div>
        <div id="dialogCommitmentHistory" clientidmode="Static" runat="server" class="web_dialog">
            <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                <div style="float: left; width: 100%;">
                    <div class="web_dialog_title" style="width: 40%; float: left;">
                        Commitment History
                    </div>
                    <div class="web_dialog_title align_right">
                        <asp:LinkButton runat="server" ID="btnCloseCommitmentHistory" ClientIDMode="Static"
                            OnClick="btnCloseCommitmentHistory_Click">Close</asp:LinkButton>
                    </div>
                </div>
                <div runat="server" id="pnlCommitmentHistory">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
