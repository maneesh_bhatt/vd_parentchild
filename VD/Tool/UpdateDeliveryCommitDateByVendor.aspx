﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="UpdateDeliveryCommitDateByVendor.aspx.cs" Inherits="Tool_UpdateDeliveryCommitDateByVendor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .align_right
        {
            text-align: right;
        }
        .tableheader
        {
            line-height: 15px;
        }
    </style>
    <script language="javascript" type="text/javascript">

        $(function () {

            GetVendor();
            GetChallanNo();
        });
        function GetVendor() {
            $('#<%=inputVendorName.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "VendorMasterJobWorkData.aspx/GetVendorName",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputChallan.ClientID%>').val("");
                $('#<%=inputVendorName.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputChallan.ClientID%>').val("");
                $('#<%=inputVendorName.ClientID%>').blur();

            });
        };

        function GetChallanNo() {
            $('#<%=inputChallan.ClientID%>').autocomplete({
                source: function (request, response) {
                    var vendorName = $('#<%=inputVendorName.ClientID%>').val();
                    $.ajax({
                        url: "UpdateDeliveryCommitDateByVendor.aspx/GetChallanNumber",
                        data: "{ 'text':'" + request.term + "','vendorName':'" + vendorName + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputChallan.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputChallan.ClientID%>').blur();

            });
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <asp:UpdatePanel ID="updatePage" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 100%; border: 1px solid Blue; min-height: 100px;">
                <div style="padding: 9px;">
                    <div>
                        <div align="center">
                            <span class="style13">Update Delivery Commitment Date</span>
                        </div>
                    </div>
                    <div style="margin-bottom: 10px; margin-top: 20px;">
                        <div style="font-size: 13px; float: left; margin-right: 6px;">
                            Vendor Name
                            <br />
                            <asp:TextBox runat="server" ID="inputVendorName" ClientIDMode="Static" CssClass="autocomplete glowing-border"
                                Style="width: 170px;" Placeholder=" Vendor Name"></asp:TextBox>
                        </div>
                        <div style="font-size: 13px; width: 190px; float: left;">
                            Challan No
                            <asp:TextBox runat="server" ID="inputChallan" ClientIDMode="Static" CssClass="autocomplete glowing-border"
                                Style="width: 170px;" Placeholder=" Challan Number"></asp:TextBox>
                        </div>
                        <div style="float: left; height: 40px; margin-top: 8px;">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click"
                                Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px; color: white;
                                height: 32px; width: 80px; line-height: 28px; text-align: center; border: 2px solid #dadada;" />
                        </div>
                        <div style="float:left;margin-left:250px;margin-bottom:40px">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePage">
                                <ProgressTemplate>
                                    <img alt="" src="../ajax-loader.gif"  style="width: 32px; height: 32px;"  /><%-- code changed by himaneesh adding the loading button --%>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                        
                        <div style="float: right; height: 40px; margin-top: 1px;"><!--code changed by himaneesh -->
                            Required_By_Date
                            <asp:TextBox ID="inputrequiredbydate" CssClass="glowing-border" runat="server"
                                AutoPostBack="true" OnTextChanged="inputrequiredbydate_TextChanged" Width="150px"></asp:TextBox>
                            <cc1:CalendarExtender ID="inputreq_by_cal" runat="server" Format="dd-MMM-yyyy"
                                Enabled="true" TargetControlID="inputrequiredbydate" OnClientShowing="CurrentDateShowing">
                            </cc1:CalendarExtender>
                            <asp:Button ID="btnupdate_req_date" runat="server" OnClick="btnupdate_req_date_Click"
                                Text="Update Required By Date" Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px;
                                color: white; height: 32px; text-align: center; margin-top: 9px; border: 2px solid #dadada;" />
                        </div>
                        <div style="float: right; height: 40px; margin-top: 1px; margin-right:3px;">
                            Delievery Commitment Date
                            <asp:TextBox ID="inputDeliveryCommitDate" CssClass="glowing-border" runat="server"
                                AutoPostBack="true" OnTextChanged="inputDeliveryCommitDate_TextChanged" Width="150px"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                Enabled="true" TargetControlID="inputDeliveryCommitDate" OnClientShowing="CurrentDateShowing">
                            </cc1:CalendarExtender>
                            <asp:Button ID="btnUpdateDeliveryCommitDate" runat="server" OnClick="btnUpdateDeliveryCommitDate_Click"
                                Text="Update Delivery Commitment Date" Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px;
                                color: white; height: 32px; text-align: center; margin-top: 9px; border: 2px solid #dadada;" />
                        </div>
                    </div>
                    <asp:GridView runat="server" ID="gridViewSentChallans" AutoGenerateColumns="false"
                        Width="100%" ShowHeaderWhenEmpty="true" AllowPaging="false" AllowSorting="false"
                        Style="font-size: 11.5px; font-family: Verdana; line-height: 26px;">
                        <RowStyle BackColor="White" ForeColor="Black" Font-Size="12px" />
                        <Columns>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="60px" HeaderStyle-ForeColor="White">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll_CheckedChanged" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkAllchild" AutoPostBack="true" runat="server" OnCheckedChanged="chkAllchild_CheckedChanged">
                                    </asp:CheckBox>
                                    <asp:HiddenField runat="server" Value='<%#Eval("Part_History_Id") %>' ID="hdnPartHistoryId">
                                    </asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Tool_No" HeaderText="Tool" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="160px" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Challan_No" HeaderText="Challan No" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Station" HeaderText="Station" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="70px" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="P_No" HeaderText="P_No" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="70px" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Rwk_No" HeaderText="Rwk" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Part_Name" HeaderText="Part Name" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="110px" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Quantity" HeaderText="Sent Quantity" HeaderStyle-BackColor="#335599"
                                HeaderStyle-Width="130px" HeaderStyle-ForeColor="White" />
                            <asp:BoundField DataField="Date_Of_Trans" DataFormatString="{0:dd-MM-yyyy}" HeaderText="Date Of Trans"
                                HeaderStyle-BackColor="#335599" HeaderStyle-Width="130px" HeaderStyle-ForeColor="White" />
                            <%-- code changed by himaneesh --%>
                            <asp:BoundField DataField="Required_By_Date" DataFormatString="{0:dd-MM-yyyy}"
                                HeaderText="Required By Date" HeaderStyle-BackColor="#335599" HeaderStyle-Width="130px"
                                HeaderStyle-ForeColor="White" />
                            <%-- code changed by himaneesh --%>
                            <asp:BoundField DataField="Delivery_Commitment_Date" DataFormatString="{0:dd-MM-yyyy}"
                                HeaderText="Delivery Commitment Date" HeaderStyle-BackColor="#335599" HeaderStyle-Width="130px"
                                HeaderStyle-ForeColor="White" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <script type="text/javascript">
                Sys.WebForms.PageRequestManager.getInstance().add_pageL
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

                function EndRequestHandler() {

                    GetVendor();
                    GetChallanNo();
                }
            </script>
        </ContentTemplate>
        
    </asp:UpdatePanel>
     
</asp:Content>
