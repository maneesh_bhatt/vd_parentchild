﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.master"
    CodeFile="upload_Excel_part.aspx.cs" Inherits="upload_Excel_part" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <style type="text/css">
        .style2, .style1
        {
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <table style="margin-left: 150px; padding-left: 36px; width: 80%; line-height: 40px;"
        border="1px" align="center">
        <tr>
            <td colspan="2" align="center" style="font-size: x-large; color: black; border: none;">
                <span class="style1">Upload Part List (Fresh / Rework)</span>
            </td>
        </tr>
        <tr>
            <td class="style2" style="width: 266px;">
                <b>Select Upload Type: </b>
            </td>
            <td style="border: none;">
                <asp:RadioButton runat="server" GroupName="UploadType" AutoPostBack="true" OnCheckedChanged="radioOldTool_CheckedChanged"
                    ID="radioOldTool" Text="Old Tool" />
                &nbsp;&nbsp;
                <asp:RadioButton runat="server" GroupName="UploadType" AutoPostBack="true" OnCheckedChanged="radioNewTool_CheckedChanged"
                    ID="radioNewTool" Text="New Tool" />
            </td>
        </tr>
        <asp:Panel runat="server" ID="panelControls" Visible="false">
            <tr align="left">
                <td class="style2" colspan="3">
                    <asp:Label ID="lblchallan" runat="server" Text="Internal Challan No:"></asp:Label>
                    <asp:Label ID="lblchallanno" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2" align="center">
                    <asp:Label ID="Label1" runat="server" Text="Vendor"></asp:Label>
                </td>
                <td class="style2">
                    <asp:DropDownList ID="DDLvendor" runat="server" Width="150px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="margin-top: 50px;" align="left" class="style2">
                    Upload Date &nbsp&nbsp
                </td>
                <td class="style2">
                    <asp:TextBox ID="txtuploaddate" runat="server" Width="152px" AutoPostBack="true"
                        OnTextChanged="txtuploaddate_TextChanged"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="true" Format="dd-MMM-yyyy"
                        OnClientShowing="CurrentDateShowing" TargetControlID="txtuploaddate">
                    </cc1:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td style="margin-top: 50px;" align="left" class="style2">
                    Browse File
                </td>
                <td class="style2" align="left">
                    <asp:FileUpload ID="FileUploadControl" runat="server" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only Exl file is allowed!"
                        ValidationExpression="^.+(.xlsx|.xls|.csv)$" ControlToValidate="FileUploadControl"> </asp:RegularExpressionValidator>
                    <br />
                </td>
            </tr>
            <tr align="center">
                <td style="border: none; text-align: left;">
                    <a href="../SampleFormats/NEW%20Software%20loading%20PPCSheet.xls">Download Excel Format</a>
                </td>
            </tr>
            <tr>
                <td class="style2">
                </td>
                <td class="style2" align="left">
                    <asp:ImageButton ID="btnsave" Width="99px" ImageUrl="~/Tool/upload.jpg" Height="44px"
                        Visible="true" Text="Upload" runat="server" OnClick="UploadButton_Click" />
                    <asp:Label ID="lbl" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2" colspan="2">
                    <asp:Label ID="lblErrorMessage" ForeColor="Red" runat="server" Style="line-height: 20px;
                        font-size: 13px;"></asp:Label>
                </td>
            </tr>
        </asp:Panel>
        <tr>
            <td class="style2" colspan="2">
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" EmptyDataText="No Record Found"
                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None"
                    Font-Size="13px" Width="100%">
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <Columns>
                        <asp:BoundField DataField="p_no" HeaderText="p no" SortExpression="p_no" ReadOnly="True">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="part_name" HeaderText="part name" SortExpression="part_name"
                            ReadOnly="True">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="station" HeaderText="station" SortExpression="station"
                            ReadOnly="True">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="location" HeaderText="location" SortExpression="location"
                            ReadOnly="True">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#999999" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
