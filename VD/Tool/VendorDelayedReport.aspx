﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="VendorDelayedReport.aspx.cs" Inherits="Tool_VendorDelayedReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 1000px;
            height: 500px;
            top: 16%;
            left: 25%;
            margin-left: -185px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 9pt;
            line-height: 28px;
            overflow-y: scroll;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .awbimagecontainer
        {
            float: right;
            width: 29%;
            margin-right: 12px;
            margin-top: 43px;
            border: 1px solid #2D89EF;
            height: 180px;
        }
        .web_dialog_info
        {
            display: none;
            position: fixed;
            width: 940px;
            height: 510px;
            top: 22%;
            left: 36%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
    </style>
    <script language="javascript" type="text/javascript">

        $(function () {
            $('#<%=inputVendorName.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "VendorMasterJobWorkData.aspx/GetVendorName",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                // The following works only once.
                // $(this).trigger('keydown.autocomplete');
                // As suggested by digitalPBK, works multiple times
                $(this).autocomplete("search", "");
            }).on('change', function (e, ui) {

                $('#<%=inputVendorName.ClientID%>').blur();

            }).on('autocompleteselect', function (e, ui) {

                $('#<%=inputVendorName.ClientID%>').blur();

            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="width: 100%; border: 1px solid Blue; min-height: 100px;">
        <div style="padding: 9px;">
            <div>
                <div align="center">
                    <span class="style13">Vendor Delayed Report</span>
                </div>
            </div>
            <div style="width: 100%; margin-bottom: 10px; margin-top: 20px;">
                <div style="font-size: 13px; width: 180px; float: left;">
                    Location<br />
                    <asp:TextBox runat="server" ID="inputVendorName" ClientIDMode="Static" CssClass="autocomplete glowing-border"
                        Style="width: 170px;" Placeholder=" Vendor Name"></asp:TextBox>
                </div>
                <div style="font-size: 13px; width: 95px; float: left;">
                    Start Date
                    <br />
                    <asp:TextBox ID="inputFromDate" CssClass="glowing-border" runat="server" Width="85px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                        Enabled="true" TargetControlID="inputFromDate" OnClientShowing="CurrentDateShowing">
                    </cc1:CalendarExtender>
                </div>
                <div style="font-size: 13px; width: 95px; float: left;">
                    End Date
                    <br />
                    <asp:TextBox ID="inputToDate" CssClass="glowing-border" runat="server" Width="85px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="true" Format="dd-MMM-yyyy"
                        TargetControlID="inputToDate" OnClientShowing="CurrentDateShowing">
                    </cc1:CalendarExtender>
                </div>
                <div style="font-size: 13px; width: 280px; float: left;">
                    Vendor Type
                    <br />
                    <asp:CheckBox runat="server" ID="checkBoxInternalVendors" Text="Internal Vendor" />
                    <asp:CheckBox runat="server" ID="checkBoxEnternalVendors" Checked="true" Text="External Vendor" />
                </div>
                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/images/searchbtn.jpg"
                    OnClick="btnSearch_Click" Style="margin-top: 10px;" />
                <%-- <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="btnDownload_Click"
                Visible="false" Style="margin-top: 9px; margin-right: 2px; float: right; background-color: green;
                color: white; padding: 6px; border: 1px solid green; box-shadow: 2px 3px 2px green;" />--%>
            </div>
            <asp:GridView ID="girdVendorDelayedReport" runat="server" EmptyDataText="No Record Found"
                AutoGenerateColumns="False" OnRowDataBound="girdVendorDelayedReport_RowDataBound"
                OnSelectedIndexChanged="girdVendorDelayedReport_SelectedIndexChanged" OnRowCommand="girdVendorDelayedReport_RowCommand"
                OnSorting="girdVendorDelayedReport_Sorting" AllowPaging="false" AllowSorting="true"
                Style="font-size: 13px; font-family: Verdana; line-height: 26px;" Width="100%">
                <RowStyle BackColor="White" ForeColor="Black" />
                <Columns>
                    <asp:BoundField HeaderText="Location" DataField="Location" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="150px" HeaderStyle-ForeColor="White" SortExpression="Location" />
                    <asp:BoundField HeaderText="Positions Sent" DataField="PositionsSent" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" SortExpression="PositionsSent" />
                    <asp:TemplateField HeaderText="DateChangeCount" HeaderStyle-BackColor="#335599" HeaderStyle-Width="60px"
                        SortExpression="DateChangeCount" HeaderStyle-ForeColor="White">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lnkbtnView" Text='<%#Eval("DateChangeCount") %>'
                                CommandName="View" CommandArgument='<%#Eval("Location") %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div>
            <div id="overlay" runat="server" clientidmode="Static" class="web_dialog_overlay">
            </div>
            <div id="dialog" clientidmode="Static" runat="server" class="web_dialog">
                <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                    <div style="float: left; width: 100%;">
                        <div class="web_dialog_title" style="width: 40%; float: left; margin-bottom: 10px;">
                            Part Wise Date Change Summary
                        </div>
                        <div class="web_dialog_title align_right">
                            <asp:LinkButton runat="server" ID="btnClosePopUp" ClientIDMode="Static" OnClick="btnClosePopUp_Click">Close</asp:LinkButton>
                        </div>
                    </div>
                    <div style="margin: 16px; float: left;">
                        <h3 style="margin: 0px;">
                            <asp:Label runat="server" ID="lblLocation"></asp:Label></h3>
                        <asp:GridView runat="server" ID="gridViewLocationWiseData" AutoGenerateColumns="false"
                            OnRowCommand="gridViewLocationWiseData_RowCommand" AllowSorting="true" OnSorting="gridViewLocationWiseData_Sorting">
                            <Columns>
                                <asp:BoundField HeaderText="Tool" DataField="Tool_No" HeaderStyle-BackColor="#335599"
                                    HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" SortExpression="Tool_No" />
                                <asp:BoundField HeaderText="P No" DataField="P_No" HeaderStyle-BackColor="#335599"
                                    HeaderStyle-Width="70px" HeaderStyle-ForeColor="White" SortExpression="P_No" />
                                <asp:BoundField HeaderText="Part Name" DataField="Part_Name" HeaderStyle-BackColor="#335599"
                                    HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" SortExpression="Part_Name" />
                                <asp:BoundField HeaderText="Station" DataField="Station" HeaderStyle-BackColor="#335599"
                                    HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" SortExpression="Station" />
                                <asp:BoundField HeaderText="Rwk No" DataField="Rwk" HeaderStyle-BackColor="#335599"
                                    HeaderStyle-Width="90px" HeaderStyle-ForeColor="White" SortExpression="Rwk" />
                                <asp:BoundField HeaderText="Qty Sent" DataField="SentQuantity" HeaderStyle-BackColor="#335599"
                                    HeaderStyle-Width="70px" HeaderStyle-ForeColor="White" SortExpression="SentQuantity" />
                                <asp:BoundField HeaderText="DateOfTrans" DataField="Date_Of_Trans" DataFormatString="{0:dd-MM-yyyy}"
                                    HeaderStyle-BackColor="#335599" HeaderStyle-Width="100px" HeaderStyle-ForeColor="White"
                                    SortExpression="Date_Of_Trans" />
                                <asp:BoundField HeaderText="Expected Delivery Date" DataField="Delivery_Commit_Date"
                                    DataFormatString="{0:dd-MM-yyyy}" HeaderStyle-BackColor="#335599" HeaderStyle-Width="150px"
                                    HeaderStyle-ForeColor="White" SortExpression="Delivery_Commit_Date" />
                                <asp:TemplateField HeaderText="DateChangeCount" HeaderStyle-BackColor="#335599" HeaderStyle-Width="60px"
                                    SortExpression="DateChangeCount" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="lnkbtnView" Text='<%#Eval("DateChangeCount") %>'
                                            CommandName="ViewDateChangedData" CommandArgument='<%#Eval("Part_History_Id") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div id="dialogCommitmentHistory" clientidmode="Static" runat="server" class="web_dialog">
                <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                    <div style="float: left; width: 100%;">
                        <div class="web_dialog_title" style="width: 20%; float: left;">
                            Commitment History
                        </div>
                        <div class="web_dialog_title align_right">
                            <asp:LinkButton runat="server" ID="btnCloseCommitmentHistory" ClientIDMode="Static"
                                OnClick="btnCloseCommitmentHistory_Click">Close</asp:LinkButton>
                        </div>
                    </div>
                    <div style="margin: 16px; float: left;">
                        <asp:GridView runat="server" ID="gridViewDateChangeSummary" AutoGenerateColumns="false"
                            OnRowCommand="gridViewLocationWiseData_RowCommand">
                            <Columns>
                                <asp:BoundField HeaderText="Tool" DataField="Tool_No" HeaderStyle-BackColor="#335599"
                                    HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                                <asp:BoundField HeaderText="P No" DataField="P_No" HeaderStyle-BackColor="#335599"
                                    HeaderStyle-Width="120px" HeaderStyle-ForeColor="White" />
                                <asp:BoundField HeaderText="Part Name" DataField="Part_Name" HeaderStyle-BackColor="#335599"
                                    HeaderStyle-Width="140px" HeaderStyle-ForeColor="White" />
                                <asp:BoundField HeaderText="Station" DataField="Station" HeaderStyle-BackColor="#335599"
                                    HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" />
                                <asp:BoundField HeaderText="Rwk No" DataField="Rework_No" HeaderStyle-BackColor="#335599"
                                    HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                                <asp:BoundField HeaderText="Location" DataField="Location" HeaderStyle-BackColor="#335599"
                                    HeaderStyle-Width="160px" HeaderStyle-ForeColor="White" />
                                <asp:BoundField HeaderText="Commitment Date" DataField="Commitment_Date" DataFormatString="{0:dd-MM-yyyy}"
                                    HeaderStyle-BackColor="#335599" HeaderStyle-Width="200px" HeaderStyle-ForeColor="White" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
