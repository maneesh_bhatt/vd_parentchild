﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="DesignHoldPartSummary.aspx.cs" Inherits="Tool_DesignHoldPartSummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


    </script>
    <style type="text/css">
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 860px;
            height: 440px;
            top: 20%;
            left: 30%;
            margin-left: -185px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
            line-height: 28px;
            overflow-y: scroll;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .awbimagecontainer
        {
            float: right;
            width: 29%;
            margin-right: 12px;
            margin-top: 43px;
            border: 1px solid #2D89EF;
            height: 180px;
        }
        .web_dialog_info
        {
            display: none;
            position: fixed;
            width: 940px;
            height: 510px;
            top: 22%;
            left: 36%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <center>
                <img alt="" src="../ajax-loader.gif" style="width: 32px; height: 32px" />
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel1">
        <ContentTemplate>
            <table align="left" width="100%">
                <tr>
                    <td colspan="2" align="center" style="font-size: x-large; color: black">
                        <span class="style1">Design Hold Part Summary</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:Panel ID="Panel4" runat="server" Style="margin-top: 10px;">
                            Project No &nbsp;<asp:TextBox ID="txtprojectno" runat="server" CssClass="glowing-border"
                                Width="100px"></asp:TextBox>
                            <asp:Button ID="btnSearch" runat="server" CssClass="glowing-border" Text="Search"
                                CausesValidation="false" OnClick="btnSearch_Click" />
                            &nbsp; Sending Date<span class="style12">&nbsp;</span>
                            <asp:TextBox ID="txtReceivecdate" CssClass="glowing-border" runat="server" AutoPostBack="true"
                                Width="90px" OnTextChanged="txtReceivecdate_TextChanged"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                Enabled="true" TargetControlID="txtReceivecdate" OnClientShowing="CurrentDateShowing">
                            </cc1:CalendarExtender>
                            &nbsp; &nbsp;
                            <asp:Label ID="lblChallanno" runat="server" Text="Challan No"></asp:Label>
                            <asp:TextBox ID="txtchallanno" CssClass="glowing-border" Visible="False" runat="server"
                                Width="80px" Enabled="False"></asp:TextBox>
                            &nbsp;
                            <asp:Button ID="btnprint" runat="server" Enabled="true" EnableViewState="true" Text="PrintChallan"
                                OnClick="btnprint_Click" />
                            <div style="margin-top: 10px;">
                                Next Expected Delivery Date:
                                <asp:TextBox ID="txtdelcdate" CssClass="glowing-border" runat="server" Width="120px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                    Enabled="true" OnClientShowing="CurrentDateShowing" TargetControlID="txtdelcdate">
                                </cc1:CalendarExtender>
                                &nbsp;
                                <asp:Button ID="btnUpdateCommitmentDate" runat="server" CssClass="glowing-border"
                                    Style="cursor: pointer;" Text="Update Date" OnClick="btnUpdateCommitmentDate_Click" />
                            </div>
                        </asp:Panel>
                        <asp:Button ID="btnRecieved" runat="server" CausesValidation="true" EnableViewState="true"
                            Text="Send To Company" Visible="false" Style="width: 130px; float: right; margin-top: -58px;
                            margin-right: 52px;" OnClick="btnRecieved_Click" />
                        <asp:Button ID="btnUpdateAsInactive" runat="server" CausesValidation="true" EnableViewState="true"
                            Text="Update As Inactive" Visible="false" Style="width: 130px; color: white;
                            float: right; margin-top: -28px; margin-right: 52px; background-color: red; padding: 4px;
                            border: 1px solid black; box-shadow: 1px 2px 0px red;" OnClick="btnUpdateAsInactive_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="2">
                        <asp:Panel ID="Panel2" runat="server">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Location :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                        <asp:DropDownList ID="ddllocauto" CssClass="glowing-border" AppendDataBoundItems="true"
                                            Width="200px" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Received Qty :&nbsp;&nbsp;
                                        <asp:TextBox ID="txtsentQty1" CssClass="glowing-border" runat="server" Width="200px"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="15px">
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Barcode No :&nbsp;&nbsp;&nbsp; &nbsp;
                                        <asp:TextBox ID="txtbarcode" CssClass="glowing-border" runat="server" AutoPostBack="true"
                                            onkeydown="myFunctionL(event)" Width="200px" OnTextChanged="txtbarcode_TextChanged"></asp:TextBox>
                                    </td>
                                    <td colspan="2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Item Name :&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                        <asp:TextBox ID="txtname1" CssClass="glowing-border" runat="server" Width="200px"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="15px">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Tool No :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                        <asp:TextBox ID="txttoolno" CssClass="glowing-border" runat="server" Width="200px"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                    <td colspan="2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Station No :&nbsp; &nbsp; &nbsp;&nbsp;
                                        <asp:TextBox ID="txtstationno" CssClass="glowing-border" runat="server" Width="200px"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="15px">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Position No :&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                        <asp:TextBox ID="txtposition" CssClass="glowing-border" runat="server" Width="200px"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                    <td colspan="2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New
                                        Receive Qty :&nbsp;
                                        <asp:TextBox ID="txtQty" CssClass="glowing-border" runat="server" Width="200px" OnTextChanged="txtQty_TextChanged"
                                            ReadOnly="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="15px">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Total Sent Qty :&nbsp;&nbsp;<asp:TextBox ID="txttotalsent" CssClass="glowing-border"
                                            runat="server" Enabled="False" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td height="30px">
                                        <asp:Label ID="lbl" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td height="10px">
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <td>
                            <asp:Panel ID="Panel1" runat="server"  ScrollBars="Horizontal">
                                <asp:GridView ID="GridView1" runat="server" EmptyDataText="No Record Found" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                    Style="border: 1px solid #aba4a4;" OnRowEditing="GridView1_RowEditing" AutoGenerateColumns="False" CellPadding="4"
                                    ForeColor="#333333" GridLines="None" Font-Size="13px" Width="100%" OnRowDataBound="GridView1_RowDataBound">
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAll1" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll1_CheckedChanged1" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkAllchild" AutoPostBack="true" runat="server" OnCheckedChanged="chkAllchild_CheckedChanged">
                                                </asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Tool No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="Server" Text='<%# Eval("tool_no") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Station
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="Server" Text='<%# Eval("station") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Position No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Label3" CausesValidation="false" runat="server" Text='<%# Eval("p_no") %>'
                                                    CommandArgument='<%# Eval("p_no") + ";" + Eval("tool_no") + ";" + Eval("station") + ";" + Eval("part_name") +";"+Eval("Rwk") %>'
                                                    OnClick="Label3_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Part Name
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="Server" Text='<%# Eval("part_name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Total Quantity Sent
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="Server" Text='<%# Eval("quant_sent") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Total Received Quantity
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="Server" Text='<%# Eval("quant_rec") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                New Qty Received
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox ID="TextBox2" runat="server" CssClass="glowing-border" onkeypress="return isNumberKey(event)"
                                                    Text="" AutoPostBack="true" Enabled="false" Width="150px" OnTextChanged="TextBox2_TextChanged"></asp:TextBox>
                                                <cc1:NumericUpDownExtender ID="NumericUpDownExtender3" runat="server" TargetControlID="TextBox2"
                                                    Width="120" RefValues="" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                    TargetButtonUpID="" Minimum="0" />
                                                <%--<asp:TextBox ID="Textbox1" onkeypress="return isNumberKey(event)" Enabled="false"  Width="150px" runat="server"></asp:TextBox>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Upl Qty
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label6Upload_Qty" runat="Server" Text='<%# Eval("Upload_Qty") %>'></asp:Label>
                                                <%--<asp:TextBox ID="Textbox1" onkeypress="return isNumberKey(event)" Enabled="false"  Width="150px" runat="server"></asp:TextBox>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Sent Qty
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label6sentqty" runat="Server" Text='<%# Eval("SentQty") %>'></asp:Label>
                                                <%--<asp:TextBox ID="Textbox1" onkeypress="return isNumberKey(event)" Enabled="false"  Width="150px" runat="server"></asp:TextBox>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Rework No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblrew" runat="Server" Text='<%# Eval("Rwk") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Last Expected Delivery Date
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%-- <asp:Label ID="lblLastCommitmentDate" runat="Server"></asp:Label>--%>
                                                <asp:LinkButton ID="lnkBtnLastCommitmentDate" CausesValidation="false" runat="server"
                                                    CommandArgument='<%# Eval("p_no") + ";" + Eval("tool_no") + ";" + Eval("station") + ";" + Eval("part_name") +";"+Eval("Rwk") %>'
                                                    OnClick="lnkBtnLastCommitmentDate_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Download Excel
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:HyperLink runat="server" ID="hyperLnkExcelFile" Target="_blank"></asp:HyperLink>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="overlay" runat="server" clientidmode="Static" class="web_dialog_overlay">
                        </div>
                        <div id="dialog" clientidmode="Static" runat="server" class="web_dialog">
                            <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                                <div style="float: left; width: 100%;">
                                    <div class="web_dialog_title" style="width: 20%; float: left;">
                                        Item History
                                    </div>
                                    <div class="web_dialog_title align_right">
                                        <asp:LinkButton runat="server" ID="btnClosePopUp" ClientIDMode="Static" OnClick="btnClosePopUp_Click">Close</asp:LinkButton>
                                    </div>
                                </div>
                                <div runat="server" id="pnlHistoryPopUp">
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="overlayCommitmentHistory" runat="server" clientidmode="Static" class="web_dialog_overlay">
                        </div>
                        <div id="dialogCommitmentHistory" clientidmode="Static" runat="server" class="web_dialog">
                            <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                                <div style="float: left; width: 100%;">
                                    <div class="web_dialog_title" style="width: 40%; float: left;">
                                       Expected Delivery Date History
                                    </div>
                                    <div class="web_dialog_title align_right">
                                        <asp:LinkButton runat="server" ID="btnCloseCommitmentHistory" ClientIDMode="Static"
                                            OnClick="btnCloseCommitmentHistory_Click">Close</asp:LinkButton>
                                    </div>
                                </div>
                                <div runat="server" id="pnlCommitmentHistory">
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                </td> </td> </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
