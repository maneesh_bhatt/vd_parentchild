﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

public partial class Tool_SentChallan : System.Web.UI.Page
{
    String MIN = "";
    SqlConnection mycon;
    SqlCommand cmd;
    SqlDataAdapter adp;
    string Challanno = "";
    string Location = "";
    int total = 0;

    /// <summary>
    /// Generates the Challan Format on the basis of Passed Challan Number, Vendor and Challan Type
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string dateTime = System.DateTime.Now.ToString("dd-MMM-yyyy");
            MIN = Session["MIN"].ToString();
            Challanno = MIN;// Session["Challano"].ToString();
            string challanItems = "", conString = "";
            if (!string.IsNullOrEmpty(MIN) && !string.IsNullOrEmpty(Request.QueryString["ChallanType"]))
            {
                string challanType = Request.QueryString["ChallanType"];

                challanItems = "SELECT phg.p_no, phg.part_name,phg.station,phg.location,phg.date_of_trans ,phg.sent_or_rec "
                             + " ,phg.quantity,phg.tool_no,phg.QtySent,phg.quant_rec ,phg.rwk_no,phg.Explain_Date, "
                             + " phg.Delivery_commit_date,phg.Challan_No,phg.Sr_Reg_No,phg.Batch_No,phg.Hsn_Code,phg.Weight_Value, "
                             + " phg.Weight_Unit,pgr.Matl, pgr.Hrc,phg.Required_Date, phg.PO_Amount, phg.PO_Amount * phg.qtysent as TotalPOAmount FROM [part_history_GR] phg left join Parts_Gr pgr "
                             + " on phg.tool_no=pgr.tool_no and phg.station=pgr.station and phg.rwk_no=pgr.rwk_no and "
                             + " phg.part_name=pgr.part_name and phg.p_no=pgr.p_no "
                             + "  where phg.Challan_No='" + MIN + "'  and phg.sent_or_rec='Sent' and ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) ";
                conString = "SELECT     Rework,Fresh,[location],PO_NO ,CONVERT(VARCHAR(10), isnull(date_of_trans,''), 103) as date_of_trans   ,CONVERT(VARCHAR(10), isnull(Explain_Date,''), 103) as Explain_Date    ,CONVERT(VARCHAR(10), isnull(Delivery_commit_date,''),103) as  Delivery_commit_date   ,isnull([Challan_No],'') as Challan_No     ,isnull([Sr_Reg_No],'') as Sr_Reg_No,Rwk_No from [part_history_GR]  where Challan_No='" + MIN + "' and sent_or_rec='Sent'";

                if (challanItems != "" && conString != "")
                {
                    DataTable dtChallanItems = bindData(challanItems);
                    DataTable dtVendorInfo = BindDetails(conString);
                    DataTable dtVendorMasterJobWork = bindData("select * from Vendor_Master_Job_Work where Vendor_Name='" + Location + "'");

                    if (challanType == "PO")
                    {
                        divChallanHtml1.Attributes.Add("style", "page-break-after: always;");
                        divChallanHtml1.InnerHtml = GenerateHtml(dtChallanItems, dtVendorInfo, dtVendorMasterJobWork, "Vendor Copy", "Purchase Order");
                        divChallanHtml2.InnerHtml = GenerateHtml(dtChallanItems, dtVendorInfo, dtVendorMasterJobWork, "Office Copy", "Purchase Order");
                    }
                    else if (challanType == "JW" || challanType == "RC" || challanType == "NR")
                    {
                        string materialType = "";
                        if (challanType.ToLower() == "jw")
                        {
                            materialType = "Job Work";
                        }
                        else if (challanType.ToLower() == "rc")
                        {
                            materialType = "Rework Challan";
                        }
                        else if (challanType.ToLower() == "nr")
                        {
                            materialType = "Non Returnable Reject Challan";
                        }
                        divChallanHtml1.Attributes.Add("style", "page-break-after: always;");
                        divChallanHtml2.Attributes.Add("style", "page-break-after: always;");
                        divChallanHtml1.InnerHtml = GenerateHtml(dtChallanItems, dtVendorInfo, dtVendorMasterJobWork, "Vendor Copy", materialType);
                        divChallanHtml2.InnerHtml = GenerateHtml(dtChallanItems, dtVendorInfo, dtVendorMasterJobWork, "Gate Copy", materialType);
                        divChallanHtml3.InnerHtml = GenerateHtml(dtChallanItems, dtVendorInfo, dtVendorMasterJobWork, "Office Copy", materialType);
                    }
                    else if (challanType == "NA")
                    {
                        divChallanHtml1.Attributes.Add("style", "page-break-after: always;");
                        divChallanHtml1.InnerHtml = GenerateHtml(dtChallanItems, dtVendorInfo, dtVendorMasterJobWork, "Vendor Copy", "");
                    }
                }


                string print = "<script language='javascript' type='text/javascript'> window.print(); </script>";
                Page.RegisterStartupScript("Print", print);
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
    }

    /// <summary>
    /// Generated the Html format
    /// </summary>
    /// <param name="dtChallanItems"></param>
    /// <param name="dtVendorInfo"></param>
    /// <param name="dtVendorMasterJobWork"></param>
    /// <param name="heading"></param>
    /// <param name="materialType"></param>
    /// <returns></returns>
    protected string GenerateHtml(DataTable dtChallanItems, DataTable dtVendorInfo, DataTable dtVendorMasterJobWork, string heading, string materialType)
    {
        try
        {
            string requDate = "", delCommitDate = "", delexplainDate = "";
            if (dtChallanItems.Rows[0]["Required_Date"] != DBNull.Value)
            {
                requDate = Convert.ToDateTime(dtChallanItems.Rows[0]["Required_Date"]).ToString("dd/MM/yyyy");
                if (requDate == "01/01/1900")
                {
                    requDate = "";
                }
            }

            if (dtVendorInfo.Rows[0]["Explain_Date"] != DBNull.Value)
            {
                delexplainDate = Convert.ToDateTime(dtVendorInfo.Rows[0]["Explain_Date"]).ToString("dd/MM/yyyy");
                if (delexplainDate == "01/01/1900")
                {
                    delexplainDate = "";
                }
            }
            if (dtVendorInfo.Rows[0]["Delivery_commit_date"] != DBNull.Value)
            {
                delCommitDate = Convert.ToDateTime(dtVendorInfo.Rows[0]["Delivery_commit_date"]).ToString("dd/MM/yyyy");
                if (delCommitDate == "01/01/1900")
                {
                    delCommitDate = "";
                }
            }
            string autoGeneratedChallan = "";
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["AutoGeneratedChallan"])))
            {
                autoGeneratedChallan = Convert.ToString(Request.QueryString["AutoGeneratedChallan"]);
            }
            StringBuilder sb = new StringBuilder();
            sb.Append("<div style='width:960px;margin:auto;word-wrap:break-word;line-height: 24px;text-align:center;font-size:14px;'>");
            sb.Append("<div style='width:100%;'><div style='float:left;width:25%;'><img src='../images/FALCON%20LOGO%20quarter.jpg'/></div>");
            sb.Append("<div style='float: left;font-weight:bold;width: 74%;'><div style='font-size:20px;'>Falcon AutoTech Pvt Ltd</div>");
            sb.Append("<div>Plot No. 308-309,Ecotech Ext-1, Greater Noida, Distt- Gautam Budh Nagar,U.P, PIN-201308</div>");
            sb.Append("<div>E-Mail-falconautotech@hotmail.com</div>");
            sb.Append("<div style='font-size:16px;'>GST No: 09AABCF5821E1ZV </div></div></div>");
            sb.Append("<div style='width: 100%;text-align: center;font-weight: bold;line-height: 55px;font-size:18px;'> Challan- " + heading + "</div>");
            sb.Append("<div style='width: 100%;text-align: center;line-height: 1px;font-size:17px;'>  " + materialType + "</div>");
            sb.Append("<div style='width: 100%;line-height: 22px;'><div style='width: 40%;float: left;text-align:left;'>");
            sb.Append("<div><b>Vendor Name:</b> <span style='font-size:16px;'>" + dtVendorMasterJobWork.Rows[0]["Vendor_Name"] + " </span></div>");
            sb.Append("<div><b>Address:</b> " + dtVendorMasterJobWork.Rows[0]["Vendor_Address"] + " </div>");
            sb.Append("<div><b>Email:</b> " + dtVendorMasterJobWork.Rows[0]["Email"] + " </div>");
            sb.Append("<div><b>Mobile No:</b> " + dtVendorMasterJobWork.Rows[0]["Phone"] + " </div>");
            sb.Append("<div><b>GST No:</b> " + dtVendorMasterJobWork.Rows[0]["GST_Number"] + " </div>");
            sb.Append("<div><b>Expected Commit Date:</b> " + delexplainDate + " </div>");
            sb.Append("<div><b>Delivery Commit Date:</b> " + delCommitDate + " </div></div>");

            sb.Append("<div style='width: 25%;text-align: left;float: right;'>");
            sb.Append("<div><b>Challan No:</b> " + MIN + " </div>");
            sb.Append("<div><b>Autogenerated Challan No:</b> " + autoGeneratedChallan + " </div>");
            sb.Append("<div><b>Po No:</b> " + dtVendorInfo.Rows[0]["Po_No"] + " </div>");
            sb.Append("<div><b>Sent Date:</b> " + dtVendorInfo.Rows[0]["Date_Of_Trans"] + " </div>");
            sb.Append("<div><b>Prepared By:</b> " + Convert.ToString(Session["UserID"]) + " </div></div></div>");
            sb.Append("<p style='width: 100%;float: left;text-align: left;font-size:16px;'>Following items are being sent for processes on return basis.</p>");
            sb.Append("<table style='border:1px solid #000000;border-collapse:collapse;width:100%;'>");
            sb.Append("<tr style='font-weight:bold;'>");
            sb.Append("<td style='border:1px solid #000000;border-collapse:collapse;'>Sr.No.</td><td  style='border:1px solid #000000;border-collapse:collapse;'>Project No/ Station/ Rework No/ Position</td><td style='border:1px solid #000000;border-collapse:collapse;'>HSN/ SAC Code</td><td style='border:1px solid #000000;border-collapse:collapse;'>Part Name</td><td style='border:1px solid #000000;border-collapse:collapse;'>Qty</td><td  style='border:1px solid #000000;border-collapse:collapse;'>Weight</td><td  style='border:1px solid #000000;border-collapse:collapse;'>MOC</td><td  style='border:1px solid #000000;border-collapse:collapse;'>HRC</td>");
            if (materialType == "Purchase Order" || materialType == "Job Work")
            {
                sb.Append("<td  style='border:1px solid #000000;border-collapse:collapse;'>PO Amount</td><td  style='border:1px solid #000000;border-collapse:collapse;'>Total Amount</td>");
            }
            sb.Append("</tr>");
            int totalQty = 0, totalAmount = 0;
            for (int i = 0; i < dtChallanItems.Rows.Count; i++)
            {
                int rowNumber = i + 1;
                if (dtChallanItems.Rows[i]["QtySent"] != DBNull.Value)
                    totalQty = totalQty + Convert.ToInt32(dtChallanItems.Rows[i]["QtySent"]);
                if (dtChallanItems.Rows[i]["TotalPOAmount"] != DBNull.Value)
                    totalAmount = totalAmount + Convert.ToInt32(dtChallanItems.Rows[i]["TotalPOAmount"]);
                sb.Append("<tr>");
                sb.Append("<td style='border:1px solid #000000;border-collapse:collapse;'>" + rowNumber + "</td><td  style='border:1px solid #000000;border-collapse:collapse;'>" + Convert.ToString(dtChallanItems.Rows[i]["Tool_No"]) + " / " + Convert.ToString(dtChallanItems.Rows[i]["Station"]) + " / " + Convert.ToString(dtChallanItems.Rows[i]["Rwk_No"]) + " / " + Convert.ToString(dtChallanItems.Rows[i]["P_NO"]) + "</td><td style='border:1px solid #000000;border-collapse:collapse;'>" + Convert.ToString(dtChallanItems.Rows[i]["HSN_Code"]) + "</td><td style='border:1px solid #000000;border-collapse:collapse;'>" + Convert.ToString(dtChallanItems.Rows[i]["Part_Name"]) + "</td><td style='border:1px solid #000000;border-collapse:collapse;'>" + Convert.ToString(dtChallanItems.Rows[i]["QtySent"]) + "</td><td  style='border:1px solid #000000;border-collapse:collapse;'>" + Convert.ToString(dtChallanItems.Rows[i]["Weight_Value"]) + " " + Convert.ToString(dtChallanItems.Rows[i]["Weight_Unit"]) + "</td><td  style='border:1px solid #000000;border-collapse:collapse;'>" + Convert.ToString(dtChallanItems.Rows[i]["Matl"]) + "</td><td  style='border:1px solid #000000;border-collapse:collapse;'>" + Convert.ToString(dtChallanItems.Rows[i]["Hrc"]) + "</td>");
                if (materialType == "Purchase Order" || materialType == "Job Work")
                {
                    string perItemAmount = "";
                    if (dtChallanItems.Rows[i]["PO_Amount"] != DBNull.Value)
                        perItemAmount = Convert.ToString(dtChallanItems.Rows[i]["PO_Amount"]);
                    sb.Append("<td  style='border:1px solid #000000;border-collapse:collapse;'>" + perItemAmount + "</td><td  style='border:1px solid #000000;border-collapse:collapse;'>" + Convert.ToString(dtChallanItems.Rows[i]["TotalPoAmount"]) + "</td>");
                }
                sb.Append("</tr>");
            }
            sb.Append("<tr>");
            sb.Append("<td style='border:1px solid #000000;border-collapse:collapse;'></td><td style='border:1px solid #000000;border-collapse:collapse;'></td><td style='border:1px solid #000000;border-collapse:collapse;'></td><td style='border:1px solid #000000;border-collapse:collapse;'><span style='color:blue;'>Total Qty</span></td></td><td style='border:1px solid #000000;border-collapse:collapse;'>" + totalQty + "</td><td style='border:1px solid #000000;border-collapse:collapse;'></td><td style='border:1px solid #000000;border-collapse:collapse;'></td><td style='border:1px solid #000000;border-collapse:collapse;'></td>");
            if (materialType == "Purchase Order" || materialType == "Job Work")
            {
                sb.Append("<td style='border:1px solid #000000;border-collapse:collapse;'><span style='color:blue;'>Total Amount</span></td><td style='border:1px solid #000000;border-collapse:collapse;'>" + totalAmount + "</td>");
            }
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<div style='width: 100%;line-height: 50px;font-weight: bold;margin-top:15px;'><div style='float:left;width:50%;text-align:left;'>Approver 1 Signature</div><div style='float:right;width:50%;text-align:right;'>Approver 2 Signature</div></div>");
            sb.Append("</div>");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            ex.ToString();
            return "";
        }
    }


    /// <summary>
    /// Returns the Location on the basis Passed Query
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    public DataTable BindDetails(string query)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        adp = new SqlDataAdapter(query, conn);
        DataTable dt = new DataTable();
        adp.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            Location = dt.Rows[0]["location"].ToString();
        }
        return dt;
    }

    /// <summary>
    /// Returns the Data Table for Passed Query
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    public DataTable bindData(string query)
    {
        DataTable dtgrid = new DataTable();
        dtgrid = UtilityFunctions.GetData(query);

        if (dtgrid.Rows.Count <= 0)
        {
            dtgrid = null;
        }
        return dtgrid;
    }

}