﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text;
using AjaxControlToolkit;
using System.Web.Services;
using System.Web.Script.Services;
using System.Drawing;
using OfficeExcel = Microsoft.Office.Interop.Excel;
using System.IO;
using ClosedXML.Excel;
using System.Text.RegularExpressions;

public partial class Tool_UpdateDeliveryCommitDateByVendor : System.Web.UI.Page
{
    SqlConnection connection;
    SqlCommand cmd;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowGrid();
        }
    }

    /// <summary>
    /// Displays the list of Items where Expected Delivery Date is Pending for Updation. User can select and Update the Dates
    /// </summary>
    public void ShowGrid()
    {
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            connection = new SqlConnection(sqlconnstring);
            connection.Open();
            string searchCondition = "";
            if (inputChallan.Text != "")
            {
                searchCondition = " cd.Challan_No='" + inputChallan.Text + "'";
            }
            if (inputVendorName.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and cd.Location='" + inputVendorName.Text + "'";
                }
                else
                {
                    searchCondition = " cd.Location='" + inputVendorName.Text + "'";
                }
            }
            //code changed by himaneesh
            //also the entry in this grid view will come if either delivery_commit_date or required date is not filled duirng the send of items
            if (!string.IsNullOrEmpty(searchCondition))
            {
                cmd = new SqlCommand("SELECT phg.Id as Part_History_Id, cd.Challan_No,cd.Sent_Or_Rec,cd.Reference_Challan_No,cd.Tool_No,cd.Part_Name,cd.P_No, "
                + " cd.Station,cd.Rwk_No,cd.Location,cd.Quantity,cd.Outstanding_Qty,cd.Send_Receive_Date as Date_Of_Trans, (case phg.Required_date  when '1900-01-01 00:00:00.000' then  NULL   else phg.Required_date  end) as Required_By_Date, (case phg.Delivery_commit_date  when '1900-01-01 00:00:00.000' then  NULL   else phg.Delivery_commit_date  end) as Delivery_Commitment_Date"
                + " FROM Challan_Sent_Received_Detail cd left join Part_History_Gr phg on cd.Tool_No= phg.Tool_No and "
                + " cd.P_No=phg.P_No and cd.Part_Name=phg.Part_Name and cd.Station=phg.Station and cd.Part_Name=phg.Part_Name and "
                + " cd.Rwk_No=phg.Rwk_No and cd.Challan_No=phg.Challan_No and phg.Sent_Or_Rec='Sent' "
                + " where cd.Sent_Or_Rec='Sent' and cd.Outstanding_Qty>0 and (phg.Delivery_commit_date Is Null or phg.Delivery_commit_Date='1900-01-01 00:00:00.000' or phg.Required_date is NULL or phg.Required_date='1900-01-01 00:00:00.000') and phg.location not in ('VD Spare Store','Store Assembly','Spare Store','Design','Design Wait','Assembly','DIRECTASSEMBLY','GR VD FINISH STORE','PPC','Weld Assembly Waiting','Weld Parts Consumed','Weld Parts Waiting','VD Finish Store',' Inspection') and " + searchCondition + " order by phg.date_of_trans Asc", connection);
            }
            else//updated the code so that now whether delivery commitment date is null or not it will display in gridview
            {

                cmd = new SqlCommand("SELECT phg.Id as Part_History_Id, cd.Challan_No,cd.Sent_Or_Rec,cd.Reference_Challan_No,cd.Tool_No,cd.Part_Name,cd.P_No, "
                               + " cd.Station,cd.Rwk_No,cd.Location,cd.Quantity,cd.Outstanding_Qty,cd.Send_Receive_Date as Date_Of_Trans, (case phg.Required_date  when '1900-01-01 00:00:00.000' then  NULL   else phg.Required_date  end) as Required_By_Date, (case phg.Delivery_commit_date  when '1900-01-01 00:00:00.000' then  NULL   else phg.Delivery_commit_date  end) as Delivery_Commitment_Date"
                               + " FROM Challan_Sent_Received_Detail cd left join Part_History_Gr phg on cd.Tool_No= phg.Tool_No and "
                               + " cd.P_No=phg.P_No and cd.Part_Name=phg.Part_Name and cd.Station=phg.Station and cd.Part_Name=phg.Part_Name and "
                               + " cd.Rwk_No=phg.Rwk_No and cd.Challan_No=phg.Challan_No and phg.Sent_Or_Rec='Sent' "
                               + " where cd.Sent_Or_Rec='Sent' and cd.Outstanding_Qty>0 and (phg.Delivery_commit_date Is Null or phg.Delivery_commit_Date='1900-01-01 00:00:00.000' or phg.Required_date is NULL or phg.Required_date='1900-01-01 00:00:00.000') and phg.location not in ('VD Spare Store','Store Assembly','Spare Store','Design','Design Wait','Assembly','DIRECTASSEMBLY','GR VD FINISH STORE','PPC','Weld Assembly Waiting','Weld Parts Consumed','Weld Parts Waiting','VD Finish Store',' Inspection') order by phg.date_of_trans Asc", connection);
            }
            gridViewSentChallans.DataSource = cmd.ExecuteReader();
            gridViewSentChallans.DataBind();
            gridViewSentChallans.EmptyDataText = "No Data Found";
            updatePage.Update();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ShowGrid();
    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    /// <summary>
    /// Event is fired when user checks/unchecks a child Postion. Controls are Enabled or Disabled on the basis of Selection
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAllchild_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox childCheck = (CheckBox)sender;

        CheckBox ChkBoxRows = (CheckBox)childCheck.FindControl("chkAllchild");
        if (ChkBoxRows.Checked == true)
        {
            ChkBoxRows.Checked = true;
            btnUpdateDeliveryCommitDate.Visible = true;
            btnUpdateDeliveryCommitDate.Enabled = true;
        }
        else
        {
            btnUpdateDeliveryCommitDate.Enabled = false;
        }
        int checkCount = 0;
        foreach (GridViewRow row in gridViewSentChallans.Rows)
        {
            CheckBox chkCheckBoxChecked = (CheckBox)gridViewSentChallans.Rows[row.RowIndex].FindControl("chkAllchild");
            if (chkCheckBoxChecked.Checked)
            {
                checkCount = checkCount + 1;
            }
        }

        if (checkCount > 0)
        {
            btnUpdateDeliveryCommitDate.Visible = true;
            btnUpdateDeliveryCommitDate.Enabled = true;
        }
        updatePage.Update();
    }

    /// <summary>
    /// Event is fired when user checks the Check ALl checkbox shown shown on the Header
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)gridViewSentChallans.HeaderRow.FindControl("chkAll");
        btnUpdateDeliveryCommitDate.Enabled = false;
        int countchk = 0;
        foreach (GridViewRow row in gridViewSentChallans.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            if (ChkBoxHeader.Checked == true)
            {
                if (ChkBoxRows.Enabled)
                {
                    btnUpdateDeliveryCommitDate.Enabled = true;
                    ChkBoxRows.Checked = true;
                    countchk = countchk + 1;
                }
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }
        if (countchk > 0)
        {
            btnUpdateDeliveryCommitDate.Visible = true;
            btnUpdateDeliveryCommitDate.Enabled = true;
        }
        updatePage.Update();
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Expected Delivery Date is Updated for all the checked Items in Part History Gr table
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnUpdateDeliveryCommitDate_Click(object sender, EventArgs e)//to update the delivery commitment date
    {
        string sqlconnstring1 = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring1);
        SqlTransaction trans = null;
        try
        {
            conn.Open();
            trans = conn.BeginTransaction();
            DateTime deliverycommitdate = new DateTime();
            if(!string.IsNullOrEmpty(inputDeliveryCommitDate.Text))//if user has filled any date or not in the delivery commitment date
            {
                deliverycommitdate = Convert.ToDateTime(inputDeliveryCommitDate.Text);
            }
            if(deliverycommitdate.ToString().Equals("01/01/0001 12:00:00 AM"))//if the user has filled no date to update 
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('please fill updated delivery commitment date');", true);
                return;
            }
            int count = 0;
            foreach (GridViewRow row in gridViewSentChallans.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                    string toolno = gridViewSentChallans.Rows[row.RowIndex].Cells[1].Text;
                    string location = gridViewSentChallans.Rows[row.RowIndex].Cells[2].Text;
                    string station = gridViewSentChallans.Rows[row.RowIndex].Cells[4].Text;
                    string pno = gridViewSentChallans.Rows[row.RowIndex].Cells[5].Text;
                    string rwk = gridViewSentChallans.Rows[row.RowIndex].Cells[6].Text;
                    string partname = gridViewSentChallans.Rows[row.RowIndex].Cells[7].Text;
                    //code changed by himaneesh adding delcommitdate,requiredbydate
                    DateTime delcommittable = new DateTime();
                    DateTime requiredbydate = new DateTime();
                    //code changed by himaneesh checking if delivery commitment date is there or not in a row
                    if (gridViewSentChallans.Rows[row.RowIndex].Cells[11].Text!= "&nbsp;" && !string.IsNullOrEmpty(gridViewSentChallans.Rows[row.RowIndex].Cells[11].Text.Replace("&nbsp;","")))
                    {
                        delcommittable = Convert.ToDateTime(gridViewSentChallans.Rows[row.RowIndex].Cells[11].Text);//taking the delivery commitment date
                    }
                    //code changed by himaneesh checking if required by date is there or not for a row
                    if (gridViewSentChallans.Rows[row.RowIndex].Cells[10].Text != "&nbsp;" && !string.IsNullOrEmpty(gridViewSentChallans.Rows[row.RowIndex].Cells[10].Text.Replace("&nbsp;", "")))
                    {
                        requiredbydate = Convert.ToDateTime(gridViewSentChallans.Rows[row.RowIndex].Cells[10].Text);
                    }
                    
                    if (isSelected)
                    {
                        count++;
                        //code changed by himaneesh checking if required date present in the selected row is valid
                        if (requiredbydate < deliverycommitdate && !requiredbydate.ToString().Equals("01/01/0001 12:00:00 AM"))//checking if the required by date is valid that is not equal to 01/01/1900 which is the default date and should be greater than commitment date
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('delivery commitment date is greater than required by date at row no " + (row.RowIndex + 1) + "');", true);
                            return;
                        }
                        //code changed by himaneesh checking if delievery commit date is present in the selected row is valid and if valid then dont allow the user to update the delivery commitment date
                        if (delcommittable != null && !delcommittable.ToString().Equals("") && !delcommittable.ToString().Equals("01/01/1900 12:00:00 AM") && !delcommittable.ToString().Equals("01/01/0001 12:00:00 AM"))//if deliverycommitment date column cell entry field is not empty and is not equal to default delivery commitment date of 01/01/1900 that means he has already filled delivery commitment date and hene cannot update on this screen. when you devlare something as new date then it gets a default value of 01/01/0001
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('first delivery commitment date is already there at  row no " + (row.RowIndex + 1) + " , for further updating this date upto 1 week go to recieve screen and update from there');", true);
                            return;
                        }
                        HiddenField hdnPartHistoryId = (HiddenField)row.FindControl("hdnPartHistoryId");
                        string query = "update part_history_GR set Delivery_Commit_Date=@DeliveryCommitDate , Delivery_Commit_Updated_Date=@DeliveryCommitUpdatedDate where Id=@PartsHistoryId";//part history id is unique and is primary key in part_history_gr table
                        SqlCommand cmd = new SqlCommand(query, conn, trans);
                        cmd.Parameters.AddWithValue("@DeliveryCommitDate", inputDeliveryCommitDate.Text);
                        cmd.Parameters.AddWithValue("@DeliveryCommitUpdatedDate", DateTime.Now);
                        cmd.Parameters.AddWithValue("@PartsHistoryId", hdnPartHistoryId.Value);
                        cmd.ExecuteNonQuery();
                        //code changed by himaneesh. also updating delivery_commitment_date of location_info_gr table
                        string query2 = "update location_info_GR set Delivery_Commit_Date=@DeliveryCommitDate where p_no='" + pno + "' and station='" + station + "' and rwk='" + rwk + "' and tool_no='" + toolno + "' and location='" + location + "'";//updating location_info_gr
                        SqlCommand cmd2 = new SqlCommand(query2, conn, trans);
                        cmd2.Parameters.AddWithValue("@DeliveryCommitDate", inputDeliveryCommitDate.Text);
                        cmd2.ExecuteNonQuery();
                        //code changed  by himaneesh. also changing commitment history table
                        string mystring = "insert into CommitmentHistory(part_history_id,p_no,part_name,tool_no,station,location,commitment_date,created_date,created_by,rework_no) values(@part_history_id,@p_no,@part_name,@tool_no,@station,@location,@commitment_date,@created_date,@created_by,@Rwk);";//inserting into commitment_history also 
                        SqlCommand cmd3 = new SqlCommand(mystring, conn, trans);
                        cmd3.Parameters.AddWithValue("@part_history_id", hdnPartHistoryId.Value);
                        cmd3.Parameters.AddWithValue("@p_no", pno);
                        cmd3.Parameters.AddWithValue("@part_name", partname);
                        cmd3.Parameters.AddWithValue("@tool_no", toolno);
                        cmd3.Parameters.AddWithValue("@station", station);
                        cmd3.Parameters.AddWithValue("@location", location);
                        cmd3.Parameters.AddWithValue("@commitment_date", inputDeliveryCommitDate.Text);
                        cmd3.Parameters.AddWithValue("@created_by", Session["UserID"]);
                        cmd3.Parameters.AddWithValue("@created_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        cmd3.Parameters.AddWithValue("@Rwk", rwk);
                        cmd3.ExecuteNonQuery();
                    }
                }
            }
            trans.Commit();
            ShowGrid();
            if (count != 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Delivery commitment Date Updated Successfully.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please select the part for which you have to update the required by date');", true);
            }
        }
        catch (Exception ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }

    /// <summary>
    /// Date TextBox is validated. User can select the Past Dates
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtdelcdate_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDateTime(inputDeliveryCommitDate.Text) < DateTime.Now.AddDays(-1))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Can not take past dates');", true);
            inputDeliveryCommitDate.Text = "";
        }
    }

    /// <summary>
    /// Vendor List is shown from Vendor Master Job Work Table
    /// </summary>
    /// <param name="text"></param>
    /// <param name="vendorName"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetChallanNumber(string text, string vendorName)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            List<string> allItems = new List<string>();
            SqlCommand cmd = new SqlCommand();
            string searchCondition = "";
            if (!string.IsNullOrEmpty(vendorName))
            {
                searchCondition = " cd.Location='" + vendorName + "'";
            }
            if (!string.IsNullOrEmpty(text))
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and cd.Challan_No like '%" + text + "%'";
                }
                else
                {
                    searchCondition = " cd.Challan_No like '%" + text + "%'";
                }
            }
            if (!string.IsNullOrWhiteSpace(searchCondition))
            {
                cmd = new SqlCommand("SELECT distinct cd.Challan_No FROM Challan_Sent_Received_Detail cd left join Part_History_Gr phg on cd.Tool_No= phg.Tool_No and cd.P_No=phg.P_No and cd.Part_Name=phg.Part_Name and cd.Station=phg.Station and cd.Part_Name=phg.Part_Name and cd.Rwk_No=phg.Rwk_No and cd.Challan_No=phg.Challan_No and phg.Sent_Or_Rec='Sent' where cd.Sent_Or_Rec='Sent' and cd.Outstanding_Qty>0 and phg.Delivery_commit_date Is Null and phg.location not in ('VD Spare Store','Store Assembly','Spare Store','Design','Design Wait','Assembly','DIRECTASSEMBLY','GR VD FINISH STORE','PPC','Weld Assembly Waiting','Weld Parts Consumed','Weld Parts Waiting','VD Finish Store',' Inspection') and " + searchCondition + " order by Challan_No Asc", con);
            }
            else
            {
                cmd = new SqlCommand("SELECT distinct cd.Challan_No FROM Challan_Sent_Received_Detail cd left join Part_History_Gr phg on cd.Tool_No= phg.Tool_No and cd.P_No=phg.P_No and cd.Part_Name=phg.Part_Name and cd.Station=phg.Station and cd.Part_Name=phg.Part_Name and cd.Rwk_No=phg.Rwk_No and cd.Challan_No=phg.Challan_No and phg.Sent_Or_Rec='Sent' where cd.Sent_Or_Rec='Sent' and cd.Outstanding_Qty>0 and phg.Delivery_commit_date Is Null and phg.location not in ('VD Spare Store','Store Assembly','Spare Store','Design','Design Wait','Assembly','DIRECTASSEMBLY','GR VD FINISH STORE','PPC','Weld Assembly Waiting','Weld Parts Consumed','Weld Parts Waiting','VD Finish Store',' Inspection') order by Challan_No Asc", con);
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string items = Convert.ToString(reader["Challan_No"]);
                allItems.Add(items);
            }
            reader.Close();
            return allItems;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    /// <summary>
    /// Date TextBox is validated. User can select the Past Dates
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void inputDeliveryCommitDate_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(inputDeliveryCommitDate.Text))
        {
            if (Convert.ToDateTime(inputDeliveryCommitDate.Text) < DateTime.Now.AddDays(-1))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Can not take past dates');", true);
                inputDeliveryCommitDate.Text = "";
            }
        }
    }

    protected void inputrequiredbydate_TextChanged(object sender, EventArgs e)//code changed by himaneesh. this code will not allow to enter required date in past
    {
        if (!string.IsNullOrEmpty(inputrequiredbydate.Text))
        {
            if (Convert.ToDateTime(inputrequiredbydate.Text) < DateTime.Now.AddDays(-1))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Can not take past dates');", true);
                inputrequiredbydate.Text = "";
            }
        }
    }

    protected void btnupdate_req_date_Click(object sender, EventArgs e)//code changed by himaneesh. to update required date 
    {
        string sqlconnstring1 = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring1);
        SqlTransaction trans = null;
        try
        {
            conn.Open();
            trans = conn.BeginTransaction();
            DateTime requiredbydate = new DateTime();
            if (!string.IsNullOrEmpty(inputrequiredbydate.Text))//if user has filled any date or not in the required by date date
            {
                requiredbydate = Convert.ToDateTime(inputrequiredbydate.Text);
            }
            if (requiredbydate.ToString().Equals("01/01/0001 12:00:00 AM"))//if the user has filled no date to update 
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('please fill required by date');", true);
                return;
            }
            int count = 0;
            foreach (GridViewRow row in gridViewSentChallans.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                    string toolno = gridViewSentChallans.Rows[row.RowIndex].Cells[1].Text;
                    string location = gridViewSentChallans.Rows[row.RowIndex].Cells[2].Text;
                    string station = gridViewSentChallans.Rows[row.RowIndex].Cells[4].Text;
                    string pno = gridViewSentChallans.Rows[row.RowIndex].Cells[5].Text;
                    string rwk = gridViewSentChallans.Rows[row.RowIndex].Cells[6].Text;
                    string partname = gridViewSentChallans.Rows[row.RowIndex].Cells[7].Text;
                    if (isSelected)
                    {
                        count++;
                        DateTime commitment_delivery_date = new DateTime(1996, 02, 05);//default date for commitment delivery date
                        if (gridViewSentChallans.Rows[row.RowIndex].Cells[11] != null && !gridViewSentChallans.Rows[row.RowIndex].Cells[11].Text.Equals("&nbsp;"))//chekcing if delivey commit date should not be null and should contain valid values
                        {
                            commitment_delivery_date = Convert.ToDateTime(gridViewSentChallans.Rows[row.RowIndex].Cells[11].Text);
                        }
                        if (requiredbydate < commitment_delivery_date)//delivery commitment date should be greater than or equal to reqiured by date but never greater than required by datef
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('required by date is less than delivery commitment date at row no " + (row.RowIndex + 1) + "');", true);
                            return;
                        }
                        HiddenField hdnPartHistoryId = (HiddenField)row.FindControl("hdnPartHistoryId");
                        string query = "update part_history_GR set Required_Date=@RequiredDate where Id=@PartsHistoryId";//updating required by date
                        SqlCommand cmd = new SqlCommand(query, conn, trans);
                        cmd.Parameters.AddWithValue("@RequiredDate", inputrequiredbydate.Text);
                        cmd.Parameters.AddWithValue("@PartsHistoryId", hdnPartHistoryId.Value);
                        cmd.ExecuteNonQuery();
                        //updating location_info_gr table too
                        string query2 = "update location_info_gr set Required_Date=@RequiredDate where p_no='" + pno + "' and station='" + station + "' and rwk='" + rwk + "' and tool_no='" + toolno + "' and location='" + location + "'";//updating required by date
                        SqlCommand cmd2 = new SqlCommand(query2, conn, trans);
                        cmd2.Parameters.AddWithValue("@RequiredDate", inputrequiredbydate.Text);
                        cmd2.ExecuteNonQuery();
                    }
                }
            }
            trans.Commit();
            ShowGrid();
            if (count != 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Required Date Updated Successfully.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,GetType(),"alertmsg","alert('Please select the part for which you have to update the required by date');",true);
            }
        }
        catch (Exception ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }
}


