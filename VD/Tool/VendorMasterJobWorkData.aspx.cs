﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Configuration;
using ClosedXML.Excel;
using System.IO;
using CustomFunctions;

public partial class Tool_VendorMasterJobWorkData : System.Web.UI.Page
{
    public static string searchCondition;
    private static bool[] rightsArray;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                SetControlsRights();
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Show Hides the Controls on the basis of User Rights assigned in Vendor Control Rights Table
    /// </summary>
    protected void SetControlsRights()
    {
        try
        {
            rightsArray = AccessRights.SetUserAccessRights();
            if (!rightsArray[3])
            {
                Response.Redirect("../Pages/WelcomePage.aspx?Error=NoRights");
            }
            if (!rightsArray[0])
            {
                hyplnkAddVendor.Visible = false;
            }
            if (!rightsArray[4])
            {
                lnkBtnDownload.Visible = false;
            }
            if (!rightsArray[5])
            {
                hyplnkUploadVendor.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Retrieves the Data from Vendor Master Job Work table and binds the GridView
    /// </summary>
    protected void BindGrid()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            if (Convert.ToString(ViewState["SearchCondition"]) != "")
            {
                adapter = new SqlDataAdapter("select * from Vendor_Master_Job_Work where " + ViewState["SearchCondition"] + " order by Vendor_Name ASC", con);
            }
            else
            {
                adapter = new SqlDataAdapter("select * from Vendor_Master_Job_Work order by Vendor_Name ASC", con);
            }

            DataTable td = new DataTable();
            adapter.Fill(td);
            gridVendor.DataSource = td;
            gridVendor.DataBind();
            gridVendor.EmptyDataText = "No Record Found";
            lblRecorrdsCount.Text = Convert.ToString(td.Rows.Count) + " " + "Records In Total";
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }

    }

    protected void gridVendor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // Set the index of the new display page.  
        gridVendor.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //gridPackages.PageIndex = 1;
        searchCondition = "";
        if (inputVendorName.Text != "")
        {
            searchCondition = "Vendor_Name ='" + inputVendorName.Text + "'";
        }

        ViewState["SearchCondition"] = searchCondition;
        BindGrid();
    }

    /// <summary>
    /// Redirects the User to Vendor Master Form on Edit Click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridVendor_PageIndexChanging_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            Response.Redirect("Vendor_Master.aspx?ID=" + e.CommandArgument);
        }
    }

    /// <summary>
    /// Retrieves the List Of Distinct Vendors from Vendor Master Job Work Table
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetVendorName(string text)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            List<string> allItems = new List<string>();
            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrWhiteSpace(text))
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work where Vendor_Name like '" + text + "%' and Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ", con);
            }
            else
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work where Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ", con);
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string vendorName = Convert.ToString(reader["Vendor_Name"]);
                allItems.Add(vendorName);
            }
            reader.Close();
            return allItems;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    /// <summary>
    /// Downloads the Vendor Master Job Work Data on Download Click in Excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkBtnDownload_Click(object sender, EventArgs e)
    {
        string query = @"select Vendor_Name,Vendor_Type,Vendor_Address,Phone, Contact_Person, Email, Tin_Number,Allocated_To,Incharge_Phone_No, Standard_Days, "
                     + " Case when Is_Active=1 then 'Yes' else 'No' end as Is_Active, Case when Item_Pending_Report_Required=1 then 'Yes' else 'No' end as Item_Pending_Report_Required, "
                     + " Case when Name_Required_For_Vendor_Verification_List=1 then 'Yes' else 'No' end as Name_Required_For_Vendor_Verification_List, "
                     + " Case when Vendor_Visit_Report_Required=1 then 'Yes' else 'No' end as Vendor_Visit_Report_Required, "
                     + " IsNull(Frequency_Of_Verification,'') as Frequency_Of_Verification,GST_Number from Vendor_Master_Job_work order by Vendor_Name ASC";
        ExportToExcel(query);
    }

    protected void ExportToExcel(string query)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        using (SqlConnection con = new SqlConnection(sqlconnstring))
        {
            using (SqlCommand cmd = new SqlCommand(query))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (System.Data.DataTable dt = new System.Data.DataTable())
                    {
                        sda.Fill(dt);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt, "VendorMasterJobWork");

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
                            Response.AddHeader("content-disposition", "attachment;filename=VendorMasterJobWork_" + dateTime + ".xlsx");
                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                    }
                }
            }
        }
    }

    protected void gridVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkBtnEdit = (LinkButton)e.Row.FindControl("lnkBtnEdit");
            if (!rightsArray[1])
            {
                lnkBtnEdit.Visible = false;
            }
        }
    }
}

