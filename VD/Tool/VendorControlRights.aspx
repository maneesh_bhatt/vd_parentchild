﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="VendorControlRights.aspx.cs" Inherits="Tool_VendorControlRights" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style2
        {
            border: none;
            width: 100px;
        }
        
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        table
        {
            border: 1px solid #dadada;
            border-collapse: collapse;
        }
        table td
        {
            border: 1px solid #B8BABD;
            border-collapse: collapse;
            width: 200px;
        }
        table th
        {
            border: 1px solid #B8BABD;
            border-collapse: collapse;
            color: White;
            background-color: #335599;
            font-weight: normal;
            width: 160px;
            text-align: left;
            padding-left: 10px;
            line-height: 28px;
        }
        .SubmitButton
        {
            background: #335599 url(../images/bg.png) repeat-x 0 -110px;
            color: white;
            height: 32px;
            width: 90px;
            margin-top: 16px;
            text-align: center;
            line-height: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updatePage" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>
    <div style="width: 100%; border: 1px solid Blue; min-height: 100px;">
        <div style="padding: 9px;">
            <div>
                <div align="center">
                    <span class="style13">Vendor Control Rights</span>
                </div>
                <p>
                    <asp:Label runat="server" ID="lblMessage" ForeColor="Red"></asp:Label>
                </p>
            </div>
            <div style="margin-bottom: 10px; margin-top: 20px;">
                <div style="font-size: 13px; width: 130px;">
                    User Name
                    <asp:DropDownList runat="server" ValidationGroup="Document" Width="220px" ForeColor="#7d6754"
                        ID="ddlUser" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged">
                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <asp:HyperLink ID="hyplnkAddUser" Style="background-color: #335599; float: right;
                    color: White; padding: 5px; margin-top: -22px; margin-right: 10px;" runat="server"
                    NavigateUrl="~/Tool/VendorControlRightsData.aspx">View Rights</asp:HyperLink>
            </div>
            <table style="font-size: 13px; font-family: Verdana; line-height: 28px; width: 40%;">
                <tr>
                    <th>
                        Add Rights
                    </th>
                    <td align="center">
                        <asp:CheckBox runat="server" ID="checkAddRights" CssClass="checkboxes" ClientIDMode="Static" />
                    </td>
                </tr>
                <tr>
                    <th>
                        Edit Rights
                    </th>
                    <td align="center">
                        <asp:CheckBox runat="server" ID="checkEditRights" CssClass="checkboxes" ClientIDMode="Static" />
                    </td>
                </tr>
                <tr>
                    <th>
                        View Rights
                    </th>
                    <td align="center">
                        <asp:CheckBox runat="server" ID="checkViewRights" CssClass="checkboxes" ClientIDMode="Static" />
                    </td>
                </tr>
                <tr>
                    <th>
                        Delete Rights
                    </th>
                    <td align="center">
                        <asp:CheckBox runat="server" ID="checkDeleteRights" CssClass="checkboxes" ClientIDMode="Static" />
                    </td>
                </tr>
                <tr>
                    <th>
                        Download Rights
                    </th>
                    <td align="center">
                        <asp:CheckBox runat="server" ID="checkDownloadRights" CssClass="checkboxes" ClientIDMode="Static" />
                    </td>
                </tr>
                <tr>
                    <th>
                        Upload Rights
                    </th>
                    <td align="center">
                        <asp:CheckBox runat="server" ID="checkUploadRights" CssClass="checkboxes" ClientIDMode="Static" />
                    </td>
                </tr>
            </table>
            <div>
                <asp:ImageButton CssClass="SubmitButton" ID="btnSave" OnClick="btnSave_Click" runat="server" />
            </div>
        </div>
    </div>
    <%--   </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
