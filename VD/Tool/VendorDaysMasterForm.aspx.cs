﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class Tool_VendorDaysMasterForm : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null)
        {
            Response.Redirect("~/Pages/Login.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    inputVendorName.Enabled = false;
                    LoadData(Convert.ToString(Request.QueryString["ID"]));
                }
                // clear();
            }

        }

    }


    protected void LoadData(string id)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd = new SqlCommand("select * from Vendor_Days_Master where Vendor_Days_Master_Id=@Id", con);
            cmd.Parameters.AddWithValue("@Id", id);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                inputVendorName.Text = Convert.ToString(reader["Vendor_Name"]);
                inputStandardDays.Text = Convert.ToString(reader["Standard_Days"]);
            }
            reader.Close();

        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }

    }


    private void clear()
    {
        inputVendorName.Text = String.Empty;
        inputStandardDays.Text = String.Empty;
    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    protected void btnSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            using (SqlConnection conn = new SqlConnection(sqlconnstring))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    bool isExist = false;
                    if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
                    {
                        cmd.CommandText = "select * from Vendor_Days_Master where Vendor_Name=@VendorName and Vendor_Days_Master_Id!=@Id";
                        cmd.Parameters.AddWithValue("@VendorName", inputVendorName.Text);
                        cmd.Parameters.AddWithValue("@Id", Request.QueryString["Id"]);
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                isExist = true;
                            }
                        }
                        if (!isExist)
                        {
                            cmd.Parameters.Clear();
                            cmd.CommandText = "Update Vendor_Days_Master set Vendor_Name=@VendorName,Standard_Days=@StandardDays where Vendor_Days_Master_Id=@Id";
                            cmd.Parameters.AddWithValue("@Id", Request.QueryString["Id"]);
                        }
                    }
                    else
                    {
                        cmd.CommandText = "select * from Vendor_Days_Master where Vendor_Name=@VendorName";
                        cmd.Parameters.AddWithValue("@VendorName", inputVendorName.Text);
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                isExist = true;
                            }
                        }
                        if (!isExist)
                        {
                            cmd.Parameters.Clear();
                            cmd.CommandText = "INSERT INTO Vendor_Days_Master(Vendor_Name,Standard_Days) VALUES (@VendorName,@StandardDays);";
                        }
                    }
                    if (isExist)
                    {
                        MessageBox("Vendor already Exist, You cant add Standard Days for same Vendor Again");
                        return;
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@StandardDays", inputStandardDays.Text);
                        cmd.Parameters.AddWithValue("@VendorName", inputVendorName.Text);
                        cmd.ExecuteNonQuery();
                        if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
                        {
                            Response.Redirect("VendorDaysMasterGrid.aspx", false);
                        }
                        else
                        {
                            MessageBox("Vendor Days added Successfuly");
                        }
                    }
                }
            }
            clear();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
    }
}

