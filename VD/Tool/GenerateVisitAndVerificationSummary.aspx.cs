﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class Tool_GenerateVisitAndVerificationSummary : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetDateRange();
        }
    }

    /// <summary>
    /// Retrieves the Last 2 Week Date Range (From-To)
    /// </summary>
    protected void GetDateRange()
    {
        DateTime firstDate = DateTime.Today.AddDays(((int)(DateTime.Today.DayOfWeek) * -1) + 1);
        DateTime endDate = DateTime.Today.AddDays(((int)(DateTime.Today.DayOfWeek) * -1) + 7);

        DateTime nextFirstDate = endDate.AddDays(1);
        DateTime nextEndDate = nextFirstDate.AddDays(6);

        ddlDateRange.Items.Add(new ListItem(firstDate.ToString("dd-MM-yyyy") + "_" + endDate.ToString("dd-MM-yyyy")));
        ddlDateRange.Items.Add(new ListItem(nextFirstDate.ToString("dd-MM-yyyy") + "_" + nextEndDate.ToString("dd-MM-yyyy")));
    }

    /// <summary>
    /// Redirects the User to PrintVendorVisitAndVerificationSummary along with the values for
    /// Report Type (Visit/Verificaition Summary), Date Range and Vendor Type(All/Internal/External) Selected Value
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGenerateVisitReport_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlDateRange.SelectedValue != "" && ddlReportType.SelectedValue != "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "window.open('PrintVendorVisitAndVerificationSummary.aspx?Type=" + ddlReportType.SelectedValue + "&Range=" + ddlDateRange.SelectedValue + "&VendorType=" + ddlVendorType.SelectedValue + "');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Date Range and Report Type is required.');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Enables disables the Vendor Type Dropdown and sets its Values on the basis of Selected Report Type
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlReportType.SelectedValue == "VisitSummary")
        {
            ddlVendorType.Enabled = false;
            ddlVendorType.SelectedValue = "Ext";
        }
        else if (ddlReportType.SelectedValue == "VerificationSummary")
        {
            ddlVendorType.Enabled = true;
            ddlVendorType.SelectedValue = "All";
        }
    }
}
