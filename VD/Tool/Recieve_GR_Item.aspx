﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Recieve_GR_Item.aspx.cs"
    MasterPageFile="~/Pages/MasterPage.master" Inherits="Tool_Recieve_GR_Item" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false; 4

            return true;
        }
    </script>
    <style type="text/css">
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 860px;
            height: 440px;
            top: 20%;
            left: 30%;
            margin-left: -185px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
            line-height: 28px;
            overflow-y: scroll;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .awbimagecontainer
        {
            float: right;
            width: 29%;
            margin-right: 12px;
            margin-top: 43px;
            border: 1px solid #2D89EF;
            height: 180px;
        }
        .web_dialog_info
        {
            display: none;
            position: fixed;
            width: 940px;
            height: 510px;
            top: 22%;
            left: 36%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table style="width: 100%;" align="left">
                <tr>
                    <td colspan="2" align="center" style="font-size: x-large; color: black">
                        <span class="style1">Receive Rework / Fresh Items </span>
                    </td>
                </tr>
                <%--      <tr>
                
                    <td colspan="6" align="right">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <center>
                                    <img alt="" src="../ajax-loader.gif" style="width: 32px; height: 32px" />
                                </center>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>--%>
                <tr>
                    <td colspan="6" align="center" height="60px">
                        <asp:RadioButton ID="RadioButton1" Text="By Automatic" AutoPostBack="true" runat="server"
                            OnCheckedChanged="RadioButton1_CheckedChanged" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton
                                ID="RadioButton2" Text="By Manually" runat="server" AutoPostBack="true" OnCheckedChanged="RadioButton2_CheckedChanged" />
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <img alt="" src="../ajax-loader.gif" style="width: 32px; height: 32px; margin-left: 10px;
                                    margin-bottom: -34px; margin-top: -24px;" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <p runat="server" id="divTotal" visible="false" style="width: 100%; margin-top: -42px;
                            font-weight: bold; text-align: right; margin-left: -47px;">
                            <span style="color: green;">Total Operation Duration:</span>
                            <asp:Label runat="server" ID="lblTotalOperationDuration"></asp:Label></p>
                        <asp:Panel ID="Panel4" runat="server">
                            <div style="font-size: 13px; width: 140px; float: left;">
                                Location:&nbsp;<asp:DropDownList ID="ddllocation" CssClass="glowing-border" AppendDataBoundItems="true"
                                    BackColor="#F6F1DB" ForeColor="#7d6754" AutoPostBack="true" Width="130px" runat="server"
                                    OnSelectedIndexChanged="ddllocation_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div style="font-size: 13px; width: 120px; float: left;">
                                Tool No<br />
                                <asp:TextBox ID="txtprojectno" CssClass="glowing-border" runat="server" Width="110px"></asp:TextBox>
                            </div>
                            <div style="font-size: 13px; width: 120px; float: left;">
                                Station No<br />
                                <asp:TextBox ID="tbStation" CssClass="glowing-border" runat="server" Width="110px"></asp:TextBox>
                            </div>
                            <div style="font-size: 13px; width: 120px; float: left;">
                                Rework No
                                <br />
                                <asp:TextBox ID="txtreworkno" CssClass="glowing-border" runat="server" Width="110px"></asp:TextBox>
                            </div>
                            <div style="font-size: 13px; width: 100px; float: left;">
                                &nbsp; Receive Date<span class="style12">&nbsp;</span>
                                <asp:TextBox ID="txtReceivecdate" CssClass="glowing-border" runat="server" AutoPostBack="true"
                                    Width="90px" OnTextChanged="txtReceivecdate_TextChanged"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                    Enabled="true" OnClientShowing="CurrentDateShowing" TargetControlID="txtReceivecdate">
                                </cc1:CalendarExtender>
                                &nbsp;
                            </div>
                            <div style="font-size: 13px; width: 120px; float: left;">
                                <asp:Button ID="btnSearch" runat="server" CssClass="glowing-border" Text="Search"
                                    Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px; color: white;
                                    height: 35px; width: 90px; margin-top: 5px" OnClick="btnSearch_Click" />
                            </div>
                            <div style="font-size: 13px; width: 173px; float: left; font-weight: bold; margin-top: 10px;">
                                <span style="margin-left: 10px;">Is Virtual Transaction </span>
                                <asp:CheckBox runat="server" Style="margin-top: 10px;" ID="checkIsVirtualTransaction" />
                            </div>
                            <div style="width: 100%; float: left;">
                                <div runat="server" id="divCommitDate" style="float: left; margin-top: 7px;">
                                    Vendor Next Expected Delivery Date:
                                    <asp:TextBox ID="txtdelcdate" CssClass="glowing-border" runat="server" Width="120px"
                                        OnTextChanged="txtdelcdate_TextChanged"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" OnClientShowing="CurrentDateShowing"
                                        runat="server" Format="dd-MM-yyyy" Enabled="true" TargetControlID="txtdelcdate">
                                    </cc1:CalendarExtender>
                                    &nbsp; &nbsp;<asp:Button ID="btnUpdateCommitmentDate" runat="server" CssClass="glowing-border"
                                        Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px; color: white;
                                        height: 32px; width: 100px; margin-top: 5px" Text="Update Date" OnClick="btnUpdateCommitmentDate_Click" />
                                    &nbsp; &nbsp;
                                </div>
                                <div style="float: left; margin-top: 7px;">
                                    <asp:Button ID="btnGenerateVisitReport" runat="server" Visible="false" EnableViewState="true"
                                        CausesValidation="false" Text="Generate Vendor Visit Report" Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px;
                                        color: white; height: 30px; width: 194px;" OnClick="btnGenerateVisitReport_Click" />
                                    <asp:Label ID="lblChallanno" runat="server" Text="Challan No:"></asp:Label>
                                    <asp:TextBox ID="txtchallanno" CssClass="glowing-border" Visible="False" runat="server"
                                        Width="80px" Enabled="False"></asp:TextBox>
                                    <asp:Button ID="btnprint" Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px;
                                        color: white; height: 32px; width: 100px; margin-top: 5px; background-color: Green;"
                                        runat="server" EnableViewState="true" CausesValidation="false" Text="PrintChallan"
                                        OnClick="btnprint_Click" />
                                    <asp:Button ID="btnSendThisChallan" runat="server" EnableViewState="true" CausesValidation="false"
                                        Text="Send this Challan" Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px;
                                        color: white; height: 32px; width: 140px; margin-top: 5px; background-color: Green;"
                                        OnClick="btnSendThisChallan_Click" />
                                </div>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="2">
                        <asp:Panel ID="Panel2" runat="server">
                            <table>
                                <tr>
                                    <td colspan="2">
                                        Location :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                        <asp:DropDownList ID="ddllocauto" CssClass="glowing-border" AppendDataBoundItems="true"
                                            Width="200px" runat="server" OnSelectedIndexChanged="ddllocauto_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Received Qty :&nbsp;&nbsp;
                                        <asp:TextBox ID="txtsentQty1" CssClass="glowing-border" runat="server" Width="200px"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="15px">
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Barcode No :&nbsp;&nbsp;&nbsp; &nbsp;
                                        <asp:TextBox ID="txtbarcode" CssClass="glowing-border" runat="server" AutoPostBack="true"
                                            onkeydown="myFunctionL(event)" Width="200px" OnTextChanged="txtbarcode_TextChanged"></asp:TextBox>
                                    </td>
                                    <td colspan="2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Item Name :&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                        <asp:TextBox ID="txtname1" CssClass="glowing-border" runat="server" Width="200px"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="15px">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Tool No :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                        <asp:TextBox ID="txttoolno" CssClass="glowing-border" runat="server" Width="200px"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                    <td colspan="2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Station No :&nbsp; &nbsp; &nbsp;&nbsp;
                                        <asp:TextBox ID="txtstationno" CssClass="glowing-border" runat="server" Width="200px"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="15px">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Position No :&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                        <asp:TextBox ID="txtposition" CssClass="glowing-border" runat="server" Width="200px"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                    <td colspan="2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New
                                        Receive Qty :&nbsp;
                                        <asp:TextBox ID="txtQty" CssClass="glowing-border" runat="server" Width="200px" OnTextChanged="txtQty_TextChanged"
                                            ReadOnly="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="15px">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Total Sent Qty :&nbsp;&nbsp;<asp:TextBox ID="txttotalsent" CssClass="glowing-border"
                                            runat="server" Enabled="False" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td height="30px">
                                        <asp:Label ID="lbl" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <td>
                            <asp:Panel ID="Panel1" runat="server" Width="100%" ScrollBars="None">
                                <asp:GridView ID="GridView1" runat="server" EmptyDataText="No Record Found" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                    Style="border: 1px solid #aba4a4;" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating"
                                    OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="False" CellPadding="4"
                                    ForeColor="#333333" GridLines="None" OnRowCommand="GridView1_RowCommand" Font-Size="13px"
                                    Width="100%" OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAll1" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll1_CheckedChanged1" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkAllchild" AutoPostBack="true" runat="server" OnCheckedChanged="chkAllchild_CheckedChanged">
                                                </asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Tool
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="Server" Text='<%# Eval("tool_no") %>'></asp:Label>
                                                <asp:HiddenField ID="hdnPartsGrId" runat="server" Value='<%#Eval("Parts_Gr_Id") %>' />
                                                <asp:HiddenField ID="hdnLocation" runat="server" Value='<%#Eval("Location") %>' />
                                                <asp:HiddenField ID="hdnSentReceivedDate" runat="server" Value='<%#Eval("Sent_Received_Date") %>' />
                                                <asp:HiddenField ID="hdnReceivedDate" runat="server" Value='<%#Eval("Received_Date") %>' />
                                                <asp:HiddenField ID="hdnDeliveryCommitDate" runat="server" Value='<%#Eval("Delivery_Commit_Date") %>' />
                                                <asp:HiddenField ID="hdnPendingRowsAtVendor" runat="server" Value='<%#Eval("TotalPendingQtyAtVendor") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="60px" ItemStyle-Width="60px"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Station
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="Server" Text='<%# Eval("station") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="40px" ItemStyle-Width="40px"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                P No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Label3" CausesValidation="false" runat="server" Text='<%# Eval("p_no") %>'
                                                    CommandArgument='<%# Eval("p_no") + ";" + Eval("tool_no") + ";" + Eval("station") + ";" + Eval("part_name") +";"+Eval("Rwk") %>'
                                                    OnClick="Label3_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="60px" HeaderStyle-Width="60px" ItemStyle-Wrap="true">
                                            <HeaderTemplate>
                                                Part Name
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="Server" Text='<%# Eval("part_name") %>' Style="word-wrap: break-word;"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-Width="60px" ItemStyle-Width="60px">
                                            <HeaderTemplate>
                                                Total Qty Sent
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="Server" Text='<%# Eval("quant_sent") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-Width="60px" ItemStyle-Width="60px">
                                            <HeaderTemplate>
                                                Total Rec. Qty
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="Server" Text='<%# Eval("quant_rec") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Outstanding Challans" HeaderStyle-Width="100px" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:DropDownList runat="server" ID="ddlChallanNumbers" CssClass="glowing-border"
                                                    Enabled="false" Width="100px" AppendDataBoundItems="true" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlChallanNumbers_SelectedIndexChanged">
                                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-Width="60px" ItemStyle-Width="60px">
                                            <HeaderTemplate>
                                                New Rec. Qty
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox ID="TextBox2" runat="server" Width="50px" CssClass="glowing-border"
                                                    onkeypress="return isNumberKey(event)" Text="" AutoPostBack="true" Enabled="false"
                                                    OnTextChanged="TextBox2_TextChanged"></asp:TextBox>
                                                <cc1:NumericUpDownExtender ID="NumericUpDownExtender3" runat="server" TargetControlID="TextBox2"
                                                    Width="120" RefValues="" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                    TargetButtonUpID="" Minimum="0" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-Width="60px" ItemStyle-Width="60px">
                                            <HeaderTemplate>
                                                Extra Qty Rec.
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblExtraReceivedQty" Text='<%#Eval("Extra_Received_Quantity") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-Width="60px" ItemStyle-Width="60px">
                                            <HeaderTemplate>
                                                Extra Rec. Qty
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtExtraQuantity" runat="server" CssClass="glowing-border" onkeypress="return isNumberKey(event)"
                                                    AutoPostBack="true" Enabled="false" Width="50px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="60px" ItemStyle-Width="60px"
                                            ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Upl Qty
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label6Upload_Qty" runat="Server" Text='<%# Eval("Upload_Qty") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="60px" HeaderStyle-Width="60px">
                                            <HeaderTemplate>
                                                Sent Qty
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label6sentqty" runat="Server" Text='<%# Eval("SentQty") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="60px" HeaderStyle-Width="60px">
                                            <HeaderTemplate>
                                                Available Qty For Receive
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAvailableQtyForReceive" runat="Server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="50px" HeaderStyle-Width="50px">
                                            <HeaderTemplate>
                                                Rework No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblrew" runat="Server" Text='<%# Eval("Rwk") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="60px" HeaderStyle-Width="60px">
                                            <HeaderTemplate>
                                                Last Expected Delivery Date
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkBtnLastCommitmentDate" CausesValidation="false" runat="server"
                                                    CommandArgument='<%# Eval("p_no") + ";" + Eval("tool_no") + ";" + Eval("station") + ";" + Eval("part_name") +";"+Eval("Rwk") %>'
                                                    OnClick="lnkBtnLastCommitmentDate_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="60px" HeaderStyle-Width="60px" ItemStyle-Wrap="true">
                                            <HeaderTemplate>
                                                Download
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:HyperLink runat="server" ID="hyperLnkExcelFile" Target="_blank"></asp:HyperLink>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Wrap="true">
                                            <HeaderTemplate>
                                                Status
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <b>
                                                    <%# Eval("Status") %></b>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="60px" HeaderStyle-Width="60px" ItemStyle-Wrap="true">
                                            <HeaderTemplate>
                                                Child Positions
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnChildPositions" runat="Server"></asp:HiddenField>
                                                <asp:PlaceHolder ID="placeHolderChildPositions" runat="Server"></asp:PlaceHolder>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Setting" HeaderStyle-Width="60px" HeaderStyle-Font-Bold="true"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ID="lnkBtnAddNewRow" Text="Add Row" OnClick="ButtonAdd_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="margin-top: 50px;" align="left" colspan="4">
                        <asp:Button ID="btnRecieved" runat="server" Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px;
                            color: white; height: 28px; width: 110px; margin-top: 5px" CausesValidation="true"
                            EnableViewState="true" Text="Receive" Visible="false" Width="100px" OnClick="btnRecieved_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="overlay" runat="server" clientidmode="Static" class="web_dialog_overlay">
                        </div>
                        <div id="dialog" clientidmode="Static" runat="server" class="web_dialog">
                            <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                                <div style="float: left; width: 100%;">
                                    <div class="web_dialog_title" style="width: 20%; float: left;">
                                        Item History
                                    </div>
                                    <div class="web_dialog_title align_right">
                                        <asp:LinkButton runat="server" ID="btnClosePopUp" ClientIDMode="Static" OnClick="btnClosePopUp_Click">Close</asp:LinkButton>
                                    </div>
                                </div>
                                <div runat="server" id="pnlHistoryPopUp">
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="overlayCommitmentHistory" runat="server" clientidmode="Static" class="web_dialog_overlay">
                        </div>
                        <div id="dialogCommitmentHistory" clientidmode="Static" runat="server" class="web_dialog">
                            <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                                <div style="float: left; width: 100%;">
                                    <div class="web_dialog_title" style="width: 40%; float: left;">
                                        Expected Delivery Date History
                                    </div>
                                    <div class="web_dialog_title align_right">
                                        <asp:LinkButton runat="server" ID="btnCloseCommitmentHistory" ClientIDMode="Static"
                                            OnClick="btnCloseCommitmentHistory_Click">Close</asp:LinkButton>
                                    </div>
                                </div>
                                <div runat="server" id="pnlCommitmentHistory">
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
