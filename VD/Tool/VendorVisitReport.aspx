﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="VendorVisitReport.aspx.cs" Inherits="Tool_VendorVisitReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style2
        {
            border: none;
            width: 100px;
        }
        
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <%--    <asp:UpdatePanel ID="updatePage" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>
    <div style="width: 100%; border: 1px solid Blue; min-height: 100px;">
        <div style="padding: 9px;">
            <div>
                <div align="center">
                    <span class="style13">Challan/Vendor Wise Report</span>
                </div>
            </div>
            <div style="margin-bottom: 10px; margin-top: 20px;">
                <div style="font-size: 13px; width: 95px; float: left;">
                    Start Date
                    <br />
                    <asp:TextBox ID="inputFromDate" CssClass="glowing-border" runat="server" Width="85px"
                        OnTextChanged="inputFromDate_TextChanged" AutoPostBack="true"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                        Enabled="true" TargetControlID="inputFromDate" OnClientShowing="CurrentDateShowing">
                    </cc1:CalendarExtender>
                </div>
                <div style="font-size: 13px; width: 95px; float: left;">
                    End Date
                    <br />
                    <asp:TextBox ID="inputToDate" CssClass="glowing-border" runat="server" Width="85px"
                        OnTextChanged="inputToDate_TextChanged" AutoPostBack="true"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="true" Format="dd-MMM-yyyy"
                        TargetControlID="inputToDate" OnClientShowing="CurrentDateShowing">
                    </cc1:CalendarExtender>
                </div>
                <div style="font-size: 13px; width: 130px; float: left;">
                    Vendor Type
                    <asp:RadioButtonList ID="radioVendorType" AppendDataBoundItems="true" ForeColor="#7d6754"
                        RepeatDirection="Horizontal" AutoPostBack="true" Width="130px" runat="server"
                        OnSelectedIndexChanged="radioVendorType_SelectedIndexChanged">
                        <asp:ListItem Text="Internal" Value="Internal Vendor"></asp:ListItem>
                        <asp:ListItem Text="External" Value="External Vendor"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div style="font-size: 13px; width: 180px; float: left;">
                    Location<br />
                    <asp:DropDownList ID="ddlLocation" CssClass="glowing-border" AppendDataBoundItems="true"
                        BackColor="#F6F1DB" ForeColor="#7d6754" AutoPostBack="true" Width="160px" runat="server"
                        OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged">
                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="font-size: 13px; width: 180px; float: left;">
                    Challan Number
                    <asp:DropDownList ID="ddlChallanNo" CssClass="glowing-border" AppendDataBoundItems="true"
                        BackColor="#F6F1DB" ForeColor="#7d6754" AutoPostBack="true" Width="160px" runat="server">
                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/images/searchbtn.jpg"
                    OnClick="btnSearch_Click" Style="margin-top: 10px;" />
                <asp:Button ID="btnGenerateVisitReport" runat="server" Visible="true" EnableViewState="true"
                    CausesValidation="false" Text="Generate Vendor Visit Report" Style="background: #335599 url(../images/bg.png) repeat-x 0 -110px;
                    color: white; height: 34px; width: 200px; float: right; margin-top: 7px;" OnClick="btnGenerateVisitReport_Click" />
            </div>
            <asp:GridView runat="server" ID="gridViewVendorPendingItems" AutoGenerateColumns="false"
                Width="100%" ShowHeaderWhenEmpty="true" OnRowDataBound="gridViewVendorPendingItems_RowDataBound"
                AllowPaging="false" AllowSorting="false" Style="font-size: 13px; font-family: Verdana;
                line-height: 26px;">
                <RowStyle BackColor="White" ForeColor="Black" />
                <Columns>
                    <asp:BoundField DataField="Tool_No" HeaderText="Tool" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="160px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Station" HeaderText="Station" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="70px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="P_No" HeaderText="PNo" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Rwk_No" HeaderText="Rwk" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="60px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Part_Name" HeaderText="Part Name" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="110px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Upload_Qty" HeaderText="Upload Qty" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="80px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Quantity" HeaderText="Qty Sent" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="80px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Challan_No" HeaderText="Challan No" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="80px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="User_Name" HeaderText="User Name" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="80px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Delivery_Commit_Date" DataFormatString="{0:dd-MM-yyyy}"
                        HeaderText="Expected Delivery Date" HeaderStyle-BackColor="#335599" HeaderStyle-Width="160px"
                        HeaderStyle-ForeColor="White" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
