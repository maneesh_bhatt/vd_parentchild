﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Configuration;
using System.Reflection;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using FileReader;
using System.Text;
using System.Threading.Tasks;

public partial class upload_Excel_part : System.Web.UI.Page
{
    int count1 = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        txtuploaddate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
        if (!IsPostBack)
        {
            string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name<>'Assembly' order by Vendor_Name asc ";
            string dataTextField = "VENDOR_NAME";
            string dataValueField = "ID";
            UtilityFunctions.bind_vendor(DDLvendor, query, dataTextField, dataValueField);
            Label1.Visible = false;
            DDLvendor.Visible = false;
        }
    }

    /// <summary>
    /// Validates if user has selected Valid Date. Displays error message for Past and Future Dates
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtuploaddate_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDateTime(txtuploaddate.Text) > DateTime.Now || Convert.ToDateTime(txtuploaddate.Text) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Can not take future/past date');", true);
            txtuploaddate.Text = "";
        }
    }

    /// <summary>
    /// Validates File Format is Valid or not. Based on the Selected Challan Type Calls the Respective (New/Old Tool Upload Method)
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UploadButton_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (txtuploaddate.Text.Length > 0)
            {
                if (!FileUploadControl.HasFile)
                {
                    lbl.Text = "You have not selected any file !!!";
                    return;
                }
                string datetime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
                string fileName = Path.GetFileNameWithoutExtension(FileUploadControl.PostedFile.FileName) + "_" + datetime + "" + Path.GetExtension(FileUploadControl.PostedFile.FileName);
                string fileExtension = Path.GetExtension(FileUploadControl.PostedFile.FileName);
                string fileLocation = Server.MapPath("~/UploadedExcelFiles/" + fileName);
                FileUploadControl.SaveAs(fileLocation);

                if (!(fileExtension == ".xls") && !(fileExtension == ".xlsx"))
                {
                    lbl.Text = "Invalid file !!! Select only an excel file";
                    return;
                }
                if (radioNewTool.Checked)
                {
                    GetDataTableFromCsv_ForNewTools(fileLocation);
                }
                else if (radioOldTool.Checked)
                {
                    GetDataTableFromCsv_ForOldTools(fileLocation);
                }
            }
            else
            {
                MessageBox("Please Select Upload Date");
                return;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
    }

    /// <summary>
    /// Validates Position is Parent and returns the No of Child Parts If its Parent Position otherwise it returns null
    /// </summary>
    /// <param name="toolNo"></param>
    /// <param name="pNo"></param>
    /// <param name="station"></param>
    /// <param name="rwkNo"></param>
    /// <param name="activeReworkNumber"></param>
    /// <param name="childPositions"></param>
    /// <returns></returns>
    private int? ValidatePositionIsParent(string toolNo, string pNo, string station, string rwkNo, string activeReworkNumber, out List<string> childPositions)
    {
        int? childRows = null;
        childPositions = new List<string>();
        DataTable dt = new DataTable();
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            connec.Open();
            SqlCommand cmd = new SqlCommand();
            string parentName = "P_" + pNo;
            SqlDataAdapter ad = new SqlDataAdapter("select * from parts_Gr where ((opr17 like '%" + parentName + ",%' or opr17 like '%," + parentName + "' or opr17 like '%," + parentName + ",%' or opr17 like '" + parentName + "') or (Parent_Positions like '%" + parentName + ",%' or Parent_Positions like '%," + parentName + "' or Parent_Positions like '%," + parentName + ",%' or Parent_Positions like '" + parentName + "')) and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + activeReworkNumber + "' and Is_Active_Record=1", connec);
            ad.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                childPositions.Add(Convert.ToString(dt.Rows[i]["p_no"]));
            }
            childRows = dt.Rows.Count;
            return childRows;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return childRows;
        }
        finally
        {
            connec.Close();
        }
    }

    /// <summary>
    /// Validates if Position is Child Position. Returns true if Child Position else returns false.
    /// In case of Exception it returns null
    /// </summary>
    /// <param name="toolNo"></param>
    /// <param name="pNo"></param>
    /// <param name="lastActiveGrNo"></param>
    /// <param name="station"></param>
    /// <param name="rwkNo"></param>
    /// <param name="parentList"></param>
    /// <returns></returns>
    private bool? ValidatePositionIsChild(string toolNo, string pNo, string lastActiveGrNo, string station, string rwkNo, out List<string> parentList)
    {
        parentList = new List<string>();
        bool? isChildPosition = null;
        DataTable dt = new DataTable();
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            connec.Open();
            SqlCommand cmd = new SqlCommand();
            //string parentName = "P_";
            SqlDataAdapter ad = new SqlDataAdapter("select * from parts_Gr where ((opr17 is not null and opr17!='') or (Parent_Positions is not null and Parent_Positions!='')) and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + lastActiveGrNo + "' and p_no='" + pNo + "' and Is_Active_Record=1", connec);
            ad.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                SqlDataAdapter adCheck = new SqlDataAdapter();
                DataTable dtExist = new DataTable();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string parentPositionsOld = Convert.ToString(dt.Rows[i]["opr17"]).ToLower();
                    string parentPositionsForNew = Convert.ToString(dt.Rows[i]["Parent_Positions"]).ToLower();
                    if (parentPositionsOld.StartsWith("p_"))
                    {
                        string[] opr17 = parentPositionsOld.Split(',');
                        if (opr17.Length > 0)
                        {
                            for (int j = 0; j < opr17.Length; j++)
                            {
                                adCheck = new SqlDataAdapter("select p_no from parts_gr where (p_no='" + Convert.ToString(opr17[j]).Replace("p_", "") + "') and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + lastActiveGrNo + "' and Is_Active_Record=1", connec);
                                adCheck.Fill(dtExist);
                                if (dtExist.Rows.Count > 0)
                                {
                                    isChildPosition = true;
                                    parentList.Add(Convert.ToString(dtExist.Rows[j]["p_no"]));
                                }
                            }
                        }
                    }

                    if (parentPositionsForNew.StartsWith("p_"))
                    {
                        if (isChildPosition == null)
                        {
                            string[] parentPosition = parentPositionsForNew.Split(',');
                            if (parentPosition.Length > 0)
                            {
                                for (int j = 0; j < parentPosition.Length; j++)
                                {
                                    adCheck = new SqlDataAdapter("select * from parts_gr where (p_no='" + Convert.ToString(parentPosition[j]).Replace("p_", "") + "') and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + lastActiveGrNo + "' and Is_Active_Record=1", connec);
                                    adCheck.Fill(dtExist);
                                    if (dtExist.Rows.Count > 0)
                                    {
                                        isChildPosition = true;
                                        parentList.Add(Convert.ToString(dtExist.Rows[j]["p_no"]));
                                    }
                                }
                            }
                        }
                    }
                    if (dtExist.Rows.Count == 0)
                        isChildPosition = false;
                }
            }
            else
            {
                isChildPosition = false;
            }
            return isChildPosition;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return isChildPosition;
        }
        finally
        {
            connec.Close();
        }
    }

    /// <summary>
    /// Returns Total Not Required and Remaining Active Quantity By Tool, Station, Position No and Version.
    /// Return true if Error Occured.
    /// </summary>
    /// <param name="excelToolNo"></param>
    /// <param name="excelStation"></param>
    /// <param name="excelPNo"></param>
    /// <param name="activeVersionNo"></param>
    /// <param name="connec"></param>
    /// <param name="totalNotReq"></param>
    /// <param name="remainingActiveQty"></param>
    /// <returns></returns>
    protected bool GetLastActiveNotRequiredAndActiveQtyByVersion(int excelToolNo, string excelStation, string excelPNo, string activeVersionNo, SqlConnection connec, out int totalNotReq, out int remainingActiveQty)
    {
        bool isError = false;
        totalNotReq = 0;
        remainingActiveQty = 0;
        try
        {
            int totalQty = 0;
            string query = "select * from parts_gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and Version_Number=@VersionNo and Type not in ('NotRequired','RatioUpdate')";
            using (SqlDataAdapter adActiveQty = new SqlDataAdapter(query, connec))
            {
                adActiveQty.SelectCommand.Parameters.AddWithValue("@ToolNo", excelToolNo);
                adActiveQty.SelectCommand.Parameters.AddWithValue("@Station", excelStation);
                adActiveQty.SelectCommand.Parameters.AddWithValue("@PNo", excelPNo);
                adActiveQty.SelectCommand.Parameters.AddWithValue("@VersionNo", activeVersionNo);
                DataTable dtActiveQty = new DataTable();
                adActiveQty.Fill(dtActiveQty);

                for (int m = 0; m < dtActiveQty.Rows.Count; m++)
                {
                    string reworkNo = Convert.ToString(dtActiveQty.Rows[m]["Rwk_No"]);
                    string type = Convert.ToString(dtActiveQty.Rows[m]["Type"]);
                    string refGrNo = Convert.ToString(dtActiveQty.Rows[m]["Gr_Reference_No"]);
                    totalQty = Convert.ToInt32(dtActiveQty.Rows[m]["Req_Qty"]) + totalQty;

                    using (SqlDataAdapter adNotRequired = new SqlDataAdapter("select Sum(Req_Qty) as NotReqQty from parts_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and Gr_Reference_No=@GrReferenceNo and Type=@Type", connec))
                    {
                        adNotRequired.SelectCommand.Parameters.AddWithValue("@ToolNo", excelToolNo);
                        adNotRequired.SelectCommand.Parameters.AddWithValue("@Station", excelStation);
                        adNotRequired.SelectCommand.Parameters.AddWithValue("@PNo", excelPNo);
                        adNotRequired.SelectCommand.Parameters.AddWithValue("@GrReferenceNo", Convert.ToString(dtActiveQty.Rows[m]["Rwk_No"]));
                        adNotRequired.SelectCommand.Parameters.AddWithValue("@Type", "NotRequired");
                        DataTable dtNotReq = new DataTable();
                        adNotRequired.Fill(dtNotReq);
                        if (dtNotReq.Rows.Count > 0)
                        {
                            if (dtNotReq.Rows[0]["NotReqQty"] != DBNull.Value)
                                totalNotReq = Convert.ToInt32(dtNotReq.Rows[0]["NotReqQty"]) + totalNotReq;

                        }
                    }
                }
                remainingActiveQty = totalQty - totalNotReq;
            }
            return isError;
        }
        catch (Exception ex)
        {
            isError = true;
            ex.ToString();
            return isError;
        }
    }

    
    /// <summary>
    /// Calculates the remaining active quantity in case of Not Required, by calculating the total Uploaded Quantity - total Uploaded Not Required Quantity
    /// </summary>
    /// <param name="excelToolNo"></param>
    /// <param name="excelStation"></param>
    /// <param name="excelPNo"></param>
    /// <param name="excelReferenceGrNo"></param>
    /// <param name="connec"></param>
    /// <param name="trans"></param>
    /// <param name="remainingActiveQty"></param>
    /// <returns></returns>
    protected bool GetLastActiveQtyByReworkNoForNotRequiredUpload(int excelToolNo, string excelStation, string excelPNo, string excelReferenceGrNo, SqlConnection connec, SqlTransaction trans, out int remainingActiveQty)
    {
        bool isError = false;
        remainingActiveQty = 0;
        try
        {
            int totalQty = 0;
            int totalNotReq = 0;
            string query = "select Sum(Req_Qty) as UploadedQty from parts_gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and Rwk_No=@RwkNo";
            using (SqlDataAdapter adActiveQty = new SqlDataAdapter(query, connec))
            {
                adActiveQty.SelectCommand.Transaction = trans;
                adActiveQty.SelectCommand.Parameters.AddWithValue("@ToolNo", excelToolNo);
                adActiveQty.SelectCommand.Parameters.AddWithValue("@Station", excelStation);
                adActiveQty.SelectCommand.Parameters.AddWithValue("@PNo", excelPNo);
                adActiveQty.SelectCommand.Parameters.AddWithValue("@RwkNo", excelReferenceGrNo);
                DataTable dtActiveQty = new DataTable();
                adActiveQty.Fill(dtActiveQty);
                if (dtActiveQty.Rows.Count > 0)
                {
                    if (dtActiveQty.Rows[0]["UploadedQty"] != DBNull.Value)
                        totalQty = Convert.ToInt32(dtActiveQty.Rows[0]["UploadedQty"]);

                }

                using (SqlDataAdapter adNotRequired = new SqlDataAdapter("select Sum(Req_Qty) as NotReqQty from parts_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and Gr_Reference_No=@GrReferenceNo and Type=@Type", connec))
                {
                    adNotRequired.SelectCommand.Transaction = trans;
                    adNotRequired.SelectCommand.Parameters.AddWithValue("@ToolNo", excelToolNo);
                    adNotRequired.SelectCommand.Parameters.AddWithValue("@Station", excelStation);
                    adNotRequired.SelectCommand.Parameters.AddWithValue("@PNo", excelPNo);
                    adNotRequired.SelectCommand.Parameters.AddWithValue("@GrReferenceNo", excelReferenceGrNo);
                    adNotRequired.SelectCommand.Parameters.AddWithValue("@Type", "NotRequired");
                    DataTable dtNotReq = new DataTable();
                    adNotRequired.Fill(dtNotReq);
                    if (dtNotReq.Rows.Count > 0)
                    {
                        if (dtNotReq.Rows[0]["NotReqQty"] != DBNull.Value)
                            totalNotReq = Convert.ToInt32(dtNotReq.Rows[0]["NotReqQty"]);

                    }
                }
                remainingActiveQty = totalQty - totalNotReq;
            }
            return isError;
        }
        catch (Exception ex)
        {
            isError = true;
            ex.ToString();
            return isError;
        }
    }

    /// <summary>
    /// Retrives and validates the uploaded parts and if validation is successful then those parts are saved in database
    /// </summary>
    /// <param name="path"></param>
    public void GetDataTableFromCsv_ForNewTools(string path)
    {
        DataTable failitem = new DataTable();
        object request_id = "";
        PartsGr partsGr = new PartsGr();
        DataTable unsuccessfulyrow = new DataTable();
        List<string> stringType = new List<string>();
        stringType.Add("Modification");
        stringType.Add("New");
        stringType.Add("R&D");
        stringType.Add("NotRequired");
        stringType.Add("RatioUpdate");
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring + "; Persist Security Info=true;");
        SqlTransaction trans = null;
        string itemsExist = "";
        try
        {
            lblErrorMessage.Text = String.Empty;
            lblchallanno.Text = String.Empty;
            conn.Open();
            trans = conn.BeginTransaction();
            string pathOnly = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);

            DataTable dt = new DataTable();

            string uploadType = "";
            string challanNumber = "";

            DataSet ds = null;
            DataTable dtprojectItems = new DataTable();
            using (clsFileReadHelper csv = new clsFileReadHelper(path))
            {
                ds = csv.getFileData();
            }
            if (ds.Tables.Count == 1)
            {
                dtprojectItems = ds.Tables[0].Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
            }

            if (dtprojectItems.Rows.Count > 0)
            {
                if (dtprojectItems.Columns.Count != 38)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Number of Columns In Uploaded Excel File, Does Not Match Actual Format.');", true);
                    return;
                }
                StringBuilder sb = new StringBuilder();
                //Validate Logic
                for (int i = 1; i < dtprojectItems.Rows.Count; i++)
                {
                    List<string> listActiveReworks = new List<string>();
                    string activeReworkNumber = "";
                    string activeVersionNo = "";
                    int currentRow = i;
                    bool rExisitInDb = false;
                    List<string> rwkList = new List<string>();
                    int excelToolNo = 0;
                    int excelReqQty = 0;
                    if (dtprojectItems.Rows[i][3] != null)
                        excelReqQty = Convert.ToInt32(dtprojectItems.Rows[i][3]);
                    string excelRwkNo = Convert.ToString(dtprojectItems.Rows[i][0]);
                    string excelStation = Convert.ToString(dtprojectItems.Rows[i][1]);
                    string excelPNo = Convert.ToString(dtprojectItems.Rows[i][2]);
                    string excelPartName = Convert.ToString(dtprojectItems.Rows[i][4]);
                    string excelType = Convert.ToString(dtprojectItems.Rows[i][35]);
                    string excelGrReferenceNumber = Convert.ToString(dtprojectItems.Rows[i][36]);
                    string excelVersion = Convert.ToString(dtprojectItems.Rows[i][37]);
                    int lastActiveQuantity = 0;
                    bool isGRUploadedAgainstRND = false;
                    bool isRNDUploaded = false;
                    string activePositionRefGRNo_ForGRAgainstRND = "";
                    if (dtprojectItems.Rows[i][11] != null)
                    {
                        excelToolNo = Convert.ToInt32(dtprojectItems.Rows[i][11]);
                    }
                    if (!stringType.Contains(excelType))
                    {
                        if (excelRwkNo.ToLower().StartsWith("gr"))
                        {
                            sb.AppendFormat("Invalid Type Entered at Row Number (" + currentRow + "), Material Type can only be (Modification,New,R&D,NotRequired,RatioUpdate).");
                            sb.Append("<br/>");
                        }
                    }
                    else if (stringType.Contains(excelType))
                    {
                        if (excelRwkNo.ToLower().StartsWith("r"))
                        {
                            sb.AppendFormat("Invalid Type Entered at Row Number (" + currentRow + "), Type can not be defined in Case of R.");
                            sb.Append("<br/>");
                        }
                    }

                    if (excelPNo != "" && excelPartName != "" && excelStation != "" && excelRwkNo != "" && ((excelType != "" && excelRwkNo.ToLower().StartsWith("gr") && excelVersion != "") || ((excelType == "" || excelType != "") && excelRwkNo.ToLower().StartsWith("r"))) && excelToolNo > 0)
                    {
                        using (SqlConnection connec = new SqlConnection(conn.ConnectionString))
                        {
                            string query = "";
                            connec.Open();
                            query = "select * from parts_gr where tool_no=@ToolNo and station=@Station and p_no=@PNo order by Parts_Gr_Id ASC";
                            using (SqlDataAdapter ad = new SqlDataAdapter(query, connec))
                            {
                                ad.SelectCommand.Parameters.AddWithValue("@ToolNo", excelToolNo);
                                ad.SelectCommand.Parameters.AddWithValue("@Station", excelStation);
                                ad.SelectCommand.Parameters.AddWithValue("@PNo", excelPNo);
                                DataTable dtPartsGr = new DataTable();
                                ad.Fill(dtPartsGr);
                                bool existActivePosition = false;
                                bool isUploadedForSameCriteria = false;
                                if (dtPartsGr.Rows.Count > 0)
                                {
                                    if (string.IsNullOrEmpty(excelVersion))
                                    {
                                        sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), Data exist for same Tool, Station and PNo so Please specify Version Number.");
                                        sb.Append("<br/>");
                                    }
                                    isUploadedForSameCriteria = true;
                                    for (int k = 0; k < dtPartsGr.Rows.Count; k++)
                                    {
                                        string status = Convert.ToString(dtPartsGr.Rows[k]["Status"]);
                                        string rwk = Convert.ToString(dtPartsGr.Rows[k]["Rwk_No"]);
                                        if (rwk.ToLower().StartsWith("r"))
                                        {
                                            rExisitInDb = true;
                                            if (rwk.Equals(excelRwkNo))
                                            {
                                                sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), Data with same Rework Number already Exist in Database.");
                                                sb.Append("<br/>");
                                            }
                                        }
                                        else if (rwk.ToLower().StartsWith("gr") && rwk.Equals(excelRwkNo))
                                        {
                                            sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), Data with same Rework Number already Exist in Database.");
                                            sb.Append("<br/>");
                                        }
                                        if (status == "Active")
                                        {
                                            existActivePosition = true;
                                            activeReworkNumber = Convert.ToString(dtPartsGr.Rows[k]["Rwk_No"]);
                                            activeVersionNo = Convert.ToString(dtPartsGr.Rows[k]["Version_Number"]);
                                            listActiveReworks.Add(activeReworkNumber);

                                            if (Convert.ToString(dtPartsGr.Rows[k]["Rwk_No"]).ToLower().StartsWith("gr"))
                                            {
                                                if (dtPartsGr.Rows[k]["Gr_Reference_No"] != DBNull.Value)
                                                {
                                                    activePositionRefGRNo_ForGRAgainstRND = Convert.ToString(dtPartsGr.Rows[k]["Gr_Reference_No"]); //means last Acitve GR is uploaded against R&D
                                                    listActiveReworks.Add(activePositionRefGRNo_ForGRAgainstRND);
                                                }
                                            }
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(activeReworkNumber) && excelRwkNo.ToLower().StartsWith("gr") && (excelType == "New" || excelType == "Modification" || excelType == "R&D" || excelType == "RatioUpdate"))
                                    {
                                        existActivePosition = true;
                                        int remainingActiveQty = 0;
                                        int totalNotReq = 0;

                                        bool isError = GetLastActiveNotRequiredAndActiveQtyByVersion(excelToolNo, excelStation, excelPNo, activeVersionNo, connec, out totalNotReq, out remainingActiveQty);
                                        if (isError)
                                        {
                                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Error occured while processing your request.');", true);
                                            return;
                                        }
                                        lastActiveQuantity = remainingActiveQty;
                                    }

                                    if (excelType == "New" || excelType == "Modification")
                                    {
                                        int validatingQty = 0;
                                        //If Ref GR Number is defined in Excel (Means Position is GR against R&D or last active position is a GR against R&D

                                        if (excelGrReferenceNumber != "")
                                        {
                                            int? result = GetLastActiveQuantityForGRAssociatedWithRND(excelToolNo, excelStation, excelPNo, excelGrReferenceNumber, out isGRUploadedAgainstRND, out isRNDUploaded);
                                            if (result != null)
                                            {
                                                if (!isGRUploadedAgainstRND)
                                                {
                                                    if (excelGrReferenceNumber != "")
                                                    {
                                                        validatingQty = result.Value + excelReqQty;
                                                    }
                                                }
                                                else
                                                {
                                                    sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), GR already uploaded against R&D. You can not upload the GR again for same R&D.");
                                                    sb.Append("<br/>");
                                                }
                                            }
                                            if (!isRNDUploaded)
                                            {
                                                sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), Invalid Reference GR Number specified, R & D is not uploaded for mentioned Tool, PNo, Station.");
                                                sb.Append("<br/>");
                                            }
                                        }

                                        if (validatingQty == 0)
                                        {
                                            validatingQty = excelReqQty;
                                        }

                                        if (lastActiveQuantity != validatingQty && !isGRUploadedAgainstRND)
                                        {
                                            if (activePositionRefGRNo_ForGRAgainstRND != "")
                                            {
                                                sb.AppendFormat("Invalid Req_Qty at Row Number (" + currentRow + "), Quanity should match last active uploaded qty for same Tool, PNo, Station.");
                                                sb.Append("<br/>");
                                            }
                                            else if (excelGrReferenceNumber != "")
                                            {
                                                if (isGRUploadedAgainstRND || isRNDUploaded)
                                                {
                                                    if (!isGRUploadedAgainstRND && isRNDUploaded)
                                                    {
                                                        sb.AppendFormat("Invalid Req_Qty at Row Number (" + currentRow + "), Sum of (Upload Quanity + Upload Quantity in R&D for mentioned Ref GR Number) should match last active uploaded qty for this Tool, PNo, Station.");
                                                        sb.Append("<br/>");
                                                    }
                                                    else if (isGRUploadedAgainstRND && isRNDUploaded)
                                                    {
                                                        sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), GR already uploaded against R & D for mentioned Ref GR Number for this Tool, PNo, Station.");
                                                        sb.Append("<br/>");
                                                    }
                                                    else if (!isRNDUploaded)
                                                    {
                                                        sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), Invalid Reference GR Number specified, R & D is not uploaded for mentioned Tool, PNo, Station.");
                                                        sb.Append("<br/>");
                                                    }
                                                }
                                                else
                                                {
                                                    sb.AppendFormat("Invalid Req_Qty at Row Number (" + currentRow + "), Quanity should match last uploaded qty for same Tool, PNo, Station.");
                                                    sb.Append("<br/>");
                                                }
                                            }
                                            else
                                            {
                                                sb.AppendFormat("Invalid Req_Qty at Row Number (" + currentRow + "), Quanity should match last uploaded qty for same Tool, PNo, Station.");
                                                sb.Append("<br/>");
                                            }
                                        }
                                    }
                                    else if (excelType == "R&D")
                                    {
                                        if (excelGrReferenceNumber != "")
                                        {
                                            sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), You can not specify Ref GR number in case of R&D.");
                                            sb.Append("<br/>");
                                        }
                                        if (excelReqQty > lastActiveQuantity || lastActiveQuantity == 0)
                                        {
                                            sb.AppendFormat("Invalid Req_Qty at Row Number (" + currentRow + "), Quanity should be equal to or smaller than last uploaded active qty for same Tool, PNo, Station.");
                                            sb.Append("<br/>");
                                        }
                                    }
                                    else if (excelType == "NotRequired")
                                    {
                                        if (excelGrReferenceNumber == "")
                                        {
                                            sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), You should specify Ref GR number in case of Not Required.");
                                            sb.Append("<br/>");
                                        }
                                        else if (!listActiveReworks.Contains(excelGrReferenceNumber))
                                        {
                                            sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), Invalid Refrence GR Number is specified. You can upload NotRequired quantities for active Items only.");
                                            sb.Append("<br/>");
                                        }

                                        int rndReminigQty = 0;
                                        bool isGRAgainstRND = false;
                                        bool isErroOccured = ValidateParentIsGRAgainstRNDANDRNDUploadedASNotRequired(excelToolNo, excelStation, excelPNo, excelGrReferenceNumber, conn, trans, out rndReminigQty, out isGRAgainstRND);
                                        if (isErroOccured)
                                        {
                                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Error occured while processing your request.');", true);
                                            return;
                                        }
                                        if (isGRAgainstRND)
                                        {
                                            if (rndReminigQty != 0)
                                            {
                                                sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), You should upload Not Required Quantity for R&D before uploading Not Required for GR Against R&D.");
                                                sb.Append("<br/>");
                                            }
                                        }
                                        int remainingActiveQty = 0;
                                        bool isError = GetLastActiveQtyByReworkNoForNotRequiredUpload(excelToolNo, excelStation, excelPNo, excelGrReferenceNumber, conn, trans, out remainingActiveQty);
                                        if (isError)
                                        {
                                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Error occured while processing your request.');", true);
                                            return;
                                        }
                                        lastActiveQuantity = remainingActiveQty;

                                        //If parent is Not Required then user should specify child as not required
                                        List<string> childPositions = new List<string>();
                                        int? resultRows = ValidatePositionIsParent(Convert.ToString(excelToolNo), excelPNo, excelStation, excelRwkNo, activeReworkNumber, out childPositions);
                                        if (resultRows == null)
                                        {
                                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured while Validating Positions is Parent or Not.');", true);
                                            return;
                                        }
                                        if (resultRows > 0)//Means Position is Parent Position
                                        {
                                            for (int r = 0; r < childPositions.Count; r++)
                                            {
                                                //validating child position exist on excel or not
                                                DataRow[] drChildExist = dtprojectItems.Select("Column11=" + excelToolNo + " and Column1='" + excelStation + "' and Column2='" + childPositions[r] + "' and Column0='" + excelRwkNo + "' and Column35='NotRequired'");
                                                if (drChildExist.Length == 0)
                                                {
                                                    sb.AppendFormat("Child Position (" + childPositions[r] + ") does not Exist for Parent Position (" + excelPNo + "), Child Position with Type NotRequired is needed.");
                                                    sb.Append("<br/>");
                                                }
                                            }
                                        }


                                        //If child is uploaded as not required, then user should add row as ratio update for parent
                                        List<string> parentPositions = new List<string>();
                                        bool? isPositionChild = ValidatePositionIsChild(Convert.ToString(excelToolNo), excelPNo, activeReworkNumber, excelStation, excelRwkNo, out parentPositions);
                                        if (isPositionChild.Value)
                                        {
                                            for (int r = 0; r < parentPositions.Count; r++)
                                            {
                                                //validating parent position exist on excel or not
                                                DataRow[] drParentExist = dtprojectItems.Select("Column11=" + excelToolNo + " and Column1='" + excelStation + "' and Column2='" + parentPositions[r] + "'  and Column35='RatioUpdate'");//and Column0='" + excelRwkNo + "'
                                                if (drParentExist.Length == 0)
                                                {
                                                    sb.AppendFormat("Parent does not exist for position mentioned at Row Number (" + currentRow + "), Parent Position with Type RatioUpdate is required.");
                                                    sb.Append("<br/>");
                                                }
                                                else
                                                {
                                                    if (Convert.ToString(drParentExist[0]["Column0"]) == excelRwkNo)
                                                    {
                                                        sb.AppendFormat("Invalid Rework Number specified in excel for Parent Position (" + parentPositions[r] + "), For Type Ratio Update.");
                                                        sb.Append("<br/>");
                                                    }
                                                }
                                            }
                                        }
                                        if (excelReqQty > lastActiveQuantity || lastActiveQuantity == 0)
                                        {
                                            if (listActiveReworks.Contains(excelGrReferenceNumber))
                                            {
                                                if (lastActiveQuantity == 0)
                                                {
                                                    sb.AppendFormat("Invalid Req_Qty at Row Number (" + currentRow + "), Initially Uploaded Qty - Sum of Uploaded Not Required Qty is Zero. Now You can not add Not Required qty for same Tool, PNo, Station and Rework No.");
                                                    sb.Append("<br/>");
                                                }
                                                else
                                                {
                                                    sb.AppendFormat("Invalid Req_Qty at Row Number (" + currentRow + "), Initially Uploaded Qty - Sum of Uploaded Not Required Qty is Zero. Please Upload Valid Not Required qty for same Tool, PNo, Station and Rework No.");
                                                    sb.Append("<br/>");
                                                }
                                            }
                                        }
                                    }
                                    else if (excelType == "RatioUpdate")
                                    {
                                        if (excelGrReferenceNumber == "")
                                        {
                                            sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), You should specify Ref GR number in case of Ratio Update.");
                                            sb.Append("<br/>");
                                        }
                                        else if (!listActiveReworks.Contains(excelGrReferenceNumber))
                                        {
                                            sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), Invalid Refrence GR Number is specified. You can upload Ratio Upload quantities for active Items only.");
                                            sb.Append("<br/>");
                                        }
                                        if (excelReqQty < lastActiveQuantity || lastActiveQuantity == 0)
                                        {
                                            sb.AppendFormat("Invalid Req_Qty at Row Number (" + currentRow + "), Quanity should be equal to last uploaded active qty for same Tool, PNo, Station for RatioUpdate.");
                                            sb.Append("<br/>");
                                        }
                                    }
                                    if (excelRwkNo.ToLower().StartsWith("r") && excelGrReferenceNumber != "")
                                    {
                                        sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), You can not specify GR Reference Number if you are uploading R.");
                                        sb.Append("<br/>");
                                    }
                                    if (!rExisitInDb && excelType.ToLower() != "notrequired")
                                    {
                                        sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), You can not add GR before adding R. Please add R First.");
                                        sb.Append("<br/>");
                                    }
                                    if (!existActivePosition && excelRwkNo.ToLower().StartsWith("gr"))
                                    {
                                        sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), You can not add new GR position if active position does not exist for previous version.");
                                        sb.Append("<br/>");
                                    }
                                    if (rExisitInDb && !existActivePosition)
                                    {
                                        sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), You can not add R position if active position does not exist for previous version.");
                                        sb.Append("<br/>");
                                    }
                                    if (activeVersionNo != "")
                                    {
                                        //Validating Versions
                                        string lastActiveVersion = "";
                                        string lastUploadedVersion = "";
                                        string lastUploadedType = "";
                                        bool isErrorValidation1 = GetLastUploadedVersionNumber(excelToolNo, excelStation, excelPNo, out lastUploadedVersion, out lastUploadedType, conn, trans); // last uploaded version
                                        if (isErrorValidation1)
                                        {
                                            lblErrorMessage.Text = "Error occured while processing your request.";
                                            return;
                                        }

                                        bool isErrorValidation2 = GetLActiveVersionNumber(excelToolNo, excelStation, excelPNo, out lastActiveVersion, conn, trans); // last active version
                                        if (isErrorValidation2)
                                        {
                                            lblErrorMessage.Text = "Error occured while processing your request.";
                                            return;
                                        }

                                        if ((excelRwkNo.ToLower().StartsWith("gr") && excelGrReferenceNumber != "" && (excelType == "New" || excelType == "Modification")))
                                        {
                                            if (lastUploadedVersion == "")
                                            {
                                                string error = "Invalid Data at Row Number (" + currentRow + "), Unable to find Valid Version for GR Against R&D.";
                                                sb.AppendFormat(error);
                                                sb.Append("<br/>");
                                            }
                                        }
                                        else if (excelRwkNo.ToLower().StartsWith("r") || (excelType == "NotRequired" || excelType == "RatioUpdate"))
                                        {
                                            //Check Version Exist in Case of R/RatioUpdate/NotRequired/GR Against R&D
                                            if (lastActiveVersion != "" && excelVersion != "")
                                            {
                                                if (excelVersion != lastActiveVersion)
                                                {
                                                    string error = "Invalid Data at Row Number (" + currentRow + "), You can upload R/NotRequired/RatioUpdate for active Version Only (i.e - " + lastActiveVersion + ").";
                                                    sb.AppendFormat(error);
                                                    sb.Append("<br/>");
                                                }

                                            }
                                        }
                                        else if (excelRwkNo.ToLower().StartsWith("gr"))
                                        {
                                            if (lastUploadedVersion != "")
                                            {
                                                string[] splitVersion = lastUploadedVersion.Split('V');
                                                int versionNo = Convert.ToInt32(splitVersion[1]) + 1;
                                                string currentVersion = "V" + versionNo;
                                                if (excelVersion != currentVersion)
                                                {
                                                    string error = "Invalid Data at Row Number (" + currentRow + "), Specify the New Version for GR  (i.e - " + currentVersion + ").";
                                                    sb.AppendFormat(error);
                                                    sb.Append("<br/>");
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (excelRwkNo.ToLower().StartsWith("gr"))
                                    {
                                        if (!existActivePosition && isUploadedForSameCriteria)
                                        {
                                            sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), You can not add new GR position if active position does not exist for previous version.");
                                            sb.Append("<br/>");
                                        }
                                        else
                                        {
                                            sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), You can not add GR before adding R. Please add R First.");
                                            sb.Append("<br/>");
                                        }
                                    }
                                    else if (excelRwkNo.ToLower().StartsWith("r"))
                                    {
                                        if (excelGrReferenceNumber != "")
                                        {
                                            sb.AppendFormat("Invalid Data at Row Number (" + currentRow + "), You can not specify GR Reference Number if you are uploading R.");
                                            sb.Append("<br/>");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        sb.Append("Invalid Data at Row Number (" + currentRow + "), Tool No/ Station/ P No/ Part Name/ Station/ Rework No are Compulsory Fields (Type & Version in case of GR) can not be blank.");
                        sb.Append("<br/>");
                    }
                }
                string errorMessage = sb.ToString();
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    lblErrorMessage.Text = errorMessage;
                    return;
                }
            }

            for (int i = 1; i < dtprojectItems.Rows.Count; i++)
            {
                if (string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][0])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][1])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][2])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][3]))
                   && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][4])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][5])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][6])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][7]))
                    && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][8])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][9])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][10])))
                {
                    continue;
                }
                partsGr.RwkNo = Convert.ToString(dtprojectItems.Rows[i][0]);
                partsGr.Station = Convert.ToString(dtprojectItems.Rows[i][1]);
                partsGr.PNo = Convert.ToString(dtprojectItems.Rows[i][2]);
                if (dtprojectItems.Rows[i][3] != null)
                    partsGr.ReqQty = Convert.ToInt32(dtprojectItems.Rows[i][3]);
                partsGr.PartName = Convert.ToString(dtprojectItems.Rows[i][4]);
                partsGr.Matl = Convert.ToString(dtprojectItems.Rows[i][5]);
                partsGr.ChildPositions = Convert.ToString(dtprojectItems.Rows[i][6]);
                partsGr.Rms = Convert.ToString(dtprojectItems.Rows[i][7]);
                partsGr.RmQty = Convert.ToString(dtprojectItems.Rows[i][8]);
                partsGr.Hrc = Convert.ToString(dtprojectItems.Rows[i][9]);
                partsGr.Remark = Convert.ToString(dtprojectItems.Rows[i][10]);
                if (dtprojectItems.Rows[i][11] != null)
                {
                    partsGr.ToolNo = Convert.ToInt32(dtprojectItems.Rows[i][11]);
                }
                partsGr.Opr1 = Convert.ToString(dtprojectItems.Rows[i][12]);
                partsGr.Opr2 = Convert.ToString(dtprojectItems.Rows[i][13]);
                partsGr.Opr3 = Convert.ToString(dtprojectItems.Rows[i][14]);
                partsGr.Opr4 = Convert.ToString(dtprojectItems.Rows[i][15]);
                partsGr.Opr5 = Convert.ToString(dtprojectItems.Rows[i][16]);
                partsGr.Opr6 = Convert.ToString(dtprojectItems.Rows[i][17]);
                partsGr.Opr7 = Convert.ToString(dtprojectItems.Rows[i][18]);
                partsGr.Opr8 = Convert.ToString(dtprojectItems.Rows[i][19]);
                partsGr.Opr9 = Convert.ToString(dtprojectItems.Rows[i][20]);
                partsGr.Opr10 = Convert.ToString(dtprojectItems.Rows[i][21]);
                partsGr.Opr11 = Convert.ToString(dtprojectItems.Rows[i][22]);
                partsGr.Opr12 = Convert.ToString(dtprojectItems.Rows[i][23]);
                partsGr.Opr13 = Convert.ToString(dtprojectItems.Rows[i][24]);
                partsGr.Opr14 = Convert.ToString(dtprojectItems.Rows[i][25]);
                partsGr.Opr15 = Convert.ToString(dtprojectItems.Rows[i][26]);
                partsGr.Opr16 = Convert.ToString(dtprojectItems.Rows[i][27]);
                partsGr.ParentPositions = Convert.ToString(dtprojectItems.Rows[i][28]);
                partsGr.WallThick = Convert.ToString(dtprojectItems.Rows[i][29]);
                partsGr.Angle1 = Convert.ToString(dtprojectItems.Rows[i][30]);
                partsGr.Angle2 = Convert.ToString(dtprojectItems.Rows[i][31]);

                if (dtprojectItems.Rows[i][32] != null)
                {
                    partsGr.TargetDate = Convert.ToDateTime(dtprojectItems.Rows[i][32]);
                }
                partsGr.Size = Convert.ToString(dtprojectItems.Rows[i][33]);
                partsGr.Opr17 = Convert.ToString(dtprojectItems.Rows[i][34]);
                partsGr.Type = Convert.ToString(dtprojectItems.Rows[i][35]);
                string excelVersion = Convert.ToString(dtprojectItems.Rows[i][37]);
                string lastActiveVersion = "";
                string lastUploadedVersion = "";
                string lastUploadedType = "";
                bool isError = GetLastUploadedVersionNumber(partsGr.ToolNo, partsGr.Station, partsGr.PNo, out lastUploadedVersion, out lastUploadedType, conn, trans); // last uploaded version
                if (isError)
                {
                    lblErrorMessage.Text = "Error occured while processing your request.";
                    return;
                }

                bool isErrorOccured = GetLActiveVersionNumber(partsGr.ToolNo, partsGr.Station, partsGr.PNo, out lastActiveVersion, conn, trans); // last active version
                if (isErrorOccured)
                {
                    lblErrorMessage.Text = "Error occured while processing your request.";
                    return;
                }

                //if (partsGr.Type.ToLower() == "ratioupdate") //partsGr.Type.ToLower() == "notrequired" ||
                //{
                //    partsGr.GrReferenceNumber = lastUploadedVersion;  //activeGRNumber; (It can be inactive version)
                //}
                //else
                //{
                partsGr.GrReferenceNumber = Convert.ToString(dtprojectItems.Rows[i][36]);
                //}

                if ((partsGr.RwkNo.ToLower().StartsWith("gr") && partsGr.GrReferenceNumber != "" && (partsGr.Type == "New" || partsGr.Type == "Modification")))
                {
                    if (lastUploadedVersion != "" && lastUploadedType == "R&D")
                    {
                        partsGr.Version = lastUploadedVersion;
                    }
                    else
                    {
                        string errorMessage = "Unable to find Valid Version for GR Against R&D. You can not add GR Against R&D, If  another Active Version Uploaded after R&D.";
                        lblErrorMessage.Text = errorMessage;
                        return;
                    }
                }
                else if (partsGr.RwkNo.ToLower().StartsWith("r") || (partsGr.Type == "NotRequired" || partsGr.Type == "RatioUpdate"))
                {
                    //Check Version Exist in Case of R
                    if (lastActiveVersion != "" && excelVersion != "")
                    {
                        if (excelVersion != lastActiveVersion)
                        {
                            string errorMessage = "You can upload R for active Version Only (i.e - " + lastActiveVersion + "). Please Upload Gr if you are entering new Version No.";
                            lblErrorMessage.Text = errorMessage;
                            return;
                        }
                        else
                        {
                            partsGr.Version = lastActiveVersion;
                        }
                    }
                    else
                    {
                        partsGr.Version = "V0";
                    }
                }
                else if (partsGr.RwkNo.ToLower().StartsWith("gr"))
                {
                    if (lastUploadedVersion != "")
                    {
                        string[] splitVersion = lastUploadedVersion.Split('V');
                        int versionNo = Convert.ToInt32(splitVersion[1]) + 1;
                        string currentVersion = "V" + versionNo;
                        if (excelVersion != currentVersion)
                        {
                            string errorMessage = "Specify the New Version for GR  (i.e - " + currentVersion + "). Invalid Version entered.";
                            lblErrorMessage.Text = errorMessage;
                            return;
                        }
                        else
                        {
                            partsGr.Version = currentVersion;
                        }
                    }
                }

                if (partsGr.Version == "")
                {
                    string errorMessage = "Unable to find active version for Uploaded Parts. Please contact Administrator.";
                    lblErrorMessage.Text = errorMessage;
                    return;
                }

                partsGr.PartName = partsGr.PartName.Replace("  ", "");
                partsGr.PartName = partsGr.PartName.Replace(" ", "");

                partsGr.Station = partsGr.Station.Replace("  ", "");
                partsGr.Station = partsGr.Station.Replace(" ", "");

                if (i == 1)
                {
                    if (partsGr.RwkNo.ToLower().StartsWith("r") || partsGr.RwkNo == "")
                    {
                        string prefixStr = "CH";
                        challanNumber = UtilityFunctions.gencode(prefixStr);
                        uploadType = "Fresh";
                    }
                    else if (partsGr.RwkNo.ToLower().StartsWith("gr"))
                    {
                        string prefixStr = "CH";
                        challanNumber = UtilityFunctions.gencode(prefixStr);
                        uploadType = "Rework";
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Invalid Rework Type Entered at Line Number " + i + " ');", true);
                        trans.Rollback();
                        break;
                    }
                }

                if (partsGr.ToolNo != 0 && partsGr.PNo != "" && partsGr.Station != "")
                {
                    //Checking if Data at the First Row is Fresh then all row should contain Fresh Data 
                    if (partsGr.RwkNo.ToLower().StartsWith("r") || partsGr.RwkNo == "")
                    {
                        if (uploadType != "Fresh")
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Please Upload Similar Type of Data. Check Row Number " + i + " ');", true);
                            trans.Rollback();
                            break;
                        }
                    }
                    //Checking if Data at the First Row is Rework then all row should contain Rework Data 
                    else if (partsGr.RwkNo.ToLower().StartsWith("gr"))
                    {
                        if (uploadType != "Rework")
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Please Upload Similar Type of Data. Check Row Number " + i + " ');", true);
                            trans.Rollback();
                            break;
                        }
                    }
                    //Insert Data into Parts Gr
                    InsertData_ForNewTools(partsGr, i, uploadType, challanNumber, itemsExist, conn, trans);
                }
            }

            if (count1 > 0)
                trans.Commit();
            lblchallanno.Text = challanNumber;
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                }
            }
        }
        catch (Exception ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }

        if (count1 > 0)
        {
            //movefile(path);
            MessageBox("File Uploaded Successfully");
            conn.Close();
        }
        itemsExist = itemsExist.TrimEnd(',');
        if (!string.IsNullOrEmpty(itemsExist))
        {
            lblErrorMessage.Text = "Items at Row Number " + itemsExist + " alreay exist in the Database.";
        }
    }

    /// <summary>
    /// Validates if position is GR Against R&D and returns true if its GR against R&D. Returns the remaining quantiy by calulating the total uploaded quantity- total uploaded Not Required Quantity
    /// </summary>
    /// <param name="excelToolNo"></param>
    /// <param name="excelStation"></param>
    /// <param name="excelPNo"></param>
    /// <param name="excelGrReferenceNumber"></param>
    /// <param name="conn"></param>
    /// <param name="trans"></param>
    /// <param name="rndReminigQty"></param>
    /// <param name="GRgainstRND"></param>
    /// <returns></returns>
    private bool ValidateParentIsGRAgainstRNDANDRNDUploadedASNotRequired(int excelToolNo, string excelStation, string excelPNo, string excelGrReferenceNumber, SqlConnection conn, SqlTransaction trans, out int rndReminigQty, out bool GRgainstRND)
    {
        bool isError = false;
        rndReminigQty = 0;
        GRgainstRND = false;
        try
        {
            int totalQty = 0;
            int totalNotReqQty = 0;
            string rndGRNumber = "";
            string query = "select * from parts_gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and Rwk_No=@RwkNo";
            using (SqlDataAdapter adValidateISRND = new SqlDataAdapter(query, conn))
            {
                adValidateISRND.SelectCommand.Transaction = trans;
                adValidateISRND.SelectCommand.Parameters.AddWithValue("@ToolNo", excelToolNo);
                adValidateISRND.SelectCommand.Parameters.AddWithValue("@Station", excelStation);
                adValidateISRND.SelectCommand.Parameters.AddWithValue("@PNo", excelPNo);
                adValidateISRND.SelectCommand.Parameters.AddWithValue("@RwkNo", excelGrReferenceNumber);
                DataTable dtValidateIsRND = new DataTable();
                adValidateISRND.Fill(dtValidateIsRND);
                if (dtValidateIsRND.Rows.Count > 0)
                {
                    string type = Convert.ToString(dtValidateIsRND.Rows[0]["Type"]);
                    string refGRNumber = Convert.ToString(dtValidateIsRND.Rows[0]["Gr_Reference_No"]);

                    if ((type == "New" || type == "Modification") && refGRNumber != "")
                    {
                        rndGRNumber = refGRNumber;
                        GRgainstRND = true;
                    }

                }
            }

            if (!string.IsNullOrEmpty(rndGRNumber))
            {
                string queryRND = "select Sum(Req_Qty) as UploadedQty from parts_gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and Rwk_No=@RwkNo";
                using (SqlDataAdapter adActiveQty = new SqlDataAdapter(queryRND, conn))
                {
                    adActiveQty.SelectCommand.Transaction = trans;
                    adActiveQty.SelectCommand.Parameters.AddWithValue("@ToolNo", excelToolNo);
                    adActiveQty.SelectCommand.Parameters.AddWithValue("@Station", excelStation);
                    adActiveQty.SelectCommand.Parameters.AddWithValue("@PNo", excelPNo);
                    adActiveQty.SelectCommand.Parameters.AddWithValue("@RwkNo", rndGRNumber);
                    DataTable dtActiveQty = new DataTable();
                    adActiveQty.Fill(dtActiveQty);
                    if (dtActiveQty.Rows.Count > 0)
                    {
                        if (dtActiveQty.Rows[0]["UploadedQty"] != DBNull.Value)
                            totalQty = Convert.ToInt32(dtActiveQty.Rows[0]["UploadedQty"]);

                    }

                    using (SqlDataAdapter adNotRequired = new SqlDataAdapter("select Sum(Req_Qty) as NotReqQty from parts_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and Gr_Reference_No=@GrReferenceNo and Type=@Type", conn))
                    {
                        adNotRequired.SelectCommand.Transaction = trans;
                        adNotRequired.SelectCommand.Parameters.AddWithValue("@ToolNo", excelToolNo);
                        adNotRequired.SelectCommand.Parameters.AddWithValue("@Station", excelStation);
                        adNotRequired.SelectCommand.Parameters.AddWithValue("@PNo", excelPNo);
                        adNotRequired.SelectCommand.Parameters.AddWithValue("@GrReferenceNo", rndGRNumber);
                        adNotRequired.SelectCommand.Parameters.AddWithValue("@Type", "NotRequired");
                        DataTable dtNotReq = new DataTable();
                        adNotRequired.Fill(dtNotReq);
                        if (dtNotReq.Rows.Count > 0)
                        {
                            if (dtNotReq.Rows[0]["NotReqQty"] != DBNull.Value)
                                totalNotReqQty = Convert.ToInt32(dtNotReq.Rows[0]["NotReqQty"]);

                        }
                    }
                    rndReminigQty = totalQty - totalNotReqQty;
                }
            }
            return isError;
        }
        catch (Exception ex)
        {
            isError = true;
            ex.ToString();
            return isError;
        }
    }

    /// <summary>
    /// Retrieves the last Uploaded Version Number from Parts Gr table
    /// </summary>
    /// <param name="toolNo"></param>
    /// <param name="station"></param>
    /// <param name="pNo"></param>
    /// <param name="version"></param>
    /// <param name="type"></param>
    /// <param name="conn"></param>
    /// <param name="trans"></param>
    /// <returns></returns>
    protected bool GetLastUploadedVersionNumber(int toolNo, string station, string pNo, out string version, out string type, SqlConnection conn, SqlTransaction trans)
    {
        bool isError = false;
        string lastUploadedType = "";
        try
        {
            string ver = "", rwk = "";
            using (SqlCommand cmd = new SqlCommand("Select top 1 Version_Number, Rwk_No,Type from parts_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo  order by Version_Number desc", conn, trans)) //and type in ('','New','Modification') and Status='Active'
            {
                cmd.Parameters.AddWithValue("@ToolNo", toolNo);
                cmd.Parameters.AddWithValue("@Station", station);
                cmd.Parameters.AddWithValue("@PNo", pNo);
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.HasRows)
                    {
                        rdr.Read();
                        ver = Convert.ToString(rdr["Version_Number"]);
                        lastUploadedType = Convert.ToString(rdr["Type"]);
                    }
                }
            }
            version = ver;
            type = lastUploadedType;
            return isError;
        }
        catch (Exception ex)
        {
            ex.ToString();
            version = "";
            type = "";
            isError = true;
            return isError;
        }
    }

    /// <summary>
    /// Retrieves the last active Verison from Parts GR Table and returns the Version Number as out parameter. 
    /// </summary>
    /// <param name="toolNo"></param>
    /// <param name="station"></param>
    /// <param name="pNo"></param>
    /// <param name="version"></param>
    /// <param name="conn"></param>
    /// <param name="trans"></param>
    /// <returns></returns>
    protected bool GetLActiveVersionNumber(int toolNo, string station, string pNo, out string version, SqlConnection conn, SqlTransaction trans)
    {
        bool isError = false;
        try
        {
            string ver = "", rwk = "";
            using (SqlCommand cmd = new SqlCommand("Select top 1 Version_Number, Rwk_No from parts_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and type in ('','New','Modification') and Status='Active' order by Parts_Gr_Id desc", conn, trans))
            {
                cmd.Parameters.AddWithValue("@ToolNo", toolNo);
                cmd.Parameters.AddWithValue("@Station", station);
                cmd.Parameters.AddWithValue("@PNo", pNo);
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.HasRows)
                    {
                        rdr.Read();
                        ver = Convert.ToString(rdr["Version_Number"]);
                    }
                }
            }
            version = ver;
            return isError;
        }
        catch (Exception ex)
        {
            ex.ToString();
            version = "";
            isError = true;
            return isError;
        }
    }

    /// <summary>
    /// Validates if GR Uploaded against R&D or R&D uploaded  and returns true/ false as out parameter for isGrUploadedForRND, isRNDUploaded
    /// </summary>
    /// <param name="excelToolNo"></param>
    /// <param name="excelStation"></param>
    /// <param name="excelPNo"></param>
    /// <param name="grReferenceNo"></param>
    /// <param name="isGrUploadedForRND"></param>
    /// <param name="isRNDUploaded"></param>
    /// <returns></returns>
    private int? GetLastActiveQuantityForGRAssociatedWithRND(int excelToolNo, string excelStation, string excelPNo, string grReferenceNo, out bool isGrUploadedForRND, out bool isRNDUploaded)
    {
        int? result = null;
        isGrUploadedForRND = false;
        isRNDUploaded = false;
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            using (SqlConnection connection = new SqlConnection(sqlconnstring + "; Persist Security Info=true;"))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand("Select * from parts_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and type=@Type and Rwk_No=@RwkNo", connection))
                {
                    cmd.Parameters.AddWithValue("@ToolNo", excelToolNo);
                    cmd.Parameters.AddWithValue("@Station", excelStation);
                    cmd.Parameters.AddWithValue("@PNo", excelPNo);
                    cmd.Parameters.AddWithValue("@Type", "R&D");
                    cmd.Parameters.AddWithValue("@RwkNo", grReferenceNo);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            result = Convert.ToInt32(rdr["Req_Qty"]);
                            isRNDUploaded = true;
                        }
                    }
                }
                // Checking if GR already Uploaded for R&D
                using (SqlCommand cmd = new SqlCommand("Select * from parts_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and Gr_Reference_No=@GrRefNo and Type in ('New','Modification')", connection))
                {
                    cmd.Parameters.AddWithValue("@ToolNo", excelToolNo);
                    cmd.Parameters.AddWithValue("@Station", excelStation);
                    cmd.Parameters.AddWithValue("@PNo", excelPNo);
                    cmd.Parameters.AddWithValue("@GrRefNo", grReferenceNo);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            result = result + Convert.ToInt32(rdr["Req_Qty"]);
                            isGrUploadedForRND = true;
                        }
                    }
                }
            }
            return result;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return result;
        }
    }

    /// <summary>
    /// Inserts the data in Parts GR Table/ Part History Gr table and BOM Master table when user uploads data as New Tool
    /// </summary>
    /// <param name="partsGr"></param>
    /// <param name="row"></param>
    /// <param name="uploadType"></param>
    /// <param name="challanNumber"></param>
    /// <param name="itemsExist"></param>
    /// <param name="conn"></param>
    /// <param name="trans"></param>
    protected void InsertData_ForNewTools(PartsGr partsGr, int row, string uploadType, string challanNumber, string itemsExist, SqlConnection conn, SqlTransaction trans)
    {
        if (partsGr.PNo != "" && partsGr.Station != "" && partsGr.PartName != "" && partsGr.TargetDate != null)
        {
            DateTime uploaddate = new DateTime();
            uploaddate = Convert.ToDateTime(txtuploaddate.Text);
            if (partsGr.PartName.Length > 0 && partsGr.Station.Length > 0)
            {
                if (partsGr.ToolNo != 0 && partsGr.RwkNo.Length > 0)
                {
                    if (partsGr.PartName.Length > 0)
                    {
                        bool isRecordAlreadyExistGr = false;
                        bool isRecordAlreadyExistGrHistory = false;

                        SqlCommand cmdCheckPartsGr = new SqlCommand("select * from parts_Gr where p_no=@PNo and station=@Station and tool_no=@ToolNo and Rwk_No=@ReworkNo", conn, trans);//and part_name=@PartName
                        cmdCheckPartsGr.Parameters.AddWithValue("@PNo", partsGr.PNo);
                        //cmdCheckPartsGr.Parameters.AddWithValue("@PartName", partsGr.PartName);
                        cmdCheckPartsGr.Parameters.AddWithValue("@Station", partsGr.Station);
                        cmdCheckPartsGr.Parameters.AddWithValue("@ToolNo", partsGr.ToolNo);
                        cmdCheckPartsGr.Parameters.AddWithValue("@ReworkNo", partsGr.RwkNo);
                        using (SqlDataReader readerCheckGr = cmdCheckPartsGr.ExecuteReader())
                        {
                            if (readerCheckGr.HasRows)
                            {
                                readerCheckGr.Read();
                                isRecordAlreadyExistGr = true;
                                if (itemsExist != "")
                                {
                                    itemsExist = itemsExist + "," + row;
                                }
                                else
                                {
                                    itemsExist = Convert.ToString(row);
                                }
                            }
                        }

                        SqlCommand cmdCheckPartsGrHistory = new SqlCommand("select * from part_History_Gr where p_no=@PNo and station=@Station and tool_no=@ToolNo and Rwk_No=@ReworkNo", conn, trans);//part_name=@PartName and
                        cmdCheckPartsGrHistory.Parameters.AddWithValue("@PNo", partsGr.PNo);
                        //cmdCheckPartsGrHistory.Parameters.AddWithValue("@PartName", partsGr.PartName);
                        cmdCheckPartsGrHistory.Parameters.AddWithValue("@Station", partsGr.Station);
                        cmdCheckPartsGrHistory.Parameters.AddWithValue("@ToolNo", partsGr.ToolNo);
                        cmdCheckPartsGrHistory.Parameters.AddWithValue("@ReworkNo", partsGr.RwkNo);
                        using (SqlDataReader readerCheckGrHistory = cmdCheckPartsGrHistory.ExecuteReader())
                        {
                            if (readerCheckGrHistory.HasRows)
                            {
                                readerCheckGrHistory.Read();
                                isRecordAlreadyExistGrHistory = true;
                            }
                        }

                        if (!isRecordAlreadyExistGr && !isRecordAlreadyExistGrHistory)
                        {
                            string status = "";
                            if (partsGr.Type == "Modification" || partsGr.Type == "New" || (partsGr.Type == "" && partsGr.RwkNo.ToLower().StartsWith("r")))
                            {
                                status = "Active";
                            }
                            else if (partsGr.Type == "R&D" || partsGr.Type == "NotRequired" || partsGr.Type == "RatioUpdate")
                            {
                                status = "Inactive";
                            }
                            string strQuery = "INSERT INTO parts_GR(p_no,req_qty,part_name,matl,size ,hrc ,remark ,tool_no,station,rwk_no,opr1 ,opr2 ,opr3 ,opr4 ,opr5 ,opr6 ,opr7 ,opr8 ,opr9 ,opr10 ,opr11,opr12 ,opr13 ,opr14,opr15 ,opr16 ,opr17 ,rms ,rm_qty ,wall_thick ,angle1 ,angle2,Is_Active_Record,Date_Of_Trans,Target_Date,Parent_Positions,Child_Positons,Type,Status,Gr_Reference_No,Version_Number)  VALUES ('" + partsGr.PNo + "'," + partsGr.ReqQty + ",'" + partsGr.PartName.Trim() + "','" + partsGr.Matl + "','" + partsGr.Size + "','" + partsGr.Hrc + "','" + partsGr.Remark + "'," + partsGr.ToolNo + ",'" + partsGr.Station.Trim() + "','" + partsGr.RwkNo + "','" + partsGr.Opr1 + "','" + partsGr.Opr2 + "','" + partsGr.Opr3 + "','" + partsGr.Opr4 + "','" + partsGr.Opr5 + "','" + partsGr.Opr6 + "','" + partsGr.Opr7 + "','" + partsGr.Opr8 + "','" + partsGr.Opr9 + "','" + partsGr.Opr10 + "','" + partsGr.Opr11 + "','" + partsGr.Opr12 + "','" + partsGr.Opr13 + "','" + partsGr.Opr14 + "','" + partsGr.Opr15 + "','" + partsGr.Opr16 + "','" + partsGr.Opr17 + "','" + partsGr.Rms + "','" + partsGr.RmQty + "','" + partsGr.WallThick + "','" + partsGr.Angle1 + "','" + partsGr.Angle2 + "','1','" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "','" + partsGr.TargetDate.ToString("yyyy/MM/dd HH:mm:ss") + "','" + partsGr.ParentPositions + "','" + partsGr.ChildPositions + "','" + partsGr.Type + "','" + status + "','" + partsGr.GrReferenceNumber + "','" + partsGr.Version + "');Select Scope_Identity();";
                            SqlCommand cmd = new SqlCommand(strQuery, conn, trans);
                            cmd.Parameters.AddWithValue("@PartsGrId", ParameterDirection.Output);
                            object value = cmd.Parameters["@PartsGrId"].Value;
                            int partsGrId = Convert.ToInt32(cmd.ExecuteScalar());

                            if (status == "Active")
                            {
                                strQuery = "update parts_Gr set Status=@Status, Updated_By=@UpdatedBy, Updated_Date=@UpdatedDate where tool_no=@ToolNo and station=@Station and P_No=@PNo and rwk_no!=@RwkNo and Version_Number!=@VersionNo";
                                SqlCommand cmdUpdate = new SqlCommand(strQuery, conn, trans);
                                cmdUpdate.Parameters.AddWithValue("@ToolNo", partsGr.ToolNo);
                                cmdUpdate.Parameters.AddWithValue("@Station", partsGr.Station);
                                cmdUpdate.Parameters.AddWithValue("@PNo", partsGr.PNo);
                                cmdUpdate.Parameters.AddWithValue("@Status", "Inactive");
                                cmdUpdate.Parameters.AddWithValue("@UpdatedBy", Convert.ToString(Session["UserID"]));
                                cmdUpdate.Parameters.AddWithValue("@UpdatedDate", DateTime.Now.ToString("yyyy/MM/dd HH:MM:ss"));
                                cmdUpdate.Parameters.AddWithValue("@RwkNo", partsGr.RwkNo);
                                cmdUpdate.Parameters.AddWithValue("@VersionNo", partsGr.Version);
                                cmdUpdate.ExecuteNonQuery();
                            }

                            if (partsGr.Type != "R&D" && partsGr.Type != "RatioUpdate")
                            {
                                int? result = UpdateBOMMasterData(partsGr.ToolNo, partsGr.Station, partsGr.PNo, partsGr.PartName, partsGr.RwkNo, partsGr.ReqQty, partsGr.Type, partsGr.GrReferenceNumber, partsGr.Version, conn, trans);
                                if (result == null)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Error Occured while updating BOM. Please contact Administrator');", true);
                                    trans.Rollback();
                                    return;
                                }
                            }
                            if (partsGr.Type != "NotRequired" && partsGr.Type != "RatioUpdate")
                            {
                                string strQuery1 = "INSERT INTO part_history_GR(p_no,quantity,part_name,tool_no,station,location,date_of_trans,sent_or_rec,Upload_Qty,rwk_no,Challan_No,Uploaded_File_Name,User_name)  VALUES ('" + partsGr.PNo + "'," + partsGr.ReqQty + ",'" + partsGr.PartName.Trim() + "'," + partsGr.ToolNo + ",'" + partsGr.Station.Trim() + "','COMPANY','" + uploaddate.ToString("yyyy/MM/dd HH:mm:ss") + "','Uploaded'," + partsGr.ReqQty + ",'" + partsGr.RwkNo + "','" + challanNumber + "','" + partsGr.FilePath + "','" + Convert.ToString(Session["UserID"]) + "')";
                                SqlCommand cmd1 = new SqlCommand(strQuery1, conn, trans);
                                cmd1.ExecuteNonQuery();

                                string[] childPositionsSplit = partsGr.ChildPositions.Split(',');
                                for (int i = 0; i < childPositionsSplit.Length; i++)
                                {
                                    bool isValidNum = false;
                                    int qty = 0;
                                    string pNo = "";
                                    if (childPositionsSplit[i].ToLower().Trim().StartsWith("p_"))
                                    {
                                        if (childPositionsSplit[i].Contains("-"))
                                        {
                                            string[] quantitySplit = childPositionsSplit[i].Split('-');
                                            pNo = quantitySplit[0].ToLower().Trim().Replace("p_", "");
                                            isValidNum = int.TryParse(quantitySplit[1], out qty);
                                        }
                                        else
                                        {
                                            pNo = childPositionsSplit[i].ToLower().Trim().Replace("p_", "");
                                        }
                                        if (isValidNum)
                                        {
                                            SqlCommand cmdInsertRatio = new SqlCommand("insert into Parent_Child_Ratio_Detail (Parts_Gr_Id,P_No,Ratio,Created_Date,Created_By) Values(@PartsGrId,@PNo,@Ratio,@CreatedDate,@CreatedBy)", conn, trans);
                                            cmdInsertRatio.Parameters.AddWithValue("@PartsGrId", partsGrId);
                                            cmdInsertRatio.Parameters.AddWithValue("@PNo", pNo);
                                            cmdInsertRatio.Parameters.AddWithValue("@Ratio", qty);
                                            cmdInsertRatio.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                                            cmdInsertRatio.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
                                            cmdInsertRatio.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                            else if (partsGr.Type == "RatioUpdate")
                            {
                                string[] childPositionsSplit = partsGr.ChildPositions.Split(',');
                                for (int i = 0; i < childPositionsSplit.Length; i++)
                                {
                                    bool isValidNum = false;
                                    int qty = 0;
                                    string pNo = "";
                                    if (childPositionsSplit[i].ToLower().Trim().StartsWith("p_"))
                                    {
                                        if (childPositionsSplit[i].Contains("-"))
                                        {
                                            string[] quantitySplit = childPositionsSplit[i].Split('-');
                                            pNo = quantitySplit[0].ToLower().Trim().Replace("p_", "");
                                            isValidNum = int.TryParse(quantitySplit[1], out qty);
                                        }
                                        else
                                        {
                                            pNo = childPositionsSplit[i].ToLower().Trim().Replace("p_", "");
                                        }
                                        if (isValidNum)
                                        {
                                            SqlCommand cmdInsertRatio = new SqlCommand();
                                            cmdInsertRatio.Connection = conn;
                                            cmdInsertRatio.Transaction = trans;
                                            string partsGrIdForParent = "";
                                            cmdInsertRatio.CommandText = "select Parts_Gr_Id from Parts_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and rwk_no=@RwkNo";
                                            cmdInsertRatio.Parameters.AddWithValue("@ToolNo", partsGr.ToolNo);
                                            cmdInsertRatio.Parameters.AddWithValue("@Station", partsGr.Station);
                                            cmdInsertRatio.Parameters.AddWithValue("PNo", partsGr.PNo);
                                            cmdInsertRatio.Parameters.AddWithValue("@RwkNo", partsGr.GrReferenceNumber);
                                            using (SqlDataReader rdr = cmdInsertRatio.ExecuteReader())
                                            {
                                                rdr.Read();
                                                partsGrIdForParent = Convert.ToString(rdr["Parts_Gr_Id"]);
                                            }
                                            if (i == 0)
                                            {
                                                cmdInsertRatio.Parameters.Clear();
                                                cmdInsertRatio.CommandText = "update parts_Gr set Child_Positons=@ChildPositons where Parts_Gr_Id=@PartsGrId";
                                                cmdInsertRatio.Parameters.AddWithValue("@ChildPositons", partsGr.ChildPositions);
                                                cmdInsertRatio.Parameters.AddWithValue("@PartsGrId", partsGrIdForParent);
                                                cmdInsertRatio.ExecuteNonQuery();

                                                cmdInsertRatio.Parameters.Clear();
                                                cmdInsertRatio.CommandText = "delete from Parent_Child_Ratio_Detail where Parts_Gr_Id=@PartsGrId";
                                                cmdInsertRatio.Parameters.AddWithValue("@PartsGrId", partsGrIdForParent);
                                                cmdInsertRatio.ExecuteNonQuery();
                                            }

                                            cmdInsertRatio.Parameters.Clear();
                                            cmdInsertRatio.CommandText = "insert into Parent_Child_Ratio_Detail (Parts_Gr_Id,P_No,Ratio,Created_Date,Created_By) Values(@PartsGrId,@PNo,@Ratio,@CreatedDate,@CreatedBy)";
                                            cmdInsertRatio.Parameters.AddWithValue("@PartsGrId", partsGrIdForParent);
                                            cmdInsertRatio.Parameters.AddWithValue("@PNo", pNo);
                                            cmdInsertRatio.Parameters.AddWithValue("@Ratio", qty);
                                            cmdInsertRatio.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                                            cmdInsertRatio.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
                                            cmdInsertRatio.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                            count1 = count1 + 1;
                        }
                    }
                }
                else
                {
                    MessageBox("Please Check Tool No or Rework No, It should not be blank");
                    conn.Close();
                    return;
                }
            }
        }
        else
        {
            MessageBox("Please Check P No, Station, Part Name and Target Date,It cannot not be blank");
            conn.Close();
            return;
        }
    }

    private string GetValue(SpreadsheetDocument doc, Cell cell)
    {
        string value = "";
        if (cell.CellValue != null)
        {
            value = cell.CellValue.InnerText;
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
            }
        }
        return value;
    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    /// <summary>
    /// Updates BOM Master Data based on data provided in Excel File
    /// </summary>
    /// <param name="toolNo"></param>
    /// <param name="station"></param>
    /// <param name="pNo"></param>
    /// <param name="partName"></param>
    /// <param name="reworkNo"></param>
    /// <param name="uploadQty"></param>
    /// <param name="type"></param>
    /// <param name="grReferenceNo"></param>
    /// <param name="version"></param>
    /// <param name="conn"></param>
    /// <param name="trans"></param>
    /// <returns></returns>
    protected int? UpdateBOMMasterData(int toolNo, string station, string pNo, string partName, string reworkNo, int uploadQty, string type, string grReferenceNo, string version, SqlConnection conn, SqlTransaction trans)
    {
        //BOM Master quantity to be updated in case GR Refrence Number in provided.
        string currentType = type;
        int? result = null;
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.Transaction = trans;
        bool isExistInBOMMaster = false;
        int bomMasterId = 0;
        int lastBOMQty = 0;
        string lastUpdatedType = "", lastReworkNo = "", lastversionNo = "";
        int lastuploadedQty = 0;
        cmd.Parameters.Clear();
        cmd.CommandText = "select * from Bom_Master where tool_no=@ToolNo and p_no=@PNo and station=@Station";
        cmd.Parameters.AddWithValue("@ToolNo", toolNo);
        cmd.Parameters.AddWithValue("@PNo", pNo);
        cmd.Parameters.AddWithValue("@Station", station);
        using (SqlDataReader rdr = cmd.ExecuteReader())
        {
            if (rdr.HasRows)
            {
                rdr.Read();
                isExistInBOMMaster = true;
                bomMasterId = Convert.ToInt32(rdr["BOM_Master_Id"]);
                lastBOMQty = Convert.ToInt32(rdr["BOM_Qty"]);
                lastuploadedQty = Convert.ToInt32(rdr["Upload_Qty"]);
                lastUpdatedType = Convert.ToString(rdr["Type"]);
                lastReworkNo = Convert.ToString(rdr["Rwk_No"]);
                lastversionNo = Convert.ToString(rdr["Version_Number"]);
            }
        }

        if (type == "" && reworkNo.ToLower().StartsWith("r") && version.ToLower().Equals("v0") && !isExistInBOMMaster)
        {
            cmd.Parameters.Clear();
            cmd.CommandText = "insert into BOM_Master (Tool_No,P_No,Part_Name,Station,Rwk_No,Upload_Qty,BOM_Qty, Type, Version_Number, Created_Date,Created_By) values(@ToolNo,@PNo,@PartName,@Station,@RwkNo,@UploadQty,@BOMQty,@Type,@VersionNumber,@CreatedDate,@CreatedBy)";
            cmd.Parameters.AddWithValue("@ToolNo", toolNo);
            cmd.Parameters.AddWithValue("@PNo", pNo);
            cmd.Parameters.AddWithValue("@PartName", partName);
            cmd.Parameters.AddWithValue("@Station", station);
            cmd.Parameters.AddWithValue("@RwkNo", reworkNo);
            cmd.Parameters.AddWithValue("@UploadQty", uploadQty);
            cmd.Parameters.AddWithValue("@BOMQty", uploadQty);
            cmd.Parameters.AddWithValue("@Type", currentType);
            cmd.Parameters.AddWithValue("@VersionNumber", version);
            cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
            cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
            cmd.ExecuteNonQuery();
            result = 1;
        }
        else
        {
            int lastUploadQty = 0;
            int bomQty = 0;
            if (bomMasterId > 0)// && qty > 0
            {
                if (type == "NotRequired")
                {
                    lastUploadQty = lastuploadedQty;
                    bomQty = lastBOMQty - uploadQty;
                    reworkNo = lastReworkNo;
                    version = lastversionNo;
                    currentType = lastUpdatedType;
                }
                // Position to be marked inactive if not required qty is equal to upload qty in case of GR against R&D also
                else if (type == "New" || type == "Modification")
                {
                    if (grReferenceNo != "")
                    {
                        //Sum the R&D and its GR Upload Qty
                        cmd.Parameters.Clear();
                        cmd.CommandText = "select * from Parts_Gr where tool_no=@ToolNo and p_no=@PNo and station=@Station and Rwk_No=@RwkNo and Type=@Type";
                        cmd.Parameters.AddWithValue("@ToolNo", toolNo);
                        cmd.Parameters.AddWithValue("@PNo", pNo);
                        cmd.Parameters.AddWithValue("@Station", station);
                        cmd.Parameters.AddWithValue("@RwkNo", grReferenceNo);
                        cmd.Parameters.AddWithValue("@Type", "R&D");
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                rdr.Read();
                                int rndUploadQty = Convert.ToInt32(rdr["req_qty"]);
                                lastUploadQty = rndUploadQty + uploadQty;
                                bomQty = rndUploadQty + uploadQty;
                            }
                        }
                    }
                    else
                    {
                        lastUploadQty = uploadQty;
                        bomQty = uploadQty;
                    }
                }
                else if (type == "")//R Upload
                {
                    lastUploadQty = lastuploadedQty + uploadQty;
                    bomQty = lastBOMQty + uploadQty;
                    reworkNo = lastReworkNo;
                    version = lastversionNo;
                    currentType = lastUpdatedType;
                }
                if (bomQty > 0)
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "update Bom_Master set rwk_no=@RwkNo, Upload_Qty=@UploadQty, BOM_Qty=@BomQty, Type=@Type, Version_Number=@VersionNumber where Bom_Master_Id=@BomMasterId";
                    cmd.Parameters.AddWithValue("@RwkNo", reworkNo);
                    cmd.Parameters.AddWithValue("@UploadQty", lastUploadQty);
                    cmd.Parameters.AddWithValue("@BomQty", bomQty);
                    cmd.Parameters.AddWithValue("@Type", currentType);
                    cmd.Parameters.AddWithValue("@VersionNumber", version);
                    cmd.Parameters.AddWithValue("@BomMasterId", bomMasterId);
                    cmd.ExecuteNonQuery();

                    //If Not Required Quantity is equal to active qty for mentioned Gr Reference Number, in that case we will make the position Inactive in parts Gr
                    if (type.ToLower() == "notrequired")
                    {
                        int remainingActiveQty = 0;
                        bool isError = GetLastActiveQtyByReworkNoForNotRequiredUpload(toolNo, station, pNo, grReferenceNo, conn, trans, out remainingActiveQty);
                        if (isError)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
                            result = null;
                            trans.Rollback();
                            return result;
                        }
                        if (remainingActiveQty == 0)
                        {
                            int partsGrId = 0;
                            cmd.Parameters.Clear();
                            cmd.CommandText = "select * from Parts_Gr where tool_no=@ToolNo and Station=@Station and p_no=@PNo and rwk_no=@RwkNo";
                            cmd.Parameters.AddWithValue("@ToolNo", toolNo);
                            cmd.Parameters.AddWithValue("@Station", station);
                            cmd.Parameters.AddWithValue("@PNo", pNo);
                            cmd.Parameters.AddWithValue("@RwkNo", grReferenceNo);
                            using (SqlDataReader rdr = cmd.ExecuteReader())
                            {
                                if (rdr.HasRows)
                                {
                                    rdr.Read();
                                    partsGrId = Convert.ToInt32(rdr["Parts_Gr_Id"]);
                                }
                            }

                            if (partsGrId > 0)
                            {
                                cmd.Parameters.Clear();
                                cmd.CommandText = "update Parts_Gr set Status=@Status where Parts_Gr_Id=@PartsGrId";
                                cmd.Parameters.AddWithValue("@Status", "Inactive");
                                cmd.Parameters.AddWithValue("@PartsGrId", partsGrId);
                                cmd.ExecuteNonQuery();
                                result = 1;
                            }

                            //Updating Child as Inactive if user uploads the Parent as Inactive
                            string parentName = "P_" + pNo;
                            DataTable dt = new DataTable();
                            SqlDataAdapter ad = new SqlDataAdapter("select Parts_Gr_Id from parts_Gr where ((opr17 like '%" + parentName + ",%' or opr17 like '%," + parentName + "' or opr17 like '%," + parentName + ",%' or opr17 like '" + parentName + "') or (Parent_Positions like '%" + parentName + ",%' or Parent_Positions like '%," + parentName + "' or Parent_Positions like '%," + parentName + ",%' or Parent_Positions like '" + parentName + "')) and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + grReferenceNo + "' and Is_Active_Record=1", conn);
                            ad.SelectCommand.Transaction = trans;
                            ad.SelectCommand.Connection = conn;
                            ad.Fill(dt);
                            for (int l = 0; l < dt.Rows.Count; l++)
                            {
                                cmd.Parameters.Clear();
                                cmd.CommandText = "update Parts_Gr set Status=@Status where Parts_Gr_Id=@PartsGrId";
                                cmd.Parameters.AddWithValue("@Status", "Inactive");
                                cmd.Parameters.AddWithValue("@PartsGrId", dt.Rows[l]["Parts_Gr_Id"]);
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }

                    result = 1;
                }
                else
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "delete from BOM_Master where Bom_Master_Id=@BomMasterId";
                    cmd.Parameters.AddWithValue("@BomMasterId", bomMasterId);
                    cmd.ExecuteNonQuery();

                    int partsGrId = 0;
                    cmd.Parameters.Clear();
                    cmd.CommandText = "select * from Parts_Gr where tool_no=@ToolNo and Station=@Station and p_no=@PNo and rwk_no=@RwkNo";
                    cmd.Parameters.AddWithValue("@ToolNo", toolNo);
                    cmd.Parameters.AddWithValue("@Station", station);
                    cmd.Parameters.AddWithValue("@PNo", pNo);
                    cmd.Parameters.AddWithValue("@RwkNo", grReferenceNo);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            partsGrId = Convert.ToInt32(rdr["Parts_Gr_Id"]);
                        }
                    }

                    if (partsGrId > 0)
                    {
                        cmd.Parameters.Clear();
                        cmd.CommandText = "update Parts_Gr set Status=@Status where Parts_Gr_Id=@PartsGrId";
                        cmd.Parameters.AddWithValue("@Status", "Inactive");
                        cmd.Parameters.AddWithValue("@PartsGrId", partsGrId);
                        cmd.ExecuteNonQuery();
                        result = 1;
                    }

                    //Updating Child as Inactive if user uploads the Parent as Inactive
                    string parentName = "P_" + pNo;
                    DataTable dt = new DataTable();
                    SqlDataAdapter ad = new SqlDataAdapter("select Parts_Gr_Id from parts_Gr where ((opr17 like '%" + parentName + ",%' or opr17 like '%," + parentName + "' or opr17 like '%," + parentName + ",%' or opr17 like '" + parentName + "') or (Parent_Positions like '%" + parentName + ",%' or Parent_Positions like '%," + parentName + "' or Parent_Positions like '%," + parentName + ",%' or Parent_Positions like '" + parentName + "')) and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + grReferenceNo + "' and Is_Active_Record=1", conn);
                    ad.SelectCommand.Transaction = trans;
                    ad.SelectCommand.Connection = conn;
                    ad.Fill(dt);
                    for (int l = 0; l < dt.Rows.Count; l++)
                    {
                        cmd.Parameters.Clear();
                        cmd.CommandText = "update Parts_Gr set Status=@Status where Parts_Gr_Id=@PartsGrId";
                        cmd.Parameters.AddWithValue("@Status", "Inactive");
                        cmd.Parameters.AddWithValue("@PartsGrId", dt.Rows[l]["Parts_Gr_Id"]);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            else
            {
                result = 0;
            }
        }
        return result;
    }

    protected void radioOldTool_CheckedChanged(object sender, EventArgs args)
    {
        if (radioOldTool.Checked)
            panelControls.Visible = true;
        else
            panelControls.Visible = false;
    }

    protected void radioNewTool_CheckedChanged(object sender, EventArgs args)
    {
        if (radioNewTool.Checked)
            panelControls.Visible = true;
        else
            panelControls.Visible = false;
    }


    /// <summary>
    /// Validates and Inserts the data for Old Tools
    /// </summary>
    /// <param name="path"></param>
    public void GetDataTableFromCsv_ForOldTools(string path)
    {
        DataTable failitem = new DataTable();

        object request_id = "";

        string p_no = "";
        DataTable unsuccessfulyrow = new DataTable();
        int req_qty = 0;
        string part_name = "", matl = "", size = "", hrc = "", remark = "", station = "", rwk_no = "", opr1 = "",
        opr2 = "", opr3 = "", opr4 = "", opr5 = "", opr6 = "", opr7 = "", opr8 = "", opr9 = "", opr10 = "", opr11 = "", opr12 = "",
        opr13 = "", opr14 = "", opr15 = "", opr16 = "", opr17 = "", rms = "", rm_qty = "", wall_thick = "", angle1 = "", angle2 = "", childPositions = "",
        parentPositions = "", type = "", grReferenceNumber = "", status = "";
        int tool_no = 0;

        DateTime targetDate = DateTime.MinValue;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        SqlTransaction trans = null;
        string itemsExist = "";
        try
        {
            lblErrorMessage.Text = String.Empty;
            lblchallanno.Text = String.Empty;
            conn.Open();
            trans = conn.BeginTransaction();
            string pathOnly = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);

            DataTable dt = new DataTable();

            string uploadType = "";
            string challanNumber = "";

            DataSet ds = null;
            DataTable dtprojectItems = new DataTable();
            using (clsFileReadHelper csv = new clsFileReadHelper(path))
            {
                ds = csv.getFileData();
            }
            if (ds.Tables.Count == 1)
            {
                dtprojectItems = ds.Tables[0].Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
            }

            if (dtprojectItems.Rows.Count > 0)
            {
                if (dtprojectItems.Columns.Count != 37)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Number of Columns In Uploaded Excel File, Does Not Match Actual Format.');", true);
                    return;
                }
            }

            for (int i = 1; i < dtprojectItems.Rows.Count; i++)
            {
                int validateTool = 0;
                if (dtprojectItems.Rows[i][11] != null)
                {
                    validateTool = Convert.ToInt32(dtprojectItems.Rows[i][11]);
                }
                if (validateTool > 673 && validateTool != 5000)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Please upload excel using New Tools option.');", true);
                    return;
                }
            }

            for (int i = 1; i < dtprojectItems.Rows.Count; i++)
            {
                if (string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][0])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][1])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][2])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][3]))
                   && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][4])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][5])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][6])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][7]))
                    && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][8])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][9])) && string.IsNullOrEmpty(Convert.ToString(dtprojectItems.Rows[i][10])))
                {
                    continue;
                }

                rwk_no = Convert.ToString(dtprojectItems.Rows[i][0]);
                station = Convert.ToString(dtprojectItems.Rows[i][1]);
                p_no = Convert.ToString(dtprojectItems.Rows[i][2]);
                if (dtprojectItems.Rows[i][3] != null)
                    req_qty = Convert.ToInt32(dtprojectItems.Rows[i][3]);
                part_name = Convert.ToString(dtprojectItems.Rows[i][4]);
                matl = Convert.ToString(dtprojectItems.Rows[i][5]);
                childPositions = Convert.ToString(dtprojectItems.Rows[i][6]);
                rms = Convert.ToString(dtprojectItems.Rows[i][7]);
                rm_qty = Convert.ToString(dtprojectItems.Rows[i][8]);
                hrc = Convert.ToString(dtprojectItems.Rows[i][9]);
                remark = Convert.ToString(dtprojectItems.Rows[i][10]);
                if (dtprojectItems.Rows[i][11] != null)
                {
                    tool_no = Convert.ToInt32(dtprojectItems.Rows[i][11]);
                }
                opr1 = Convert.ToString(dtprojectItems.Rows[i][12]);
                opr2 = Convert.ToString(dtprojectItems.Rows[i][13]);
                opr3 = Convert.ToString(dtprojectItems.Rows[i][14]);
                opr4 = Convert.ToString(dtprojectItems.Rows[i][15]);
                opr5 = Convert.ToString(dtprojectItems.Rows[i][16]);
                opr6 = Convert.ToString(dtprojectItems.Rows[i][17]);
                opr7 = Convert.ToString(dtprojectItems.Rows[i][18]);
                opr8 = Convert.ToString(dtprojectItems.Rows[i][19]);
                opr9 = Convert.ToString(dtprojectItems.Rows[i][20]);
                opr10 = Convert.ToString(dtprojectItems.Rows[i][21]);
                opr11 = Convert.ToString(dtprojectItems.Rows[i][22]);
                opr12 = Convert.ToString(dtprojectItems.Rows[i][23]);
                opr13 = Convert.ToString(dtprojectItems.Rows[i][24]);
                opr14 = Convert.ToString(dtprojectItems.Rows[i][25]);
                opr15 = Convert.ToString(dtprojectItems.Rows[i][26]);
                opr16 = Convert.ToString(dtprojectItems.Rows[i][27]);
                parentPositions = Convert.ToString(dtprojectItems.Rows[i][28]);
                wall_thick = Convert.ToString(dtprojectItems.Rows[i][29]);
                angle1 = Convert.ToString(dtprojectItems.Rows[i][30]);
                angle2 = Convert.ToString(dtprojectItems.Rows[i][31]);
                if (dtprojectItems.Rows[i][32] != null)
                {
                    targetDate = Convert.ToDateTime(dtprojectItems.Rows[i][32]);
                }
                size = Convert.ToString(dtprojectItems.Rows[i][33]);
                opr17 = Convert.ToString(dtprojectItems.Rows[i][34]);
                status = "Old";
                type = "Old";
                grReferenceNumber = "";
                part_name = part_name.Replace("  ", "");
                part_name = part_name.Replace(" ", "");


                station = station.Replace("  ", "");
                station = station.Replace(" ", "");

                if (i == 1)
                {
                    if (rwk_no.StartsWith("R") || rwk_no == "")
                    {
                        string prefixStr = "CH";
                        challanNumber = UtilityFunctions.gencode(prefixStr);
                        uploadType = "Fresh";
                    }
                    else if (rwk_no.StartsWith("GR"))
                    {
                        string prefixStr = "CH";
                        challanNumber = UtilityFunctions.gencode(prefixStr);
                        uploadType = "Rework";
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Invalid Rework Type Entered at Line Number " + i + " ');", true);
                        trans.Rollback();
                        break;
                    }
                }

                if (tool_no != 0 && p_no != "" && station != "")
                {

                    //Checking if Data at the First Row is Fresh then all row should contain Fresh Data 
                    if (rwk_no.StartsWith("R") || rwk_no == "")
                    {
                        if (uploadType != "Fresh")
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Please Upload Similar Type of Data. Check Row Number " + i + " ');", true);
                            trans.Rollback();
                            break;
                        }
                    }
                    //Checking if Data at the First Row is Rework then all row should contain Rework Data 
                    else if (rwk_no.StartsWith("GR"))
                    {
                        if (uploadType != "Rework")
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Please Upload Similar Type of Data. Check Row Number " + i + " ');", true);
                            trans.Rollback();
                            break;
                        }
                    }

                    // Code goes here 
                    InsertData_ForOldTools(tool_no, station, p_no, part_name, rwk_no, i, uploadType, challanNumber, itemsExist, req_qty, matl, size, hrc, remark, opr1, opr2, opr3, opr4, opr5,
            opr6, opr7, opr8, opr9, opr10, opr11, opr12, opr13, opr14, opr15,
                opr16, opr17, rms, rm_qty, wall_thick, angle1, angle2, fileName, targetDate, parentPositions, childPositions, conn, trans);
                }
            }
            if (count1 > 0)
                trans.Commit();
            lblchallanno.Text = challanNumber;
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                }
            }
        }
        catch (Exception ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }

        if (count1 > 0)
        {
            //movefile(path);
            MessageBox("File Uploaded Successfully");
            conn.Close();
        }
        else
        {
            lblchallanno.Text = String.Empty;
            MessageBox("Upload Failed. Data Already Exist in Database.");
            conn.Close();
        }
        itemsExist = itemsExist.TrimEnd(',');
        if (!string.IsNullOrEmpty(itemsExist))
        {
            lblErrorMessage.Text = "Items at Row Number " + itemsExist + " alreay exist in the Database.";
        }
    }

    /// <summary>
    /// Inserts the data in Parts Gr table, Part History Gr table and BOM Master table. This method is called when user uploads data as Old Tools.
    /// </summary>
    /// <param name="tool_no"></param>
    /// <param name="station"></param>
    /// <param name="p_no"></param>
    /// <param name="part_name"></param>
    /// <param name="rwk_no"></param>
    /// <param name="row"></param>
    /// <param name="uploadType"></param>
    /// <param name="challanNumber"></param>
    /// <param name="itemsExist"></param>
    /// <param name="req_qty"></param>
    /// <param name="matl"></param>
    /// <param name="size"></param>
    /// <param name="hrc"></param>
    /// <param name="remark"></param>
    /// <param name="opr1"></param>
    /// <param name="opr2"></param>
    /// <param name="opr3"></param>
    /// <param name="opr4"></param>
    /// <param name="opr5"></param>
    /// <param name="opr6"></param>
    /// <param name="opr7"></param>
    /// <param name="opr8"></param>
    /// <param name="opr9"></param>
    /// <param name="opr10"></param>
    /// <param name="opr11"></param>
    /// <param name="opr12"></param>
    /// <param name="opr13"></param>
    /// <param name="opr14"></param>
    /// <param name="opr15"></param>
    /// <param name="opr16"></param>
    /// <param name="opr17"></param>
    /// <param name="rms"></param>
    /// <param name="rm_qty"></param>
    /// <param name="wall_thick"></param>
    /// <param name="angle1"></param>
    /// <param name="angle2"></param>
    /// <param name="fileName"></param>
    /// <param name="targetDate"></param>
    /// <param name="parentPositions"></param>
    /// <param name="childPositions"></param>
    /// <param name="conn"></param>
    /// <param name="trans"></param>
    protected void InsertData_ForOldTools(int tool_no, string station, string p_no, string part_name, string rwk_no, int row, string uploadType, string challanNumber, string itemsExist, int req_qty, string matl, string size, string hrc, string remark, string opr1, string opr2, string opr3, string opr4, string opr5,
        string opr6, string opr7, string opr8, string opr9, string opr10, string opr11, string opr12, string opr13, string opr14, string opr15,
            string opr16, string opr17, string rms, string rm_qty, string wall_thick, string angle1, string angle2, string fileName, DateTime targetDate, string parentPositions, string childPositions, SqlConnection conn, SqlTransaction trans)
    {
        if (p_no != "" && station != "" && part_name != "" && targetDate != null)
        {

            DateTime uploaddate = new DateTime();
            uploaddate = Convert.ToDateTime(txtuploaddate.Text);
            if (part_name.Length > 0 && station.Length > 0)
            {
                if (tool_no != 0 && rwk_no.Length > 0)
                {

                    if (part_name.Length > 0)
                    {

                        bool isRecordAlreadyExistGr = false;
                        SqlCommand cmdCheckPartsGr = new SqlCommand("select * from parts_Gr where p_no=@PNo and part_name=@PartName and station=@Station and tool_no=@ToolNo and Rwk_No=@ReworkNo", conn, trans);
                        cmdCheckPartsGr.Parameters.AddWithValue("@PNo", p_no);
                        cmdCheckPartsGr.Parameters.AddWithValue("@PartName", part_name);
                        cmdCheckPartsGr.Parameters.AddWithValue("@Station", station);
                        cmdCheckPartsGr.Parameters.AddWithValue("@ToolNo", tool_no);
                        cmdCheckPartsGr.Parameters.AddWithValue("@ReworkNo", rwk_no);
                        using (SqlDataReader readerCheckGr = cmdCheckPartsGr.ExecuteReader())
                        {
                            if (readerCheckGr.HasRows)
                            {
                                readerCheckGr.Read();
                                isRecordAlreadyExistGr = true;
                                if (itemsExist != "")
                                {
                                    itemsExist = itemsExist + "," + row;
                                }
                                else
                                {
                                    itemsExist = Convert.ToString(row);
                                }
                            }
                        }

                        bool isRecordAlreadyExistGrHistory = false;
                        SqlCommand cmdCheckPartsGrHistory = new SqlCommand("select * from part_History_Gr where p_no=@PNo and part_name=@PartName and station=@Station and tool_no=@ToolNo and Rwk_No=@ReworkNo", conn, trans);
                        cmdCheckPartsGrHistory.Parameters.AddWithValue("@PNo", p_no);
                        cmdCheckPartsGrHistory.Parameters.AddWithValue("@PartName", part_name);
                        cmdCheckPartsGrHistory.Parameters.AddWithValue("@Station", station);
                        cmdCheckPartsGrHistory.Parameters.AddWithValue("@ToolNo", tool_no);
                        cmdCheckPartsGrHistory.Parameters.AddWithValue("@ReworkNo", rwk_no);
                        using (SqlDataReader readerCheckGrHistory = cmdCheckPartsGrHistory.ExecuteReader())
                        {
                            if (readerCheckGrHistory.HasRows)
                            {
                                readerCheckGrHistory.Read();
                                isRecordAlreadyExistGrHistory = true;
                            }
                        }

                        if (!isRecordAlreadyExistGr && !isRecordAlreadyExistGrHistory)
                        {
                            string strQuery = "INSERT INTO parts_GR( p_no,req_qty,part_name,matl,size ,hrc ,remark ,tool_no,station,rwk_no,opr1 ,opr2 ,opr3 ,opr4 ,opr5 ,opr6 ,opr7 ,opr8 ,opr9 ,opr10 ,opr11,opr12 ,opr13 ,opr14,opr15 ,opr16 ,opr17 ,rms ,rm_qty ,wall_thick ,angle1 ,angle2,Is_Active_Record,Date_Of_Trans,Target_Date,Parent_Positions,Child_Positons,Type,Status)  VALUES ('" + p_no.ToString() + "'," + req_qty + ",'" + part_name.Trim() + "','" + matl + "','" + size + "','" + hrc + "','" + remark + "'," + tool_no + ",'" + station.Trim() + "','" + rwk_no + "','" + opr1 + "','" + opr2 + "','" + opr3 + "','" + opr4 + "','" + opr5 + "','" + opr6 + "','" + opr7 + "','" + opr8 + "','" + opr9 + "','" + opr10 + "','" + opr11 + "','" + opr12 + "','" + opr13 + "','" + opr14 + "','" + opr15 + "','" + opr16 + "','" + opr17 + "','" + rms + "','" + rm_qty + "','" + wall_thick + "','" + angle1 + "','" + angle2 + "','1','" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "','" + targetDate.ToString("yyyy/MM/dd HH:mm:ss") + "','" + parentPositions + "','" + childPositions + "','Old','Old');Select Scope_Identity();";
                            SqlCommand cmd = new SqlCommand(strQuery, conn, trans);
                            cmd.Parameters.AddWithValue("@PartsGrId", ParameterDirection.Output);
                            object value = cmd.Parameters["@PartsGrId"].Value;
                            int partsGrId = Convert.ToInt32(cmd.ExecuteScalar());

                            string strQuery1 = "INSERT INTO part_history_GR(p_no,quantity,part_name,tool_no,station,location,date_of_trans,sent_or_rec,Upload_Qty,rwk_no,Challan_No,Uploaded_File_Name,User_name)  VALUES ('" + p_no.ToString() + "'," + req_qty + ",'" + part_name.Trim() + "'," + tool_no + ",'" + station.Trim() + "','COMPANY','" + uploaddate.ToString("yyyy/MM/dd HH:mm:ss") + "','Uploaded'," + req_qty + ",'" + rwk_no + "','" + challanNumber + "','" + fileName + "','" + Convert.ToString(Session["UserID"]) + "')";
                            SqlCommand cmd1 = new SqlCommand(strQuery1, conn, trans);
                            cmd1.ExecuteNonQuery();
                            count1 = count1 + 1;

                            string[] childPositionsSplit = childPositions.Split(',');
                            for (int i = 0; i < childPositionsSplit.Length; i++)
                            {
                                bool isValidNum = false;
                                int qty = 0;
                                string pNo = "";
                                if (childPositionsSplit[i].ToLower().Trim().StartsWith("p_"))
                                {
                                    if (childPositionsSplit[i].Contains("-"))
                                    {
                                        string[] quantitySplit = childPositionsSplit[i].Split('-');
                                        pNo = quantitySplit[0].ToLower().Trim().Replace("p_", "");
                                        isValidNum = int.TryParse(quantitySplit[1], out qty);
                                    }
                                    else
                                    {
                                        pNo = childPositionsSplit[i].ToLower().Trim().Replace("p_", "");
                                    }
                                    if (isValidNum)
                                    {
                                        SqlCommand cmdInsertRatio = new SqlCommand("insert into Parent_Child_Ratio_Detail (Parts_Gr_Id,P_No,Ratio,Created_Date,Created_By) Values(@PartsGrId,@PNo,@Ratio,@CreatedDate,@CreatedBy)", conn, trans);
                                        cmdInsertRatio.Parameters.AddWithValue("@PartsGrId", partsGrId);
                                        cmdInsertRatio.Parameters.AddWithValue("@PNo", pNo);
                                        cmdInsertRatio.Parameters.AddWithValue("@Ratio", qty);
                                        cmdInsertRatio.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                                        cmdInsertRatio.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
                                        cmdInsertRatio.ExecuteNonQuery();

                                    }
                                }
                            }
                            count1 = count1 + 1;
                        }
                    }
                }

                else
                {

                    MessageBox("Please Check Tool No or Rework No, It should not be blank");
                    conn.Close();
                    return;
                }
            }
        }
        else
        {
            MessageBox("Please Check P No, Station, Part Name and Target Date,It cannot not be blank");
            conn.Close();
            return;
        }
    }
}

