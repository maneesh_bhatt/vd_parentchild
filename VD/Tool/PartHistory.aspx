﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.master" CodeFile="PartHistory.aspx.cs" Inherits="Tool_PartHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script type="text/javascript">


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="margin-left: 25px" align="left" width="850px">
        <tr>
            <td colspan="4" align="center" style="font-size: x-large; color: black">
                <span class="style1">Part History </span>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
        <td></td>
            <td height="30px">
                Part Name:&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label1" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <td></td>
            <td height="30px">
                Position No:&nbsp;&nbsp; <asp:Label ID="Label2" runat="server"></asp:Label>
            </td>
           
        </tr>
        <tr>
         <td></td>
            <td height="30px">
                Tool No:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label3" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <td></td>
            <td height="30px">
                Station :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label4" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <td></td>
           <%-- <td height="30px">
                Total Required Quantity:&nbsp;&nbsp; <asp:Label ID="Label5" runat="server"></asp:Label>
                &nbsp;
            </td>--%>
        </tr>
        <tr>
            <td height="10px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server" Width="900px" ScrollBars="Horizontal">
                                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" EmptyDataText="No Record Found"
                                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None"
                                    OnPageIndexChanging="GridView1_PageIndexChanging" Font-Size="13px" Width="100%"
                                    OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                ID
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" HeaderStyle-Width="150px" Text='<%# Eval("id") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Location
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("location") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Date of Transaction
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label7" runat="server"  Text='<%# Eval("date_of_trans") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Sent/Received
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" Text='<%# Eval("sent_or_rec") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                quantity
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label9" runat="server" Text='<%# Eval("quantity") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                         <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                SendQty/Received Qty
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label23" runat="server" Text='<%# Eval("SendQty_Or_ReceivedQty") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                Rework No
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblReworkNo" runat="server" Text='<%# Eval("Rwk_No") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                </asp:GridView>
                            </asp:Panel>
                            </td> </td> </tr>
                            <%-- <tr>
                                <td height="50px">
                                </td>
                                <td colspan="3" align="center">
                                    <asp:Button ID="btnApproved" runat="server" Text="Add Rate" OnClick="btnApproved_Click"
                                        Width="150px" />
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                </td>
                            </tr>--%>
                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
    </table>
</asp:Content>