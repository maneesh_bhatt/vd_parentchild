﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

public partial class ChallanByPending_Part_List : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ImageButton2.Visible = false;
        }
    }

    public void showgrid()
    {
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);

            string s = "  select temp.tool_no,temp.location,SUM(temp.Total_pending) as Total_pending,'' as TotalRecQty ,sum(temp.TotUploadqty) as TotUploadqty,temp.Sent_Received_Date,temp.Delivery_commit_date,temp.Challan_No,temp.Explain_Date,temp.sent_or_rec from (select tool_no,sum(Upload_Qty) as TotUploadqty,location,sum(isnull(quant_sent,0)-ISNULL(quant_rec,0)) as Total_pending,(isnull(quant_sent,0)-ISNULL(quant_rec,0)) as qtypending,quant_sent,quant_rec, convert(varchar(30),CONVERT(date,Sent_Received_Date,103)) as Sent_Received_Date,convert(varchar(30),CONVERT(date,Delivery_commit_date,103)) as Delivery_commit_date,convert(varchar(30),CONVERT(date,Explain_Date,103)) as Explain_Date,Challan_No,'' as sent_or_rec from location_info_GR  group by Challan_No,tool_no,location,Upload_Qty,quant_sent,quant_rec,Sent_Received_Date,Received_Date,Delivery_commit_date,Explain_Date  having (isnull(quant_sent,0)-ISNULL(quant_rec,0))>0 and  (location<>'Assembly' and location<>'VD Finish Store')  and Challan_No='" + tbchallanTool.Text + "' )   temp group by temp.Challan_No,temp.tool_no,temp.Delivery_commit_date,temp.Sent_Received_Date,temp.Explain_Date ,temp.location,temp.sent_or_rec  union all  select temp2.tool_no,temp2.location,'' as Total_pending,sum(temp2.TransSentQty)  as TotalRecQty ,SUM(temp2.Upload_Qty) as TotalUpload_Qty,convert(varchar(30),CONVERT(date,temp2.date_of_trans,103)) as Sent_Received_Date,convert(varchar(30),CONVERT(date,temp2.Delivery_commit_date,103)) as Delivery_commit_date,temp2.Challan_No,convert(varchar(30),CONVERT(date,temp2.Explain_Date,103)) as Explain_Date,temp2.sent_or_rec from(SELECT distinct tool_no ,station,date_of_trans,p_no ,location ,quantity ,QtySent ,TransSentQty ,Upload_Qty ,Explain_Date ,Delivery_commit_date, Challan_No,sent_or_rec FROM part_history_GR where TransSentQty>0  and (location<>'Assembly' and location<>'VD Finish Store')   and sent_or_rec='Received' and  Challan_No='" + tbchallanTool.Text + "' )temp2 group by Challan_No,temp2.tool_no,temp2.location,convert(varchar(30),CONVERT(date,temp2.date_of_trans,103)),convert(varchar(30),CONVERT(date,temp2.Delivery_commit_date,103)) ,convert(varchar(30),CONVERT(date,temp2.Explain_Date,103)),temp2.sent_or_rec   union all  SELECT temp3.tool_no,temp3.location,sum(temp3.Total_pending) as Total_pending,temp3.TotalRecQty as TotalRecQty,sum(temp3.TotUploadqty) as TotUploadqty,temp3.Sent_Received_Date,temp3.Delivery_commit_date,temp3.Challan_No,temp3.Explain_Date,temp3.sent_or_rec FROM (select parthist.tool_no,parthist.location,parthist.quantity as Total_pending,'' as TotalRecQty,parthist.Upload_Qty  as TotUploadqty,parthist.Sent_Date as Sent_Received_Date,parthist.Delivery_commit_date,parthist.Challan_No,parthist.Explain_Date,parthist.sent_or_rec FROM part_history_GR parthist  where parthist.Challan_No='" + tbchallanTool.Text + "'  and location='COMPANY' and parthist.Sent_Date is null  and quantity>0)temp3  group by temp3.Challan_No,temp3.tool_no,temp3.location,temp3.Total_pending,temp3.TotUploadqty,temp3.Sent_Received_Date,temp3.Delivery_commit_date,temp3.Challan_No,temp3.Explain_Date,temp3.sent_or_rec,temp3.TotalRecQty";
            SqlCommand cmd = new SqlCommand(s);

            cmd.CommandType = CommandType.Text;
            conn.Open();
            cmd.Connection = conn;
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            Session["DataTableAdvanceReport"] = dt;
            if (dt.Rows.Count > 0)
            {
                GridView1.DataSource = dt;
                lblTotal1.Text = dt.Rows.Count.ToString();
                GridView1.DataBind();
                ImageButton2.Visible = true;
                conn.Close();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        DataTable table = (DataTable)Session["DataTableAdvanceReport"];
        GridView1.DataSource = table;
        GridView1.DataBind();
    }
    
  private void ExportGridToPDF()  
  {  
     //GridView1.AllowPaging = Convert.ToBoolean(rbPaging.SelectedItem.Value);
      GridView1.DataBind();

      //Create a table
      iTextSharp.text.Table table = new iTextSharp.text
                   .Table(GridView1.Columns.Count);
      table.Cellpadding = 5;

      //Set the column widths
      int[] widths = new int[GridView1.Columns.Count];
      for (int x = 0; x < GridView1.Columns.Count; x++)
      {
          widths[x] = (int)GridView1.Columns[x].ItemStyle.Width.Value;
          string cellText = Server.HtmlDecode(GridView1.HeaderRow.Cells[x].Text);
          iTextSharp.text.Cell cell = new iTextSharp.text.Cell(cellText);
          cell.BackgroundColor = new Color(System
                             .Drawing.ColorTranslator.FromHtml("#008000"));
          table.AddCell(cell);
      }
      table.SetWidths(widths);

      //Transfer rows from GridView to table
      for (int i = 0; i < GridView1.Rows.Count; i++)
      {
          if (GridView1.Rows[i].RowType == DataControlRowType.DataRow)
          {
              for (int j = 0; j < GridView1.Columns.Count; j++)
              {
                  string cellText = Server.HtmlDecode
                                    (GridView1.Rows[i].Cells[j].Text);
                  iTextSharp.text.Cell cell = new iTextSharp.text.Cell(cellText);

                  //Set Color of Alternating row
                  if (i % 2 != 0)
                  {
                      cell.BackgroundColor = new Color(System.Drawing
                                          .ColorTranslator.FromHtml("#C2D69B"));
                  }
                  table.AddCell(cell);
              }
          }
      }

      //Create the PDF Document
      Document pdfDoc = new Document(PageSize.A4, 7f, 7f, 7f, 0f);
      PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
      pdfDoc.Open();
      pdfDoc.Add(table);
      pdfDoc.Close();
      Response.ContentType = "application/pdf";
      Response.AddHeader("content-disposition", "attachment;" +
                                     "filename=GridViewExport.pdf");
      Response.Cache.SetCacheability(HttpCacheability.NoCache);
      Response.Write(pdfDoc);
      Response.End();
  }  

    protected void Show_Hide_OrdersGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlProducts").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            //string customerId = gvCustomers.DataKeys[row.RowIndex].Value.ToString();

            //BindOrders(customerId, gvOrders);
            string customerId = GridView1.DataKeys[row.RowIndex].Value.ToString();
            string cell_1_Value = GridView1.Rows[row.RowIndex].Cells[10].Text;
            GridView gvProducts = row.FindControl("gvProducts") as GridView;
            BindOrders(customerId, gvProducts, cell_1_Value);
        }
        else
        {
            row.FindControl("pnlProducts").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    private void BindOrders(string customerId, GridView gvProducts, string status)
    {
        gvProducts.ToolTip = customerId;
        DataTable dt = new DataTable();
        if (status == "Received")
        {
            dt = UtilityFunctions.GetData(string.Format(" SELECT distinct   p_no   ,station   ,location ,quantity,TransSentQty  ,tool_no ,Explain_Date,'' as qtypending  ,Delivery_commit_date  ,Challan_No ,TransSentQty  ,Upload_Qty FROM part_history_GR where Challan_No='" + customerId + "' and sent_or_rec='Received' and (location<>'Assembly' and location<>'VD Finish Store' ) and TransSentQty>0 "));
        }

        if (status == "Received")
        {
            dt = UtilityFunctions.GetData(string.Format(" SELECT distinct   p_no   ,station   ,location ,quantity,TransSentQty  ,tool_no ,Explain_Date,'' as qtypending  ,Delivery_commit_date  ,Challan_No ,TransSentQty  ,Upload_Qty FROM part_history_GR where Challan_No='" + customerId + "' and sent_or_rec='Received' and (location<>'Assembly' and location<>'VD Finish Store' ) and TransSentQty>0 "));
        }

        if(status == "Uploaded")
        {
            dt = UtilityFunctions.GetData(string.Format(" SELECT tool_no   ,Rework   ,station   ,p_no   ,part_name ,location   ,quantity as qtypending ,Upload_Qty,date_of_trans, TransSentQty ,Explain_Date   ,Delivery_commit_date  ,Challan_No    ,Received_Date   ,Sent_Date  FROM part_history_GR where quantity>0  and  sent_or_rec='Uploaded' and Sent_Date is null  and quantity>0 and Challan_No='" + customerId + "'"));
        }
        else
        {
            dt = UtilityFunctions.GetData(string.Format(" select tool_no,Rework,station,p_no,part_name,req_qty,'' as TransSentQty,Upload_Qty,location,(isnull(quant_sent,0)-ISNULL(quant_rec,0)) as qtypending,quant_sent,quant_rec,quant_pend,quant_others,Sent_Received_Date,Received_Date from location_info_GR  where (isnull(quant_sent,0)-ISNULL(quant_rec,0))>0 and  (location<>'Assembly' and location<>'VD Finish Store' )       and Challan_No='" + customerId + "'"));
        }

        gvProducts.DataSource = dt;
        Session["BindOrders"] = dt;
        gvProducts.DataBind();
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        showgrid();
    }

    protected void ImageButton2_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        if (GridView1.Rows.Count > 0)
        {
            if (tbchallanTool.Text.Length > 0)
            {
                Session["Min"] = tbchallanTool.Text;
                Response.Write("<script language=javascript>child=window.open('Print_challan_Details.aspx');</script>");
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('No Row For Print');", true);
        }
    }
}