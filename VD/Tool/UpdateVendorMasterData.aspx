﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true" CodeFile="UpdateVendorMasterData.aspx.cs" Inherits="Tool_UpdateVendorMasterData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <style type="text/css">
        .style2
        {
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table style="margin-left: 150px; padding-left: 36px; width: 80%; line-height: 40px;"
        border="1px" align="center">
        <tr style="border-bottom: 1px solid black; height: 30px;">
            <td class="style2" align="left" colspan="4">
                <strong>Upload Vendor Master Job Work</strong>
            </td>
        </tr>
        <tr>
            <td style="margin-top: 50px;" align="left" class="style2">
                Browse File
            </td>
           <td class="style2" colspan="2" align="left">
                <asp:FileUpload ID="FileUploadControl" runat="server" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only Exl file is allowed!"
                    ValidationExpression="^.+(.xlsx|.xls|.csv)$" ControlToValidate="FileUploadControl"> </asp:RegularExpressionValidator>
                <br />
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="2">
                <asp:Label ID="lblErrorMessage" ForeColor="Red" runat="server" Style="line-height: 20px;"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
            <td class="style2" align="left">
                <asp:ImageButton ID="btnsave" Width="99px" ImageUrl="~/Tool/upload.jpg"  Height="44px" style="float:left;"
                    Visible="true" Text="Upload" runat="server" OnClick="UploadButton_Click" />
               
                <br />
            </td>
           
        </tr>
    </table>
</asp:Content>

