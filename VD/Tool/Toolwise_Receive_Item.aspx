﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Pages/MasterPage.master" CodeFile="Toolwise_Receive_Item.aspx.cs"
    Inherits="Toolwise_Receive_Item" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../Scripts/ScrollableGridViewPlugin_ASP.NetAJAXmin.js" type="text/javascript"></script>
    <script type="text/javascript">


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <title></title>
    <style type="text/css">
	 	body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .Grid
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .Grid td
        {
            background-color: Highlight;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Grid th
        {
            background-color: Navy;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .ChildGrid td
        {
            background-color: #eee !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .ChildGrid th
        {
            background-color: #6C6C6C !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid td
        {
            background-color: #fff !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid th
        {
            background-color: #2B579A !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
    </style>
    <style type="text/css">
        .style2
        {
            border: none;
        }
    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[id*=imgOrdersShow]").each(function () {
                if ($(this)[0].src.indexOf("minus") != -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
                    $(this).next().remove();
                }
            });
            $("[id*=imgProductsShow]").each(function () {
                if ($(this)[0].src.indexOf("minus") != -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
                    $(this).next().remove();
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="1px" style="border-color: Blue;width:100%;" align="left">
        <tr>
            <td class="style2" align="center" style="font-size: x-large; color: black">
                <span class="style1">Receive Items
                    <br />
                    <br />
                </span>
                <asp:ScriptManager ID="ScriptManager2" runat="server">
                </asp:ScriptManager>
            </td>
         
        </tr>
        <tr>
            <td class="style2">
                <asp:Panel ID="Panel4" runat="server">
                    Location:&nbsp;&nbsp;<asp:DropDownList ID="ddllocation" AppendDataBoundItems="true"
                        AutoPostBack="true" Width="140px" runat="server" OnSelectedIndexChanged="ddllocation_SelectedIndexChanged">
                    </asp:DropDownList>
 
                    &nbsp;&nbsp;Project No &nbsp;<asp:TextBox ID="txtprojectno" runat="server" Width="90px"></asp:TextBox>
                    &nbsp;&nbsp; Receive Date<span class="style12">&nbsp;</span><asp:TextBox ID="txtReceivecdate"
                        runat="server" Width="90px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                        Enabled="true" TargetControlID="txtReceivecdate">
                    </cc1:CalendarExtender>
                    &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnSearch" runat="server" style="background:#335599 url(../images/bg.png) repeat-x 0 -110px;color:white;height:37px;width:90px;margin-left:20px" Text="Search" OnClick="btnSearch_Click" />
               
                 <asp:Button ID="btnprint" runat="server" EnableViewState="true" style="float:right;    height: 30px;
    width: 120px; color:white;background-color:Red;border:1px solid #dadada;"  Text="PrintChallan"
                    OnClick="btnprint_Click" Width="100px" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
            <td class="style2" height="5px">
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="5">
                <asp:Label ID="lbl" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblChallanno" runat="server" Text="Challan No"></asp:Label>
                &nbsp;&nbsp;&nbsp;<asp:Label ID="txtchallanno" Visible="False" runat="server" Text=""></asp:Label>
                <asp:GridView ID="gvCustomers" runat="server" AutoGenerateColumns="false" Width="100%"
                    CssClass="Grid" DataKeyNames="tool_no">
                    <Columns>
                        <asp:TemplateField HeaderStyle-BackColor="Green">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgOrdersShow" runat="server" OnClick="Show_Hide_OrdersGrid"
                                    ImageUrl="~/images/plus.png" CommandArgument="Show" />
                                <asp:Panel ID="pnlOrders" runat="server" Visible="false" Style="position: relative">
                                    <asp:GridView ID="gvOrders" runat="server" AutoGenerateColumns="false" PageSize="50"
                                        AllowPaging="true" OnPageIndexChanging="OnOrdersGrid_PageIndexChanging" CssClass="ChildGrid">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgProductsShow" runat="server" OnClick="Show_Hide_ProductsGrid"
                                                        ImageUrl="~/images/plus.png" CommandArgument="Show" />
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="pnlProducts" runat="server" Visible="true" Style="position: relative">
                                                                <asp:GridView ID="gvProducts" runat="server" ShowFooter="true" CellPadding="4" ForeColor="#333333"
                                                                    GridLines="None" Font-Size="13px" Width="100%" AutoGenerateColumns="false" CssClass="Nested_ChildGrid">
                                                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkAll1" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll1_CheckedChanged1" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkAllchild" AutoPostBack="true" runat="server" OnCheckedChanged="chkAllchild_CheckedChanged">
                                                                                </asp:CheckBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Tool No
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label1" runat="Server" Text='<%# Eval("tool_no") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Station
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label2" runat="Server" Text='<%# Eval("station") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Position No
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="Label3" runat="server" Text='<%# Eval("p_no") %>'></asp:LinkButton>
                                                                                <cc1:PopupControlExtender ID="PopupControlExtender1" runat="server" PopupControlID="Panel5"
                                                                                    TargetControlID="Label3" DynamicContextKey='<%# Eval("p_no") + ";" + Eval("tool_no") + ";" + Eval("station") + ";" + Eval("part_name") %>'
                                                                                    DynamicControlID="Panel5" DynamicServiceMethod="GetDynamicContent" Position="Bottom">
                                                                                </cc1:PopupControlExtender>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="200px" ItemStyle-Width="200px"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Part Name
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label4" runat="Server" Text='<%# Eval("part_name") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Total Quantity Sent
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label5" runat="Server" Text='<%# Eval("quant_sent") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                Total Received Quantity
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label6" runat="Server" Text='<%# Eval("quant_rec") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                            <HeaderTemplate>
                                                                                New Qty Received
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="TextBox2" runat="server" ViewStateMode="Enabled" onkeypress="return isNumberKey(event)"
                                                                                    AutoPostBack="true" Enabled="false" Width="100px" Text='<%# Eval("SentQty") %>'></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="center" HeaderStyle-Width="150px"
                                                                            ItemStyle-Width="150px" ItemStyle-HorizontalAlign="center">
                                                                            <HeaderTemplate>
                                                                                Upl Qty
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label6Upload_Qty" runat="Server" Text='<%# Eval("Upload_Qty") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="200px"
                                                                            ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                Sent Qty
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label6sentqty" runat="Server" Text='<%# Eval("SentQty") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:Button ID="btnRecieved" runat="server" Text="Recieved" Width="90px" OnClick="btnRecieved_Click" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="200px"
                                                                            ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                Rework No
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblReworkNo" runat="Server" Text='<%# Eval("Rwk") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <%--     <asp:TemplateField></asp:TemplateField>--%>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <asp:Panel ID="Panel5" runat="server">
                                                                </asp:Panel>
                                                            </asp:Panel>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField ItemStyle-Width="200px" DataField="Challan_No" HeaderText="Challan_No" />
                                            <asp:BoundField ItemStyle-Width="300px" DataField="location" HeaderText="location" />
                                            <asp:BoundField ItemStyle-Width="100px" DataField="station" HeaderText="Station" />
                                            <asp:BoundField ItemStyle-Width="100px" DataField="qtypending" HeaderText="qtypending" />
                                            <asp:BoundField ItemStyle-Width="100px" DataField="tool_no" HeaderText="tool_no" />
                                            <asp:BoundField ItemStyle-Width="100px" DataField="Rwk" HeaderText="Rwk No" />
                                            <asp:BoundField ItemStyle-Width="150px" DataFormatString="{0:dd/MM/yyyy}" DataField="Sent_Received_Date"
                                                HeaderText="Sent Date" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Panel ID="Panel4" runat="server">
                                    </asp:Panel>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ItemStyle-Width="150px" DataField="tool_no" HeaderStyle-BackColor="Green"
                            HeaderText="ToolNo" />
                        <asp:BoundField ItemStyle-Width="400px" DataField="location" HeaderStyle-BackColor="Green"
                            HeaderText="Vendor" />
                        <%-- <asp:BoundField ItemStyle-Width="150px" DataField="part_name" HeaderText="Part Name" />--%>
                        <asp:BoundField ItemStyle-Width="300px" DataField="qtypending" HeaderStyle-BackColor="Green"
                            HeaderText="PendingQty" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
        </tr>
    </table>
</asp:Content>
