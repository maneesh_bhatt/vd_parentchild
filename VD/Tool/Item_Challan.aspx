﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Item_Challan.aspx.cs" Inherits="GateInPrintGR_Item_Challan" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <script type="text/javascript" language="JavaScript">

    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="css.css" rel="stylesheet" type="text/css" />
</head>
<body onload="timer=setTimeout('move()',50000)">
    <form runat="server" id="frm1">
    <div id="wraper">
        <div id="main ">
            <div class="box_contner1">
                <div class="logo">
                    <asp:Image ID="Image1" ImageUrl="~/images/FALCON LOGO quarter.jpg" runat="server" /></div>
                <div class="textmed">
                    Falcon AutoTech PVT. LTD<br />
                    Works: Plot No. 308-309,Eco tech Ext-1, Greater Noida,Dist Gautam Budh Nagar,U.P
                    PIN-201308<br />
                    E-mail falconautotech@hotmail.com&nbsp;
                                       <br /> &nbsp;<div style="margin-top: 12px;">
                        <asp:Label runat="server" ID="lblPageTitle"></asp:Label></div>
                </div>
                <div class="textmedR">
                    <asp:Label runat="server"  ID="LblDt"></asp:Label><br />
                    
                    <br />
                </div>
               
            </div>
           
            <div class="box_contner mrgnt">
             
                <div class="cont_box">
                    <div class="cont_boxin" style="margin-top: 30px;">
                        
                        <div class="cl">
                       
                    <strong> Challan No- </strong>  <asp:Label ID="lblMino" runat="server" Text=""></asp:Label>
                    <br />
                     <strong> Auto Generated Challan No- </strong>
                     <asp:Label ID="lblAutoChallanNumber" runat="server" Text=""></asp:Label>
                     
                      <br />
                    <strong> PO No-</strong> <asp:Label ID="lblpono" runat="server" Text="Label"></asp:Label> <br />
                            <strong>Vendor Name: <asp:Label ID="lblvendor" Font-Bold="false" runat="server"></asp:Label>
                            </strong><br />
                            <strong>SR.Reg. No: <asp:Label ID="Srnoofreg" Font-Bold="false" runat="server"></asp:Label><br />
                            Expected Comm. Date:   <asp:Label ID="lblexpextedcommiteddate" Font-Bold="false" runat="server"></asp:Label><br />
                            Delivery Comm. Date:   <asp:Label ID="lbldelcomdate" Font-Bold="false" runat="server"></asp:Label><br />
                            Required Date:  <asp:Label ID="lblregdate" Font-Bold="false" runat="server"></asp:Label><br />
                            <asp:Label runat="server" ID="lblSentRecDateHeading"></asp:Label> <asp:Label ID="lblsenddate" Font-Bold="false" runat="server"></asp:Label>
                           <br />
                            </strong><br />
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
        <div class="box_contner1">
        
        <p style="width: 100%; float: left;margin-bottom:10px;"> <asp:Label runat="server" ID="lblContent"></asp:Label>   </p>
            <asp:GridView Width="800px" runat="server" AutoGenerateColumns="false" ID="Grdview1" ShowFooter="true" OnRowDataBound="Grdview1_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            Sr.No.
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Lblsr" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                           Project No
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblmake" Text='<%# Eval("tool_no") %>' Width="80px" runat="server"></asp:Label></ItemTemplate>
                             <FooterTemplate>
                            <div style="padding:0 0 5px 0"><asp:Label ID="lblTotal2" runat="server" /></div>
                            <div><asp:Label ID="lblGrandTotal1" runat="server" /></div>
                           </FooterTemplate>
                    </asp:TemplateField>

                      <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                           station
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSPECIFICATION" Text='<%# Eval("station") %>' Width="60px" runat="server"></asp:Label></ItemTemplate>
                             <FooterTemplate>
                            <div style="padding:0 0 5px 0"><asp:Label ID="lblTotal1" runat="server" /></div>
                            <div><asp:Label ID="lblGrandTotal2" runat="server" /></div>
                           </FooterTemplate>
                    </asp:TemplateField>

                           


                      <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                          Rework No.
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblsub_head1" Text='<%# Eval("rwk_no") %>' runat="server"></asp:Label></ItemTemplate>
                             <FooterTemplate>
                            <div style="padding:0 0 5px 0"><asp:Label ID="lblTotal3" runat="server" /></div>
                            <div><asp:Label ID="lblGrandTotal3" runat="server" /></div>
                           </FooterTemplate>
                    </asp:TemplateField>


                     <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                           Batch No
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblsub_head2" Text='<%# Eval("Batch_No") %>' runat="server"></asp:Label></ItemTemplate>
                             <FooterTemplate>
                            <div style="padding:0 0 5px 0"><asp:Label ID="lblTotal4" runat="server" /></div>
                            <div><asp:Label ID="lblGrandTotal4" runat="server" /></div>
                           </FooterTemplate>
                    </asp:TemplateField>
             
                  <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            Position No
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblitemdesc" Text='<%# Eval("p_no") %>' Width="90px" runat="server"></asp:Label></ItemTemplate>
                             <FooterTemplate>
                            <div style="padding:0 0 5px 0;"><asp:Label ID="lblTotal5" runat="server" /></div>
                            <div><asp:Label ID="lblGrandTotal5" runat="server" /></div>
                           </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            Part Name
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblmodelno" Text='<%# Eval("part_name") %>' style="word-break: break-all;" Width="200px" runat="server"></asp:Label></ItemTemplate>
                             <FooterTemplate>
                            <div style="padding:0 0 5px 0;color:Blue;"><asp:Label ID="lblTotal6" Text="Total Qty" runat="server" /></div>
                            <div><asp:Label ID="lblGrandTotal6" runat="server" /></div>
                           </FooterTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            Estimated Time(hh:mm) 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEstimatedTime" style="word-break: break-all;" Width="70px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            Actual Time(hh:mm)
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblActualTime"  style="word-break: break-all;" Width="70px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                           Inspected By
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblInspectedBy" style="word-break: break-all;" Width="110px" runat="server"></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                       <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                         <asp:Label runat="server" Id="lblQtyHeading"></asp:Label>   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTransQty"  runat="server"></asp:Label></ItemTemplate>
                             <FooterTemplate>
                            <div style="padding:0 0 5px 0;  color:Blue; "><asp:Label ID="lblTotal"  runat="server" /></div>
                            <div style="padding:0 0 5px 0;  color:Blue; "><asp:Label ID="lblGrandTotal" runat="server" /></div>
                           </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
   
    <div class="box_contner1">
        
        <div style="vertical-align="Top">
            <br />
            <br />
            <br />
            Approver1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            Approver2 Signature&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Signature&nbsp;&nbsp;&nbsp; <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
    </div>
    </form>
</body>
</html>
