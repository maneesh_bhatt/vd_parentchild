﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Tool_Item_Waiting_for_process : System.Web.UI.Page
{
    int count = 0;
    String p_no = "";
    int tool_no, quantity, Qtysent;
    int countchk = 0;
    string approvername = "";
    object Approveid = "";
    string str_dt1 = "";
    string part_name, station, location, str_dt;
    object checkqty = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        txtpodate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
        if (!IsPostBack)
        {
            txtPONO.Enabled = false;
            string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name<>'Assembly' and Vendor_Name<> ' Inspection' and Vendor_Name <> 'Weld Parts Consumed' order by Vendor_Name asc ";
            string dataTextField = "VENDOR_NAME";
            string dataValueField = "ID";

            if (tbTool.Text.Length > 0)
            {
                showgrid();
                Panel2.Visible = false;

                UtilityFunctions.bind_vendor(ddlvendor, query, dataTextField, dataValueField);
                GridView1.Columns[9].Visible = false;
                GridView1.Columns[10].Visible = false;
                ddlvendor.Enabled = false;
            }
            GridView1.Columns[9].Visible = true;
            GridView1.Columns[8].Visible = true;
            GridView1.Columns[10].Visible = false;
            ddlvendor.Enabled = false;
            UtilityFunctions.bind_vendor(ddlvendor, query, dataTextField, dataValueField);

            Panel3.Visible = false;
            Panel2.Visible = false;
            tbStation.Visible = false;
            tbTool.Visible = false;
            btnSearch.Visible = false;
        }
        else
        {
            if (rdbAutomatic.Checked)
            {
                loadAutomaticPanel();
            }
        }
    }

    private void loadAutomaticPanel()
    {
        string s = "";
        int quant_rec = 0;
        string command = "insert";
        int first_location_sent = 0;
        int quant_others = 0;
        DataTable dt = new DataTable();

        if (txttoolno.Text.Length > 0)
        {
            if (ViewState["toolno"].ToString().Length > 0 && ViewState["stationno"].ToString().Length > 0 && ViewState["position"].ToString().Length > 0)
            {
                string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
                SqlConnection conn = new SqlConnection(sqlconnstring);

                SqlTransaction trans = null;
                SqlCommand cmd = new SqlCommand();
                try
                {
                    conn.Open();
                    trans = conn.BeginTransaction();
                    cmd.Transaction = trans;
                    string toolno = ViewState["toolno"].ToString();
                    string stationno = ViewState["stationno"].ToString();
                    string position = ViewState["position"].ToString();
                    string Qty = ViewState["Qty"].ToString();
                    string TotalQty = ViewState["TotalQty"].ToString();
                    string part_name = ViewState["ItemName"].ToString();
                    string vendor = ViewState["vendor"].ToString();
                    Int32 sentQty = Convert.ToInt32(ViewState["SENT"].ToString());
                    Int32 totqty = Convert.ToInt32(TotalQty);
                    Int32 sendQty = Convert.ToInt32(txtQty.Text);
                    if (totqty > 0)
                    {
                        if ((Convert.ToInt32(sendQty)) > totqty)
                        {
                            //If sendQuantity is more than the total quantity, show error message.
                            MessageBox("Qty Should be less than total Qty");
                            return;
                        }

                        Int32 qtysent1 = Convert.ToInt32(Qtysent);

                        string strQuery4 = "update part_history_gr  set quantity=(quantity - " + qtysent1 + ") where p_no='" + position + "' and location='COMPANY' and part_name='" + part_name + "' ";
                        cmd.CommandText = strQuery4;
                        cmd.ExecuteNonQuery();

                        //Combined the queries strQuery2 and strQuery3 can be combined.
                        string strQuery2 = "update part_history_gr  set  QtySent=" + sendQty + " + isnull(QtySent,0) where p_no='" + position + "' and (location='COMPANY' or location='" + vendor + "') and part_name='" + part_name + "' ";
                        cmd.CommandText = strQuery2;
                        cmd.ExecuteNonQuery();

                        //FIXME :: Move this code into some function insertIntoPartHistoryGR()
                        DateTime str_dt = new DateTime();
                        str_dt = DateTime.Now;
                        s = "insert into part_history_gr(p_no,tool_no,station,location,quantity,part_name,QtySent,date_of_trans,sent_or_rec,Explain_Date,Delivery_commit_date,Challan_No,Sr_Reg_No,Required_date) values     (@p_no,@tool_no,@station,@location,@quantity,@ItemName,@Qtysent,@date_of_trans,@sent_or_rec)";
                        cmd.CommandText = s;
                        cmd.Parameters.AddWithValue("@p_no", position);
                        cmd.Parameters.AddWithValue("@tool_no", toolno);
                        cmd.Parameters.AddWithValue("@station", stationno);
                        cmd.Parameters.AddWithValue("@location", vendor);
                        cmd.Parameters.AddWithValue("@quantity", totqty);
                        cmd.Parameters.AddWithValue("@ItemName", part_name);
                        cmd.Parameters.AddWithValue("@Qtysent", sendQty);
                        cmd.Parameters.AddWithValue("@sent_or_rec", "Sent");
                        cmd.Parameters.AddWithValue("@date_of_trans", str_dt);
                        cmd.Parameters.AddWithValue("@Explain_Date", DateTime.Now.AddDays(3));
                        cmd.Parameters.AddWithValue("@Delivery_commit_date", DBNull.Value);
                        cmd.Parameters.AddWithValue("@Challan_No", txtchalanno.Text);
                        cmd.Parameters.AddWithValue("@Sr_Reg_No", txtSRREGNO.Text);
                        cmd.Parameters.AddWithValue("@Required_date", txtrequired.Text);
                        cmd.ExecuteNonQuery();

                        //FIXME :: selectFromLocationInfoGR()
                        s = "select quant_sent,quant_rec from location_info_gr where location=@location and tool_no=@tool_no and station=@station and p_no=@p_no and part_name=@part_name ";
                        cmd.CommandText = s;
                        cmd.Parameters.AddWithValue("@tool_no", toolno);
                        cmd.Parameters.AddWithValue("@p_no", position);
                        cmd.Parameters.AddWithValue("@station", stationno);
                        cmd.Parameters.AddWithValue("@part_name", part_name);
                        cmd.Parameters.AddWithValue("@location", vendor);

                        dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());

                        if (dt.Rows.Count != 0)
                        {
                            command = "update";
                            quantity += Convert.ToInt32(dt.Rows[0][0].ToString());
                            quant_rec = Convert.ToInt32(dt.Rows[0][1].ToString());
                        }

                        //CALCULATING THE PENDING QUANTITY
                        int quant_pend = quantity - quant_rec;

                        //FINDING QUANTITY PENDING WITH PREVIOUS VENDOR

                        //FINDING FIRST VENDOR  and QUANTITY SENT TO HIM
                        s = "select top 1 location from part_history_gr where tool_no=@tool_no and station=@station and p_no=@p_no and part_name=@part_name  order by date_of_trans asc";
                        cmd.CommandText = s;
                        cmd.Parameters.AddWithValue("@p_no", position);
                        cmd.Parameters.AddWithValue("@part_name", part_name);
                        cmd.Parameters.AddWithValue("@tool_no", toolno);
                        cmd.Parameters.AddWithValue("@station", stationno);
                        cmd.Parameters.AddWithValue("@sent_or_rec", "Sent");
                        dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());

                        if (dt.Rows.Count > 0)
                        {
                            string first_location = dt.Rows[0][0].ToString();
                            if (!first_location.Equals(location))
                            {
                                s = "select quant_sent from location_info_gr where tool_no=@tool_no and station=@station and p_no=@p_no and part_name=@part_name and location=@location";
                                cmd.CommandText = s;
                                cmd.Parameters.AddWithValue("@p_no", position);
                                cmd.Parameters.AddWithValue("@part_name", part_name);
                                cmd.Parameters.AddWithValue("@tool_no", toolno);
                                cmd.Parameters.AddWithValue("@station", stationno);
                                cmd.Parameters.AddWithValue("@location", first_location);
                                dt = new DataTable();
                                dt.Load(cmd.ExecuteReader());
                                if (dt.Rows.Count > 0)
                                {
                                    first_location_sent = Convert.ToInt32(dt.Rows[0][0].ToString());
                                    //finding quantity pending with first vendor
                                    quant_others = first_location_sent - quantity;
                                }
                            }
                        }

                        //NOW INSERTING INTO LOCATION_INFO TABLE
                        if (command.Equals("insert"))
                        {
                            s = "insert into location_info_gr(tool_no,station,p_no,part_name,req_qty,location,quant_sent,quant_rec,quant_pend,quant_others,Explain_Date,Delivery_commit_date,Challan_No,Sr_Reg_No,Required_date) values(" + Convert.ToInt32(toolno) + ",'" + stationno.ToString() + "'," + Convert.ToInt32(position) + ",'" + part_name.ToString() + "'," + Convert.ToInt32(totqty) + ",'" + vendor.ToString() + "'," + Convert.ToInt32(sendQty) + "," + Convert.ToInt32(quant_rec) + "," + Convert.ToInt32(quant_pend) + "," + Convert.ToInt32(quant_others) + ",'" + DateTime.Now.AddDays(3).ToString("yyyy/MM/dd HH:mm:ss") + "','" + DBNull.Value + "','" + txtchalanno.Text + "','" + txtSRREGNO.Text + "','" + txtrequired.Text + "')";
                        }
                        else
                        {
                            //string strQuery2 = "update part_history  set  QtySent=" + sendQty + " + isnull(QtySent,0) where p_no='" + position + "' and location='COMPANY' and part_name='" + part_name + "' ";
                            s = "update location_info_gr set quant_sent=" + sendQty + " + isnull(quant_sent,0) where tool_no=" + Convert.ToInt32(toolno.ToString()) + " and station='" + stationno + "' and p_no=" + Convert.ToInt32(position.ToString()) + " and part_name='" + part_name.ToString() + "' and location='" + vendor + "'";
                        }
                        cmd.CommandText = s;
                        cmd.ExecuteNonQuery();

                        //TODO :: Log if insertion succesful or not.
                    }
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
                    ex.ToString();
                }
                finally
                {
                    conn.Close();
                }
            }
        }
    }

    /// <summary>
    /// Binds the Data which has been uploaded but have not moved at any location from Part History Gr table.
    /// The data can be filtered by the given filters like Tool No, Station, Rework Number etc
    /// </summary>
    public void showgrid()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();

        clear();

        string query = "";
        try
        {
            query = " select phg.p_no,phg.part_name,phg.location,phg.date_of_trans,phg.tool_no,phg.Sent_Date,phg.Received_Date,phg.Batch_No, (isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0)) as PendingQty,(isnull(phg.Upload_Qty,0)) as Upload_Qty, isnull(phg.QtySent,0) as QtySent,phg.quantity,phg.station,phg.date_of_trans,phg.rwk_no,pgr.opr17,pgr.Parent_Positions, pgr.Child_Positons,pgr.Matl,pgr.Hrc,pgr.Status,pgr.Type,pgr.Gr_Reference_No,pgr.req_qty,pgr.version_number from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where  phg.location='COMPANY' and phg.date_of_trans is not null and  (phg.Sent_Date is null  or  phg.Received_Date is null)  and  phg.quantity>0 and ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) and pgr.Is_Active_Record=1";
            if (tbTool.Text.Length > 0)
            {
                query += " and phg.tool_no= '" + tbTool.Text.Trim() + "'";
            }
            if (txtbatchno.Text.Length > 0)
            {
                //FIXME : this is to be the toolno condition or batch no condition
                query += " AND phg.batch_no= '" + txtbatchno.Text.Trim() + "'";
            }
            if (txtreworkno.Text.Length > 0)
            {
                query += " AND phg.rwk_no= '" + txtreworkno.Text.Trim() + "'";
            }
            if (tbStation.Text.Length > 0)
            {
                query += "AND phg.station= '" + tbStation.Text.Trim() + "'";
            }
            if (ddlvendor.SelectedItem.Text == "GR VD FINISH STORE")
            {
                query += " and (pgr.Status='InActive' or pgr.Status='Old')";
            }
            query += " order by phg.tool_no,phg.station,phg.p_no asc";

            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = conn;
            DataTable dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            if (dt.Rows.Count > 0)
            {
                btnSend.Visible = false;
                //btnprint.Visible = false;
                ddlvendor.Enabled = true;
                GridView1.DataSource = dt;
                Session["DataTableAdvanceReport"] = dt;
                GridView1.DataBind();
            }
            else
            {
                //btnprint.Visible = false;
                btnSend.Visible = false;
                //ddlvendor.Enabled = false;
                dt = null;
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    /// <summary>
    /// When header is checked, this method is called. It shows the Checkbox on each row as selected. Also makes the Controls Enabled if Checked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAll1_CheckedChanged1(object sender, EventArgs e)
    {
        txtPONO.Text = String.Empty;
        CheckBox ChkBoxHeader = (CheckBox)GridView1.HeaderRow.FindControl("chkAll1");
        if (ddlvendor.SelectedItem.Text == "" && ChkBoxHeader.Checked || radiobtnListChallanType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please select Vendor Name/Challan Type before selecting any Position.');", true);
            ChkBoxHeader.Checked = false;
            return;
        }
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            TextBox TextBox3 = (TextBox)row.FindControl("TextBox3");
            TextBox txtbalc = (TextBox)row.FindControl("txtbalc");
            Label Label8 = (Label)row.FindControl("Label8");
            Label Labelsent = (Label)row.FindControl("Labelsent");
            Label uploadqty = (Label)row.FindControl("LabelUpload_Qty");
            TextBox txtPOAmount = (TextBox)row.FindControl("txtPOAmount");
            if (ChkBoxHeader.Checked)
            {
                if (radiobtnListChallanType.SelectedValue == "Purchase Order" || radiobtnListChallanType.SelectedValue == "Job Work")
                {
                    txtPOAmount.Enabled = true;
                }
                else
                {
                    txtPOAmount.Enabled = false;
                }
                ChkBoxRows.Checked = true;
                countchk = countchk + 1;
            }
            else
            {
                txtPOAmount.Enabled = false;
                ChkBoxRows.Checked = false;
            }
            EnableTextBox(ChkBoxRows);
        }
        if (countchk > 0)
        {
            txtPONO.Enabled = true;
            GridView1.Columns[10].Visible = true;
            GridView1.Columns[11].Visible = true;
            btnSend.Visible = true;
        }
        else
        {
            txtPONO.Enabled = false;
            btnSend.Visible = false;
            GridView1.Columns[10].Visible = false;
            GridView1.Columns[11].Visible = false;
        }
        if (!Convert.ToBoolean(ViewState["IsActiveVendor"]))
        {
            btnSend.Visible = false;
        }
        updatePO.Update();
        UpdatePanel1.Update();
    }

    /// <summary>
    /// This event is called when user presses check on the checkbox at any row.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAllchild_CheckedChanged(object sender, EventArgs e)
    {
        txtPONO.Text = String.Empty;
        txtchalanno.Text = String.Empty;
        CheckBox ChkBoxRows = (CheckBox)sender;
        TextBox txtPOAmount = (TextBox)ChkBoxRows.FindControl("txtPOAmount");
        if (ChkBoxRows.Checked == true)
        {
            if (ddlvendor.SelectedItem.Text == "" || radiobtnListChallanType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please select Vendor Name/Challan Type before selecting any Position.');", true);
                ChkBoxRows.Checked = false;
                return;
            }
            if (radiobtnListChallanType.SelectedValue == "Purchase Order" || radiobtnListChallanType.SelectedValue == "Job Work")
            {
                txtPOAmount.Enabled = true;
            }
            else
            {
                txtPOAmount.Enabled = false;
            }

            GridView1.Columns[8].Visible = true;
            GridView1.Columns[9].Visible = true;
            btnSend.Visible = true;
        }
        else
        {
            txtPOAmount.Enabled = false;
            ChkBoxRows.Checked = false;
        }
        EnableTextBox(ChkBoxRows);

        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox chkCheckBoxChecked = (CheckBox)GridView1.Rows[row.RowIndex].FindControl("chkAllchild");
            if (chkCheckBoxChecked.Checked)
            {
                countchk = countchk + 1;
            }
        }

        if (countchk > 0)
        {
            txtPONO.Enabled = true;
            GridView1.Columns[10].Visible = true;
            GridView1.Columns[11].Visible = true;
            btnSend.Visible = true;
        }
        else
        {
            txtPONO.Enabled = false;
            btnSend.Visible = false;
            GridView1.Columns[10].Visible = false;
            GridView1.Columns[11].Visible = false;
        }
        if (!Convert.ToBoolean(ViewState["IsActiveVendor"]))
        {
            btnSend.Visible = false;
        }
        updatePO.Update();
        UpdatePanel1.Update();
    }

    private void clear()
    {
        txtrequired.Text = "";
        txtpodate.Text = "";
        txtSRREGNO.Text = "";
    }

    /// <summary>
    /// This method make the controls enabled if it is checked. Also validates for Valid Quantity and shows the Valid quantity on Check
    /// </summary>
    /// <param name="cb"></param>
    protected void EnableTextBox(CheckBox cb)
    {
        int count = int.Parse(GridView1.Rows.Count.ToString());
        TextBox TextBox3 = (TextBox)cb.FindControl("TextBox3");
        TextBox txtbalc = (TextBox)cb.FindControl("txtbalc");
        Label Label8 = (Label)cb.FindControl("Label8");
        Label Labelsent = (Label)cb.FindControl("Labelsent");
        Label LabelUpload_Qty = (Label)cb.FindControl("LabelUpload_Qty");
        TextBox txtHSNCode = (TextBox)cb.FindControl("txtHSNCode");
        TextBox txtWeight = (TextBox)cb.FindControl("txtWeight");
        DropDownList ddlWeightUnit = (DropDownList)cb.FindControl("ddlWeightUnit");

        Label lblStation = cb.FindControl("Label10") as Label;
        Label lblrew = cb.FindControl("lblReworkNo") as Label;
        Label lblToolNo = cb.FindControl("lblproject") as Label;
        LinkButton lnkbtnPNo = cb.FindControl("Label1") as LinkButton;
        Label lblPartName = cb.FindControl("Label2") as Label;
        HiddenField hdnNotRequired = cb.FindControl("hdnNotRequired") as HiddenField;

        if (cb.Checked == true)
        {
            txtHSNCode.Enabled = true;
            txtWeight.Enabled = true;
            ddlWeightUnit.Enabled = true;
            txtbalc.Text = (Convert.ToInt32(Label8.Text) - Convert.ToInt32(TextBox3.Text)).ToString();
            txtbalc.Text = ((Convert.ToInt32(LabelUpload_Qty.Text) - (Convert.ToInt32(TextBox3.Text) + Convert.ToInt32(Labelsent.Text)))).ToString();
            TextBox3.Enabled = true;

            int notReqQty = (hdnNotRequired.Value == "") ? 0 : Convert.ToInt32(hdnNotRequired.Value);
            int possibleQtyForSend = 0;
            int balanceQty = 0;
            int? availableAndValidQty = ValidateQuantities(lblToolNo.Text, lblStation.Text, lblPartName.Text, lnkbtnPNo.Text, lblrew.Text, notReqQty, Convert.ToInt32(Label8.Text), Convert.ToInt32(TextBox3.Text), out possibleQtyForSend, out balanceQty);
            if (availableAndValidQty != null)
            {
                TextBox3.Text = Convert.ToString(possibleQtyForSend);
                txtbalc.Text = Convert.ToString(balanceQty);
            }
            //TextBox3.Enabled = true;
            //TextBox3.Visible = true;
        }
        else
        {
            txtHSNCode.Enabled = false;
            txtWeight.Enabled = false;
            ddlWeightUnit.Enabled = false;
            TextBox3.Enabled = false;
            TextBox3.Text = "0";
        }
    }

    /// <summary>
    /// This event is called when user enters some text on Quantity To be send TextBox. It Validates for Valid Quantity
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TextBox3_TextChanged(object sender, EventArgs e)
    {
        TextBox txtQtyToBeSent = (TextBox)sender;
        TextBox TextBox3 = (TextBox)txtQtyToBeSent.FindControl("TextBox3");
        TextBox txtbalc = (TextBox)txtQtyToBeSent.FindControl("txtbalc");
        Label Label8 = (Label)txtQtyToBeSent.FindControl("Label8");
        Label Labelsent = (Label)txtQtyToBeSent.FindControl("Labelsent");
        Label uploadqty = (Label)txtQtyToBeSent.FindControl("LabelUpload_Qty");

        Label lblStation = txtQtyToBeSent.FindControl("Label10") as Label;
        Label lblrew = txtQtyToBeSent.FindControl("lblReworkNo") as Label;
        Label lblToolNo = txtQtyToBeSent.FindControl("lblproject") as Label;
        LinkButton lnkbtnPNo = txtQtyToBeSent.FindControl("Label1") as LinkButton;
        Label lblPartName = txtQtyToBeSent.FindControl("Label2") as Label;
        HiddenField hdnNotRequired = txtQtyToBeSent.FindControl("hdnNotRequired") as HiddenField;

        if (TextBox3.Enabled)
        {
            bool isSelected = (txtQtyToBeSent.FindControl("chkAllchild") as CheckBox).Checked;
            if (isSelected == true && TextBox3.Text != "")
            {
                if (Convert.ToInt32(TextBox3.Text) > 0)
                {
                    if (String.IsNullOrEmpty(Label8.Text))
                        Label8.Text = "0";
                    if (String.IsNullOrEmpty(TextBox3.Text))
                        TextBox3.Text = "0";
                    if (String.IsNullOrEmpty(Labelsent.Text))
                        Labelsent.Text = "0";
                    if (String.IsNullOrEmpty(uploadqty.Text))
                        uploadqty.Text = "0";

                    int notReqQty = (hdnNotRequired.Value == "") ? 0 : Convert.ToInt32(hdnNotRequired.Value);
                    int possibleQtyForSend = 0, balanceQty;
                    int? availableAndValidQty = ValidateQuantities(lblToolNo.Text, lblStation.Text, lblPartName.Text, lnkbtnPNo.Text, lblrew.Text, notReqQty,
                        Convert.ToInt32(Label8.Text), Convert.ToInt32(TextBox3.Text), out possibleQtyForSend, out balanceQty);
                    if (availableAndValidQty != null)
                    {
                        TextBox3.Text = Convert.ToString(possibleQtyForSend);
                        txtbalc.Text = Convert.ToString(balanceQty);
                    }
                }
                else
                {
                    txtbalc.Text = Convert.ToString(Label8.Text);
                }
            }
            else
            {
                txtbalc.Text = Convert.ToString(Label8.Text);
                TextBox3.Text = Convert.ToString("0");
            }
        }
    }


    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    /// <summary>
    /// Checks if Column in Active or Inactive and displays Active / Inactive based on New Version Uploaded or not.
    /// Calculates and display the Pending Not Required Quantity, In case of Vendor Type Not Required/ GR VD Finish Store shows and hides the rows.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            int totalNotRequiredQty = 0;
            HiddenField hdnNotRequired = (HiddenField)e.Row.FindControl("hdnNotRequired");
            Label lblPendingNotReq = (Label)e.Row.FindControl("lblPendingNotReq");
            Label lblStatus = (Label)e.Row.FindControl("lblStatus");
            string referenceGRNo = "", reworkNoOfGrAgainstRND = "";
            using (SqlConnection connec = new SqlConnection(sqlconnstring))
            {
                connec.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connec;
                //2 cases R&D and GR Against R&D to be considered. In other the pending not required qunatity will be shown as it is
                string type = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Type"));

                if (type == "New" || type == "Modification")
                {
                    //means gr against R&D
                    // referenceGRNo = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Gr_Reference_No"));
                }
                else if (type == "R&D")
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "select rwk_no from parts_Gr where Gr_Reference_No=@RwkNo and tool_no=@ToolNo and station=@Station and p_no=@PNo and Type in ('New','Modification')";
                    cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                    cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                    cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                    cmd.Parameters.AddWithValue("@RwkNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            reworkNoOfGrAgainstRND = Convert.ToString(rdr["rwk_no"]);
                        }
                    }
                    string lastVersion = "";
                    cmd.Parameters.Clear();
                    cmd.CommandText = "select top 1 Version_Number from parts_Gr where tool_no=@ToolNo and p_no=@PNo and station=@Station order by Version_Number Desc;";
                    cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                    cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                    cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            lastVersion = Convert.ToString(rdr["Version_Number"]);
                        }
                    }

                    cmd.Parameters.Clear();
                    cmd.CommandText = "select top 1 Version_Number from parts_Gr where Status='Active' and tool_no=@ToolNo and p_no=@PNo and station=@Station";
                    cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                    cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                    cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            string activeVersion = Convert.ToString(rdr["Version_Number"]);
                            if ((activeVersion == Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Version_Number"))) || (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Version_Number")) == lastVersion))
                            {
                                lblStatus.Text = "Active";
                            }
                        }
                    }
                }
                cmd.Parameters.Clear();
                //calculating total uploaded not required quantity for current row
                cmd.CommandText = "select Sum(req_qty) as NotRequiredQty from parts_Gr where tool_no=@ToolNo and p_no=@PNo and part_name=@PartName and station=@Station and Gr_Reference_No=@GrReferenceNo and Type='NotRequired'";
                cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                cmd.Parameters.AddWithValue("@PartName", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Part_Name")));
                cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                cmd.Parameters.AddWithValue("@GrReferenceNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.HasRows)
                    {
                        rdr.Read();
                        if (rdr["NotRequiredQty"] != DBNull.Value)
                            totalNotRequiredQty = Convert.ToInt32(rdr["NotRequiredQty"]);
                    }
                }

                int qtySentToNotRequiredLocation = 0;
                int reqQty = 0;
                //calculating quantity sum that are sent to not required location - For R&D
                if (type == "R&D")
                {
                    reqQty = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "req_qty"));

                    cmd.Parameters.Clear();
                    cmd.CommandText = "select isnull(isnull(quant_sent,0)- isnull(quant_rec,0),0) as QtyPending from location_info_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and part_name=@PartName and rwk=@RwkNo and location='Not Required'";
                    cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                    cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                    cmd.Parameters.AddWithValue("@PartName", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Part_Name")));
                    cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                    cmd.Parameters.AddWithValue("@RwkNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            if (rdr["QtyPending"] != DBNull.Value)
                                qtySentToNotRequiredLocation = Convert.ToInt32(rdr["QtyPending"]);
                        }
                    }

                    if (totalNotRequiredQty > 0)
                    {
                        int totalNotReq = Convert.ToInt32(totalNotRequiredQty);
                        int validPossibleNotRequired = 0;
                        if (totalNotReq >= reqQty)
                        {
                            validPossibleNotRequired = reqQty;
                        }
                        else
                        {
                            validPossibleNotRequired = totalNotReq;
                        }
                        hdnNotRequired.Value = Convert.ToString(validPossibleNotRequired);
                        int pendingNotReq = validPossibleNotRequired - qtySentToNotRequiredLocation;
                        if (pendingNotReq > 0)
                            lblPendingNotReq.Text = Convert.ToString(pendingNotReq);



                        int remainingActiveQty = 0;
                        int totalQty = 0;
                        int totalNotReqQty = 0;
                        string query = "select Sum(Req_Qty) as UploadedQty from parts_gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and Rwk_No=@RwkNo";
                        using (SqlDataAdapter adActiveQty = new SqlDataAdapter(query, connec))
                        {
                            adActiveQty.SelectCommand.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                            adActiveQty.SelectCommand.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                            adActiveQty.SelectCommand.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                            adActiveQty.SelectCommand.Parameters.AddWithValue("@RwkNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                            DataTable dtActiveQty = new DataTable();
                            adActiveQty.Fill(dtActiveQty);
                            if (dtActiveQty.Rows.Count > 0)
                            {
                                if (dtActiveQty.Rows[0]["UploadedQty"] != DBNull.Value)
                                    totalQty = Convert.ToInt32(dtActiveQty.Rows[0]["UploadedQty"]);

                            }

                            using (SqlDataAdapter adNotRequired = new SqlDataAdapter("select Sum(Req_Qty) as NotReqQty from parts_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and Gr_Reference_No=@GrReferenceNo and Type=@Type", connec))
                            {
                                adNotRequired.SelectCommand.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                                adNotRequired.SelectCommand.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                                adNotRequired.SelectCommand.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                                adNotRequired.SelectCommand.Parameters.AddWithValue("@GrReferenceNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                                adNotRequired.SelectCommand.Parameters.AddWithValue("@Type", "NotRequired");
                                DataTable dtNotReq = new DataTable();
                                adNotRequired.Fill(dtNotReq);
                                if (dtNotReq.Rows.Count > 0)
                                {
                                    if (dtNotReq.Rows[0]["NotReqQty"] != DBNull.Value)
                                        totalNotReqQty = Convert.ToInt32(dtNotReq.Rows[0]["NotReqQty"]);

                                }
                            }
                            remainingActiveQty = totalQty - totalNotReqQty;

                            if (remainingActiveQty == 0)
                            {
                                lblStatus.Text = "Inactive";
                            }
                        }
                    }
                }
                else
                {
                    //Calculating qty sent to Not Required Location for current row
                    cmd.Parameters.Clear();
                    cmd.CommandText = "select isnull(isnull(quant_sent,0)- isnull(quant_rec,0),0) as QtyPending from location_info_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and part_name=@PartName and rwk=@RwkNo and location='Not Required'";
                    cmd.Parameters.AddWithValue("@ToolNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Tool_No")));
                    cmd.Parameters.AddWithValue("@PNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "P_No")));
                    cmd.Parameters.AddWithValue("@PartName", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Part_Name")));
                    cmd.Parameters.AddWithValue("@Station", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Station")));
                    cmd.Parameters.AddWithValue("@RwkNo", Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rwk_No")));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            if (rdr["QtyPending"] != DBNull.Value)
                                qtySentToNotRequiredLocation = Convert.ToInt32(rdr["QtyPending"]);
                        }
                    }

                    if (totalNotRequiredQty > 0)
                    {
                        hdnNotRequired.Value = Convert.ToString(totalNotRequiredQty);
                        int pendingNotReq = Convert.ToInt32(hdnNotRequired.Value) - qtySentToNotRequiredLocation;
                        if (pendingNotReq > 0)
                            lblPendingNotReq.Text = Convert.ToString(pendingNotReq);
                    }

                    //Change your logic here for R&D and GR against that R&D
                }
            }

            if (ddlvendor.SelectedItem.Text == "Not Required")
            {
                if (lblPendingNotReq.Text != "")
                {
                    if (Convert.ToInt32(lblPendingNotReq.Text) > 0)
                    {
                        e.Row.Visible = true;
                    }
                    else
                    {
                        e.Row.Visible = false;
                    }
                }
                else
                {
                    e.Row.Visible = false;
                }
            }
            else if (ddlvendor.SelectedItem.Text == "GR VD FINISH STORE")
            {
                if (lblStatus.Text.ToLower() == "inactive")
                {
                    if (lblPendingNotReq.Text != "")
                    {
                        if (Convert.ToInt32(lblPendingNotReq.Text) > 0)
                        {
                            e.Row.Visible = false;
                        }
                        else
                        {
                            e.Row.Visible = true;
                        }
                    }
                    else
                    {
                        e.Row.Visible = true;
                    }
                }
                else if (lblStatus.Text.ToLower() == "active")
                {
                    e.Row.Visible = false;
                }
                else if (lblStatus.Text.ToLower() == "old")
                {
                    e.Row.Visible = true;
                }
            }
        }
    }

    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbAutomatic.Checked == true)
        {
            Panel2.Visible = true;
            Panel3.Visible = false;
            rdbAutomatic.Checked = true;
            rdbManually.Checked = false;
            tbStation.Visible = false;
            tbTool.Visible = false;
            btnSearch.Visible = false;
            txtbarcode.Focus();

            string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name<>'Assembly' order by Vendor_Name asc ";
            string dataTextField = "VENDOR_NAME";
            string dataValueField = "ID";
            UtilityFunctions.bind_vendor(ddllocauto, query, dataTextField, dataValueField);
        }
        else
        {
            Panel2.Visible = false;
        }
    }

    protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbManually.Checked == true)
        {
            Panel2.Visible = false;
            rdbAutomatic.Checked = false;
            rdbManually.Checked = true;
            tbStation.Visible = true;
            tbTool.Visible = true;
            btnSearch.Visible = true;
            Panel3.Visible = true;

        }
        else
        {
            Panel2.Visible = true;
        }
    }

    protected void txtbarcode_TextChanged(object sender, EventArgs e)
    {
        DateTime lastKeyPress = DateTime.Now;
        if (txtbarcode.Text.Length > 0)
        {
            if (((TimeSpan)(DateTime.Now - lastKeyPress)).Seconds <= 0.5)
            {
                string str = null;
                string[] strArr = null;
                str = txtbarcode.Text;

                char[] splitchar = { ' ' };
                strArr = str.Split(splitchar);
                //MessageBox.Show(strArr[count]);
                txttoolno.Text = strArr[0];
                txtstationno.Text = strArr[1];
                txtposition.Text = strArr[2];

                txtbarcodescaned.Text = txtbarcode.Text;

                fill_qty();
                ViewState["toolno"] = txttoolno.Text;
                ViewState["stationno"] = txtstationno.Text;
                ViewState["position"] = txtposition.Text;
                ViewState["Qty"] = txtQty.Text;
                ViewState["SENT"] = txtsentQty1.Text;
                ViewState["vendor"] = ddllocauto.SelectedItem.Text;
                ViewState["ItemName"] = txtname1.Text;
                ViewState["TotalQty"] = totalQty.Text;
                txtbarcode.Text = "";
                txtbarcode.Focus();
            }
            else
            {
                txtbarcode.Enabled = true;
            }
            lastKeyPress = DateTime.Now;
        }
    }

    public void fill_qty()
    {
        SqlCommand cmd;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);

        conn.Open();
        if (txtstationno.Text.Length > 0 && txtposition.Text.Length > 0 && txttoolno.Text.Length > 0)
        {
            cmd = new SqlCommand("SELECT distinct  isnull(quantity,0) as quantity ,isnull(QtySent,0) as QtySent ,isnull(part_name,'') as part_name FROM  part_history_gr where station=@station_no and location='COMPANY' and tool_no=tool_no and p_no=@positio_no and (isnull(quantity,0)>=isnull(QtySent,0)) ");
            cmd.Parameters.AddWithValue("@tool_no", txttoolno.Text);
            cmd.Parameters.AddWithValue("@station_no", txtstationno.Text);
            cmd.Parameters.AddWithValue("@positio_no", txtposition.Text);
            cmd.Connection = conn;
            DataTable dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            if (dt.Rows.Count > 0)
            {
                txtQty.Text = dt.Rows[0][0].ToString();
                txtsentQty1.Text = dt.Rows[0][1].ToString();
                txtname1.Text = dt.Rows[0][2].ToString();
                totalQty.Text = dt.Rows[0][0].ToString();
            }
            else
            {
                txtQty.Text = "0";
                txtsentQty1.Text = "0";
                txtname1.Text = "";
                totalQty.Text = "0";
                MessageBox("Record Not Found");
                return;
            }
        }
        else
        {
            MessageBox("Tool no or Station No or Position No should not be empty");
            return;
        }

    }

    protected void txtQty_TextChanged(object sender, EventArgs e)
    {
        if (txtQty.Text.Length > 0)
        {
            ViewState["Qty"] = txtQty.Text;
            txtbarcode.Focus();
        }
    }

    /// <summary>
    /// Validates the Entered Fields and then Sends the position to Selected Vendor. Rows are inserted into Part History Gr table, Location Info Gr table and quantity is updated 
    /// for the Row where location is Company in History Gr table. Challan is generated on each Send, which can be printed by the user if required.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSend_Click(object sender, EventArgs e)
    {
        if ((radiobtnListChallanType.SelectedValue == "Purchase Order" || radiobtnListChallanType.SelectedValue == "Job Work") && ((hdnVendorType.Value == "External Vendor") || ddlvendor.SelectedItem.Text.Trim() == "Inspection") && txtPONO.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('PO Number is Required for Challan Type (Purchase Order / Job Work) and Vendor Type (External Vendor) excluding Inspection.');", true);
            return;
        }
        if (txtPONO.Text != "")
        {
            if (hdnVendorType.Value == "Internal Vendor" && ddlvendor.SelectedItem.Text.Trim() != "Inspection")
            {
                if (txtPONO.Text != "Internal" && txtPONO.Text != "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid PO Number specified. For Vendor Type (Internal Vendor) excluding Inspection, PO Number has to be Blank or it has to be Specified as Internal');", true);
                    return;
                }
            }
            else if (txtPONO.Text != "")
            {
                //int num = 0;
                //bool isValid = int.TryParse(txtPONO.Text, out num);
                //if (isValid && num > 200)
                //{
                    string vendorName = "";
                    string sqlconn = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
                    using (SqlConnection connec = new SqlConnection(sqlconn))
                    {
                        connec.Open();
                        using (SqlCommand cmd = new SqlCommand("Select top 1 Location from part_history_Gr where PO_No=@PONo order by id asc", connec))
                        {
                            cmd.Parameters.AddWithValue("@PONo", txtPONO.Text);
                            using (SqlDataReader rdr = cmd.ExecuteReader())
                            {
                                if (rdr.HasRows)
                                {
                                    rdr.Read();
                                    vendorName = Convert.ToString(rdr["Location"]);
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(vendorName) && ddlvendor.SelectedItem.Text.Trim() != "Inspection")
                    {
                        if (ddlvendor.SelectedItem.Text != vendorName)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Same PO Number is already used for another Vendor.');", true);
                            return;
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Enter PO Number.');", true);
                    //ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Minimum Value for PO No is 200. Please enter PO No greater than 200.');", true);
                    return;
                }
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Minimum Value for PO No is 200. Please enter PO No greater than 200.');", true);
            //    return;
            //}
        }

        if (ddlvendor.SelectedItem.Text.Trim() == "Inspection")
        {
            printispectionchallan.Visible = true;
            btnprint.Visible = false;
        }
        else
        {
            btnprint.Visible = true;
        }

        btnSend.Attributes.Add("onclick", " this.disabled = true; " + ClientScript.GetPostBackEventReference(btnSend, null) + ";");
        approvername = Convert.ToString(Session["UserID"]).Replace("_", " ");
        //Parent Child Validation Check.
        string positionsNotParent = "";
        string positionNotChild = "";
        string weightUnitRequired = "";
        foreach (GridViewRow row in GridView1.Rows)
        {
            LinkButton lblPNo = row.FindControl("Label1") as LinkButton;
            Label lblPartName = row.FindControl("Label2") as Label;
            Label lblproject = row.FindControl("lblproject") as Label;
            Label lblStation = row.FindControl("Label10") as Label;
            TextBox tobeQtysent = row.FindControl("TextBox3") as TextBox;
            Label lblReworkNo = row.FindControl("lblReworkNo") as Label;
            TextBox txtPOAmount = row.FindControl("txtPOAmount") as TextBox;
            //tool_no1 = Convert.ToInt32(lblproject.Text);
            //Qtysent1 = Convert.ToInt32(tobeQtysent.Text);

            if (row.RowType == DataControlRowType.DataRow)
            {
                bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                if (isSelected == true)
                {
                    if ((radiobtnListChallanType.SelectedValue == "Purchase Order" || radiobtnListChallanType.SelectedValue == "Job Work") && (hdnVendorType.Value == "External Vendor" || (hdnVendorType.Value == "Internal Vendor" && ddlvendor.SelectedItem.Text.Trim() == "Inspection")) && txtPOAmount.Text == "")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('PO Amount is Required for Challan Type Purchase Order/Job Work');", true);
                        return;
                    }
                    else if (ddlvendor.SelectedItem.Text == "Weld Assembly Waiting")
                    {
                        int? resultRows = ValidatePositionIsParent(lblproject.Text, lblPNo.Text, lblPartName.Text, lblStation.Text, lblReworkNo.Text);
                        if (resultRows == 0)
                        {
                            int rowIndex = row.RowIndex + 1;
                            if (!string.IsNullOrEmpty(positionsNotParent))
                            {
                                positionsNotParent = positionsNotParent + "," + rowIndex;
                            }
                            else
                            {
                                positionsNotParent = rowIndex.ToString();
                            }
                        }
                        if (resultRows == null)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured while Validating Selected Positions are Parent or Not.');", true);
                            return;
                        }
                    }
                    else if (ddlvendor.SelectedItem.Text == "Weld Parts Waiting" || ddlvendor.SelectedItem.Text == "Weld Parts Consumed")
                    {

                        bool? isChildPosition = ValidatePositionIsChild(lblproject.Text, lblPNo.Text, lblPartName.Text, lblStation.Text, lblReworkNo.Text);
                        if (!isChildPosition.Value)
                        {
                            int rowIndex = row.RowIndex + 1;
                            if (!string.IsNullOrEmpty(positionNotChild))
                            {
                                positionNotChild = positionNotChild + "," + rowIndex;
                            }
                            else
                            {
                                positionNotChild = rowIndex.ToString();
                            }
                        }
                        if (isChildPosition == null)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Some Error Occured while Validating Selected Positions is Child.');", true);
                            return;
                        }
                    }

                    TextBox txtWeight = row.FindControl("txtWeight") as TextBox;
                    DropDownList ddlWeightUnit = row.FindControl("ddlWeightUnit") as DropDownList;
                    if (txtWeight.Text != "" && ddlWeightUnit.SelectedValue == "")
                    {
                        int rowIndex = row.RowIndex + 1;
                        if (!string.IsNullOrEmpty(weightUnitRequired))
                        {
                            weightUnitRequired = weightUnitRequired + "," + rowIndex;
                        }
                        else
                        {
                            weightUnitRequired = rowIndex.ToString();
                        }

                    }

                    if (txtPONO.Text != "Internal" && txtPONO.Text != "")
                    {
                        bool isPONOValidForUse = false;
                        string sqlconn = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
                        using (SqlConnection connec = new SqlConnection(sqlconn))
                        {
                            DataTable dt = new DataTable();
                            connec.Open();
                            using (SqlDataAdapter ad = new SqlDataAdapter("Select * from part_history_Gr where PO_No=@PONo", connec))
                            {
                                ad.SelectCommand.Parameters.AddWithValue("@PONo", txtPONO.Text);
                                ad.Fill(dt);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                isPONOValidForUse = false;
                                if (!isPONOValidForUse)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Same PO Number is already used for another Vendor/ Same Vendor with different Tool No,Position No,Part Name,Station, Rework No.');", true);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
        positionsNotParent = positionsNotParent.TrimEnd(',');
        if (!string.IsNullOrEmpty(positionsNotParent))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Position can not be sent to Weld Assembly Waiting as its not Parent Position. Please check row number " + positionsNotParent + ".');", true);
            return;
        }

        positionNotChild = positionNotChild.TrimEnd(',');
        if (!string.IsNullOrEmpty(positionNotChild))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Position can not be sent to Weld Parts Waiting/Weld Parts Consumed as its not Child Position. Please check row number " + positionNotChild + ".');", true);
            return;
        }

        weightUnitRequired = weightUnitRequired.TrimEnd(',');
        if (!string.IsNullOrEmpty(weightUnitRequired))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Unit of Weight is not selected in some Rows. Please check row number " + weightUnitRequired + ".');", true);
            return;
        }

        int tool_no1, quantity1, Qtysent1, sent1;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        SqlTransaction trans = null;
        try
        {
            conn.Open();
            trans = conn.BeginTransaction();
            string prefixStr = "CH";
            string generatedChallanNumber = "";
            generatedChallanNumber = UtilityFunctions.gencode(prefixStr);
            txtchalanno.Text = generatedChallanNumber;
            if (!string.IsNullOrEmpty(generatedChallanNumber))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Transaction = trans;
                    cmd.Connection = conn;
                    foreach (GridViewRow row in GridView1.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            LinkButton p_no = row.FindControl("Label1") as LinkButton;
                            Label part_name = row.FindControl("Label2") as Label;
                            Label lblproject = row.FindControl("lblproject") as Label;
                            Label station = row.FindControl("Label10") as Label;
                            Label lblbatchno = row.FindControl("lblbatchno") as Label;
                            str_dt1 = string.Format("{0:yyyy-MM-dd}", txtpodate.Text); // you can specify format 
                            CultureInfo provider = CultureInfo.InvariantCulture;
                            string kk = System.DateTime.Now.ToLongTimeString();
                            str_dt1 = str_dt1 + " " + kk;
                            Label Labelrw = row.FindControl("lblReworkNo") as Label;
                            string reworkNo = Labelrw.Text;


                            Label quantity = row.FindControl("LabelUpload_Qty") as Label;
                            TextBox tobeQtysent = row.FindControl("TextBox3") as TextBox;
                            Label sent = row.FindControl("Labelsent") as Label;
                            TextBox txtHSNCode = row.FindControl("txtHSNCode") as TextBox;
                            TextBox txtWeight = row.FindControl("txtWeight") as TextBox;
                            DropDownList ddlWeightUnit = row.FindControl("ddlWeightUnit") as DropDownList;
                            TextBox txtPOAmount = row.FindControl("txtPOAmount") as TextBox;
                            quantity1 = Convert.ToInt32(quantity.Text);
                            Qtysent1 = Convert.ToInt32(tobeQtysent.Text);
                            sent1 = Convert.ToInt32(sent.Text);
                            tool_no1 = Convert.ToInt32(lblproject.Text);
                            string location1 = ddlvendor.SelectedItem.Text;

                            bool isValidData = false;
                            bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                            if (isSelected)
                            {
                                SqlCommand cmdCheckQuantity = new SqlCommand("select (isnull(((Upload_Qty)-(isnull(QtySent,0))),0)) as PendingQty "
                                + " from part_history_Gr where location='Company'  and (Sent_Date is null or Received_Date is null) and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and tool_no='" + lblproject.Text + "' "
                                + " and station='" + station.Text + "' and rwk_no='" + Labelrw.Text + "'", conn, trans);
                                using (SqlDataReader reader = cmdCheckQuantity.ExecuteReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        int enteredQty = Convert.ToInt32(tobeQtysent.Text);
                                        int pendingQty = Convert.ToInt32(reader["PendingQty"]);

                                        if (enteredQty <= pendingQty && enteredQty > 0)
                                        {
                                            isValidData = true;
                                        }
                                    }
                                }
                            }
                            if (isSelected)
                            {
                                if (!isValidData)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Invalid Quantity Entered. Quantiy has been modified Please refresh the Page.');", true);
                                    return;
                                }
                                if (location1 == "VD Finish Store" || location1 == "Assembly")
                                {
                                    int quant_rec = 0;
                                    int req_qty = 0;
                                    string cmd1 = "insert";
                                    Int32 qtysent1 = Convert.ToInt32(Qtysent1);

                                    string strQuery2 = "update part_history_gr  set  QtySent=" + qtysent1 + ", Sent_Date='" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "' , quantity=(isnull(quantity,0) - " + qtysent1 + ") where  p_no='" + p_no.Text + "' and location='COMPANY' and part_name='" + part_name.Text.Trim() + "' and station='" + station.Text + "' and tool_no=" + tool_no1 + " and rwk_no='" + reworkNo + "' ";
                                    cmd.CommandText = strQuery2;
                                    cmd.Connection = conn;
                                    cmd.ExecuteNonQuery();

                                    string poAmount = (txtPOAmount.Text == "") ? "0" : txtPOAmount.Text;
                                    string s = "insert into part_history_gr(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,Challan_No,Rework,Fresh,TransSentQty,Upload_Qty,Sent_Date,rwk_no,User_name,Used_Date,Challan_Type,HSN_Code,Weight_Value,Weight_Unit,PO_Amount) values ('" + p_no.Text + "','" + part_name.Text + "', '" + tool_no1 + "','" + station.Text + "','" + location1 + "','" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','Sent'," + qtysent1 + "," + Qtysent1 + ",'" + txtchalanno.Text + "','','','" + Qtysent1 + "'," + quantity1 + ",'" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','" + Labelrw.Text + "','" + approvername + "','" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "','" + radiobtnListChallanType.SelectedValue + "','" + txtHSNCode.Text + "','" + txtWeight.Text + "','" + ddlWeightUnit.SelectedValue + "'," + poAmount + ")";
                                    cmd.CommandText = s;
                                    cmd.Connection = conn;
                                    cmd.ExecuteNonQuery();

                                    s = "select quant_sent,quant_rec,req_qty from location_info_gr where location='" + location1 + "' and tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and rwk='" + reworkNo + "'";
                                    cmd.CommandText = s;

                                    DataTable dt = new DataTable();
                                    dt.Load(cmd.ExecuteReader());

                                    if (dt.Rows.Count != 0)
                                    {
                                        cmd1 = "update";
                                        quantity1 += Convert.ToInt32(dt.Rows[0][0].ToString());
                                        quant_rec = Convert.ToInt32(dt.Rows[0][1].ToString());
                                        req_qty = Convert.ToInt32(dt.Rows[0][2].ToString());
                                    }

                                    if (cmd1.Equals("insert"))
                                    {
                                        s = "insert into location_info_gr(tool_no,station,p_no,part_name,quant_sent,Sent_Received_Date,Challan_No,Rework,Fresh,TransSentQty,location,req_qty,Upload_Qty,Rwk) values(" + tool_no1 + ",'" + station.Text + "','" + p_no.Text + "','" + part_name.Text + "'," + Qtysent1 + ",'" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','" + txtchalanno.Text + "','',''," + qtysent1 + ",'" + location1 + "','" + req_qty + "'," + quantity1 + ",'" + reworkNo + "')";
                                    }
                                    else
                                    {
                                        s = "update location_info_gr set  TransSentQty=" + qtysent1 + ", quant_sent=" + qtysent1 + " + isnull(quant_sent,0)  where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and location='" + location1 + "' and rwk='" + reworkNo + "'";
                                    }
                                    cmd.CommandText = s;
                                    cmd.ExecuteNonQuery();


                                    //Insert into Challan Sent Received Detail table
                                    cmd.Parameters.Clear();
                                    cmd.CommandText = "insert into Challan_Sent_Received_Detail (Challan_No,Tool_No,Part_Name,P_No,Station,Rwk_No,Location,Sent_Or_Rec,Quantity,Outstanding_Qty,Created_Date,Created_By,Send_Receive_Date) Values(@ChallanNo,@ToolNo,@PartName,@PNo,@Station,@RwkNo,@Location,@SentOrRec,@Quantity,@OutstandingQty,@CreatedDate,@CreatedBy,@SendReceiveDate)";
                                    cmd.Parameters.AddWithValue("@ChallanNo", txtchalanno.Text);
                                    cmd.Parameters.AddWithValue("@ToolNo", tool_no1);
                                    cmd.Parameters.AddWithValue("@PartName", part_name.Text);
                                    cmd.Parameters.AddWithValue("@PNo", p_no.Text);
                                    cmd.Parameters.AddWithValue("@Station", station.Text);
                                    cmd.Parameters.AddWithValue("@RwkNo", Labelrw.Text);
                                    cmd.Parameters.AddWithValue("@Location", location1);
                                    cmd.Parameters.AddWithValue("@SentOrRec", "Sent");
                                    cmd.Parameters.AddWithValue("@Quantity", qtysent1);
                                    cmd.Parameters.AddWithValue("@OutstandingQty", qtysent1);
                                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                                    cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
                                    cmd.Parameters.AddWithValue("@SendReceiveDate", Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss"));
                                    cmd.ExecuteNonQuery();
                                }
                                else
                                {
                                    if (txtesthrs.Text.Length <= 0)
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('please enter estimated hrs');", true);
                                        btnSend.Enabled = true;
                                        return;
                                    }
                                    if (location1.Length <= 0)
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select vendor');", true);
                                        btnSend.Enabled = true;
                                        return;
                                    }

                                    if (txtpodate.Text.Length > 0)
                                    {
                                        int req_qty = 0;
                                        string s = "";
                                        int quant_rec = 0;
                                        string command = "insert";
                                        int first_location_sent = 0;
                                        int quant_others = 0;

                                        DataTable dt = new DataTable();

                                        if (quantity1 < Qtysent1)
                                        {
                                            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Send Qty Should be less than Total Qty');", true);
                                            btnSend.Enabled = true;
                                            return;
                                        }

                                        Int32 qtysent1 = Convert.ToInt32(Qtysent1);
                                        string strQuery4 = "update part_history_gr  set quantity=(quantity - " + qtysent1 + ") , Sent_Date='" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "' where location='COMPANY' and  p_no='" + p_no.Text + "' and station='" + station.Text + "' and part_name='" + part_name.Text.Trim() + "' and tool_no=" + tool_no1 + " and rwk_no='" + reworkNo + "'";
                                        cmd.CommandText = strQuery4;
                                        cmd.ExecuteNonQuery();

                                        string strQuery2 = "update part_history_gr  set  QtySent=" + qtysent1 + " + isnull(QtySent,0) where p_no='" + p_no.Text + "' and station='" + station.Text + "' and location='COMPANY' and part_name='" + part_name.Text.Trim() + "' and tool_no=" + tool_no1 + " and rwk_no='" + reworkNo + "'";
                                        cmd.CommandText = strQuery2;
                                        cmd.ExecuteNonQuery();

                                        //TO RETRIEVE REQUIRED QUANTITY OF THE PARTICULAR TOOL FROM PARTS TABLE
                                        s = "select req_qty from parts_gr where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and rwk_no='" + reworkNo + "'";
                                        cmd.CommandText = s;

                                        dt.Load(cmd.ExecuteReader());
                                        if (dt.Rows.Count == 0)
                                        {
                                            lbl.Text = "No such part exists..!!";
                                            return;
                                        }
                                        else
                                        {
                                            req_qty = Convert.ToInt32(dt.Rows[0][0].ToString());
                                        }

                                        //TO UPDATE PART_HISTORY TABLE
                                        string poAmount = (txtPOAmount.Text == "") ? "0" : txtPOAmount.Text;
                                        s = "insert into part_history_gr(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,Explain_Date,Challan_No,Sr_Reg_No,Batch_No,Required_date,PO_NO,EST_Hrs,Rework,Fresh,TransSentQty,Upload_Qty,Sent_Date,rwk_no,User_name,Used_Date,Challan_Type,HSN_Code,Weight_Value,Weight_Unit,PO_Amount) values ('" + p_no.Text + "','" + part_name.Text + "', '" + tool_no1 + "','" + station.Text + "','" + location1 + "','" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','Sent'," + qtysent1 + "," + Qtysent1 + ",'" + DateTime.Now.AddDays(3).ToString("yyyy/MM/dd HH:mm:ss") + "','" + txtchalanno.Text + "','" + txtSRREGNO.Text + "','" + lblbatchno.Text + "','" + txtrequired.Text + "','" + txtPONO.Text + "','" + txtesthrs.Text + "','',''," + qtysent1 + "," + quantity1 + ",'" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','" + Labelrw.Text + "','" + approvername + "','" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "','" + radiobtnListChallanType.SelectedValue + "','" + txtHSNCode.Text + "','" + txtWeight.Text + "','" + ddlWeightUnit.SelectedValue + "'," + poAmount + ")";
                                        cmd.CommandText = s;
                                        cmd.ExecuteNonQuery();

                                        //UPDATING TABLE LOCATION_INFO

                                        count = count + 1; //FIXME :: why is this needed here ???

                                        //RETRIEVING TOTAL QUANTITTY SENT OF A PARTICULAR PART TO A PARTICULAR VENDOR
                                        s = "select quant_sent,quant_rec from location_info_gr where location='" + location1 + "' and tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and rwk='" + reworkNo + "' ";
                                        cmd.CommandText = s;
                                        dt = new DataTable();
                                        dt.Load(cmd.ExecuteReader());

                                        if (dt.Rows.Count != 0)
                                        {
                                            command = "update";
                                            quantity1 += Convert.ToInt32(dt.Rows[0][0].ToString());
                                            quant_rec = Convert.ToInt32(dt.Rows[0][1].ToString());
                                        }

                                        //CALCULATING THE PENDING QUANTITY
                                        int quant_pend = quantity1 - quant_rec;

                                        //FINDING QUANTITY PENDING WITH PREVIOUS VENDOR

                                        //FINDING FIRST VENDOR and QUANTITY SENT TO HIM
                                        s = "select top 1 location from part_history_gr where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and rwk_no='" + reworkNo + "' and sent_or_rec='Sent' order by date_of_trans asc";
                                        cmd.CommandText = s;

                                        dt = new DataTable();
                                        dt.Load(cmd.ExecuteReader());

                                        string first_location = dt.Rows[0][0].ToString();
                                        if (!first_location.Equals(location))
                                        {
                                            s = "select quant_sent from location_info_gr where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and location='" + first_location + "' and rwk='" + reworkNo + "'";
                                            cmd.CommandText = s;

                                            dt = new DataTable();
                                            dt.Load(cmd.ExecuteReader());
                                            if (dt.Rows.Count > 0)
                                            {
                                                first_location_sent = Convert.ToInt32(dt.Rows[0][0].ToString());
                                                //finding quantity pending with first vendor
                                                quant_others = first_location_sent - quantity1;
                                            }
                                        }

                                        string modsentdate = Convert.ToString(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                                        //NOW INSERTING INTO LOCATION_INFO TABLE
                                        if (command.Equals("insert"))
                                        {
                                            s = "insert into location_info_gr values(" + tool_no1 + ",'" + station.Text + "','" + p_no.Text + "','" + part_name.Text + "'," + req_qty + ",'" + location1 + "'," + Qtysent1 + "," + quant_rec + "," + quant_pend + "," + quant_others + ",'" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','','" + DateTime.Now.AddDays(3).ToString("yyyy/MM/dd HH:mm:ss") + "','" + DBNull.Value + "','" + txtchalanno.Text + "','" + txtSRREGNO.Text + "','" + lblbatchno.Text + "','" + txtrequired.Text + "','" + txtPONO.Text + "','','','" + txtesthrs.Text + "'," + qtysent1 + "," + quantity1 + ",'" + modsentdate + "','" + Labelrw.Text + "'," + 0 + ")";
                                        }
                                        else
                                        {
                                            s = "update location_info_gr set  TransSentQty=" + qtysent1 + ", quant_sent=" + qtysent1 + " + isnull(quant_sent,0),quant_pend=" + quant_pend + ",quant_others=" + quant_others + " where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and location='" + location1 + "' and rwk='" + reworkNo + "'";
                                        }
                                        cmd.CommandText = s;
                                        cmd.ExecuteNonQuery();

                                        //Insert into Challan Sent Received Detail table
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = "insert into Challan_Sent_Received_Detail (Challan_No,Tool_No,Part_Name,P_No,Station,Rwk_No,Location,Sent_Or_Rec,Quantity,Outstanding_Qty,Created_Date,Created_By,Send_Receive_Date) Values(@ChallanNo,@ToolNo,@PartName,@PNo,@Station,@RwkNo,@Location,@SentOrRec,@Quantity,@OutstandingQty,@CreatedDate,@CreatedBy,@SendReceiveDate)";
                                        cmd.Parameters.AddWithValue("@ChallanNo", txtchalanno.Text);
                                        cmd.Parameters.AddWithValue("@ToolNo", tool_no1);
                                        cmd.Parameters.AddWithValue("@PartName", part_name.Text);
                                        cmd.Parameters.AddWithValue("@PNo", p_no.Text);
                                        cmd.Parameters.AddWithValue("@Station", station.Text);
                                        cmd.Parameters.AddWithValue("@RwkNo", Labelrw.Text);
                                        cmd.Parameters.AddWithValue("@Location", location1);
                                        cmd.Parameters.AddWithValue("@SentOrRec", "Sent");
                                        cmd.Parameters.AddWithValue("@Quantity", qtysent1);
                                        cmd.Parameters.AddWithValue("@OutstandingQty", qtysent1);
                                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                                        cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UserID"]));
                                        cmd.Parameters.AddWithValue("@SendReceiveDate", Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss"));
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Challan Number generation Failed.');", true);
                return;
            }
            trans.Commit();
        }
        catch (Exception ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }

        if (count > 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Send Item Successfully');", true);
            btnSend.Enabled = true;
            btnSend.Visible = false;

        }
        showgrid();

        string selectQuery = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name<>'Assembly' order by Vendor_Name asc ";
        string dataTextField = "VENDOR_NAME";
        string dataValueField = "ID";
        UtilityFunctions.bind_vendor(ddlvendor, selectQuery, dataTextField, dataValueField);
    }

    /// <summary>
    /// Validates if Position is Parent, returns the number of childs if position is parent
    /// </summary>
    /// <param name="toolNo"></param>
    /// <param name="pNo"></param>
    /// <param name="partName"></param>
    /// <param name="station"></param>
    /// <param name="rwkNo"></param>
    /// <returns></returns>
    private int? ValidatePositionIsParent(string toolNo, string pNo, string partName, string station, string rwkNo)
    {
        int? childRows = null;
        DataTable dt = new DataTable();
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            connec.Open();
            SqlCommand cmd = new SqlCommand();
            string parentName = "P_" + pNo;
            SqlDataAdapter ad = new SqlDataAdapter("select * from parts_Gr where ((opr17 like '%" + parentName + ",%' or opr17 like '%," + parentName + "' or opr17 like '%," + parentName + ",%' or opr17 like '" + parentName + "') or (Parent_Positions like '%" + parentName + ",%' or Parent_Positions like '%," + parentName + "' or Parent_Positions like '%," + parentName + ",%' or Parent_Positions like '" + parentName + "')) and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + rwkNo + "' and Is_Active_Record=1", connec);
            ad.Fill(dt);
            childRows = dt.Rows.Count;
            return childRows;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return childRows;
        }
        finally
        {
            connec.Close();
        }
    }

    /// <summary>
    /// Validates if position is Child position, returns true if position is child and false if position is not child, returns null if there is any exception
    /// </summary>
    /// <param name="toolNo"></param>
    /// <param name="pNo"></param>
    /// <param name="partName"></param>
    /// <param name="station"></param>
    /// <param name="rwkNo"></param>
    /// <returns></returns>
    private bool? ValidatePositionIsChild(string toolNo, string pNo, string partName, string station, string rwkNo)
    {
        bool? isChildPosition = null;
        DataTable dt = new DataTable();
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            connec.Open();
            SqlCommand cmd = new SqlCommand();
            //string parentName = "P_";
            SqlDataAdapter ad = new SqlDataAdapter("select * from parts_Gr where ((opr17 is not null and opr17!='') or (Parent_Positions is not null and Parent_Positions!='')) and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + rwkNo + "' and p_no='" + pNo + "' and part_name='" + partName + "' and Is_Active_Record=1", connec);
            ad.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string parentPositionsOld = Convert.ToString(dt.Rows[i]["opr17"]).ToLower();
                    string parentPositionsForNew = Convert.ToString(dt.Rows[i]["Parent_Positions"]).ToLower();
                    if (parentPositionsOld.StartsWith("p_") || parentPositionsForNew.StartsWith("p_"))
                    {
                        string[] opr17 = parentPositionsOld.Split(',');
                        string[] parentPosition = parentPositionsForNew.Split(',');
                        for (int j = 0; j < opr17.Length; j++)
                        {
                            SqlDataAdapter adCheck = new SqlDataAdapter("select * from parts_gr where (p_no='" + Convert.ToString(opr17[j]).Replace("p_", "") + "' or p_no='" + Convert.ToString(parentPosition[j]).Replace("p_", "") + "') and tool_no='" + toolNo + "'  and  station='" + station + "' and rwk_no='" + rwkNo + "' and Is_Active_Record=1", connec);
                            DataTable dtExist = new DataTable();
                            adCheck.Fill(dtExist);
                            if (dtExist.Rows.Count > 0)
                            {
                                isChildPosition = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        isChildPosition = false;
                    }
                }
            }
            else
            {
                isChildPosition = false;
            }
            return isChildPosition;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return isChildPosition;
        }
        finally
        {
            connec.Close();
        }
    }

    /// <summary>
    /// Prints the Challan Based on the selected Challan Type, Each Challan Type has different number of Copies 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnprint_Click(object sender, EventArgs e)
    {
        Session["MIN"] = txtchalanno.Text;
        if (txtchalanno.Text.Length > 0)
        {
            string challanType = "";
            if (radiobtnListChallanType.SelectedValue == "Purchase Order")
            {
                challanType = "PO";
            }
            else if (radiobtnListChallanType.SelectedValue == "Job Work")
            {
                challanType = "JW";
            }
            else if (radiobtnListChallanType.SelectedValue == "Rework Challan")
            {
                challanType = "RC";
            }
            else if (radiobtnListChallanType.SelectedValue == "Non Returnable Reject Challan")
            {
                challanType = "NR";
            }
            Response.Write("<script language=javascript>child=window.open('SentChallan.aspx?ChallanType=" + challanType + "&AutoGeneratedChallan=" + ViewState["GeneratedGRChallanNumber"] + "');</script>");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Send part before Print');", true);
            return;
        }
    }

    /// <summary>
    /// Validates if Vendor is Active or Inactive and displays the same Message. User is not allowed to send parts to active Vendors only.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlvendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlvendor.SelectedItem.Text == "VD Finish Store" || ddlvendor.SelectedItem.Text == "Assembly")
            {
                txtesthrs.Enabled = false;
                txtrequired.Enabled = false;

            }
            if (ddlvendor.SelectedItem.Text.Trim() == "Inspection")
            {
                printispectionchallan.Visible = true;
                btnprint.Visible = false;
            }
            else
            {
                printispectionchallan.Visible = false;
                btnprint.Visible = true;
            }

            lblVendorInactiveMessage.Text = String.Empty;
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            using (SqlConnection conn = new SqlConnection(sqlconnstring))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("select * from Vendor_Master_Job_Work where Vendor_Name=@VendorName", conn))
                {
                    cmd.Parameters.AddWithValue("@VendorName", ddlvendor.SelectedItem.Text);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            hdnVendorType.Value = Convert.ToString(rdr["vendor_type"]);
                            ViewState["IsActiveVendor"] = Convert.ToBoolean(rdr["Is_Active"]);
                            if (string.IsNullOrEmpty(Convert.ToString(rdr["Allocated_To"])))
                            {
                                ViewState["IsActiveVendor"] = false;
                            }
                            if (!Convert.ToBoolean(ViewState["IsActiveVendor"]))
                            {
                                lblVendorInactiveMessage.Text = "Inactive Vendor Selected, You can't Send Positions to Inactive Vendor.  <b>Note:-</b> If Supervisor is not assigned then Vendor will be Considered as Inactive.";
                                btnSend.Visible = false;
                            }
                            else
                            {
                                btnSend.Visible = true;
                            }
                        }
                    }
                }
            }

            //Validating and Updating the Vendor Quantities for Each selected Row
            foreach (GridViewRow row in GridView1.Rows)
            {
                CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
                TextBox TextBox3 = (TextBox)row.FindControl("TextBox3");
                TextBox txtbalc = (TextBox)row.FindControl("txtbalc");
                Label Label8 = (Label)row.FindControl("Label8");
                Label Labelsent = (Label)row.FindControl("Labelsent");
                Label uploadqty = (Label)row.FindControl("LabelUpload_Qty");
                TextBox3.Text = "0";
                if (ChkBoxRows.Checked == true)
                {
                    EnableTextBox(ChkBoxRows);
                }
            }
            if (ddlvendor.SelectedItem.Text == "GR VD FINISH STORE" || ddlvendor.SelectedItem.Text == "Not Required")
            {
                showgrid();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void ddllocauto_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddllocauto.SelectedItem.Text == "Inspection")
        {
            printispectionchallan.Visible = true;
            btnprint.Visible = false;
        }
        else
        {
            printispectionchallan.Visible = false;
            btnprint.Visible = true;
        }
    }

    protected void printispectionchallan_Click(object sender, EventArgs e)
    {
        if (txtchalanno.Text != "" && ddlvendor.SelectedItem.Text != "")
        {
            Session["MIN"] = txtchalanno.Text;
            Session["vendor"] = ddlvendor.SelectedItem.Text;
            Response.Write("<script language=javascript>child=window.open('GetPrintGRChallan.aspx');</script>");
        }
    }

    /// <summary>
    /// Clicking on the position number it displays the History of that Position
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Label1_Click(object sender, EventArgs e)
    {
        LinkButton lnkBtn = (LinkButton)sender;
        string contextKey = lnkBtn.CommandArgument;
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwk_no = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwk_no = List[4];
        try
        {
            string query = "select  id,location as Party,date_of_trans as Trans_Date,sent_or_rec as Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as Pending_Qty_PPC,TransSentQty as Send_Or_Rec_Qty, User_Name  from part_history_GR  where   p_no='" + Positionno + "' and  tool_no='" + toolno + "' and  station='" + station + "' and  part_name='" + part_name + "' and rwk_no='" + rwk_no + "' order by date_of_trans asc ";
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable table = new DataTable();

            da.Fill(table);

            StringBuilder b = new StringBuilder();

            b.Append("<table style='");
            b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<th>");
                b.Append(column.ColumnName);
                b.Append("</th>");
            }
            b.Append("</tr>");

            foreach (DataRow row in table.Rows)
            {
                b.Append("<tr>");
                foreach (DataColumn column in table.Columns)
                {
                    b.Append("<td>");
                    b.Append(row[column.ColumnName]);
                    b.Append("</td>");
                }
                b.Append("</tr>");
            }
            b.Append("</table>");

            pnlHistoryPopUp.InnerHtml = b.ToString();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }

    protected void Button1_Click(object sender, ImageClickEventArgs e)
    {
        showgrid();

        if (GridView1.Rows.Count == 1)
            lbl.Text = "";
        else
        {
            GridView1.Columns[10].Visible = false;
            GridView1.Columns[11].Visible = false;
        }
    }

    /// <summary>
    /// Validates if date is past or future date and display error message if it is part or future dates
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtpodate_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDateTime(txtpodate.Text) > DateTime.Now || Convert.ToDateTime(txtpodate.Text) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Can not take future/past date');", true);
            txtpodate.Text = "";
        }
    }

    /// <summary>
    /// Retrieves the Valid and Possible quantity for Send, also Retrieves the Balance Quantity and displays the same to user.
    /// </summary>
    /// <param name="currentToolNo"></param>
    /// <param name="currentStation"></param>
    /// <param name="currentPartName"></param>
    /// <param name="currentPNo"></param>
    /// <param name="currentRwkNo"></param>
    /// <param name="notRequiredQty"></param>
    /// <param name="availableQty"></param>
    /// <param name="enteredQty"></param>
    /// <param name="possibleQtyForSend"></param>
    /// <param name="balanceQty"></param>
    /// <returns></returns>
    protected int? ValidateQuantities(string currentToolNo, string currentStation, string currentPartName, string currentPNo, string currentRwkNo,
      int notRequiredQty, int availableQty, int enteredQty, out int possibleQtyForSend, out int balanceQty)
    {
        try
        {
            possibleQtyForSend = 0; balanceQty = 0;
            int qtySentToGrVDFinish = 0;
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            using (SqlConnection connec = new SqlConnection(sqlconnstring))
            {
                connec.Open();
                using (SqlCommand cmd = new SqlCommand("select isnull(isnull(quant_sent,0)- isnull(quant_rec,0),0) as QtyPending from location_info_Gr where tool_no=@ToolNo and station=@Station and p_no=@PNo and part_name=@PartName and rwk=@RwkNo and location='Not Required'", connec))
                {
                    cmd.Parameters.AddWithValue("@ToolNo", currentToolNo);
                    cmd.Parameters.AddWithValue("@Station", currentStation);
                    cmd.Parameters.AddWithValue("@PNo", currentPNo);
                    cmd.Parameters.AddWithValue("@PartName", currentPartName);
                    cmd.Parameters.AddWithValue("@RwkNo", currentRwkNo);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            if (rdr["QtyPending"] != DBNull.Value)
                                qtySentToGrVDFinish = Convert.ToInt32(rdr["QtyPending"]);
                        }
                    }

                }
            }

            int? returnQty = null;
            if (notRequiredQty > 0)
            {
                int qtyYetToBeSentAtGRVDFinishStore = notRequiredQty - qtySentToGrVDFinish;
                if (ddlvendor.SelectedItem.Text != "Not Required")
                {
                    availableQty = availableQty - qtyYetToBeSentAtGRVDFinishStore;
                    returnQty = availableQty;
                }
                else
                {
                    if (qtyYetToBeSentAtGRVDFinishStore < availableQty)
                    {
                        availableQty = qtyYetToBeSentAtGRVDFinishStore;
                    }
                    returnQty = availableQty;
                }
            }
            else
            {
                returnQty = availableQty;
            }
            if (enteredQty <= availableQty && enteredQty > 0)
            {
                returnQty = enteredQty;
            }
            if ((ddlvendor.SelectedItem.Text != "Not Required" && notRequiredQty >= 0) || (ddlvendor.SelectedItem.Text == "Not Required" && notRequiredQty > 0))
            {
                if (returnQty != null)
                {
                    if (returnQty > 0)
                    {
                        possibleQtyForSend = Convert.ToInt32(returnQty.Value);
                        int balance = Convert.ToInt32(availableQty) - Convert.ToInt32(returnQty);
                        balanceQty = Convert.ToInt32(balance);
                    }
                    else
                    {
                        balanceQty = 0;
                    }
                }
            }
            else if (ddlvendor.SelectedItem.Text == "Not Required" && notRequiredQty == 0)
            {
                possibleQtyForSend = 0;
                balanceQty = 0;
            }
            else
            {
                int balance = Convert.ToInt32(availableQty) - Convert.ToInt32(returnQty.Value);
                balanceQty = Convert.ToInt32(balance);
                possibleQtyForSend = returnQty.Value;
            }
            return returnQty;
        }
        catch (Exception ex)
        {
            ex.ToString();
            possibleQtyForSend = 0;
            balanceQty = 0;
            return null;
        }
    }

    /// <summary>
    /// Retrieves the PO Number as Internal if its Internal Vendor apart from Inspection
    /// </summary>
    /// <param name="text"></param>
    /// <param name="vendorType"></param>
    /// <param name="vendorName"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetPONumber(string text, string vendorType, string vendorName)
    {
        try
        {
            if (vendorType == "Internal Vendor" && vendorName.Trim() != "Inspection")
            {
                List<string> poNoList = new List<string>();
                poNoList.Add("Internal");
                return poNoList;
            }
            return null;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {

        }
    }
}