﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class PrintPendingItemsVendorVisitReport : System.Web.UI.Page
{
    /// <summary>
    /// Generates the Vendor Visit Report for the DataTable and passed Visit Detail ID as QueryString.
    /// Displays the Operation and Estimated Hours from Parts Gr table for each row in DataTable and creates the html
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection connec = new SqlConnection(sqlconnstring);
            try
            {
                connec.Open();
                if ((DataTable)Session["DataTableAdvanceReport"] != null && !string.IsNullOrEmpty(Convert.ToString(Request.QueryString["TicketDetailId"])))
                {
                    string vendorName = "";
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = connec;
                    cmd.CommandText = "select Visit_Number from Vendor_Visit_Detail where Vendor_Visit_Detail_Id=" + Request.QueryString["TicketDetailId"];
                    string visitNumber = "";
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            visitNumber = Convert.ToString(reader["Visit_Number"]);
                        }
                    }

                    SqlDataAdapter ad = new SqlDataAdapter("select * from Part_Operation_Sequence order by Sequence ASC", connec);
                    DataTable dtOperation = new DataTable();
                    ad.Fill(dtOperation);

                    DataTable dt = new DataTable();
                    dt = (DataTable)Session["DataTableAdvanceReport"];
                    string html = "";

                    List<PartOperations> partOperationList = new List<PartOperations>();
                    if (dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataTable dtParts = new DataTable();
                            SqlDataAdapter adParts = new SqlDataAdapter("select * from parts_Gr where Parts_Gr_Id=" + Convert.ToInt32(dt.Rows[i]["Parts_Gr_Id"]), connec);
                            adParts.Fill(dtParts);
                            for (int k = 0; k < dtParts.Rows.Count; k++)
                            {
                                int operationsCount = 1;
                                int rowsequence = 1;
                                while (operationsCount <= 17)
                                {

                                    string dynamcicColName = "opr" + operationsCount;
                                    if (!string.IsNullOrEmpty(Convert.ToString(dtParts.Rows[k][dynamcicColName])))
                                    {
                                        PartOperations partOperation = new PartOperations();
                                        partOperation.OperationsSequence = rowsequence;
                                        partOperation.PartsGrId = Convert.ToInt32(dt.Rows[i]["Parts_Gr_Id"]);
                                        string columnName = dynamcicColName;
                                        string sequence = columnName.Replace("opr", "");
                                        partOperation.OperationSeqName = columnName;
                                        partOperation.OperationValue = Convert.ToString(dtParts.Rows[k][columnName]);
                                        decimal result = 0;
                                        bool isValid = decimal.TryParse(partOperation.OperationValue, out result);
                                        if (isValid)
                                        {
                                            string[] splitValue = result.ToString().Split('.');
                                            decimal convertHrsIntoMins = 0;
                                            decimal convertDecimalValueIntoMins = 0;
                                            decimal totalMins = 0;
                                            if (splitValue.Length == 2)
                                            {
                                                if (Convert.ToDecimal(splitValue[0]) > 0)
                                                {
                                                    convertHrsIntoMins = Convert.ToDecimal(splitValue[0]) * 60;
                                                }
                                                if (Convert.ToDecimal(splitValue[1]) > 0)
                                                {
                                                    int decimalDivideFactor = 1;
                                                    int limit = 0;
                                                    while (splitValue[1].Length > limit)
                                                    {
                                                        decimalDivideFactor = decimalDivideFactor * 10;
                                                        limit = limit + 1;
                                                    }
                                                    convertDecimalValueIntoMins = (Convert.ToDecimal(splitValue[1]) * 60) / decimalDivideFactor;
                                                }
                                                totalMins = Math.Round(convertHrsIntoMins) + Math.Round(convertDecimalValueIntoMins);
                                            }
                                            else
                                            {
                                                convertHrsIntoMins = Convert.ToDecimal(splitValue[0]) * 60;
                                                totalMins = convertHrsIntoMins;
                                            }
                                            decimal totalDuration = Convert.ToDecimal(dt.Rows[i]["SentQty"]) * totalMins;
                                            if (totalDuration >= 60)
                                            {
                                                int hrs = Convert.ToInt32(totalDuration) / 60;
                                                int mins = Convert.ToInt32(totalDuration) % 60;
                                                if (hrs > 0 && mins > 0)
                                                {
                                                    partOperation.TotalDuration = hrs + " hrs " + mins + " mins";
                                                }
                                                else if (hrs > 0)
                                                {
                                                    partOperation.TotalDuration = hrs + " hrs ";
                                                }
                                                else if (mins > 0)
                                                {
                                                    partOperation.TotalDuration = mins + " mins";
                                                }
                                            }
                                            else
                                            {
                                                partOperation.TotalDuration = totalDuration + " mins";
                                            }
                                            //
                                        }
                                        DataRow dr = dtOperation.AsEnumerable().Single(data => data.Field<int>("Sequence") == int.Parse(sequence));
                                        partOperation.OperationActualName = Convert.ToString(dr["Operation_Name"]);
                                        if (partOperation.OperationActualName != "INSP")
                                        {
                                            partOperationList.Add(partOperation);
                                            rowsequence = rowsequence + 1;
                                        }
                                    }
                                    operationsCount = operationsCount + 1;
                                }
                            }
                        }
                    }
                    int maxOperationsCount = 0;
                    if (partOperationList.Count > 0)
                    {
                        maxOperationsCount = partOperationList.Max(x => x.OperationsSequence);
                    }
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                vendorName = Convert.ToString(dt.Rows[i]["Location"]);
                                html = html + "<div style='width:100%;margin:auto;word-wrap:break-word;line-height:30px;text-align:center;'><h3 style='text-align: center; margin-bottom: 3px; margin-top: 0px;'>Vendor Visit Report</h3>";
                                html = html + " <div style='text-align:center;margin-top:0px;'><b>Visit Number: </b>" + visitNumber + "</div>";
                                html = html + "<div style='width:100%;height:40px;'><div style='font-weight:bold;float:left;width:150px;'>Vendor Name:</div><div style='width:200px;float:left;'>" + Convert.ToString(dt.Rows[i]["Location"]) + "</div><div style='font-weight:bold;float:left;width:150px;'>Date:</div><div style='width:200px;float:left;'>" + DateTime.Now.ToShortDateString() + "</div><div style='font-weight:bold;float:left;width:200px;'>Filled By:</div><div></div></div>";
                                html = html + "<table style='table-layout:fixed;width:100%;border:1px solid black;border-collapse:collapse;font-size: 12px;line-height: 20px;'>";
                                html = html + "<tr style='font-weight:bold;'><td cellspacing='0' style='border:1px solid black;width:30px;'>Sr.No</td><td cellspacing='0' style='border:1px solid black;width:35px;'>Tool</td><td cellspacing='0' style='border:1px solid black;width: 40px;'>Station</td><td cellspacing='0' style='border:1px solid black;width: 35px;'>P No</td><td cellspacing='0' style='border:1px solid black;width: 140px;'>Part Name</td><td cellspacing='0' style='border:1px solid black;width:40px;'>Rwk No</td><td cellspacing='0' style='border:1px solid black;width: 33px;'>Qty</td><td cellspacing='0' style='border:1px solid black;width: 85px;'>Date of Transfer</td><td cellspacing='0' style='border:1px solid black;width: 70px;'>Original Date</td>"
                                     + "<td cellspacing='0' style='border:1px solid black;width:70px;'> Commit Date</td>";
                                for (int m = 1; m < maxOperationsCount + 1; m++)
                                {
                                    html = html + " <td cellspacing='0' style='border:1px solid black;width: 92px;'>Operation " + m + "</td>";
                                }
                                int colspan = 11 + maxOperationsCount;
                                html = html + " <td cellspacing='0' style='border:1px solid black;width: 50px;'>Remarks</td></tr><tr></tr> <td cellspacing='0' style='border:1px solid black;height:30px;' colspan='" + colspan + "'></td></tr>";
                            }
                            html = html + "<tr>";
                            html = html + "<td cellspacing='0' style='border:1px solid black;'>" + Convert.ToInt32(i + 1) + "</td>";
                            html = html + "<td cellspacing='0' style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["Tool_No"]) + "</td>";
                            html = html + "<td cellspacing='0' style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["Station"]) + "</td>";
                            html = html + "<td cellspacing='0' style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["P_No"]) + "</td>";
                            html = html + "<td cellspacing='0' style='border:1px solid black;font-size:10px;'>" + Convert.ToString(dt.Rows[i]["Part_Name"]) + "</td>";
                            html = html + "<td cellspacing='0' style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["Rwk"]) + "</td>";
                            html = html + "<td cellspacing='0' style='border:1px solid black;'>" + Convert.ToString(dt.Rows[i]["SentQty"]) + "</td>";
                            html = html + "<td cellspacing='0' style='border:1px solid black;'>" + Convert.ToDateTime(dt.Rows[i]["Sent_Received_Date"]).ToString("dd-MM-yyyy") + "</td>";
                            html = html + "<td cellspacing='0' style='border:1px solid black;'>" + Convert.ToDateTime(dt.Rows[i]["Sent_Received_Date"]).ToString("dd-MM-yyyy") + "</td>";
                            html = html + "<td cellspacing='0' style='border:1px solid black;'>" + ((dt.Rows[i]["Delivery_commit_date"] == DBNull.Value) ? String.Empty : Convert.ToDateTime(dt.Rows[i]["Delivery_commit_date"]).ToString("dd-MM-yyyy")) + "</td>";

                            for (int m = 1; m < maxOperationsCount + 1; m++)
                            {
                                PartOperations operationData = partOperationList.Find(x => x.PartsGrId == Convert.ToInt32(dt.Rows[i]["Parts_Gr_Id"]) && x.OperationsSequence == m);
                                if (operationData != null)
                                {
                                    html = html + "<td style='border:1px solid black;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse;table-layout:fixed;line-height:15px;height:40px;'><tr><td cellspacing='0' style='border-right:1px solid black;border-bottom:1px solid black;text-align:center;height:40px;'>" + operationData.OperationActualName + "</td><td cellspacing='0' style='border-bottom:1px solid black;text-align:center;height:40px;'>&nbsp;&nbsp;" + operationData.TotalDuration + "</td></tr><tr><td cellspacing='0' style='border-right:1px solid black;text-align:center;height:40px;'>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr></table></td>";
                                }
                                else
                                {
                                    html = html + "<td style='border:1px solid black;'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse;table-layout:fixed;line-height:15px;height:40px;'><tr><td cellspacing='0' style='border-right:1px solid black;border-bottom:1px solid black;text-align:center;height:40px;'>&nbsp;&nbsp;</td><td cellspacing='0' style='border-bottom:1px solid black;text-align:center;height:40px;'>&nbsp;&nbsp;</td></tr><tr><td cellspacing='0' style='border-right:1px solid black;text-align:center;height:40px;'>&nbsp;&nbsp;</td><td style='height:40px;'>&nbsp;&nbsp;</td></tr></table></td>";
                                }
                            }
                            html = html + "<td cellspacing='0' style='border:1px solid black;'></td>";
                            html = html + "</tr>";
                        }
                    }
                    html = html + "</table><div style='width: 100%;height: 60px;margin-top: 14px;padding-left: 6px;'><div style='font-weight:bold;float:left;width:170px;'>Vendor Commit Date:</div><div style='width:120px;float:left;'>_____________</div><div style='font-weight:bold;float:left;width:120px;'>Sent By Vendor:</div><div style='width:120px;float:left;'>_____________</div><div style='font-weight:bold;float:left;width:160px;'>Visit Person Sign:</div><div style='width:120px;float:left;'>____________</div> <div style='font-weight:bold;float:left;width:140px;'> Authorized By:</div><div style='width:110px;float:left;'>____________</div></div></div>";
                    panelVendorVisitReport.InnerHtml = html;


                    string print = "<script language='javascript' type='text/javascript'> window.print(); </script>";
                    Page.RegisterStartupScript("Print", print);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Error occured while processing your request. Please contact Administrator.');", true);
            }
        }
    }
}