﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Pages/MasterPage.master" CodeFile="GRPartList_summary_report.aspx.cs"
    Inherits="GRPartList_summary_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title></title>
    <style type="text/css">
        .Grid
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .Grid td
        {
            background-color: Highlight;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Grid th
        {
            background-color: Navy;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .ChildGrid td
        {
            background-color: #eee !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .ChildGrid th
        {
            background-color: #6C6C6C !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid td
        {
            background-color: #fff !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }
        .Nested_ChildGrid th
        {
            background-color: #2B579A !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }
    </style>
    <style type="text/css">
        .style2
        {
            border: none;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[id*=imgOrdersShow]").each(function () {
                if ($(this)[0].src.indexOf("minus") != -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
                    $(this).next().remove();
                }
            });
            $("[id*=imgProductsShow]").each(function () {
                if ($(this)[0].src.indexOf("minus") != -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
                    $(this).next().remove();
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="1px" style="margin-left: 10px; border-color: Blue" align="left" width="925px">
        <tr>
            <td class="style2" colspan="4" align="center" style="font-size: x-large; color: black">
                <span class="style1">Pending Part List History Summary Report </span>
                <asp:ScriptManager ID="ScriptManager2" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td class="style2" height="10px;">
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
            <td class="style2" height="30px">
                Tool No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="tbTool" CssClass="glowing-border" runat="server" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
            <td class="style2" height="30px">
                Station No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:TextBox ID="tbStation"
                    runat="server" CssClass="glowing-border" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
            <td class="style2" height="30px">
                Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="tbLocation" CssClass="glowing-border" runat="server" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
            <td height="30px" class="style2" colspan="51">
                GR No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="txtGr" CssClass="glowing-border" runat="server" Width="200px"></asp:TextBox>
                <asp:Button ID="Button1" runat="server" CssClass="glowing-border" Text="Search" OnClick="Button1_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Button2" runat="server" CssClass="glowing-border"
                    Text="ExpToExl" OnClick="Button2_Click" />
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
            <td class="style2" height="5px">
                <asp:Label ID="lbl" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="5">
                <asp:GridView ID="gvCustomers" runat="server" AutoGenerateColumns="false" Width="920px"
                    CssClass="Grid" DataKeyNames="tool_no">
                    <Columns>
                        <asp:TemplateField HeaderStyle-BackColor="Green">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgOrdersShow" runat="server" OnClick="Show_Hide_OrdersGrid"
                                    ImageUrl="~/images/plus.png" CommandArgument="Show" />
                                <asp:Panel ID="pnlOrders" runat="server" Visible="false" Style="position: relative">
                                    <asp:GridView ID="gvOrders" runat="server" AutoGenerateColumns="false" PageSize="50"
                                        AllowPaging="true" OnPageIndexChanging="OnOrdersGrid_PageIndexChanging" CssClass="ChildGrid">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgProductsShow" runat="server" OnClick="Show_Hide_ProductsGrid"
                                                        ImageUrl="~/images/plus.png" CommandArgument="Show" />
                                                    <asp:Panel ID="pnlProducts" runat="server" Visible="false" Style="position: relative">
                                                        <asp:GridView ID="gvProducts" runat="server" AutoGenerateColumns="false"
                                                            CssClass="Nested_ChildGrid">
                                                            <Columns>
                                                                <asp:BoundField ItemStyle-Width="150px" DataField="location" HeaderText="Location" />
                                                                <asp:BoundField ItemStyle-Width="150px" DataField="date_of_trans" HeaderText="Transaction Date" />
                                                                <asp:BoundField ItemStyle-Width="150px" DataField="sent_or_rec" HeaderText="Status" />
                                                                <asp:BoundField ItemStyle-Width="150px" DataField="PendingTotQty_in_PPC" HeaderText="PendingTotQty_in_PPC" />
                                                                <%-- <asp:BoundField ItemStyle-Width="150px" DataField="qtypending" HeaderText="qtypending" />--%>
                                                                <asp:BoundField ItemStyle-Width="150px" DataField="TransSentQty" HeaderText="Send/Receive Qty" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField ItemStyle-Width="150px" DataField="p_no" HeaderText="Position No" />
                                            <asp:BoundField ItemStyle-Width="150px" DataField="part_name" HeaderText="Part name" />
                                            <asp:BoundField ItemStyle-Width="150px" DataField="location" HeaderText="Vendor name" />
                                            <asp:BoundField ItemStyle-Width="150px" DataField="station" HeaderText="Station" />
                                            <asp:BoundField ItemStyle-Width="100px" DataField="qtypending" HeaderText="qtypending" />
                                            <asp:BoundField ItemStyle-Width="150px" DataFormatString="{0:dd/MM/yyyy HH:mm:ss tt}"
                                                DataField="Sent_Received_Date" HeaderText="Sent Date" />
                                            <asp:BoundField ItemStyle-Width="150px" DataFormatString="{0:dd/MM/yyyy HH:mm:ss tt}"
                                                DataField="Delivery_commit_date" HeaderText="Deliver Commit Date" />
                                            <asp:BoundField ItemStyle-Width="150px" DataFormatString="{0:dd/MM/yyyy HH:mm:ss tt}"
                                                DataField="date_of_trans" HeaderText="Upload Date" />
                                            <asp:BoundField ItemStyle-Width="50px" DataField="tool_no" HeaderText="tool_no" />
                                            <asp:BoundField ItemStyle-Width="50px" DataField="Challan_No" HeaderText="Challan No" />
                                            <asp:BoundField ItemStyle-Width="50px" DataField="rwk_no" HeaderText="GR NO" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="hdnGrNo" Value='<%#Eval("rwk_no") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-BackColor="Green">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" OnClick="Button3_Click" AlternateText="Download to excel"
                                                        ToolTip="Download to excel" ImageUrl="~/images/download.png" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ItemStyle-Width="150px" DataField="tool_no" HeaderStyle-BackColor="Green"
                            HeaderText="ToolNo" />
                        <asp:BoundField ItemStyle-Width="400px" DataField="location" HeaderStyle-BackColor="Green"
                            HeaderText="Vendor" />
                        <%-- <asp:BoundField ItemStyle-Width="150px" DataField="part_name" HeaderText="Part Name" />--%>
                        <asp:BoundField ItemStyle-Width="300px" DataField="qtypending" HeaderStyle-BackColor="Green"
                            HeaderText="PendingQty" />
                        <asp:TemplateField HeaderStyle-BackColor="Green">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" OnClick="loadExternalData" AlternateText="Download to excel"
                                    ToolTip="Download to excel" ImageUrl="~/images/download.png" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
        </tr>
    </table>
</asp:Content>
