﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text.RegularExpressions;

public partial class Tool_GenerateVendorStatusReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadSupervisors();
        }
    }

    /// <summary>
    /// Loads the Supervisor Name from Vendor_Master_Job_Work
    /// </summary>
    protected void LoadSupervisors()
    {
        ddlSupervisor.DataSource = UtilityFunctions.GetData("select distinct Allocated_To from Vendor_Master_Job_Work");
        ddlSupervisor.DataTextField = "Allocated_To";
        ddlSupervisor.DataValueField = "Allocated_To";
        ddlSupervisor.DataBind();

    }

    protected void gridViewVendorPendingItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }

    /// <summary>
    /// Retrieves the data and binds it with the grid view
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            gridViewVendorPendingItems.DataSource = null;
            gridViewVendorPendingItems.DataBind();

            btnGenerateVisitReport.Visible = true;
            DataTable dt = UtilityFunctions.GetDataForVendorStatusReport(inputVendorName.Text, ddlSupervisor.SelectedValue, ddlVendorType.SelectedValue);

            var result =
                    dt.AsEnumerable()
                        .Select(p => new
                        {
                            allocatedTo = p.Field<string>("Allocated_To"),
                            vendorName = p.Field<string>("Vendor_Name"),
                            vendorType = p.Field<string>("Vendor_Type"),
                            tool = p.Field<int>("Tool_No"),
                            station = p.Field<string>("Station"),
                            verifiedBy = p.Field<string>("Verified_By"),
                            challanNo = p.Field<string>("Challan_No"),
                            dateOfTrans = p.Field<DateTime>("Date_Of_Trans")

                        }).Distinct().Select(p => new { Allocated_To = p.allocatedTo, Vendor_Name = p.vendorName, Vendor_Type = p.vendorType, Tool_No = p.tool, Station = p.station, Verified_By = p.verifiedBy, Challan_No = p.challanNo, Date_Of_Trans = p.dateOfTrans })
                        .ToList();

            DataTable dtOutStandingParts = new DataTable();
            dtOutStandingParts.Columns.Add("Allocated_To", typeof(string));
            dtOutStandingParts.Columns.Add("Vendor_Name", typeof(string));
            dtOutStandingParts.Columns.Add("Vendor_Type", typeof(string));
            dtOutStandingParts.Columns.Add("Tool_No", typeof(string));
            dtOutStandingParts.Columns.Add("Station", typeof(string));
            dtOutStandingParts.Columns.Add("Verified_By", typeof(string));
            dtOutStandingParts.Columns.Add("Challan_No", typeof(string));
            dtOutStandingParts.Columns.Add("Date_Of_Trans", typeof(DateTime));
            dtOutStandingParts.Columns.Add("Part_Info", typeof(string));
            int total = 0;
            for (int i = 0; i < result.Count; i++)
            {
                string allocatedTo = result[i].Allocated_To;
                string vendorName = result[i].Vendor_Name;
                string vendorType = result[i].Vendor_Type;
                int tool = result[i].Tool_No;
                string station = result[i].Station;
                string verifiedBy = result[i].Verified_By;
                string challanNo = result[i].Challan_No;
                DateTime dateOfTrans = result[i].Date_Of_Trans;
                string html = "";
                DataRow dr = dtOutStandingParts.NewRow();
                DataRow[] drPartInfo = dt.Select("Allocated_To='" + allocatedTo + "' and Vendor_Name='" + vendorName + "' and Vendor_Type='" + vendorType + "' and Tool_No='" + tool + "' and Station='" + station + "' and Verified_By='" + verifiedBy + "' and Challan_No='" + challanNo + "'");
                if (drPartInfo.Length > 0)
                {
                    foreach (DataRow drRow in drPartInfo)
                    {
                        html = drRow["Rwk"] + "," + drRow["P_No"] + "," + drRow["PendingQty"] + " | " + html;
                        total = total + 1;
                    }
                    html = html.TrimEnd(' ');
                    html = html.TrimEnd('|');
                }
                dr["Allocated_To"] = allocatedTo;
                dr["Vendor_Name"] = vendorName;
                dr["Vendor_Type"] = vendorType;
                dr["Tool_No"] = tool;
                dr["Station"] = station;
                dr["Verified_By"] = verifiedBy;
                dr["Challan_No"] = challanNo;
                dr["Date_Of_Trans"] = dateOfTrans;
                dr["Part_Info"] = html;
                dtOutStandingParts.Rows.Add(dr);
            }
           int ftotal = total;
            Session["DataTableAdvanceReport"] = dtOutStandingParts;
            gridViewVendorPendingItems.DataSource = dtOutStandingParts;
            gridViewVendorPendingItems.DataBind();
            gridViewVendorPendingItems.EmptyDataText = "No Records Found";
            lblTotalRows.Text = "   Total Records Count - " + result.Count;
            //updatePage.Update();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }

    /// <summary>
    /// Generates the Vendor Status Report based on selected filters
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGenerateVisitReport_Click(object sender, EventArgs e)
    {
        SqlConnection connec = new SqlConnection();
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            if (gridViewVendorPendingItems.Rows.Count > 0)
            {
                string vendor = "", supervisor = "";
                if (string.IsNullOrEmpty(inputVendorName.Text))
                {
                    vendor = "All";
                }
                else
                {
                    vendor = inputVendorName.Text;
                }
                if (string.IsNullOrEmpty(ddlSupervisor.SelectedValue))
                {
                    supervisor = "All";
                }
                else
                {
                    supervisor = ddlSupervisor.SelectedValue;
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "window.open('PrintVendorStatusReport.aspx?Loc=" + vendor + "&Sup=" + supervisor + "&Type=" + ddlVendorType.SelectedValue + "');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('No Parts Found.');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Retrieves the list of Vendors, Company and Item Waiting For Process is also retrieved as Vendor
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetVendorName(string text)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            List<string> allItems = new List<string>();
            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrWhiteSpace(text))
            {
                cmd = new SqlCommand("select * from (select distinct Vendor_Name from Vendor_Master_Job_Work union select 'Company' as Vendor_Name from Vendor_Master_Job_Work union select 'Item Waiting For Process' as Vendor_Name from Vendor_Master_Job_Work)dt where Vendor_Name like '" + text + "%' and Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ", con);
            }
            else
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work union select 'Company' as Vendor_Name from Vendor_Master_Job_Work union select 'Item Waiting For Process' as Vendor_Name from Vendor_Master_Job_Work where Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ", con);
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string vendorName = Convert.ToString(reader["Vendor_Name"]);
                allItems.Add(vendorName);
            }
            reader.Close();
            return allItems;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            con.Close();
        }
    }
}
