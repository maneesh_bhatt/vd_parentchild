﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="VendorWisePartHistoryReport.aspx.cs" Inherits="Tool_VendorWisePartHistoryReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../Scripts/ScrollableGridViewPlugin_ASP.NetAJAXmin.js" type="text/javascript"></script>
    <title></title>
    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate >= new Date()) {
                alert("You cannot select a day less than today!");
                sender._selectedDate = new Date();

                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }


    </script>
    <script type="text/javascript">


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


        var TL;
        var TB;
        var TH;
        var TWt;
        var TAWB;
        var TMID;
        var TimeElapsed;


        function myFunctionL(e) {

            debugger;

            TL = e.timeStamp;
            document.getElementById("tbTool").value = TL;
            debugger;
        }

       
    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style14
        {
            width: 1034px;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 860px;
            height: 440px;
            top: 20%;
            left: 30%;
            margin-left: -185px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
            line-height: 28px;
            overflow-y: scroll;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .awbimagecontainer
        {
            float: right;
            width: 29%;
            margin-right: 12px;
            margin-top: 43px;
            border: 1px solid #2D89EF;
            height: 180px;
        }
        .web_dialog_info
        {
            display: none;
            position: fixed;
            width: 940px;
            height: 510px;
            top: 22%;
            left: 36%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="width: 100%; border: 1px solid Blue; min-height: 100px;">
        <div style="padding: 9px;">
            <div>
                <div align="center">
                    <span class="style13">Vendor Wise Summary Report</span>
                </div>
                <p style="font-size: 13px;">
                    Note: Displays the history of sent receive operations done on the data.</p>
            </div>
            <div style="margin-bottom: 10px; margin-top: 20px;">
                <div style="font-size: 13px; width: 145px; float: left;">
                    Location <span style="color: Red;">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddllocation"
                        ErrorMessage="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <asp:DropDownList ID="ddllocation" CssClass="glowing-border" AppendDataBoundItems="true"
                        BackColor="#F6F1DB" ForeColor="#7d6754" Width="140px" runat="server">
                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="font-size: 13px; width: 90px; float: left;">
                    Tool No
                    <br />
                    <asp:TextBox ID="inputToolNo" CssClass="glowing-border" runat="server" Width="80px"></asp:TextBox>
                </div>
                <div style="font-size: 13px; width: 85px; float: left;">
                    Rework No
                    <br />
                    <asp:TextBox ID="inputReworkNo" CssClass="glowing-border" runat="server" Width="75px"></asp:TextBox>
                </div>
                <div style="font-size: 13px; width: 90px; float: left;">
                    Operation
                    <br />
                    <asp:DropDownList ID="ddlOperation" CssClass="glowing-border" AppendDataBoundItems="true"
                        BackColor="#F6F1DB" ForeColor="#7d6754" Width="80px" runat="server">
                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                        <asp:ListItem Text="Sent" Value="Sent"></asp:ListItem>
                        <asp:ListItem Text="Received" Value="Received"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="font-size: 13px; width: 85px; float: left;">
                    Station
                    <br />
                    <asp:TextBox ID="inputStation" CssClass="glowing-border" runat="server" Width="75px"></asp:TextBox>
                </div>
                <div style="font-size: 13px; width: 90px; float: left;">
                    Position No
                    <br />
                    <asp:TextBox ID="inputPNo" CssClass="glowing-border" runat="server" Width="80px"></asp:TextBox>
                </div>
                <div style="font-size: 13px; width: 80px; float: left;">
                    Date Interval
                    <br />
                    <asp:DropDownList ID="ddlDateInterval" CssClass="glowing-border" AppendDataBoundItems="true"
                        BackColor="#F6F1DB" ForeColor="#7d6754" Width="70px" runat="server">
                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                        <asp:ListItem Text="1 Month" Value="1"></asp:ListItem>
                        <asp:ListItem Text="2 Month" Value="2"></asp:ListItem>
                        <asp:ListItem Text="3 Month" Value="3"></asp:ListItem>
                        <asp:ListItem Text="6 Month" Value="6"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="font-size: 13px; width: 95px; float: left;">
                    Start Date
                    <br />
                    <asp:TextBox ID="inputFromDate" CssClass="glowing-border" runat="server" Width="85px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                        Enabled="true" TargetControlID="inputFromDate" OnClientShowing="CurrentDateShowing">
                    </cc1:CalendarExtender>
                </div>
                <div style="font-size: 13px; width: 95px; float: left;">
                    End Date
                    <br />
                    <asp:TextBox ID="inputToDate" CssClass="glowing-border" runat="server" Width="85px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="true" Format="dd-MMM-yyyy"
                        TargetControlID="inputToDate" OnClientShowing="CurrentDateShowing">
                    </cc1:CalendarExtender>
                </div>
                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/images/searchbtn.jpg"
                    OnClick="btnSearch_Click" Style="margin-top: 10px;" />
                <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="btnDownload_Click"
                    Visible="false" Style="margin-top: 9px; margin-right: -4px; float: right; background-color: green;
                    color: white; padding: 6px; border: 1px solid green; box-shadow: 1px 1px 3px green;" />
            </div>
            <asp:GridView runat="server" ID="gridViewNegativeData" AutoGenerateColumns="false"
                OnRowDataBound="gridViewNegativeData_RowDataBound" AllowPaging="false" AllowSorting="false"
                Style="font-size: 13px; font-family: Verdana; line-height: 26px;">
                <RowStyle BackColor="White" ForeColor="Black" />
                <Columns>
                    <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Tool_No" HeaderText="Tool No" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="P_No" HeaderText="Position" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Part_Name" HeaderText="Part Name" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Station" HeaderText="Station" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Rwk_No" HeaderText="Rework No" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Operation" HeaderText="Operation" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Quantity" HeaderText="Sent/Receive Quantity" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                    <asp:BoundField DataField="Date_Of_Trans" HeaderText="Date Of Transaction" DataFormatString="{0:dd/MM/yyyy}"
                        HeaderStyle-BackColor="#335599" HeaderStyle-Width="150px" HeaderStyle-ForeColor="White" />
                    <asp:TemplateField HeaderStyle-BackColor="#335599" HeaderStyle-Width="70px" HeaderText="Estimated Time"
                        ItemStyle-Width="70px" HeaderStyle-ForeColor="White">
                        <ItemTemplate>
                            <asp:Label ID="lblEstimatedHrs" runat="Server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="User_Name" HeaderText="User Name" HeaderStyle-BackColor="#335599"
                        HeaderStyle-Width="100px" HeaderStyle-ForeColor="White" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
