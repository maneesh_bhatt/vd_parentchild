﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class Tool_ItemsInactiveReport : System.Web.UI.Page
{
    public static string searchCondition;
    protected void Page_Load(object sender, EventArgs e)
    {
        searchCondition = "";

        try
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void BindGrid()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            string searchCondition = "";

            if (inputToolNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and pgr.tool_no='" + inputToolNo.Text + "'";
                }
                else
                {
                    searchCondition = "  pgr.tool_no='" + inputToolNo.Text + "'";
                }
            }
            if (inputStation.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and pgr.station='" + inputStation.Text + "'";
                }
                else
                {
                    searchCondition = "  pgr.station='" + inputStation.Text + "'";
                }
            }
            if (inputPNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and pgr.p_no='" + inputPNo.Text + "'";
                }
                else
                {
                    searchCondition = "  pgr.p_no='" + inputPNo.Text + "'";
                }
            }
            if (inputReworkNo.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and pgr.rwk_no='" + inputReworkNo.Text + "'";
                }
                else
                {
                    searchCondition = "  pgr.rwk_no='" + inputReworkNo.Text + "'";
                }
            }
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = "";
            if (!string.IsNullOrEmpty(searchCondition))
            {
                query = "select pgr.tool_no,pgr.station,pgr.p_no,pgr.part_name,pgr.Rwk_No,pgr.req_qty as inactive_qty,Type,Version_Number from  parts_GR pgr where Status='Inactive' and Type!='NotRequired' and " + searchCondition + " order by pgr.tool_no,pgr.station asc";
                adapter = new SqlDataAdapter(query, con);
            }
            else
            {
                query = "select pgr.tool_no,pgr.station,pgr.p_no,pgr.part_name,pgr.Rwk_No,pgr.req_qty as inactive_qty,Type,Version_Number from  parts_GR pgr where Status='Inactive' and Type!='NotRequired' order by pgr.tool_no,pgr.station asc";
                adapter = new SqlDataAdapter(query, con);
            }
            ViewState["CurrentQuery"] = query;
            DataTable td = new DataTable();
            adapter.Fill(td);
            gridItemsInactive.DataSource = td;
            gridItemsInactive.DataBind();
            gridItemsInactive.EmptyDataText = "No Record Found";
            lblRecorrdsCount.Text = Convert.ToString(td.Rows.Count) + " " + "Records In Total";
            if (gridItemsInactive.Rows.Count > 0)
            {
                btnDownload.Visible = true;
            }
            else
            {
                btnDownload.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }
    }

    protected void gridItemsInactive_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // Set the index of the new display page.  
        gridItemsInactive.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void gridItemsInactive_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            Response.Redirect("Vendor_Master.aspx?ID=" + e.CommandArgument);
        }
    }

    //protected void ExportToExcel()
    //{
    //    Response.ClearContent();
    //    string fileName = "InactivePositionsReport";
    //    string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
    //    Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
    //    Response.ContentType = "application/excel";
    //    System.IO.StringWriter sw = new System.IO.StringWriter();
    //    HtmlTextWriter htw = new HtmlTextWriter(sw);
    //    gridItemsInactive.RenderControl(htw);
    //    Response.Write(sw.ToString());
    //    Response.End();
    //}

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        try
        {
            UtilityFunctions.ExportToExcel(Convert.ToString(ViewState["CurrentQuery"]), "InactivePositionsReport");
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}