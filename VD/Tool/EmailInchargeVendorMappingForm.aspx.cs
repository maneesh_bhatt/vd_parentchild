﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;

public partial class Tool_EmailInchargeVendorMappingForm : System.Web.UI.Page
{
    static string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadDropdowns();
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                ddlEmailIncharge.Enabled = false;
                LoadData(Convert.ToString(Request.QueryString["ID"]));
            }
        }
    }

    /// <summary>
    /// Retrieves and displays the Data for the Email Incharge. User is navigated to this page when he clicks Edit on Vendor Inchage Mapping Admin
    /// </summary>
    /// <param name="Id"></param>
    protected void LoadData(string Id)
    {
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            connec.Open();
            SqlDataAdapter ad = new SqlDataAdapter("select * from Email_Incharge_Vendor_Mapping where Email_Incharge=@EmailIncharge", connec);
            ad.SelectCommand.Parameters.AddWithValue("@EmailIncharge", Id);
            DataTable dt = new DataTable();
            ad.Fill(dt);
            ddlEmailIncharge.SelectedValue = Id;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                foreach (ListItem list in checkVendorList.Items)
                {
                    if (list.Value == Convert.ToString(dt.Rows[i]["Vendor_Id"]))
                    {
                        list.Selected = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    /// <summary>
    /// Binds the Vendor Master List from Vendor Master Job Work, User List from LDAP (Active Directory)
    /// </summary>
    protected void LoadDropdowns()
    {
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            DataTable dt = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter();

            ad = new SqlDataAdapter("select * from Vendor_Master_Job_Work where Vendor_Name not in ('Design Wait','Assembly','DIRECTASSEMBLY','GR VD FINISH STORE','PPC','Weld Assembly Waiting','Weld Parts Consumed','Weld Parts Waiting','VD Finish Store')", con);
            ad.Fill(dt);
            checkVendorList.DataSource = dt;
            checkVendorList.DataTextField = "Vendor_Name";
            checkVendorList.DataValueField = "ID";
            checkVendorList.DataBind();

            dt.Rows.Clear();
            List<Users> userList = Users.LoadUsersList();
            ddlEmailIncharge.DataSource = userList;
            ddlEmailIncharge.DataTextField = "UserName";
            ddlEmailIncharge.DataValueField = "UserName";
            ddlEmailIncharge.DataBind();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }
    }
    
    /// <summary>
    /// Adds a new record If record does not exist for selected Incharge, Updates the Exisitng Entry if record Exists
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SqlConnection connec = new SqlConnection(sqlconnstring);
        SqlCommand cmd = new SqlCommand();
        SqlTransaction trans = null;
        try
        {
            connec.Open();
            cmd.Connection = connec;
            trans = connec.BeginTransaction();
            cmd.Transaction = trans;
            cmd.Connection = connec;
            for (int i = 0; i < checkVendorList.Items.Count; i++)
            {
                string selectedItem = checkVendorList.Items[i].Text;
                string id = checkVendorList.Items[i].Value;
                bool recordExist = false;
                cmd.Parameters.Clear();
                cmd.CommandText = "select * from Email_Incharge_Vendor_Mapping where Vendor_Id=@VendorId and Email_Incharge=@EmailIncharge";
                cmd.Parameters.AddWithValue("@EmailIncharge", ddlEmailIncharge.SelectedValue);
                cmd.Parameters.AddWithValue("@VendorId", id);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        recordExist = true;
                    }
                }

                cmd.Parameters.Clear();
                if (checkVendorList.Items[i].Selected)
                {
                    if (!recordExist && checkVendorList.Items[i].Text != "")
                    {
                        cmd.CommandText = "insert into Email_Incharge_Vendor_Mapping(Vendor_Id,Email_Incharge,Created_By,Created_Date)values(@VendorId,@EmailIncharge,@CreatedBy,@CreatedDate)";
                        cmd.Parameters.AddWithValue("@VendorId", id);
                        cmd.Parameters.AddWithValue("@EmailIncharge", ddlEmailIncharge.SelectedValue);
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["UserID"]);
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    if (recordExist)
                    {
                        cmd.Parameters.Clear();
                        cmd.CommandText = "delete from Email_Incharge_Vendor_Mapping where vendor_Id=@VendorId and Email_Incharge=@EmailIncharge";
                        cmd.Parameters.AddWithValue("@VendorId", id);
                        cmd.Parameters.AddWithValue("@EmailIncharge", ddlEmailIncharge.SelectedValue);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            trans.Commit();
            lblMessage.Text = "Data Updated Successfully.";
            lblMessage.ForeColor = Color.Green;
        }
        catch (Exception ex)
        {
            ex.ToString();
            trans.Rollback();
        }
        finally
        {
            connec.Close();
        }
    }

    protected void ClearControls()
    {
        ddlEmailIncharge.SelectedIndex = 0;
        checkVendorList.ClearSelection();
    }
}