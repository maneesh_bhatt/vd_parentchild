﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Sent_GR_Item.aspx.cs" MasterPageFile="~/Pages/MasterPage.master"
    Inherits="Tool_Sent_GR_Item" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script src="../Scripts/ScrollableGridViewPlugin_ASP.NetAJAXmin.js" type="text/javascript"></script>
    <title></title>
    <script type="text/javascript">

        $(function () {
            GetChallanNumber();
            GetPONumber();
            ValidatePO();
            ValidatePONumber();

        });
        function GetChallanNumber() {
            $('#<%=inputChallanNumber.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "Sent_GR_Item.aspx/GetChallanNumber",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {

                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                $(this).autocomplete("search", "");
            });
        };

        function GetPONumber() {
            $('#<%=txtPONO.ClientID%>').autocomplete({
                source: function (request, response) {
                    var vendorType = $("#hdnVendorType").val();
                    var vendorName = $("#ddlvendor option:selected").text();

                    $.ajax({
                        url: "Sent_GR_Item.aspx/GetPONumber",
                        data: "{ 'text':'" + request.term + "','vendorType':'" + vendorType + "','vendorName':'" + vendorName + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {

                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                $(this).autocomplete("search", "");
            });
        };

        function ValidatePO() {

            var checked_radio = $("[id*=radiobtnListChallanType] input:checked");
            var value = checked_radio.val();
            var vendorType = $("#hdnVendorType").val();
            var vendorName = $("#ddlvendor option:selected").text();
            var text = checked_radio.closest("td").find("label").html();
            if ((value == "Purchase Order" || value == "Job Work") && ((vendorType == "External Vendor") || vendorName.trim() == "Inspection")) {

                $("#spanPORequired").text("*");
                $("#spanPORequired").css("color", "Red");
            }
            else {
                $("#spanPORequired").text("");
            }
        }

        function ValidatePONumber() {
            ValidatePO();
            $("#radiobtnListChallanType").change(function () {

                ValidatePO();
            });
            $("#ddlvendor").change(function () {

                ValidatePO();
            });
//            $("#txtPONO").change(function () {
//                var enteredPO = $(this).val();
//                if (enteredPO < 200) {

//                    alert("Invalid Value Entered for PO NO, Minumum Value for PO Number is 200.");
//                }
//            });
        };

        function checkDate(sender, args) {
            if (sender._selectedDate >= new Date()) {
                alert("You cannot select a day less than today!");
                sender._selectedDate = new Date();

                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        var TL;
        var TB;
        var TH;
        var TWt;
        var TAWB;
        var TMID;
        var TimeElapsed;

        function myFunctionL(e) {

            debugger;

            TL = e.timeStamp;
            document.getElementById("tbTool").value = TL;
            debugger;
        }
    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .style14
        {
            width: 1034px;
        }
    </style>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .glowing-border
        {
            /* border: 1px solid rgba(158, 158, 158, 0.58);
            margin-left: 2px;
            height: 28px;*/
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
        
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 860px;
            height: 440px;
            top: 20%;
            left: 30%;
            margin-left: -185px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
            line-height: 28px;
            overflow-y: scroll;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .awbimagecontainer
        {
            float: right;
            width: 29%;
            margin-right: 12px;
            margin-top: 43px;
            border: 1px solid #2D89EF;
            height: 180px;
        }
        .web_dialog_info
        {
            display: none;
            position: fixed;
            width: 940px;
            height: 510px;
            top: 22%;
            left: 36%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField runat="server" ID="hdnVendorType" ClientIDMode="Static" />
    <table style="margin-left: 0px;" align="left" width="100%">
        <tr>
            <td align="center" colspan="4">
                <span class="style13">Sent Rework / Fresh Items</span>
            </td>
        </tr>
        <tr>
            <td colspan="10" align="center" height="40px">
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="RadioButton1" Text="By Automatic" AutoPostBack="true"
                    runat="server" OnCheckedChanged="RadioButton1_CheckedChanged" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton
                        ID="RadioButton2" Text="By Manually" runat="server" AutoPostBack="true" OnCheckedChanged="RadioButton2_CheckedChanged" />
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="Button1" runat="server" CausesValidation="false"
                    Style="margin-bottom: -7px;" ImageUrl="~/images/searchbtn.jpg" OnClick="Button1_Click" />
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                    <ProgressTemplate>
                        <img alt="" src="../ajax-loader.gif" style="width: 32px; height: 32px; margin-left: -81px;
                            margin-bottom: -34px; margin-top: -20px;" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center" height="20px" align="left">
                <asp:Label runat="server" ID="lblErrorMessage" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center" height="20px" align="left">
                <asp:Label runat="server" ID="lblVendorInactiveMessage" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel2" runat="server">
                    <table>
                        <tr>
                            <td colspan="2">
                                Location :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                <asp:DropDownList ID="ddllocauto" AutoPostBack="true" AppendDataBoundItems="true"
                                    Width="150px" runat="server" OnSelectedIndexChanged="ddllocauto_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td colspan="2">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sent Qty :&nbsp; &nbsp;&nbsp;&nbsp;
                                &nbsp;
                                <asp:TextBox ID="txtsentQty1" runat="server" Width="150px" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td height="5px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Barcode No :&nbsp;&nbsp;&nbsp; &nbsp;
                                <asp:TextBox ID="txtbarcode" runat="server" AutoPostBack="true" Enabled="true" onkeydown="myFunctionL(event)"
                                    Width="150px" OnTextChanged="txtbarcode_TextChanged"></asp:TextBox>
                            </td>
                            <td colspan="2">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Item Name :&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                <asp:TextBox ID="txtname1" runat="server" Width="150px" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td height="5px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Tool No :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                <asp:TextBox ID="txttoolno" runat="server" Width="150px" Enabled="False"></asp:TextBox>
                            </td>
                            <td colspan="2">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp; Station No :&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="txtstationno" runat="server" Width="150px" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td height="5px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Position No :&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                <asp:TextBox ID="txtposition" runat="server" Width="150px" Enabled="False"></asp:TextBox>
                            </td>
                            <td colspan="2">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Send Qty :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                <asp:TextBox ID="txtQty" runat="server" Width="150px" OnTextChanged="txtQty_TextChanged"
                                    ReadOnly="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td height="5px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Old&nbsp; Barcode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="txtbarcodescaned" Enabled="false" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td colspan="2">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Total Qty :&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                <asp:TextBox ID="totalQty" runat="server" Width="150px" ReadOnly="false" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel3" runat="server" Style="height: 140px;">
                    <table>
                        <tr>
                            <td colspan="12">
                                <span style="float: left; margin-bottom: 10px;"><b>Challan Type: <span style="color: Red;">
                                    *</span></b></span> <span style="float: left; margin-bottom: 13px; margin-top: -3px;">
                                        <asp:RadioButtonList ID="radiobtnListChallanType" ClientIDMode="Static" runat="server"
                                            RepeatDirection="Horizontal" RepeatLayout="Flow" RepeatColumns="6">
                                            <asp:ListItem Text="Purchase Order" Value="Purchase Order"></asp:ListItem>
                                            <asp:ListItem Text="Job Work" Value="Job Work"></asp:ListItem>
                                            <asp:ListItem Text="Rework Challan" Value="Rework Challan"></asp:ListItem>
                                            <asp:ListItem Text="Non Returnable Reject Challan" Value="Non Returnable Reject Challan"></asp:ListItem>
                                        </asp:RadioButtonList>
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="radiobtnListChallanType"
                                            ErrorMessage="*" ForeColor="Red">
                                        </asp:RequiredFieldValidator>
                                    </span><span>
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateButton">
                                            <ContentTemplate>
                                                <asp:Label runat="server" ID="lblQCSheetNo"></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="font-size: 13px;">
                                Date: <span class="style12">* </span>
                                <br />
                                <asp:TextBox ID="txtpodate" CssClass="glowing-border" runat="server" AutoPostBack="true"
                                    Width="100px" OnTextChanged="txtpodate_TextChanged"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpodate"
                                    Display="None" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <cc1:CalendarExtender ID="txtpodate_CalendarExtender" runat="server" Enabled="true"
                                    Format="dd-MMM-yyyy" OnClientShowing="CurrentDateShowing" TargetControlID="txtpodate">
                                </cc1:CalendarExtender>
                            </td>
                            <td style="font-size: 13px;">
                                &nbsp;Comp.Chall.No <span class="style12">* </span>
                                <br />
                                <asp:TextBox ID="txtchalanno" CssClass="glowing-border" runat="server" Enabled="False"
                                    Width="100px"></asp:TextBox>
                                <br />
                            </td>
                            <td style="font-size: 13px;">
                                Vendor Name<span class="style12">* </span>
                                <br />
                                <asp:DropDownList ID="ddlvendor" CssClass="glowing-border" BackColor="#F6F1DB" ForeColor="#7d6754"
                                    runat="server" AutoPostBack="true" AppendDataBoundItems="true" Width="105px"
                                    ClientIDMode="Static" OnSelectedIndexChanged="ddlvendor_SelectedIndexChanged">
                                </asp:DropDownList>
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlvendor"
                                    Display="Dynamic" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td style="font-size: 13px;">
                                Tool No<br />
                                <asp:TextBox ID="tbTool" CssClass="glowing-border" runat="server" Width="120px"></asp:TextBox>
                            </td>
                            <td height="15px" style="font-size: 13px;">
                                Station No<br />
                                <asp:TextBox ID="tbStation" CssClass="glowing-border" runat="server" Width="120px"></asp:TextBox>
                            </td>
                            <td style="font-size: 13px;">
                                Rework No
                                <br />
                                <asp:TextBox ID="txtreworkno" CssClass="glowing-border" runat="server" Width="120px"></asp:TextBox>
                            </td>
                            <td style="font-size: 13px;" runat="server" id="tdChallanNumnber">
                                Challan Number
                                <br />
                                <asp:TextBox runat="server" ID="inputChallanNumber" Style="width: 120px;" CssClass="autocomplete glowing-border"
                                    ClientIDMode="Static"></asp:TextBox>&nbsp;
                            </td>
                            <td style="font-size: 13px; font-weight: bold; width: 200px; margin-top: 13px;">
                                <br />
                                Is Virtual Transaction
                                <asp:CheckBox runat="server" Style="margin-top: 10px;" ID="checkIsVirtualTransaction" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblbillno" runat="server" Text="Bill No"></asp:Label><span style="color: Red;">*</span><br />
                                <asp:TextBox ID="txtbillno" runat="server" CssClass="glowing-border" Width="75px"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtbillno" ErrorMessage="*"
                                    ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="font-size: 13px;">
                                <br />
                                Batch No<br />
                                <asp:TextBox ID="txtbatchno" CssClass="glowing-border" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td style="font-size: 13px;">
                                <br />
                                PO NO<span class="style12"><span id="spanPORequired"></span> </span>
                                <br />
                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updatePO">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtPONO" ClientIDMode="Static" onkeypress="return isNumberKey(event)"
                                            CssClass="glowing-border" runat="server" Width="100px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="font-size: 13px;">
                                <br />
                                Sr No of Reg.<br />
                                <asp:TextBox ID="txtSRREGNO" CssClass="glowing-border" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td style="font-size: 13px;">
                                <br />
                                Est.Hrs<span class="style12">* </span>
                                <br />
                                <asp:TextBox ID="txtesthrs" CssClass="glowing-border" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td valign="bottom" style="font-size: 13px;">
                                <br />
                                Req By Date:
                                <br />
                                <asp:TextBox ID="txtrequired" CssClass="glowing-border" runat="server" Width="120px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="true" Format="dd-MMM-yyyy"
                                    OnClientShowing="CurrentDateShowing" TargetControlID="txtrequired">
                                </cc1:CalendarExtender>
                            </td>
                            <td style="font-size:13px">
                                Delivery Commitment Date<br />
                                By Vendor<span class="style12">*</span>
                                <br />
                                <asp:TextBox ID="txtdelcdate" CssClass="glowing-border" runat="server" Width="120px" AutoPostBack="true"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                    Enabled="true" OnClientShowing="CurrentDateShowing" TargetControlID="txtdelcdate">
                                </cc1:CalendarExtender>
                            </td>
                            <td style="font-size: 13px; margin-right: 100px;" runat="server" id="tdPrintChallan"
                                visible="false">
                                &nbsp;
                                <asp:Button ID="btnprint" runat="server" CausesValidation="false" OnClick="btnprint_Click"
                                    Text="PrintChallan" Visible="false" Width="120px" Style="margin-top: 26px;" />
                            </td>
                            <td class="style1" style="font-size: 13px;" valign="middle">
                                <br />
                                <asp:Button ID="printispectionchallan" runat="server" CausesValidation="false" OnClick="printispectionchallan_Click"
                                    Text="PntQCChallan" Visible="false" Style="width: 100px; margin-top: 14px;" />
                            </td>
                            <td style="font-size: 13px;" valign="middle" colspan="4" runat="server" id="tdVendorForInspection">
                                <br />
                                <asp:Label ID="Label4" runat="server" Text="Vendor For Inspection" Visible="true"></asp:Label>
                                <br />
                                <asp:DropDownList ID="ddlreceived" BackColor="#F6F1DB" ForeColor="#7d6754" CssClass="glowing-border"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlreceived_SelectedIndexChanged"
                                    runat="server" Width="150px">
                                </asp:DropDownList>
                            </td>
                            <td height="5px">
                                <asp:Label ID="lbl" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="margin-left: 100px;" align="left">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="Panel1" runat="server" Style="overflow-x: scroll; width: 1270px; margin-top: -10px;">
                            <asp:Button ID="btnSend" runat="server" CausesValidation="true" Text="Send Items"
                                Visible="false" Style="width: 87px; float: right; margin-top: 1px; margin-right: 158px;
                                margin-bottom: 8px;" OnClick="btnSend_Click" />
                            <asp:GridView ID="GridView1" runat="server" EmptyDataText="No Record Found" AutoGenerateColumns="False"
                                Style="border: 1px solid #aba4a4;" CellPadding="4" ForeColor="#333333" GridLines="None"
                                OnPageIndexChanging="GridView1_PageIndexChanging" Font-Size="12px" Width="100%"
                                OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing"
                                OnRowUpdating="GridView1_RowUpdating" OnRowDataBound="GridView1_RowDataBound"
                                OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="40px" ItemStyle-Width="40px"
                                        ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll1" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll1_CheckedChanged1" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnBatchNo" runat="server" Value='<%# Eval("Batch_No") %>'>
                                            </asp:HiddenField>
                                            <asp:CheckBox ID="chkAllchild" AutoPostBack="true" runat="server" OnCheckedChanged="chkAllchild_CheckedChanged">
                                            </asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="30px" ItemStyle-Width="40px"
                                        ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Tool
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblproject" runat="server" Text='<%# Eval("tool_no") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="20px" ItemStyle-Width="50px"
                                        ItemStyle-HorizontalAlign="left">
                                        <HeaderTemplate>
                                            Stn
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label10" runat="server" Text='<%# Eval("station") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="70px" ItemStyle-Width="70px"
                                        ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            P No.
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Label1" CausesValidation="false" runat="server" Text='<%# Eval("p_no") %>'
                                                CommandArgument='<%# Eval("p_no") + ";" + Eval("tool_no") + ";" + Eval("station") + ";" + Eval("part_name") +";"+Eval("rwk_no") %>'
                                                OnClick="Label1_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="40px" ItemStyle-Width="40px"
                                        ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Rwk
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReworkNo" runat="server" Text='<%# Eval("rwk_no") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="90px" ItemStyle-Width="90px"
                                        ItemStyle-HorizontalAlign="left" ItemStyle-Wrap="true">
                                        <HeaderTemplate>
                                            Part Name
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("part_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" Visible="false" HeaderStyle-Width="100px"
                                        ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Location
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("location") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Bal.Qty in PPC
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label8" runat="server" Text='<%# Eval("quantity") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Qty Sent
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Labelsent" Visible="true" runat="server" Text='<%# Eval("QtySent") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px" HeaderStyle-Width="100px">
                                        <HeaderTemplate>
                                            Qty To Be send
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox3" CssClass="glowing-border" runat="server" OnTextChanged="TextBox3_TextChanged"
                                                AutoPostBack="true" Enabled="false" Width="40px" onkeypress="return isNumberKey(event)"
                                                Text='<%# Eval("PendingQty") %>'> </asp:TextBox>
                                            <cc1:NumericUpDownExtender ID="NumericUpDownExtender3" runat="server" TargetControlID="TextBox3"
                                                Width="120" RefValues="" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                TargetButtonUpID="" Minimum="0" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Bal.Qty
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtbalc" CssClass="glowing-border" runat="server" Enabled="false"
                                                Width="40px" Text=""></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Upl Qty
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelUpload_Qty" Visible="true" runat="server" Text='<%# Eval("Upload_Qty") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            HSN Code
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtHSNCode" Visible="true" Width="50px" Enabled="false" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="120px" HeaderStyle-Width="90px">
                                        <HeaderTemplate>
                                            Weight
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="float: left; width: 90px;">
                                                <asp:TextBox ID="txtWeight" Visible="true" Width="25px" Enabled="false" runat="server"></asp:TextBox>
                                                <asp:DropDownList ID="ddlWeightUnit" Enabled="false" Visible="true" runat="server"
                                                    AppendDataBoundItems="true" Width="45px">
                                                    <asp:ListItem Text="Unit" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Nos" Value="Nos"></asp:ListItem>
                                                    <asp:ListItem Text="MTR" Value="MTR"></asp:ListItem>
                                                    <asp:ListItem Text="Kg" Value="Kg"></asp:ListItem>
                                                    <asp:ListItem Text="Lit" Value="Lit"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            PO Amount Per Item
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPOAmount" Visible="true" Width="50px" Enabled="false" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            MOC
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("matl")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            HRC
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("hrc")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Upload Date
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbluplodedate" Visible="true" runat="server" Text='<%# Eval("date_of_trans","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="80px" ItemStyle-Width="80px">
                                        <HeaderTemplate>
                                            LSentDate
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSent_Date" Visible="true" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="80px" ItemStyle-Width="80px">
                                        <HeaderTemplate>
                                            LRecDate
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReceived_Date" Visible="true" runat="server" Text='<%# Eval("Received_Date","{0:dd/MM/yyyy HH:MM:ss}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="200px" ItemStyle-Width="200px">
                                        <HeaderTemplate>
                                            Pending Not Required Qty
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;&nbsp;
                                            <asp:HiddenField ID="hdnNotRequired" runat="server"></asp:HiddenField>
                                            <asp:Label ID="lblPendingNotReq" ForeColor="Red" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Wrap="true">
                                        <HeaderTemplate>
                                            Status
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <b>
                                                <asp:Label runat="server" ID="lblStatus" Text='<%# Eval("Status") %>'></asp:Label></b>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Wrap="true">
                                        <HeaderTemplate>
                                            Type
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("Type") %>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Wrap="true">
                                        <HeaderTemplate>
                                            Download Excel
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="hyperLnkExcelFile" Target="_blank"></asp:HyperLink>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Wrap="true">
                                        <HeaderTemplate>
                                            S.T
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("Opr17") %>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Wrap="true" HeaderStyle-Width="200px" ItemStyle-Width="200px">
                                        <HeaderTemplate>
                                            Parent Positions<br />
                                            <span style="font-size: 11px;">(For New Uploads)</span>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("Parent_Positions")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            </asp:GridView>
                            <asp:Panel ID="Panel4" runat="server">
                            </asp:Panel>
                        </asp:Panel>
                        </td> </td> </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="margin-top: 50px;" align="left" colspan="4">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="overlay" runat="server" clientidmode="Static" class="web_dialog_overlay">
                                </div>
                                <div id="dialog" clientidmode="Static" runat="server" class="web_dialog">
                                    <div style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                                        <div style="float: left; width: 100%;">
                                            <div class="web_dialog_title" style="width: 20%; float: left;">
                                                Item History
                                            </div>
                                            <div class="web_dialog_title align_right">
                                                <asp:LinkButton runat="server" ID="btnClosePopUp" CausesValidation="false" ClientIDMode="Static"
                                                    OnClick="btnClosePopUp_Click">Close</asp:LinkButton>
                                            </div>
                                        </div>
                                        <div runat="server" id="pnlHistoryPopUp">
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <script type="text/javascript">
                            Sys.WebForms.PageRequestManager.getInstance().add_pageL
                            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

                            function EndRequestHandler() {
                                GetChallanNumber();
                                GetPONumber();
                                ValidatePO();
                                ValidatePONumber();
                                $('.datepicker').datepicker({
                                    format: 'dd/mm/yyyy'

                                });
                            }
                        </script>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSend" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
