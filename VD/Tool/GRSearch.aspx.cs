﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class Tool_GRSearch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //showgrid();
        }
    }

    /// <summary>
    /// Retrieves the History of Parts based on user search from locaiton info gr and part history Gr table
    /// </summary>
    public void showgrid()
    {
        string s = "";
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(sqlconnstring);

            string searchConditionLocationInfo = "", searchConditionPartHistoryGr = "";

            if (tbTool.Text != "")
            {
                if (searchConditionLocationInfo != "")
                {
                    searchConditionLocationInfo = searchConditionLocationInfo + " and lig.tool_no='" + tbTool.Text + "'";
                    searchConditionPartHistoryGr = searchConditionPartHistoryGr + " and phg.tool_no='" + tbTool.Text + "'";
                }
                else
                {
                    searchConditionLocationInfo = "lig.tool_no='" + tbTool.Text + "'";
                    searchConditionPartHistoryGr = "phg.tool_no='" + tbTool.Text + "'";
                }
            }
            if (tbStation.Text != "")
            {
                if (searchConditionLocationInfo != "")
                {
                    searchConditionLocationInfo = searchConditionLocationInfo + " and lig.station='" + tbStation.Text + "'";
                    searchConditionPartHistoryGr = searchConditionPartHistoryGr + " and phg.station='" + tbStation.Text + "'";
                }
                else
                {
                    searchConditionLocationInfo = " lig.station='" + tbStation.Text + "'";
                    searchConditionPartHistoryGr = " phg.station='" + tbStation.Text + "'";
                }
            }
            if (txtposition.Text != "")
            {
                if (searchConditionLocationInfo != "")
                {
                    searchConditionLocationInfo = searchConditionLocationInfo + " and lig.p_no='" + txtposition.Text + "'";
                    searchConditionPartHistoryGr = searchConditionPartHistoryGr + " and phg.p_no='" + txtposition.Text + "'";
                }
                else
                {
                    searchConditionLocationInfo = " lig.p_no='" + txtposition.Text + "'";
                    searchConditionPartHistoryGr = " phg.p_no='" + txtposition.Text + "'";
                }
            }
            if (tbLocation.Text != "")
            {
                if (searchConditionLocationInfo != "")
                {
                    searchConditionLocationInfo = searchConditionLocationInfo + " and lig.location='" + tbLocation.Text + "'";
                    searchConditionPartHistoryGr = searchConditionPartHistoryGr + " and phg.location='" + tbLocation.Text + "'";
                }
                else
                {
                    searchConditionLocationInfo = " lig.location='" + tbLocation.Text + "'";
                    searchConditionPartHistoryGr = " phg.location='" + tbLocation.Text + "'";
                }
            }
            if (txtGr.Text != "")
            {
                if (searchConditionLocationInfo != "")
                {
                    searchConditionLocationInfo = searchConditionLocationInfo + " and lig.Rwk='" + txtGr.Text + "'";
                    searchConditionPartHistoryGr = searchConditionPartHistoryGr + " and phg.Rwk_No='" + txtGr.Text + "'";
                }
                else
                {
                    searchConditionLocationInfo = " lig.Rwk='" + txtGr.Text + "'";
                    searchConditionPartHistoryGr = " phg.Rwk_No='" + txtGr.Text + "'";
                }
            }

            if (string.IsNullOrEmpty(searchConditionLocationInfo))
            {
                searchConditionLocationInfo = "1=1";
                searchConditionPartHistoryGr = "1=1";
            }

             s = " select lig.tool_no,lig.Rwk,lig.station,lig.p_no,lig.part_name,lig.req_qty,lig.Upload_Qty,lig.location,(isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0)) as qtypending,lig.quant_sent,lig.quant_rec,lig.quant_pend,lig.quant_others, lig.Sent_Received_Date,lig.Received_Date from location_info_GR lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk= pgr.rwk_no where (isnull(lig.quant_sent,0)-ISNULL(lig.quant_rec,0))>0 and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchConditionLocationInfo + " union all "
            + "  select phg.tool_no,phg.rwk_no as Rwk,phg.station, phg.p_no,phg.part_name,(isnull(phg.Upload_Qty,0))as Req_Qty,(isnull(phg.Upload_Qty,0))as Upload_Qty,phg.Location, (isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0)) as qtypending, isnull(phg.QtySent,0) as quant_sent,0 as quant_rec, phg.quantity as quant_pend,0 as quant_others,phg.Sent_Date as 'Sent_Received_Date',phg.Received_Date from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where  phg.location='COMPANY' and phg.date_of_trans is not null and (phg.Sent_Date is null  or  phg.Received_Date is null)  and  phg.quantity>0  and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchConditionPartHistoryGr + "";

            SqlCommand cmd = new SqlCommand(s);
            cmd.CommandType = CommandType.Text;
            conn.Open();
            cmd.Connection = conn;
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            Session["DataTableAdvanceReport"] = dt;
            lblTotal1.Text = dt.Rows.Count.ToString();
            GridView1.DataSource = dt;
            GridView1.DataBind();
            conn.Close();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        DataTable table = (DataTable)Session["DataTableAdvanceReport"];
        GridView1.DataSource = table;
        GridView1.DataBind();
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label result = (Label)e.Row.FindControl("Label60");
            if (result.Text == "1900-01-01 00:00:00.000" || result.Text == "01/01/1900 00:00:00")
            {
                result.Text = "";
            }
        }
    }
}