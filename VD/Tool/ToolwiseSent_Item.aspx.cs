﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Text;

public partial class ToolwiseSent_Item : System.Web.UI.Page
{
    int count = 0;
    string approvername = "";
    int countchk = 0;
    String sent_or_rec, location;

    object Approveid = "";
    string str_dt1 = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlreceived.Visible = false;
            string query = " SELECT distinct  Vendor_Name ,ID FROM Vendor_Master_Job_work where Vendor_Name<>'Assembly' order by Vendor_Name asc ";
            string dataTextField = "VENDOR_NAME";
            string dataValueField = "ID";
            UtilityFunctions.bind_vendor(ddlvendor1, query, dataTextField, dataValueField);

            string selectLocationQuery = "SELECT distinct   [location]  FROM [dbo].[part_history_QC_GR] ";
            string dataText = "location";
            string dataValue = "location";
            UtilityFunctions.bind_vendor(ddlreceived, selectLocationQuery, dataText, dataValue);
            if (tbTool.Text.Length > 0)
            {
                showgrid();
                Panel5.Visible = true;
                ddlvendor1.SelectedItem.Text = "";
                UtilityFunctions.bind_vendor(ddlvendor1, query, dataTextField, dataValueField);
            }
        }
        ddlvendor1.Enabled = true;
        Panel5.Visible = true;
        tbStation.Visible = true;
        tbTool.Visible = true;
        Button1.Visible = true;
    }

    protected void Show_Hide_OrdersGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlOrders").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            GridView gvOrders = row.FindControl("gvOrders") as GridView;

            string customerId = gvOrders.DataKeys[row.RowIndex].Value.ToString();
            string cell_1_Value = gvOrders.Rows[row.RowIndex].Cells[5].Text;
            string station = gvOrders.Rows[row.RowIndex].Cells[2].Text;
            if (tbStation.Text.Length > 0)
            {
                station = tbStation.Text;
            }
            else
            {
                station = "";
            }
            BindOrders(gvOrders, cell_1_Value, station);
        }
        else
        {
            row.FindControl("pnlOrders").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    protected void ddlvendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlvendor1.SelectedItem.Text.Trim() == "Inspection")
        {
            printispectionchallan.Visible = true;
            btnprint.Visible = false;
            tdReceived.Visible = true;
        }
        else
        {
            printispectionchallan.Visible = false;
            btnprint.Visible = true;
            tdReceived.Visible = false;
        }
    }

    private void BindOrders(GridView gvOrders, string challan, string station)
    {
        //gvOrders.ToolTip = customerId;
        DataTable dt = new DataTable();

    }

    protected void Show_Hide_ProductsGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        GridViewRow Gv2Row = (GridViewRow)((ImageButton)sender).NamingContainer;
        GridView Childgrid = (GridView)(Gv2Row.Parent.Parent);
        GridViewRow Gv1Row = (GridViewRow)(Childgrid.NamingContainer);
        int b = Gv1Row.RowIndex;

        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("Panel1").Visible = true;

            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            //int orderId = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Value);
            GridView GridView1 = row.FindControl("GridView1") as GridView;

            string station = Childgrid.Rows[row.RowIndex].Cells[1].Text;
            string tool = Childgrid.Rows[row.RowIndex].Cells[4].Text;

            BindProducts(tool, GridView1, station);
        }
        else
        {
            row.FindControl("Panel1").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    private void BindProducts(string tool, GridView gvOrdersPosition, string station)
    {
        string searchCondition = "";
        SearchCondition(out searchCondition);
        if (!string.IsNullOrEmpty(searchCondition))
        {
            gvOrdersPosition.DataSource = UtilityFunctions.GetData(string.Format(" select phg.p_no,phg.part_name,phg.location,phg.tool_no,phg.date_of_trans,phg.Sent_Date,phg.Received_Date,phg.Batch_No, (isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0)) as PendingQty, (isnull(phg.Upload_Qty,0)) as Upload_Qty,isnull(phg.QtySent,0) as QtySent,phg.quantity,phg.station,phg.date_of_trans,phg.rwk_no from  part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where   phg.location='COMPANY' and  phg.quantity>0  AND  phg.Received_Date is not null  and phg.tool_no='" + tool + "' and phg.station='" + station + "' and " + searchCondition + " and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) "));
        }
        else
        {
            gvOrdersPosition.DataSource = UtilityFunctions.GetData(string.Format(" select phg.p_no,phg.part_name,phg.location,phg.tool_no,phg.date_of_trans,phg.Sent_Date,phg.Received_Date,phg.Batch_No, (isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0)) as PendingQty, (isnull(phg.Upload_Qty,0)) as Upload_Qty,isnull(phg.QtySent,0) as QtySent,phg.quantity,phg.station,phg.date_of_trans,phg.rwk_no from  part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where   phg.location='COMPANY' and  phg.quantity>0  AND  phg.Received_Date is not null  and phg.tool_no='" + tool + "' and phg.station='" + station + "' and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) "));
        }
        gvOrdersPosition.DataBind();
    }

    private void BindProducts_Position(int tool, GridView gvOrdersPosition)
    {
        string searchCondition = "";
        SearchCondition(out searchCondition);
        if (!string.IsNullOrEmpty(searchCondition))
        {
            gvOrdersPosition.DataSource = UtilityFunctions.GetData(string.Format("select count(temp.p_no)TotNoPosition,temp.station,sum(temp.PendingQty) as TotalPendingQty,temp.tool_no from (select phg.tool_no,(isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0)) as PendingQty,phg.station,phg.p_no from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where   phg.location='COMPANY'and  phg.quantity>0  AND  phg.Received_Date is not null and phg.tool_no='" + tool + "' and " + searchCondition + " and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) ) temp group by temp.station,temp.tool_no having sum(temp.PendingQty)>0"));
        }
        else
        {
            gvOrdersPosition.DataSource = UtilityFunctions.GetData(string.Format("select count(temp.p_no)TotNoPosition,temp.station,sum(temp.PendingQty) as TotalPendingQty,temp.tool_no from (select phg.tool_no,(isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0)) as PendingQty,phg.station,phg.p_no from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where   phg.location='COMPANY'and  phg.quantity>0  AND  phg.Received_Date is not null and phg.tool_no='" + tool + "' and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) ) temp group by temp.station,temp.tool_no having sum(temp.PendingQty)>0"));
        }
        gvOrdersPosition.DataBind();
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        showgrid();
        if (gvCustomers.Rows.Count == 1)
        {
            lbl.Text = "";
        }
    }

    private void clear()
    {
        txtexpdate.Text = "";
        txtrequired.Text = "";
        txtdelcdate.Text = "";
        txtpodate.Text = "";
        txtSRREGNO.Text = "";
    }

    protected void SearchCondition(out string searchQuery)
    {

        string searchCondition = "";
        if (tbTool.Text != "")
        {
            if (searchCondition != "")
            {
                searchCondition = searchCondition + " and phg.tool_no='" + tbTool.Text + "'";
            }
            else
            {
                searchCondition = "phg.tool_no='" + tbTool.Text + "'";
            }
        }
        if (tbStation.Text != "")
        {
            if (searchCondition != "")
            {
                searchCondition = searchCondition + " and phg.station='" + tbStation.Text + "'";
            }
            else
            {
                searchCondition = " phg.station='" + tbStation.Text + "'";
            }
        }
        if (txtbatchno.Text != "")
        {
            if (searchCondition != "")
            {
                searchCondition = searchCondition + " and phg.batch_no='" + txtbatchno.Text + "'";
            }
            else
            {
                searchCondition = " phg.batch_no='" + txtbatchno.Text + "'";
            }
        }
        if (txtreworkno.Text != "")
        {
            if (searchCondition != "")
            {
                searchCondition = searchCondition + " and phg.rwk_no='" + txtreworkno.Text + "'";
            }
            else
            {
                searchCondition = " phg.rwk_no='" + txtreworkno.Text + "'";
            }
        }
        searchQuery = searchCondition;
    }

    public void showgrid()
    {
        //FIXME :: verify the query formation , usage of variable count, text field conditions not handled in the query
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();

        clear();
        if (ddlvendor1.SelectedItem.Text.Trim() == "Inspection")
        {
            printispectionchallan.Visible = true;
            btnprint.Visible = false;
        }
        else
        {
            btnprint.Visible = true;
        }
        string query = "";
        try
        {
            string searchCondition = "";
            SearchCondition(out searchCondition);

            if (string.IsNullOrEmpty(searchCondition))
            {
                query = "  select temp.tool_no,sum(temp.PendingQty) as Totalpendingqty from (select phg.tool_no,(isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0)) as PendingQty,isnull(phg.QtySent,0) as QtySent from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where   phg.location='COMPANY'and  phg.quantity>0 AND  phg.Received_Date is not null and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) ) temp group by temp.tool_no";
            }
            else
            {
                query = "  select temp.tool_no,sum(temp.PendingQty) as Totalpendingqty from (select phg.tool_no,(isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0)) as PendingQty,isnull(phg.QtySent,0) as QtySent from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where   phg.location='COMPANY'and  phg.quantity>0 AND  phg.Received_Date is not null and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and " + searchCondition + ") temp  group by temp.tool_no";
            }
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = conn;
            DataTable dt = new DataTable();
            dt.Load(cmd.ExecuteReader());

            if (dt.Rows.Count > 0)
            {
                Session["DataTableAdvanceReport"] = dt;
            }
            gvCustomers.DataSource = dt;
            gvCustomers.DataBind();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    protected void TextBox3_TextChanged(object sender, EventArgs e)
    {
        TextBox imgShowHide = (sender as TextBox);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        GridView Childgrid = (GridView)(row.Parent.Parent);

        foreach (GridViewRow row1 in Childgrid.Rows)
        {
            int count = int.Parse(Childgrid.Rows.Count.ToString());
            for (int i = 0; i < count; i++)
            {
                TextBox TextBox3 = (TextBox)Childgrid.Rows[i].Cells[10].FindControl("TextBox3");
                TextBox txtbalc = (TextBox)Childgrid.Rows[i].Cells[11].FindControl("txtbalc");
                Label Label8 = (Label)Childgrid.Rows[i].Cells[8].FindControl("Label8");
                Label Labelsent = (Label)Childgrid.Rows[i].Cells[9].FindControl("Labelsent");
                Label uploadqty = (Label)Childgrid.Rows[i].Cells[11].FindControl("LabelUpload_Qty");
                txtbalc.Text = (Convert.ToInt32(Label8.Text) - Convert.ToInt32(TextBox3.Text)).ToString();

                txtbalc.Text = ((Convert.ToInt32(uploadqty.Text) - (Convert.ToInt32(TextBox3.Text) + Convert.ToInt32(Labelsent.Text)))).ToString();
            }
        }
    }

    protected void Show_Hide_OrdersGrid(object sender, ImageClickEventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        int tool = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Value);

        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlOrdersStation").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            GridView gvdStation = row.FindControl("gvdStation") as GridView;
            BindProducts_Position(tool, gvdStation);
        }
        else
        {
            row.FindControl("pnlOrdersStation").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    protected void gvdchaalan_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label result = (Label)e.Row.FindControl("Label60");
            string langId = e.Row.Cells[2].Text;

            if (langId == "01/01/1900" || langId == "01/01/1900 00:00:00")
            {
                e.Row.Cells[2].Text = "";
            }
            else
            {

            }
        }
    }

    protected void ddlvendor1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlvendor1.SelectedItem.Text == "VD Finish Store" || ddlvendor1.SelectedItem.Text == "Assembly")
        {
            txtesthrs.Enabled = false;
            txtdelcdate.Enabled = false;
            txtexpdate.Enabled = false;
            txtrequired.Enabled = false;
        }
        if (ddlvendor1.SelectedItem.Text.Trim() == "Inspection")
        {
            printispectionchallan.Visible = true;
            btnprint.Visible = false;
        }
        else
        {
            ddlvendor1.SelectedItem.Text = ddlvendor1.SelectedItem.Text;
            printispectionchallan.Visible = false;
            btnprint.Visible = true;
        }
    }

    [System.Web.Services.WebMethodAttribute(),
   System.Web.Script.Services.ScriptMethodAttribute()]
    public static string GetDynamicContent(string contextKey)
    {
        string toolno = "";
        string Positionno = "";
        string part_name = "";
        string station = "";
        string rwkNo = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string[] List = new string[4];
        List = contextKey.Split(';');

        Positionno = List[0];
        toolno = List[1];
        station = List[2];
        part_name = List[3];
        rwkNo = List[4];

        string query = "select  id,location as Party, CONVERT(VARCHAR(10), date_of_trans, 103)  as Trans_Date,sent_or_rec as Trans_Status,(case sent_or_rec  when 'Uploaded' then  quantity   else NULL  end) as PendingTotQty_in_PPC,TransSentQty as SendQty_Or_ReceivedQty  from part_history_GR  where   p_no='" + Positionno + "' and  tool_no='" + toolno + "' and  station='" + station + "' and  part_name='" + part_name + "' and rwk_no='" + rwkNo + "' order by date_of_trans asc ";

        SqlDataAdapter da = new SqlDataAdapter(query, conn);
        DataTable table = new DataTable();

        da.Fill(table);

        StringBuilder b = new StringBuilder();

        b.Append("<table style='background-color:#f3f3f3; border: #336699 3px solid; ");
        b.Append("width:350px; font-size:10pt; font-family:Verdana;' cellspacing='0' cellpadding='3'>");

        b.Append("<tr><td colspan='6' style='background-color:#336699;text-align:center; color:white;'>");
        b.Append("<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Summary Details</b>"); b.Append("</td></tr>");

        b.Append("<tr>");
        foreach (DataColumn column in table.Columns)
        {
            b.Append("<th>");
            b.Append(column.ColumnName);
            b.Append("</th>");
        }
        b.Append("</tr>");


        foreach (DataRow row in table.Rows)
        {
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<td>");
                b.Append(row[column.ColumnName]);
                b.Append("</td>");
            }
            b.Append("</tr>");
        }

        b.Append("</table>");
        conn.Close();

        return b.ToString();
    }


    protected void btnprint_Click(object sender, EventArgs e)
    {
        Session["MIN"] = txtchalanno.Text;
        if (txtchalanno.Text.Length > 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "window.open('Item_Challan.aspx?ProcessType=Send');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Send some part before Print');", true);
            return;
        }
    }

    protected void printispectionchallan_Click(object sender, EventArgs e)
    {
        if (ddlreceived.SelectedItem.Text != "")
        {
            Session["MIN"] = txtchalanno.Text;
            Session["vendor"] = ddlreceived.SelectedItem.Text;
            Response.Write("<script language=javascript>child=window.open('GetPrintGrChallan.aspx');</script>");
        }
        else 
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Vendor for Inspection');", true);
        }
    }

    protected void btnSend_Click(object sender, System.EventArgs e)
    {
        Button btnSend = (sender as Button);
        GridViewRow row1 = (btnSend.NamingContainer as GridViewRow);
        GridView GridView1 = (GridView)(row1.Parent.Parent);

        btnSend.Attributes.Add("onclick", " this.disabled = true; " + ClientScript.GetPostBackEventReference(btnSend, null) + ";");
        approvername = Session["UserID"].ToString();
        approvername = approvername.Replace("_", " ");

        string prefixStr = "CH";
        //string query = "SELECT MAX(CAST(REPLACE(Challan_No,'" + prefixStr + "','') AS INT))  FROM [dbo].[part_history_GR] where Challan_No like '" + prefixStr + "%'";
        //string query = "select top 1 Replace(Replace(Challan_No,'CH',''),'V_','') from Part_History_Gr where Challan_No is not null order by id Desc";
        txtchalanno.Text = UtilityFunctions.gencode(prefixStr);

        int count1 = 0;

        int tool_no1, quantity1, Qtysent1, sent1;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        SqlTransaction trans = null;

        try
        {
            conn.Open();
            trans = conn.BeginTransaction();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Transaction = trans;
                foreach (GridViewRow row in GridView1.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        LinkButton p_no = row.FindControl("Label1") as LinkButton;
                        Label part_name = row.FindControl("Label2") as Label;
                        Label lblproject = row.FindControl("lblproject") as Label;
                        Label station = row.FindControl("Label10") as Label;
                        Label lblbatchno = row.FindControl("lblbatchno") as Label;
                        Label lblRwk = row.FindControl("Label111") as Label;

                        str_dt1 = string.Format("{0:yyyy-MM-dd}", txtpodate.Text); // you can specify format 
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        string kk = System.DateTime.Now.ToLongTimeString();
                        str_dt1 = str_dt1 + " " + kk;

                        sent_or_rec = "Sent";

                        Label quantity = row.FindControl("LabelUpload_Qty") as Label;
                        TextBox tobeQtysent = row.FindControl("TextBox3") as TextBox;
                        Label sent = row.FindControl("Labelsent") as Label;

                        quantity1 = Convert.ToInt32(quantity.Text);
                        Qtysent1 = Convert.ToInt32(tobeQtysent.Text);
                        sent1 = Convert.ToInt32(sent.Text);
                        tool_no1 = Convert.ToInt32(lblproject.Text);
                        string location1 = ddlvendor1.SelectedItem.Text;

                        if (location1 == "VD Finish Store" || location1 == "Assembly")
                        {
                            bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                            if (isSelected)
                            {
                                int quant_rec = 0;
                                int req_qty = 0;
                                string cmd1 = "insert";

                                Int32 qtysent1 = Convert.ToInt32(Qtysent1);

                                string strQuery2 = "update part_history_GR  set  QtySent=" + qtysent1 + ", Sent_Date='" + str_dt1 + "' , quantity=(isnull(quantity,0) - " + qtysent1 + ") where  p_no='" + p_no.Text + "' and location='COMPANY' and part_name='" + part_name.Text.Trim() + "' and station='" + station.Text + "' and tool_no=" + tool_no1 + " and rwk_no='" + lblRwk.Text + "' ";
                                cmd.CommandText = strQuery2;
                                cmd.ExecuteNonQuery();

                                string s = "insert into part_history_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,Challan_No,Rework,Fresh,TransSentQty,Upload_Qty,Sent_Date,User_name,Used_Date,Rwk_No) values ('" + p_no.Text + "','" + part_name.Text + "', '" + tool_no1 + "','" + station.Text + "','" + location1 + "','" + str_dt1 + "','Sent'," + qtysent1 + "," + Qtysent1 + ",'" + txtchalanno.Text + "','','','" + Qtysent1 + "'," + quantity1 + ",'" + str_dt1 + "','" + approvername + "','" + System.DateTime.Now + "','" + lblRwk.Text + "')";
                                cmd.CommandText = s;
                                cmd.Connection = conn;
                                cmd.ExecuteNonQuery();

                                s = "select quant_sent,quant_rec,req_qty from location_info_GR where location='" + location1 + "' and tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and Rwk='" + lblRwk.Text + "'";
                                cmd.CommandText = s;

                                DataTable dt = new DataTable();
                                dt.Load(cmd.ExecuteReader());

                                if (dt.Rows.Count != 0)
                                {
                                    cmd1 = "update";
                                    quantity1 += Convert.ToInt32(dt.Rows[0][0].ToString());
                                    quant_rec = Convert.ToInt32(dt.Rows[0][1].ToString());
                                    req_qty = Convert.ToInt32(dt.Rows[0][2].ToString());
                                }

                                if (cmd1.Equals("insert"))
                                {
                                    s = "insert into location_info_GR(tool_no,station,p_no,part_name,quant_sent,Sent_Received_Date,Challan_No,Rework,Fresh,TransSentQty,location,req_qty,Upload_Qty,Rwk) values(" + tool_no1 + ",'" + station.Text + "','" + p_no.Text + "','" + part_name.Text + "'," + Qtysent1 + ",'" + str_dt1 + "','" + txtchalanno.Text + "','',''," + qtysent1 + ",'" + location1 + "','" + req_qty + "'," + quantity1 + ",'" + lblRwk.Text + "')";
                                }
                                else
                                {
                                    s = "update location_info_GR set  TransSentQty=" + qtysent1 + ", quant_sent=" + qtysent1 + " + isnull(quant_sent,0)  where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and location='" + location1 + "' and Rwk='" + lblRwk.Text + "'";
                                }
                                cmd.CommandText = s;
                                cmd.ExecuteNonQuery();
                                count1 = count1 + 1;
                            }
                        }
                        else
                        {
                            if (txtesthrs.Text.Length <= 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('please enter estimated hrs');", true);
                                btnSend.Enabled = true;
                                return;
                            }
                            if (txtexpdate.Text.Length <= 0 || txtdelcdate.Text.Length <= 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Commitment or Expected date should not be empty');", true);
                                btnSend.Enabled = true;
                                return;
                            }

                            if (location1.Length <= 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select vendor');", true);
                                btnSend.Enabled = true;
                                return;
                            }

                            //Get the checked value of the CheckBox.
                            bool isSelected = (row.FindControl("chkAllchild") as CheckBox).Checked;
                            if (isSelected && txtpodate.Text.Length > 0)
                            {
                                int req_qty = 0;
                                string s = "";
                                int quant_rec = 0;
                                string command = "insert";
                                int first_location_sent = 0;
                                int quant_others = 0;

                                DataTable dt = new DataTable();

                                if (Qtysent1 > quantity1)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Send Qty Should be less than Total Qty');", true);
                                    btnSend.Enabled = true;
                                    return;
                                }

                                Int32 qtysent1 = Convert.ToInt32(Qtysent1);
                                string strQuery4 = "update part_history_GR  set quantity=(quantity - " + qtysent1 + ") , Sent_Date='" + str_dt1 + "' where location='COMPANY' and  p_no='" + p_no.Text + "' and station='" + station.Text + "' and part_name='" + part_name.Text.Trim() + "' and tool_no=" + tool_no1 + " and rwk_no='" + lblRwk.Text + "'";
                                SqlCommand cmd4 = new SqlCommand(strQuery4, conn, trans);
                                Int32 k1 = cmd4.ExecuteNonQuery();

                                string strQuery2 = "update part_history_GR  set  QtySent=" + qtysent1 + " + isnull(QtySent,0) where p_no='" + p_no.Text + "' and station='" + station.Text + "' and location='COMPANY' and part_name='" + part_name.Text.Trim() + "' and tool_no=" + tool_no1 + "  and rwk_no='" + lblRwk.Text + "'";
                                SqlCommand cmd2 = new SqlCommand(strQuery2, conn, trans);
                                cmd2.ExecuteNonQuery();


                                //TO RETRIEVE REQUIRED QUANTITY OF THE PARTICULAR TOOL FROM parts_GR TABLE
                                s = "select req_qty from parts_GR where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "'  and rwk_no='" + lblRwk.Text + "'";
                                //cmd = new SqlCommand(s, conn);
                                cmd.CommandText = s;
                                cmd.Connection = conn;

                                dt.Load(cmd.ExecuteReader());
                                if (dt.Rows.Count == 0)
                                {
                                    lbl.Text = "No such part exists..!!";
                                    return;
                                }
                                else
                                {
                                    req_qty = Convert.ToInt32(dt.Rows[0][0].ToString());
                                }

                                //TO UPDATE part_history_GR TABLE

                                s = "insert into part_history_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,Explain_Date,Delivery_commit_date,Challan_No,Sr_Reg_No,Batch_No,Required_date,PO_NO,EST_Hrs,Rework,Fresh,TransSentQty,Upload_Qty,Sent_Date,User_name,Used_Date,Rwk_No) values ('" + p_no.Text + "','" + part_name.Text + "', '" + tool_no1 + "','" + station.Text + "','" + location1 + "','" + str_dt1 + "','Sent'," + qtysent1 + "," + Qtysent1 + ",'" + txtexpdate.Text + "','" + txtdelcdate.Text + "','" + txtchalanno.Text + "','" + txtSRREGNO.Text + "','" + lblbatchno.Text + "','" + txtrequired.Text + "','" + txtPONO.Text + "','" + txtesthrs.Text + "','',''," + qtysent1 + "," + quantity1 + ",'" + str_dt1 + "','" + approvername + "','" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "','" + lblRwk.Text + "')";
                                cmd.CommandText = s;
                                cmd.ExecuteNonQuery();

                                if (ddlvendor1.SelectedItem.Text == " Inspection")
                                {
                                    s = "insert into Temp_part_history_QC_GR(p_no,part_name,tool_no,station,location,date_of_trans,sent_or_rec,quantity,QtySent,Explain_Date,Delivery_commit_date,Challan_No,Sr_Reg_No,Batch_No,Required_date,PO_NO,EST_Hrs,Rework,Fresh,TransSentQty,Upload_Qty,Sent_Date,User_name,Used_Date,BillNo,rwk_no) values ('" + p_no.Text + "','" + part_name.Text + "', '" + tool_no1 + "','" + station.Text + "','" + location1 + "','" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','Sent'," + qtysent1 + "," + Qtysent1 + ",'" + txtexpdate.Text + "','" + txtdelcdate.Text + "','" + txtchalanno.Text + "','" + txtSRREGNO.Text + "','" + lblbatchno.Text + "','" + txtrequired.Text + "','" + txtPONO.Text + "','" + txtesthrs.Text + "','',''," + qtysent1 + "," + quantity1 + ",'" + Convert.ToDateTime(str_dt1).ToString("yyyy/MM/dd HH:mm:ss") + "','" + approvername + "','" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "','" + txtbillno.Text + "','" + lblRwk.Text + "')";
                                    cmd.CommandText = s;
                                    cmd.ExecuteNonQuery();
                                }
                                //UPDATING TABLE LOCATION_INFO
                                count1 = count1 + 1;
                                //RETRIEVING TOTAL QUANTITTY SENT OF A PARTICULAR PART TO A PARTICULAR VENDOR
                                s = "select quant_sent,quant_rec from location_info_GR where location='" + location1 + "' and tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and Rwk='" + lblRwk.Text + "'";
                                cmd.CommandText = s;
                                dt = new DataTable();
                                dt.Load(cmd.ExecuteReader());

                                if (dt.Rows.Count != 0)
                                {
                                    command = "update";
                                    quantity1 += Convert.ToInt32(dt.Rows[0][0].ToString());
                                    quant_rec = Convert.ToInt32(dt.Rows[0][1].ToString());
                                }

                                //CALCULATING THE PENDING QUANTITY
                                int quant_pend = quantity1 - quant_rec;

                                //FINDING QUANTITY PENDING WITH PREVIOUS VENDOR

                                //FINDING FIRST VENDOR  and QUANTITY SENT TO HIM
                                s = "select top 1 location from part_history_GR where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and rwk_no='" + lblRwk.Text + "' and sent_or_rec='Sent' order by date_of_trans asc";
                                cmd.CommandText = s;

                                dt = new DataTable();
                                dt.Load(cmd.ExecuteReader());


                                string first_location = dt.Rows[0][0].ToString();
                                if (!first_location.Equals(location))
                                {
                                    s = "select quant_sent from location_info_GR where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and location='" + first_location + "' and rwk='" + lblRwk.Text + "'";
                                    cmd.CommandText = s;

                                    dt = new DataTable();
                                    dt.Load(cmd.ExecuteReader());
                                    if (dt.Rows.Count > 0)
                                    {
                                        first_location_sent = Convert.ToInt32(dt.Rows[0][0].ToString());
                                        //finding quantity pending with first vendor
                                        quant_others = first_location_sent - quantity1;
                                    }
                                }

                                string modsentdate = Convert.ToString(DateTime.Now);
                                //NOW INSERTING INTO location_info_GR TABLE
                                if (command.Equals("insert"))
                                {
                                    s = "insert into location_info_GR values(" + tool_no1 + ",'" + station.Text + "','" + p_no.Text + "','" + part_name.Text + "'," + req_qty + ",'" + location1 + "'," + Qtysent1 + "," + quant_rec + "," + quant_pend + "," + quant_others + ",'" + str_dt1 + "','','" + txtexpdate.Text + "','" + txtdelcdate.Text + "','" + txtchalanno.Text + "','" + txtSRREGNO.Text + "','" + lblbatchno.Text + "','" + txtrequired.Text + "','" + txtPONO.Text + "','','','" + txtesthrs.Text + "'," + qtysent1 + "," + quantity1 + ",'" + modsentdate + "','" + lblRwk.Text + "'," + 0 + ")";
                                }
                                else
                                {
                                    s = "update location_info_GR set  TransSentQty=" + qtysent1 + ", quant_sent=" + qtysent1 + " + isnull(quant_sent,0),quant_pend=" + quant_pend + ",quant_others=" + quant_others + " where tool_no=" + tool_no1 + " and station='" + station.Text + "' and p_no='" + p_no.Text + "' and part_name='" + part_name.Text + "' and location='" + location1 + "' and rwk='" + lblRwk.Text + "'";
                                }
                                cmd.CommandText = s;
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            trans.Commit();
            showgrid();
        }
        catch (Exception ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }

        if (count1 > 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Send Item Successfully');", true);
            btnSend.Enabled = true;
            btnSend.Visible = false;
        }
    }

    protected void chkAllchild_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox imgShowHide = (sender as CheckBox);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);

        GridView Childgrid = (GridView)(row.Parent.Parent);
        CheckBox ChkBoxHeader = (CheckBox)Childgrid.HeaderRow.FindControl("chkAll1");
        Button btnSend = Childgrid.FooterRow.FindControl("btnSend") as Button;

        foreach (GridViewRow row1 in Childgrid.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkAllchild");
            if (ChkBoxRows.Checked == true)
            {
                countchk = countchk + 1;
                Childgrid.Columns[9].Visible = true;
                Childgrid.Columns[10].Visible = true;
                btnSend.Visible = true;
                EnableTextBox(Childgrid);
            }
            else
            {
                ChkBoxRows.Checked = false;
                int count = int.Parse(Childgrid.Rows.Count.ToString());
                for (int i = 0; i < count; i++)
                {
                    CheckBox cb = (CheckBox)Childgrid.Rows[i].Cells[0].FindControl("chkAllchild");
                    if (cb.Checked == true)
                    {
                        countchk = countchk + 1;
                    }
                    Childgrid.Columns[8].Visible = true;
                    Childgrid.Columns[9].Visible = true;
                    TextBox TextBox3 = (TextBox)Childgrid.Rows[i].Cells[9].FindControl("TextBox3");
                    TextBox3.Enabled = false;
                    btnSend.Visible = true;
                }
            }
        }

        if (countchk > 0)
        {
            Childgrid.Columns[10].Visible = true;
            Childgrid.Columns[11].Visible = true;
            btnSend.Visible = true;

        }
        else
        {
            btnSend.Visible = false;
        }
    }

    protected void chkAll1_CheckedChanged1(object sender, EventArgs e)
    {
        CheckBox imgShowHide = (sender as CheckBox);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        GridView Childgrid = (GridView)(row.Parent.Parent);
        Button btnSend = Childgrid.FooterRow.FindControl("btnSend") as Button;

        CheckBox ChkBoxHeader = (CheckBox)Childgrid.HeaderRow.FindControl("chkAll1");
        foreach (GridViewRow row1 in Childgrid.Rows)
        {
            CheckBox ChkBoxRows = (CheckBox)row1.FindControl("chkAllchild");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
                countchk = countchk + 1;
                EnableTextBox(Childgrid);
            }
            else
            {
                ChkBoxRows.Checked = false;

                int count = int.Parse(Childgrid.Rows.Count.ToString());
                EnableTextBox(Childgrid);
            }
        }
        if (countchk > 0)
        {
            Childgrid.Columns[10].Visible = true;
            Childgrid.Columns[11].Visible = true;
            btnSend.Visible = true;
        }
        else
        {
            btnSend.Visible = false;
            Childgrid.Columns[10].Visible = false;
            Childgrid.Columns[11].Visible = false;
        }
    }

    protected void EnableTextBox(GridView GridView1)
    {
        Button btnSend = GridView1.FooterRow.FindControl("btnSend") as Button;
        int count = int.Parse(GridView1.Rows.Count.ToString());

        for (int i = 0; i < count; i++)
        {
            CheckBox cb = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkAllchild");
            if (cb.Checked == true)
            {
                btnSend.Visible = true;
                TextBox TextBox3 = (TextBox)GridView1.Rows[i].Cells[10].FindControl("TextBox3");
                TextBox txtbalc = (TextBox)GridView1.Rows[i].Cells[11].FindControl("txtbalc");
                Label Label8 = (Label)GridView1.Rows[i].Cells[8].FindControl("Label8");
                Label Labelsent = (Label)GridView1.Rows[i].Cells[9].FindControl("Labelsent");
                Label LabelUpload_Qty = (Label)GridView1.Rows[i].Cells[12].FindControl("LabelUpload_Qty");
                txtbalc.Text = (Convert.ToInt32(Label8.Text) - Convert.ToInt32(TextBox3.Text)).ToString();
                txtbalc.Text = ((Convert.ToInt32(LabelUpload_Qty.Text) - (Convert.ToInt32(TextBox3.Text) + Convert.ToInt32(Labelsent.Text)))).ToString();

                TextBox3.Enabled = true;
            }
            else
            {
                TextBox TextBox3 = (TextBox)GridView1.Rows[i].Cells[10].FindControl("TextBox3");
                TextBox3.Enabled = false;
                btnSend.Visible = false;
            }
        }
    }
}