﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="SpareStorePartSummary.aspx.cs" Inherits="Tool_SpareStorePartSummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../css1/style1.css" type="text/css" media="screen" />
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../Scripts/ScrollableGridViewPlugin_ASP.NetAJAXmin.js" type="text/javascript"></script>
    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate >= new Date()) {
                alert("You cannot select a day less than today!");
                sender._selectedDate = new Date();

                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>
    <script type="text/javascript">


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


        var TL;
        var TB;
        var TH;
        var TWt;
        var TAWB;
        var TMID;
        var TimeElapsed;


        function myFunctionL(e) {

            debugger;

            TL = e.timeStamp;
            document.getElementById("tbTool").value = TL;
            debugger;
        }

       
    </script>
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        
        .glowing-border
        {
            border: 2px solid #dadada;
            border-radius: 4px;
        }
        .glowing-border:focus
        {
            outline: none;
            border-color: #9ecaed;
            box-shadow: 0 0 10px #9ecaed;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table style="margin-left: 0px;" align="left" width="100%">
                <tr>
                    <td colspan="3" align="left" height="5px">
                        <asp:ScriptManager ID="ScriptManager2" runat="server">
                        </asp:ScriptManager>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="font-size: x-large; color: black">
                        <span class="style1">Spare Store Part Summary</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel3" runat="server">
                            <table width="940px">
                                <tr>
                                    <td style="font-size: 13px; width: 106px;">
                                        Date: <span class="style12">* </span>
                                        <br />
                                        <asp:TextBox ID="txtpodate" CssClass="glowing-border" runat="server" Width="100px"
                                            AutoPostBack="true" OnTextChanged="txtpodate_TextChanged"></asp:TextBox>
                                        <br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpodate"
                                            Display="None" ErrorMessage="*"></asp:RequiredFieldValidator>
                                        <cc1:CalendarExtender ID="txtpodate_CalendarExtender" runat="server" Enabled="true"
                                            Format="dd-MMM-yyyy" OnClientDateSelectionChanged="checkDate" TargetControlID="txtpodate"
                                            OnClientShowing="CurrentDateShowing">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td style="font-size: 13px; width: 106px;">
                                        Project No<span class="style12"></span>
                                        <br />
                                        <asp:TextBox ID="txtprojectno" CssClass="glowing-border" runat="server" Width="100px"></asp:TextBox>
                                        <br />
                                    </td>
                                    <td style="font-size: 13px; width: 106px;">
                                        Station No<span class="style12"></span>
                                        <br />
                                        <asp:TextBox ID="txtstation" CssClass="glowing-border" runat="server" Width="100px"></asp:TextBox>
                                        <br />
                                    </td>
                                    <td style="font-size: 13px; width: 130px;">
                                        Department<span class="style12"></span>
                                        <br />
                                        <asp:DropDownList ID="ddldepartment" BackColor="#F6F1DB" ForeColor="#7d6754" CssClass="glowing-border"
                                            runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem Selected="True">Assembly</asp:ListItem>
                                            <asp:ListItem>VD Process Store</asp:ListItem>
                                            <asp:ListItem>Store Assembly</asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                    </td>
                                    <td valign="bottom">
                                        <asp:Button ID="Button1" CssClass="glowing-border" CausesValidation="false" runat="server"
                                            Text="Search" OnClick="Button1_Click" />
                                    </td>
                                    <td height="5px">
                                        <asp:Label ID="lbl" Visible="false" runat="server"></asp:Label>
                                        <asp:Label ID="lblChalanno" runat="server"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Button ID="btnSend" runat="server" CausesValidation="true" Text="Send" Visible="false"
                                            Width="100px" OnClick="btnSend_Click" Style="height: 26px; margin-bottom: -30px;" />
                                        &nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnprint" runat="server" Text="PrintChallan" OnClick="btnprintChallan_Click"
                                            CausesValidation="false" Visible="False" Style="margin-bottom: -30px;" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="margin-left: 100px;" align="left">
                        <asp:Panel ID="Panel1" runat="server">
                            <asp:GridView ID="GridView1" runat="server" EmptyDataText="No Record Found" AutoGenerateColumns="False"
                                Style="border: 1px solid #aba4a4;" CellPadding="4" ForeColor="#333333" GridLines="None"
                                Font-Size="13px" Width="100%" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                OnRowEditing="GridView1_RowEditing">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                        ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll1" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll1_CheckedChanged1" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkAllchild" AutoPostBack="true" runat="server" OnCheckedChanged="chkAllchild_CheckedChanged">
                                            </asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                        ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Project No
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblproject" runat="server" Text='<%# Eval("tool_no") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="20px" ItemStyle-Width="50px"
                                        ItemStyle-HorizontalAlign="left">
                                        <HeaderTemplate>
                                            Station No
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label10" runat="server" Text='<%# Eval("station") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                        ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Position No
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("p_no") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                        ItemStyle-HorizontalAlign="left">
                                        <HeaderTemplate>
                                            Batch No
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblbatchno" runat="server" Text='<%# Eval("Batch_No") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="40px" ItemStyle-Width="100px"
                                        ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            GR No
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label111" Visible="true" runat="server" Text='<%# Eval("Rwk") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="left" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="left">
                                        <HeaderTemplate>
                                            Part Name
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("part_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" Visible="false" HeaderStyle-Width="100px"
                                        ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Location
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("location") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Total Quantity
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label8" runat="server" Text='<%# Eval("quant_sent") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Quantity sent
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Labelsent" Visible="false" runat="server" Text='<%# Eval("quant_sent") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Qty To Be send
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox3" runat="server" OnTextChanged="TextBox3_TextChanged" AutoPostBack="true"
                                                Enabled="false" Width="100px" onkeypress="return isNumberKey(event)" Text='<%# Eval("quant_sent") %>'> 
                                            </asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            Upload Qty
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelUpload_Qty" Visible="true" runat="server" Text='<%# Eval("Upload_Qty") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        style="text-align: right; position: fixed; margin-left: 50%; margin-top: 3%;">
        <ProgressTemplate>
            <img src="../images/loading.gif" width="50px" style="float: right;" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
