﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Configuration;

public partial class Tool_VDDepartmentIdleReport : System.Web.UI.Page
{
    public static string searchCondition;
    Image sortImage = new Image();
    private static string sortCondition;
    private static string sortColumn;
    public string SortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
                return string.Empty;
            else
                return ViewState["SortDirection"].ToString();
        }
        set
        {
            ViewState["SortDirection"] = value;
        }
    }
    private string _sortDirection;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    /// <summary>
    /// Binds the Grid
    /// </summary>
    public void showgrid()
    {
        try
        {
            if (inputFromDate.Text != "" || inputToDate.Text != "")
            {
                DateTime dt = DateTime.MinValue;
                List<PartHistory> partList = GetDataByCriteria("PartsSent");
                var result =
                         partList.AsEnumerable()
                             .Select(p => new
                             {
                                 partsSent = p.PartsSentCount,
                                 lateReceived = p.LateReceivedData,
                                 TimelyReceived = p.TimelyReceivedData,
                             })
                             .GroupBy(p => new { })
                             .Select(p => new
                             {
                                 PartsSentCount = p.Sum(x => Convert.ToInt32(x.partsSent)),
                                 SentAfterTwoDays = p.Sum(x => Convert.ToInt32(x.lateReceived)),
                                 SentWithInTwoDays = p.Sum(x => Convert.ToInt32(x.TimelyReceived)),
                             })

                             .ToList();
                girdVendorPerformanceReport.DataSource = result;
                girdVendorPerformanceReport.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertmsg", "alert('Please Select Time Interval or Select From Date or To Date');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

        }
    }

    /// <summary>
    /// All the Data that is received in Parts History Gr table is retrieved for the selected Date Range and it is checked that after how many data data is sent further on that
    /// basis VD department is idle or not is calculated.
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    protected List<PartHistory> GetDataByCriteria(string type)
    {

        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        List<PartHistory> partList = new List<PartHistory>();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        SqlDataAdapter ad = new SqlDataAdapter();
        try
        {
            string searchCondition = "";
            if (checkBoxInternalVendors.Checked && checkBoxEnternalVendors.Checked)
            {
                if (searchCondition != "")
                    searchCondition = searchCondition + " and (vm.vendor_type='End Store' or vm.vendor_type='Internal Vendor' or vm.Vendor_Type='External Vendor')";
                else
                    searchCondition = " (vm.vendor_type='End Store' or vm.vendor_type='Internal Vendor' or vm.Vendor_Type='External Vendor')";
            }
            else if (checkBoxInternalVendors.Checked)
            {
                if (searchCondition != "")
                    searchCondition = searchCondition + " and (vm.vendor_type='End Store' or vm.vendor_type='Internal Vendor')";
                else
                    searchCondition = " (vm.vendor_type='End Store' or vm.Vendor_Type='Internal Vendor')";
            }
            else if (checkBoxEnternalVendors.Checked)
            {
                if (searchCondition != "")
                    searchCondition = searchCondition + " and vm.vendor_type='External Vendor'";
                else
                    searchCondition = " vm.vendor_type='External Vendor'";
            }
            if (inputFromDate.Text != "" && inputToDate.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,phg.date_of_trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,phg.date_of_trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,phg.date_of_trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "' and Convert(Date,phg.date_of_trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
            }
            else if (inputFromDate.Text != "" && inputToDate.Text == "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,phg.date_of_trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,phg.date_of_trans)>='" + Convert.ToDateTime(inputFromDate.Text).ToString("yyyy/MM/dd") + "'";
                }
            }
            else if (inputFromDate.Text == "" && inputToDate.Text != "")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Convert(Date,phg.date_of_trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                else
                {
                    searchCondition = " Convert(Date,phg.date_of_trans)<='" + Convert.ToDateTime(inputToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
            }
            if (searchCondition != "")
            {
                ad = new SqlDataAdapter("select distinct phg.location,phg.tool_no,phg.p_no,phg.part_name,phg.station,phg.rwk_no,phg.Upload_Qty,phg.quantity,phg.id, phg.sent_or_rec, phg.date_of_trans, phg.Delivery_commit_date from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no left join Vendor_Master_Job_work vm on phg.location=vm.Vendor_Name where phg.location not in ('Company','')  and " + searchCondition + " and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and phg.sent_or_rec='Received' order by phg.location,phg.tool_no, phg.p_no, phg.part_name, phg.station, phg.rwk_no, phg.id ", connec);
            }
            else
            {
                ad = new SqlDataAdapter("select distinct phg.location,phg.tool_no,phg.p_no,phg.part_name,phg.station,phg.rwk_no,phg.Upload_Qty,phg.quantity,phg.id, phg.sent_or_rec, phg.date_of_trans, phg.Delivery_commit_date from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no left join Vendor_Master_Job_work vm on phg.location=vm.Vendor_Name where phg.location not in ('Company','') and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and phg.sent_or_rec='Received' order by phg.location,phg.tool_no, phg.p_no, phg.part_name, phg.station, phg.rwk_no, phg.id ", connec);
            };
            DataTable dt = new DataTable();
            ad.Fill(dt);

            SqlCommand cmd = new SqlCommand();
            connec.Open();
            cmd.Connection = connec;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                cmd.Parameters.Clear();
                cmd.CommandText = "select top 1 id,date_of_trans as SentDate from part_history_Gr where sent_or_rec='Sent' and tool_no=@TooNo and p_no=@PNo and part_name=@PartName and station=@Station and rwk_no=@RwkNo and date_of_trans> @DateofTrans order by id asc";
                cmd.Parameters.AddWithValue("@TooNo", Convert.ToInt32(dt.Rows[i]["Tool_No"]));
                cmd.Parameters.AddWithValue("@PNo", Convert.ToString(dt.Rows[i]["P_No"]));
                cmd.Parameters.AddWithValue("@PartName", Convert.ToString(dt.Rows[i]["Part_Name"]));
                cmd.Parameters.AddWithValue("@Station", Convert.ToString(dt.Rows[i]["Station"]));
                cmd.Parameters.AddWithValue("@RwkNo", Convert.ToString(dt.Rows[i]["Rwk_No"]));
                cmd.Parameters.AddWithValue("@DateofTrans", Convert.ToDateTime(dt.Rows[i]["Date_Of_Trans"]));
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.HasRows)
                    {
                        rdr.Read();
                        string idrec = Convert.ToString(dt.Rows[i]["id"]);
                        string idSent = Convert.ToString(rdr["id"]);
                        string sentDate = Convert.ToDateTime(rdr["SentDate"]).ToString("dd-MM-yyyy");
                        string recDate = Convert.ToDateTime(dt.Rows[i]["date_of_trans"]).ToString("dd-MM-yyyy");
                        int days = Convert.ToDateTime(sentDate).Subtract(Convert.ToDateTime(recDate)).Days;
                        PartHistory ph = new PartHistory();
                        ph.Location = Convert.ToString(dt.Rows[i]["Location"]);
                        ph.ToolNo = Convert.ToInt32(dt.Rows[i]["Tool_No"]);
                        ph.PositionNumber = Convert.ToString(dt.Rows[i]["P_No"]);
                        ph.PartName = Convert.ToString(dt.Rows[i]["Part_Name"]);
                        ph.Station = Convert.ToString(dt.Rows[i]["Station"]);
                        ph.ReworkNumber = Convert.ToString(dt.Rows[i]["Rwk_No"]);
                        ph.UploadQuantity = Convert.ToInt32(dt.Rows[i]["Upload_Qty"]);
                        ph.Quantity = Convert.ToInt32(dt.Rows[i]["Quantity"]);
                        ph.DateOfTrans = Convert.ToDateTime(dt.Rows[i]["Date_Of_Trans"]);
                        ph.OutStandingData = Convert.ToInt32(dt.Rows[i]["Quantity"]);
                        ph.ReceivedDate = recDate;
                        ph.SentDate = sentDate;
                        if (dt.Rows[i]["Delivery_commit_date"] != DBNull.Value)
                            ph.DeliveryCommitDate = Convert.ToDateTime(dt.Rows[i]["Delivery_commit_date"]);
                        ph.SentOrRec = Convert.ToString(dt.Rows[i]["Sent_Or_Rec"]);
                        if (days > 2)
                        {
                            ph.PartsSentCount = 1;
                            ph.LateReceivedData = 1;
                        }
                        else
                        {
                            ph.PartsSentCount = 1;
                            ph.TimelyReceivedData = 1;
                        }
                        partList.Add(ph);
                    }
                }

            }
            Session["PartList"] = partList;
            return partList;
        }
        catch (Exception ex)
        {
            ex.ToString();
            Session["PartList"] = null;
            return null;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        showgrid();
    }

    /// <summary>
    /// Pop is displayed to User based on the Count user has clicked.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void girdVendorPerformanceReport_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "PartsSent")
        {
            lblRecordType.Text = "PartsSent";
            gridViewLocationWiseData.DataSource = ((List<PartHistory>)Session["PartList"]);
            gridViewLocationWiseData.DataBind();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
        else if (e.CommandName == "SentAfterTwoDays")
        {
            lblRecordType.Text = "SentAfterTwoDays";
            gridViewLocationWiseData.DataSource = ((List<PartHistory>)Session["PartList"]).FindAll(x => x.LateReceivedData == 1);
            gridViewLocationWiseData.DataBind();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
        else if (e.CommandName == "SentWithInTwoDays")
        {
            lblRecordType.Text = "SentWithInTwoDays";
            gridViewLocationWiseData.DataSource = ((List<PartHistory>)Session["PartList"]).FindAll(x => x.TimelyReceivedData == 1);
            gridViewLocationWiseData.DataBind();
            dialog.Attributes.Add("style", "display:block;");
            overlay.Attributes.Add("style", "display:block;");
        }
    }

    /// <summary>
    /// Sent After 2 Days and Sent With in 2 Days Percetage is Calculated
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void girdVendorPerformanceReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            double totalsentQuantity = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "PartsSentCount"));
            double totalLateReceivedQuantity = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SentAfterTwoDays"));
            double totalTimelyeivedQuantity = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SentWithInTwoDays"));
            Label lblSentAfterTwoDaysPercent = (Label)e.Row.FindControl("lblSentAfterTwoDaysPercent");
            Label lblSentWithInTwoDaysPercent = (Label)e.Row.FindControl("lblSentWithInTwoDaysPercent");
            if (totalLateReceivedQuantity > 0)
            {
                double lateReceivedPercent = (totalLateReceivedQuantity * 100) / totalsentQuantity;
                lblSentAfterTwoDaysPercent.Text = Convert.ToString(Math.Round(lateReceivedPercent, 1));
            }
            else {
                lblSentAfterTwoDaysPercent.Text = "0";
            }
            if (totalTimelyeivedQuantity > 0)
            {
                double TimelyReceivedPercent = (totalTimelyeivedQuantity * 100) / totalsentQuantity;
                lblSentWithInTwoDaysPercent.Text = Convert.ToString(Math.Round(TimelyReceivedPercent, 1));
            }
            else {
                lblSentWithInTwoDaysPercent.Text = "0";
            }
        }
    }

    protected void girdVendorPerformanceReport_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    protected void girdVendorPerformanceReport_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Html is generated for the Passed Datatable
    /// </summary>
    /// <param name="table"></param>
    /// <returns></returns>
    protected string GetHtmlContent(DataTable table)
    {

        StringBuilder b = new StringBuilder();
        b.Append("<table style='");
        b.Append("width:100%; font-size:10pt; font-family:Verdana;margin-top:10px;line-height:28px;' cellspacing='0' cellpadding='3'>");
        b.Append("<tr>");
        foreach (DataColumn column in table.Columns)
        {
            b.Append("<th>");
            b.Append(column.ColumnName);
            b.Append("</th>");
        }
        b.Append("</tr>");


        foreach (DataRow row in table.Rows)
        {
            b.Append("<tr>");
            foreach (DataColumn column in table.Columns)
            {
                b.Append("<td>");
                b.Append(row[column.ColumnName]);
                b.Append("</td>");
            }
            b.Append("</tr>");
        }
        b.Append("</table>");
        return b.ToString();
    }

    protected void btnClosePopUp_Click(object sender, EventArgs e)
    {
        dialog.Attributes.Add("style", "display:none;");
        overlay.Attributes.Add("style", "display:none;");
    }


    protected void ExportToExcel()
    {
        Response.ClearContent();
        string fileName = "CommitmentDateData";
        string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        girdVendorPerformanceReport.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    protected void ApplySortingImage(string sortExpression)
    {
        int columnIndex = 0;
        foreach (DataControlFieldHeaderCell headerCell in gridViewLocationWiseData.HeaderRow.Cells)
        {
            if (headerCell.ContainingField.SortExpression == sortExpression)
            {
                columnIndex = gridViewLocationWiseData.HeaderRow.Cells.GetCellIndex(headerCell);
            }
        }
        gridViewLocationWiseData.HeaderRow.Cells[columnIndex].Controls.Add(sortImage);
    }

    protected void SetSortDirection(string sortDirection)
    {
        if (sortDirection.ToLower() == "asc")
        {
            _sortDirection = "DESC";
            sortImage.ImageUrl = "../Images/view_sort_ascending.png";

        }
        else
        {
            _sortDirection = "ASC";
            sortImage.ImageUrl = "../Images/view_sort_descending.png";
        }
    }

    protected void gridViewLocationWiseData_Sorting(object sender, GridViewSortEventArgs e)
    {
        // gridBagList.PageIndex = 0;
        SetSortDirection(SortDirection);
        sortCondition = e.SortExpression + " " + _sortDirection;
        sortColumn = e.SortExpression;

        if (string.IsNullOrEmpty(sortCondition))
        {
            sortCondition = "ToolNo ASC";
        }

        var result = new List<PartHistory>();
        if (lblRecordType.Text == "PartsSent")
        {
            result = ((List<PartHistory>)Session["PartList"]);
        }
        else if (lblRecordType.Text == "SentAfterTwoDays")
        {
            result = ((List<PartHistory>)Session["PartList"]).FindAll(x => x.LateReceivedData == 1);
        }
        else if (lblRecordType.Text == "SentWithInTwoDays")
        {
            result = ((List<PartHistory>)Session["PartList"]).FindAll(x => x.TimelyReceivedData == 1);
        }

        string[] splitCondition = sortCondition.Split(' ');
        if (splitCondition[1].ToLower() == "asc")
        {
            result = result.OrderBy(r => r.GetType().GetProperty(splitCondition[0]).GetValue(r, null)).ToList();
        }
        else if (splitCondition[1].ToLower() == "desc")
        {
            result = result.OrderByDescending(r => r.GetType().GetProperty(splitCondition[0]).GetValue(r, null)).ToList();
        }
        gridViewLocationWiseData.DataSource = result;
        gridViewLocationWiseData.DataBind();

        SortDirection = _sortDirection;
        ApplySortingImage(e.SortExpression);

    }
}
