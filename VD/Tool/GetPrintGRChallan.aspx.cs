﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class Tool_GetPrintGRChallan : System.Web.UI.Page
{
    String MIN = "";
    string location = "";
    SqlConnection mycon;
    SqlCommand cmd;
    int Total = 0;
    SqlDataAdapter adp;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadQCData();
        }
    }

    /// <summary>
    /// Generates the QC Sheet. Retrieves the Data on the basis of Passed Vendor for Inspection and Challan No is Type as QC is not provided.
    /// If Type as QC is passed then generates the QCsheet for the passed value of QCsheet. 
    /// </summary>
    protected void LoadQCData()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        SqlTransaction trans = null;
        try
        {
            con.Open();
            trans = con.BeginTransaction();
            LblDt.Text = System.DateTime.Now.ToString("dd-MMM-yyyy");
            MIN = Session["MIN"].ToString();
            string query = "";
            if (Convert.ToString(Request.QueryString["Type"]) == "QC")
            {
                LblDt.Text = System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");//code changed by himaneesh conflict due to MM for month and mm for minute
                query = "select * from(select  ROW_NUMBER()  over(partition by [p_no]  ,BillNo,[part_name],[station], quantity  ,tool_no   ,QtySent,Challan_No,date_of_trans order by location Desc)  as  RowNum ,[Po_No], [p_no] , BillNo, [part_name],[station],  rwk_no  , quantity  , tool_no   , QtySent, Challan_No, date_of_trans ,location"
                         + " from (SELECT distinct Temp_Part_QC.[po_no], Temp_Part_QC.[p_no]  , Temp_Part_QC.BillNo,  Temp_Part_QC.[part_name], Temp_Part_QC.[station],  parts.rwk_no  ,  Temp_Part_QC.quantity  , Temp_Part_QC.tool_no   , Temp_Part_QC.QtySent,  Temp_Part_QC.Challan_No, Temp_Part_QC.date_of_trans , QCPart_His.location"
                         + " FROM    Temp_part_history_QC_GR Temp_Part_QC  inner join parts_gr parts  on parts.tool_no=Temp_Part_QC.tool_no and parts.p_no=Temp_Part_QC.[p_no]  and parts.station=Temp_Part_QC.station  and parts.part_name=Temp_Part_QC.part_name and  parts.rwk_no= Temp_Part_QC.rwk_no "
                         + " inner join    part_history_QC_GR  QCPart_His  on QCPart_His.p_no=Temp_Part_QC.p_no  and QCPart_His.part_name=Temp_Part_QC.part_name   and QCPart_His.station=Temp_Part_QC.station "
                         + " and QCPart_His.quantity= Temp_Part_QC.quantity where  Temp_Part_QC.Qcsheet_no='" + MIN + "' )myTempTable)a where RowNum=1";
                Label1.Text = MIN;
            }
            else
            {
                location = Session["vendor"].ToString();

                int k = showData1();
                k = k + 1;
                Label1.Text = k.ToString();

                SqlCommand cmdTran = new SqlCommand();
                cmdTran.Connection = con;
                cmdTran.Transaction = trans;
                cmdTran.CommandText = "insert into part_history1(Challan_No) values('" + MIN + "')";
                cmdTran.ExecuteNonQuery();

                if (MIN.StartsWith("CH") || MIN.StartsWith("V_"))
                {
                    Label1.Text = "QCSheet" + Label1.Text.ToString();
                }
                else if (MIN.StartsWith("GR"))
                {
                    Label1.Text = "GRQCSheet" + Label1.Text.ToString();
                }
                cmdTran.CommandText = "update  temp_part_history_QC_GR set Qcsheet_no='" + Label1.Text + "'where Challan_No='" + MIN + "'";
                cmdTran.ExecuteNonQuery();
                trans.Commit();
                query = "SELECT distinct tphqc_gr.po_no, tphqc_gr.[p_no],tphqc_gr.BillNo,tphqc_gr.[part_name],tphqc_gr.[station],tphqc_gr.quantity,"
                + " tphqc_gr.tool_no ,tphqc_gr.QtySent,phq_gr.location,tphqc_gr.Challan_No,tphqc_gr.date_of_trans, phg.rwk_no FROM part_history_QC_GR phq_gr "
                + " inner join Temp_part_history_QC_GR tphqc_gr on tphqc_gr.p_no=phq_gr.p_no and phq_gr.part_name=tphqc_gr.part_name and phq_gr.station=tphqc_gr.station "
                + " inner join part_history_GR phg on phg.p_no=phq_gr.p_no and phg.location=phq_gr.location and phg.station=phq_gr.station and phg.part_name = phq_gr.part_name "
                + " where tphqc_gr.QtySent=phq_gr.quantity and tphqc_gr.Challan_No='" + MIN + "' and phq_gr.location='" + location + "' and phg.[Rwk_No]=tphqc_gr.[Rwk_No]";
                //query = "SELECT distinct   Temp_Part_QC.[p_no]  ,Temp_Part_QC.BillNo, Temp_Part_QC.[part_name]   ,Temp_Part_QC.[station],Temp_Part_QC.quantity  ,Temp_Part_QC.tool_no   ,Temp_Part_QC.QtySent,QCPart_His.location,Temp_Part_QC.Challan_No   ,Temp_Part_QC.date_of_trans, part_history_GR.rwk_no  FROM part_history_QC_GR QCPart_His inner join Temp_part_history_QC_GR Temp_Part_QC on Temp_Part_QC.p_no=QCPart_His.p_no and QCPart_His.part_name=Temp_Part_QC.part_name and QCPart_His.station=Temp_Part_QC.station inner join part_history_GR Part_History_gr on Part_History_gr.p_no=QCPart_His.p_no and Part_History_gr.location=QCPart_His.location and Part_History_gr.station=QCPart_His.station and Part_History_gr.part_name = QCPart_His.part_name  where Temp_Part_QC.QtySent=QCPart_His.quantity and Temp_Part_QC.Challan_No='" + MIN + "' and QCPart_His.location='" + location + "' and Part_History_gr.[Rwk_No]=Temp_Part_QC.[Rwk_No]";
            }
            Session["DataTableAdvanceReport"] = UtilityFunctions.GetData(query);
            ExportGridView();
        }
        catch (Exception ex)
        {
            trans.Rollback();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Some Error Occured. Please contact Administrator');", true);
            ex.ToString();
        }
        finally
        {
            con.Close();
        }
    }

    public int showData1()
    {
        int id = 0;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        conn.Open();
        string conString = "SELECT isnull(MAX(id),0) as id  FROM part_history1 ";
        cmd = new SqlCommand(conString, conn);
        adp = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        adp.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            id = Convert.ToInt16(dt.Rows[0][0].ToString());
        }
        return id;
    }

    /// <summary>
    /// Assigns the Values for the Labels and headings. Also Binds the GridView for the data in the Session Variable (DataTableAdvanceReport) which is assigned on Page Load
    /// </summary>
    private void ExportGridView()
    {
        try
        {
            int count = 0;
            string dirpath = AppDomain.CurrentDomain.BaseDirectory;
            DataTable dt1 = new System.Data.DataTable();
            DataRow dr2 = null;
            dt1.Columns.Add(new DataColumn("Sno", typeof(string)));
            dt1.Columns.Add(new DataColumn("Tool No", typeof(string)));
            dt1.Columns.Add(new DataColumn("Station No", typeof(string)));
            dt1.Columns.Add(new DataColumn("Rework No", typeof(string)));
            dt1.Columns.Add(new DataColumn("Position No", typeof(string)));

            dt1.Columns.Add(new DataColumn("Total Incoming Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("Ok Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("Rejected Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("Parts Sent to party for Rework Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("Inhouse Rework Qty", typeof(string)));
            dt1.Columns.Add(new DataColumn("inhouse rework hours planned", typeof(string)));
            dt1.Columns.Add(new DataColumn("Est Inspection Time Hr", typeof(string)));
            dt1.Columns.Add(new DataColumn("Checked By", typeof(string)));


            dr2 = dt1.NewRow();
            dr2["Sno"] = 0;
            dr2["Tool No"] = string.Empty;
            dr2["Station No"] = string.Empty;
            dr2["Rework No"] = string.Empty;
            dr2["Position No"] = string.Empty;
            dr2["Total Incoming Qty"] = string.Empty;
            dr2["Ok Qty"] = string.Empty;
            dr2["Rejected Qty"] = string.Empty;
            dr2["Parts Sent to party for Rework Qty"] = string.Empty;
            dr2["Inhouse Rework Qty"] = string.Empty;
            dr2["inhouse rework hours planned"] = string.Empty;

            dt1.Rows.Add(dr2);

            DataTable dt = (DataTable)Session["DataTableAdvanceReport"];

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                dr2 = dt1.NewRow();
                dt1.Rows.Add(dr2);
                dt1.Rows[i]["Sno"] = i + 1;
                DateTime currentdate = DateTime.Now;
                string currentdate1 = currentdate.ToString("dd/MM/yyyy");
                lblpartyname.Text = dt.Rows[i]["location"].ToString();
                lblchallanno2.Text = dt.Rows[i]["Challan_No"].ToString();
                lblbillno.Text = dt.Rows[i]["BillNo"].ToString();
                lblPONumber.Text = dt.Rows[i]["PO_No"].ToString();
                dt1.Rows[i]["Tool No"] = dt.Rows[i]["tool_no"];
                dt1.Rows[i]["Station No"] = dt.Rows[i]["station"].ToString();
                dt1.Rows[i]["Position No"] = dt.Rows[i]["p_no"].ToString();
                dt1.Rows[i]["Rework No"] = dt.Rows[i]["rwk_no"].ToString();
                dt1.Rows[i]["Total Incoming Qty"] = dt.Rows[i]["QtySent"].ToString();
                Total = Total + Convert.ToInt32(dt.Rows[i]["QtySent"].ToString());
                count = i;
            }

            count = count + 2;
            dr2 = dt1.NewRow();

            dt1.Rows.Add(dr2);
            dt1.Rows[count - 1][3] = "Total Qty";
            dt1.Rows[count - 1][4] = Total;

            dt1.Rows[count][0] = "Details of Inhouse Rework Planned";
            count = count + 1;
            dr2 = dt1.NewRow();
            dt1.Rows.Add(dr2);
            dt1.Rows[count][0] = "Estimated Hours";

            count = count + 1;
            dr2 = dt1.NewRow();
            dt1.Rows.Add(dr2);

            dt1.Rows[count][0] = "Tool No";
            dt1.Rows[count][1] = "station no";
            dt1.Rows[count][2] = "Part No";
            dt1.Rows[count][3] = "Description";
            dt1.Rows[count][4] = "Qty";
            dt1.Rows[count][5] = "ML";
            dt1.Rows[count][6] = "SG";
            dt1.Rows[count][7] = "TUR";
            dt1.Rows[count][8] = "BW";
            dt1.Rows[count][9] = "fab";
            dt1.Rows[count][10] = "est.hr";

            for (int i = 1; i <= 35 - 1; i++)
            {
                dr2 = dt1.NewRow();
                dt1.Rows.Add(dr2);
            }

            if (dt1.Rows.Count > 0)
            {
                Grdview1.DataSource = dt1;
                Grdview1.DataBind();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

    }

    /// <summary>
    /// Manipulates the GridView Cells on the basis of Cell Value
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Grdview1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[3].Text == "Total Qty")
            {
                e.Row.Cells[3].Font.Bold = true;

                e.Row.Cells[4].Font.Bold = true;
                e.Row.BackColor = System.Drawing.Color.LemonChiffon;
            }

            if (e.Row.Cells[0].Text == "Details of Inhouse Rework Planned")
            {
                e.Row.Cells[1].ColumnSpan = 3;
                e.Row.Cells[0].ColumnSpan = 12;
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                //now make up for the colspan from cell2
                e.Row.Cells.RemoveAt(1);
                e.Row.Cells.RemoveAt(2);
                e.Row.Cells.RemoveAt(3);
                e.Row.Cells.RemoveAt(4);
                e.Row.Cells.RemoveAt(5);
                //e.Row.Cells.RemoveAt(6);

                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                //e.Row.Cells[6].Visible = false;

            }

            if (e.Row.Cells[0].Text == "Estimated Hours")
            {

                e.Row.Cells[0].ColumnSpan = 13;
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells.RemoveAt(1);
                e.Row.Cells.RemoveAt(2);
                e.Row.Cells.RemoveAt(3);
                e.Row.Cells.RemoveAt(4);
                e.Row.Cells.RemoveAt(5);
                //e.Row.Cells.RemoveAt(6);
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;

            }
        }

    }

    protected void Grdview1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string kk = e.Row.Cells[0].Text;
            if (e.Row.Cells[0].Text == "Estimated Hours")
            {

                e.Row.Cells[0].ColumnSpan = 12;
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells.RemoveAt(1);
                e.Row.Cells.RemoveAt(2);
                e.Row.Cells.RemoveAt(3);
                e.Row.Cells.RemoveAt(4);
                e.Row.Cells.RemoveAt(5);
                e.Row.Cells.RemoveAt(6);
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;

            }
        }

    }
}

