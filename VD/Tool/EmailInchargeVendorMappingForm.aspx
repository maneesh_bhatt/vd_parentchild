﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPage.master" AutoEventWireup="true"
    CodeFile="EmailInchargeVendorMappingForm.aspx.cs" Inherits="Tool_EmailInchargeVendorMappingForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .style14
        {
            width: 1034px;
        }
        .style12
        {
            color: #CC3300;
        }
        .style13
        {
            font-size: x-large;
        }
        .control-container
        {
            width: 100%;
            height: 54px;
        }
        .label
        {
            width: 18%;
            float: left;
            padding-left: 10px;
        }
        .input-control-container
        {
            width: 60%;
            float: left;
        }
        .input-control
        {
            width: 58%;
            height: 26px;
        }
        .control-container-multiline
        {
            width: 100%;
            height: 90px;
        }
        .input-control-multiline
        {
            width: 58%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div style="border: 1px solid Blue; min-height: 100px; padding: 9px; float: left;
        width: 98.5%;">
        <div>
            <div align="center">
                <span class="style13">Incharge Vendor Mapping Form</span>
            </div>
        </div>
        <div style="width: 100%; margin-bottom: 60px; margin-top: 20px;">
            <div style="font-size: 13px; float: left;">
                <asp:Label runat="server" ID="lblMessage"></asp:Label>
            </div>
            <asp:HyperLink ID="HyperLink1" Style="background-color: #335599; float: right; color: White;
                padding: 5px; margin-top: 9px;" runat="server" NavigateUrl="~/Tool/EmailInchargeVendorMappingAdmin.aspx">View Email Incharge Vendor Mapping</asp:HyperLink>
        </div>
        <div class="control-container">
            <div class="label">
                Email Incharge <span style="color: Red;">*</span></div>
            <div class="input-control-container">
                <asp:DropDownList ID="ddlEmailIncharge" AppendDataBoundItems="true" BackColor="#F6F1DB"
                    Width="58.5%" Height="30px" ForeColor="#7d6754" CssClass="input-control" runat="server">
                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="control-container-multiline">
            <div class="label">
                Vendor List <span style="color: Red;">*</span></div>
            <div class="input-control-container">
                <asp:CheckBoxList ID="checkVendorList" runat="server" RepeatColumns="5" Style="font-size: 12px;
                    line-height: 30px;">
                </asp:CheckBoxList>
            </div>
        </div>
        <div class="control-container">
            <div class="label">
            </div>
            <div class="input-control-container" style="margin-left: 234px;">
                <asp:ImageButton ImageUrl="~/images/summit.jpg" ID="btnsave" Style="width: 20%;"
                    OnClick="btnSubmit_Click" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
