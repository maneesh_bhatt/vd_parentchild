﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Tool_VendorDaysMasterGrid : System.Web.UI.Page
{
    public static string searchCondition;
    protected void Page_Load(object sender, EventArgs e)
    {
        searchCondition = "";

        try
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void BindGrid()
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            if (Convert.ToString(ViewState["SearchCondition"]) != "")
            {
                adapter = new SqlDataAdapter("select * from Vendor_Days_Master where " + ViewState["SearchCondition"], con);
            }
            else
            {
                adapter = new SqlDataAdapter("select * from Vendor_Days_Master", con);
            }

            DataTable td = new DataTable();
            adapter.Fill(td);
            gridVendor.DataSource = td;
            gridVendor.DataBind();
            gridVendor.EmptyDataText = "No Record Found";
            lblRecorrdsCount.Text = Convert.ToString(td.Rows.Count) + " " + "Records In Total";
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            con.Close();
        }

    }

    protected void gridVendor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // Set the index of the new display page.  
        gridVendor.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //gridPackages.PageIndex = 1;
        searchCondition = "";
        if (inputVendorName.Text != "")
        {
            searchCondition = "Vendor_Name ='" + inputVendorName.Text + "'";
        }

        ViewState["SearchCondition"] = searchCondition;
        BindGrid();
    }

    protected void gridVendor_PageIndexChanging_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            Response.Redirect("VendorDaysMasterForm.aspx?ID=" + e.CommandArgument);
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetVendorName(string text)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection con = new SqlConnection(sqlconnstring);
        try
        {
            con.Open();
            List<string> allItems = new List<string>();
            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrWhiteSpace(text))
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work where Vendor_Name like '" + text + "%' and Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ", con);
            }
            else
            {
                cmd = new SqlCommand("select distinct Vendor_Name from Vendor_Master_Job_Work where Vendor_Name <>'Assembly' and Vendor_Name<>'Spare Store' order by Vendor_Name asc ", con);
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string vendorName = Convert.ToString(reader["Vendor_Name"]);
                allItems.Add(vendorName);
            }
            reader.Close();
            return allItems;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return null;
        }
        finally
        {
            con.Close();
        }
    }

}

