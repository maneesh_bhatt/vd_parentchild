﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data;
using System.IO;
using System.Configuration;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Threading;
using System.Text;


public partial class Tool_GenerateProcessPlanningSheet : System.Web.UI.Page
{
    int count1 = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }

    }

    protected void Hide()
    {
        hypDownloadFile.Visible = false;
    }

    /// <summary>
    /// Validates the File Format is Valid and then calls the GetDataTableFromCsv funtion to process data
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UploadButton_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Thread th = new Thread(Hide);
            th.Start();

            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                }
            }

            if (!FileUploadControl.HasFile)
            {
                lbl.Text = "You have not selected any file !!!";
                return;
            }
            string datetime = DateTime.Now.Day + "" + DateTime.Now.Month + "" + DateTime.Now.Year + "" + DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
            string fileName = Path.GetFileNameWithoutExtension(FileUploadControl.PostedFile.FileName) + "_" + datetime + "" + Path.GetExtension(FileUploadControl.PostedFile.FileName);
            string fileExtension = Path.GetExtension(FileUploadControl.PostedFile.FileName);
            string fileLocation = Server.MapPath("~/UploadedExcelFiles/" + fileName);
            FileUploadControl.SaveAs(fileLocation);

            if (!(fileExtension == ".xls") && !(fileExtension == ".xlsx"))
            {
                lbl.Text = "Invalid file !!! Select only an excel file";
                return;
            }

            GetDataTableFromCsv(fileLocation);
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.ToString(), ex.StackTrace);
        }
    }

    /// <summary>
    /// Inserts the data into Process_Planning_Request_Master to generate unique Request Id for Uploaded File then inserts Excel Rows into Process_Planning_Part_Master along with
    /// generated request Id, also inserts the operations and its sequence into Part_Operation_Master
    /// </summary>
    /// <param name="path"></param>
    public void GetDataTableFromCsv(string path)
    {
        object request_id = "";

        string p_no = "";
        DataTable unsuccessfulyrow = new DataTable();
        int req_qty = 0;
        string part_name = "", matl = "", size = "", hrc = "", remark = "";
        string tool_no = "";
        string station = "", rwk_no = "", make = "", modelNumber = "", vrNo = "", length = "", width = "", typeOfCosting = "", rms = "", rm_qty = "", wall_thick = "", angle1 = "", angle2 = "";
        DateTime targetDate = DateTime.MinValue;
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        SqlTransaction trans = null;
        try
        {

            lblErrorMessage.Text = String.Empty;
            conn.Open();
            trans = conn.BeginTransaction();
            string pathOnly = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);
            Microsoft.Office.Interop.Excel.Application excelApp = null;
            excelApp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook workbook;
            Microsoft.Office.Interop.Excel.Worksheet worksheet;
            Microsoft.Office.Interop.Excel.Range range;
            workbook = excelApp.Workbooks.Open(path);
            DataTable dt = new DataTable();

            worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets[1];// read data from first sheet of excel file
            worksheet.Unprotect("<><");
            Microsoft.Office.Interop.Excel.Range last = worksheet.Cells.SpecialCells(Microsoft.Office.Interop.Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
            range = worksheet.get_Range("A1", last);
            int row = 1;
            range = worksheet.UsedRange;
            SqlCommand cmd = new SqlCommand();

            cmd = new SqlCommand("insert into Process_Planning_Request_Master (User_Id,Created_Date) Values(@UserID,@CreatedDate); select Scope_Identity();", conn, trans);
            cmd.Parameters.AddWithValue("@UserID", Session["UserID"]);
            cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
            int currentRequestId = Convert.ToInt32(cmd.ExecuteScalar());
            ViewState["CurrentRequestId"] = currentRequestId;
            for (row = 2; row <= range.Rows.Count; row++)
            {
                List<string> operationList = new List<string>();
                if ((range.Cells[row, 1] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    p_no = (range.Cells[row, 1] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 2] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    req_qty = Convert.ToInt32((range.Cells[row, 2] as Microsoft.Office.Interop.Excel.Range).Value2);
                }
                if ((range.Cells[row, 3] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    part_name = (range.Cells[row, 3] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 4] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    matl = (range.Cells[row, 4] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 5] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    hrc = (range.Cells[row, 5] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 6] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    make = (range.Cells[row, 6] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 7] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    modelNumber = (range.Cells[row, 7] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 8] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    vrNo = (range.Cells[row, 8] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 9] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    size = (range.Cells[row, 9] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 10] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    remark = (range.Cells[row, 10] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 11] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    length = (range.Cells[row, 11] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 12] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    width = (range.Cells[row, 12] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 13] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    typeOfCosting = (range.Cells[row, 13] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 14] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    tool_no = (range.Cells[row, 14] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                    station = tool_no.Substring(tool_no.Length - 1, 1);
                    tool_no = tool_no.Substring(0, tool_no.Length - 1);
                }
                if ((range.Cells[row, 15] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    rwk_no = (range.Cells[row, 15] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }
                if ((range.Cells[row, 16] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 16] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }
                if ((range.Cells[row, 17] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 17] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }
                if ((range.Cells[row, 18] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 18] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }
                if ((range.Cells[row, 19] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 19] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }
                if ((range.Cells[row, 20] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 20] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }
                if ((range.Cells[row, 21] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 21] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }
                if ((range.Cells[row, 22] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 22] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }
                if ((range.Cells[row, 23] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 23] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }
                if ((range.Cells[row, 24] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 24] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }

                if ((range.Cells[row, 25] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 25] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }

                if ((range.Cells[row, 26] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 26] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }

                if ((range.Cells[row, 27] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 27] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }

                if ((range.Cells[row, 28] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 28] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }

                if ((range.Cells[row, 29] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    operationList.Add((range.Cells[row, 29] as Microsoft.Office.Interop.Excel.Range).Value2.ToString());
                }

                if ((range.Cells[row, 30] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    rms = (range.Cells[row, 30] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }

                if ((range.Cells[row, 31] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    rm_qty = (range.Cells[row, 31] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }

                if ((range.Cells[row, 32] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    wall_thick = (range.Cells[row, 32] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                }

                if ((range.Cells[row, 33] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    angle1 = (range.Cells[row, 33] as Microsoft.Office.Interop.Excel.Range).Value.ToString();
                }
                if ((range.Cells[row, 34] as Microsoft.Office.Interop.Excel.Range).Value2 != null)
                {
                    angle2 = (range.Cells[row, 34] as Microsoft.Office.Interop.Excel.Range).Value.ToString();
                }


                part_name = part_name.Replace("  ", "");
                part_name = part_name.Replace(" ", "");

                station = station.Replace("  ", "");
                station = station.Replace(" ", "");

                if ((p_no.StartsWith("1") || p_no.StartsWith("2")))//p_no.Length == 3 && 
                {
                    if (tool_no != "" && p_no != "" && station != "")
                    {
                        string strQuery = "";
                        if (part_name.Length > 0 && tool_no != "")
                        {
                            strQuery = "INSERT INTO Process_Planning_Part_Master( P_No,Part_Name,Tool_No,Quantity,Station,Material,Size,Make,Model_Number,Length,Width,Process_Planning_Request_Master_Id)  VALUES ('" + p_no.ToString() + "','" + part_name.Trim() + "','" + tool_no + "','" + req_qty + "','" + station + "','" + matl + "','" + size + "','" + make + "','" + modelNumber + "','" + length + "','" + width + "'," + ViewState["CurrentRequestId"] + "); select Scope_Identity();";
                            cmd = new SqlCommand(strQuery, conn, trans);
                            int insertedId = Convert.ToInt32(cmd.ExecuteScalar());

                            if (operationList.Count > 0)
                            {
                                int sequence = 1;
                                foreach (string operation in operationList)
                                {
                                    string[] operationSplit = operation.Split('-');
                                    string operationName = "";
                                    double optTimeCheck = 0;
                                    string opTime = "";
                                    if (operationSplit.Length > 1)
                                    {
                                        if (operation.StartsWith("ST"))
                                        {
                                            operationName = operationSplit[0];
                                            opTime = operationSplit[1];
                                        }
                                        else
                                        {
                                            bool result = double.TryParse(operationSplit[1].ToString().TrimEnd(' '), out optTimeCheck);
                                            if (result)
                                            {
                                                operationName = operationSplit[0];
                                            }
                                            else
                                            {
                                                operationName = operation;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        operationName = operationSplit[0];
                                    }

                                    if (Convert.ToString(optTimeCheck) != "0")
                                    {
                                        opTime = Convert.ToString(optTimeCheck);
                                    }
                                    strQuery = "INSERT INTO Part_Operation_Master(Process_Planning_Part_Id,Operation_Name,Operation_Time,Sequence,Process_Planning_Request_Master_Id)  VALUES (" + insertedId + ",'" + operationName + "','" + opTime + "'," + sequence + "," + ViewState["CurrentRequestId"] + ")";
                                    cmd = new SqlCommand(strQuery, conn, trans);
                                    cmd.ExecuteNonQuery();
                                    sequence = sequence + 1;
                                }
                            }
                        }
                    }
                }
            }

            trans.Commit();
            //bool isSuccessfulSequencing = GetDataBySequences();
            bool isSccessfulWrite = ExportToCSV();// WriteDataToExcel();
            if (isSccessfulWrite)
            {
                ClearData();
                MessageBox("File Uploaded Successfully");
            }
            else
            {
                MessageBox("Some Error occured while processing your request.");
            }
        }
        catch (Exception ex)
        {
            trans.Rollback();
            ClearData();
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.ToString(), ex.StackTrace);
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }
 
    /// <summary>
    /// Writes the data into Excel from the inserted tables during upload
    /// </summary>
    /// <returns></returns>
    protected bool ExportToCSV()
    {
        bool isSuccessful = false;
        try
        {
            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            using (SqlConnection connec = new SqlConnection(sqlconnstring))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (StreamWriter sw = new StreamWriter(ms, Encoding.UTF8))
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("Rework No,");
                        sb.Append("STN,");
                        sb.Append("Pos. No,");
                        sb.Append("Qty,");
                        sb.Append("Part Description,");
                        sb.Append("Material,");
                        sb.Append("Finish Size,");
                        sb.Append("WT Finish,");
                        sb.Append("B. No.,");
                        sb.Append("Raw Material Size,");
                        sb.Append("RM Qty,");
                        sb.Append("ADD Mat Cost,");
                        sb.Append("PT,");
                        sb.Append("P/GR,");
                        sb.Append("Loading,");
                        sb.Append("Date,");

                        connec.Open();
                        SqlCommand cmd = new SqlCommand("select * from dbo.Part_Operation_Sequence order by Part_Operation_Sequence_Id ASC", connec);
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.HasRows)
                            {
                                while (rdr.Read())
                                {
                                    sb.Append(Convert.ToString(rdr["Operation_Name"]));
                                    sb.Append(",");
                                }
                            }
                        }
                        sb.Length = sb.Length - 1;
                        sb.AppendLine();

                        SqlDataAdapter ad = new SqlDataAdapter("select * from Process_Planning_Part_Master where Process_Planning_Request_Master_Id=" + ViewState["CurrentRequestId"] + "", connec);
                        DataTable dt = new DataTable();
                        ad.Fill(dt);

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            SqlCommand cmdOp = new SqlCommand();
                            sb.Append(",");
                            sb.Append(Convert.ToString(dt.Rows[i]["Station"] + ","));
                            sb.Append(Convert.ToString(dt.Rows[i]["P_No"] + ","));
                            sb.Append(Convert.ToString(dt.Rows[i]["Quantity"] + ","));
                            sb.Append(Convert.ToString(dt.Rows[i]["Part_Name"] + ","));
                            sb.Append(Convert.ToString(dt.Rows[i]["Material"] + ","));
                            sb.Append(Convert.ToString(dt.Rows[i]["Size"] + ","));
                            sb.Append(",");
                            sb.Append(",");
                            sb.Append(",");
                            sb.Append(",");
                            sb.Append(",");
                            int partOperationId = 0;
                            string ptColumnValue = "";

                            //For PT
                            cmdOp.Parameters.Clear();
                            cmdOp = new SqlCommand("select * from Part_Operation_Master where Operation_Name=@Operation and Process_Planning_Part_Id=@ProcessPlanningPartId and (Is_Already_Read=0 or Is_Already_Read is null) Order By Sequence ASC", connec);
                            cmdOp.Parameters.AddWithValue("@Operation", "PT");
                            cmdOp.Parameters.AddWithValue("@ProcessPlanningPartId", dt.Rows[i]["Process_Planning_Part_Id"]);
                            using (SqlDataReader rdr = cmdOp.ExecuteReader())
                            {
                                if (rdr.HasRows)
                                {
                                    rdr.Read();
                                    partOperationId = Convert.ToInt32(rdr["Part_Operation_Master_Id"]);
                                    ptColumnValue = Convert.ToString(rdr["Operation_Time"]);
                                    if (Convert.ToString(ptColumnValue) == "" || Convert.ToString(ptColumnValue) == "0")
                                    {
                                        ptColumnValue = "N";
                                    }
                                    else
                                    {
                                        ptColumnValue = "Y";
                                    }
                                }
                                else
                                {
                                    ptColumnValue = "N";
                                }
                            }
                            if (partOperationId > 0)
                            {
                                cmd.Parameters.Clear();
                                cmd.CommandText = "update Part_Operation_Master set Is_Already_Read=1 where Part_Operation_Master_Id=" + partOperationId;
                                cmd.ExecuteNonQuery();
                            }
                            sb.Append("\"" + ptColumnValue + "\"");
                            sb.Append(",");

                            //For P/GR Column
                            partOperationId = 0;
                            ptColumnValue = "";
                            cmdOp.Parameters.Clear();
                            cmdOp = new SqlCommand("select * from Part_Operation_Master where Operation_Name=@Operation and Process_Planning_Part_Id=@ProcessPlanningPartId and (Is_Already_Read=0 or Is_Already_Read is null) Order By Sequence ASC", connec);
                            cmdOp.Parameters.AddWithValue("@Operation", "PGR");
                            cmdOp.Parameters.AddWithValue("@ProcessPlanningPartId", dt.Rows[i]["Process_Planning_Part_Id"]);
                            using (SqlDataReader rdr = cmdOp.ExecuteReader())
                            {
                                if (rdr.HasRows)
                                {
                                    rdr.Read();
                                    partOperationId = Convert.ToInt32(rdr["Part_Operation_Master_Id"]);
                                    ptColumnValue = Convert.ToString(rdr["Operation_Time"]);
                                    if (Convert.ToString(ptColumnValue) == "" || Convert.ToString(ptColumnValue) == "0")
                                    {
                                        ptColumnValue = "N";
                                    }
                                    else
                                    {
                                        ptColumnValue = "Y";
                                    }
                                }
                                else
                                {
                                    ptColumnValue = "N";
                                }
                            }
                            if (partOperationId > 0)
                            {
                                cmd.Parameters.Clear();
                                cmd.CommandText = "update Part_Operation_Master set Is_Already_Read=1 where Part_Operation_Master_Id=" + partOperationId;
                                cmd.ExecuteNonQuery();
                            }
                            sb.Append("\"" + ptColumnValue + "\"");
                            sb.Append(",");


                            sb.Append(",");
                            sb.Append(",");

                            SqlDataAdapter adp = new SqlDataAdapter("select * from dbo.Part_Operation_Sequence order by Part_Operation_Sequence_Id ASC", connec);
                            DataTable dtable = new DataTable();
                            adp.Fill(dtable);
                            for (int j = 0; j < dtable.Rows.Count; j++)
                            {
                                string operationTime = "";

                                int partOperationMasterId = 0;
                                if (Convert.ToString(dtable.Rows[j]["Operation_Name"]) == "PIPC/INSP" || Convert.ToString(dtable.Rows[j]["Operation_Name"]) == "INSP")
                                {
                                    bool isExist = false;
                                    int pipcCount = 0;
                                    int inspCouunt = 0;
                                    cmdOp = new SqlCommand("select * from Part_Operation_Master where Operation_Name=@Operation and Process_Planning_Part_Id=@ProcessPlanningPartId and (Is_Already_Read=0 or Is_Already_Read is null) Order By Sequence ASC", connec);
                                    cmdOp.Parameters.AddWithValue("@Operation", "PIPC");
                                    cmdOp.Parameters.AddWithValue("@ProcessPlanningPartId", dt.Rows[i]["Process_Planning_Part_Id"]);
                                    using (SqlDataReader oprdr = cmdOp.ExecuteReader())
                                    {
                                        if (oprdr.HasRows)
                                        {
                                            isExist = true;
                                            oprdr.Read();
                                            pipcCount = 1;
                                            partOperationMasterId = Convert.ToInt32(oprdr["Part_Operation_Master_Id"]);
                                            operationTime = Convert.ToString(oprdr["Operation_Time"]);
                                        }
                                    }
                                    if (!isExist)
                                    {
                                        int rowsCount = 0;
                                        cmd.Parameters.Clear();
                                        cmdOp = new SqlCommand("select Count(Part_Operation_Master_Id) as RecordCount from Part_Operation_Master where Operation_Name=@Operation and Process_Planning_Part_Id=@ProcessPlanningPartId", connec);
                                        cmdOp.Parameters.AddWithValue("@Operation", "INSP");
                                        cmdOp.Parameters.AddWithValue("@ProcessPlanningPartId", dt.Rows[i]["Process_Planning_Part_Id"]);
                                        using (SqlDataReader oprdr = cmdOp.ExecuteReader())
                                        {
                                            if (oprdr.HasRows)
                                            {
                                                oprdr.Read();
                                                if (oprdr["RecordCount"] != DBNull.Value)
                                                {
                                                    rowsCount = Convert.ToInt32(oprdr["RecordCount"]);
                                                }
                                            }
                                        }

                                        if (rowsCount == 1 && Convert.ToString(dtable.Rows[j]["Operation_Name"]) == "PIPC/INSP")
                                        {
                                            //If there is only one operation then value should not been shown on PIPC/INSP Column
                                        }
                                        else
                                        {
                                            cmdOp.Parameters.Clear();
                                            cmdOp = new SqlCommand("select * from Part_Operation_Master where Operation_Name=@Operation and Process_Planning_Part_Id=@ProcessPlanningPartId  and (Is_Already_Read=0 or Is_Already_Read is null) Order By Sequence ASC", connec);
                                            cmdOp.Parameters.AddWithValue("@Operation", "INSP");
                                            cmdOp.Parameters.AddWithValue("@ProcessPlanningPartId", dt.Rows[i]["Process_Planning_Part_Id"]);
                                            using (SqlDataReader oprdr = cmdOp.ExecuteReader())
                                            {
                                                if (oprdr.HasRows)
                                                {

                                                    oprdr.Read();
                                                    partOperationMasterId = Convert.ToInt32(oprdr["Part_Operation_Master_Id"]);
                                                    operationTime = Convert.ToString(oprdr["Operation_Time"]);
                                                    inspCouunt = 1;
                                                }
                                            }
                                        }

                                    }
                                }
                                else if (Convert.ToString(dtable.Rows[j]["Operation_Name"]) == "CNC TU/CNC ML")
                                {
                                    bool isExist = false;
                                    //int pipcCount = 0;
                                    //int inspCouunt = 0;
                                    cmdOp = new SqlCommand("select * from Part_Operation_Master where Operation_Name=@Operation and Process_Planning_Part_Id=@ProcessPlanningPartId and (Is_Already_Read=0 or Is_Already_Read is null) Order By Sequence ASC", connec);
                                    cmdOp.Parameters.AddWithValue("@Operation", "CNC ML");
                                    cmdOp.Parameters.AddWithValue("@ProcessPlanningPartId", dt.Rows[i]["Process_Planning_Part_Id"]);
                                    using (SqlDataReader oprdr = cmdOp.ExecuteReader())
                                    {
                                        if (oprdr.HasRows)
                                        {
                                            isExist = true;
                                            oprdr.Read();
                                            partOperationMasterId = Convert.ToInt32(oprdr["Part_Operation_Master_Id"]);
                                            operationTime = Convert.ToString(oprdr["Operation_Time"]);
                                        }
                                    }
                                    if (!isExist)
                                    {
                                        cmdOp = new SqlCommand("select * from Part_Operation_Master where Operation_Name=@Operation and Process_Planning_Part_Id=@ProcessPlanningPartId  and (Is_Already_Read=0 or Is_Already_Read is null) Order By Sequence ASC", connec);
                                        cmdOp.Parameters.AddWithValue("@Operation", "CNC TU");
                                        cmdOp.Parameters.AddWithValue("@ProcessPlanningPartId", dt.Rows[i]["Process_Planning_Part_Id"]);
                                        using (SqlDataReader oprdr = cmdOp.ExecuteReader())
                                        {
                                            if (oprdr.HasRows)
                                            {
                                                oprdr.Read();
                                                partOperationMasterId = Convert.ToInt32(oprdr["Part_Operation_Master_Id"]);
                                                operationTime = Convert.ToString(oprdr["Operation_Time"]);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    cmdOp.Parameters.Clear();
                                    cmdOp = new SqlCommand("select * from Part_Operation_Master where Operation_Name=@Operation and Process_Planning_Part_Id=@ProcessPlanningPartId and (Is_Already_Read=0 or Is_Already_Read is null) Order By Sequence ASC", connec);
                                    cmdOp.Parameters.AddWithValue("@Operation", dtable.Rows[j]["Operation_Name"]);
                                    cmdOp.Parameters.AddWithValue("@ProcessPlanningPartId", dt.Rows[i]["Process_Planning_Part_Id"]);
                                    using (SqlDataReader rdr = cmdOp.ExecuteReader())
                                    {
                                        if (rdr.HasRows)
                                        {
                                            rdr.Read();
                                            partOperationMasterId = Convert.ToInt32(rdr["Part_Operation_Master_Id"]);
                                            operationTime = Convert.ToString(rdr["Operation_Time"]);
                                            if (Convert.ToString(dtable.Rows[j]["Operation_Name"]).StartsWith("PT") || Convert.ToString(dtable.Rows[j]["Operation_Name"]).StartsWith("PGR"))
                                            {
                                                if (Convert.ToString(operationTime) == "" || Convert.ToString(operationTime) == "0")
                                                {
                                                    operationTime = "";//N
                                                }
                                                else
                                                {
                                                    operationTime = "";//Y
                                                }
                                            }

                                        }
                                        else
                                        {
                                            if (Convert.ToString(dtable.Rows[j]["Operation_Name"]).StartsWith("PT") || Convert.ToString(dtable.Rows[j]["Operation_Name"]).StartsWith("PGR"))
                                            {
                                                operationTime = "";
                                            }
                                        }
                                    }
                                }

                                if (partOperationMasterId > 0)
                                {
                                    cmd.Parameters.Clear();
                                    cmd.CommandText = "update Part_Operation_Master set Is_Already_Read=1 where Part_Operation_Master_Id=" + partOperationMasterId;
                                    cmd.ExecuteNonQuery();
                                }
                                sb.Append("\"" + operationTime + "\"");
                                sb.Append(",");

                            }
                            sb.Length = sb.Length - 1;
                            sb.AppendLine();
                        }

                        sw.Write(sb);

                        byte[] bytes = Encoding.Default.GetBytes(sb.ToString());
                        if (bytes != null)
                        {
                            Response.Clear();
                            Response.ContentType = "text/csv";
                            Response.AddHeader("Content-Length", bytes.Length.ToString());
                            string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
                            Response.AddHeader("Content-disposition", "attachment; filename=\"" + dateTime + ".csv" + "\"");
                            Response.BinaryWrite(bytes);
                            Response.Flush();
                            Response.End();
                            isSuccessful = true;
                        }
                    }
                }
            }
            return isSuccessful;
        }
        catch (Exception ex)
        {
            ex.ToString();
            ClearData();
            isSuccessful = false;
            return isSuccessful;
        }


    }

    private void MessageBox(String msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'> { window.alert('" + msg.Replace("'", "`").Replace("''", "``") + "');}</script>";
        Page.Controls.Add(lbl);
    }

    /// <summary>
    /// Deletes the data when Upload File is generated on the basis of Uploaded Process Planning Sheet
    /// </summary>
    protected void ClearData()
    {

        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection connec = new SqlConnection(sqlconnstring);
        try
        {
            connec.Open();
            SqlCommand cmd = new SqlCommand("delete from Process_Planning_Request_Master where Process_Planning_Request_Master_Id=" + ViewState["CurrentRequestId"] + ";delete from Part_Operation_Master where Process_Planning_Request_Master_Id=" + ViewState["CurrentRequestId"] + ";delete from Process_Planning_Part_Master where Process_Planning_Request_Master_Id=" + ViewState["CurrentRequestId"] + ";", connec);
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            ex.ToString();
            ClearData();
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.ToString(), ex.StackTrace);

        }
        finally
        {
            connec.Close();
        }
    }
}