﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PartHistory
/// </summary>
public class PartHistory
{
    public PartHistory()
    {

    }

    public string Location { get; set; }
    public int ToolNo { get; set; }
    public string PositionNumber { get; set; }
    public string PartName { get; set; }
    public string Station { get; set; }
    public string ReworkNumber { get; set; }
    public int UploadQuantity { get; set; }
    public int Quantity { get; set; }
    public int QuantitySent { get; set; }
    public string SentOrRec { get; set; }
    public DateTime DateOfTrans { get; set; }
    public DateTime DeliveryCommitDate { get; set; }
    public int TimelyReceivedData { get; set; }
    public int LateReceivedData { get; set; }
    public int OutStandingData { get; set; }
    public string ReceivedDate { get; set; }
    public string DelayDays { get; set; }
    public int PartsSentCount { get; set; }
    public string SentDate { get; set; }
    public int UploadPartsCount { get; set; }
    public string TargetDate { get; set; }
    public int OutStandingCount { get; set; }

}