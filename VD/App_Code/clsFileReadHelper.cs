﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelDataReader;
using CsvHelper;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;

namespace FileReader
{
    public class clsFileReadHelper : IDisposable
    {
        private string _filePath = string.Empty;
        private SafeHandle handle = null;
        private bool disposed = false; // to detect redundant calls
        public clsFileReadHelper(string filePath)
        {
            this._filePath = filePath;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (handle != null)
                    {
                        handle.Dispose();
                    }
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        public DataSet getFileData()
        {
            DataSet result = null;
            IExcelDataReader excelReader = null;
            FileStream stream = null;
            try
            {
                stream = File.Open(this._filePath, FileMode.Open, FileAccess.Read);
                switch (Path.GetExtension(this._filePath))
                {
                    case ".xls":
                        excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                        break;
                    case ".xlsx":
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        break;
                    //case ".csv":
                    //  CsvHelper.CsvReader objCsv=new CsvReader()

                }
                result = excelReader.AsDataSet();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (result != null)
                    result.Dispose();
                if (excelReader != null)
                    excelReader.Dispose();
                if (stream != null)
                    stream.Close();
            }
        }
    }
}
