﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices;
using System.IO;
using System.Web;

public class Users
{

    public Users()
    {
    }

    public string UserName { get; set; }
    public string Email { get; set; }
    public string DisplayName { get; set; }
    public string Department { get; set; }
    public string EmpCode { get; set; }

    public List<Users> GetADUsers()
    {
        List<Users> lstADUsers = new List<Users>();
        try
        {

            string DomainPath = "LDAP://192.168.1.254/CN=USERS,DC=falconautoonline,DC=com";
            DirectoryEntry searchRoot = new DirectoryEntry("192.168.1.254");
            DirectorySearcher search = new DirectorySearcher(searchRoot);


            //DirectoryEntry myLdapConnection = createDirectoryEntry();
            //DirectorySearcher search = new DirectorySearcher(myLdapConnection);
            //search.Filter = "(samaccountname=" + user + ")";

            // create an array of properties that we would like and  
            // add them to the search object  

            //string[] requiredProperties = new string[] { "mail", "Department", "Password" };
            string[] requiredProperties = new string[] { "Department" };

            foreach (String property in requiredProperties)
                search.PropertiesToLoad.Add(property);

            search.Filter = "(&(samaccountname))";
            search.PropertiesToLoad.Add("samaccountname");

            SearchResult result;
            SearchResultCollection resultCol = search.FindAll();
            if (resultCol != null)
            {
                for (int counter = 0; counter < resultCol.Count; counter++)
                {
                    string UserNameEmailString = string.Empty;
                    result = resultCol[counter];
                    if (result.Properties.Contains("samaccountname") &&
                             result.Properties.Contains("mail") &&
                        result.Properties.Contains("displayname"))
                    {
                        Users objSurveyUsers = new Users();

                        objSurveyUsers.UserName = (String)result.Properties["samaccountname"][0];
                        lstADUsers.Add(objSurveyUsers);
                    }
                }
            }

        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        return lstADUsers;
    }


    public static List<Users> LoadUsersList()
    {
        List<Users> lstADUsers = new List<Users>();
        try
        {
            string ip = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Pages/LDapIP.txt"));
            string DomainPath = "LDAP://" + ip + "/CN=Users,DC=falconautoonline,DC=com";
            DirectoryEntry searchRoot = new DirectoryEntry(DomainPath, Convert.ToString(HttpContext.Current.Session["UserID"]), Convert.ToString(HttpContext.Current.Session["pass"])); //
            DirectorySearcher search = new DirectorySearcher(searchRoot);
            search.Filter = "(&(objectClass=user)(objectCategory=person))";
            search.PropertiesToLoad.Add("samaccountname");
            search.PropertiesToLoad.Add("mail");
            search.PropertiesToLoad.Add("usergroup");
            search.PropertiesToLoad.Add("displayname");//first name
            search.PropertiesToLoad.Add("description");//first name
            search.PropertiesToLoad.Add("department");//first name
            SearchResult result;
            SearchResultCollection resultCol = search.FindAll();
            if (resultCol != null)
            {
                for (int counter = 0; counter < resultCol.Count; counter++)
                {
                    string UserNameEmailString = string.Empty;
                    result = resultCol[counter];
                    if (result.Properties.Contains("samaccountname") &&
                             result.Properties.Contains("mail") &&
                        result.Properties.Contains("displayname") &&
                             result.Properties.Contains("description") &&
                        result.Properties.Contains("department"))
                    {
                        Users objSurveyUsers = new Users();
                        objSurveyUsers.Email = (String)result.Properties["mail"][0];
                        objSurveyUsers.UserName = (String)result.Properties["samaccountname"][0];
                        objSurveyUsers.DisplayName = (String)result.Properties["displayname"][0];
                        objSurveyUsers.Department = (String)result.Properties["department"][0];
                        objSurveyUsers.EmpCode = (String)result.Properties["description"][0];
                        lstADUsers.Add(objSurveyUsers);
                    }
                }
            }
            return lstADUsers;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return lstADUsers;
        }

    }
}

