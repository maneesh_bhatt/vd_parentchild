﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PartOperations
/// </summary>
public class PartOperations
{
    public PartOperations()
    {

    }

    public int PartsGrId { get; set; }
    public string OperationSeqName { get; set; }
    public string OperationValue { get; set; }
    public string OperationActualName { get; set; }
    public int OperationsSequence { get; set; }
    public string TotalDuration { get; set; }
}