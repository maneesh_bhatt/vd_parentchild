﻿using System;
using System.Text;
using System.IO;
using System.Security.AccessControl;
using System.DirectoryServices;
using System.Security.Principal;
using System.Collections;
  

namespace activeDirectoryLdapExamples
{
    public class Program
    {
        string depart = "";
       public  Program()
        {
        }
         

        public ArrayList searchuserList()
        {
            ArrayList userlist = new ArrayList();
            try
            {
                
               
                DirectoryEntry myLdapConnection = createDirectoryEntry();
                DirectorySearcher search = new DirectorySearcher(myLdapConnection);
                search.Filter = "(&(objectClass=user)(objectCategory=person))";

                // create an array of properties that we would like and  
                // add them to the search object  
                //string[] requiredProperties = new string[] { "mail", "Department", "Password" };
                string[] requiredProperties = new string[] { "displayname" };

                foreach (String property in requiredProperties)
                    search.PropertiesToLoad.Add(property);

                SearchResultCollection results = search.FindAll();

                foreach(SearchResult result in results)
                {
                    if (result != null)
                    {
                        foreach (String property in requiredProperties)
                            foreach (Object myCollection in result.Properties[property])
                                userlist.Add(myCollection.ToString());
                                //depart = myCollection.ToString();
               
                    }
                }
               
            }

            catch (Exception e)
            {
                Console.WriteLine("Exception caught:\n\n" + e.ToString());
            }

            return userlist;
        }

        public string searchuser(string user, string pass)
        {

            try
            {
                DirectoryEntry myLdapConnection = createDirectoryEntry();
                DirectorySearcher search = new DirectorySearcher(myLdapConnection);
                search.Filter = "(samaccountname=" + user + ")";

                // create an array of properties that we would like and  
                // add them to the search object  

                string[] requiredProperties = new string[] { "Department" };
                foreach (String property in requiredProperties)
                    search.PropertiesToLoad.Add(property);

                SearchResult result = search.FindOne();

                if (result != null)
                {
                    foreach (String property in requiredProperties)
                        foreach (Object myCollection in result.Properties[property])
                            depart = myCollection.ToString();

                }

            }

            catch (Exception e)
            {
                Console.WriteLine("Exception caught:\n\n" + e.ToString());
            }

            return depart;
        }


        public string searchpurchaseuser(string Dept)
        {

            try
            {
                DirectoryEntry myLdapConnection = createDirectoryEntry();
                DirectorySearcher search = new DirectorySearcher(myLdapConnection);
                search.Filter = "(Department=" + Dept + ")";

                // create an array of properties that we would like and  
                // add them to the search object  

                //string[] requiredProperties = new string[] { "mail", "Department", "Password" };
                string[] requiredProperties = new string[] { "displayname" };

                foreach (String property in requiredProperties)
                    search.PropertiesToLoad.Add(property);

                SearchResult result = search.FindOne();

                if (result != null)
                {
                    foreach (String property in requiredProperties)
                        foreach (Object myCollection in result.Properties[property])
                            depart = myCollection.ToString();

                }

            }

            catch (Exception e)
            {
                Console.WriteLine("Exception caught:\n\n" + e.ToString());
            }

            return depart;
        }

        static DirectoryEntry createDirectoryEntry()
        {
            // create and return new LDAP connection with desired settings  
            DirectoryEntry ldapConnection = new DirectoryEntry("192.168.1.254");
            ldapConnection.Path = "LDAP://192.168.1.254/CN=USERS,DC=falconautoonline,DC=com";
            ldapConnection.AuthenticationType = AuthenticationTypes.Secure;
            return ldapConnection;
        }

        public  bool IsAuthenticated(string ldap, string usr, string pwd)
        {
            bool authenticated = false;

            try
            {
             
                DirectoryEntry entry = new DirectoryEntry(ldap, usr, pwd);
                
                object nativeObject = entry.NativeObject;
                authenticated = true;
            }
            catch (DirectoryServicesCOMException cex)
            {
                Console.WriteLine(cex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return authenticated;
        }
    }
}