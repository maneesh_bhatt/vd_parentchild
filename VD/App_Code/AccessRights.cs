﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data.SqlClient;

namespace CustomFunctions
{
    public class AccessRights
    {

        public static bool[] SetUserAccessRights()
        {
            bool[] rights = new bool[6];

            string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
            SqlConnection con = new SqlConnection(sqlconnstring);
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from Vendor_Control_Rights where User_Name=@UserName", con);
                cmd.Parameters.AddWithValue("@UserName", Convert.ToString(HttpContext.Current.Session["UserID"]));
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        rights[0] = Convert.ToBoolean(reader["Add_Rights"]);
                        rights[1] = Convert.ToBoolean(reader["Edit_Rights"]);
                        rights[2] = Convert.ToBoolean(reader["Delete_Rights"]);
                        rights[3] = Convert.ToBoolean(reader["View_Rights"]);
                        rights[4] = Convert.ToBoolean(reader["Download_Rights"]);
                        rights[5] = Convert.ToBoolean(reader["Upload_Rights"]);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                con.Close();
            }
            return rights;
        }
    }
}