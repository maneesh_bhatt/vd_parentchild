﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Reflection;
using ClosedXML.Excel;
using System.IO;

public static class UtilityFunctions
{
    private static Object thisLock = new Object();
    public static DataTable GetData(string query)
    {
        string constr = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = query;
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataSet ds = new DataSet())
                    {
                        DataTable dt = new DataTable();
                        sda.Fill(dt);

                        return dt;
                    }
                }
            }
        }
    }

    public static void bind_vendor(DropDownList ddlloc, String query, String dataTextField, String dataValueField)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable request_det_head = new DataTable();
            da.Fill(request_det_head);
            if (request_det_head.Rows.Count > 0)
            {
                ddlloc.DataSource = request_det_head;
                ddlloc.DataTextField = dataTextField;
                ddlloc.DataValueField = dataValueField;
                ddlloc.DataBind();
                ddlloc.Items.Insert(0, new ListItem(String.Empty, String.Empty));
                ddlloc.SelectedIndex = 0;
                ddlloc.Visible = true;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
    }

    public static void executeQuery(string query)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);

        try
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        finally
        {
            conn.Close();
        }
    }

    public static string gencode(string str)
    {
        string finalChallanNo = "";
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        try
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("Challan_Number_Generator_GenerateChallanNumber", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                if (rdr.HasRows)
                {
                    rdr.Read();
                    finalChallanNo = str + "" + Convert.ToString(rdr["ChallanNumber"]);
                }
            }

        }
        catch (Exception ex)
        {
            ErrorLogger.LogError(MethodInfo.GetCurrentMethod().DeclaringType.Name, MethodInfo.GetCurrentMethod().Name, ex.Message, ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }
        return finalChallanNo;
    }

    public static DataTable GetDataForVendorStatusReport(string vendorName, string supervisorName, string selectedVendorType)
    {
        string sqlconnstring = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(sqlconnstring);
        DataTable dt = new DataTable();
        try
        {
            string searchCondition = "";
            if (vendorName != "" && vendorName != "All")
            {
                searchCondition = " Vendor_Name='" + vendorName + "'";
            }
            if (supervisorName != "" && supervisorName != "All")
            {
                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Allocated_To='" + supervisorName + "'";
                }
                else
                {
                    searchCondition = " Allocated_To='" + supervisorName + "'";
                }
            }
            if (selectedVendorType != "" && selectedVendorType != "All")
            {
                string vendorType = "";
                if (selectedVendorType == "Int")
                    vendorType = "Internal Vendor";
                else if (selectedVendorType == "Ext")
                    vendorType = "External Vendor";

                if (searchCondition != "")
                {
                    searchCondition = searchCondition + " and Vendor_Type='" + vendorType + "'";
                }
                else
                {
                    searchCondition = " Vendor_Type='" + vendorType + "'";
                }
            }
            SqlDataAdapter ad = new SqlDataAdapter();
            string query = "";

            query = "   select * from (select vm.Allocated_To, vm.Vendor_Name, vm.Vendor_Type, lig.Tool_No, lig.Station, lig.Rwk_No as Rwk, lig.P_No, lig.Outstanding_Qty as PendingQty, '' as Verified_By, lig.Challan_No,lig.Send_Receive_Date as Date_Of_Trans "
                    + " from Challan_Sent_Received_Detail lig left join parts_gr pgr on lig.tool_no=pgr.tool_no and lig.p_no=pgr.p_no and lig.part_name=pgr.part_name and lig.station=pgr.station and lig.rwk_No= pgr.rwk_no "
                    + " left join Vendor_Master_Job_Work vm on lig.Location=vm.Vendor_Name where lig.Outstanding_Qty >0 and lig.Sent_Or_Rec='Sent' and pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null)  and vm.Vendor_Type!='End Store' "
                    + " and( (case when Vendor_Type='Internal Vendor' then 1 else Item_Pending_Report_Required end) = vm.Item_Pending_Report_Required) "
                    + " union all "
                    + " select 'Sudheer Dabas' as Allocated_To,'Company' as Vendor_Name,'Internal Vendor' as Vendor_Type, phg.Tool_No, phg.Station, phg.Rwk_No, phg.P_No, phg.quantity, '' as Verified_By,'' as Challan_No, phg.Date_Of_Trans "
                    + " from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and "
                    + " phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no "
                    + " where phg.location='COMPANY' and phg.quantity>0 and (isnull(((phg.Upload_Qty)-(isnull(phg.QtySent,0))),0))>0 "
                    + " and (phg.Sent_Date is not null and "
                    + " phg.Received_Date is not null) and  pgr.Is_Active_Record=1  and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) "
                    + " union all "
                    + " select 'Kunal Dhiman' as Allocated_To,'Item Waiting For Process' as Vendor_Name, 'Internal Vendor' as Vendor_Type, phg.Tool_No, phg.Station, phg.Rwk_No, phg.P_No, "
                    + " phg.quantity, '' as Verified_By , '' as Challan_No, phg.Date_Of_Trans "
                    + " from part_history_GR phg left join parts_gr pgr on phg.tool_no=pgr.tool_no and phg.p_no=pgr.p_no and "
                    + " phg.part_name=pgr.part_name and phg.station=pgr.station and phg.rwk_no= pgr.rwk_no where "
                    + " phg.location='COMPANY' and phg.date_of_trans is not null and  "
                    + " (phg.Sent_Date is null  or  phg.Received_Date is null)  and  phg.quantity>0  and pgr.Is_Active_Record=1 and  ((pgr.Type!='NotRequired' and pgr.Type!='RatioUpdate') or pgr.Type is null) )dt";

            if (searchCondition != "")
            {
                query = query + " where " + searchCondition + "";
            }
            query = query + " order by Allocated_To Asc, Vendor_Name Asc, Date_Of_Trans Asc"; //PendingQty Desc,

            ad = new SqlDataAdapter(query, conn);
            ad.SelectCommand.CommandTimeout = 0;
            ad.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return dt;
        }
        finally
        {
            conn.Close();
        }
    }

    public static void ExportToExcel(string query, string fileName)
    {
        string constr = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand(query))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (System.Data.DataTable dt = new System.Data.DataTable())
                    {
                        sda.Fill(dt);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt, fileName);

                            HttpContext.Current.Response.Clear();
                            HttpContext.Current.Response.Buffer = true;
                            HttpContext.Current.Response.Charset = "";
                            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            string dateTime = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
                            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_" + dateTime + ".xlsx");
                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                                HttpContext.Current.Response.Flush();
                                HttpContext.Current.Response.End();
                            }
                        }
                    }
                }
            }
        }
    }
}