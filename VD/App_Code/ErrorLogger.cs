using System;
using System.Web;
using System.Text;
using System.IO;
using System.Configuration;
using System.Reflection;



/// <summary>
/// Generic class for log the all types of excetions.
/// </summary>
public static class ErrorLogger
{
    #region MEMBER CONSTRUCTOR
    /// <summary>
    /// static constructor
    /// </summary>
    static ErrorLogger()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion MEMBER CONSTRUCTOR

    #region PUBLIC METHODS
    /// <summary>
    /// This method logs the exection to the text file with all the exception details.
    /// </summary>
    /// <param name="strClassName">Class Name</param>
    /// <param name="strMethodName">Method Name</param>
    /// <param name="strMsg">Exception Message</param>
    /// <param name="strStatckTrace">Stack Trace of exception</param>
    public static void LogError(String strClassName, String strMethodName, String strMsg, String strStackTrace)
    {
        String strMessage = String.Empty;

        try
        {

            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/FTPLOG"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/FTPLOG");


            TextWriter oTextWriterF = File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "/FTPLOG/Log.txt");

                oTextWriterF.WriteLine("---------------------------------------------------------------------------------------------------------------");

                //Write the Log in the File
                strMessage = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + " : " + strClassName + " : " + strMethodName + " : " + strMsg + " : " + strStackTrace;

                oTextWriterF.WriteLine(strMessage);

                oTextWriterF.Close();
            
        }

        catch (Exception ex)
        {
            throw ex;
        }

    }
    #endregion PUBLIC METHODS
}
