﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PartsGr
/// </summary>
public class PartsGr
{
    public PartsGr()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string PartName { get; set; }
    public string PNo { get; set; }
    public int ToolNo { get; set; }
    public int ReqQty { get; set; }
    public string Matl { get; set; }
    public string Size { get; set; }
    public string Hrc { get; set; }
    public string Remark { get; set; }
    public string Station { get; set; }
    public string RwkNo { get; set; }
    public string Opr1 { get; set; }
    public string Opr2 { get; set; }
    public string Opr3 { get; set; }
    public string Opr4 { get; set; }
    public string Opr5 { get; set; }
    public string Opr6 { get; set; }
    public string Opr7 { get; set; }
    public string Opr8 { get; set; }
    public string Opr9 { get; set; }
    public string Opr10 { get; set; }
    public string Opr11 { get; set; }
    public string Opr12 { get; set; }
    public string Opr13 { get; set; }
    public string Opr14 { get; set; }
    public string Opr15 { get; set; }
    public string Opr16 { get; set; }
    public string Opr17 { get; set; }
    public string Rms { get; set; }
    public string RmQty { get; set; }
    public string WallThick { get; set; }
    public string Angle1 { get; set; }
    public string Angle2 { get; set; }
    public string ChildPositions { get; set; }
    public string ParentPositions { get; set; }
    public string Type { get; set; }
    public string GrReferenceNumber { get; set; }
    public string FilePath { get; set; }
    public DateTime TargetDate { get; set; }
    public bool IsChildPosition { get; set; }
    public bool IsParentExistInExcelWithRatioUpdateType {  get;  set;  }
    public string Version { get; set; }
}